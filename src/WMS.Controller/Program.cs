using Serilog;
using WMS.Business;
using WMS.Controller;
using WMS.Controller.Middlewares;
using WMS.Domain;

var builder = WebApplication.CreateBuilder(args);

builder.Host.UseSerilog((context, configuration) =>
{
    configuration.ReadFrom.Configuration(context.Configuration);
});

// Add services to the container.
builder.Services
    .ControllerConfig(builder.Configuration)
    .AddDomain(builder.Configuration)
    .AddBusiness();

var app = builder.Build();

try
{
    using var scope = app.Services.CreateScope();
    var dbInitializer = scope.ServiceProvider.GetRequiredService<DbInitializer>()!;
    dbInitializer.Seed().Wait();
}
catch (Exception e)
{
    var logger = app.Services.GetService<ILogger<Program>>()!;
    logger.LogError(e, "An error occurred while seeding the database");
}

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment() || app.Environment.IsStaging() || app.Environment.IsEnvironment("Local"))
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseSerilogRequestLogging();

app.ConfigureExceptionHandler();

app.UseCors(nameof(WMS));

//app.UseHttpsRedirection();

app.UseMiddleware<CheckAccessTokenMiddleware>();

app.UseAuthentication();

app.UseMiddleware<PermissionsMiddleware>();

app.UseAuthorization();

app.MapControllers();

app.Run();

public partial class Program { }
