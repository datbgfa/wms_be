﻿using System.Net;
using WMS.Business.Abstractions.Service;
using WMS.Business.Dtos.GlobalResponseDto;

namespace WMS.Controller.Middlewares
{
    public class CheckAccessTokenMiddleware : IMiddleware
    {
        private readonly ITokenService _tokenService;

        public CheckAccessTokenMiddleware(ITokenService tokenService)
        {
            _tokenService = tokenService;
        }

        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            string? bearer = context.Request.Headers.Authorization;
            string? token = bearer?.Replace("Bearer", "").Trim();

            if (string.IsNullOrWhiteSpace(token))
            {
                await next(context);
                return;
            }

            var isBlacked = _tokenService.IsBlacklistedToken(token);
            if (isBlacked)
            {
                context.Response.StatusCode = StatusCodes.Status401Unauthorized;
                await context.Response
                            .WriteAsJsonAsync(new ApiFormatResponse(StatusCodes.Status401Unauthorized, false, HttpStatusCode.Unauthorized.ToString()))
                            .ConfigureAwait(false);
                return;
            }

            await next(context);
        }
    }
}
