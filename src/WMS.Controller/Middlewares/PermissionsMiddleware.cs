﻿using System.Net;
using System.Security.Claims;
using WMS.Business.Abstractions.Service;
using WMS.Business.Dtos.GlobalResponseDto;

namespace WMS.Controller.Middlewares
{
    public class PermissionsMiddleware : IMiddleware
    {
        private readonly IUserService _userService;

        public PermissionsMiddleware(IUserService userService)
        {
            _userService = userService;
        }

        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            if (context.User.Identity == null || !context.User.Identity.IsAuthenticated)
            {
                await next(context);
                return;
            }

            var userSub = context.User.FindFirst(ClaimTypes.Name)?.Value;
            if (string.IsNullOrEmpty(userSub))
            {
                context.Response.StatusCode = StatusCodes.Status403Forbidden;
                await context.Response
                            .WriteAsJsonAsync(new ApiFormatResponse(StatusCodes.Status403Forbidden, false, HttpStatusCode.Forbidden.ToString()))
                            .ConfigureAwait(false);
                return;
            }

            var permissionsIdentity = await _userService.GetUserPermissionsIdentity(userSub)
                .ConfigureAwait(false);

            if (permissionsIdentity == null)
            {
                context.Response.StatusCode = StatusCodes.Status403Forbidden;
                await context.Response
                            .WriteAsJsonAsync(new ApiFormatResponse(StatusCodes.Status403Forbidden, false, HttpStatusCode.Forbidden.ToString()))
                            .ConfigureAwait(false);
                return;
            }

            context.User.AddIdentity(permissionsIdentity);
            await next(context);
        }
    }
}
