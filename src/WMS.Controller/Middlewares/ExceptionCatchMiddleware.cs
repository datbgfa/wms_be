﻿using Microsoft.AspNetCore.Diagnostics;
using System.Net;
using WMS.Business.Dtos.GlobalResponseDto;

namespace WMS.Controller.Middlewares
{
    public static class ExceptionCatchMiddleware
    {

        public static void ConfigureExceptionHandler(this IApplicationBuilder app)
        {
            var logger = app.ApplicationServices.GetService<ILogger>();

            app.UseExceptionHandler(appError =>
            {
                appError.Run(async context =>
                {
                    context.Response.ContentType = "application/json";
                    var contextFeature = context.Features.Get<IExceptionHandlerFeature>();
                    if (contextFeature != null)
                    {
                        string message = string.Empty;
                        int statusCode = 0;

                        switch (contextFeature.Error)
                        {
                            case UnauthorizedAccessException:
                                statusCode = StatusCodes.Status401Unauthorized;
                                message = HttpStatusCode.Unauthorized.ToString();
                                break;

                            default:
                                statusCode = StatusCodes.Status500InternalServerError;
                                message = HttpStatusCode.InternalServerError.ToString();
                                break;
                        }

                        context.Response.StatusCode = statusCode;
                        logger?.LogError(message);

                        await context.Response
                            .WriteAsJsonAsync(new ApiFormatResponse(statusCode, false, contextFeature.Error.Message))
                            .ConfigureAwait(false);
                    }
                });
            });

        }
    }
}
