﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using System.Collections.ObjectModel;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using WMS.Business.Abstractions.Service;
using WMS.Business.Dtos.CommonDto;
using WMS.Business.Dtos.OptionDto;
using WMS.Business.Dtos.UserDto;
using WMS.Domain.Constant;
using WMS.Domain.Enums;

namespace WMS.Controller.Controllers
{
    [ApiController]
    public class UserController : WmsControllerBase
    {
        private readonly IUserService _userService;
        private readonly ITokenService _tokenService;
        private readonly IEmailService _emailService;
        private readonly IMapper _mapper;
        private readonly ILogger<UserController> _logger;
        private readonly JwtOptions _options;
        private readonly IMemoryCache _cache;

        public UserController(IUserService userService,
            ITokenService tokenService,
            IEmailService emailService,
            IMapper mapper,
            ILogger<UserController> logger,
            IOptions<JwtOptions> options,
            IMemoryCache cache)
        {
            _userService = userService;
            _tokenService = tokenService;
            _emailService = emailService;
            _mapper = mapper;
            _logger = logger;
            _options = options.Value;
            _cache = cache;
        }

        [HttpPost("~/api/login")]
        public async Task<ActionResult<UserLoginResponseDto>> Login([FromBody] UserLoginRequestDto userRq)
        {
            var user = await _userService.AccountLogin(userRq);

            if (user is null)
            {
                return Unauthorized("Wrong username or password!");
            }

            if (user.Status != (short)UserStatusEnum.Active)
            {
                return StatusCode(403,"Account suspended!");
            }

            var userClaim = new Collection<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
                new Claim(JwtRegisteredClaimNames.Name, user.Username),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            };

            var accessToken = _tokenService.GenerateAccessToken(userClaim);
            var refreshToken = _tokenService.GenerateRefreshToken();

            var expiredTime = DateTime.UtcNow.AddDays(_options.RefreshTokenExpriedTimeInDays);

            bool isSuccessful = await _tokenService.SaveRefreshToken(user.UserId, refreshToken, accessToken, expiredTime);

            if (!isSuccessful)
            {
                return Conflict("Something when wrong!");
            }

            return Ok(new UserLoginResponseDto(accessToken, refreshToken));
        }

        [Authorize]
        [HttpGet("~/api/logout")]
        public async Task<IActionResult> Logout()
        {
            if (CurrentUser == "Unauthorized")
                return Unauthorized(null);

            var result = await _userService.Logout(CurrentUser)
                .ConfigureAwait(false);

            if (result)
            {
                _tokenService.RevokeAccessToken(JWTToken);
                return Ok();
            }
            else
                return BadRequest();
        }

        [HttpPut("~/api/reset-password")]
        public async Task<IActionResult> ResetPassword([FromBody] ResetPasswordRequestDto entity)
        {
            var result = await _userService.ResetPassword(entity)
                .ConfigureAwait(false);

            if (result) return Ok(null);

            return BadRequest("Username not found!");
        }

        [Authorize]
        [HttpPut("~/api/change-password")]
        public async Task<IActionResult> ChangePassword([FromBody] ChangePasswordRequestDto entity)
        {
            var result = await _userService.ChangePassword(entity)
                .ConfigureAwait(false);

            if (result) return Ok(null);

            return BadRequest("Username or password is wrong!");
        }

        [Authorize(Roles = CommonConstant.Admin)]
        [HttpGet("~/api/view-accounts-list")]
        public async Task<ActionResult<ViewPaging<ViewAccountsListResponseDto>>> ViewAccountsList([FromQuery] ViewAccountsListRequestDto entity)
        {
            var accountsList = await _userService.ViewAccountsList(entity).ConfigureAwait(false);
            if (accountsList == null) return BadRequest("Something is wrong");
            return Ok(accountsList);
        }

        [Authorize(Roles = CommonConstant.Admin)]
        [HttpPut("~/api/change-account-status")]
        public async Task<IActionResult> ChangeAccountStatus([FromBody] ChangeUserStatusRequestDto userRq)
        {
            if (userRq.Username.Equals(CurrentUser))
            {
                return BadRequest("You can't change own account status!");
            }

            var userId = await _userService.UpdateUserStatus(userRq);

            if (userId.Equals(Guid.Empty))
            {
                return Conflict("Something when wrong!");
            }

            var currentToken = await _tokenService.GetTokenByUserId(userId)
                .ConfigureAwait(false);

            if (currentToken is not null)
            {
                var revokeSuccess = _tokenService.RevokeAccessToken(currentToken.CurrentAccessToken);
                if (!revokeSuccess)
                {
                    _logger.LogError("Change account status successfully, but can't revoke accesstoken!");
                }
            }

            return Ok("Change successfully!");
        }

        [Authorize]
        [HttpGet("~/api/view-profile")]
        public async Task<ActionResult<ProfileInformationResponseDto>> ViewProfile()
        {
            var profileInformation = await _userService.GetProfileInformationByUsername(CurrentUser).ConfigureAwait(false);
            if (profileInformation == null)
            {
                return NotFound("Profile information is null");
            }
            return Ok(profileInformation);
        }

        [Authorize]
        [HttpPut("~/api/edit-profile")]
        public async Task<IActionResult> EditProfile([FromForm] EditProfileInformationRequestDto editProfile, [FromForm] IFormFile? avatarImage)
        {
            var result = await _userService.EditProfileInformationByUsername(editProfile, avatarImage, CurrentUser).ConfigureAwait(false);
            if (result == false)
            {
                return BadRequest("Editing profile failed!");
            }
            return Ok("Edit profile successfully!");
        }

        [Authorize(Roles = CommonConstant.Admin)]
        [HttpPost("~/api/create-employee-account")]
        public async Task<IActionResult> CreateAccount([FromForm] CreateUserAccountRequestDto userDto)
        {
            try
            {
                var created = await _userService.CreateAccount(userDto)
                    .ConfigureAwait(false);

                if (created == null)
                {
                    return BadRequest("Create new account failed!");
                }
            }
            catch (ArgumentException argEx)
            {
                return BadRequest(argEx.Message);
            }

            return Ok("Create new account successfully!");
        }

        [HttpPost("~/api/refresh-token")]
        public async Task<IActionResult> RefreshToken([FromBody] RefreshTokenRequestDto userRq)
        {
            var token = await _tokenService.IsValidRefreshToken(userRq.RefreshToken);
            if (token is null)
            {
                return Unauthorized("Unauthorized");
            }

            _tokenService.RevokeAccessToken(token.CurrentAccessToken);

            var user = await _userService.FindUserById(token.UserId);
            if (user is null)
            {
                return Unauthorized("Unauthorized");
            }

            var userClaim = new Collection<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
                new Claim(JwtRegisteredClaimNames.Name, user.Username),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            };

            var accessToken = _tokenService.GenerateAccessToken(userClaim);
            var isSaved = await _tokenService.UpdateAccessToken(user.UserId, accessToken);

            if (!isSaved)
            {
                return Unauthorized("Unauthorized");
            }

            return Ok(new UserLoginResponseDto(accessToken, userRq.RefreshToken));
        }

        [Authorize]
        [HttpGet("~/api/get-role")]
        public async Task<ActionResult> GetUserRole()
        {
            return Ok(await _userService.GetUserRoleByUsername(CurrentUser).ConfigureAwait(false));
        }


        [Authorize(Roles = $"{CommonConstant.Admin},{CommonConstant.BaseManager},{CommonConstant.BranchDirector}")]
        [HttpGet("~/api/check-email-exist")]
        public async Task<IActionResult> CheckEmailExist([FromQuery] string email)
        {
            var emailExist = await _userService.FindUserByEmail(email).ConfigureAwait(false);
            if (emailExist != null) return Ok(false);
            return Ok(true);
        }

        [Authorize(Roles = $"{CommonConstant.Admin},{CommonConstant.BaseManager},{CommonConstant.BranchDirector}")]
        [HttpGet("~/api/check-username-exist")]
        public async Task<IActionResult> CheckUsernameExist([FromQuery] string username)
        {
            var usernameExist = await _userService.FindUserByUsername(username).ConfigureAwait(false);
            if (usernameExist != null) return Ok(false);
            return Ok(true);
        }
    }
}
