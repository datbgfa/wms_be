﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WMS.Business.Abstractions.Service;

namespace WMS.Controller.Controllers
{

    [ApiController]
    public class StockCheckingController : WmsControllerBase
    {
        private readonly IStockCheckingService _stockCheckingService;
        private readonly ILogger<StockCheckingController> _logger;

        public StockCheckingController(ILogger<StockCheckingController> logger,
            IStockCheckingService stockCheckingService)
        {
            _logger = logger;
            _stockCheckingService = stockCheckingService;
        }

        [Authorize]
        [HttpGet("~/api/get-product-quantity")]
        public async Task<ActionResult<int>> GetProductQuantity([FromQuery] IEnumerable<Guid> productIds)
        {

            var warehouseInformation = await _stockCheckingService.GetProductQuantity(productIds, CurrentUser).ConfigureAwait(false);
            return Ok(warehouseInformation);
        }
    }
}
