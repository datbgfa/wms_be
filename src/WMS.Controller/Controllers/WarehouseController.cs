﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WMS.Business.Abstractions.Service;
using WMS.Business.Dtos.BranchWarehouseDto;
using WMS.Business.Dtos.WarehouseDto;
using WMS.Domain.Constant;

namespace WMS.Controller.Controllers
{

    [ApiController]
    public class WarehouseController : WmsControllerBase
    {
        private readonly IWarehouseService _warehouseService;
        private readonly IMapper _mapper;
        private readonly ILogger<WarehouseController> _logger;

        public WarehouseController(IWarehouseService warehouseService,
            IMapper mapper,
            ILogger<WarehouseController> logger)
        {
            _warehouseService = warehouseService;
            _mapper = mapper;
            _logger = logger;
        }

        [Authorize(Roles = $"{CommonConstant.BaseManager},{CommonConstant.BranchDirector}")]
        [HttpGet("~/api/view-warehouse-profile")]
        public async Task<ActionResult<ViewWarehouseProfileResponseDto>> ViewWarehouseProfile()
        {

            var warehouseInformation = await _warehouseService.ViewWarehouseProfile(CurrentUser).ConfigureAwait(false);
            if (warehouseInformation == null) return NotFound("Not found warehouse!!");
            return Ok(warehouseInformation);
        }

        [Authorize(Roles = $"{CommonConstant.BaseManager},{CommonConstant.BranchDirector}")]
        [HttpPut("~/api/edit-warehouse-profile")]
        public async Task<ActionResult<string>> EditWarehouseProfile([FromBody] EditWarehouseProfileRequestDto newWarehouseInformation)
        {
            var warehouseInformation = await _warehouseService.EditWarehouseProfile(newWarehouseInformation, CurrentUser).ConfigureAwait(false);
            if (!warehouseInformation) return BadRequest("Edit warehouse profile failed");
            return Ok("Edit warehouse profile successful");
        }

        [Authorize(Roles = $"{CommonConstant.BaseManager},{CommonConstant.BranchDirector},{CommonConstant.BranchManager},{CommonConstant.Admin}")]
        [HttpGet("~/api/view-all-base-warehouses-list")]
        public async Task<ActionResult<IEnumerable<ViewBranchWarehousesListResponseDto>>> ViewBaseWarehousesList()
        {
            var basewarehouse = await _warehouseService.ViewAllBaseWarehousesList().ConfigureAwait(false);
            if (basewarehouse == null) return BadRequest();
            return Ok(basewarehouse);
        }

        [Authorize(Roles = $"{CommonConstant.BranchDirector},{CommonConstant.BranchManager},{CommonConstant.BranchEmployee}")]
        [HttpGet("~/api/get-branch-warehouse-by-username")]
        public async Task<ActionResult<ViewBranchWarehousesListResponseDto>> GetWarehouseByUsername()
        {
            var warehouse = await _warehouseService.GetBranchWarehouseByUsername(CurrentUser).ConfigureAwait(false);
            if (warehouse == null) return BadRequest();
            return Ok(warehouse);
        }

        [Authorize(Roles = $"{CommonConstant.BranchDirector},{CommonConstant.BranchManager},{CommonConstant.BranchEmployee}")]
        [HttpGet("~/api/get-branch-warehouse-by-current-user")]
        public async Task<ActionResult<ViewBranchWarehousesListResponseDto>> GetWarehouseByCurrentUser()
        {
            var warehouse = await _warehouseService.GetBranchWarehouseByCurrentUser(CurrentUser).ConfigureAwait(false);
            if (warehouse == null) return BadRequest();
            return Ok(warehouse);
        }
    }
}
