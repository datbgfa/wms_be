﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WMS.Business.Abstractions.Service;
using WMS.Business.Dtos.CommonDto;
using WMS.Business.Dtos.EmployeeDto;
using WMS.Domain.Constant;

namespace WMS.Controller.Controllers
{
    [ApiController]
    public class EmployeeController : WmsControllerBase
    {
        private readonly IEmployeeService _employeeService;
        private readonly ILogger<EmployeeController> _logger;

        public EmployeeController(IEmployeeService employeeService,
            ILogger<EmployeeController> logger)
        {
            _employeeService = employeeService;
            _logger = logger;
        }

        [Authorize(Roles = $"{CommonConstant.BaseManager},{CommonConstant.BranchDirector}")]
        [HttpGet("~/api/view-employees-list")]
        public async Task<ActionResult<ViewPaging<ViewEmployeesListResponseDto>>> ViewEmployeesList([FromQuery] ViewEmployeesListRequestDto entity)
        {
            var result = await _employeeService.ViewEmployeesList(entity, CurrentUser).ConfigureAwait(false);
            if (result == null) return BadRequest("Something is wrong!");
            return Ok(result);
        }

        [Authorize(Roles = $"{CommonConstant.BaseManager},{CommonConstant.BranchDirector}")]
        [HttpGet("~/api/view-employees-details")]
        public async Task<ActionResult<ViewEmployeeDetailsResponseDto>> ViewEmployeeDetails([FromQuery] Guid employeeId)
        {
            var employee = await _employeeService.ViewEmployeeDetails(employeeId, CurrentUser).ConfigureAwait(false);
            if (employee == null) return BadRequest("Something is wrong!");
            return Ok(employee);
        }

        [Authorize(Roles = $"{CommonConstant.BaseManager},{CommonConstant.BranchDirector}")]
        [HttpPost("~/api/create-employee")]
        public async Task<IActionResult> CreateEmployee([FromForm] CreateEmployeeRequestDto entity)
        {
            var result = await _employeeService.CreateEmployee(entity, CurrentUser).ConfigureAwait(false);
            if (!result) return Conflict("Fail to create new employee!");
            return Ok("Employee created successfully");
        }

        [Authorize(Roles = $"{CommonConstant.BaseManager},{CommonConstant.BranchDirector}")]
        [HttpPut("~/api/update-employee-status")]
        public async Task<IActionResult> UpdateEmployeeStatus([FromBody] UpdateEmployeeStatusRequestDto entity)
        {
            var result = await _employeeService.UpdateEmployeeStatus(entity, CurrentUser).ConfigureAwait(false);
            if (!result) return Conflict("Something is wrong!");
            return Ok("Employees updated successfully");
        }


        [Authorize]
        [HttpGet("~/api/get-all-base-employee")]
        public async Task<ActionResult<ViewEmployeesListResponseDto>> GetAllBaseEmployee()
        {
            var result = await _employeeService.GetAllBaseEmployee().ConfigureAwait(false);
            if (result == null) return BadRequest();
            return Ok(result);
        }

        [Authorize]
        [HttpGet("~/api/get-all-branch-employee")]
        public async Task<ActionResult<ViewEmployeesListResponseDto>> GetAllBranchEmployee()
        {
            var result = await _employeeService.GetAllBranchEmployee(CurrentUser).ConfigureAwait(false);
            if (result == null) return BadRequest();
            return Ok(result);
        }
    }
}
