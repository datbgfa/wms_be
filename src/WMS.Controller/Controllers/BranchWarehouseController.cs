﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WMS.Business.Abstractions.Service;
using WMS.Business.Dtos.BranchWarehouseDto;
using WMS.Business.Dtos.CommonDto;
using WMS.Domain.Constant;

namespace WMS.Controller.Controllers
{

    [ApiController]
    public class BranchWarehouseController : WmsControllerBase
    {
        private readonly IBranchWarehouseService _branchWarehouseService;
        private readonly ILogger<BranchWarehouseController> _logger;

        public BranchWarehouseController(IBranchWarehouseService branchWarehouseService,
            ILogger<BranchWarehouseController> logger)
        {
            _branchWarehouseService = branchWarehouseService;
            _logger = logger;
        }

        [Authorize(Roles = CommonConstant.BaseManager)]
        [HttpGet("~/api/view-branch-warehouses-list")]
        public async Task<ActionResult<ViewPaging<ViewBranchWarehousesListResponseDto>>> ViewBranchWarehousesList([FromQuery] ViewBranchWarehousesListRequestDto entity)
        {
            var branchWarehouses = await _branchWarehouseService.ViewBranchWarehouseList(entity).ConfigureAwait(false);
            if (branchWarehouses == null) return BadRequest();
            return Ok(branchWarehouses);
        }

        [Authorize(Roles = CommonConstant.BaseManager)]
        [HttpGet("~/api/view-branch-warehouse-details")]
        public async Task<ActionResult<ViewBranchWarehouseDetailsResponseDto>> ViewBranchWarehouseDetails([FromQuery] Guid warehouseId)
        {
            var branchWarehouse = await _branchWarehouseService.ViewBranchWarehouseDetails(warehouseId).ConfigureAwait(false);
            if (branchWarehouse == null) return BadRequest();
            return Ok(branchWarehouse);
        }

        [Authorize(Roles = CommonConstant.BaseManager)]
        [HttpPut("~/api/update-branch-warehouse-details")]
        public async Task<IActionResult> UpdateBranchWarehouseDetails([FromBody] UpdateBranchWarehouseDetailsRequestDto entity)
        {
            var result = await _branchWarehouseService.UpdateBranchWarehouseDetails(entity).ConfigureAwait(false);
            if (!result) return BadRequest();
            return Ok("Branch warehouse updated successfully!");
        }

        [Authorize(Roles = CommonConstant.BaseManager)]
        [HttpPost("~/api/create-branch-warehouse")]
        public async Task<IActionResult> CreateBranchWarehouse([FromBody] CreateBranchWarehouseRequestDto entity)
        {
            var result = await _branchWarehouseService.CreateBranchWarehouse(entity).ConfigureAwait(false);
            if (!result) return BadRequest("Create branch warehouse fail!");
            return Ok("Branch warehouse created successfully!");
        }

        [Authorize(Roles = $"{CommonConstant.BaseManager},{CommonConstant.BaseEmployee},{CommonConstant.Admin}")]
        [HttpGet("~/api/view-all-branch-warehouses-list")]
        public async Task<ActionResult<IEnumerable<ViewBranchWarehousesListResponseDto>>> ViewAllBranchWarehousesList([FromQuery] bool? hasOwner)
        {
            var branchWarehouses = await _branchWarehouseService.ViewAllBranchWarehousesList(hasOwner).ConfigureAwait(false);
            if (branchWarehouses == null) return BadRequest();
            return Ok(branchWarehouses);
        }

        [Authorize(Roles = $"{CommonConstant.BaseManager},{CommonConstant.BaseEmployee}")]
        [HttpGet("~/api/get-all-branch-warehouses")]
        public async Task<ActionResult<IEnumerable<ViewBranchWarehousesListResponseDto>>> GetAllBranchWarehouse()
        {
            var branchWarehouses = await _branchWarehouseService.GetAllBranchWarehouse().ConfigureAwait(false);
            if (branchWarehouses == null) return BadRequest();
            return Ok(branchWarehouses);
        }

        [Authorize(Roles = CommonConstant.BaseManager)]
        [HttpGet("~/api/get-branch-have-debt")]
        public async Task<ActionResult<List<BranchWarehouseHaveDebtResponseDto>>> GetBranchWarehouseHaveDebt()
        {
            var result = await _branchWarehouseService.GetBranchHaveDebt();
            return Ok(result);
        }
    }
}
