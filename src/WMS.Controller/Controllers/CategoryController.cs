﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WMS.Business.Abstractions.Service;
using WMS.Business.Dtos.CategoryDto;
using WMS.Domain.Constant;

namespace WMS.Controller.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : WmsControllerBase
    {
        private readonly ICategoryService _categoryService;

        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        [Authorize(Roles = CommonConstant.BaseManager)]
        [HttpGet("~/api/view-categories-list")]
        public async Task<ActionResult> ViewCategoriesList([FromQuery] CategoryRequestDto entity)
        {
            var result = await _categoryService.GetAllWithPaging(entity).ConfigureAwait(false);
            return Ok(result);
        }

        [Authorize(Roles = $"{CommonConstant.BaseManager},{CommonConstant.BaseEmployee},{CommonConstant.BranchDirector},{CommonConstant.BranchManager},{CommonConstant.BranchEmployee}")]
        [HttpGet("~/api/view-all-categorys")]
        public async Task<ActionResult> ViewAllCategories()
        {
            var result = await _categoryService.GetAll().ConfigureAwait(false);
            return Ok(result);
        }

        [Authorize(Roles = CommonConstant.BaseManager)]
        [HttpPost("~/api/create-category")]
        public async Task<IActionResult> CreateCategory([FromBody] AddCategoryDto categoryName)
        {

            var result = await _categoryService.AddCategory(categoryName.CategoryName).ConfigureAwait(false);
            if (result)
            {
                return Ok("Add category successful!");
            }
            return BadRequest("Add category failed");

        }

        [Authorize(Roles = CommonConstant.BaseManager)]
        [HttpPut("~/api/update-category")]
        public async Task<IActionResult> UpdateCategory([FromBody] UpdateCategoryDto category)
        {
            var result = await _categoryService.UpdateCategory(category).ConfigureAwait(false);
            if (result)
            {
                return Ok("Update category successful!");
            }
            return BadRequest("Update category failed");
        }
    }
}
