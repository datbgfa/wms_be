﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WMS.Business.Abstractions.Service;
using WMS.Business.Dtos.BaseExportDto;
using WMS.Business.Dtos.BranchExportDto;
using WMS.Business.Dtos.CommonDto;
using WMS.Domain.Constant;

namespace WMS.Controller.Controllers
{
    [ApiController]
    public class BranchExportController : WmsControllerBase
    {
        private readonly IBranchExportService _branchExportService;
        private readonly ILogger<BaseExportController> _logger;

        public BranchExportController(IBranchExportService branchExportService,
            ILogger<BaseExportController> logger)
        {
            _branchExportService = branchExportService;
            _logger = logger;
        }

        [Authorize(Roles = $"{CommonConstant.BranchDirector},{CommonConstant.BranchManager},{CommonConstant.BranchEmployee}")]
        [HttpGet("~/api/view-branch-exports-list")]
        public async Task<ActionResult<ViewPaging<GetBranchExportResponseDto>>> ViewExportsList([FromQuery] GetBranchExportRequestDto requestDto)
        {
            var result = await _branchExportService.GetExportsList(requestDto, CurrentUser).ConfigureAwait(false);
            if (result == null) return NotFound("Exports List is empty");
            return Ok(result);
        }

        [Authorize(Roles = $"{CommonConstant.BranchDirector},{CommonConstant.BranchManager},{CommonConstant.BranchEmployee}")]
        [HttpGet("~/api/view-branch-export-details")]
        public async Task<ActionResult<GetBranchExportResponseDto>> ViewExportDetails([FromQuery] Guid exportId)
        {
            var result = await _branchExportService.GetExportsDetails(exportId).ConfigureAwait(false);
            if (result == null) return NotFound("Export details is empty");
            return Ok(result);
        }

        [Authorize(Roles = CommonConstant.BranchDirector)]
        [HttpPost("~/api/create-branch-export")]
        public async Task<ActionResult<Guid>> CreateBranchExport([FromBody] CreateBranchExportRequestDto requestDto)
        {
            var result = await _branchExportService.CreateBranchExport(requestDto, CurrentUser).ConfigureAwait(false);
            if (result == Guid.Empty) return BadRequest("Create branch export failed");
            return Ok(result);
        }

    }
}
