﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WMS.Business.Abstractions.Service;
using WMS.Business.Dtos.BaseImportDto;
using WMS.Business.Dtos.CommonDto;
using WMS.Business.Dtos.WarehouseDto;
using WMS.Domain.Constant;

namespace WMS.Controller.Controllers
{
    [ApiController]
    public class BaseImportController : WmsControllerBase
    {
        private readonly IBaseImportService _baseImportService;
        private readonly ILogger<BaseImportController> _logger;

        public BaseImportController(IBaseImportService baseImportService,
            ILogger<BaseImportController> logger)
        {
            _baseImportService = baseImportService;
            _logger = logger;
        }

        [Authorize(Roles = $"{CommonConstant.BaseManager},{CommonConstant.BaseEmployee}")]
        [HttpPost("~/api/create-base-import")]
        public async Task<ActionResult<Guid>> CreateBaseImport([FromBody] CreateBaseImportRequestDto entity)
        {
            var result = await _baseImportService.CreateBaseImport(entity, CurrentUser).ConfigureAwait(false);
            if (result == null) return BadRequest();
            return Ok(result);
        }

        /// <summary>
        /// GetBaseImportList 
        /// input paging and searchWord, DateFilter, importStatus, paymentStatus
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        [Authorize(Roles = $"{CommonConstant.BaseManager},{CommonConstant.BaseEmployee}")]
        [HttpGet("~/api/view-base-import-list")]
        public async Task<ActionResult<ViewPaging<GetBaseImportResponseDto>>> GetBaseImportList([FromQuery] GetBaseImportRequestDto entity)
        {
            var result = await _baseImportService.GetBaseImportList(entity).ConfigureAwait(false);
            if (result == null) return BadRequest();
            return Ok(result);
        }

        /// <summary>
        /// GetBaseImportDetails
        /// </summary>
        /// <param name="baseImportId"></param>
        /// <returns>GetBaseImportDetailsResponseDto</returns>
        [Authorize(Roles = $"{CommonConstant.BaseManager},{CommonConstant.BaseEmployee}")]
        [HttpGet("~/api/view-base-import-details")]
        public async Task<ActionResult<GetBaseImportDetailsResponseDto>> GetBaseImportDetails([FromQuery] Guid baseImportId)
        {
            var result = await _baseImportService.GetBaseImportDetails(baseImportId).ConfigureAwait(false);
            if (result == null) return BadRequest();
            return Ok(result);
        }

        [Authorize(Roles = $"{CommonConstant.BaseManager},{CommonConstant.BaseEmployee}")]
        [HttpPut("~/api/update-base-import")]
        public async Task<IActionResult> UpdateBaseImport([FromBody] UpdateBaseImportRequestDto entity)
        {
            var result = await _baseImportService.UpdateBaseImport(entity, CurrentUser).ConfigureAwait(false);
            if (!result) return BadRequest();
            return Ok("Update import successfully!");
        }

        [Authorize(Roles = $"{CommonConstant.BaseManager},{CommonConstant.BaseEmployee}")]
        [HttpPut("~/api/update-base-import-note")]
        public async Task<IActionResult> UpdateBaseImportNote([FromBody] UpdateBaseImportNoteRequestDto entity)
        {
            var result = await _baseImportService.UpdateBaseImportNote(entity, CurrentUser).ConfigureAwait(false);
            if (!result) return BadRequest();
            return Ok("Update import successfully!");
        }

        [Authorize(Roles = CommonConstant.BaseManager)]
        [HttpPut("~/api/pay-for-base-import")]
        public async Task<IActionResult> PayForBaseImport([FromBody] PayForBaseImportRequestDto entity)
        {
            var result = await _baseImportService.PayForBaseImport(entity, CurrentUser).ConfigureAwait(false);
            if (!result) return BadRequest();
            return Ok("Pay for import successfully!");
        }

        [Authorize(Roles = $"{CommonConstant.BaseManager},{CommonConstant.BaseEmployee}")]
        [HttpPut("~/api/change-base-import-status/{importId}")]
        public async Task<IActionResult> ChangeBaseImportStatus([FromRoute] Guid importId)
        {
            var result = await _baseImportService.ChangeBaseImportStatus(importId, CurrentUser).ConfigureAwait(false);
            if (!result) return BadRequest();
            return Ok("Change import status successfully!");
        }

        [Authorize(Roles = CommonConstant.BaseManager)]
        [HttpPut("~/api/pay-for-base-product-return")]
        public async Task<IActionResult> PayForBaseProductReturn([FromBody] PayForBaseProductReturnRequestDto entity)
        {
            var result = await _baseImportService.PayForBaseProductReturn(entity, CurrentUser).ConfigureAwait(false);
            if (!result) return BadRequest();
            return Ok("Paid successfully!");
        }
    }
}
