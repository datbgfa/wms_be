﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WMS.Business.Abstractions.Service;
using WMS.Business.Dtos.BranchImportDto;
using WMS.Business.Dtos.BranchWarehouseDto;
using WMS.Business.Dtos.CommonDto;
using WMS.Domain.Constant;

namespace WMS.Controller.Controllers
{
    [ApiController]
    public class BranchImportController : WmsControllerBase
    {
        private readonly IBranchImportService _branchImportService;
        private readonly ILogger<BranchImportController> _logger;

        public BranchImportController(IBranchImportService branchImportService,
            ILogger<BranchImportController> logger)
        {
            _branchImportService = branchImportService;
            _logger = logger;
        }

        [Authorize(Roles = $"{CommonConstant.BranchDirector},{CommonConstant.BranchManager}")]
        [HttpPost("~/api/create-branch-import-request")]
        public async Task<IActionResult> CreateBranchImportRequest([FromBody] CreateBranchImportRequestRequestDto entity)
        {
            var result = await _branchImportService.CreateBranchImportRequest(entity, CurrentUser).ConfigureAwait(false);
            if (result == null) return BadRequest();
            return Ok(result);
        }

        [Authorize(Roles = $"{CommonConstant.BranchDirector},{CommonConstant.BranchManager}")]
        [HttpPut("~/api/update-branch-import-request")]
        public async Task<IActionResult> UpdateBranchImportRequest([FromBody] UpdateBranchImportRequestRequestDto entity)
        {
            var result = await _branchImportService.UpdateBranchImportRequest(entity, CurrentUser).ConfigureAwait(false);
            if (!result) return BadRequest();
            return Ok("Update  request successfully!");
        }

        [Authorize(Roles = CommonConstant.BranchDirector)]
        [HttpPut("~/api/approve-branch-import-request/{requestId}")]
        public async Task<IActionResult> ApproveBranchImportRequest([FromRoute] Guid requestId)
        {
            var result = await _branchImportService.ApproveBranchImportRequest(requestId, CurrentUser).ConfigureAwait(false);
            if (!result) return BadRequest();
            return Ok("Approve request successfully!");
        }

        [Authorize(Roles = CommonConstant.BranchDirector)]
        [HttpPut("~/api/refuse-branch-import-request/{requestId}")]
        public async Task<IActionResult> RefuseBranchImportRequest([FromRoute] Guid requestId)
        {
            var result = await _branchImportService.RefuseBranchImportRequest(requestId, CurrentUser).ConfigureAwait(false);
            if (!result) return BadRequest();
            return Ok("Refuse request successfully!");
        }

        [Authorize(Roles = $"{CommonConstant.BranchDirector},{CommonConstant.BranchManager},{CommonConstant.BranchEmployee}")]
        [HttpPut("~/api/change-branch-import-status/{importId}")]
        public async Task<IActionResult> ChangeBranchImportStatus([FromRoute] Guid importId)
        {
            var result = await _branchImportService.ChangeBranchImportStatus(importId, CurrentUser).ConfigureAwait(false);
            if (!result) return BadRequest();
            return Ok("Change import status successfully!");
        }

        [Authorize(Roles = $"{CommonConstant.BranchDirector},{CommonConstant.BranchManager},{CommonConstant.BranchEmployee}")]
        [HttpGet("~/api/view-branch-import-list")]
        public async Task<ActionResult<ViewPaging<GetBranchImportResponseDto>>> GetBranchImportList([FromQuery] GetBranchImportRequestDto requestDto)
        {
            var result = await _branchImportService.GetBranchImportList(requestDto, CurrentUser).ConfigureAwait(false);
            if (result == null) return NotFound("branch import empty");
            return Ok(result);
        }

        [Authorize(Roles = $"{CommonConstant.BranchDirector},{CommonConstant.BranchManager},{CommonConstant.BranchEmployee}")]
        [HttpGet("~/api/view-branch-import-details")]
        public async Task<ActionResult<GetBranchImportDetailsResponseDto>> GetBranchImportDetails([FromQuery] Guid branchImportId)
        {
            var result = await _branchImportService.GetBranchImportDetails(branchImportId).ConfigureAwait(false);
            if (result == null) return NotFound("branch import destails empty");
            return Ok(result);
        }

        [Authorize(Roles = CommonConstant.BaseManager)]
        [HttpPut("~/api/pay-for-branch-product-return")]
        public async Task<IActionResult> PayForBranchProductReturn([FromBody] PayForBranchProductReturnRequestDto entity)
        {
            var result = await _branchImportService.PayForBranchProductReturn(entity, CurrentUser).ConfigureAwait(false);
            if (!result) return BadRequest();
            return Ok("Paid successfully!");
        }
    }
}
