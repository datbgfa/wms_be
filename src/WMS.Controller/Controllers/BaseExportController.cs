﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WMS.Business.Abstractions.Service;
using WMS.Business.Dtos.BaseExportDto;
using WMS.Business.Dtos.CommonDto;
using WMS.Business.Dtos.WarehouseDto;
using WMS.Domain.Constant;

namespace WMS.Controller.Controllers
{
    [ApiController]
    public class BaseExportController : WmsControllerBase
    {
        private readonly IBaseExportService _baseExportService;
        private readonly ILogger<BaseExportController> _logger;

        public BaseExportController(IBaseExportService baseExportService,
            ILogger<BaseExportController> logger)
        {
            _baseExportService = baseExportService;
            _logger = logger;
        }

        [Authorize(Roles = CommonConstant.BaseManager)]
        [HttpPut("~/api/pay-for-base-export")]
        public async Task<IActionResult> PayForBaseExport([FromBody] PayForBaseExportRequestDto entity)
        {
            var result = await _baseExportService.PayForBaseExport(entity, CurrentUser).ConfigureAwait(false);
            if (!result) return BadRequest();
            return Ok("Pay for export successfully!");
        }

        [Authorize(Roles = $"{CommonConstant.BaseManager},{CommonConstant.BaseEmployee}")]
        [HttpPut("~/api/update-base-export")]
        public async Task<IActionResult> UpdateBaseExport([FromBody] UpdateBaseExportRequestDto entity)
        {
            var result = await _baseExportService.UpdateBaseExport(entity, CurrentUser).ConfigureAwait(false);
            if (!result) return BadRequest();
            return Ok("Update export successfully!");
        }

        [Authorize(Roles = $"{CommonConstant.BaseManager},{CommonConstant.BaseEmployee}")]
        [HttpGet("~/api/view-exports-list")]
        public async Task<ActionResult<ViewPaging<GetBaseExportResponseDto>>> ViewExportsList([FromQuery] GetBaseExportRequestDto entity)
        {
            var result = await _baseExportService.GetExportsList(entity).ConfigureAwait(false);
            if (result == null) return NotFound("ExportsList is empty");
            return Ok(result);
        }

        [Authorize(Roles = $"{CommonConstant.BaseManager},{CommonConstant.BaseEmployee}")]
        [HttpGet("~/api/view-export-details")]
        public async Task<ActionResult<GetBaseExportResponseDetailsDto>> ViewExportDetails([FromQuery] Guid exportId)
        {
            var result = await _baseExportService.GetExportsDetails(exportId).ConfigureAwait(false);
            if (result == null) return NotFound("Export details is empty");
            return Ok(result);
        }
    }
}
