﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WMS.Business.Abstractions.Service;
using WMS.Business.Dtos.BranchRequestDto;
using WMS.Business.Dtos.CommonDto;
using WMS.Domain.Constant;

namespace WMS.Controller.Controllers
{
    [ApiController]
    public class BranchRequestController : WmsControllerBase
    {
        private readonly IBranchRequestService _branchRequestService;
        private readonly ILogger<BranchRequestController> _logger;

        public BranchRequestController(IBranchRequestService branchRequestService,
            ILogger<BranchRequestController> logger)
        {
            _branchRequestService = branchRequestService;
            _logger = logger;
        }

        [Authorize(Roles = $"{CommonConstant.BranchDirector},{CommonConstant.BranchManager}")]
        [HttpGet("~/api/get-branch-import-request")]
        public async Task<ActionResult<ViewPaging<GetBranchRequestReponseDto>>> GetBranchImportRequest([FromQuery] GetBranchRequestListRequestDto entity)
        {
            var result = await _branchRequestService.FilterBranchRequest(entity, CurrentUser).ConfigureAwait(false);
            if (result == null) return NotFound("List empty");
            return Ok(result);
        }

        [Authorize(Roles = $"{CommonConstant.BranchDirector},{CommonConstant.BranchManager}")]
        [HttpGet("~/api/get-branch-import-request-details")]
        public async Task<ActionResult<GetBranchRequestReponseDto>> GetBranchImportRequestDetails([FromQuery] Guid branchRequestId)
        {
            var result = await _branchRequestService.GetBranchRequestDetails(branchRequestId).ConfigureAwait(false);
            if (result == null) return NotFound("branchRequestId not exist!");
            return Ok(result);
        }
    }
}
