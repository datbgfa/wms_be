﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WMS.Business.Abstractions.Service;
using WMS.Business.Dtos.StockCheckSheetDto;
using WMS.Domain.Constant;

namespace WMS.Controller.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StockCheckSheetController : WmsControllerBase
    {
        private readonly IStockCheckSheetService _stockCheckSheetService;
        private readonly ILogger<StockCheckSheetController> _logger;

        public StockCheckSheetController(IStockCheckSheetService stockCheckSheetService,
            ILogger<StockCheckSheetController> logger)
        {
            _stockCheckSheetService = stockCheckSheetService;
            _logger = logger;
        }

        [Authorize(Roles = $"{CommonConstant.BaseManager},{CommonConstant.BaseEmployee},{CommonConstant.BranchDirector},{CommonConstant.BranchManager},{CommonConstant.BranchEmployee}")]
        [HttpGet("~/api/view-stock-check-sheet")]
        public async Task<IActionResult> GetStockCheckSheetList([FromQuery] StockCheckSheetRequestDto entity)
        {
            var result = await _stockCheckSheetService.GetAllStockCheckSheet(entity, CurrentUser).ConfigureAwait(false);
            return Ok(result);
        }

        [Authorize(Roles = $"{CommonConstant.BaseManager},{CommonConstant.BaseEmployee},{CommonConstant.BranchDirector},{CommonConstant.BranchManager},{CommonConstant.BranchEmployee}")]
        [HttpGet("~/api/view-stock-check-sheet-by-id")]
        public async Task<IActionResult> GetStockCheckSheetById([FromQuery] StockCheckSheetDetailsRequestDto entity)
        {
            var result = await _stockCheckSheetService.GetStockCheckSheetById(entity, CurrentUser).ConfigureAwait(false);
            return Ok(result);
        }

        [Authorize(Roles = $"{CommonConstant.BaseManager},{CommonConstant.BaseEmployee},{CommonConstant.BranchDirector},{CommonConstant.BranchManager},{CommonConstant.BranchEmployee}")]
        [HttpGet("~/api/view-stock-check-sheet-details")]
        public async Task<IActionResult> GetStockCheckSheetDetails([FromQuery] StockCheckSheetDetailsRequestDto entity)
        {
            var result = await _stockCheckSheetService.GetAllStockCheckSheetDetails(entity, CurrentUser).ConfigureAwait(false);
            if (result is null)
            {
                return BadRequest();
            }
            return Ok(result);
        }

        [Authorize(Roles = $"{CommonConstant.BaseManager},{CommonConstant.BaseEmployee},{CommonConstant.BranchDirector},{CommonConstant.BranchManager},{CommonConstant.BranchEmployee}")]
        [HttpGet("~/api/view-stock-check-sheet-imports")]
        public async Task<IActionResult> GetStockCheckSheetImports([FromQuery] StockCheckSheetImportsRequestDto entity)
        {
            var result = await _stockCheckSheetService.GetAllStockCheckSheetImports(entity, CurrentUser).ConfigureAwait(false);
            if (result is null)
            {
                return BadRequest();
            }
            return Ok(result);
        }

        [Authorize(Roles = $"{CommonConstant.BaseManager},{CommonConstant.BaseEmployee},{CommonConstant.BranchDirector},{CommonConstant.BranchManager},{CommonConstant.BranchEmployee}")]
        [HttpGet("~/api/get-imports-by-productId")]
        public async Task<IActionResult> GetImportsByProductId([FromQuery] StockCheckSheetImportsRequestDto entity)
        {
            var result = await _stockCheckSheetService.GetImportsByProductId(entity, CurrentUser).ConfigureAwait(false);
            return Ok(result);
        }

        [Authorize(Roles = $"{CommonConstant.BaseManager},{CommonConstant.BaseEmployee},{CommonConstant.BranchDirector},{CommonConstant.BranchManager},{CommonConstant.BranchEmployee}")]
        [HttpPost("~/api/create-stock-check-sheet")]
        public async Task<IActionResult> CreateStockCheckSheet([FromBody] CreateStockCheckSheetRequestDto entity)
        {
            var result = await _stockCheckSheetService.AddStockCheckSheet(entity, CurrentUser).ConfigureAwait(false);
            if (result)
            {
                return Ok(result);
            }
            return BadRequest(result);
        }
    }
}
