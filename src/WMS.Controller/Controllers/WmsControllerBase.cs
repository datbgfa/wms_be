﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using WMS.Business.Dtos.GlobalResponseDto;

namespace WMS.Controller.Controllers
{
    public class WmsControllerBase : ControllerBase
    {
        protected string CurrentUser
        {
            get
            {
                const string nameKey = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name";
                var currentUser = HttpContext.User.Claims
                    .FirstOrDefault(x => x.Type.Equals(nameKey))?.Value;
                return currentUser == null ? HttpStatusCode.Unauthorized.ToString() : currentUser.ToString();
            }
        }

        protected string CurrentEmail
        {
            get
            {
                const string emailKey = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress";
                var currentUser = HttpContext.User.Claims
                    .FirstOrDefault(x => x.Type.Equals(emailKey))?.Value;
                return currentUser == null ? HttpStatusCode.Unauthorized.ToString() : currentUser.ToString();
            }
        }

        protected string? JWTToken
        {
            get
            {
                string? bearer = HttpContext.Request.Headers.Authorization;
                string? token = bearer?.Replace("Bearer", "").Trim();
                return token;
            }
        }

        protected int JWTExp
        {
            get
            {
                var expTime = HttpContext.User.Claims
                    .FirstOrDefault(x => x.Type.Equals(JwtRegisteredClaimNames.Exp))?.Value;
                long expTimeTicks = 0;
                if (expTime is not null)
                {
                    expTimeTicks = long.Parse(expTime);
                }

                var tokenDate = DateTimeOffset.FromUnixTimeSeconds(expTimeTicks).UtcDateTime;

                var now = DateTime.Now.ToUniversalTime();

                return (tokenDate - now).Seconds;
            }
        }

        public override BadRequestObjectResult BadRequest([ActionResultObjectValue] object? error)
        {
            if (error is null)
            {
                error = HttpStatusCode.BadRequest.ToString();
            }
            return base.BadRequest(new ApiFormatResponse(StatusCodes.Status400BadRequest, true, error));
        }

        protected BadRequestObjectResult BadRequest()
        {
            return BadRequest(HttpStatusCode.BadRequest.ToString());
        }

        public override NotFoundObjectResult NotFound([ActionResultObjectValue] object? value)
        {
            return base.NotFound(new ApiFormatResponse(StatusCodes.Status404NotFound, true, value));
        }

        public override OkObjectResult Ok([ActionResultObjectValue] object? value)
        {
            return base.Ok(new ApiFormatResponse(StatusCodes.Status200OK, true, value));
        }

        public override UnauthorizedObjectResult Unauthorized([ActionResultObjectValue] object? value)
        {
            if (value is null)
            {
                value = HttpStatusCode.Unauthorized.ToString();
            }
            throw new UnauthorizedAccessException(value?.ToString());
        }

        public override UnauthorizedResult Unauthorized()
        {
            throw new UnauthorizedAccessException();
        }

        public override ConflictObjectResult Conflict([ActionResultObjectValue] object? error)
        {
            return base.Conflict(new ApiFormatResponse(StatusCodes.Status409Conflict, true, error));
        }
    }
}
