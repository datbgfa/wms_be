﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WMS.Business.Abstractions.Service;
using WMS.Business.Dtos.CommonDto;
using WMS.Business.Dtos.SupplierDto;
using WMS.Domain.Constant;

namespace WMS.Controller.Controllers
{
    [ApiController]
    public class SupplierController : WmsControllerBase
    {
        private readonly ILogger<SupplierController> _logger;
        private readonly ISupplierService _supplierService;

        public SupplierController(ILogger<SupplierController> logger, ISupplierService supplierService)
        {
            _logger = logger;
            _supplierService = supplierService;
        }

        [Authorize(Roles = $"{CommonConstant.BaseManager},{CommonConstant.BaseEmployee}")]
        [HttpGet("~/api/view-suppliers-list")]
        public async Task<ActionResult<ViewPaging<SupplierResponseDto>>> GetSuppliers([FromQuery] SupplierRequestDto entity)
        {
            var data = await _supplierService.GetSuppliersList(entity).ConfigureAwait(false);
            if (data == null) return NotFound("Supplier  does not exist?");
            return Ok(data);
        }

        [Authorize(Roles = $"{CommonConstant.BaseManager},{CommonConstant.BaseEmployee}")]
        [HttpGet("~/api/view-all-suppliers-list")]
        public async Task<ActionResult<ViewPaging<SupplierResponseDto>>> GetAllSuppliers()
        {
            var data = await _supplierService.GetAllSuppliers().ConfigureAwait(false);
            if (data == null) return NotFound("Supplier  does not exist?");
            return Ok(data);
        }

        [Authorize(Roles = $"{CommonConstant.BaseManager},{CommonConstant.BaseEmployee}")]
        [HttpGet("~/api/get-supplier-details")]
        public async Task<ActionResult<SupplierResponseDto>> GetSupplierDetails([FromQuery] int supplierId)
        {
            var data = await _supplierService.GetBySupplierId(supplierId).ConfigureAwait(false);

            if (data is null)
            {
                return NotFound($"Supplier with id {supplierId} does not exist?");
            }

            return Ok(data);
        }

        [Authorize(Roles = CommonConstant.BaseManager)]
        [HttpPost("~/api/create-supplier")]
        public async Task<ActionResult> CreateSupplier([FromForm] AddSupplierRequestDto entity, [FromForm] IFormFile? supplierLogo)
        {
            var result = await _supplierService.AddSupplier(entity, supplierLogo).ConfigureAwait(false);

            if (!result)
            {
                return BadRequest("Create supplier failed!!!");
            }

            return Ok(result);
        }

        [Authorize(Roles = CommonConstant.BaseManager)]
        [HttpPut("~/api/update-supplier")]
        public async Task<ActionResult> UpdateSupplier([FromForm] AddSupplierRequestDto entity, [FromForm] IFormFile? supplierLogo)
        {
            var result = await _supplierService.UpdateSupplier(entity, supplierLogo).ConfigureAwait(false);

            if (!result)
            {
                return BadRequest("Update supplier failed!!!");
            }

            return Ok(result);
        }

        [Authorize(Roles = CommonConstant.BaseManager)]
        [HttpPut("~/api/update-supplier-status")]
        public async Task<ActionResult> UpdateSupplierStatus([FromBody] UpdateSupplierStatusRequestDto entity)
        {
            var result = await _supplierService.UpdateStatus(entity).ConfigureAwait(false);

            if (!result)
            {
                return BadRequest("Update supplier failed!!!");
            }

            return Ok(result);
        }

        [Authorize(Roles = CommonConstant.BaseManager)]
        [HttpGet("~/api/get-history-import")]
        public async Task<ActionResult> GetHistoryImport([FromQuery] int supplierId)
        {
            var result = await _supplierService.GetHistoryImport(supplierId).ConfigureAwait(false);

            return Ok(result);
        }

        [Authorize(Roles = CommonConstant.BaseManager)]
        [HttpGet("~/api/get-debts")]
        public async Task<ActionResult> GetDebts([FromQuery] int supplierId)
        {
            var result = await _supplierService.GetDebts(supplierId).ConfigureAwait(false);

            return Ok(result);
        }

        [Authorize(Roles = CommonConstant.BaseManager)]
        [HttpGet("~/api/getall-have-debts")]
        public async Task<ActionResult<List<SupplierHaveDebtResponseDto>>> GetAllSupplierHaveDebt()
        {
            var result = await _supplierService.GetSupplierHaveDebt()
                .ConfigureAwait(false);

            return Ok(result);
        }

        [Authorize(Roles = $"{CommonConstant.BaseManager},{CommonConstant.BaseEmployee}")]
        [HttpGet("~/api/view-all-active-suppliers")]
        public async Task<ActionResult<ViewPaging<SupplierResponseDto>>> GetAllActiveSuppliers([FromQuery] string? searchWord)
        {
            var data = await _supplierService.GetAllActiveSuppliers(searchWord).ConfigureAwait(false);
            if (data == null) return NotFound("Supplier  does not exist?");
            return Ok(data);
        }
    }
}
