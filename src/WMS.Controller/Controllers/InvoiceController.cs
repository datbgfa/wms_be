﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WMS.Business.Abstractions.Service;
using WMS.Business.Dtos.CommonDto;
using WMS.Business.Dtos.InvoiceDto;
using WMS.Domain.Constant;

namespace WMS.Controller.Controllers
{
    [ApiController]
    public class InvoiceController : WmsControllerBase
    {
        private readonly ILogger<InvoiceController> _logger;
        private readonly IPaymentInvoiceService _paymentinvoiceService;
        private readonly IInvoiceService _invoiceService;

        public InvoiceController(ILogger<InvoiceController> logger, IPaymentInvoiceService paymentinvoiceService, IInvoiceService invoiceService)
        {
            _logger = logger;
            _paymentinvoiceService = paymentinvoiceService;
            _invoiceService = invoiceService;
        }

        [Authorize(Roles = CommonConstant.BaseManager)]
        [HttpPost("~/api/create-payment-invoice")]
        public async Task<IActionResult> CreatePaymentInvoice([FromBody] CreatePaymentInvoiceRequestDto userRq)
        {
            var isSuccess = await _paymentinvoiceService.CreatePaymentInvoice(userRq, CurrentUser)
                .ConfigureAwait(false);

            if (!isSuccess)
                return BadRequest("Create payment invoice failed!");

            return Ok("Create payment invoice success!");
        }

        [Authorize(Roles = CommonConstant.BaseManager)]
        [HttpPost("~/api/create-receipt-invoice")]
        public async Task<IActionResult> CreateReceiptInvoice([FromBody] CreateReceiptInvoiceRequestDto userRq)
        {
            var isSuccess = await _paymentinvoiceService.CreateReceiptInvoice(userRq, CurrentUser)
                .ConfigureAwait(false);

            if (!isSuccess)
                return BadRequest("Create receipt invoice failed!");

            return Ok("Create receipt invoice success!");
        }

        [Authorize(Roles = CommonConstant.BaseManager)]
        [HttpGet("~/api/get-base-invoices")]
        public async Task<ActionResult<ViewPaging<InvoiceResponseDto>>> GetBaseInvoices([FromQuery] InvoiceDataRequestDto userRq)
        {
            try
            {
                var result = await _invoiceService.GetBaseInvoice(userRq)
                    .ConfigureAwait(false);

                return Ok(result);
            }
            catch (Exception e)
            {
                if (e is ArgumentException)
                {
                    return BadRequest(e.Message);
                }

                throw;
            }
        }

        [Authorize(Roles = CommonConstant.BranchDirector)]
        [HttpGet("~/api/get-branch-invoices")]
        public async Task<ActionResult<IEnumerable<InvoiceDataResponseDto>>> GetBranchInvoices([FromQuery] InvoiceDataRequestDto userRq)
        {
            try
            {
                var result = await _invoiceService.GetBranchInvoice(userRq, CurrentUser)
                    .ConfigureAwait(false);

                return Ok(result);
            }
            catch (Exception e)
            {
                if (e is ArgumentException)
                {
                    return BadRequest(e.Message);
                }

                throw;
            }
        }

        [Authorize(Roles = $"{CommonConstant.BaseManager},{CommonConstant.BranchDirector}")]
        [HttpGet("~/api/view-dashboard")]
        public async Task<ActionResult<DashboardResponseDto>> ViewDashboard([FromQuery] DateTime? fromRq, DateTime? toRq)
        {
            var result = await _invoiceService.ViewDashboard(fromRq, toRq, CurrentUser).ConfigureAwait(false);
            return Ok(result);
        }
    }
}
