﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WMS.Business.Abstractions.Service;
using WMS.Business.Dtos.CommonDto;
using WMS.Business.Dtos.ProductDto;
using WMS.Business.Dtos.ProductReturnFromBranchDto;
using WMS.Domain.Constant;

namespace WMS.Controller.Controllers
{
    [ApiController]
    public class ProductController : WmsControllerBase
    {
        private readonly ILogger<ProductController> _logger;
        private readonly IProductService _productService;
        private readonly IInvoiceService _invoiceService;

        public ProductController(ILogger<ProductController> logger, IProductService productService, IInvoiceService invoiceService)
        {
            _logger = logger;
            _productService = productService;
            _invoiceService = invoiceService;
        }

        [Authorize(Roles = $"{CommonConstant.BaseManager},{CommonConstant.BaseEmployee},{CommonConstant.BranchDirector},{CommonConstant.BranchManager},{CommonConstant.BranchEmployee}")]
        [HttpGet("~/api/get-products")]
        public async Task<ActionResult<ViewPaging<GetProductListResponseDto>>> GetProductList([FromQuery] GetProductListRequestDto requestDto)
        {
            var data = await _productService.GetProductList(requestDto);
            return Ok(data);
        }

        [Authorize(Roles = $"{CommonConstant.BaseManager},{CommonConstant.BaseEmployee},{CommonConstant.BranchDirector},{CommonConstant.BranchManager},{CommonConstant.BranchEmployee}")]
        [HttpGet("~/api/get-product-by-name")]
        public async Task<ActionResult> GetProductByName([FromQuery] string? name)
        {
            if (string.IsNullOrWhiteSpace(name)) name = string.Empty;
            var data = await _productService.SearchProductByName(name)
                .ConfigureAwait(false);

            return Ok(data);
        }

        [Authorize(Roles = $"{CommonConstant.BaseManager},{CommonConstant.BaseEmployee},{CommonConstant.BranchDirector},{CommonConstant.BranchManager},{CommonConstant.BranchEmployee}")]
        [HttpGet("~/api/get-product-detail/{productId}")]
        public async Task<ActionResult<GetProductDetailResponseDto>> GetProductDetail([FromRoute] Guid productId)
        {
            var data = await _productService.GetProductDetail(productId);

            if (data is null)
            {
                return NotFound($"Product with id {productId} does not exist?");
            }

            return Ok(data);
        }

        [Authorize(Roles = CommonConstant.BaseManager)]
        [HttpPost("~/api/create-product")]
        public async Task<ActionResult<string>> CreateProduct([FromForm] CreateProductRequestDto newProduct, [FromForm] IFormFile imageProduct)
        {
            var result = await _productService.CreateProduct(newProduct, imageProduct);

            if (!result)
            {
                return BadRequest("Create product failed!!!");
            }

            return Ok("Create product susccessful!!!");
        }

        [Authorize(Roles = $"{CommonConstant.BaseManager},{CommonConstant.BaseEmployee}")]
        [HttpGet("~/api/get-products-return-supplier")]
        public async Task<ActionResult<ViewPaging<ProductReturnSupplierResponseDto>>> GetProductReturnToSupplier([FromQuery] PagingRequest pagingRequest)
        {
            var data = await _productService.GetProductReturnToSupplier(pagingRequest)
                .ConfigureAwait(false);

            return Ok(data);
        }

        [Authorize(Roles = CommonConstant.BaseManager)]
        [HttpPut("~/api/update-product")]
        public async Task<IActionResult> UpdateProduct([FromForm] UpdateProductDetailRequestDto userRq)
        {
            var result = await _productService.UpdateProductDetail(userRq)
                .ConfigureAwait(false);

            if (!result)
            {
                return BadRequest("Update failed!");
            }

            return Ok("Update successfully!");
        }

        [Authorize(Roles = $"{CommonConstant.BaseManager},{CommonConstant.BaseEmployee}")]
        [HttpPost("~/api/create-product-return-to-supplier")]
        public async Task<IActionResult> CreateBaseProductReturn([FromBody] CreateProducReturnSupplierRequestDto userRq)
        {
            var result = await _productService.CreateBaseReturn(userRq, CurrentUser).ConfigureAwait(false);
            if (result) return Ok("Create return successfully");
            return BadRequest("Something went wrong");
        }

        [Authorize(Roles = $"{CommonConstant.BaseManager},{CommonConstant.BaseEmployee},{CommonConstant.BranchDirector},{CommonConstant.BranchManager},{CommonConstant.BranchEmployee}")]
        [HttpGet("~/api/view-stock-quantity")]
        public async Task<IActionResult> GetAllStockQuantity([FromQuery] GetStockQuantityRequestDto paging)
        {
            var data = await _productService.GetAllStockQuantity(paging, CurrentUser).ConfigureAwait(false);
            if (data is null)
            {
                return BadRequest();
            }
            return Ok(data);
        }

        [Authorize(Roles = $"{CommonConstant.BaseManager},{CommonConstant.BaseEmployee},{CommonConstant.BranchDirector},{CommonConstant.BranchManager},{CommonConstant.BranchEmployee}")]
        [HttpGet("~/api/get-stock-quantity-by-productId")]
        public async Task<IActionResult> GetAllStockQuantityByProductId([FromQuery] GetStockQuantityByIdRequestDto entity)
        {
            var data = await _productService.GetAllStockQuantityProductId(entity, CurrentUser).ConfigureAwait(false);
            return Ok(data);
        }

        [Authorize(Roles = $"{CommonConstant.BaseManager},{CommonConstant.BaseEmployee}")]
        [HttpGet("~/api/get-product-return-from-branch")]
        public async Task<IActionResult> GetProductReturnFromBranch([FromQuery] GetProductReturnFromBranchRequestDto requestDto)
        {
            var result = await _productService.GetProductReturnFromBranch(requestDto)
                .ConfigureAwait(false);

            return Ok(result);
        }

        [Authorize(Roles = $"{CommonConstant.BaseManager},{CommonConstant.BaseEmployee}")]
        [HttpPut("~/api/update-product-return-status-from-branch")]
        public async Task<IActionResult> UpdateProductReturnStatusFromBranch([FromBody] UpdateStatusProductReturnFromBranchRequestDto userRq)
        {
            var isSuccess = await _productService.UpdateBranchReturn(userRq)
                .ConfigureAwait(false);

            if (!isSuccess)
            {
                return BadRequest("Update status failed!");
            }

            return Ok("Update status successfully");
        }

        [Authorize(Roles = CommonConstant.BranchDirector)]
        [HttpGet("~/api/get-branch-product-in-stock-by-name")]
        public async Task<ActionResult<IEnumerable<GetBranchProductInStockByNameResponseDto>>> GetBranchProductInStockByName([FromQuery] string? searchWord)
        {
            if (string.IsNullOrWhiteSpace(searchWord)) searchWord = string.Empty;
            var result = await _productService.GetBranchProductInStockByName(searchWord, CurrentUser).ConfigureAwait(false);
            if (result == null) return BadRequest();
            else return Ok(result);
        }

        [Authorize(Roles = $"{CommonConstant.BranchDirector},{CommonConstant.BranchManager},{CommonConstant.BranchEmployee}")]
        [HttpPost("~/api/create-product-return-to-base")]
        public async Task<IActionResult> CreateBranchProductReturn([FromBody] CreateProductReturnFromBranchToBaseRequestDto userRq)
        {
            var isSuccess = await _productService.CreateBranchReturn(userRq)
                .ConfigureAwait(false);

            if (!isSuccess)
            {
                return BadRequest("Create failed!!!");
            }

            return Ok("Create successfully!!!");
        }

        [Authorize(Roles = $"{CommonConstant.BranchDirector},{CommonConstant.BranchManager},{CommonConstant.BranchEmployee}")]
        [HttpGet("~/api/get-branch-warehouse-products")]
        public async Task<IActionResult> GetBranchWarehouseProducts([FromQuery] GetProductListRequestDto requestDto)
        {
            var rs = await _productService.GetBranchWarehouseProducts(CurrentUser, requestDto).ConfigureAwait(false);
            if (rs == null) return BadRequest();
            return Ok(rs);
        }

        [Authorize(Roles = $"{CommonConstant.BranchDirector},{CommonConstant.BranchManager},{CommonConstant.BranchEmployee}")]
        [HttpGet("~/api/get-branch-warehouse-product-details/{productId}")]
        public async Task<IActionResult> GetBranchWarehouseProductDetails([FromRoute] Guid productId)
        {
            var rs = await _productService.GetBranchWarehouseProductDetails(CurrentUser, productId).ConfigureAwait(false);
            if (rs == null) return BadRequest();
            return Ok(rs);
        }

        [Authorize(Roles = CommonConstant.BranchDirector)]
        [HttpPut("~/api/update-branch-warehouse-product")]
        public async Task<IActionResult> UpdateBranchWarehouseProduct([FromBody] UpdateBranchWarehouseProductRequestDto entity)
        {
            var rs = await _productService.UpdateBranchWarehouseProduct(CurrentUser, entity.ProductId, entity.ExportPrice)
                .ConfigureAwait(false);
            if (!rs) return BadRequest();
            return Ok("update successfull");
        }
    }
}
