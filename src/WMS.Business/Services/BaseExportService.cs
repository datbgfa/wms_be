﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using WMS.Business.Abstractions.Service;
using WMS.Business.Dtos.BaseExportDto;
using WMS.Business.Dtos.CommonDto;
using WMS.Business.Dtos.ProductDto;
using WMS.Business.Dtos.S3Dto;
using WMS.Business.Dtos.WarehouseDto;
using WMS.Business.Utils.ExtensionMethods;
using WMS.Business.Utils.StringUtils;
using WMS.Domain.Abstractions;
using WMS.Domain.Constant;
using WMS.Domain.Entities;
using WMS.Domain.Enums;

namespace WMS.Business.Services
{
    public class BaseExportService : IBaseExportService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<BaseExportService> _logger;
        private readonly IMapper _mapper;
        private readonly IDateService _dateService;
        private readonly IS3StorageService _s3StorageService;
        private readonly IProductService _productService;
        private readonly IInvoiceService _invoiceService;

        public BaseExportService(IUnitOfWork unitOfWork,
            ILogger<BaseExportService> logger,
            IMapper mapper,
            IDateService dateService,
            IS3StorageService s3StorageService,
            IProductService productService,
            IInvoiceService invoiceService)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
            _mapper = mapper;
            _dateService = dateService;
            _s3StorageService = s3StorageService;
            _productService = productService;
            _invoiceService = invoiceService;
        }

        public async Task<GetBaseExportResponseDetailsDto> GetExportsDetails(Guid exportId)
        {
            try
            {
                var baseExport = await _unitOfWork.BaseExportRepository.GetBaseExportDetailsById(exportId).ConfigureAwait(false);

                if (baseExport == null) return null;

                var result = _mapper.Map<GetBaseExportResponseDetailsDto>(baseExport);
                //merge product duplicate
                result.WmsBaseExportProducts = result.WmsBaseExportProducts.GroupBy(bep => bep.ProductId)
                                                                           .Select(bep => new BaseExportProductResponseDto(
                                                                            bep.Key,
                                                                            bep.First().ProductName,
                                                                            bep.First().ProductImage,
                                                                            bep.First().UnitPrice,
                                                                            bep.First().CostPrice,
                                                                            bep.Sum(k => k.TotalPrice),
                                                                            bep.Sum(k => k.Quantity)))
                                                                           .ToList();
                //get image
                foreach (var baseExportProduct in result.WmsBaseExportProducts)
                {
                    //GetBaseExportProductByExportIdProductIdAndListBaseImportId 
                    var exportProductDetails = await _unitOfWork.BaseExportProductRepository
                        .GetBaseExportProductByExportIdProductIdAndListBaseImportId(exportId,
                                                                                    baseExportProduct.ProductId,
                                                                                    baseExport.WmsBaseExportProducts.Select(bep => bep.BaseImportId))
                                                                                    .ConfigureAwait(false);
                    var exportProductDetailsMap = _mapper.Map<IEnumerable<BaseExportProductDetailsResponseDto>>(exportProductDetails);
                    baseExportProduct.ExportProductDetails = exportProductDetailsMap;

                    var productImageUrl = _s3StorageService.GetFileUrl(new S3RequestData()
                    {
                        Name = baseExportProduct.ProductImage,
                    });
                    baseExportProduct.ProductImage = productImageUrl;
                }
                //get debt by branchWarehouseId
                var debt = await _unitOfWork.BranchDebtRepository.FindObject(bd => bd.DebtorId.Equals(baseExport.BranchWarehouseId)).ConfigureAwait(false);
                decimal debtAmount = 0;
                if (debt != null) debtAmount = debt.DebtAmount;
                //get branchImport by branchWarehouseId
                var branchImports = await _unitOfWork.BranchImportRepository.FindMultiple(bi => bi.WarehouseId.Equals(baseExport.BranchWarehouseId)).ConfigureAwait(false);
                var listImportId = branchImports.Select(bi => bi.ImportId);
                //get branchProductReturnDetails by list ImportId
                var branchProductReturnDetails = await _unitOfWork.BranchProductReturnDetailRepository.FindMultiple(bdrd => listImportId.Contains(bdrd.ImportId)).ConfigureAwait(false);
                var listProductReturnId = branchProductReturnDetails.Select(bprd => bprd.ProductReturnId);
                var totalReturnProduct = listProductReturnId.Distinct().Count();
                result.DebtBranchResponseDto = new DebtBranchResponseDto()
                {
                    DebtAmount = debtAmount,
                    TotalImport = branchImports.Count(),
                    TotalReturnProduct = totalReturnProduct
                };
                //get list product return off branch import by base export id
                var import = await _unitOfWork.BranchImportRepository.FindObject(bi => bi.BaseExportId.Equals(exportId)).ConfigureAwait(false);
                if (import != null)
                {
                    result.ProductReturnFromBranch = await _productService.GetProductReturnFromBranchByImportId(import.ImportId).ConfigureAwait(false);
                }
                else
                {
                    result.ProductReturnFromBranch = new List<GetProductsReturnFromBranchResponseDto>();
                }
                //get invoice
                var invoices = await _invoiceService.GetBaseInvoiceByExportId(exportId).ConfigureAwait(false);

                //var branchProductReturn = await _unitOfWork.BranchProductReturnRepository
                //    .FindMultiple(bpr => bpr.BaseExportId.Equals(exportId))
                //    .ConfigureAwait(false);
                //var invoiceReturn = await _invoiceService.GetBaseInvoiceByBranchProductReturnId(branchProductReturn.Select(bpr => bpr.ProductReturnId)).ConfigureAwait(false);
                result.Invoices = invoices.OrderByDescending(i => i.UpdateDate);


                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return null;
            }
        }

        public async Task<ViewPaging<GetBaseExportResponseDto>> GetExportsList(GetBaseExportRequestDto requestDto)
        {
            try
            {
                var dateFilter = _dateService.GetDateTimeByDateFilter(requestDto.DateFilter);
                //Convert enum date -> dateTime
                var fullList = await _unitOfWork.BaseExportRepository.FilterSearchBaseExport(requestDto.SearchWord,
                                                                                       requestDto.ExportStatus,
                                                                                       requestDto.PaymentStatus,
                                                                                       requestDto.BranchWarehouseId,
                                                                                       dateFilter.StartDate,
                                                                                       dateFilter.EndDate)
                                                                                       .ConfigureAwait(false);
                var filteredList = fullList
                    .Skip(requestDto.PagingRequest.PageSize * (requestDto.PagingRequest.CurrentPage - 1))
                    .Take(requestDto.PagingRequest.PageSize);

                var pagination = new Pagination(fullList.Count(), requestDto.PagingRequest.CurrentPage,
                    requestDto.PagingRequest.PageRange, requestDto.PagingRequest.PageSize);

                var result = _mapper.Map<IEnumerable<GetBaseExportResponseDto>>(filteredList);

                foreach (var baseExport in result)
                {
                    //merge product duplicate
                    baseExport.WmsBaseExportProducts = baseExport.WmsBaseExportProducts.GroupBy(bep => bep.ProductId)
                                                                          .Select(bep => new BaseExportProductResponseDto(
                                                                           bep.Key,
                                                                           bep.First().ProductName,
                                                                           bep.First().ProductImage,
                                                                           bep.First().UnitPrice,
                                                                           bep.First().CostPrice,
                                                                           bep.Sum(k => k.TotalPrice),
                                                                           bep.Sum(k => k.Quantity)))
                                                                          .ToList();
                    //find image link
                    foreach (var baseExportProduct in baseExport.WmsBaseExportProducts)
                    {
                        var productImageUrl = _s3StorageService.GetFileUrl(new S3RequestData()
                        {
                            Name = baseExportProduct.ProductImage,
                        });
                        baseExportProduct.ProductImage = productImageUrl;
                    }
                }

                return new ViewPaging<GetBaseExportResponseDto>(result, pagination);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return null;
            }
        }

        public async Task<bool> PayForBaseExport(PayForBaseExportRequestDto entity, string currentUser)
        {
            //find user
            var manager = await _unitOfWork.UserRepository.FindObject(x => x.Username.Equals(currentUser)
                && x.Status == (short)UserStatusEnum.Active).ConfigureAwait(false);
            if (manager == null) return false;

            //find export
            var baseExport = await _unitOfWork.BaseExportRepository.FindObject(x => x.ExportId.Equals(entity.ExportId)).ConfigureAwait(false);
            if (baseExport == null || baseExport.PaymentStatus == PaymentStatusConstant.Paid
                || entity.PayAmount <= 0 || baseExport.ExportStatus != ExportStatusConstant.Exported) return false;

            //find debt
            var debt = await _unitOfWork.BranchDebtRepository.FindObject(x => x.DebtorId.Equals(baseExport.BranchWarehouseId)).ConfigureAwait(false);
            if (debt == null) return false;

            //find branchImport
            var branchImport = await _unitOfWork.BranchImportRepository.FindObject(x => x.BaseExportId.Equals(baseExport.ExportId)).ConfigureAwait(false);
            if (branchImport == null) return false;

            //update export
            var paid = baseExport.PaidAmount + entity.PayAmount;
            if (paid > baseExport.TotalCost) return false;

            baseExport.PaidAmount += entity.PayAmount;
            if (baseExport.PaidAmount == baseExport.TotalCost) baseExport.PaymentStatus = PaymentStatusConstant.Paid;
            else baseExport.PaymentStatus = PaymentStatusConstant.PartlyPaid;

            using var transaction = _unitOfWork.BeginTransaction();
            try
            {
                //update export
                baseExport.UpdateDate = DateTime.Now;
                baseExport.UpdateUser = manager.Username;
                _unitOfWork.BaseExportRepository.Update(baseExport);

                //update import of branch              
                branchImport.PaidAmount += entity.PayAmount;
                if (branchImport.PaidAmount == branchImport.TotalCost) branchImport.PaymentStatus = PaymentStatusConstant.Paid;
                else branchImport.PaymentStatus = PaymentStatusConstant.PartlyPaid;

                branchImport.UpdateDate = DateTime.Now;
                branchImport.UpdateUser = manager.Username;
                _unitOfWork.BranchImportRepository.Update(branchImport);

                //update debt
                debt.DebtAmount -= entity.PayAmount;
                _unitOfWork.BranchDebtRepository.Update(debt);

                //add new invoice
                var branchWarehouse = await _unitOfWork.BranchWarehouseRepository.FindObject(x => x.WarehouseId.Equals(baseExport.BranchWarehouseId))
                    .ConfigureAwait(false);
                var branchInvoice = new WmsBranchInvoice()
                {
                    DebtId = debt.DebtId,
                    CreateDate = DateTime.Now,
                    CreateUser = manager.Username,
                    Type = InvoiceTypeConstant.PaymentVoucher,
                    Amount = entity.PayAmount,
                    Description = $"Kho con {branchWarehouse.WarehouseName} trả {entity.PayAmount} VND cho đơn nhập hàng từ kho tổng",
                    ImportId = branchImport.ImportId,
                    UpdateDate = DateTime.Now,
                    UpdateUser = manager.Username,
                    BalanceChange = debt.DebtAmount,
                    InvoiceCode = StringHelper.GenerateSpecifyId(50, "BRINV"),
                };
                await _unitOfWork.BranchInvoiceRepository.AddAsync(branchInvoice).ConfigureAwait(false);

                var baseInvoice = new WmsBaseInvoice()
                {
                    BranchDebtId = debt.DebtId,
                    CreateDate = DateTime.Now,
                    CreateUser = manager.Username,
                    Type = InvoiceTypeConstant.ReceiptVoucher,
                    Amount = entity.PayAmount,
                    Description = $"Nhận {entity.PayAmount} VND cho đơn xuất hàng tới kho con {branchWarehouse.WarehouseName}",
                    UpdateDate = DateTime.Now,
                    UpdateUser = manager.Username,
                    ExportId = baseExport.ExportId,
                    BalanceChange = debt.DebtAmount,
                    InvoiceCode = StringHelper.GenerateSpecifyId(50, "BAINV"),
                };
                await _unitOfWork.BaseInvoiceRepository.AddAsync(baseInvoice).ConfigureAwait(false);

                await _unitOfWork.CommitAsync().ConfigureAwait(false);
                transaction.Commit();
                _logger.LogInformation($"Pay import successfully!!!");
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                transaction.Rollback();
                return false;
            }
            finally
            {
                if (transaction.Connection != null) transaction.Connection.Close();
            }
        }

        public async Task<bool> UpdateBaseExport(UpdateBaseExportRequestDto entity, string currentUser)
        {
            entity.ExportDate = DateTimeExtension.ConvertUTCToLocalTimeZone(entity.ExportDate);
            //check all conditions
            if (entity.ProductsList.Count() == 0 || entity.ExportDate.Date < DateTime.Now.Date) return false;
            if (entity.ExportStatus != ExportStatusConstant.Requested && entity.ExportStatus != ExportStatusConstant.Confirmed
                && entity.ExportStatus != ExportStatusConstant.Refused) return false;
            var manager = await _unitOfWork.UserRepository.FindObject(x => x.Username.Equals(currentUser)
                && x.Status == (short)UserStatusEnum.Active).ConfigureAwait(false);
            if (manager == null) return false;

            var exportBy = await _unitOfWork.UserRepository.FindObject(x => x.UserId.Equals(entity.ExportBy)
                && x.Status == (short)UserStatusEnum.Active).ConfigureAwait(false);
            if (entity.ExportStatus != ExportStatusConstant.Refused && exportBy == null) return false;

            var warehouse = (await _unitOfWork.BaseWarehouseRepository.All().ConfigureAwait(false)).FirstOrDefault();
            if (warehouse == null) return false;

            var export = await _unitOfWork.BaseExportRepository.FindObject(x => x.ExportId.Equals(entity.ExportId)).ConfigureAwait(false);
            if (export == null || export.ExportStatus != ExportStatusConstant.Requested) return false;

            var branchRequest = await _unitOfWork.BranchRequestRepository.FindObject(x => x.RequestId.Equals(export.RequestId)).ConfigureAwait(false);
            if (branchRequest == null) return false;

            using var transaction = _unitOfWork.BeginTransaction();
            try
            {
                if (entity.ExportStatus == ExportStatusConstant.Refused)
                {
                    export.ExportStatus = ExportStatusConstant.Refused;
                    _unitOfWork.BaseExportRepository.Update(export);

                    branchRequest.ApprovalStatus = BranchRequestApprovalStatusConstant.RefusedByBaseWarehouse;
                    _unitOfWork.BranchRequestRepository.Update(branchRequest);

                    await _unitOfWork.CommitAsync().ConfigureAwait(false);
                    transaction.Commit();
                    return true;
                }
                //update export
                export.AssignBy = manager.UserId;
                export.ExportBy = entity.ExportBy;
                export.ExportDate = entity.ExportDate;
                export.ExtraCost = entity.ExtraCost;
                export.ExportStatus = entity.ExportStatus;
                export.UpdateDate = DateTime.Now;
                export.UpdateUser = manager.Username;

                //calculate price of each product and total price
                decimal totalPrice = 0;
                int totalNumberOfItems = 0;
                foreach (var product in entity.ProductsList)
                {
                    var existProduct = await _unitOfWork.ProductRepository.GetById(product.ProductId).ConfigureAwait(false);
                    if (existProduct == null || existProduct.Visible == false) return false;
                    totalPrice += existProduct.ExportPrice * product.Quantity;
                    totalNumberOfItems += product.Quantity;
                }
                decimal extraFeeForEachItem = export.ExtraCost / totalNumberOfItems;

                export.TotalPrice = totalPrice;
                export.TotalCost = totalPrice + entity.ExtraCost;
                _unitOfWork.BaseExportRepository.Update(export);

                //delete old base export products
                var oldExportProducts = await _unitOfWork.BaseExportProductRepository.FindMultiple(x => x.ExportId.Equals(export.ExportId))
                    .ConfigureAwait(false);
                foreach (var product in oldExportProducts)
                {
                    _unitOfWork.BaseExportProductRepository.Delete(product);
                }
                await _unitOfWork.CommitAsync().ConfigureAwait(false);

                int index = 0;
                //add base export products
                foreach (var product in entity.ProductsList)
                {
                    //check stock
                    var stock = await _unitOfWork.BaseImportProductRepository.FindMultiple(x => x.ProductId.Equals(product.ProductId)
                    && x.StockQuantity > 0).ConfigureAwait(false);
                    if (product.Quantity > stock.Sum(x => x.StockQuantity)) return false;

                    var existProduct = await _unitOfWork.ProductRepository.GetById(product.ProductId).ConfigureAwait(false);
                    var importProducts = await _unitOfWork.BaseImportProductRepository.GetBaseStockOrderByImportDate(product.ProductId)
                        .ToListAsync().ConfigureAwait(false);

                    //FIRST IN FIRST OUT
                    var requestQuantity = product.Quantity;
                    foreach (var importProduct in importProducts)
                    {
                        //if quantity has been decrease to 0, end the loop
                        if (requestQuantity <= 0) break;

                        //create a temporary variable to change the export quantity
                        int exportQuantity = 0;

                        //create a new base export product
                        var productExport = new WmsBaseExportProduct()
                        {
                            ProductId = product.ProductId,
                            ExportId = export.ExportId,
                            UnitPrice = existProduct.ExportPrice,
                            Quantity = 0,
                            BaseImportId = importProduct.ImportId,
                            ExportCodeItem = export.ExportCode + "_" + index.ToString(),
                        };
             
                        if (requestQuantity >= importProduct.StockQuantity) { exportQuantity = importProduct.StockQuantity; }
                        else exportQuantity = requestQuantity;
                        requestQuantity -= exportQuantity;
                  
                        productExport.Quantity = exportQuantity;
                        await _unitOfWork.BaseExportProductRepository.AddAsync(productExport).ConfigureAwait(false);
                        index++;

                        //decrease base stock
                        importProduct.StockQuantity -= exportQuantity;
                        _unitOfWork.BaseImportProductRepository.Update(importProduct);

                    }
                }
                await _unitOfWork.CommitAsync().ConfigureAwait(false);

                //if this export has been confirm, create a debt and a branch import
                if (entity.ExportStatus == ExportStatusConstant.Confirmed)
                {
                    //add debt
                    var debt = await _unitOfWork.BranchDebtRepository.FindObject(x => x.DebtorId.Equals(export.BranchWarehouseId)).ConfigureAwait(false);
                    if (debt == null)
                    {
                        debt = new WmsBranchDebt()
                        {
                            CreditorId = warehouse.WarehouseId,
                            DebtAmount = 0,
                            DebtorId = export.BranchWarehouseId,
                        };
                        await _unitOfWork.BranchDebtRepository.AddAsync(debt).ConfigureAwait(false);
                    }

                    //add import and products for branch warehouse
                    var assignBy = await _unitOfWork.UserRepository.FindObject(x => x.Username == branchRequest.UpdateUser).ConfigureAwait(false);
                    if (assignBy is null) return false;

                    var import = new WmsBranchImport()
                    {
                        ExportCode = export.ExportCode,
                        ImportCode = StringHelper.GenerateSpecifyId(50, "BRIM"),
                        WarehouseId = export.BranchWarehouseId,
                        RequestId = export.RequestId,
                        AssignBy = assignBy.UserId,
                        ImportBy = branchRequest.RequestBy,
                        BaseWarehouseId = warehouse.WarehouseId,
                        ImportedDate = null,
                        TotalPrice = totalPrice,
                        TotalCost = totalPrice + entity.ExtraCost,
                        ExtraCost = entity.ExtraCost,
                        PaidAmount = 0,
                        Note = string.Empty,
                        ImportStatus = ImportStatusConstant.NotImported,
                        PaymentStatus = PaymentStatusConstant.NotPaid,
                        BaseExportId = entity.ExportId,
                        CreateDate = DateTime.Now,
                        CreateUser = manager.Username,
                        UpdateDate = DateTime.Now,
                        UpdateUser = manager.Username,
                    };
                    await _unitOfWork.BranchImportRepository.AddAsync(import).ConfigureAwait(false);

                    var exportProducts = await _unitOfWork.BaseExportProductRepository.FindMultiple(x => x.ExportId.Equals(export.ExportId))
                        .ConfigureAwait(false);

                    foreach (var product in exportProducts)
                    {
                        var branchImportProduct = new WmsBranchImportProduct()
                        {
                            ImportId = import.ImportId,
                            ProductId = product.ProductId,
                            UnitPrice = product.UnitPrice,
                            ImportedQuantity = product.Quantity,
                            StockQuantity = 0,
                            BaseImportId = product.BaseImportId,
                            ExportCodeItem = product.ExportCodeItem,
                            CostPrice = product.UnitPrice + extraFeeForEachItem,
                        };
                        await _unitOfWork.BranchImportProductRepository.AddAsync(branchImportProduct).ConfigureAwait(false);
                    }
                }

                await _unitOfWork.CommitAsync().ConfigureAwait(false);
                transaction.Commit();
                _logger.LogInformation($"Update export successfully!!!");
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                transaction.Rollback();
                return false;
            }
            finally
            {
                if (transaction.Connection != null) transaction.Connection.Close();
            }
        }
    }
}
