﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using WMS.Business.Abstractions.Service;
using WMS.Business.Dtos.CommonDto;
using WMS.Business.Dtos.DebtDto;
using WMS.Business.Dtos.S3Dto;
using WMS.Business.Dtos.SupplierDto;
using WMS.Business.Utils.FileHelper;
using WMS.Domain.Abstractions;
using WMS.Domain.Entities;

namespace WMS.Business.Services
{
    public class SupplierService : ISupplierService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<SupplierService> _logger;
        private readonly IS3StorageService _s3StorageService;
        private readonly IMapper _mapper;

        public SupplierService(IUnitOfWork unitOfWork,
                                     IMapper mapper,
                                     IS3StorageService s3StorageService,
        ILogger<SupplierService> logger)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _s3StorageService = s3StorageService;
            _logger = logger;
        }

        public async Task<ViewPaging<SupplierResponseDto>> GetSuppliersList(SupplierRequestDto entity)
        {
            try
            {
                if (entity.PagingRequest.PageSize <= 0) entity.PagingRequest.PageSize = 10;
                if (entity.PagingRequest.CurrentPage <= 0) entity.PagingRequest.CurrentPage = 1;
                var searchByName = _unitOfWork.SupplierRepository
                    .SearchSuppliersList(entity.SearchWord).OrderByDescending(x => x.SupplierId);

                var pagingList = await searchByName
                .Skip(entity.PagingRequest.PageSize * (entity.PagingRequest.CurrentPage - 1))
                    .Take(entity.PagingRequest.PageSize).OrderByDescending(x => x.SupplierId)
                    .ToListAsync()
                    .ConfigureAwait(false);

                var pagination = new Pagination(searchByName.Count(), entity.PagingRequest.CurrentPage,
                    entity.PagingRequest.PageRange, entity.PagingRequest.PageSize);

                var result = _mapper.Map<IEnumerable<SupplierResponseDto>>(pagingList);

                foreach (var item in result)
                {
                    item.Logo = _s3StorageService.GetFileUrl(new S3RequestData() { Name = item.Logo });
                }

                return new ViewPaging<SupplierResponseDto>(result, pagination);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return null;
            }
        }

        public async Task<SupplierResponseDto?> GetBySupplierId(int id)
        {

            var data = await _unitOfWork.SupplierRepository
                .GetSupplierDetails(id)
                .ConfigureAwait(false);

            if (data is null)
            {
                return null;
            }

            return _mapper.Map<SupplierResponseDto>(data);
        }

        public async Task<bool> AddSupplier(AddSupplierRequestDto newSupplierDto, IFormFile logoSupplier)
        {

            const string DEFAULT_SUPPLIER = "Supplier/default_supplier.jpg";

            if (string.IsNullOrWhiteSpace(newSupplierDto.SupplierName) || string.IsNullOrWhiteSpace(newSupplierDto.Email) || string.IsNullOrWhiteSpace(newSupplierDto.PhoneNumber)) return false;
            using var transaction = _unitOfWork.BeginTransaction();
            try
            {
                var pathLogo = "";
                if (logoSupplier != null)
                {
                    //check image 
                    if (FileHelper.CheckFileSignature(logoSupplier, ".png") ||
                    FileHelper.CheckFileSignature(logoSupplier, ".jpg"))
                    {
                        // Process file
                        await using var item = new MemoryStream();
                        await logoSupplier.CopyToAsync(item);
                        //Up file to s3
                        pathLogo = $"Supplier/{newSupplierDto.SupplierName}_{Guid.NewGuid()}{Path.GetExtension(logoSupplier.FileName)}";
                        await _s3StorageService.UploadFileAsync(new S3RequestData()
                        {
                            InputStream = item,
                            Name = pathLogo
                        });
                    }
                    else
                    {
                        _logger.LogError($"Image need type png or jpg!!!");
                        return false;
                    }
                }
                else
                {
                    pathLogo = DEFAULT_SUPPLIER;
                }

                //add supplier
                var newSupplier = _mapper.Map<WmsSupplier>(newSupplierDto);
                //newSupplier.SupplierId = 6;
                newSupplier.Logo = pathLogo;
                newSupplier.CooperatedTime = DateTime.Now;
                newSupplier.Status = true;
                await _unitOfWork.SupplierRepository.AddAsync(newSupplier).ConfigureAwait(false);
                await _unitOfWork.CommitAsync().ConfigureAwait(false);

                //Get id base warehouse
                var allBaseWarehouses = await _unitOfWork.BaseWarehouseRepository.All().ConfigureAwait(false);
                var findBaseWarehouse = allBaseWarehouses.FirstOrDefault();

                //Add debt
                var newDebt = new DebtRequestDto
                {
                    DebtAmount = newSupplierDto.DebtAmount,
                    CreditorId = newSupplier.SupplierId,
                    DebtorId = findBaseWarehouse.WarehouseId
                };
                await _unitOfWork.BaseDebtRepository.AddAsync(_mapper.Map<WmsBaseDebt>(newDebt)).ConfigureAwait(false);
                await _unitOfWork.CommitAsync().ConfigureAwait(false);
                transaction.Commit();
                return true;

            }
            catch (Exception ex)
            {
                _logger.LogError($"Add supplier failed!!!", ex.Message);
                transaction.Rollback();
                return false;
            }
            finally
            {
                if (transaction.Connection != null) transaction.Connection.Close();
            }
        }

        public async Task<bool> UpdateSupplier(AddSupplierRequestDto entity, IFormFile logoSupplier)
        {
            if (string.IsNullOrWhiteSpace(entity.SupplierName) || string.IsNullOrWhiteSpace(entity.Email) || string.IsNullOrWhiteSpace(entity.PhoneNumber)) return false;
            using var transaction = _unitOfWork.BeginTransaction();
            try
            {
                var currentSupplier = await _unitOfWork.SupplierRepository.GetById(entity.SupplierId).ConfigureAwait(false);
                if (currentSupplier != null)
                {
                    var pathLogo = "";
                    if (logoSupplier != null)
                    {
                        if (FileHelper.CheckFileSignature(logoSupplier, ".png") ||
                       FileHelper.CheckFileSignature(logoSupplier, ".jpg"))
                        {
                            // Process file
                            await using var item = new MemoryStream();
                            await logoSupplier.CopyToAsync(item);
                            //Up file to s3
                            pathLogo = $"Supplier/{currentSupplier.SupplierName}_{Guid.NewGuid()}{Path.GetExtension(logoSupplier.FileName)}";
                            await _s3StorageService.UploadFileAsync(new S3RequestData()
                            {
                                InputStream = item,
                                Name = pathLogo
                            });
                        }
                        else
                        {
                            _logger.LogError($"Image need type png or jpg!!!");
                            return false;
                        }
                    }

                    currentSupplier.SupplierName = entity.SupplierName;
                    currentSupplier.SupplierName = entity.SupplierName;
                    currentSupplier.Address = entity.Address;
                    currentSupplier.Email = entity.Email;
                    currentSupplier.OtherInformation = entity.OtherInformation;
                    currentSupplier.CooperatedTime = entity.CooperatedTime;
                    currentSupplier.PhoneNumber = entity.PhoneNumber;
                    currentSupplier.Logo = (!string.IsNullOrEmpty(pathLogo)) ? pathLogo : currentSupplier.Logo;

                    _unitOfWork.SupplierRepository.Update(currentSupplier);
                    await _unitOfWork.CommitAsync().ConfigureAwait(false);
                    transaction.Commit();
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                transaction.Rollback();
                return false;
            }
            finally
            {
                if (transaction.Connection != null) transaction.Connection.Close();
            }
        }

        public async Task<bool> UpdateStatus(UpdateSupplierStatusRequestDto entity)
        {
            using var transaction = _unitOfWork.BeginTransaction();
            try
            {
                var currentSupplier = await _unitOfWork.SupplierRepository.GetById(entity.SupplierId).ConfigureAwait(false);
                if (currentSupplier != null)
                {
                    currentSupplier.Status = entity.Status;

                    _unitOfWork.SupplierRepository.Update(currentSupplier);
                    await _unitOfWork.CommitAsync().ConfigureAwait(false);
                    transaction.Commit();
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                transaction.Rollback();
                return false;
            }
            finally
            {
                if (transaction.Connection != null) transaction.Connection.Close();
            }
        }

        public async Task<List<SupplierHistoryImportResponseDto>> GetHistoryImport(int supplierId)
        {
            var result = await _unitOfWork.BaseImportRepository.GetHistoryImport(supplierId).ToListAsync().ConfigureAwait(false);
            return _mapper.Map<List<SupplierHistoryImportResponseDto>>(result);
        }

        public async Task<List<SupplierDebtResponseDto>> GetDebts(int supplierId)
        {
            var invoices = await _unitOfWork.BaseInvoiceRepository.GetDebt(supplierId).ToListAsync().ConfigureAwait(false);
            var result = invoices.Where(bi => bi.ImportId != null).OrderByDescending(bi => bi.CreateDate);
            return _mapper.Map<List<SupplierDebtResponseDto>>(result);
        }

        public async Task<List<SupplierHaveDebtResponseDto>> GetSupplierHaveDebt()
        {
            var result = await _unitOfWork.SupplierRepository
                .GetSupplierHaveDebt()
                .ConfigureAwait(false);

            return _mapper.Map<List<SupplierHaveDebtResponseDto>>(result);
        }

        public async Task<IEnumerable<SupplierSimpleResponseDto>> GetAllSuppliers()
        {
            var result = await _unitOfWork.SupplierRepository.All().ConfigureAwait(false);
            return _mapper.Map<IEnumerable<SupplierSimpleResponseDto>>(result); ;
        }

        public async Task<IEnumerable<SupplierResponseDto>> GetAllActiveSuppliers(string? searchWord)
        {
            var result = await _unitOfWork.SupplierRepository.SearchSuppliersList(searchWord).Where(x => x.Status == true)
                .ToListAsync().ConfigureAwait(false);

            foreach (var item in result)
            {
                item.Logo = _s3StorageService.GetFileUrl(new S3RequestData() { Name = item.Logo });
            }

            return _mapper.Map<IEnumerable<SupplierResponseDto>>(result); ;
        }
    }
}
