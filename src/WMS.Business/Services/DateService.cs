﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using WMS.Business.Abstractions.Service;
using WMS.Business.Dtos.FilterDto;
using WMS.Business.Utils.ExtensionMethods;
using WMS.Domain.Abstractions;
using WMS.Domain.Enums;

namespace WMS.Business.Services
{
    public class DateService : IDateService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<DateService> _logger;
        private readonly IMapper _mapper;

        public DateService(IUnitOfWork unitOfWork,
            ILogger<DateService> logger,
            IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
            _mapper = mapper;
        }

        /// <summary>
        /// 1-> firstDayOfWeek , lastDayOfWeek
        /// 2-> firstDayOfMonth , lastDayOfMonth
        /// 3-> firstDayOfYear , lastDayOfYear
        /// </summary>
        /// <param name="dateFilter"></param>
        /// <returns></returns>
        public DateFilterRequestDto GetDateTimeByDateFilter(short dateFilter)
        {
            DateTime dateNow = DateTime.Now;
            switch (dateFilter)
            {
                case (short)DateFilter.None:
                    return new DateFilterRequestDto();
                case (short)DateFilter.ThisWeek:
                    var firstDayOfWeek = DateTime.Now.StartOfWeek(DayOfWeek.Monday);
                    var lastDayOfWeek = firstDayOfWeek.AddDays(6);
                    return new DateFilterRequestDto(firstDayOfWeek, lastDayOfWeek);
                case (short)DateFilter.ThisMonth:
                    var firstDayOfMonth = new DateTime(dateNow.Year, dateNow.Month, 1);
                    var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddSeconds(-1);
                    return new DateFilterRequestDto(firstDayOfMonth, lastDayOfMonth);
                case (short)DateFilter.ThisYear:
                    DateTime firstDayOfYear = new DateTime(dateNow.Year, 1, 1);
                    DateTime lastDayOfYear = new DateTime(dateNow.Year, 12, 31);
                    return new DateFilterRequestDto(firstDayOfYear, lastDayOfYear);
                default:
                    return new DateFilterRequestDto(); ;
            }
        }
    }
}
