﻿using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using WMS.Business.Abstractions.Service;
using WMS.Business.Dtos.OptionDto;
using WMS.Business.Utils.StringUtils;
using WMS.Domain.Abstractions;
using WMS.Domain.Entities;

namespace WMS.Business.Services
{
    public class TokenService : ITokenService
    {
        private readonly JwtOptions _options;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<TokenService> _logger;
        private readonly IMemoryCache _cache;

        public TokenService(IOptions<JwtOptions> options,
            IUnitOfWork unitOfWork,
            ILogger<TokenService> logger,
            IMemoryCache cache)
        {
            _options = options.Value;
            _unitOfWork = unitOfWork;
            _logger = logger;
            _cache = cache;
        }

        public string GenerateAccessToken(IEnumerable<Claim> claims)
        {
            var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_options.AccessTokenSecret!));

            var token = new JwtSecurityToken(
                issuer: _options.ValidIssuer,
                audience: _options.ValidAudience,
                expires: DateTime.UtcNow.AddMinutes(_options.AccessTokenExpriedTimeInMinutes),
                claims: claims,
                signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
                );
            var tokenString = new JwtSecurityTokenHandler().WriteToken(token);
            return tokenString;
        }

        public string GenerateRefreshToken()
        {
            return StringHelper.RandomStringGenerate(60);
        }

        public ClaimsPrincipal GetPrincipalFromExpriedToken(string token)
        {
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateAudience = false,
                ValidateIssuer = false,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_options.AccessTokenSecret!)),
                ValidateLifetime = false
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out SecurityToken securityToken);
            if (securityToken is not JwtSecurityToken jwtSecurityToken || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
                throw new SecurityTokenException("Invalid token");

            return principal;
        }

        public async Task<bool> SaveRefreshToken(Guid userId, string refreshToken, string accessToken, DateTime expTime)
        {
            using var transction = _unitOfWork.BeginTransaction();
            try
            {
                //Get token by userId
                var token = await GetTokenByUserId(userId)
                    .ConfigureAwait(false);

                //Delete current refresh token if exist
                if (token is not null)
                {
                    if (!string.IsNullOrWhiteSpace(token.CurrentAccessToken))
                    {
                        bool revoked = RevokeAccessToken(token.CurrentAccessToken);
                        if (!revoked)
                        {
                            _logger.LogInformation($"Can't revoke token with value {token.CurrentAccessToken}");
                        }
                    }

                    _unitOfWork.TokenRepository
                        .Delete(token);

                    _logger.LogInformation($"Delete token with userId {userId}");
                }

                //Save new refresh token
                var newTokenInfo = new WmsToken(userId, refreshToken, accessToken, DateTime.UtcNow, expTime);

                await _unitOfWork.TokenRepository
                    .AddAsync(newTokenInfo);

                _logger.LogInformation($"Save new token with userId {userId}");

                await _unitOfWork.CommitAsync()
                    .ConfigureAwait(false);

                transction.Commit();

                return true;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                transction.Rollback();
                return false;
            }
        }

        public async Task<bool> UpdateAccessToken(Guid userId, string accessToken)
        {
            if (string.IsNullOrWhiteSpace(accessToken))
            {
                return false;
            }

            using var transaction = _unitOfWork.BeginTransaction();
            try
            {
                //Get token by userId
                var token = await GetTokenByUserId(userId)
                    .ConfigureAwait(false);

                if (token is not null)
                {
                    token.CurrentAccessToken = accessToken;
                    _unitOfWork.TokenRepository.Update(token);
                    await _unitOfWork.CommitAsync();

                    transaction.Commit();
                    return true;
                }

                return false;
            }
            catch (Exception)
            {
                transaction.Rollback();
            }
            return false;
        }

        public async Task<WmsToken?> GetTokenByUserId(Guid userId)
        {
            try
            {
                var user = await _unitOfWork.TokenRepository
                    .GetById(userId)
                    .ConfigureAwait(false);

                _logger.LogInformation($"Get token by userId: {userId}");

                return user;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return null;
            }
        }

        public bool RevokeAccessToken(string? token)
        {
            if (string.IsNullOrWhiteSpace(token))
            {
                return false;
            }

            try
            {
                var decoded = DecodeJwtToken(token);
                var jti = decoded.Claims.FirstOrDefault(claim => claim.Type.Equals(JwtRegisteredClaimNames.Jti))?.Value;
                var exp = decoded.Claims.FirstOrDefault(claim => claim.Type.Equals(JwtRegisteredClaimNames.Exp))?.Value;

                var blackListKey = string.Format("{0}_{1}", jti, exp);
                if (string.IsNullOrEmpty(jti) || string.IsNullOrEmpty(exp)) return false;

                var datetimeOffset = DateTimeOffset.FromUnixTimeSeconds(long.Parse(exp));
                var cacheEntryOptions = new MemoryCacheEntryOptions()
                    .SetAbsoluteExpiration(datetimeOffset);
                _cache.Set(blackListKey, token, cacheEntryOptions);
                _logger.LogInformation($"Revoke accesstoken {blackListKey}");
                return true;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return false;
            }
        }

        public bool IsBlacklistedToken(string? token)
        {
            if (string.IsNullOrWhiteSpace(token))
            {
                throw new ArgumentNullException(nameof(token));
            }

            try
            {
                var decoded = DecodeJwtToken(token);
                var jti = decoded.Claims.FirstOrDefault(claim => claim.Type.Equals(JwtRegisteredClaimNames.Jti))?.Value;
                var exp = decoded.Claims.FirstOrDefault(claim => claim.Type.Equals(JwtRegisteredClaimNames.Exp))?.Value;

                var blackListKey = string.Format("{0}_{1}", jti, exp);
                if (string.IsNullOrEmpty(jti) || string.IsNullOrEmpty(exp))
                {
                    return false;
                }

                return _cache.TryGetValue(blackListKey, out _);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return false;
            }
        }

        private JwtSecurityToken DecodeJwtToken(string token)
        {
            var handler = new JwtSecurityTokenHandler();
            var jwtSecurityToken = handler.ReadJwtToken(token);

            return jwtSecurityToken;
        }

        public async Task<WmsToken?> IsValidRefreshToken(string refreshToken)
        {
            try
            {
                var token = await _unitOfWork.TokenRepository
                    .FindObject(x => x.Token == refreshToken)
                    .ConfigureAwait(false);

                if (token is not null && token.CreateDate <= DateTime.UtcNow && token.ExpriedDate >= DateTime.UtcNow)
                {
                    return token;
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
            }

            return null;
        }
    }
}
