﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using WMS.Business.Abstractions.Service;
using WMS.Business.Dtos.BranchWarehouseDto;
using WMS.Business.Dtos.CommonDto;
using WMS.Domain.Abstractions;
using WMS.Domain.Entities;

namespace WMS.Business.Services
{
    public class BranchWarehouseService : IBranchWarehouseService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<BranchWarehouseService> _logger;
        private readonly IMapper _mapper;

        public BranchWarehouseService(IUnitOfWork unitOfWork,
            ILogger<BranchWarehouseService> logger,
            IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
            _mapper = mapper;
        }

        public async Task<ViewPaging<ViewBranchWarehousesListResponseDto>> ViewBranchWarehouseList(ViewBranchWarehousesListRequestDto entity)
        {
            try
            {
                if (entity.PagingRequest.PageSize <= 0) entity.PagingRequest.PageSize = 10;
                if (entity.PagingRequest.CurrentPage <= 0) entity.PagingRequest.CurrentPage = 1;
                var fullList = _unitOfWork.BranchWarehouseRepository
                    .SearchBranchWarehousesList(entity.SearchWord, entity.Status);

                var filteredList = await fullList
                    .Skip(entity.PagingRequest.PageSize * (entity.PagingRequest.CurrentPage - 1))
                    .Take(entity.PagingRequest.PageSize)
                    .ToListAsync()
                    .ConfigureAwait(false);

                var pagination = new Pagination(fullList.Count(), entity.PagingRequest.CurrentPage, entity.PagingRequest.PageRange, entity.PagingRequest.PageSize);

                var result = _mapper.Map<IEnumerable<ViewBranchWarehousesListResponseDto>>(filteredList);
                return new ViewPaging<ViewBranchWarehousesListResponseDto>(result, pagination);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return null;
            }
        }

        public async Task<ViewBranchWarehouseDetailsResponseDto> ViewBranchWarehouseDetails(Guid warehouseId)
        {
            var warehouse = await _unitOfWork.BranchWarehouseRepository.GetById(warehouseId).ConfigureAwait(false);
            if (warehouse == null) return null;
            warehouse.WmsWarehouseOwner = await _unitOfWork.UserRepository.FindObject(x => x.UserId.Equals(warehouse.WarehouseOwner)).ConfigureAwait(false);
            try
            {
                var result = _mapper.Map<ViewBranchWarehouseDetailsResponseDto>(warehouse);
                //get debt
                var debts = await _unitOfWork.BranchDebtRepository.FindObject(bd => bd.DebtorId.Equals(warehouseId)).ConfigureAwait(false);
                result.Debts = _mapper.Map<BranchDebtDto>(debts);
                //get invoice
                var invoices = await _unitOfWork.BaseInvoiceRepository.FindMultiple(bi => bi.BranchDebtId.Equals(debts.DebtId) && bi.ExportId != null).ConfigureAwait(false);
                result.Invoices = _mapper.Map<IEnumerable<BaseInvoiceDto>>(invoices.OrderByDescending(i=>i.CreateDate));
                //get export
                var exports = await _unitOfWork.BaseExportRepository.FindMultiple(bi => bi.BranchWarehouseId.Equals(warehouseId)).ConfigureAwait(false);
                result.Exports = _mapper.Map<IEnumerable<BaseHistoryExportResponseDto>>(exports);

                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return null;
            }
        }

        public async Task<bool> UpdateBranchWarehouseDetails(UpdateBranchWarehouseDetailsRequestDto entity)
        {
            var warehouse = await _unitOfWork.BranchWarehouseRepository.GetById(entity.WarehouseId).ConfigureAwait(false);
            if (warehouse == null) return false;

            if (entity.Status != 1 && entity.Status != 0) return false;

            using var transaction = _unitOfWork.BeginTransaction();
            try
            {
                warehouse.Note = entity.Note;
                warehouse.Status = entity.Status;
                _unitOfWork.BranchWarehouseRepository.Update(warehouse);

                var usersInWarehouse = await _unitOfWork.UserRepository.GetAllBranchUser(warehouse.WarehouseId).ToListAsync();
                foreach (var u in usersInWarehouse)
                {
                    if (entity.Status == 0) u.Status = 0;
                    if (entity.Status == 1) u.Status = 1;
                    _unitOfWork.UserRepository.Update(u);
                }

                await _unitOfWork.CommitAsync().ConfigureAwait(false);
                transaction.Commit();
                _logger.LogInformation($"Branch warehouse update successfully!!!");
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                transaction.Rollback();
                return false;
            }
            finally
            {
                if (transaction.Connection != null) transaction.Connection.Close();
            }
        }

        public async Task<bool> CreateBranchWarehouse(CreateBranchWarehouseRequestDto entity)
        {
            if (entity.Status != 1 && entity.Status != 0) return false;
            using var transaction = _unitOfWork.BeginTransaction();
            try
            {
                var baseWarehouse = await _unitOfWork.BaseWarehouseRepository.All().ConfigureAwait(false);
                var warehouse = _mapper.Map<WmsBranchWarehouse>(entity);
                await _unitOfWork.BranchWarehouseRepository.AddAsync(warehouse).ConfigureAwait(false);

                await _unitOfWork.BranchDebtRepository.AddAsync(new WmsBranchDebt()
                {
                    DebtorId = warehouse.WarehouseId,
                    CreditorId = baseWarehouse.First().WarehouseId,
                    DebtAmount = 0,
                }).ConfigureAwait(false);

                await _unitOfWork.CommitAsync().ConfigureAwait(false);
                transaction.Commit();
                _logger.LogInformation($"Branch warehouse created successfully!!!");
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return false;
            }
            finally
            {
                if (transaction.Connection != null) transaction.Connection.Close();
            }
        }

        public async Task<IEnumerable<ViewBranchWarehousesListResponseDto>> ViewAllBranchWarehousesList(bool? hasOwner)
        {
            var list = await _unitOfWork.BranchWarehouseRepository.GetBranchWarehousesIncludingOwner(hasOwner)
                .ToListAsync().ConfigureAwait(false);
            var result = _mapper.Map<IEnumerable<ViewBranchWarehousesListResponseDto>>(list);
            return result;
        }

        public async Task<IEnumerable<ViewBranchWarehousesListResponseDto>> GetAllBranchWarehouse()
        {
            var list = await _unitOfWork.BranchWarehouseRepository.All().ConfigureAwait(false);
            var result = _mapper.Map<IEnumerable<ViewBranchWarehousesListResponseDto>>(list);
            return result;
        }

        public async Task<List<BranchWarehouseHaveDebtResponseDto>> GetBranchHaveDebt()
        {
            var data = await _unitOfWork.BranchDebtRepository
                .GetBranchHaveDebt()
                .ConfigureAwait(false);

            return _mapper.Map<List<BranchWarehouseHaveDebtResponseDto>>(data);
        }
    }
}
