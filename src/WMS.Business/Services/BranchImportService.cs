﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using WMS.Business.Abstractions.Service;
using WMS.Business.Dtos.BaseExportDto;
using WMS.Business.Dtos.BranchImportDto;
using WMS.Business.Dtos.BranchWarehouseDto;
using WMS.Business.Dtos.CommonDto;
using WMS.Business.Dtos.S3Dto;
using WMS.Business.Utils.ExtensionMethods;
using WMS.Business.Utils.StringUtils;
using WMS.Domain.Abstractions;
using WMS.Domain.Constant;
using WMS.Domain.Entities;
using WMS.Domain.Enums;

namespace WMS.Business.Services
{
    public class BranchImportService : IBranchImportService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<BranchImportService> _logger;
        private readonly IMapper _mapper;
        private readonly IS3StorageService _s3StorageService;
        private readonly IDateService _dateService;
        private readonly IWarehouseService _warehouseService;
        private readonly IProductService _productService;
        private readonly IInvoiceService _invoiceService;

        public BranchImportService(IUnitOfWork unitOfWork,
            ILogger<BranchImportService> logger,
            IMapper mapper,
            IS3StorageService s3StorageService,
            IDateService dateService,
            IWarehouseService warehouseService,
            IProductService productService,
            IInvoiceService invoiceService)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
            _mapper = mapper;
            _s3StorageService = s3StorageService;
            _dateService = dateService;
            _warehouseService = warehouseService;
            _productService = productService;
            _invoiceService = invoiceService;
        }

        public async Task<Guid?> CreateBranchImportRequest(CreateBranchImportRequestRequestDto entity, string currentUser)
        {
            entity.RequestDate = DateTimeExtension.ConvertUTCToLocalTimeZone(entity.RequestDate);
            if (entity.RequestDate.Date < DateTime.Now.Date || entity.ProductRequests.Count() == 0) return null;
            //find manager or director
            var user = await _unitOfWork.UserRepository.FindObject(x => x.Username.Equals(currentUser)
                && x.Status == (short)UserStatusEnum.Active).ConfigureAwait(false);
            if (user == null) return null;

            //find warehouse of this manager
            var warehouse = await _warehouseService.GetWarehouseIdByUsername(currentUser).ConfigureAwait(false);
            if (warehouse.Equals(Guid.Empty)) return null;

            using var transaction = _unitOfWork.BeginTransaction();
            try
            {
                //create a new branch import request
                var request = new WmsBranchRequest();
                request.RequestCode = StringHelper.GenerateSpecifyId(50, "BRRQ");
                request.WarehouseId = warehouse;
                request.ApprovedBy = null;
                request.RequestBy = entity.ImportBy;
                request.RequestDate = entity.RequestDate;
                request.ApprovalStatus = BranchRequestApprovalStatusConstant.NotApproved;
                request.CreateDate = DateTime.Now;
                request.CreateUser = user.Username;
                request.UpdateDate = DateTime.Now;
                request.UpdateUser = user.Username;

                //calculate total price
                decimal totalPrice = 0;
                foreach (var product in entity.ProductRequests)
                {
                    //calculate
                    var existProduct = await _unitOfWork.ProductRepository.GetById(product.ProductId).ConfigureAwait(false);
                    if (existProduct == null) return null;
                    totalPrice += existProduct.ExportPrice * product.RequestQuantity;
                }

                request.TotalPrice = totalPrice;
                await _unitOfWork.BranchRequestRepository.AddAsync(request).ConfigureAwait(false);

                //add branch import products
                var productRequests = _mapper.Map<IEnumerable<WmsBranchProductRequest>>(entity.ProductRequests);
                foreach (var product in productRequests)
                {
                    var existProduct = await _unitOfWork.ProductRepository.GetById(product.ProductId).ConfigureAwait(false);
                    product.UnitPrice = existProduct.ExportPrice;
                    product.RequestId = request.RequestId;
                    await _unitOfWork.BranchProductRequestRepository.AddAsync(product).ConfigureAwait(false);
                }

                await _unitOfWork.CommitAsync().ConfigureAwait(false);
                transaction.Commit();
                _logger.LogInformation($"Create import request successfully!!!");
                return request.RequestId;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                transaction.Rollback();
                return null;
            }
            finally
            {
                if (transaction.Connection != null) transaction.Connection.Close();
            }
        }

        public async Task<bool> UpdateBranchImportRequest(UpdateBranchImportRequestRequestDto entity, string currentUser)
        {
            entity.RequestDate = DateTimeExtension.ConvertUTCToLocalTimeZone(entity.RequestDate);
            if (entity.RequestDate.Date < DateTime.Now.Date || entity.ProductRequests.Count() == 0) return false;

            //find manager or director
            var user = await _unitOfWork.UserRepository.FindObject(x => x.Username.Equals(currentUser)
                && x.Status == (short)UserStatusEnum.Active).ConfigureAwait(false);
            if (user == null) return false;

            if (await _unitOfWork.UserRepository.FindObject(x => x.UserId.Equals(entity.ImportBy)
                && x.Status == (short)UserStatusEnum.Active).ConfigureAwait(false) == null) return false;

            using var transaction = _unitOfWork.BeginTransaction();
            try
            {
                //cant update if already approved
                var branchRequest = await _unitOfWork.BranchRequestRepository.FindObject(x => x.RequestId.Equals(entity.RequestId)).ConfigureAwait(false);
                if (branchRequest == null || branchRequest.ApprovalStatus != BranchRequestApprovalStatusConstant.NotApproved) return false;

                if (await _unitOfWork.BranchWarehouseUserRepository
                .FindObject(x => x.BranchWarehouseId.Equals(branchRequest.WarehouseId)
                && x.UserId.Equals(user.UserId)).ConfigureAwait(false) == null) return false;

                //update request
                branchRequest.RequestDate = entity.RequestDate;
                branchRequest.RequestBy = entity.ImportBy;
                branchRequest.UpdateDate = DateTime.Now;
                branchRequest.UpdateUser = user.Username;

                //calculate  total price
                decimal totalPrice = 0;
                foreach (var product in entity.ProductRequests)
                {
                    var existProduct = await _unitOfWork.ProductRepository.GetById(product.ProductId).ConfigureAwait(false);
                    totalPrice += existProduct.ExportPrice * product.RequestQuantity;
                }

                branchRequest.TotalPrice = totalPrice;
                _unitOfWork.BranchRequestRepository.Update(branchRequest);

                //delete old products
                var products = await _unitOfWork.BranchProductRequestRepository.FindMultiple(x => x.RequestId.Equals(branchRequest.RequestId))
                    .ConfigureAwait(false);
                foreach (var product in products)
                {
                    _unitOfWork.BranchProductRequestRepository.Delete(product);
                }

                //add new branch import products
                var branchImportProducts = _mapper.Map<IEnumerable<WmsBranchProductRequest>>(entity.ProductRequests);
                foreach (var product in branchImportProducts)
                {
                    var existProduct = await _unitOfWork.ProductRepository.GetById(product.ProductId).ConfigureAwait(false);
                    product.UnitPrice = existProduct.ExportPrice;
                    product.RequestId = branchRequest.RequestId;
                    await _unitOfWork.BranchProductRequestRepository.AddAsync(product).ConfigureAwait(false);
                }

                await _unitOfWork.CommitAsync().ConfigureAwait(false);
                transaction.Commit();
                _logger.LogInformation($"Update import request successfully!!!");
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                transaction.Rollback();
                return false;
            }
            finally
            {
                if (transaction.Connection != null) transaction.Connection.Close();
            }
        }

        public async Task<bool> ApproveBranchImportRequest(Guid requestId, string currentUser)
        {
            var baseImport = await _unitOfWork.BaseImportRepository.FilterSearchBaseImport(null, null, null, null, default(DateTime), default(DateTime))
                .FirstOrDefaultAsync()
                .ConfigureAwait(false);
            if (baseImport == null) return false;

            //find manager or director
            var user = await _unitOfWork.UserRepository.FindObject(x => x.Username.Equals(currentUser)
                && x.Status == (short)UserStatusEnum.Active).ConfigureAwait(false);
            if (user == null) return false;

            //find base warehouse
            var baseWarehouse = await _unitOfWork.BaseWarehouseRepository.GetBaseWarehouseIncludingOwner()
                .FirstOrDefaultAsync()
                .ConfigureAwait(false);
            if (baseWarehouse == null) return false;

            var request = await _unitOfWork.BranchRequestRepository.FindObject(x => x.RequestId.Equals(requestId)).ConfigureAwait(false);
            if (request == null || request.ApprovalStatus != BranchRequestApprovalStatusConstant.NotApproved) return false;

            if (await _unitOfWork.BranchWarehouseUserRepository
                .FindObject(x => x.BranchWarehouseId.Equals(request.WarehouseId)
                && x.UserId.Equals(user.UserId)).ConfigureAwait(false) == null) return false;

            //create export for base warehouse
            var export = new WmsBaseExport()
            {
                WarehouseId = baseWarehouse.WarehouseId,
                ExportCode = StringHelper.GenerateSpecifyId(50, "BAEX"),
                AssignBy = null,
                ExportBy = null,
                ExportDate = request.RequestDate,
                TotalPrice = request.TotalPrice,
                TotalCost = request.TotalPrice,
                PaidAmount = 0,
                ExportStatus = ExportStatusConstant.Requested,
                PaymentStatus = PaymentStatusConstant.NotPaid,
                BranchWarehouseId = request.WarehouseId,
                ExtraCost = 0,
                RequestId = requestId,
                CreateDate = DateTime.Now,
                CreateUser = user.Username,
                UpdateDate = DateTime.Now,
                UpdateUser = user.Username,
            };

            var listProducts = await _unitOfWork.BranchProductRequestRepository.FindMultiple(x => x.RequestId.Equals(request.RequestId)).ConfigureAwait(false);

            using var transaction = _unitOfWork.BeginTransaction();
            try
            {
                //update request
                request.ApprovalStatus = BranchRequestApprovalStatusConstant.Approved;
                request.ApprovedBy = user.UserId;
                request.UpdateDate = DateTime.Now;
                request.UpdateUser = user.Username;
                _unitOfWork.BranchRequestRepository.Update(request);

                //add base export
                await _unitOfWork.BaseExportRepository.AddAsync(export).ConfigureAwait(false);

                //add base export products

                int index = 0;
                foreach (var product in listProducts)
                {
                    var existProduct = await _unitOfWork.ProductRepository.GetById(product.ProductId).ConfigureAwait(false);

                    var productExport = new WmsBaseExportProduct()
                    {
                        ProductId = product.ProductId,
                        ExportId = export.ExportId,
                        UnitPrice = existProduct.ExportPrice,
                        Quantity = product.RequestQuantity,
                        BaseImportId = baseImport.ImportId,
                        ExportCodeItem = export.ExportCode + "_" + index.ToString(),
                    };

                    index++;
                    await _unitOfWork.BaseExportProductRepository.AddAsync(productExport).ConfigureAwait(false);
                }

                await _unitOfWork.CommitAsync().ConfigureAwait(false);
                transaction.Commit();
                _logger.LogInformation("Approve request successfully!!!");
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                transaction.Rollback();
                return false;
            }
            finally
            {
                if (transaction.Connection != null) transaction.Connection.Close();
            }
        }

        public async Task<bool> RefuseBranchImportRequest(Guid requestId, string currentUser)
        {
            var request = await _unitOfWork.BranchRequestRepository.FindObject(x => x.RequestId.Equals(requestId)).ConfigureAwait(false);
            if (request == null || request.ApprovalStatus != BranchRequestApprovalStatusConstant.NotApproved) return false;

            var user = await _unitOfWork.UserRepository.FindObject(x => x.Username.Equals(currentUser)
                && x.Status == (short)UserStatusEnum.Active).ConfigureAwait(false);
            if (user == null) return false;

            if (await _unitOfWork.BranchWarehouseUserRepository
                .FindObject(x => x.BranchWarehouseId.Equals(request.WarehouseId)
                && x.UserId.Equals(user.UserId)).ConfigureAwait(false) == null) return false;

            using var transaction = _unitOfWork.BeginTransaction();
            try
            {
                request.ApprovalStatus = BranchRequestApprovalStatusConstant.Refused;
                _unitOfWork.BranchRequestRepository.Update(request);

                await _unitOfWork.CommitAsync().ConfigureAwait(false);
                transaction.Commit();
                _logger.LogInformation("Refuse request successfully!!!");
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                transaction.Rollback();
                return false;
            }
            finally
            {
                if (transaction.Connection != null) transaction.Connection.Close();
            }
        }

        public async Task<bool> ChangeBranchImportStatus(Guid importId, string currentUser)
        {
            //find manager or employee
            var user = await _unitOfWork.UserRepository.FindObject(x => x.Username.Equals(currentUser)
                && x.Status == (short)UserStatusEnum.Active).ConfigureAwait(false);
            if (user == null) return false;

            //find import
            var branchImport = await _unitOfWork.BranchImportRepository.FindObject(x => x.ImportId.Equals(importId)).ConfigureAwait(false);
            if (branchImport == null || branchImport.ImportStatus == ImportStatusConstant.Imported) return false;

            var branchWarehouse = await _unitOfWork.BranchWarehouseRepository.FindObject(x => x.WarehouseId.Equals(branchImport.WarehouseId) && x.Status == 1).ConfigureAwait(false);
            if (branchWarehouse == null) return false;

            if (await _unitOfWork.BranchWarehouseUserRepository
                .FindObject(x => x.BranchWarehouseId.Equals(branchImport.WarehouseId)
                && x.UserId.Equals(user.UserId)).ConfigureAwait(false) == null) return false;

            //find debt 
            var debt = await _unitOfWork.BranchDebtRepository.FindObject(x => x.DebtorId.Equals(branchImport.WarehouseId)).ConfigureAwait(false);
            if (debt == null) return false;

            using var transaction = _unitOfWork.BeginTransaction();
            try
            {
                //update status
                branchImport.ImportBy = user.UserId;
                branchImport.ImportedDate = DateTime.Now;
                branchImport.ImportStatus = ImportStatusConstant.Imported;
                branchImport.UpdateDate = DateTime.Now;
                branchImport.UpdateUser = currentUser;
                _unitOfWork.BranchImportRepository.Update(branchImport);
                await _unitOfWork.CommitAsync().ConfigureAwait(false);

                //update base export
                var baseExport = await _unitOfWork.BaseExportRepository.FindObject(x => x.ExportId.Equals(branchImport.BaseExportId)).ConfigureAwait(false);
                baseExport.ExportStatus = ExportStatusConstant.Exported;
                _unitOfWork.BaseExportRepository.Update(baseExport);

                //update debt
                if (branchImport.PaymentStatus == PaymentStatusConstant.NotPaid
                    || branchImport.PaymentStatus == PaymentStatusConstant.PartlyPaid)
                    debt.DebtAmount += (branchImport.TotalCost - branchImport.PaidAmount);
                _unitOfWork.BranchDebtRepository.Update(debt);

                //update stock
                var products = await _unitOfWork.BranchImportProductRepository
                    .FindMultiple(x => x.ImportId.Equals(importId)).ConfigureAwait(false);
                foreach (var product in products)
                {
                    product.StockQuantity = product.ImportedQuantity;
                    _unitOfWork.BranchImportProductRepository.Update(product);
                }

                foreach (var productId in products.Select(x => x.ProductId).Distinct())
                {
                    //change product price
                    var existProduct = await _unitOfWork.BranchWarehouseProductRepository
                        .FindObject(x => x.ProductId.Equals(productId) && x.WarehouseId.Equals(branchImport.WarehouseId)).ConfigureAwait(false);
                    if (existProduct == null)
                    {
                        var branchImportProduct = await _unitOfWork.BranchImportProductRepository
                            .FindObject(x => x.ImportId.Equals(importId) && x.ProductId.Equals(productId)).ConfigureAwait(false);
                        await _unitOfWork.BranchWarehouseProductRepository.AddAsync(new WmsBranchWarehouseProduct()
                        {
                            ProductId = productId,
                            WarehouseId = branchImport.WarehouseId,
                            CostPrice = branchImportProduct.CostPrice,
                            ExportPrice = branchImportProduct.CostPrice,
                        }).ConfigureAwait(false);
                    }
                    else
                    {
                        existProduct.CostPrice = _unitOfWork.BranchWarehouseProductRepository.GetCostPriceByProductId(productId, branchImport.WarehouseId);
                        _unitOfWork.BranchWarehouseProductRepository.Update(existProduct);
                    }
                }

                //add invoices 
                await _unitOfWork.BaseInvoiceRepository.AddAsync(new WmsBaseInvoice()
                {
                    BranchDebtId = debt.DebtId,
                    Type = InvoiceTypeConstant.PaymentVoucher,
                    Amount = baseExport.TotalCost,
                    Description = $"Chuyển hàng trị giá {baseExport.TotalCost} VND tới kho con {branchWarehouse.WarehouseName}",
                    ExportId = baseExport.ExportId,
                    BalanceChange = debt.DebtAmount,
                    InvoiceCode = StringHelper.GenerateSpecifyId(50, "BAINV"),
                }).ConfigureAwait(false);

                await _unitOfWork.BranchInvoiceRepository.AddAsync(new WmsBranchInvoice()
                {
                    DebtId = debt.DebtId,
                    Type = InvoiceTypeConstant.ReceiptVoucher,
                    Amount = baseExport.TotalCost,
                    Description = $"Nhận hàng trị giá {baseExport.TotalCost} VND từ kho tổng",
                    ImportId = branchImport.ImportId,
                    BalanceChange = debt.DebtAmount,
                    InvoiceCode = StringHelper.GenerateSpecifyId(50, "BRINV"),
                }).ConfigureAwait(false);

                await _unitOfWork.CommitAsync().ConfigureAwait(false);
                transaction.Commit();
                _logger.LogInformation($"Update import successfully!!!");
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                transaction.Rollback();
                return false;
            }
            finally
            {
                if (transaction.Connection != null) transaction.Connection.Close();
            }
        }

        public async Task<ViewPaging<GetBranchImportResponseDto>> GetBranchImportList(GetBranchImportRequestDto requestDto, string currentUser)
        {
            try
            {
                if (requestDto.PagingRequest.PageSize <= 0) requestDto.PagingRequest.PageSize = 10;
                if (requestDto.PagingRequest.CurrentPage <= 0) requestDto.PagingRequest.CurrentPage = 1;
                if (requestDto.ImportStatus > ImportStatusConstant.Imported || requestDto.ImportStatus < ImportStatusConstant.NotImported
                    || requestDto.PaymentStatus < PaymentStatusConstant.NotPaid || requestDto.PaymentStatus > PaymentStatusConstant.Paid
                    || requestDto.DateFilter < 0 || requestDto.DateFilter > 3) return null;

                //Convert enum date -> dateTime◘
                var dateFilter = _dateService.GetDateTimeByDateFilter(requestDto.DateFilter);
                //GetWarehouseIdByUsername
                var warehouseId = await _warehouseService.GetWarehouseIdByUsername(currentUser).ConfigureAwait(false);
                //FilderBranchRequest
                var fullList = await _unitOfWork.BranchImportRepository.FilterBranchImport(warehouseId,
                                                                                             requestDto.SearchWord,
                                                                                             requestDto.ImportStatus,
                                                                                             requestDto.PaymentStatus,
                                                                                             dateFilter.StartDate,
                                                                                             dateFilter.EndDate)
                    .ToListAsync().ConfigureAwait(false);

                var filteredList = fullList
                    .Skip(requestDto.PagingRequest.PageSize * (requestDto.PagingRequest.CurrentPage - 1))
                    .Take(requestDto.PagingRequest.PageSize);

                var pagination = new Pagination(fullList.Count(), requestDto.PagingRequest.CurrentPage,
                    requestDto.PagingRequest.PageRange, requestDto.PagingRequest.PageSize);

                var result = _mapper.Map<IEnumerable<GetBranchImportResponseDto>>(filteredList);

                foreach (var branchImport in result)
                {
                    //merge product duplicate
                    branchImport.WmsBranchImportProducts = branchImport.WmsBranchImportProducts.GroupBy(bip => bip.ProductId)
                                                                               .Select(bip => new BranchImportProductResponseDto(
                                                                            bip.Key,
                                                                            bip.First().ProductName,
                                                                            bip.First().ProductImage,
                                                                            bip.First().UnitPrice,
                                                                            bip.Sum(k => k.ImportedQuantity),
                                                                            bip.Sum(k => k.StockQuantity)))
                                                                           .ToList();
                    foreach (var branchImportProduct in branchImport.WmsBranchImportProducts)
                    {
                        var productImageUrl = _s3StorageService.GetFileUrl(new S3RequestData()
                        {
                            Name = branchImportProduct.ProductImage,
                        });
                        branchImportProduct.ProductImage = productImageUrl;
                    }
                }
                return new ViewPaging<GetBranchImportResponseDto>(result, pagination);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return null;
            }
        }

        public async Task<GetBranchImportDetailsResponseDto> GetBranchImportDetails(Guid branchImportId)
        {

            try
            {
                var branchImport = await _unitOfWork.BranchImportRepository.GetBranchRequestDetails(branchImportId).ConfigureAwait(false);
                var result = _mapper.Map<GetBranchImportDetailsResponseDto>(branchImport);
                //merge product duplicate
                result.WmsBranchImportProducts = result.WmsBranchImportProducts.GroupBy(bip => bip.ProductId)
                                                                           .Select(bip => new BranchImportProductResponseDto(
                                                                            bip.Key,
                                                                            bip.First().ProductName,
                                                                            bip.First().ProductImage,
                                                                            bip.First().UnitPrice,
                                                                            bip.Sum(k => k.ImportedQuantity),
                                                                            bip.Sum(k => k.StockQuantity)))
                                                                           .ToList();
                if (result != null)
                {
                    foreach (var branchImportProduct in result.WmsBranchImportProducts)
                    {
                        //Get branch import Product By ImportId ProductId And List BaseImportId 
                        var importProductDetails = await _unitOfWork.BranchImportProductRepository
                            .GetBranchImportProductByImportIdAndProductId(branchImportId,
                                                                          branchImportProduct.ProductId)
                                                                          .ConfigureAwait(false);
                        var importProductDetailsMap = _mapper.Map<IEnumerable<BranchImportProductResponseList>>(importProductDetails);
                        branchImportProduct.BranchImportProductList = importProductDetailsMap;


                        var productImageUrl = _s3StorageService.GetFileUrl(new S3RequestData()
                        {
                            Name = branchImportProduct.ProductImage,
                        });
                        branchImportProduct.ProductImage = productImageUrl;
                    }
                }
                //get debt by branchWarehouseId
                var debt = await _unitOfWork.BranchDebtRepository.FindObject(bd => bd.DebtorId.Equals(branchImport.WarehouseId)).ConfigureAwait(false);
                decimal debtAmount = 0;
                if (debt != null) debtAmount = debt.DebtAmount;
                //get branchImport by branchWarehouseId
                var branchImports = await _unitOfWork.BranchImportRepository.FindMultiple(bi => bi.WarehouseId.Equals(branchImport.WarehouseId)).ConfigureAwait(false);
                var listImportId = branchImports.Select(bi => bi.ImportId);
                //get branchProductReturnDetails by list ImportId
                var branchProductReturnDetails = await _unitOfWork.BranchProductReturnDetailRepository.FindMultiple(bdrd => listImportId.Contains(bdrd.ImportId)).ConfigureAwait(false);
                var listProductReturnId = branchProductReturnDetails.Select(bprd => bprd.ProductReturnId);
                var totalReturnProduct = listProductReturnId.Distinct().Count();
                result.DebtBranchResponseDto = new DebtBranchResponseDto()
                {
                    DebtAmount = debtAmount,
                    TotalImport = branchImports.Count(),
                    TotalReturnProduct = totalReturnProduct
                };
                //get product returns by branch import id
                result.ProductReturns = await _productService.GetProductReturnFromBranchByImportId(branchImportId).ConfigureAwait(false);

                //get invoice
                var invoices = await _invoiceService.GetBranchInvoiceByImportId(branchImportId).ConfigureAwait(false);
                //var branchroductReturn = await _unitOfWork.BranchProductReturnRepository
                //    .FindMultiple(bpr => bpr.ImportId.Equals(branchImportId))
                //    .ConfigureAwait(false);
                //var invoiceReturn = await _invoiceService.GetBranchInvoiceByReturnId(branchroductReturn.Select(bpr => bpr.ProductReturnId)).ConfigureAwait(false);
                result.Invoices = invoices.OrderByDescending(i => i.UpdateDate);


                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return null;
            }
            throw new NotImplementedException();
        }

        public async Task<bool> PayForBranchProductReturn(PayForBranchProductReturnRequestDto entity, string currentUser)
        {
            var user = await _unitOfWork.UserRepository.FindObject(x => x.Username.Equals(currentUser)
                && x.Status == (short)UserStatusEnum.Active).ConfigureAwait(false);
            if (user == null) return false;

            var branchReturn = await _unitOfWork.BranchProductReturnRepository
                .FindObject(x => x.ProductReturnId.Equals(entity.BranchProductReturnId))
                .ConfigureAwait(false);
            if (branchReturn == null || branchReturn.PaymentStatus == PaymentStatusConstant.Paid
                || entity.PayAmount <= 0 || branchReturn.Status != ProductReturnStatusConstant.Returned) return false;

            var branchImport = await _unitOfWork.BranchImportRepository.FindObject(x => x.ImportId.Equals(branchReturn.ImportId))
                .ConfigureAwait(false);
            if (branchImport == null) return false;

            var debt = await _unitOfWork.BranchDebtRepository.FindObject(x => x.DebtorId == branchImport.WarehouseId).ConfigureAwait(false);
            if (debt == null) return false;

            //update return
            var paid = branchReturn.PaidAmount + entity.PayAmount;
            if (paid > branchReturn.TotalPrice) return false;

            branchReturn.PaidAmount += entity.PayAmount;
            if (branchReturn.PaidAmount == branchReturn.TotalPrice) branchReturn.PaymentStatus = PaymentStatusConstant.Paid;
            else branchReturn.PaymentStatus = PaymentStatusConstant.PartlyPaid;

            using var transaction = _unitOfWork.BeginTransaction();
            try
            {
                //update return
                branchReturn.UpdateDate = DateTime.Now;
                branchReturn.UpdateUser = user.Username;
                _unitOfWork.BranchProductReturnRepository.Update(branchReturn);

                //update debt
                debt.DebtAmount += entity.PayAmount;
                _unitOfWork.BranchDebtRepository.Update(debt);

                //add new base invoice
                var branchWarehouse = await _unitOfWork.BranchWarehouseRepository.FindObject(x => x.WarehouseId.Equals(branchImport.WarehouseId)).ConfigureAwait(false);
                var baseInvoice = new WmsBaseInvoice()
                {
                    BranchDebtId = debt.DebtId,
                    Type = InvoiceTypeConstant.PaymentVoucher,
                    Amount = entity.PayAmount,
                    Description = $"Trả {entity.PayAmount} VND cho đơn trả hàng từ kho con {branchWarehouse.WarehouseName}",
                    BranchProductReturnId = entity.BranchProductReturnId,
                    CreateDate = DateTime.Now,
                    CreateUser = user.Username,
                    UpdateDate = DateTime.Now,
                    UpdateUser = user.Username,
                    BalanceChange = debt.DebtAmount,
                    InvoiceCode = StringHelper.GenerateSpecifyId(50, "BAINV"),
                };
                await _unitOfWork.BaseInvoiceRepository.AddAsync(baseInvoice).ConfigureAwait(false);

                //add new branch invoice
                var branchInvoice = new WmsBranchInvoice()
                {
                    DebtId = debt.DebtId,
                    Type = InvoiceTypeConstant.ReceiptVoucher,
                    Amount = entity.PayAmount,
                    Description = $"Kho con {branchWarehouse.WarehouseName} nhận {entity.PayAmount} VND cho đơn trả hàng tới kho tổng",
                    ReturnId = entity.BranchProductReturnId,
                    CreateDate = DateTime.Now,
                    CreateUser = user.Username,
                    UpdateDate = DateTime.Now,
                    UpdateUser = user.Username,
                    BalanceChange = debt.DebtAmount,
                    InvoiceCode = StringHelper.GenerateSpecifyId(50, "BRINV"),
                };
                await _unitOfWork.BranchInvoiceRepository.AddAsync(branchInvoice).ConfigureAwait(false);

                await _unitOfWork.CommitAsync().ConfigureAwait(false);
                transaction.Commit();
                _logger.LogInformation($"Pay for return successfully!!!");
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                transaction.Rollback();
                return false;
            }
            finally
            {
                if (transaction.Connection != null) transaction.Connection.Close();
            }
        }
    }
}
