﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using WMS.Business.Abstractions.Service;
using WMS.Business.Dtos.CommonDto;
using WMS.Business.Dtos.ProductDto;
using WMS.Business.Dtos.ProductReturnFromBranchDto;
using WMS.Business.Dtos.S3Dto;
using WMS.Business.Utils.FileHelper;
using WMS.Business.Utils.StringUtils;
using WMS.Domain.Abstractions;
using WMS.Domain.Constant;
using WMS.Domain.Entities;
using static Amazon.S3.Util.S3EventNotification;

namespace WMS.Business.Services
{
    public class ProductService : IProductService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IS3StorageService _s3StorageService;
        private readonly ILogger<ProductService> _logger;
        private readonly IDateService _dateService;
        private readonly IWarehouseService _warehouseService;

        public ProductService(IUnitOfWork unitOfWork,
                              IMapper mapper,
                              IS3StorageService s3StorageService,
                              ILogger<ProductService> logger,
                              IDateService dateService,
                              IWarehouseService warehouseService)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _s3StorageService = s3StorageService;
            _logger = logger;
            _dateService = dateService;
            _warehouseService = warehouseService;
        }

        public async Task<bool> CreateProduct(CreateProductRequestDto newProductDto, IFormFile imageProduct)
        {
            if (await _unitOfWork.CategoryRepository.GetById(newProductDto.CategoryId).ConfigureAwait(false) == null) return false;
            using var transaction = _unitOfWork.BeginTransaction();
            try
            {
                //check image 
                if (FileHelper.CheckFileSignature(imageProduct, ".png") ||
                    FileHelper.CheckFileSignature(imageProduct, ".jpg"))
                {
                    // Process file
                    await using var item = new MemoryStream();
                    await imageProduct.CopyToAsync(item);
                    //Up file to s3
                    var nameProduct = $"Product/{newProductDto.ProductName}_{Guid.NewGuid()}{Path.GetExtension(imageProduct.FileName)}";
                    await _s3StorageService.UploadFileAsync(new S3RequestData()
                    {
                        InputStream = item,
                        Name = nameProduct
                    });
                    //add product
                    var newProduct = _mapper.Map<WmsProduct>(newProductDto);
                    newProduct.ProductImage = nameProduct;
                    newProduct.CostPrice = newProductDto.UnitPrice;
                    if (String.IsNullOrEmpty(newProduct.SKU))
                    {
                        newProduct.SKU = StringHelper.GenerateSpecifyId(50, "SKU");
                    }
                    await _unitOfWork.ProductRepository.AddAsync(newProduct).ConfigureAwait(false);
                    await _unitOfWork.CommitAsync().ConfigureAwait(false);
                    transaction.Commit();
                    return true;
                }
                else
                {
                    _logger.LogError($"Image need type png or jpg!!!");
                    return false;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Create product failed!!!", ex.Message);
                transaction.Rollback();
                return false;
            }
        }

        /// <summary>
        /// Get product detail by productId
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public async Task<GetProductDetailResponseDto?> GetProductDetail(Guid productId)
        {
            var productInfo = await _unitOfWork.ProductRepository
                .GetById(productId)
                .ConfigureAwait(false);

            if (productInfo is null)
            {
                return null;
            }

            var result = _mapper.Map<GetProductDetailResponseDto>(productInfo);

            result.ProductImage = _s3StorageService.GetFileUrl(new S3RequestData()
            {
                Name = result.ProductImage
            });

            return result;
        }

        /// <summary>
        /// Get Product List with paging
        /// </summary>
        /// <param name="pagingRequest"></param>
        /// <returns></returns>
        public async Task<ViewPaging<GetProductListResponseDto>> GetProductList(GetProductListRequestDto requestDto)
        {
            var queryData = await _unitOfWork.ProductRepository
                .GetProductListPaging(requestDto.CategoryId,
                                      requestDto.SearchWord,
                                      requestDto.Visible,
                                      (requestDto.PagingRequest.CurrentPage - 1) * requestDto.PagingRequest.PageSize,
                                       requestDto.PagingRequest.PageSize)
                .ConfigureAwait(false);

            var mappedData = _mapper.Map<List<GetProductListResponseDto>>(queryData);

            var pagination = new Pagination(queryData.Count(), requestDto.PagingRequest.CurrentPage, requestDto.PagingRequest.PageRange, requestDto.PagingRequest.PageSize);

            foreach (var item in mappedData)
            {
                item.ProductImage = _s3StorageService.GetFileUrl(new S3RequestData() { Name = item.ProductImage });
            }

            return new ViewPaging<GetProductListResponseDto>(mappedData, pagination);
        }

        /// <summary>
        /// Get Product Return To Supplier
        /// </summary>
        /// <param name="pagingRequest"></param>
        /// <returns></returns>
        public async Task<ViewPaging<ProductReturnSupplierResponseDto>> GetProductReturnToSupplier(PagingRequest pagingRequest)
        {
            var totalItems = await _unitOfWork.BaseProductReturnRepository
                .Count()
                .ConfigureAwait(false);

            var pagination = new Pagination(totalItems, pagingRequest.CurrentPage, pagingRequest.PageRange, pagingRequest.PageSize);

            var queryData = await _unitOfWork.BaseProductReturnRepository
                .GetProductReturnToSupplier((pagingRequest.CurrentPage - 1) * pagingRequest.PageSize, pagingRequest.PageSize)
                .ConfigureAwait(false);

            var mappedData = _mapper.Map<List<ProductReturnSupplierResponseDto>>(queryData);

            return new ViewPaging<ProductReturnSupplierResponseDto>(mappedData, pagination);
        }

        /// <summary>
        /// Get Product Return To Supplier By ImportId
        /// </summary>
        /// <param name="pagingRequest"></param>
        /// <param name="importId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<ProductReturnSupplierResponseDto>> GetProductReturnToSupplierByImportId(Guid importId)
        {

            var queryData = await _unitOfWork.BaseProductReturnRepository
                .GetProductReturnToSupplierByImportId(importId)
                .ConfigureAwait(false);

            var result = _mapper.Map<IEnumerable<ProductReturnSupplierResponseDto>>(queryData);

            return result;
        }

        /// <summary>
        /// Get Product Return From Branch
        /// </summary>
        /// <param name="pagingRequest"></param>
        /// <returns></returns>
        public async Task<ViewPaging<GetProductsReturnFromBranchResponseDto>> GetProductReturnFromBranch(GetProductReturnFromBranchRequestDto requestDto)
        {
            try
            {
                //Convert enum date -> dateTime
                var dateFilter = _dateService.GetDateTimeByDateFilter(requestDto.DateFilter);

                //Get product return from branch
                int skipPage = (requestDto.PagingRequest.CurrentPage - 1) * requestDto.PagingRequest.PageSize;
                var queryData = await _unitOfWork.BranchProductReturnRepository
                    .GetProductsReturnFromBranch(skipPage,
                    requestDto.PagingRequest.PageSize,
                    requestDto.SearchWord,
                    requestDto.BranchWarehouseId,
                    dateFilter.StartDate,
                    dateFilter.EndDate,
                    requestDto.PaymentStatus,
                    requestDto.Status)
                    .ConfigureAwait(false);

                //Get total items
                var totalItems = await _unitOfWork.BranchProductReturnRepository
                    .CountProductReturnFromBranch(
                    requestDto.SearchWord,
                    requestDto.BranchWarehouseId,
                    dateFilter.StartDate,
                    dateFilter.EndDate,
                    requestDto.PaymentStatus,
                    requestDto.Status)
                    .ConfigureAwait(false);
                var pagination = new Pagination(totalItems, requestDto.PagingRequest.CurrentPage, requestDto.PagingRequest.PageRange, requestDto.PagingRequest.PageSize);

                var mappedData = _mapper.Map<List<GetProductsReturnFromBranchResponseDto>>(queryData);

                return new ViewPaging<GetProductsReturnFromBranchResponseDto>(mappedData, pagination);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                var pagination = new Pagination(0, 1, requestDto.PagingRequest.PageRange, requestDto.PagingRequest.PageSize);
                return new ViewPaging<GetProductsReturnFromBranchResponseDto>(new List<GetProductsReturnFromBranchResponseDto>(), pagination);
            }
        }

        /// <summary>
        /// Get Product Return From Branch By ImportId
        /// </summary>
        /// <param name="pagingRequest"></param>
        /// <returns></returns>
        public async Task<List<GetProductsReturnFromBranchResponseDto>> GetProductReturnFromBranchByImportId(Guid importId)
        {
            //Get product return from branch
            var queryData = await _unitOfWork.BranchProductReturnRepository
                .GetProductsReturnFromBranchByImportId(importId)
                .ConfigureAwait(false);

            var mappedData = _mapper.Map<List<GetProductsReturnFromBranchResponseDto>>(queryData);

            return mappedData;
        }

        /// <summary>
        /// Update Product Detail
        /// </summary>
        /// <param name="requestDto"></param>
        /// <returns></returns>
        public async Task<bool> UpdateProductDetail(UpdateProductDetailRequestDto requestDto)
        {
            using var transaction = _unitOfWork.BeginTransaction();
            try
            {
                var currentProductInfo = await _unitOfWork.ProductRepository
                    .FindObject(x => x.ProductId.Equals(requestDto.ProductId))
                    .ConfigureAwait(false);

                if (currentProductInfo is null)
                {
                    return false;
                }

                if (requestDto.ProductImage is not null)
                {
                    if (!FileHelper.CheckFileSignature(requestDto.ProductImage, ".png") ||
                    !FileHelper.CheckFileSignature(requestDto.ProductImage, ".jpg"))
                    {
                        return false;
                    }

                    // Delete old image in s3
                    await _s3StorageService.DeleteFileAsync(new S3RequestData()
                    {
                        Name = currentProductInfo.ProductName
                    });

                    //Upload new image
                    await using var item = new MemoryStream();
                    await requestDto.ProductImage.CopyToAsync(item);
                    var nameProduct = $"Product/{requestDto.ProductName}_{Guid.NewGuid()}{Path.GetExtension(requestDto.ProductImage.FileName)}";
                    await _s3StorageService.UploadFileAsync(new S3RequestData()
                    {
                        InputStream = item,
                        Name = nameProduct
                    });
                    currentProductInfo.ProductImage = nameProduct;
                }

                //Update basic product info
                currentProductInfo.SKU = requestDto.SKU;
                currentProductInfo.CategoryId = requestDto.CategoryId;
                currentProductInfo.ProductName = requestDto.ProductName;
                currentProductInfo.ProductDescription = requestDto.ProductDescription;
                currentProductInfo.Visible = requestDto.Visible;
                currentProductInfo.UnitPrice = requestDto.UnitPrice;
                currentProductInfo.CostPrice = requestDto.SalePrice;
                currentProductInfo.ExportPrice = requestDto.ExportPrice;
                currentProductInfo.UnitType = requestDto.UnitType;
                currentProductInfo.Status = requestDto.Status;

                _unitOfWork.ProductRepository.Update(currentProductInfo);

                await _unitOfWork.CommitAsync();
                transaction.Commit();

                return true;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                _logger.LogError("Update product failed", e.Message);
            }

            return false;
        }

        /// <summary>
        /// Search Product By Name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<IEnumerable<SearchProductByNameResponseDto>> SearchProductByName(string name)
        {
            var data = await _unitOfWork.ProductRepository
                .GetProductByName(name)
                .ConfigureAwait(false);

            data = data.Where(x => x.Visible!.Value);

            var mappedData = _mapper.Map<IEnumerable<SearchProductByNameResponseDto>>(data);

            foreach (var product in mappedData)
            {
                var productImageUrl = _s3StorageService.GetFileUrl(new S3RequestData()
                {
                    Name = product.ProductImage,
                });
                product.ProductImage = productImageUrl;
            }
            return mappedData;
        }

        /// <summary>
        /// Create Product Return Supplier
        /// </summary>
        /// <param name="userRq"></param>
        /// <returns></returns>
        public async Task<bool> CreateBaseReturn(CreateProducReturnSupplierRequestDto userRq, string currentUser)
        {
            if (userRq.ProductRequest.Count == 0) return false;
            var baseImport = await _unitOfWork.BaseImportRepository.FindObject(x => x.ImportId.Equals(userRq.ImportId))
                .ConfigureAwait(false);
            if (baseImport == null || baseImport.ImportStatus != ImportStatusConstant.Imported) return false;
            var supplier = await _unitOfWork.SupplierRepository.GetById(baseImport.SupplierId).ConfigureAwait(false);

            var transaction = _unitOfWork.BeginTransaction();
            try
            {
                var productReturn = _mapper.Map<WmsBaseProductReturn>(userRq);
                productReturn.ReturnCode = StringHelper.GenerateSpecifyId(50, "BART");
                productReturn.Status = ProductReturnStatusConstant.Returned;
                productReturn.CreateDate = DateTime.Now;
                productReturn.CreateUser = currentUser;
                productReturn.UpdateDate = DateTime.Now;
                productReturn.UpdateUser = currentUser;

                //calculate total price
                decimal totalPrice = 0;
                foreach (var product in userRq.ProductRequest)
                {
                    var baseProduct = await _unitOfWork.BaseImportProductRepository
                        .FindObject(x => x.ImportId.Equals(baseImport.ImportId) && x.ProductId.Equals(product.ProductId))
                        .ConfigureAwait(false);
                    if (baseProduct == null || product.RequestQuantity > baseProduct.StockQuantity) return false;
                    totalPrice += baseProduct.UnitPrice * product.RequestQuantity;
                }
                productReturn.TotalPrice = totalPrice;

                //calculate payment
                //decrease base debt
                var baseDebt = await _unitOfWork.BaseDebtRepository.FindObject(x => x.CreditorId == baseImport.SupplierId)
                    .ConfigureAwait(false);
                if (baseDebt is null) return false;

                //if the import debt is more than the return price, then pay for the return by decreasing the import debt
                var importDebt = baseImport.TotalCost - baseImport.PaidAmount;
                if (importDebt >= productReturn.TotalPrice)
                {
                    //update return
                    productReturn.PaidAmount = productReturn.TotalPrice;
                    productReturn.PaymentStatus = PaymentStatusConstant.Paid;
                    await _unitOfWork.BaseProductReturnRepository.AddAsync(productReturn).ConfigureAwait(false);

                    //create invoice pay
                    var baseInvoicePay = new WmsBaseInvoice()
                    {
                        DebtId = baseDebt.DebtId,
                        Type = InvoiceTypeConstant.PaymentVoucher,
                        Amount = productReturn.TotalPrice,
                        Description = $"Trả hàng trị giá {productReturn.TotalPrice} VND cho đơn trả hàng tới NCC {supplier.SupplierName}",
                        ReturnId = productReturn.ProductReturnId,
                        CreateDate = DateTime.Now,
                        CreateUser = currentUser,
                        UpdateDate = DateTime.Now,
                        UpdateUser = currentUser,
                        BalanceChange = baseDebt.DebtAmount - productReturn.TotalPrice,
                        InvoiceCode = StringHelper.GenerateSpecifyId(50, "BAINV"),
                    };
                    await _unitOfWork.BaseInvoiceRepository.AddAsync(baseInvoicePay).ConfigureAwait(false);

                    //create invoice receive
                    var baseInvoice = new WmsBaseInvoice()
                    {
                        DebtId = baseDebt.DebtId,
                        Type = InvoiceTypeConstant.ReceiptVoucher,
                        Amount = productReturn.TotalPrice,
                        Description = $"Nhận {productReturn.TotalPrice} VND cho đơn trả hàng tới NCC {supplier.SupplierName}",
                        ReturnId = productReturn.ProductReturnId,
                        CreateDate = DateTime.Now,
                        CreateUser = currentUser,
                        UpdateDate = DateTime.Now,
                        UpdateUser = currentUser,
                        BalanceChange = baseDebt.DebtAmount,
                        InvoiceCode = StringHelper.GenerateSpecifyId(50, "BAINV"),
                    };
                    await _unitOfWork.BaseInvoiceRepository.AddAsync(baseInvoice).ConfigureAwait(false);


                    //update import
                    //increase paid amount
                    baseImport.PaidAmount += productReturn.TotalPrice;
                    if (baseImport.PaidAmount == baseImport.TotalCost) baseImport.PaymentStatus = PaymentStatusConstant.Paid;
                    else baseImport.PaymentStatus = PaymentStatusConstant.PartlyPaid;
                    _unitOfWork.BaseImportRepository.Update(baseImport);

                    //add new invoice
                    var invoice = new WmsBaseInvoice()
                    {
                        DebtId = baseDebt.DebtId,
                        Type = InvoiceTypeConstant.PaymentVoucher,
                        Amount = productReturn.TotalPrice,
                        Description = $"Trả {productReturn.TotalPrice} VND cho đơn nhập hàng từ NCC {supplier.SupplierName}",
                        ImportId = baseImport.ImportId,
                        BalanceChange = baseDebt.DebtAmount - productReturn.TotalPrice,
                        InvoiceCode = StringHelper.GenerateSpecifyId(50, "BAINV"),
                    };
                    await _unitOfWork.BaseInvoiceRepository.AddAsync(invoice).ConfigureAwait(false);
                }
                else
                {
                    //update return
                    productReturn.PaidAmount = 0;
                    productReturn.PaymentStatus = PaymentStatusConstant.NotPaid;
                    await _unitOfWork.BaseProductReturnRepository.AddAsync(productReturn).ConfigureAwait(false);

                    //create invoice pay
                    var baseInvoice = new WmsBaseInvoice()
                    {
                        DebtId = baseDebt.DebtId,
                        Type = InvoiceTypeConstant.PaymentVoucher,
                        Amount = productReturn.TotalPrice,
                        Description = $"Trả hàng trị giá {productReturn.TotalPrice} VND cho đơn trả hàng tới NCC {supplier.SupplierName}",
                        ReturnId = productReturn.ProductReturnId,
                        CreateDate = DateTime.Now,
                        CreateUser = currentUser,
                        UpdateDate = DateTime.Now,
                        UpdateUser = currentUser,
                        BalanceChange = baseDebt.DebtAmount - productReturn.TotalPrice,
                        InvoiceCode = StringHelper.GenerateSpecifyId(50, "BAINV"),
                    };
                    await _unitOfWork.BaseInvoiceRepository.AddAsync(baseInvoice).ConfigureAwait(false);
                }

                //decrease debt
                baseDebt.DebtAmount -= productReturn.TotalPrice;
                _unitOfWork.BaseDebtRepository.Update(baseDebt);
                await _unitOfWork.CommitAsync().ConfigureAwait(false);


                //add detail
                if (userRq.ProductRequest is not null && userRq.ProductRequest.Any())
                {
                    var productReturnDetails = _mapper.Map<List<WmsBaseProductReturnDetail>>(userRq.ProductRequest);
                    foreach (var item in productReturnDetails)
                    {
                        var baseProduct = await _unitOfWork.BaseImportProductRepository
                            .FindObject(x => x.ImportId.Equals(baseImport.ImportId) && x.ProductId.Equals(item.ProductId))
                            .ConfigureAwait(false);
                        //does not need to check stock again
                        baseProduct.StockQuantity -= item.RequestQuantity;
                        _unitOfWork.BaseImportProductRepository.Update(baseProduct);

                        item.ProductReturnId = productReturn.ProductReturnId;
                        item.ImportId = productReturn.ImportId;
                        item.UnitPrice = baseProduct.UnitPrice;
                        item.CostPrice = baseProduct.CostPrice;
                    }
                    await _unitOfWork.BaseProductReturnDetailRepository
                        .AddRangeAsync(productReturnDetails)
                        .ConfigureAwait(false);
                }

                await _unitOfWork.CommitAsync().ConfigureAwait(false);
                transaction.Commit();
                return true;
            }
            catch (Exception)
            {
                transaction.Rollback();
                return false;
            }
            finally
            {
                if (transaction.Connection != null) transaction.Connection.Close();
            }
        }

        public async Task<ViewPaging<GetStockQuantityResponseDto>> GetAllStockQuantity(GetStockQuantityRequestDto entity, string currentUser)
        {
            try
            {
                IQueryable<Object> data = null;

                if (entity.RoleId == CommonConstant.BaseWarehouseManager
                    || entity.RoleId == CommonConstant.BaseWarehouseEmployee)
                {
                    data = _unitOfWork.ProductRepository.SearchStockQuantityForBase(entity.SearchWord, entity.CategoryId);
                }

                if (entity.RoleId == CommonConstant.BranchWarehouseDirector
                    || entity.RoleId == CommonConstant.BranchWarehouseManager
                    || entity.RoleId == CommonConstant.BranchWarehouseEmployee)
                {
                    var getUserId = await _unitOfWork.UserRepository.GetUserWithRoleByUsername(currentUser, 1).FirstOrDefaultAsync().ConfigureAwait(false);

                    var branchWarehouse = await _unitOfWork.BranchWarehouseUserRepository.GetBranchWarehouseByUserId(getUserId.UserId)
                        .FirstOrDefaultAsync();

                    data = _unitOfWork.ProductRepository.SearchStockQuantityForBranch(entity.SearchWord, branchWarehouse.BranchWarehouseId, entity.CategoryId);
                }

                var castList = data.AsEnumerable().Select(item =>
                {
                    var dto = new GetStockQuantityResponseDto();
                    if (item != null)
                    {
                        dto.ProductId = (Guid)item.GetType().GetProperty("ProductId").GetValue(item, null);
                        dto.ImportId = (Guid)item.GetType().GetProperty("ImportId").GetValue(item, null);
                        dto.SKU = (string)item.GetType().GetProperty("SKU").GetValue(item, null);
                        dto.CategoryName = (string)item.GetType().GetProperty("CategoryName").GetValue(item, null);
                        dto.ProductName = (string)item.GetType().GetProperty("ProductName").GetValue(item, null);
                        dto.ProductImage = (string)item.GetType().GetProperty("ProductImage").GetValue(item, null);
                        dto.UnitPrice = (decimal)item.GetType().GetProperty("UnitPrice").GetValue(item, null);
                        dto.InventoryNumber = (int)item.GetType().GetProperty("InventoryNumber").GetValue(item, null);
                    }
                    return dto;
                }).ToList();

                var pagingList = castList
                .Skip(entity.PagingRequest.PageSize * (entity.PagingRequest.CurrentPage - 1))
                    .Take(entity.PagingRequest.PageSize)
                    .ToList();


                var pagination = new Pagination(castList.Count, entity.PagingRequest.CurrentPage,
                    entity.PagingRequest.PageRange, entity.PagingRequest.PageSize);


                var result = _mapper.Map<IEnumerable<GetStockQuantityResponseDto>>(pagingList);

                foreach (var item in result)
                {
                    item.ProductImage = _s3StorageService.GetFileUrl(new S3RequestData() { Name = item.ProductImage });
                }

                return new ViewPaging<GetStockQuantityResponseDto>(result, pagination);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return null;
            }
        }

        public async Task<List<GetAllImportsByIdResponseDto>> GetAllStockQuantityProductId(GetStockQuantityByIdRequestDto entity, string currentUser)
        {
            try
            {
                IQueryable<Object> data = null;

                if (entity.RoleId == CommonConstant.BaseWarehouseManager
                    || entity.RoleId == CommonConstant.BaseWarehouseEmployee)
                {
                    data = _unitOfWork.ProductRepository.GetAllStockQuantityProductIdForBase(entity.ProductId);
                }

                if (entity.RoleId == CommonConstant.BranchWarehouseDirector
                    || entity.RoleId == CommonConstant.BranchWarehouseManager
                    || entity.RoleId == CommonConstant.BranchWarehouseEmployee)
                {
                    var getUserId = await _unitOfWork.UserRepository.GetUserWithRoleByUsername(currentUser, 1).FirstOrDefaultAsync().ConfigureAwait(false);
                    var branchWarehouse = await _unitOfWork.BranchWarehouseUserRepository.GetBranchWarehouseByUserId(getUserId.UserId)
                        .FirstOrDefaultAsync();

                    data = _unitOfWork.ProductRepository.GetAllStockQuantityProductIdForBranch(entity.ProductId, branchWarehouse.BranchWarehouseId);
                }

                var result = _mapper.Map<List<GetAllImportsByIdResponseDto>>(await data.ToListAsync().ConfigureAwait(false));
                var groupdByResult = result
                                    .GroupBy(b => b.ImportId).Select(b => new GetAllImportsByIdResponseDto()
                                    {
                                        BaseImportCode = b.First().BaseImportCode,
                                        ImportCode = b.First().ImportCode,
                                        ImportedQuantity = b.Sum(x => x.ImportedQuantity),
                                        ImportId = b.First().ImportId,
                                        ProductId = b.First().ProductId,
                                        StockQuantity = b.Sum(x => x.StockQuantity)
                                    }).ToList();

                return groupdByResult;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Update Status Product Return From Branch
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> UpdateBranchReturn(UpdateStatusProductReturnFromBranchRequestDto entity)
        {
            if (entity.Status != ProductReturnStatusConstant.Refused && entity.Status != ProductReturnStatusConstant.Returned) return false;
            using var transaction = _unitOfWork.BeginTransaction();
            try
            {
                //Find return info by return id
                var branchReturn = await _unitOfWork.BranchProductReturnRepository
                    .FindObject(x => x.ProductReturnId.Equals(entity.ReturnId))
                    .ConfigureAwait(false);
                if (branchReturn is null || branchReturn.Status == ProductReturnStatusConstant.Returned || branchReturn.Status == ProductReturnStatusConstant.Refused) return false;

                var branchImport = await _unitOfWork.BranchImportRepository.FindObject(x => x.ImportId.Equals(branchReturn.ImportId)).ConfigureAwait(false);
                if (branchImport is null) return false;

                var warehouse = await _unitOfWork.BranchWarehouseRepository.GetById(branchImport.WarehouseId).ConfigureAwait(false);
                if (warehouse is null) return false;

                //Update return status
                if (entity.Status == ProductReturnStatusConstant.Refused)
                {
                    branchReturn.Status = entity.Status;
                    _unitOfWork.BranchProductReturnRepository.Update(branchReturn);

                    //update stock
                    var returnDetails = await _unitOfWork.BranchProductReturnDetailRepository
                        .FindMultiple(x => x.ProductReturnId.Equals(branchReturn.ProductReturnId)).ConfigureAwait(false);
                    foreach (var returnDetail in returnDetails)
                    {
                        var branchStock = await _unitOfWork.BranchImportProductRepository
                            .FindObject(x => x.BaseImportId.Equals(returnDetail.BaseImportId) && x.ProductId.Equals(returnDetail.ProductId)
                            && x.ImportId.Equals(returnDetail.ImportId))
                            .ConfigureAwait(false);
                        branchStock.StockQuantity += returnDetail.RequestQuantity;
                        _unitOfWork.BranchImportProductRepository.Update(branchStock);
                    }
                    await _unitOfWork.CommitAsync()
                        .ConfigureAwait(false);
                    transaction.Commit();
                    return true;
                }
                if (entity.Status == ProductReturnStatusConstant.Returned)
                {
                    branchReturn.Status = entity.Status;

                    //calculate payment
                    var branchDebt = await _unitOfWork.BranchDebtRepository.FindObject(x => x.DebtorId == branchImport.WarehouseId)
                        .ConfigureAwait(false);
                    if (branchDebt is null) return false;

                    //if the import debt is more than the return price, then pay for the return by decreasing the import debt
                    var importDebt = branchImport.TotalCost - branchImport.PaidAmount;
                    if (importDebt >= branchReturn.TotalPrice)
                    {
                        //update return
                        branchReturn.PaidAmount = branchReturn.TotalPrice;
                        branchReturn.PaymentStatus = PaymentStatusConstant.Paid;
                        _unitOfWork.BranchProductReturnRepository.Update(branchReturn);

                        //create invoice for return
                        var branchInvoice = new WmsBranchInvoice()
                        {
                            DebtId = branchDebt.DebtId,
                            Type = InvoiceTypeConstant.PaymentVoucher,
                            Amount = branchReturn.TotalPrice,
                            Description = $"Trả hàng trị giá {branchReturn.TotalPrice} VND cho đơn trả hàng tới kho tổng",
                            ReturnId = branchReturn.ProductReturnId,
                            BalanceChange = branchDebt.DebtAmount - branchReturn.TotalPrice,
                            InvoiceCode = StringHelper.GenerateSpecifyId(50, "BRINV"),
                        };
                        await _unitOfWork.BranchInvoiceRepository.AddAsync(branchInvoice).ConfigureAwait(false);

                        var baseInvoice = new WmsBaseInvoice()
                        {
                            BranchDebtId = branchDebt.DebtId,
                            Type = InvoiceTypeConstant.ReceiptVoucher,
                            Amount = branchReturn.TotalPrice,
                            Description = $"Nhận hàng trị giá {branchReturn.TotalPrice} VND cho đơn trả hàng từ kho con {warehouse.WarehouseName}",
                            BranchProductReturnId = branchReturn.ProductReturnId,
                            BalanceChange = branchDebt.DebtAmount - branchReturn.TotalPrice,  //this balance change is currently not used for any purpose
                            InvoiceCode = StringHelper.GenerateSpecifyId(50, "BAINV"),
                        };


                        //create invoice for payment
                        var branchInvoice2 = new WmsBranchInvoice()
                        {
                            DebtId = branchDebt.DebtId,
                            Type = InvoiceTypeConstant.ReceiptVoucher,
                            Amount = branchReturn.TotalPrice,
                            Description = $"Nhận {branchReturn.TotalPrice} VND cho đơn trả hàng tới kho tổng",
                            ReturnId = branchReturn.ProductReturnId,
                            BalanceChange = branchDebt.DebtAmount,
                            InvoiceCode = StringHelper.GenerateSpecifyId(50, "BRINV"),
                        };
                        await _unitOfWork.BranchInvoiceRepository.AddAsync(branchInvoice2).ConfigureAwait(false);

                        var baseInvoice2 = new WmsBaseInvoice()
                        {
                            BranchDebtId = branchDebt.DebtId,
                            Type = InvoiceTypeConstant.PaymentVoucher,
                            Amount = branchReturn.TotalPrice,
                            Description = $"Trả {branchReturn.TotalPrice} VND cho đơn trả hàng từ kho con {warehouse.WarehouseName}",
                            BranchProductReturnId = branchReturn.ProductReturnId,
                            BalanceChange = branchDebt.DebtAmount,  //this balance change is currently not used for any purpose
                            InvoiceCode = StringHelper.GenerateSpecifyId(50, "BAINV"),
                        };
                        await _unitOfWork.BaseInvoiceRepository.AddAsync(baseInvoice2).ConfigureAwait(false);


                        //increase paid amount
                        branchImport.PaidAmount += branchReturn.TotalPrice;
                        if (branchImport.PaidAmount == branchImport.TotalCost) branchImport.PaymentStatus = PaymentStatusConstant.Paid;
                        else branchImport.PaymentStatus = PaymentStatusConstant.PartlyPaid;
                        _unitOfWork.BranchImportRepository.Update(branchImport);

                        var baseExport = await _unitOfWork.BaseExportRepository
                            .FindObject(x => x.ExportId.Equals(branchImport.BaseExportId)).ConfigureAwait(false);
                        if (baseExport is null) return false;
                        baseExport.PaidAmount += branchReturn.TotalPrice;
                        if (baseExport.PaidAmount == baseExport.TotalCost) baseExport.PaymentStatus = PaymentStatusConstant.Paid;
                        else baseExport.PaymentStatus = PaymentStatusConstant.PartlyPaid;
                        _unitOfWork.BaseExportRepository.Update(baseExport);

                        //create invoice for branch import
                        var branchInvoice3 = new WmsBranchInvoice()
                        {
                            DebtId = branchDebt.DebtId,
                            Type = InvoiceTypeConstant.PaymentVoucher,
                            Amount = branchReturn.TotalPrice,
                            Description = $"Kho con {warehouse.WarehouseName} trả {branchReturn.TotalPrice} VND cho đơn nhập hàng từ kho tổng",
                            ImportId = branchImport.ImportId,
                            BalanceChange = branchDebt.DebtAmount,
                            InvoiceCode = StringHelper.GenerateSpecifyId(50, "BRINV"),
                        };
                        await _unitOfWork.BranchInvoiceRepository.AddAsync(branchInvoice3).ConfigureAwait(false);

                        var baseInvoice3 = new WmsBaseInvoice()
                        {
                            BranchDebtId = branchDebt.DebtId,
                            Type = InvoiceTypeConstant.ReceiptVoucher,
                            Amount = branchReturn.TotalPrice,
                            Description = $"Nhận {branchReturn.TotalPrice} VND cho đơn xuất hàng tới kho con {warehouse.WarehouseName}",
                            ExportId = baseExport.ExportId,
                            BalanceChange = branchDebt.DebtAmount, //this balance change is currently not used for any purpose
                            InvoiceCode = StringHelper.GenerateSpecifyId(50, "BAINV"),
                        };
                        await _unitOfWork.BaseInvoiceRepository.AddAsync(baseInvoice3).ConfigureAwait(false);
                    }
                    else
                    {
                        //update return
                        branchReturn.PaidAmount = 0;
                        branchReturn.PaymentStatus = PaymentStatusConstant.NotPaid;
                        _unitOfWork.BranchProductReturnRepository.Update(branchReturn);

                        //create invoices
                        var branchInvoice = new WmsBranchInvoice()
                        {
                            DebtId = branchDebt.DebtId,
                            Type = InvoiceTypeConstant.PaymentVoucher,
                            Amount = branchReturn.TotalPrice,
                            Description = $"Trả hàng trị giá {branchReturn.TotalPrice} VND cho đơn trả hàng tới kho tổng",
                            ReturnId = branchReturn.ProductReturnId,
                            BalanceChange = branchDebt.DebtAmount - branchReturn.TotalPrice,
                            InvoiceCode = StringHelper.GenerateSpecifyId(50, "BRINV"),
                        };
                        await _unitOfWork.BranchInvoiceRepository.AddAsync(branchInvoice).ConfigureAwait(false);

                        var baseInvoice = new WmsBaseInvoice()
                        {
                            BranchDebtId = branchDebt.DebtId,
                            Type = InvoiceTypeConstant.ReceiptVoucher,
                            Amount = branchReturn.TotalPrice,
                            Description = $"Nhận hàng trị giá {branchReturn.TotalPrice} VND cho đơn trả hàng từ kho con {warehouse.WarehouseName}",
                            BranchProductReturnId = branchReturn.ProductReturnId,
                            BalanceChange = branchDebt.DebtAmount - branchReturn.TotalPrice,  //this balance change is not used for any purpose
                            InvoiceCode = StringHelper.GenerateSpecifyId(50, "BAINV"),
                        };
                        await _unitOfWork.BaseInvoiceRepository.AddAsync(baseInvoice).ConfigureAwait(false);
                    }

                    //update debt
                    branchDebt.DebtAmount -= branchReturn.TotalPrice;
                    _unitOfWork.BranchDebtRepository.Update(branchDebt);
                    await _unitOfWork.CommitAsync().ConfigureAwait(false);

                    //update stock
                    var returnDetails = await _unitOfWork.BranchProductReturnDetailRepository
                        .FindMultiple(x => x.ProductReturnId.Equals(branchReturn.ProductReturnId)).ConfigureAwait(false);
                    foreach (var returnDetail in returnDetails)
                    {
                        var baseStock = await _unitOfWork.BaseImportProductRepository
                            .FindObject(x => x.ImportId.Equals(returnDetail.BaseImportId) && x.ProductId.Equals(returnDetail.ProductId))
                            .ConfigureAwait(false);
                        baseStock.StockQuantity += returnDetail.RequestQuantity;
                        _unitOfWork.BaseImportProductRepository.Update(baseStock);
                    }

                    await _unitOfWork.CommitAsync().ConfigureAwait(false);
                    transaction.Commit();
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                transaction.Rollback();
                return false;
            }
        }

        public async Task<bool> CreateBranchReturn(CreateProductReturnFromBranchToBaseRequestDto userRq)
        {
            using var transaction = _unitOfWork.BeginTransaction();
            try
            {
                //Find import by id
                var branchImport = await _unitOfWork.BranchImportRepository
                    .FindObject(x => x.ImportId.Equals(userRq.ImportId))
                    .ConfigureAwait(false);
                if (branchImport is null || branchImport.ImportStatus != ImportStatusConstant.Imported)
                    return false;

                var productReturn = _mapper.Map<WmsBranchProductReturn>(userRq);
                productReturn.Status = ProductReturnStatusConstant.Requested;
                productReturn.PaymentStatus = PaymentStatusConstant.NotPaid;
                productReturn.PaidAmount = 0;
                productReturn.BaseExportId = branchImport.BaseExportId;
                productReturn.ReturnCode = StringHelper.GenerateSpecifyId(50, "BRRT");

                //calculate total price
                decimal totalPrice = 0;
                foreach (var product in userRq.ProductRequest)
                {
                    var branchProduct = await _unitOfWork.BranchImportProductRepository
                        .FindObject(x => x.ImportId.Equals(branchImport.ImportId) && x.ProductId.Equals(product.ProductId)
                        && x.ExportCodeItem == product.ExportCodeItem)
                        .ConfigureAwait(false);
                    if (branchProduct is null || product.RequestQuantity > branchProduct.StockQuantity)
                    {
                        return false;
                    }

                    totalPrice += branchProduct.UnitPrice * product.RequestQuantity;
                }
                productReturn.TotalPrice = totalPrice;
                await _unitOfWork.BranchProductReturnRepository.AddAsync(productReturn).ConfigureAwait(false);

                //add detail
                if (userRq.ProductRequest is not null && userRq.ProductRequest.Any())
                {
                    var productReturnDetails = _mapper.Map<List<WmsBranchProductReturnDetail>>(userRq.ProductRequest);
                    foreach (var item in productReturnDetails)
                    {
                        var branchProduct = await _unitOfWork.BranchImportProductRepository
                            .FindObject(x => x.ImportId.Equals(branchImport.ImportId) && x.ProductId.Equals(item.ProductId)
                            && x.ExportCodeItem == item.ExportCodeItem)
                            .ConfigureAwait(false);
                        if (branchProduct is null)
                        {
                            return false;
                        }

                        item.ProductReturnId = productReturn.ProductReturnId;
                        item.ImportId = productReturn.ImportId;
                        item.UnitPrice = branchProduct.UnitPrice;
                        item.CostPrice = branchProduct.CostPrice;
                        item.BaseImportId = branchProduct.BaseImportId;

                        branchProduct.StockQuantity -= item.RequestQuantity;
                        _unitOfWork.BranchImportProductRepository.Update(branchProduct);
                    }

                    await _unitOfWork.BranchProductReturnDetailRepository
                        .AddRangeAsync(productReturnDetails)
                        .ConfigureAwait(false);

                }

                await _unitOfWork.CommitAsync()
                    .ConfigureAwait(false);
                transaction.Commit();
                return true;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                transaction.Rollback();
                return false;
            }
        }

        public async Task<IEnumerable<GetBranchProductInStockByNameResponseDto>> GetBranchProductInStockByName(string searchWord, string currentUser)
        {
            var user = await _unitOfWork.UserRepository.FindObject(x => x.Username == currentUser).ConfigureAwait(false);
            if (user is null) return null;
            var branchWarehouse = await _unitOfWork.BranchWarehouseUserRepository.FindObject(x => x.UserId.Equals(user.UserId)).ConfigureAwait(false);
            if (branchWarehouse is null) return null;

            try
            {
                var result = new List<GetBranchProductInStockByNameResponseDto>();
                var productsList = await _unitOfWork.ProductRepository
                    .GetProductByName(searchWord)
                    .ConfigureAwait(false);
                foreach (var product in productsList)
                {
                    var stockQuantity = _unitOfWork.BranchImportProductRepository
                        .GetBranchQuantityByProductId(product.ProductId, branchWarehouse.BranchWarehouseId);
                    if (stockQuantity > 0)
                    {
                        var branchProduct = await _unitOfWork.BranchWarehouseProductRepository.FindObject(x => x.ProductId.Equals(product.ProductId)
                        && x.WarehouseId.Equals(branchWarehouse.BranchWarehouseId)).ConfigureAwait(false);
                        var productResult = _mapper.Map<GetBranchProductInStockByNameResponseDto>(product);
                        productResult.ProductImageLink = _s3StorageService.GetFileUrl(new S3RequestData() { Name = product.ProductImage });
                        productResult.Quantity = stockQuantity;
                        productResult.ExportPrice = branchProduct.ExportPrice;
                        productResult.CostPrice = branchProduct.CostPrice;
                        result.Add(productResult);
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return null;
            }
        }

        public async Task<ViewPaging<GetProductListResponseDto>> GetBranchWarehouseProducts(string currentUser, GetProductListRequestDto requestDto)
        {
            var defaultPagination = new Pagination(0, 1,
                    requestDto.PagingRequest.PageRange, requestDto.PagingRequest.PageSize);
            var defaultPaging = new ViewPaging<GetProductListResponseDto>(Enumerable.Empty<GetProductListResponseDto>(), defaultPagination);

            try
            {
                var warehouseId = await _warehouseService.GetWarehouseIdByUsername(currentUser).ConfigureAwait(false);
                if (warehouseId == Guid.Empty) return defaultPaging;

                var warehouseProducts = await _unitOfWork.BranchWarehouseProductRepository
                    .FindMultiple(x => x.WarehouseId.Equals(warehouseId))
                    .ConfigureAwait(false);
                if (warehouseProducts == null || !warehouseProducts.Any()) return defaultPaging;


                var queryData = await _unitOfWork.BranchWarehouseProductRepository
                    .GetProductListPaging(warehouseId,
                                          requestDto.CategoryId,
                                          requestDto.SearchWord,
                                          (requestDto.PagingRequest.CurrentPage - 1) * requestDto.PagingRequest.PageSize,
                                           requestDto.PagingRequest.PageSize)
                    .ConfigureAwait(false);

                var pagination = new Pagination(queryData.Count(), requestDto.PagingRequest.CurrentPage, requestDto.PagingRequest.PageRange, requestDto.PagingRequest.PageSize);

                var mappedData = _mapper.Map<List<GetProductListResponseDto>>(queryData);

                foreach (var item in mappedData)
                {
                    //var branchProductInformation = warehouseProducts.FirstOrDefault(wp => wp.ProductId.Equals(item.ProductId));
                    //item.UnitPrice = item.ExportPrice;
                    //item.ExportPrice = branchProductInformation.ExportPrice;
                    //item.CostPrice = branchProductInformation.CostPrice;
                    item.ProductImage = _s3StorageService.GetFileUrl(new S3RequestData() { Name = item.ProductImage });
                }

                return new ViewPaging<GetProductListResponseDto>(mappedData, pagination);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return null;
            }
        }

        public async Task<GetProductDetailResponseDto> GetBranchWarehouseProductDetails(string currentUser, Guid productId)
        {
            var warehouseId = await _warehouseService.GetWarehouseIdByUsername(currentUser).ConfigureAwait(false);
            if (warehouseId == Guid.Empty) return null;

            var warehouseProduct = await _unitOfWork.BranchWarehouseProductRepository
                .FindObject(x => x.WarehouseId.Equals(warehouseId) && x.ProductId.Equals(productId))
                .ConfigureAwait(false);
            if (warehouseProduct == null) return null;

            var product = await _unitOfWork.ProductRepository
                    .GetById(productId)
                    .ConfigureAwait(false);
            if (product is null) return null;

            product.UnitPrice = product.ExportPrice;
            product.ExportPrice = warehouseProduct.ExportPrice;
            product.CostPrice = warehouseProduct.CostPrice;
            product.ProductImage = _s3StorageService.GetFileUrl(new S3RequestData() { Name = product.ProductImage });
            var mappedProduct = _mapper.Map<GetProductDetailResponseDto>(product);
            return mappedProduct;
        }

        public async Task<bool> UpdateBranchWarehouseProduct(string currentUser, Guid productId, decimal exportPrice)
        {
            var warehouseId = await _warehouseService.GetWarehouseIdByUsername(currentUser).ConfigureAwait(false);
            if (warehouseId == Guid.Empty) return false;

            var warehouseProduct = await _unitOfWork.BranchWarehouseProductRepository
                .FindObject(x => x.WarehouseId.Equals(warehouseId) && x.ProductId.Equals(productId))
                .ConfigureAwait(false);
            if (warehouseProduct == null) return false;

            using var transaction = _unitOfWork.BeginTransaction();
            try
            {
                warehouseProduct.ExportPrice = exportPrice;
                _unitOfWork.BranchWarehouseProductRepository.Update(warehouseProduct);

                await _unitOfWork.CommitAsync().ConfigureAwait(false);
                transaction.Commit();
                _logger.LogInformation($"Update product successfully!!!");
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return false;
            }
            finally
            {
                if (transaction.Connection != null) transaction.Connection.Close();
            }
        }
    }
}
