﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using WMS.Business.Abstractions.Service;
using WMS.Business.Dtos.CommonDto;
using WMS.Business.Dtos.EmployeeDto;
using WMS.Business.Dtos.S3Dto;
using WMS.Domain.Abstractions;
using WMS.Domain.Constant;
using WMS.Domain.Entities;
using WMS.Domain.Enums;

namespace WMS.Business.Services
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<EmployeeService> _logger;
        private readonly IMapper _mapper;
        private readonly IUserService _userService;
        private readonly IS3StorageService _storageService;

        public EmployeeService(IUnitOfWork unitOfWork,
            ILogger<EmployeeService> logger,
            IMapper mapper,
            IUserService userService,
            IS3StorageService storageService)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
            _mapper = mapper;
            _userService = userService;
            _storageService = storageService;
        }
        public async Task<ViewPaging<ViewEmployeesListResponseDto>> ViewEmployeesList(ViewEmployeesListRequestDto entity, string currentUser)
        {
            try
            {
                if (entity.PagingRequest.PageSize <= 0) entity.PagingRequest.PageSize = 10;
                if (entity.PagingRequest.CurrentPage <= 0) entity.PagingRequest.CurrentPage = 1;

                var managerType = await CheckManagerType(currentUser).ConfigureAwait(false);
                if (managerType == 0) return null;

                //find list of employees in the warehouse
                var list = _unitOfWork.UserRepository
                    .SearchEmployeesList(entity.SearchWord, entity.Status, managerType, currentUser);
                var filteredList = await list
                    .Skip(entity.PagingRequest.PageSize * (entity.PagingRequest.CurrentPage - 1))
                    .Take(entity.PagingRequest.PageSize)
                    .ToListAsync()
                    .ConfigureAwait(false);

                var pagination = new Pagination(list.Count(), entity.PagingRequest.CurrentPage, entity.PagingRequest.PageRange, entity.PagingRequest.PageSize);

                var result = _mapper.Map<IEnumerable<ViewEmployeesListResponseDto>>(filteredList);

                //get image for each record
                foreach (var item in result)
                {
                    item.AvatarImage = _storageService.GetFileUrl(new S3RequestData()
                    {
                        Name = $"{item.Avatar}",
                    });
                }

                return new ViewPaging<ViewEmployeesListResponseDto>(result, pagination); ;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return null;
            }
        }

        public async Task<ViewEmployeeDetailsResponseDto> ViewEmployeeDetails(Guid employeeId, string currentUser)
        {
            try
            {
                //check role of this employee
                var user = await _unitOfWork.UserRepository.GetUserWithRoleByUserid(employeeId, null)
                    .FirstOrDefaultAsync()
                    .ConfigureAwait(false);
                if (user == null || user.UserRole == null) return null;

                //check if the current manager can view
                var managerType = await CheckManagerType(currentUser).ConfigureAwait(false);
                if (managerType == 0) return null;

                var manager = await _unitOfWork.UserRepository.FindObject(x => x.Username == currentUser).ConfigureAwait(false);

                var checkEmployeeInManagerWarehouse = await CheckEmployeeInManagerWarehouse(managerType, manager.UserId, user).ConfigureAwait(false);
                if (!checkEmployeeInManagerWarehouse) return null;

                //return the mapped result
                var result = _mapper.Map<ViewEmployeeDetailsResponseDto>(user);
                result.AvatarImage = _storageService.GetFileUrl(new S3RequestData()
                {
                    Name = $"{result.Avatar}",
                });
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return null;
            }
        }

        public async Task<bool> CreateEmployee(CreateEmployeeRequestDto entity, string currentUser)
        {
            if (entity.CreateUser.RoleId != CommonConstant.BaseWarehouseEmployee &&
            entity.CreateUser.RoleId != CommonConstant.BranchWarehouseEmployee &&
            entity.CreateUser.RoleId != CommonConstant.BranchWarehouseManager)
                return false;

            //check role of user
            var managerType = await CheckManagerType(currentUser).ConfigureAwait(false);
            if (managerType == 0) return false;
            var manager = await _unitOfWork.UserRepository.FindObject(x => x.Username == currentUser).ConfigureAwait(false);

            //if current user is a not a base manager, he cant create base employee
            if (managerType != CommonConstant.BaseWarehouseManager
                && entity.CreateUser.RoleId == CommonConstant.BaseWarehouseEmployee)
                return false;
            //if current user is a not a branch director, he cant create branch employee and manager
            if (managerType != CommonConstant.BranchWarehouseDirector
                && (entity.CreateUser.RoleId == CommonConstant.BranchWarehouseManager
                || entity.CreateUser.RoleId == CommonConstant.BranchWarehouseEmployee))
                return false;

            //add user warehouse
            if (managerType == CommonConstant.BaseWarehouseManager)
            {
                var warehouse = await _unitOfWork.BaseWarehouseRepository
                    .FindObject(x => x.WarehouseOwner.Equals(manager.UserId))
                    .ConfigureAwait(false);
                if (warehouse == null) return false;
                entity.CreateUser.WarehouseId = warehouse.WarehouseId;
            }

            if (managerType == CommonConstant.BranchWarehouseDirector)
            {
                var warehouse = await _unitOfWork.BranchWarehouseRepository
                    .FindObject(x => x.WarehouseOwner.Equals(manager.UserId))
                    .ConfigureAwait(false);
                if (warehouse == null) return false;
                entity.CreateUser.WarehouseId = warehouse.WarehouseId;
            }

            var userId = await _userService.CreateAccount(entity.CreateUser).ConfigureAwait(false);
            if (userId == null) return false;
            return true;
        }

        public async Task<bool> UpdateEmployeeStatus(UpdateEmployeeStatusRequestDto entity, string currentUser)
        {
            if (entity.Status != (short)UserStatusEnum.Active && entity.Status != (short)UserStatusEnum.Disable) return false;
            using var transaction = _unitOfWork.BeginTransaction();
            try
            {
                //check role of user
                var managerType = await CheckManagerType(currentUser).ConfigureAwait(false);
                if (managerType == 0) return false;
                var manager = await _unitOfWork.UserRepository.FindObject(x => x.Username == currentUser).ConfigureAwait(false);

                var employee = await _unitOfWork.UserRepository.GetUserWithRoleByUserid(entity.EmployeeId, null)
                    .FirstOrDefaultAsync()
                    .ConfigureAwait(false);
                if (employee == null) return false;

                var checkEmployeeInManagerWarehouse = await CheckEmployeeInManagerWarehouse(managerType, manager.UserId, employee).ConfigureAwait(false);
                if (!checkEmployeeInManagerWarehouse) return false;

                //update status
                employee.Status = entity.Status;
                _unitOfWork.UserRepository.Update(employee);

                await _unitOfWork.CommitAsync().ConfigureAwait(false);
                transaction.Commit();
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                transaction.Rollback();
                return false;
            }
            finally
            {
                if (transaction.Connection != null) transaction.Connection.Close();
            }
        }

        public async Task<int> CheckManagerType(string currentUser)
        {
            try
            {
                //check if current user is branch director or base manager
                var user = await _unitOfWork.UserRepository.GetUserWithRoleByUsername(currentUser, (short)UserStatusEnum.Active)
                    .FirstOrDefaultAsync()
                    .ConfigureAwait(false);
                if (user == null || user.UserRole == null) return 0;

                int type = 0;
                var isBaseManager = (user.UserRole.FirstOrDefault(ur => ur.RoleId.Equals(CommonConstant.BaseWarehouseManager)) != null);
                var isBranchDirector = user.UserRole.FirstOrDefault(ur => ur.RoleId.Equals(CommonConstant.BranchWarehouseDirector)) != null;
                if (isBaseManager && !isBranchDirector) type = CommonConstant.BaseWarehouseManager;
                if (!isBaseManager && isBranchDirector) type = CommonConstant.BranchWarehouseDirector;
                return type;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public async Task<bool> CheckEmployeeInManagerWarehouse(int managerType, Guid managerId, WmsUser userWithRole)
        {
            if (managerType != CommonConstant.BaseWarehouseManager && managerType != CommonConstant.BranchWarehouseDirector) return false;
            if (managerType == CommonConstant.BaseWarehouseManager)
            {
                if (userWithRole.UserRole.FirstOrDefault(ur => ur.RoleId.Equals(CommonConstant.BaseWarehouseEmployee)) == null) return false;

                var warehouse = await _unitOfWork.BaseWarehouseRepository
                    .FindObject(x => x.WarehouseOwner.Equals(managerId))
                    .ConfigureAwait(false);
                var employee = await _unitOfWork.UserRepository.GetUserWithWarehouseUsersByUserid(userWithRole.UserId, null)
                    .FirstOrDefaultAsync()
                    .ConfigureAwait(false);
                if (employee.WmsBaseWarehouseUsers.Where(x => x.BaseWarehouseId.Equals(warehouse.WarehouseId)) == null
                    || employee.WmsBaseWarehouseUsers.Where(x => x.BaseWarehouseId.Equals(warehouse.WarehouseId)).Count() == 0) return false;
            }

            if (managerType == CommonConstant.BranchWarehouseDirector)
            {
                if (userWithRole.UserRole.FirstOrDefault(ur => ur.RoleId.Equals(CommonConstant.BranchWarehouseEmployee)) == null
                && userWithRole.UserRole.FirstOrDefault(ur => ur.RoleId.Equals(CommonConstant.BranchWarehouseManager)) == null) return false;

                var warehouse = await _unitOfWork.BranchWarehouseRepository
                    .FindObject(x => x.WarehouseOwner.Equals(managerId))
                    .ConfigureAwait(false);
                var employee = await _unitOfWork.UserRepository.GetUserWithWarehouseUsersByUserid(userWithRole.UserId, null)
                    .FirstOrDefaultAsync()
                    .ConfigureAwait(false);
                if (employee.WmsBranchWarehouseUsers.Where(x => x.BranchWarehouseId.Equals(warehouse.WarehouseId)) == null
                    || employee.WmsBranchWarehouseUsers.Where(x => x.BranchWarehouseId.Equals(warehouse.WarehouseId)).Count() == 0) return false;
            }
            return true;
        }

        public async Task<IEnumerable<ViewEmployeesListResponseDto>> GetAllBaseEmployee()
        {
            var list = await _unitOfWork.UserRepository.GetAllBaseEmployee().ToListAsync()
                .ConfigureAwait(false);

            return _mapper.Map<IEnumerable<ViewEmployeesListResponseDto>>(list);
        }

        public async Task<IEnumerable<ViewEmployeesListResponseDto>> GetAllBranchEmployee(string currentUser)
        {
            var user = await _userService.FindUserByUsername(currentUser).ConfigureAwait(false);
            if (user == null) return null;

            var list = _unitOfWork.UserRepository.GetAllBranchEmployee(user.UserId);
            if (list == null) return null;

            return _mapper.Map<IEnumerable<ViewEmployeesListResponseDto>>(await list.ToListAsync().ConfigureAwait(false));
        }
    }
}
