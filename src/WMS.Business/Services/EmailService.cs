﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Net;
using System.Net.Mail;
using WMS.Business.Abstractions.Service;

namespace WMS.Business.Services
{
    public class EmailService : IEmailService
    {
        private readonly ILogger<EmailService> _logger;
        private readonly IConfiguration _configuration;

        public EmailService(ILogger<EmailService> logger,
            IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
        }

        public async Task<bool> SendEmail(string emailTo, string subject, string message)
        {
            try
            {
                //config this in appsettings later
                //account used to log in gmail in chrome
                var emailFrom = _configuration["Email:Account"];
                var appPassword = _configuration["Email:AppPassword"];

                //initiate a new client
                var smtpClient = new SmtpClient("smtp.gmail.com")
                {
                    Port = 587,
                    EnableSsl = true,
                    UseDefaultCredentials = false,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    Credentials = new NetworkCredential(emailFrom, appPassword),
                };

                //write a new email
                var mailMessage = new MailMessage(emailFrom, emailTo, subject, message);
                mailMessage.IsBodyHtml = true;

                await smtpClient.SendMailAsync(mailMessage).ConfigureAwait(false);
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return false;
            }
        }
    }
}
