﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Security.Claims;
using WMS.Business.Abstractions.Service;
using WMS.Business.Dtos.CommonDto;
using WMS.Business.Dtos.S3Dto;
using WMS.Business.Dtos.UserDto;
using WMS.Business.Utils.FileHelper;
using WMS.Business.Utils.StringUtils;
using WMS.Domain.Abstractions;
using WMS.Domain.Constant;
using WMS.Domain.Entities;
using WMS.Domain.Enums;

namespace WMS.Business.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<UserService> _logger;
        private readonly IMapper _mapper;
        private readonly IEmailService _emailService;
        private readonly IS3StorageService _s3StorageService;
        private readonly ITokenService _tokenService;

        public UserService(IUnitOfWork unitOfWork,
            ILogger<UserService> logger,
            IMapper mapper,
            IEmailService emailService,
            IS3StorageService s3StorageService,
            ITokenService tokenService)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
            _mapper = mapper;
            _emailService = emailService;
            _s3StorageService = s3StorageService;
            _tokenService = tokenService;
        }

        public async Task<WmsUser?> AccountLogin(UserLoginRequestDto rqDto)
        {
            try
            {
                WmsUser? user = null;

                if (string.IsNullOrWhiteSpace(rqDto.UserName))
                {
                    return null;
                }

                if (StringHelper.IsValidEmail(rqDto.UserName))
                {
                    user = await FindUserByEmailIncludeNotActive(rqDto.UserName)
                        .ConfigureAwait(false);
                }

                if (StringHelper.IsValidUsername(rqDto.UserName))
                {
                    user = await FindUserByUsernameIncludeNotActive(rqDto.UserName)
                        .ConfigureAwait(false);
                }

                if (user is not null && HashingHelper.VerifyPassword(rqDto.Password, user.HashedPassword, user.SaltKey))
                {
                    //Check accesstoken if not expried, revoke it
                    var token = await _tokenService.GetTokenByUserId(user.UserId);
                    if (token is not null)
                    {
                        _tokenService.RevokeAccessToken(token.CurrentAccessToken);
                    }

                    return user;
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
            }

            return null;
        }

        public Task AccountRegister()
        {
            throw new NotImplementedException();
        }

        public async Task<WmsUser?> FindUserByEmail(string email)
        {
            try
            {
                var user = await _unitOfWork.UserRepository
                    .FindObject(f => f.Email.Equals(email)
                    && f.Status == (short)UserStatusEnum.Active)
                .ConfigureAwait(false);

                _logger.LogInformation($"Find user with email: {email}");

                return user;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return null;
            }

        }

        public async Task<WmsUser?> FindUserById(Guid userId)
        {
            try
            {
                var user = await _unitOfWork.UserRepository
                    .FindObject(f => f.UserId.Equals(userId)
                    && f.Status == (short)UserStatusEnum.Active)
                .ConfigureAwait(false);

                _logger.LogInformation($"Find user with userid: {userId}");

                return user;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return null;
            }
        }

        public async Task<WmsUser?> FindUserByUsername(string username)
        {
            try
            {
                var user = await _unitOfWork.UserRepository
                    .FindObject(f => f.Username.Equals(username)
                && f.Status == (short)UserStatusEnum.Active)
                .ConfigureAwait(false);

                _logger.LogInformation($"Find user with username: {username}");

                return user;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return null;
            }
        }

        public async Task<WmsUser?> FindUserByUsernameIncludeNotActive(string username)
        {
            try
            {
                var user = await _unitOfWork.UserRepository
                    .FindObject(f => f.Username.Equals(username)
               )
                .ConfigureAwait(false);

                _logger.LogInformation($"Find user with username: {username}");

                return user;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return null;
            }
        }

        public async Task<WmsUser?> FindUserByEmailIncludeNotActive(string email)
        {
            try
            {
                var user = await _unitOfWork.UserRepository
                    .FindObject(f => f.Email.Equals(email))
                .ConfigureAwait(false);

                _logger.LogInformation($"Find user with email: {email}");

                return user;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return null;
            }
        }

        //Get profile information by id
        //Input: String username
        //Output: ProfileInformationResponseDTO
        public async Task<ProfileInformationResponseDto?> GetProfileInformationByUsername(string username)
        {
            var user = await FindUserByUsername(username).ConfigureAwait(false);
            //get avatar url from s3
            var url = _s3StorageService.GetFileUrl(new S3RequestData()
            {
                Name = $"{user.Avatar}",
            });
            var profileInformation = _mapper.Map<ProfileInformationResponseDto>(user);
            profileInformation.Avatar = url;
            return profileInformation;
        }

        //Edit profile information by id
        //Input: EditProfileInformationRequestDTO editProfile
        //Output: bool
        public async Task<bool?> EditProfileInformationByUsername(EditProfileInformationRequestDto editProfile, IFormFile avatarImage, string currentUser)
        {
            using var transaction = _unitOfWork.BeginTransaction();
            try
            {
                var user = await FindUserByUsername(currentUser).ConfigureAwait(false);
                if (user is null) return false;
                if (avatarImage != null)
                {
                    //check image 
                    if (FileHelper.CheckFileSignature(avatarImage, ".png") ||
                        FileHelper.CheckFileSignature(avatarImage, ".jpg"))
                    {
                        // Process file
                        await using var item = new MemoryStream();
                        await avatarImage.CopyToAsync(item);
                        //delete old image in s3
                        if (!user.Avatar.Equals(CommonConstant.DefaultMaleAvatar) && !user.Avatar.Equals(CommonConstant.DefaultFemaleAvatar))
                        {
                            await _s3StorageService.DeleteFileAsync(new S3RequestData()
                            {
                                Name = $"{user.Avatar}",
                            });
                        }
                        //Up file to s3
                        var nameAvatar = $"Avatar/{currentUser}_{Guid.NewGuid()}{Path.GetExtension(avatarImage.FileName)}";
                        await _s3StorageService.UploadFileAsync(new S3RequestData()
                        {
                            InputStream = item,
                            Name = nameAvatar
                        });
                        user.Avatar = nameAvatar;
                    }
                }
                //Update user
                user.Fullname = editProfile.Fullname;
                user.Email = editProfile.Email;
                user.Gender = editProfile.Gender;
                user.PhoneNumber = editProfile.PhoneNumber;
                user.Birthdate = DateTime.Parse(editProfile.Birthdate);
                user.Address = editProfile.Address;
                _unitOfWork.UserRepository.Update(user);
                await _unitOfWork.CommitAsync().ConfigureAwait(false);
                transaction.Commit();
                _logger.LogInformation($"User {user.Username} update successfully!!!");
                return true;
            }
            catch (Exception e)
            {
                _logger.LogError($"User update failed!!!", e.Message);
                transaction.Rollback();
                return false;
            }
            finally
            {
                if (transaction.Connection != null) transaction.Connection.Close();
            }
        }

        public HashedPasswordDto GenerateHashedPassword(string originalPassword)
        {
            if (string.IsNullOrWhiteSpace(originalPassword))
            {
                throw new ArgumentException(nameof(originalPassword));
            }

            string saltKey = StringHelper.RandomStringGenerate(32, true, true, true);

            string hashedPassword = HashingHelper.EncryptPassword(originalPassword, saltKey);

            return new HashedPasswordDto(hashedPassword, saltKey);
        }

        public async Task<ViewPaging<ViewAccountsListResponseDto>> ViewAccountsList(ViewAccountsListRequestDto entity)
        {
            try
            {
                if (entity.PagingRequest.PageSize <= 0) entity.PagingRequest.PageSize = 10;
                if (entity.PagingRequest.CurrentPage <= 0) entity.PagingRequest.CurrentPage = 1;
                var fullList = _unitOfWork.UserRepository
                    .SearchAccountsList(entity.SearchWord, entity.Status);
                var filteredList = await fullList
                    .Skip(entity.PagingRequest.PageSize * (entity.PagingRequest.CurrentPage - 1))
                    .Take(entity.PagingRequest.PageSize)
                    .ToListAsync()
                    .ConfigureAwait(false);

                var pagination = new Pagination(fullList.Count(), entity.PagingRequest.CurrentPage,
                    entity.PagingRequest.PageRange, entity.PagingRequest.PageSize);

                var result = _mapper.Map<IEnumerable<ViewAccountsListResponseDto>>(filteredList);
                return new ViewPaging<ViewAccountsListResponseDto>(result, pagination);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return null;
            }
        }

        public async Task<bool> ResetPassword(ResetPasswordRequestDto entity)
        {
            //create new random password for user
            var newPassword = StringHelper.RandomStringGenerate(8, true, true, true);
            var hashedDto = GenerateHashedPassword(newPassword);

            //change password in database
            var user = await _unitOfWork.UserRepository
                    .FindObject(f => f.Username.Equals(entity.Username) && f.Status == (short)UserStatusEnum.Active)
                    .ConfigureAwait(false);
            if (user == null) return false;

            if (user.Email != entity.Email) return false;
            using var transaction = _unitOfWork.BeginTransaction();
            try
            {
                user.HashedPassword = hashedDto.HashedPassword;
                user.SaltKey = hashedDto.SaltKey;
                _unitOfWork.UserRepository.Update(user);
                await _unitOfWork.CommitAsync().ConfigureAwait(false);
                transaction.Commit();
                _logger.LogInformation($"Password for user {user.Username} update successfully!!!");
            }
            catch (Exception ex)
            {
                _logger.LogError($"User password update failed!!!", ex.Message);
                transaction.Rollback();
                return false;
            }
            finally
            {
                if (transaction.Connection != null) transaction.Connection.Close();
            }

            //send email
            var subject = "[WMS] Reset your password";
            var message = $"<h2>Hello {user.Fullname}</h2> " +
                $"It seems like you have forgotten your password. But do not worry! <br>" +
                $"Here is your new password: <span style = 'color:red'>{newPassword}</span> <br>" +
                $"Use this password to log in WMS with your account.";
            var sentEmail = await _emailService.SendEmail(entity.Email, subject, message).ConfigureAwait(false);
            return sentEmail;
        }

        public async Task<bool> ChangePassword(ChangePasswordRequestDto entity)
        {
            var user = await FindUserByUsername(entity.Username).ConfigureAwait(false);
            if (user == null) return false;
            var verifyPass = HashingHelper.VerifyPassword(entity.OldPassword, user.HashedPassword, user.SaltKey);
            if (!verifyPass) return false;

            var checkNewPass = HashingHelper.VerifyPassword(entity.NewPassword, user.HashedPassword, user.SaltKey);
            if (checkNewPass) return false;
            //change password in database
            using var transaction = _unitOfWork.BeginTransaction();
            try
            {
                var hashedDto = GenerateHashedPassword(entity.NewPassword);
                user.SaltKey = hashedDto.SaltKey;
                user.HashedPassword = hashedDto.HashedPassword;
                _unitOfWork.UserRepository.Update(user);
                await _unitOfWork.CommitAsync().ConfigureAwait(false);
                transaction.Commit();
                _logger.LogInformation($"Password for user {user.Username} update successfully!!!");
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError($"User password update failed!!!", ex.Message);
                transaction.Rollback();
                return false;
            }
            finally
            {
                if (transaction.Connection != null) transaction.Connection.Close();
            }
        }

        public async Task<bool> Logout(string username)
        {
            var user = await _unitOfWork.UserRepository
                .FindObject(x => x.Username.Equals(username)).ConfigureAwait(false);
            if (user == null) return false;

            var token = await _unitOfWork.TokenRepository
                .FindObject(x => x.UserId.Equals(user.UserId)).ConfigureAwait(false);
            if (token == null) return false;
            else
            {
                using var transaction = _unitOfWork.BeginTransaction();
                try
                {
                    _unitOfWork.TokenRepository.Delete(token);
                    await _unitOfWork.CommitAsync().ConfigureAwait(false);
                    transaction.Commit();
                    _logger.LogInformation($"Delete token successfully");
                    return true;
                }
                catch (Exception ex)
                {
                    _logger.LogError($"Delete token failed!!!", ex.Message);
                    transaction.Rollback();
                    return false;
                }
                finally
                {
                    if (transaction.Connection != null) transaction.Connection.Close();
                }
            }
        }

        public async Task<Guid> UpdateUserStatus(ChangeUserStatusRequestDto userRq)
        {
            using var transaction = _unitOfWork.BeginTransaction();
            try
            {
                var user = await FindUserByUsernameIncludeNotActive(userRq.Username).ConfigureAwait(false);
                if (user is null)
                    return Guid.Empty;

                _logger.LogInformation("Change user status");
                user.Status = userRq.UserStatus;
                _unitOfWork.UserRepository.Update(user);
                await _unitOfWork.CommitAsync();
                transaction.Commit();
                return user.UserId;
            }
            catch (Exception e)
            {
                _logger.LogError("Something when wrong when change user status", e.Message);
                transaction.Rollback();
                return Guid.Empty;
            }
            finally
            {
                if (transaction.Connection != null) transaction.Connection.Close();
            }
        }

        public async Task<Guid?> CreateAccount(CreateUserAccountRequestDto userRq)
        {
            const string DEFAULT_MALE_IMG = "Avatar/default_male.jpg";
            const string DEFAULT_FEMALE_IMG = "Avatar/default_female.jpg";

            using var transaction = _unitOfWork.BeginTransaction();
            try
            {
                var mappedUser = _mapper.Map<WmsUser>(userRq);

                //Check username, email is unique
                var checkUsername = await FindUserByUsername(mappedUser.Username).ConfigureAwait(false);
                if (checkUsername is not null)
                    throw new ArgumentException("Username must be unique");

                var checkEmail = await FindUserByEmail(mappedUser.Email).ConfigureAwait(false);
                if (checkEmail is not null)
                    throw new ArgumentException("Email must be unique");
                _logger.LogInformation("Check email and username done!");

                //Set default avatar or upload img
                if (userRq.AvatarImage is null || userRq.AvatarImage.Length <= 0)
                {
                    mappedUser.Avatar = mappedUser.Gender ? DEFAULT_FEMALE_IMG : DEFAULT_MALE_IMG;
                }
                else
                {
                    await using var item = new MemoryStream();
                    await userRq.AvatarImage.CopyToAsync(item).ConfigureAwait(false);

                    var nameAvatar = $"Avatar/{userRq.Username}_{Guid.NewGuid()}{Path.GetExtension(userRq.AvatarImage.FileName)}";
                    var uploadSuccess = await _s3StorageService.UploadFileAsync(new S3RequestData()
                    {
                        InputStream = item,
                        Name = nameAvatar
                    });

                    if (!uploadSuccess)
                    {
                        mappedUser.Avatar = mappedUser.Gender ? DEFAULT_FEMALE_IMG : DEFAULT_MALE_IMG;
                    }
                    else
                    {
                        mappedUser.Avatar = nameAvatar;
                        _logger.LogInformation("Upload image success!");
                    }
                }

                //Hashing password with random salt
                string randomPassword = StringHelper.GeneratePassword(8, 2, 2, 2, 2);
                var hashedPasswordInfo = GenerateHashedPassword(randomPassword);
                mappedUser.HashedPassword = hashedPasswordInfo.HashedPassword;
                mappedUser.SaltKey = hashedPasswordInfo.SaltKey;
                _logger.LogInformation("Generate password and hash!");

                //Set account status 'Active'
                mappedUser.Status = (short)UserStatusEnum.Active;
                mappedUser.CreateDate = DateTime.UtcNow;

                await _unitOfWork.UserRepository
                     .AddAsync(mappedUser)
                     .ConfigureAwait(false);

                await _unitOfWork.CommitAsync()
                    .ConfigureAwait(false);

                //Add user role
                var mappedUserRole = _mapper.Map<WmsUserRole>(userRq);
                mappedUserRole.UserId = mappedUser.UserId;
                await _unitOfWork.UserRoleRepository.AddAsync(mappedUserRole);
                _logger.LogInformation("Add user role");

                //Check warehouse is branch or base
                var isBase = await _unitOfWork.BaseWarehouseRepository
                    .FindObject(f => f.WarehouseId.Equals(userRq.WarehouseId))
                    .ConfigureAwait(false);

                var isBranch = await _unitOfWork.BranchWarehouseRepository
                    .FindObject(f => f.WarehouseId.Equals(userRq.WarehouseId))
                    .ConfigureAwait(false);

                if (userRq.RoleId != CommonConstant.Administrator && userRq.WarehouseId != null)
                {
                    //Add user warehouse
                    if (isBase is not null)
                    {
                        var baseUser = new WmsBaseWarehouseUser(mappedUser.UserId, userRq.WarehouseId.Value, false);
                        await _unitOfWork.BaseWarehouseUserRepository
                            .AddAsync(baseUser)
                            .ConfigureAwait(false);
                        _logger.LogInformation("Add base user warehouse");
                    }

                    if (isBranch is not null)
                    {
                        var branchUser = new WmsBranchWarehouseUser(mappedUser.UserId, userRq.WarehouseId.Value, false);
                        await _unitOfWork.BranchWarehouseUserRepository
                            .AddAsync(branchUser)
                            .ConfigureAwait(false);
                        _logger.LogInformation("Add branch user warehouse");
                    }
                }


                if (userRq.RoleId == CommonConstant.BranchWarehouseDirector && isBranch != null)
                {
                    isBranch.WarehouseOwner = mappedUser.UserId;
                    _unitOfWork.BranchWarehouseRepository.Update(isBranch);
                }

                await _unitOfWork.CommitAsync()
                    .ConfigureAwait(false);

                transaction.Commit();
                _logger.LogInformation("Create account success!");

                var subject = "[WMS] Welcome to Warehouse Management System";
                var message = $"<h2>Hello {userRq.Fullname}</h2> " +
                    $"Here is your account: <span style = 'color:red'>{userRq.Username}</span> <br>" +
                    $"Here is your password: <span style = 'color:red'>{randomPassword}</span> <br>" +
                    $"Use this account and password to log in WMS. Welcome on board!";
                var sentEmail = await _emailService.SendEmail(userRq.Email, subject, message).ConfigureAwait(false);
                if (!sentEmail) return null;
                _logger.LogInformation("Send mail success!");
                return mappedUser.UserId;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                return null;
            }
            finally
            {
                if (transaction.Connection != null) transaction.Connection.Close();
            }
        }

        public async Task<IEnumerable<UserRoleResponseDto>> GetUserRoleByUsername(string? username)
        {
            try
            {
                if (username is null || username.Equals("Unauthorized"))
                {
                    return Enumerable.Empty<UserRoleResponseDto>();
                }

                var data = await _unitOfWork.UserRepository
                    .GetUserRoleByUsername(username)
                    .ConfigureAwait(false);

                var final = _mapper.Map<IEnumerable<UserRoleResponseDto>>(data);

                return final;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
            }

            return Enumerable.Empty<UserRoleResponseDto>();
        }

        public async Task<ClaimsIdentity?> GetUserPermissionsIdentity(string username)
        {
            var userPermissions = await _unitOfWork.UserRepository
                .GetUserRoleClaim(username)
                .ConfigureAwait(false);

            return CreatePermissionsIdentity(userPermissions);
        }

        private static ClaimsIdentity? CreatePermissionsIdentity(IReadOnlyCollection<Claim> claimPermissions)
        {
            if (!claimPermissions.Any())
                return null;

            var permissionsIdentity = new ClaimsIdentity(JwtBearerDefaults.AuthenticationScheme);
            permissionsIdentity.AddClaims(claimPermissions);

            return permissionsIdentity;
        }
    }
}
