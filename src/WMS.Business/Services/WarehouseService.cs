﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using WMS.Business.Abstractions.Service;
using WMS.Business.Dtos.BranchWarehouseDto;
using WMS.Business.Dtos.WarehouseDto;
using WMS.Domain.Abstractions;
using WMS.Domain.Constant;

namespace WMS.Business.Services
{
    public class WarehouseService : IWarehouseService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<WarehouseService> _logger;
        private readonly IMapper _mapper;
        private readonly IUserService _userService;

        public WarehouseService(IUnitOfWork unitOfWork,
            ILogger<WarehouseService> logger,
            IMapper mapper,
            IUserService userService)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
            _mapper = mapper;
            _userService = userService;
        }

        public async Task<bool> EditWarehouseProfile(EditWarehouseProfileRequestDto newWarehouseInformation, string username)
        {
            using var transaction = _unitOfWork.BeginTransaction();
            try
            {
                var user = await _unitOfWork.UserRepository.GetUserContainRoleByUsername(username);

                if (CheckUserIsRoleDirector(user, CommonConstant.BaseWarehouseManager))
                {
                    //get base warehouse
                    var baseWarehouse = await _unitOfWork.BaseWarehouseRepository
                        .GetById(newWarehouseInformation.WarehouseId).ConfigureAwait(false);
                    //update 
                    baseWarehouse.WarehouseName = newWarehouseInformation.WarehouseName;
                    baseWarehouse.PhoneNumber = newWarehouseInformation.PhoneNumber;
                    baseWarehouse.Email = newWarehouseInformation.Email;
                    baseWarehouse.Address = newWarehouseInformation.Address;
                    //update to db
                    _unitOfWork.BaseWarehouseRepository.Update(baseWarehouse);
                }
                else if (CheckUserIsRoleDirector(user, CommonConstant.BranchWarehouseDirector))
                {
                    //get base warehouse
                    var branchWarehouse = await _unitOfWork.BranchWarehouseRepository
                        .GetById(newWarehouseInformation.WarehouseId).ConfigureAwait(false);
                    //update 
                    branchWarehouse.WarehouseName = newWarehouseInformation.WarehouseName;
                    branchWarehouse.PhoneNumber = newWarehouseInformation.PhoneNumber;
                    branchWarehouse.Email = newWarehouseInformation.Email;
                    branchWarehouse.Address = newWarehouseInformation.Address;
                    //update to db
                    _unitOfWork.BranchWarehouseRepository.Update(branchWarehouse);
                }
                else
                {
                    _logger.LogError($"Warehouse update failed!!!");
                    transaction.Rollback();
                    return false;
                }
                await _unitOfWork.CommitAsync().ConfigureAwait(false);
                transaction.Commit();
                _logger.LogInformation($"Warehouse update successfully!!!");
                return true;
            }
            catch (Exception e)
            {
                _logger.LogError($"Warehouse update failed!!!", e.Message);
                transaction.Rollback();
                return false;
            }
        }

        /// <summary>
        /// CheckUserIsRoleDirector , condition user name contain list userRole
        /// </summary>
        /// <param name="user"></param>
        /// <param name="roleId"></param>
        /// <returns>bool</returns>
        public async Task<Guid> GetWarehouseIdByUsername(string username)
        {
            int[] mybase = { CommonConstant.BaseWarehouseManager, CommonConstant.BaseWarehouseEmployee };
            try
            {
                var user = await _unitOfWork.UserRepository.GetUserContainRoleByUsername(username).ConfigureAwait(false);
                var userRole = user.UserRole.FirstOrDefault();
                if (mybase.Contains(userRole.RoleId))
                {
                    var warehouseUser = await _unitOfWork.BaseWarehouseUserRepository.FindObject(x => x.UserId.Equals(user.UserId)).ConfigureAwait(false);
                    var warehouse = await _unitOfWork.BaseWarehouseRepository.FindObject(x => x.WarehouseId.Equals(warehouseUser.BaseWarehouseId))
                        .ConfigureAwait(false);
                    return warehouse.WarehouseId;
                }
                else
                {
                    var warehouseUser = await _unitOfWork.BranchWarehouseUserRepository.FindObject(x => x.UserId.Equals(user.UserId)).ConfigureAwait(false);
                    var warehouse = await _unitOfWork.BranchWarehouseRepository.FindObject(x => x.WarehouseId.Equals(warehouseUser.BranchWarehouseId))
                        .ConfigureAwait(false);
                    return warehouse.WarehouseId;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return Guid.Empty;
            }

        }

        /// <summary>
        /// CheckUserIsRoleDirector , condition user name contain list userRole
        /// </summary>
        /// <param name="user"></param>
        /// <param name="roleId"></param>
        /// <returns>bool</returns>
        public bool CheckUserIsRoleDirector(Domain.Entities.WmsUser user, int roleId)
        {
            try
            {
                var result = user.UserRole.FirstOrDefault(ur => ur.RoleId.Equals(roleId));
                if (result == null)
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return false;
            }

        }
        /// <summary>
        /// ViewWarehouseProfile
        /// </summary>
        /// <param name="username"></param>
        /// <returns>ViewWarehouseProfileResponseDto</returns>
        public async Task<ViewWarehouseProfileResponseDto> ViewWarehouseProfile(string username)
        {
            var user = await _unitOfWork.UserRepository.GetUserContainRoleByUsername(username);
            ViewWarehouseProfileResponseDto result = null;
            if (CheckUserIsRoleDirector(user, CommonConstant.BaseWarehouseManager))
            {
                var baseWarehouseInformation = await _unitOfWork.BaseWarehouseRepository
                   .FindBaseWarehouseByWarehouseOwner(user.UserId).ConfigureAwait(false);
                result = _mapper.Map<ViewWarehouseProfileResponseDto>(baseWarehouseInformation);
            }
            if (CheckUserIsRoleDirector(user, CommonConstant.BranchWarehouseDirector))
            {
                var branchWarehouseInformation = await _unitOfWork.BranchWarehouseRepository
                    .FindBranchWarehouseByWarehouseOwner(user.UserId).ConfigureAwait(false);
                result = _mapper.Map<ViewWarehouseProfileResponseDto>(branchWarehouseInformation);
            }
            if (result != null)
            {
                result.NameOfBranchWarehouseDirector = user.Fullname;
            }
            return result;
        }

        public async Task<IEnumerable<ViewBranchWarehousesListResponseDto>> ViewAllBaseWarehousesList()
        {
            var list = await _unitOfWork.BaseWarehouseRepository.GetBaseWarehouseIncludingOwner()
                .ToListAsync().ConfigureAwait(false);
            var result = _mapper.Map<IEnumerable<ViewBranchWarehousesListResponseDto>>(list);
            return result;
        }

        public async Task<ViewBranchWarehousesListResponseDto> GetBranchWarehouseByUsername(string currentUser)
        {
            var warehouse = await _unitOfWork.BranchWarehouseRepository.GetBranchWarehouseByUsername(currentUser)
                .FirstOrDefaultAsync().ConfigureAwait(false);
            return _mapper.Map<ViewBranchWarehousesListResponseDto>(warehouse);
        }

        public async Task<ViewBranchWarehousesListResponseDto> GetBranchWarehouseByCurrentUser(string currentUser)
        {
            var warehouse = await _unitOfWork.BranchWarehouseRepository.GetBranchWarehouseByCurrentUser(currentUser)
                .FirstOrDefaultAsync().ConfigureAwait(false);
            return _mapper.Map<ViewBranchWarehousesListResponseDto>(warehouse);
        }
    }
}
