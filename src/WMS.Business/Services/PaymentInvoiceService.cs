﻿using Microsoft.Extensions.Logging;
using WMS.Business.Abstractions.Service;
using WMS.Business.Dtos.BaseImportDto;
using WMS.Business.Dtos.BranchImportDto;
using WMS.Business.Dtos.InvoiceDto;
using WMS.Business.Dtos.WarehouseDto;
using WMS.Domain.Abstractions;

namespace WMS.Business.Services
{
    public class PaymentInvoiceService : IPaymentInvoiceService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<PaymentInvoiceService> _logger;
        private readonly IBaseImportService _baseImportService;
        private readonly IBaseExportService _baseExportService;
        private readonly IBranchImportService _branchImportService;

        public PaymentInvoiceService(IUnitOfWork unitOfWork,
            ILogger<PaymentInvoiceService> logger,
            IBaseImportService baseImportService,
            IBaseExportService baseExportService,
            IBranchImportService branchImportService)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
            _baseImportService = baseImportService;
            _baseExportService = baseExportService;
            _branchImportService = branchImportService;
        }

        public async Task<bool> CreatePaymentInvoice(CreatePaymentInvoiceRequestDto userRq, string currentUser)
        {
            //Base payment for base import or branch return
            //Base receipt for base export or base return
            //=> Base Export + Base Import
            try
            {
                switch (userRq.Type)
                {
                    //Invoice for base import
                    case 1:
                        return await CreateInvoiceForBaseImport(userRq, currentUser)
                            .ConfigureAwait(false);

                    //Invoice for branch return
                    case 2:
                        return await CreateInvoiceForBranchReturn(userRq, currentUser)
                            .ConfigureAwait(false);

                    default:
                        return false;
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
            }

            return false;
        }

        public async Task<bool> CreateReceiptInvoice(CreateReceiptInvoiceRequestDto userRq, string currentUser)
        {
            try
            {
                switch (userRq.Type)
                {
                    //Create invoice for base return
                    case 1:
                        return await CreateInvoiceForBaseReturn(userRq, currentUser);

                    //Create invoice for base export
                    case 2:
                        return await CreateInvoiceForBaseExport(userRq, currentUser);

                    default:
                        return false;
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
            }

            return false;
        }
        #region Receipt
        public async Task<bool> CreateInvoiceForBaseReturn(CreateReceiptInvoiceRequestDto userRq, string currentUser)
        {
            decimal canPaid = userRq.Amount;
            if (userRq.SupplierId is null) return false;

            //Get current base debt
            var baseDebt = await _unitOfWork.BaseDebtRepository
                .FindObject(x => x.CreditorId == userRq.SupplierId)
                .ConfigureAwait(false);
            if (baseDebt is null || baseDebt.DebtAmount <= 0) return false;

            //Get current base return debt
            var baseReturnDebt = await _unitOfWork.BaseProductReturnRepository
                .GetProductReturnBySupplierId(userRq.SupplierId.Value)
                .ConfigureAwait(false);
            if (!baseReturnDebt.Any()) return false;

            //Calculate total need to pay
            decimal totalNeedToPaid = baseReturnDebt.Sum(x => x.TotalPrice - x.PaidAmount);
            //Total need to paid smaller than total cost user can pay, cancel  
            if (canPaid > totalNeedToPaid)
                throw new ArgumentException("Không thể trả số tiền lớn hơn tổng tiền nợ!");

            foreach (var item in baseReturnDebt)
            {
                if (canPaid <= 0) break;

                decimal needPaid = item.TotalPrice - item.PaidAmount;
                decimal realAmount = 0;

                if (canPaid >= needPaid)
                {
                    realAmount = needPaid;
                }

                if (canPaid < needPaid)
                {
                    realAmount = canPaid;
                }

                var isSuccess = await _baseImportService.PayForBaseProductReturn(new PayForBaseProductReturnRequestDto()
                {
                    BaseProductReturnId = item.ProductReturnId,
                    PayAmount = realAmount,
                }, currentUser).ConfigureAwait(false);

                if (!isSuccess) return false;
                canPaid = canPaid - realAmount;
            }

            return true;
        }

        public async Task<bool> CreateInvoiceForBaseExport(CreateReceiptInvoiceRequestDto userRq, string currentUser)
        {
            decimal canPaid = userRq.Amount;
            if (userRq.BranchId is null) return false;

            //Get current branch debt
            var branchDebt = await _unitOfWork.BranchDebtRepository
                .FindObject(x => x.DebtorId.Equals(userRq.BranchId))
                .ConfigureAwait(false);
            if (branchDebt is null || branchDebt.DebtAmount <= 0) return false;

            //Get current export debt
            var exportDebt = await _unitOfWork.BaseExportRepository
                .GetBaseExportByBranchWarehouseId(userRq.BranchId.Value)
                .ConfigureAwait(false);
            if (!exportDebt.Any()) return false;

            //Calculate total need to pay
            decimal totalNeedToPaid = exportDebt.Sum(x => x.TotalCost - x.PaidAmount);
            //Total need to paid smaller than total cost user can pay, cancel  
            if (canPaid > totalNeedToPaid)
                throw new ArgumentException("Không thể trả số tiền lớn hơn tổng tiền nợ!");

            foreach (var item in exportDebt)
            {
                if (canPaid <= 0) break;

                decimal needPaid = item.TotalCost - item.PaidAmount;
                decimal realAmount = 0;

                if (canPaid >= needPaid)
                {
                    realAmount = needPaid;
                }

                if (canPaid < needPaid)
                {
                    realAmount = canPaid;
                }

                var isSuccess = await _baseExportService.PayForBaseExport(new PayForBaseExportRequestDto()
                {
                    ExportId = item.ExportId,
                    PayAmount = realAmount,
                }, currentUser).ConfigureAwait(false);

                if (!isSuccess) return false;
                canPaid = canPaid - realAmount;
            }
            return true;
        }
        #endregion

        #region Payment
        private async Task<bool> CreateInvoiceForBaseImport(CreatePaymentInvoiceRequestDto userRq, string currentUser)
        {
            decimal canPaid = userRq.Amount;
            if (userRq.SupplierId is null) return false;

            //Get current base debt
            var baseDebt = await _unitOfWork.BaseDebtRepository
                .FindObject(x => x.CreditorId == userRq.SupplierId)
                .ConfigureAwait(false);
            if (baseDebt is null || baseDebt.DebtAmount <= 0) return false;

            //Get current import debt
            var importDebt = await _unitOfWork.BaseImportRepository
                .GetBaseImportBySupplierId(userRq.SupplierId.Value)
                .ConfigureAwait(false);
            if (!importDebt.Any()) return false;

            //Calculate total need to pay
            decimal totalNeedToPaid = importDebt.Sum(x => x.TotalCost - x.PaidAmount);
            //Total need to paid smaller than total cost user can pay, cancel  
            if (canPaid > totalNeedToPaid)
                throw new ArgumentException("Không thể trả số tiền lớn hơn tổng tiền nợ!");

            foreach (var item in importDebt)
            {
                if (canPaid <= 0) break;

                decimal needPaid = item.TotalCost - item.PaidAmount;
                decimal realAmount = 0;

                if (canPaid >= needPaid)
                {
                    realAmount = needPaid;
                }

                if (canPaid < needPaid)
                {
                    realAmount = canPaid;
                }

                var isSuccess = await _baseImportService.PayForBaseImport(new PayForBaseImportRequestDto()
                {
                    ImportId = item.ImportId,
                    PayAmount = realAmount,
                }, currentUser).ConfigureAwait(false);

                if (!isSuccess) return false;
                canPaid = canPaid - realAmount;
            }

            return true;
        }

        private async Task<bool> CreateInvoiceForBranchReturn(CreatePaymentInvoiceRequestDto userRq, string currentUser)
        {
            decimal canPaid = userRq.Amount;
            if (userRq.BranchId is null) return false;

            //Get current branch debt
            var branchDebt = await _unitOfWork.BranchDebtRepository
                .FindObject(x => x.DebtorId.Equals(userRq.BranchId))
                .ConfigureAwait(false);

            if (branchDebt is null || branchDebt.DebtAmount <= 0) return false;

            var branchReturnDebt = await _unitOfWork.BranchProductReturnRepository
                .GetBranchProducReturnByBranchId(userRq.BranchId.Value)
                .ConfigureAwait(false);
            if (!branchReturnDebt.Any()) return false;

            //Calculate total need to pay
            decimal totalNeedToPaid = branchReturnDebt.Sum(x => x.TotalPrice - x.PaidAmount);
            //Total need to paid smaller than total cost user can pay, cancel  
            if (canPaid > totalNeedToPaid)
                throw new ArgumentException("Không thể trả số tiền lớn hơn tổng tiền nợ!");

            foreach (var item in branchReturnDebt)
            {
                if (canPaid <= 0) break;

                decimal needPaid = item.TotalPrice - item.PaidAmount;
                decimal realAmount = 0;

                if (canPaid >= needPaid)
                {
                    realAmount = needPaid;
                }

                if (canPaid < needPaid)
                {
                    realAmount = canPaid;
                }

                var isSuccess = await _branchImportService.PayForBranchProductReturn(new PayForBranchProductReturnRequestDto()
                {
                    BranchProductReturnId = item.ProductReturnId,
                    PayAmount = realAmount,
                }, currentUser).ConfigureAwait(false);

                if (!isSuccess) return false;
                canPaid = canPaid - realAmount;
            }

            return true;
        }
        #endregion
    }
}
