﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using WMS.Business.Abstractions.Service;
using WMS.Business.Dtos.CommonDto;
using WMS.Business.Dtos.S3Dto;
using WMS.Business.Dtos.StockCheckSheetDto;
using WMS.Business.Utils.StringUtils;
using WMS.Domain.Abstractions;
using WMS.Domain.Constant;
using WMS.Domain.Entities;

namespace WMS.Business.Services
{
    public class StockCheckSheetService : IStockCheckSheetService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<StockCheckSheetService> _logger;
        private readonly IS3StorageService _s3StorageService;
        private readonly IMapper _mapper;
        private readonly IDateService dateService;

        public StockCheckSheetService(IUnitOfWork unitOfWork,
                                     IMapper mapper,
                                     IS3StorageService s3StorageService,
                                     ILogger<StockCheckSheetService> logger,
                                     IDateService dateService)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _s3StorageService = s3StorageService;
            _logger = logger;
            this.dateService = dateService;
        }

        public async Task<bool> AddStockCheckSheet(CreateStockCheckSheetRequestDto entity, string currentUser)
        {
            using var transaction = _unitOfWork.BeginTransaction();
            try
            {
                bool isBase = entity.RoleId == CommonConstant.BaseWarehouseManager
                    || entity.RoleId == CommonConstant.BaseWarehouseEmployee;

                bool isBranch = entity.RoleId == CommonConstant.BranchWarehouseDirector
                    || entity.RoleId == CommonConstant.BranchWarehouseManager
                    || entity.RoleId == CommonConstant.BranchWarehouseEmployee;

                if (!isBase && !isBranch)
                {
                    return false;
                }

                var getUserId = await _unitOfWork.UserRepository.GetUserWithRoleByUsername(currentUser, 1).FirstOrDefaultAsync().ConfigureAwait(false);
                if (getUserId == null) return false;

                var addStockCheckSheet = new AddStockCheckSheetRequestDto
                {
                    CheckBy = getUserId.UserId,
                    CheckDate = DateTime.UtcNow,
                    StockCheckSheetCode = StringHelper.GenerateSpecifyId(50, (isBase) ? "BASC" : "BRSC"),
                    Status = 1,
                    Description = entity.Description
                };

                if (isBase)
                {
                    var stockCheckSheet = _mapper.Map<WmsBaseStockCheckSheet>(addStockCheckSheet);
                    await _unitOfWork.BaseStockCheckSheetRepository.AddAsync(stockCheckSheet).ConfigureAwait(false);
                    await _unitOfWork.CommitAsync().ConfigureAwait(false);

                    //var getStockCheckSheet = await _unitOfWork.BaseStockCheckSheetRepository
                    //    .GetStockCheckSheetByCode(stockCheckSheet.StockCheckSheetCode).FirstOrDefaultAsync().ConfigureAwait(false);
                    //var getStockCheckSheetId = getStockCheckSheet!.CheckId;

                    foreach (var item in entity.StockCheckSheetDetails)
                    {
                        item.CheckId = stockCheckSheet.CheckId;
                    }

                    var stockCheckSheetDetails = _mapper.Map<List<WmsBaseStockCheckSheetDetail>>(entity.StockCheckSheetDetails);

                    await _unitOfWork.BaseStockCheckSheetDetailRepository.AddRangeAsync(stockCheckSheetDetails).ConfigureAwait(false);
                    await _unitOfWork.CommitAsync().ConfigureAwait(false);

                    //var stockCheckSheetList = await _unitOfWork.BaseImportProductRepository
                    //    .GetByConditions(stockCheckSheetDetails.FirstOrDefault().ProductId)
                    //    .ToListAsync().ConfigureAwait(false);

                    //foreach (var item1 in entity.StockCheckSheetDetails)
                    //{
                    //    foreach (var item2 in stockCheckSheetList)
                    //    {
                    //        var check = item1.ProductId == item2.ProductId
                    //         && item1.ImportId == item2.ImportId;

                    //        if (check)
                    //        {
                    //            if (item2.StockQuantity != item1.RealQuantity)
                    //            {
                    //                item2.StockQuantity = item1.RealQuantity;
                    //                _unitOfWork.BaseImportProductRepository.Update(item2);
                    //            }
                    //        }
                    //    }
                    //}

                    foreach (var detail in stockCheckSheetDetails)
                    {
                        var baseImportProduct = await _unitOfWork.BaseImportProductRepository
                            .FindObject(x => x.ProductId.Equals(detail.ProductId) && x.ImportId.Equals(detail.ImportId))
                            .ConfigureAwait(false);
                        if (baseImportProduct == null) return false;
                        baseImportProduct.StockQuantity = detail.RealQuantity;
                        _unitOfWork.BaseImportProductRepository.Update(baseImportProduct);
                    }
                    await _unitOfWork.CommitAsync().ConfigureAwait(false);
                }

                if (isBranch)
                {
                    var branchWarehouse = await _unitOfWork.BranchWarehouseUserRepository.GetBranchWarehouseByUserId(getUserId.UserId)
                        .FirstOrDefaultAsync();

                    var stockCheckSheet = _mapper.Map<WmsBranchStockCheckSheet>(addStockCheckSheet);
                    await _unitOfWork.BranchStockCheckSheetRepository.AddAsync(stockCheckSheet).ConfigureAwait(false);
                    await _unitOfWork.CommitAsync().ConfigureAwait(false);

                    //var getStockCheckSheet = await _unitOfWork.BranchStockCheckSheetRepository
                    //   .GetStockCheckSheetByCode(stockCheckSheet.StockCheckSheetCode).FirstOrDefaultAsync().ConfigureAwait(false);
                    //var getStockCheckSheetId = getStockCheckSheet!.CheckId;

                    foreach (var item in entity.StockCheckSheetDetails)
                    {
                        item.CheckId = stockCheckSheet.CheckId;
                    }

                    var stockCheckSheetDetails = _mapper.Map<List<WmsBranchStockCheckSheetDetail>>(entity.StockCheckSheetDetails);

                    await _unitOfWork.BranchStockCheckSheetDetailRepository.AddRangeAsync(stockCheckSheetDetails).ConfigureAwait(false);
                    await _unitOfWork.CommitAsync().ConfigureAwait(false);

                    //var stockCheckSheetList = await _unitOfWork.BranchImportProductRepository
                    //    .GetByConditions(branchWarehouse.BranchWarehouseId, stockCheckSheetDetails.FirstOrDefault().ProductId)
                    //    .ToListAsync().ConfigureAwait(false);

                    //foreach (var item1 in entity.StockCheckSheetDetails)
                    //{
                    //    foreach (var item2 in stockCheckSheetList)
                    //    {
                    //        var check = item1.ProductId == item2.ProductId
                    //         && item1.ImportId == item2.ImportId && item1.BaseImportId == item2.BaseImportId;

                    //        if (check)
                    //        {
                    //            if (item2.StockQuantity != item1.RealQuantity)
                    //            {
                    //                item2.StockQuantity = item1.RealQuantity;
                    //                _unitOfWork.BranchImportProductRepository.Update(item2);
                    //            }
                    //        }
                    //    }
                    //}

                    foreach (var detail in stockCheckSheetDetails)
                    {
                        var branchImportProduct = await _unitOfWork.BranchImportProductRepository
                            .FindObject(x => x.ProductId.Equals(detail.ProductId) && x.ImportId.Equals(detail.ImportId) && x.BaseImportId.Equals(detail.BaseImportId))
                            .ConfigureAwait(false);
                        if (branchImportProduct == null) return false;
                        branchImportProduct.StockQuantity = detail.RealQuantity;
                        _unitOfWork.BranchImportProductRepository.Update(branchImportProduct);
                    }
                    await _unitOfWork.CommitAsync().ConfigureAwait(false);
                }

                transaction.Commit();
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                transaction.Rollback();
                return false;
            }
            finally
            {
                if (transaction.Connection != null) transaction.Connection.Close();
            }
        }

        public async Task<ViewPaging<StockCheckSheetResponseDto>> GetAllStockCheckSheet(StockCheckSheetRequestDto entity, string currentUser)
        {
            try
            {
                var dateFilter = dateService.GetDateTimeByDateFilter(entity.DateFilter);
                IQueryable<Object> search = null;
                if (entity.RoleId == CommonConstant.BaseWarehouseManager
                    || entity.RoleId == CommonConstant.BaseWarehouseEmployee)
                {
                    search = _unitOfWork.BaseStockCheckSheetRepository.SearchBaseStockCheckSheet(entity.SearchWord, dateFilter.StartDate, dateFilter.EndDate);
                }

                if (entity.RoleId == CommonConstant.BranchWarehouseDirector
                    || entity.RoleId == CommonConstant.BranchWarehouseManager
                    || entity.RoleId == CommonConstant.BranchWarehouseEmployee)
                {
                    var getUserId = await _unitOfWork.UserRepository.GetUserWithRoleByUsername(currentUser, 1).FirstOrDefaultAsync().ConfigureAwait(false);
                    var defaultPaging = new Pagination(0, 1,entity.PagingRequest.PageRange, entity.PagingRequest.PageSize);
                    if (getUserId == null) 
                        return new ViewPaging<StockCheckSheetResponseDto>(Enumerable.Empty<StockCheckSheetResponseDto>(), defaultPaging); 

                    var branchWarehouse = await _unitOfWork.BranchWarehouseUserRepository.GetBranchWarehouseByUserId(getUserId.UserId)
                        .FirstOrDefaultAsync();

                    search = _unitOfWork.BranchStockCheckSheetRepository.SearchBranchStockCheckSheet(entity.SearchWord, branchWarehouse!.BranchWarehouseId, dateFilter.StartDate, dateFilter.EndDate);
                }

                var pagingList = await search
                    .Skip(entity.PagingRequest.PageSize * (entity.PagingRequest.CurrentPage - 1))
                    .Take(entity.PagingRequest.PageSize)
                    .ToListAsync();

                var pagination = new Pagination(search.Count(), entity.PagingRequest.CurrentPage,
                    entity.PagingRequest.PageRange, entity.PagingRequest.PageSize);


                var result = _mapper.Map<IEnumerable<StockCheckSheetResponseDto>>(pagingList);

                return new ViewPaging<StockCheckSheetResponseDto>(result, pagination);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return null;
            }

        }

        public async Task<List<StockCheckSheetDetailsResponseDto>> GetAllStockCheckSheetDetails(StockCheckSheetDetailsRequestDto entity, string currentUser)
        {
            try
            {
                IQueryable<Object> list = null;
                if (entity.RoleId == CommonConstant.BaseWarehouseManager
                    || entity.RoleId == CommonConstant.BaseWarehouseEmployee)
                {
                    list = _unitOfWork.BaseStockCheckSheetDetailRepository.GetAllStockCheckSheetDetail(entity.CheckId);
                }

                if (entity.RoleId == CommonConstant.BranchWarehouseDirector
                    || entity.RoleId == CommonConstant.BranchWarehouseManager
                    || entity.RoleId == CommonConstant.BranchWarehouseEmployee)
                {
                    var getUserId = await _unitOfWork.UserRepository.GetUserWithRoleByUsername(currentUser, 1).FirstOrDefaultAsync().ConfigureAwait(false);
                    var branchWarehouse = await _unitOfWork.BranchWarehouseUserRepository.GetBranchWarehouseByUserId(getUserId.UserId)
                        .FirstOrDefaultAsync();

                    list = _unitOfWork.BranchStockCheckSheetDetailRepository.GetAllStockCheckSheetDetail(entity.CheckId, branchWarehouse.BranchWarehouseId);
                }

                var castList = list.AsEnumerable().Select(item =>
                {
                    var dto = new StockCheckSheetDetailsResponseDto();
                    if (item != null)
                    {
                        dto.ProductId = (Guid)item.GetType().GetProperty("ProductId").GetValue(item, null);
                        dto.CheckId = (Guid)item.GetType().GetProperty("CheckId").GetValue(item, null);
                        dto.SKU = (string)item.GetType().GetProperty("SKU").GetValue(item, null);
                        dto.ProductName = (string)item.GetType().GetProperty("ProductName").GetValue(item, null);
                        dto.ProductImage = (string)item.GetType().GetProperty("ProductImage").GetValue(item, null);
                        dto.Quantity = (int)item.GetType().GetProperty("Quantity").GetValue(item, null);
                        dto.RealQuantity = (int)item.GetType().GetProperty("RealQuantity").GetValue(item, null);
                        dto.Difference = (int)item.GetType().GetProperty("Difference").GetValue(item, null);
                    }
                    return dto;
                }).ToList();

                foreach (var item in castList)
                {
                    item.ProductImage = _s3StorageService.GetFileUrl(new S3RequestData() { Name = item.ProductImage });
                }

                return castList;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return null;
            }
        }

        public async Task<List<StockCheckSheetImportsResponseDto>> GetAllStockCheckSheetImports(StockCheckSheetImportsRequestDto entity, string currentUser)
        {
            try
            {
                IQueryable<Object> list = null;
                if (entity.RoleId == CommonConstant.BaseWarehouseManager
                    || entity.RoleId == CommonConstant.BaseWarehouseEmployee)
                {
                    list = _unitOfWork.BaseStockCheckSheetDetailRepository.GetStockCheckSheetImports(entity.CheckId, entity.ProductId);
                }

                if (entity.RoleId == CommonConstant.BranchWarehouseDirector
                    || entity.RoleId == CommonConstant.BranchWarehouseManager
                    || entity.RoleId == CommonConstant.BranchWarehouseEmployee)
                {
                    var getUserId = await _unitOfWork.UserRepository.GetUserWithRoleByUsername(currentUser, 1).FirstOrDefaultAsync().ConfigureAwait(false);
                    var branchWarehouse = await _unitOfWork.BranchWarehouseUserRepository.GetBranchWarehouseByUserId(getUserId.UserId)
                        .FirstOrDefaultAsync();

                    list = _unitOfWork.BranchStockCheckSheetDetailRepository.GetStockCheckSheetImports(entity.CheckId, entity.ProductId, branchWarehouse.BranchWarehouseId);
                }

                var castList = list.AsEnumerable().Select(item =>
                {
                    var dto = new StockCheckSheetImportsResponseDto();
                    if (item != null)
                    {
                        dto.ImportCode = (string)item.GetType().GetProperty("ImportCode").GetValue(item, null);
                        dto.StockQuantity = (int)item.GetType().GetProperty("StockQuantity").GetValue(item, null);
                        dto.RealQuantity = (int)item.GetType().GetProperty("RealQuantity").GetValue(item, null);
                        dto.Difference = (int)item.GetType().GetProperty("Difference").GetValue(item, null);
                        dto.ImportedDate = (DateTime)item.GetType().GetProperty("ImportedDate").GetValue(item, null);
                    }
                    return dto;
                }).ToList();

                return castList;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return null;
            }
        }

        public async Task<List<GetImportsByProductResponseDto>> GetImportsByProductId(StockCheckSheetImportsRequestDto entity, string currentUser)
        {
            try
            {
                bool isBase = entity.RoleId == CommonConstant.BaseWarehouseManager
                    || entity.RoleId == CommonConstant.BaseWarehouseEmployee;

                bool isBranch = entity.RoleId == CommonConstant.BranchWarehouseDirector
                    || entity.RoleId == CommonConstant.BranchWarehouseManager
                    || entity.RoleId == CommonConstant.BranchWarehouseEmployee;

                if (!isBase && !isBranch)
                {
                    return null;
                }

                IQueryable<Object> list = null;
                if (isBase)
                {
                    list = _unitOfWork.BaseStockCheckSheetDetailRepository.GetImportsByProductId(entity.ProductId);
                }

                if (isBranch)
                {
                    var getUserId = await _unitOfWork.UserRepository.GetUserWithRoleByUsername(currentUser, 1).FirstOrDefaultAsync().ConfigureAwait(false);
                    var branchWarehouse = await _unitOfWork.BranchWarehouseUserRepository.GetBranchWarehouseByUserId(getUserId.UserId)
                        .FirstOrDefaultAsync();

                    list = _unitOfWork.BranchStockCheckSheetDetailRepository.GetImportsByProductId(
                        entity.ProductId, branchWarehouse.BranchWarehouseId);
                }

                var castList = list.AsEnumerable().Select(item =>
                {
                    var dto = new GetImportsByProductResponseDto();
                    if (item != null)
                    {
                        dto.ProductId = (Guid)item.GetType().GetProperty("ProductId").GetValue(item, null);
                        dto.SKU = (string)item.GetType().GetProperty("SKU").GetValue(item, null);
                        dto.ProductName = (string)item.GetType().GetProperty("ProductName").GetValue(item, null);
                        dto.ProductImage = (string)item.GetType().GetProperty("ProductImage").GetValue(item, null);
                        dto.ImportId = (Guid)item.GetType().GetProperty("ImportId").GetValue(item, null);
                        dto.ImportCode = (string)item.GetType().GetProperty("ImportCode").GetValue(item, null);
                        dto.StockQuantity = (int)item.GetType().GetProperty("StockQuantity").GetValue(item, null);
                        if (isBranch)
                        {
                            dto.BaseImportId = (Guid)item.GetType().GetProperty("BaseImportId").GetValue(item, null);
                            dto.ExportCodeItem = (string)item.GetType().GetProperty("ExportCodeItem").GetValue(item, null);
                        }
                    }
                    return dto;
                }).ToList();

                foreach (var item in castList)
                {
                    item.ProductImage = _s3StorageService.GetFileUrl(new S3RequestData() { Name = item.ProductImage });
                }

                return castList;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return null;
            }
        }

        public async Task<StockCheckSheetResponseDto> GetStockCheckSheetById(StockCheckSheetDetailsRequestDto entity, string currentUser)
        {
            try
            {
                if (entity.RoleId == CommonConstant.BaseWarehouseManager
                    || entity.RoleId == CommonConstant.BaseWarehouseEmployee)
                {
                    var getStockCheckSheet1 = await _unitOfWork.BaseStockCheckSheetRepository.GetStockCheckSheetById(entity.CheckId).FirstOrDefaultAsync();
                    var result1 = _mapper.Map<StockCheckSheetResponseDto>(getStockCheckSheet1);

                    return result1;
                }

                if (entity.RoleId == CommonConstant.BranchWarehouseDirector
                    || entity.RoleId == CommonConstant.BranchWarehouseManager
                    || entity.RoleId == CommonConstant.BranchWarehouseEmployee)
                {
                    var getUserId = await _unitOfWork.UserRepository.GetUserWithRoleByUsername(currentUser, 1).FirstOrDefaultAsync().ConfigureAwait(false);
                    var branchWarehouse = await _unitOfWork.BranchWarehouseUserRepository.GetBranchWarehouseByUserId(getUserId.UserId)
                        .FirstOrDefaultAsync();

                    var getStockCheckSheet2 = await _unitOfWork.BranchStockCheckSheetRepository.GetStockCheckSheetById(entity.CheckId, branchWarehouse.BranchWarehouseId).FirstOrDefaultAsync();
                    var result2 = _mapper.Map<StockCheckSheetResponseDto>(getStockCheckSheet2);

                    return result2;
                }

                return null;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return null;
            }
        }

    }
}
