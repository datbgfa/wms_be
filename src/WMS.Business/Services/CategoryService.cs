﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using WMS.Business.Abstractions.Service;
using WMS.Business.Dtos.CategoryDto;
using WMS.Business.Dtos.CommonDto;
using WMS.Domain.Abstractions;
using WMS.Domain.Entities;

namespace WMS.Business.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<CategoryService> _logger;
        private readonly IMapper _mapper;

        public CategoryService(IUnitOfWork unitOfWork,
            ILogger<CategoryService> logger,
            IMapper mapper
           )
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
            _mapper = mapper;
        }

        public async Task<ViewPaging<CategoryResponseDto>> GetAllWithPaging(CategoryRequestDto entity)
        {

            var filter = _unitOfWork.CategoryRepository
                .SearchAndFilterCategory(entity.SearchWord, entity.Visible)
                .OrderBy(x => x.CategoryId);

            var pagingList = await filter.Skip(entity.PagingRequest.PageSize * (entity.PagingRequest.CurrentPage - 1))
                .Take(entity.PagingRequest.PageSize).OrderBy(x => x.CategoryId)
                .ToListAsync()
                .ConfigureAwait(false);

            var pagination = new Pagination(await filter.CountAsync(), entity.PagingRequest.CurrentPage,
                entity.PagingRequest.PageRange, entity.PagingRequest.PageSize);

            var result = _mapper.Map<IEnumerable<CategoryResponseDto>>(pagingList);


            return new ViewPaging<CategoryResponseDto>(result, pagination);
        }

        public async Task<bool> AddCategory(string name)
        {
            using var transaction = _unitOfWork.BeginTransaction();
            try
            {
                var category = new UpdateCategoryDto
                {
                    CategoryName = name,
                    Visible = true,
                };

                var checkDuplicate = await CheckDuplicateName(name).ConfigureAwait(false);

                if (checkDuplicate)
                {
                    return false;
                }
                await _unitOfWork.CategoryRepository.AddAsync(_mapper.Map<WmsCategory>(category));
                await _unitOfWork.CommitAsync().ConfigureAwait(false);
                transaction.Commit();
                return true;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                return false;
            }
            finally
            {
                if (transaction.Connection != null) transaction.Connection.Close();
            }
        }

        public async Task<bool> UpdateCategory(UpdateCategoryDto category)
        {
            using var transaction = _unitOfWork.BeginTransaction();
            try
            {
                var currentCategory = await _unitOfWork.CategoryRepository.GetById(category.CategoryId).ConfigureAwait(false);

                if (currentCategory is null)
                {
                    return false;
                }
                var checkDuplicate = await CheckDuplicateNameIgnore(currentCategory, category.CategoryName).ConfigureAwait(false);

                if (checkDuplicate)
                {
                    return false;
                }
                currentCategory.CategoryName = category.CategoryName;
                currentCategory.Visible = category.Visible;

                _unitOfWork.CategoryRepository.Update(currentCategory);
                await _unitOfWork.CommitAsync().ConfigureAwait(false);
                transaction.Commit();
                return true;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                return false;
            }
            finally
            {
                if (transaction.Connection != null) transaction.Connection.Close();
            }
        }

        public async Task<bool> CheckDuplicateNameIgnore(WmsCategory currentCategory, string name)
        {
            if (name.Trim().ToLower().Equals(currentCategory.CategoryName.Trim().ToLower()))
            {
                return false;
            }
            return await CheckDuplicateName(name).ConfigureAwait(false);
        }

        public async Task<bool> CheckDuplicateName(string name)
        {
            var list = await _unitOfWork.CategoryRepository.All().ConfigureAwait(false);
            var categoryExist = list.FirstOrDefault(x => x.CategoryName.Trim().ToLower().Equals(name.Trim().ToLower()));
            if (categoryExist != null)
                return true;


            return false;
        }

        public async Task<IEnumerable<CategoryResponseDto>> GetAll()
        {
            var result = await _unitOfWork.CategoryRepository.FindMultiple(c => c.Visible.Equals(true)).ConfigureAwait(false);
            return _mapper.Map<IEnumerable<CategoryResponseDto>>(result);
        }
    }
}
