﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Globalization;
using WMS.Business.Abstractions.Service;
using WMS.Business.Dtos.CommonDto;
using WMS.Business.Dtos.InvoiceDto;
using WMS.Business.Dtos.S3Dto;
using WMS.Business.Utils.ExtensionMethods;
using WMS.Domain.Abstractions;
using WMS.Domain.Constant;

namespace WMS.Business.Services
{
    public class InvoiceService : IInvoiceService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<InvoiceService> _logger;
        private readonly IMapper _mapper;
        private readonly IUserService _userService;
        private readonly IWarehouseService _warehouseService;
        private readonly IS3StorageService _storageService;

        public InvoiceService(IUnitOfWork unitOfWork,
            ILogger<InvoiceService> logger,
            IMapper mapper,
            IUserService userService,
            IWarehouseService warehouseService,
            IS3StorageService s3StorageService)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
            _mapper = mapper;
            _userService = userService;
            _warehouseService = warehouseService;
            _storageService = s3StorageService;
        }

        public async Task<IEnumerable<InvoiceResponseDto>> GetBaseInvoiceByImportId(Guid importId)
        {
            var baseInvoice = await _unitOfWork.BaseInvoiceRepository.FindMultiple(bi => bi.ImportId == importId && bi.Type.Equals(InvoiceTypeConstant.PaymentVoucher)).ConfigureAwait(false);
            var result = _mapper.Map<IEnumerable<InvoiceResponseDto>>(baseInvoice.OrderByDescending(be => be.CreateDate));
            return result;
        }

        public async Task<IEnumerable<InvoiceResponseDto>> GetBaseInvoiceByExportId(Guid exportId)
        {
            var baseInvoice = await _unitOfWork.BaseInvoiceRepository.FindMultiple(bi => bi.ExportId == exportId).ConfigureAwait(false);
            var result = _mapper.Map<IEnumerable<InvoiceResponseDto>>(baseInvoice.OrderByDescending(be => be.CreateDate));
            return result;
        }

        public async Task<IEnumerable<InvoiceResponseDto>> GetBranchInvoiceByImportId(Guid importId)
        {
            var baseInvoice = await _unitOfWork.BranchInvoiceRepository.FindMultiple(bi => bi.ImportId == importId && bi.Type.Equals(InvoiceTypeConstant.PaymentVoucher) ).ConfigureAwait(false);
            var result = _mapper.Map<IEnumerable<InvoiceResponseDto>>(baseInvoice.OrderByDescending(be => be.CreateDate));
            return result;
        }

        public async Task<IEnumerable<InvoiceResponseDto>> GetBranchInvoiceByExportId(Guid exportId)
        {
            var baseInvoice = await _unitOfWork.BranchInvoiceRepository.FindMultiple(bi => bi.ExportId == exportId).ConfigureAwait(false);
            var result = _mapper.Map<IEnumerable<InvoiceResponseDto>>(baseInvoice.OrderByDescending(be => be.CreateDate));
            return result;
        }

        public async Task<IEnumerable<InvoiceResponseDto>> GetBaseInvoiceByReturnId(IEnumerable<Guid> returnId)
        {
            var baseInvoice = await _unitOfWork.BaseInvoiceRepository.FindMultiple(bi => returnId.Contains(bi.ReturnId.Value)).ConfigureAwait(false);
            var result = _mapper.Map<IEnumerable<InvoiceResponseDto>>(baseInvoice.OrderByDescending(be => be.CreateDate));
            return result;
        }

        public async Task<IEnumerable<InvoiceResponseDto>> GetBranchInvoiceByReturnId(IEnumerable<Guid> returnId)
        {
            var branchInvoice = await _unitOfWork.BranchInvoiceRepository.FindMultiple(bi => returnId.Contains(bi.ReturnId.Value)).ConfigureAwait(false);
            var result = _mapper.Map<IEnumerable<InvoiceResponseDto>>(branchInvoice.OrderByDescending(be => be.CreateDate));
            return result;
        }

        public async Task<IEnumerable<InvoiceResponseDto>> GetBaseInvoiceByBranchProductReturnId(IEnumerable<Guid> branchProductReturnId)
        {
            var branchInvoice = await _unitOfWork.BaseInvoiceRepository.FindMultiple(bi => branchProductReturnId.Contains(bi.BranchProductReturnId.Value)).ConfigureAwait(false);
            var result = _mapper.Map<IEnumerable<InvoiceResponseDto>>(branchInvoice.OrderByDescending(be => be.CreateDate));
            return result;
        }

        /// <summary>
        /// Get Base Invoice
        /// </summary>
        /// <param name="userRq"></param>
        /// <returns></returns>
        public async Task<ViewPaging<InvoiceDataResponseDto>> GetBaseInvoice(InvoiceDataRequestDto userRq)
        {
            try
            {
                int skip = (userRq.CurrentPage - 1) * userRq.PageSize;
                int take = userRq.PageSize;

                //Get invoice data
                var data = await _unitOfWork.BaseInvoiceRepository
                    .GetBaseInvoices(userRq.Type, userRq.From, userRq.To, userRq.InvoiceFor, skip, take)
                    .ToListAsync()
                    .ConfigureAwait(false);
                //Count number of invoice data
                var countInvoice = await _unitOfWork.BaseInvoiceRepository
                    .CountBaseInvoices(userRq.Type, userRq.From, userRq.To, userRq.InvoiceFor)
                    .ConfigureAwait(false);

                //Calculate paging
                var paging = new Pagination(countInvoice, userRq.CurrentPage, userRq.PageRange, take);
                var mapData = _mapper.Map<IEnumerable<InvoiceDataResponseDto>>(data);

                var result = new ViewPaging<InvoiceDataResponseDto>(mapData, paging);
                return result;
            }
            catch (Exception e)
            {
                if (e is ArgumentException) throw;
                _logger.LogError(e.Message);
            }

            var defaultPaging = new Pagination(0, userRq.CurrentPage, userRq.PageRange, userRq.PageSize);
            return new ViewPaging<InvoiceDataResponseDto>(Enumerable.Empty<InvoiceDataResponseDto>(), defaultPaging);
        }

        /// <summary>
        /// Get Branch Invoice
        /// </summary>
        /// <param name="userRq"></param>
        /// <returns></returns>
        public async Task<ViewPaging<InvoiceDataResponseDto>> GetBranchInvoice(InvoiceDataRequestDto userRq, string currentUser)
        {
            var defaultPaging = new Pagination(0, userRq.CurrentPage, userRq.PageRange, userRq.PageSize);
            try
            {
                if (currentUser.Equals("Unauthorized"))
                {
                    return new ViewPaging<InvoiceDataResponseDto>(Enumerable.Empty<InvoiceDataResponseDto>(), defaultPaging);
                }

                var userInfo = await _userService.FindUserByUsername(currentUser)
                    .ConfigureAwait(false);

                if (userInfo is null)
                {
                    return new ViewPaging<InvoiceDataResponseDto>(Enumerable.Empty<InvoiceDataResponseDto>(), defaultPaging);
                }

                var currentUserBranch = await _unitOfWork.BranchWarehouseUserRepository
                    .FindObject(x => x.UserId.Equals(userInfo.UserId))
                    .ConfigureAwait(false);

                if (currentUserBranch is null)
                {
                    return new ViewPaging<InvoiceDataResponseDto>(Enumerable.Empty<InvoiceDataResponseDto>(), defaultPaging);
                }

                int skip = (userRq.CurrentPage - 1) * userRq.PageSize;
                int take = userRq.PageSize;

                //Get invoice data
                var data = await _unitOfWork.BranchInvoiceRepository
                    .GetBranchInvoices(userRq.Type, userRq.From, userRq.To, userRq.InvoiceFor, skip, take, currentUserBranch.BranchWarehouseId)
                    .ToListAsync()
                    .ConfigureAwait(false);
                //Count number of invoice data
                var countInvoice = await _unitOfWork.BranchInvoiceRepository
                    .CountBranchInvoices(userRq.Type, userRq.From, userRq.To, userRq.InvoiceFor, currentUserBranch.BranchWarehouseId)
                    .ConfigureAwait(false);

                //Calculate paging
                var paging = new Pagination(countInvoice, userRq.CurrentPage, userRq.PageRange, take);
                var mapData = _mapper.Map<IEnumerable<InvoiceDataResponseDto>>(data);

                var result = new ViewPaging<InvoiceDataResponseDto>(mapData, paging);
                return result;
            }
            catch (Exception e)
            {
                if (e is ArgumentException) throw;
                _logger.LogError(e.Message);
            }

            return new ViewPaging<InvoiceDataResponseDto>(Enumerable.Empty<InvoiceDataResponseDto>(), defaultPaging);
        }


        public async Task<DashboardResponseDto> ViewDashboard(DateTime? fromRq, DateTime? toRq, string currentUser)
        {
            var role = (await _unitOfWork.UserRepository.GetUserRoleByUsername(currentUser).ConfigureAwait(false)).FirstOrDefault();
            if (role is null) return new DashboardResponseDto();

            DateTime from = fromRq == null ? DateTime.Now.AddYears(-1) : DateTimeExtension.ConvertUTCToLocalTimeZone(fromRq.Value);
            DateTime to = toRq == null ? DateTime.Now : DateTimeExtension.ConvertUTCToLocalTimeZone(toRq.Value);

            if (role.RoleId == CommonConstant.BaseWarehouseManager)
            {
                return await ViewBaseDashboard(from, to).ConfigureAwait(false);
            }

            if (role.RoleId == CommonConstant.BranchWarehouseDirector)
            {
                return await ViewBranchDashboard(from, to, currentUser).ConfigureAwait(false);
            }

            return new DashboardResponseDto();
        }

        public async Task<DashboardResponseDto> ViewBaseDashboard(DateTime from, DateTime to)
        {
            //get imports, exports products
            var totalImports = (await _unitOfWork.BaseImportRepository
                .FindMultiple(x => x.ImportStatus == ImportStatusConstant.Imported
                && x.ImportedDate!.Value.Date >= from.Date && x.ImportedDate!.Value.Date <= to.Date)
                .ConfigureAwait(false)).Count();

            var totalExports = (await _unitOfWork.BaseExportRepository
                .FindMultiple(x => (x.ExportStatus == ExportStatusConstant.Confirmed || x.ExportStatus == ExportStatusConstant.Exported)
                && x.ExportDate!.Value.Date >= from.Date && x.ExportDate!.Value.Date <= to.Date)
                .ConfigureAwait(false)).Count();

            var productsInStock = _unitOfWork.ProductRepository.TotalProductsInStock(0, from, to, null);
            var productsExported = _unitOfWork.ProductRepository.TotalProductsExported(0, from, to, null);

            var groupTrendingProducts = _unitOfWork.ProductRepository.GetBaseTrendingProducts(from, to);
            var list = new List<TrendingProductsResponseDto>();
            foreach (var group in groupTrendingProducts)
            {
                var product = await _unitOfWork.ProductRepository.GetById(group.Key).ConfigureAwait(false);
                var image = _storageService.GetFileUrl(new S3RequestData() { Name = product!.ProductImage });

                list.Add(new TrendingProductsResponseDto()
                {
                    ProductId = group.Key,
                    ProductName = product.ProductName,
                    ImageLink = image,
                    TotalProducts = group.Sum(x => x.Quantity),
                });
            }

            //get debt
            var baseDebt = (await _unitOfWork.BaseDebtRepository.All().ConfigureAwait(false)).Sum(x => x.DebtAmount);
            var branchDebt = (await _unitOfWork.BranchDebtRepository.All().ConfigureAwait(false)).Sum(x => x.DebtAmount);

            //get statistics
            var statistics = new List<StatisticsResponseDto>();
            var temp = from;
            while (temp.Year < to.Year || (temp.Year == to.Year && temp.Month <= to.Month))
            {
                decimal paid = 0;
                decimal received = 0;

                //if from and to is the same month in the same year
                if (from.Year == to.Year && from.Month == to.Month)
                {
                    paid = _unitOfWork.BaseInvoiceRepository
                            .GetMoneyPaid(from, to);
                    received = _unitOfWork.BaseInvoiceRepository
                        .GetMoneyReceived(from, to);
                }
                else
                {
                    //if first month then get from request day to last day of month
                    if (temp.Year == from.Year && temp.Month == from.Month)
                    {
                        paid = _unitOfWork.BaseInvoiceRepository
                            .GetMoneyPaid(temp, new DateTime(temp.Year, temp.Month, DateTime.DaysInMonth(temp.Year, temp.Month)));
                        received = _unitOfWork.BaseInvoiceRepository
                            .GetMoneyReceived(temp, new DateTime(temp.Year, temp.Month, DateTime.DaysInMonth(temp.Year, temp.Month)));
                    }
                    //if last month then get from first day of month to request day
                    else if (temp.Year == to.Year && temp.Month == to.Month)
                    {
                        paid = _unitOfWork.BaseInvoiceRepository
                            .GetMoneyPaid(new DateTime(temp.Year, temp.Month, 1), to);
                        received = _unitOfWork.BaseInvoiceRepository
                            .GetMoneyReceived(new DateTime(temp.Year, temp.Month, 1), to);
                    }
                    //if middle months then get from first day of month to last day of month
                    else
                    {
                        paid = _unitOfWork.BaseInvoiceRepository
                            .GetMoneyPaid(new DateTime(temp.Year, temp.Month, 1), new DateTime(temp.Year, temp.Month, DateTime.DaysInMonth(temp.Year, temp.Month)));
                        received = _unitOfWork.BaseInvoiceRepository
                            .GetMoneyReceived(new DateTime(temp.Year, temp.Month, 1), new DateTime(temp.Year, temp.Month, DateTime.DaysInMonth(temp.Year, temp.Month)));
                    }
                }

                statistics.Add(new StatisticsResponseDto()
                {
                    Month = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(temp.Month),
                    Year = temp.Year,
                    MoneyPaid = paid,
                    MoneyReceived = received,
                });
                temp = temp.AddMonths(1);
            }

            return new DashboardResponseDto()
            {
                TotalImports = totalImports,
                TotalExports = totalExports,
                TotalProductsInStock = productsInStock,
                TotalProductsExport = productsExported,
                TrendingProducts = list,
                TotalBaseDebt = baseDebt,
                TotalBranchDebt = branchDebt,
                Statistics = statistics,
            };
        }

        public async Task<DashboardResponseDto> ViewBranchDashboard(DateTime from, DateTime to, string currentUser)
        {
            var warehouse = await _warehouseService.GetWarehouseIdByUsername(currentUser).ConfigureAwait(false);

            //get imports, exports products
            var totalImports = (await _unitOfWork.BranchImportRepository
                .FindMultiple(x => x.ImportStatus == ImportStatusConstant.Imported
                && x.ImportedDate!.Value.Date >= from.Date && x.ImportedDate!.Value.Date <= to.Date && x.WarehouseId == warehouse)
                .ConfigureAwait(false)).Count();

            var totalExports = (await _unitOfWork.BranchExportRepository
                .FindMultiple(x => (x.ApprovalStatus == ExportStatusConstant.Exported)
                && x.ExportDate >= from && x.ExportDate <= to && x.WarehouseId == warehouse)
                .ConfigureAwait(false)).Count();

            var productsInStock = _unitOfWork.ProductRepository.TotalProductsInStock(1, from, to, warehouse);
            var productsExported = _unitOfWork.ProductRepository.TotalProductsExported(1, from, to, warehouse);

            var groupTrendingProducts = _unitOfWork.ProductRepository.GetBranchTrendingProducts(from, to, warehouse);
            var list = new List<TrendingProductsResponseDto>();
            foreach (var group in groupTrendingProducts)
            {
                var product = await _unitOfWork.ProductRepository.GetById(group.Key).ConfigureAwait(false);
                var image = _storageService.GetFileUrl(new S3RequestData() { Name = product!.ProductImage });

                list.Add(new TrendingProductsResponseDto()
                {
                    ProductId = group.Key,
                    ProductName = product.ProductName,
                    ImageLink = image,
                    TotalProducts = group.Sum(x => x.Quantity)
                });
            }

            //get debt
            var branchDebt = (await _unitOfWork.BranchDebtRepository.FindObject(x => x.DebtorId.Equals(warehouse)).ConfigureAwait(false))!.DebtAmount;

            //get statistics
            var statistics = new List<StatisticsResponseDto>();
            var temp = from;
            while (temp.Year < to.Year || (temp.Year == to.Year && temp.Month <= to.Month))
            {
                decimal paid = 0;
                decimal received = 0;

                //if from and to is the same month in the same year
                if (from.Year == to.Year && from.Month == to.Month)
                {
                    paid = _unitOfWork.BranchInvoiceRepository
                            .GetMoneyPaid(from, to, warehouse);
                    received = _unitOfWork.BranchInvoiceRepository
                        .GetMoneyReceived(from, to, warehouse);
                }
                else
                {
                    //if first month then get from request day to last day of month
                    if (temp.Year == from.Year && temp.Month == from.Month)
                    {
                        paid = _unitOfWork.BranchInvoiceRepository
                            .GetMoneyPaid(temp, new DateTime(temp.Year, temp.Month, DateTime.DaysInMonth(temp.Year, temp.Month)), warehouse);
                        received = _unitOfWork.BranchInvoiceRepository
                            .GetMoneyReceived(temp, new DateTime(temp.Year, temp.Month, DateTime.DaysInMonth(temp.Year, temp.Month)), warehouse);
                    }
                    //if last month then get from first day of month to request day
                    else if (temp.Year == to.Year && temp.Month == to.Month)
                    {
                        paid = _unitOfWork.BranchInvoiceRepository
                            .GetMoneyPaid(new DateTime(temp.Year, temp.Month, 1), to, warehouse);
                        received = _unitOfWork.BranchInvoiceRepository
                            .GetMoneyReceived(new DateTime(temp.Year, temp.Month, 1), to, warehouse);
                    }
                    //if middle months then get from first day of month to last day of month
                    else
                    {
                        paid = _unitOfWork.BranchInvoiceRepository
                            .GetMoneyPaid(new DateTime(temp.Year, temp.Month, 1), new DateTime(temp.Year, temp.Month, DateTime.DaysInMonth(temp.Year, temp.Month)), warehouse);
                        received = _unitOfWork.BranchInvoiceRepository
                            .GetMoneyReceived(new DateTime(temp.Year, temp.Month, 1), new DateTime(temp.Year, temp.Month, DateTime.DaysInMonth(temp.Year, temp.Month)), warehouse);
                    }
                }

                statistics.Add(new StatisticsResponseDto()
                {
                    Month = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(temp.Month),
                    Year = temp.Year,
                    MoneyPaid = paid,
                    MoneyReceived = received,
                });
                temp = temp.AddMonths(1);
            }

            return new DashboardResponseDto()
            {
                TotalImports = totalImports,
                TotalExports = totalExports,
                TotalProductsInStock = productsInStock,
                TotalProductsExport = productsExported,
                TrendingProducts = list,
                TotalBaseDebt = 0,
                TotalBranchDebt = branchDebt,
                Statistics = statistics,
            };
        }
    }
}