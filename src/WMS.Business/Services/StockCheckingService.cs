﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using WMS.Business.Abstractions.Service;
using WMS.Business.Dtos.StockCheckSheetDto;
using WMS.Domain.Abstractions;
using WMS.Domain.Constant;

namespace WMS.Business.Services
{
    public class StockCheckingService : IStockCheckingService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<StockCheckingService> _logger;
        private readonly IMapper _mapper;
        private readonly IUserService _userService;
        private readonly IWarehouseService _warehouseService;

        public StockCheckingService(IUnitOfWork unitOfWork,
            ILogger<StockCheckingService> logger,
            IMapper mapper,
            IUserService userService,
            IWarehouseService warehouseService)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
            _mapper = mapper;
            _userService = userService;
            _warehouseService = warehouseService;
        }

        public async Task<IEnumerable<GetProductByWarehouseIdAndProductIdResponseDto>> GetProductQuantity(IEnumerable<Guid> productIds, string currentUser)
        {
            int[] mybase = { CommonConstant.BaseWarehouseManager, CommonConstant.BaseWarehouseEmployee };
            var user = await _unitOfWork.UserRepository.GetUserContainRoleByUsername(currentUser).ConfigureAwait(false);

            var warehouseId = await _warehouseService.GetWarehouseIdByUsername(currentUser).ConfigureAwait(false);
            var userRole = user.UserRole.FirstOrDefault();
            if (mybase.Contains(userRole.RoleId))
            {
                var baseImport = _unitOfWork.BaseImportRepository.GetBaseImportByWarehouseIdAndProductIds(warehouseId, productIds);
                var result = baseImport
                    .GroupBy(bip => bip.ProductId)
                    .Select(bip => new GetProductByWarehouseIdAndProductIdResponseDto()
                    {
                        ProductId = bip.Key,
                        ProductName = bip.First().WmsProduct.ProductName,
                        SKU = bip.First().WmsProduct.SKU,
                        UnitPrice = bip.First().UnitPrice,
                        CostPrice = bip.First().WmsProduct.CostPrice,
                        ExportPrice = bip.First().WmsProduct.ExportPrice,
                        TotalQuantity = bip.Sum(bip => bip.StockQuantity),
                        Visible = bip.First().WmsProduct.Visible,
                    });
                return result;
            }
            else
            {
                var branchImport = _unitOfWork.BranchImportRepository.GetBranchImportByWarehouseIdAndProductIds(warehouseId, productIds);
                var result = branchImport
                    .GroupBy(bip => bip.ProductId)
                    .Select(bip => new GetProductByWarehouseIdAndProductIdResponseDto()
                    {
                        ProductId = bip.Key,
                        ProductName = bip.First().WmsProduct.ProductName,
                        SKU = bip.First().WmsProduct.SKU,
                        UnitPrice = bip.First().UnitPrice,
                        CostPrice = bip.First().WmsProduct.WmsBranchWarehouseProducts
                                               .FirstOrDefault(bwp => bwp.WarehouseId.Equals(warehouseId)).CostPrice,
                        ExportPrice = bip.First().WmsProduct.WmsBranchWarehouseProducts
                                               .FirstOrDefault(bwp => bwp.WarehouseId.Equals(warehouseId)).ExportPrice,
                        TotalQuantity = bip.Sum(bip => bip.StockQuantity),
                        Visible = bip.First().WmsProduct.Visible,
                    });
                return result;
            }
        }
    }
}
