﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using WMS.Business.Abstractions.Service;
using WMS.Business.Dtos.BaseExportDto;
using WMS.Business.Dtos.BranchExportDto;
using WMS.Business.Dtos.CommonDto;
using WMS.Business.Dtos.S3Dto;
using WMS.Business.Utils.ExtensionMethods;
using WMS.Business.Utils.StringUtils;
using WMS.Domain.Abstractions;
using WMS.Domain.Constant;
using WMS.Domain.Entities;
using WMS.Domain.Enums;

namespace WMS.Business.Services
{
    public class BranchExportService : IBranchExportService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<BranchExportService> _logger;
        private readonly IMapper _mapper;
        private readonly IDateService _dateService;
        private readonly IS3StorageService _s3StorageService;
        private readonly IWarehouseService _warehouseService;
        private readonly IInvoiceService _invoiceService;

        public BranchExportService(IUnitOfWork unitOfWork,
            ILogger<BranchExportService> logger,
            IMapper mapper,
            IDateService dateService,
            IS3StorageService s3StorageService,
            IWarehouseService warehouseService,
            IInvoiceService invoiceService)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
            _mapper = mapper;
            _dateService = dateService;
            _s3StorageService = s3StorageService;
            _warehouseService = warehouseService;
            _invoiceService = invoiceService;
        }

        public async Task<ViewPaging<GetBranchExportResponseDto>> GetExportsList(GetBranchExportRequestDto requestDto, string currentUser)
        {
            try
            {
                var dateFilter = _dateService.GetDateTimeByDateFilter(requestDto.DateFilter);
                //Convert enum date -> dateTime
                var warehouseId = await _warehouseService.GetWarehouseIdByUsername(currentUser).ConfigureAwait(false);

                var fullList = await _unitOfWork.BranchExportRepository.FilterSearchBranchExport(requestDto.SearchWord,
                                                                                       warehouseId,
                                                                                       dateFilter.StartDate,
                                                                                       dateFilter.EndDate)
                                                                                       .ConfigureAwait(false);
                var filteredList = fullList
                    .Skip(requestDto.PagingRequest.PageSize * (requestDto.PagingRequest.CurrentPage - 1))
                    .Take(requestDto.PagingRequest.PageSize);

                var pagination = new Pagination(fullList.Count(), requestDto.PagingRequest.CurrentPage,
                    requestDto.PagingRequest.PageRange, requestDto.PagingRequest.PageSize);

                var result = _mapper.Map<IEnumerable<GetBranchExportResponseDto>>(filteredList);

                foreach (var branchExport in result)
                {
                    //merge product duplicate
                    branchExport.WmsBranchExportProducts = branchExport.WmsBranchExportProducts.GroupBy(bep => bep.ProductId)
                                                                               .Select(bep => new BranchExportProductResponseDto(
                                                                                bep.Key,
                                                                                bep.First().ProductName,
                                                                                bep.First().ProductImage,
                                                                                bep.First().UnitPrice,
                                                                                bep.Sum(k => k.Quantity),
                                                                                bep.Sum(k => k.TotalPrice)))
                                                                               .ToList();
                    foreach (var branchExportProduct in branchExport.WmsBranchExportProducts)
                    {
                        var productImageUrl = _s3StorageService.GetFileUrl(new S3RequestData()
                        {
                            Name = branchExportProduct.ProductImage,
                        });
                        branchExportProduct.ProductImage = productImageUrl;
                    }
                }
                return new ViewPaging<GetBranchExportResponseDto>(result, pagination);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return null;
            }
        }

        public async Task<GetBranchExportResponseDto> GetExportsDetails(Guid exportId)
        {
            try
            {
                var branchExport = await _unitOfWork.BranchExportRepository.GetBranchExportDetailsById(exportId).ConfigureAwait(false);
                var result = _mapper.Map<GetBranchExportResponseDto>(branchExport);
                //merge product duplicate
                result.WmsBranchExportProducts = result.WmsBranchExportProducts.GroupBy(bep => bep.ProductId)
                                                                           .Select(bep => new BranchExportProductResponseDto(
                                                                            bep.Key,
                                                                            bep.First().ProductName,
                                                                            bep.First().ProductImage,
                                                                            bep.First().UnitPrice,
                                                                            bep.Sum(k => k.Quantity),
                                                                            bep.Sum(k => k.TotalPrice)))
                                                                           .ToList();
                //get image
                foreach (var branchExportProduct in result.WmsBranchExportProducts)
                {

                    //GetBaseExportProductByExportIdProductIdAndListBaseImportId 
                    var exportProductDetails = await _unitOfWork.BranchExportProductRepository
                        .GetBaseExportProductByExportIdProductIdAndListBaseImportId(exportId,
                                                                                    branchExportProduct.ProductId,
                                                                                    branchExport.WmsBranchExportProducts.Select(bep => bep.ImportId))
                                                                                    .ConfigureAwait(false);
                    var exportProductDetailsMap = _mapper.Map<IEnumerable<BranchExportProductDetailsResponseDto>>(exportProductDetails);
                    branchExportProduct.BranchExportProductDetails = exportProductDetailsMap;

                    var productImageUrl = _s3StorageService.GetFileUrl(new S3RequestData()
                    {
                        Name = branchExportProduct.ProductImage,
                    });
                    branchExportProduct.ProductImage = productImageUrl;
                }
                //get invoice
                result.Invoices = await _invoiceService.GetBranchInvoiceByExportId(exportId).ConfigureAwait(false);
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return null;
            }
        }

        public async Task<Guid> CreateBranchExport(CreateBranchExportRequestDto requestDto, string currentUser)
        {
            requestDto.ExportDate = DateTimeExtension.ConvertUTCToLocalTimeZone(requestDto.ExportDate);
            if (requestDto.BranchExportProducts.Count() == 0 || requestDto.ExportDate.Date < DateTime.Now.Date) return Guid.Empty;
            if (requestDto.BranchExportProducts == null) return Guid.Empty;
            using var transaction = _unitOfWork.BeginTransaction();
            try
            {
                //get warehouse id by current user
                var warehouseId = await _warehouseService.GetWarehouseIdByUsername(currentUser).ConfigureAwait(false);
                //get id current user
                var assignBy = await _unitOfWork.UserRepository.FindObject(x => x.Username.Equals(currentUser)
                    && x.Status == (short)UserStatusEnum.Active).ConfigureAwait(false);
                if (assignBy == null) return Guid.Empty;
                // assign by is current user when assignBy empty
                if (requestDto.ExportBy == Guid.Empty)
                {
                    requestDto.ExportBy = assignBy.UserId;
                }


                var branchExport = _mapper.Map<WmsBranchExport>(requestDto);
                branchExport.WarehouseId = warehouseId;
                branchExport.AssignBy = assignBy.UserId;
                branchExport.ApprovalStatus = ExportStatusConstant.Exported;
                branchExport.ExportCode = StringHelper.GenerateSpecifyId(50, "BREX");


                //get total price and add branch export
                decimal totalPrice = 0;
                foreach (var product in requestDto.BranchExportProducts)
                {
                    var existProduct = await _unitOfWork.BranchWarehouseProductRepository
                        .FindObject(x => x.ProductId.Equals(product.ProductId)
                        && x.WarehouseId.Equals(warehouseId))
                        .ConfigureAwait(false);
                    totalPrice += existProduct.ExportPrice * product.Quantity;
                }
                branchExport.TotalPrice = totalPrice;
                await _unitOfWork.BranchExportRepository.AddAsync(branchExport).ConfigureAwait(false);


                //add branch export products
                foreach (var product in requestDto.BranchExportProducts)
                {
                    //check stock
                    var stock = await _unitOfWork.BranchImportProductRepository.FindMultiple(x => x.ProductId.Equals(product.ProductId)
                    && x.StockQuantity > 0).ConfigureAwait(false);
                    if (product.Quantity > stock.Sum(x => x.StockQuantity)) return Guid.Empty;

                    var existProduct = await _unitOfWork.BranchWarehouseProductRepository
                        .FindObject(x => x.ProductId.Equals(product.ProductId)
                        && x.WarehouseId.Equals(warehouseId))
                        .ConfigureAwait(false);
                    var importProducts = await _unitOfWork.BranchImportProductRepository
                        .GetBranchStockOrderByImportDate(product.ProductId, warehouseId)
                        .ToListAsync().ConfigureAwait(false);

                    //FIRST IN FIRST OUT
                    var requestQuantity = product.Quantity;
                    foreach (var importProduct in importProducts)
                    {
                        //if quantity has been decrease to 0, end the loop
                        if (requestQuantity <= 0) break;

                        //create a temporary variable to change the export quantity
                        int exportQuantity = 0;

                        //if this is not the last import in this list
                        if (importProducts.IndexOf(importProduct) != importProducts.Count - 1)
                        {
                            if (requestQuantity >= importProduct.StockQuantity) { exportQuantity = importProduct.StockQuantity; }
                            else exportQuantity = requestQuantity;
                            requestQuantity -= exportQuantity;
                        }
                        else  //if this is the last import in this list
                        {
                            exportQuantity = requestQuantity;
                            requestQuantity = 0; //optional - for better understanding
                        }

                        //create a new branch export product
                        var productExport = await _unitOfWork.BranchExportProductRepository
                            .FindObject(x => x.ExportId.Equals(branchExport.ExportId) && x.ProductId.Equals(product.ProductId)
                            && x.ImportId.Equals(importProduct.ImportId)).ConfigureAwait(false);
                        if (productExport == null)
                        {
                            productExport = new WmsBranchExportProduct()
                            {
                                ProductId = product.ProductId,
                                ExportId = branchExport.ExportId,
                                UnitPrice = existProduct.ExportPrice,
                                Quantity = exportQuantity,
                                ImportId = importProduct.ImportId,
                            };
                            await _unitOfWork.BranchExportProductRepository.AddAsync(productExport).ConfigureAwait(false);
                            await _unitOfWork.CommitAsync().ConfigureAwait(false);
                        }
                        else
                        {
                            productExport.Quantity += exportQuantity;
                            _unitOfWork.BranchExportProductRepository.Update(productExport);
                        }


                        importProduct.StockQuantity -= exportQuantity;
                        _unitOfWork.BranchImportProductRepository.Update(importProduct);
                    }
                }

                //add invoice 
                var branchWarehouse = await _unitOfWork.BranchWarehouseRepository.FindObject(x => x.WarehouseId.Equals(warehouseId)).ConfigureAwait(false);
                var newInvoice = new WmsBranchInvoice()
                {
                    ExportId = branchExport.ExportId,
                    Type = InvoiceTypeConstant.ReceiptVoucher,
                    Amount = branchExport.TotalPrice,
                    Description = $"Kho con {branchWarehouse.WarehouseName} nhận {branchExport.TotalPrice} VND cho đơn xuất hàng",
                    InvoiceCode = StringHelper.GenerateSpecifyId(50, "BRINV"),
                };
                await _unitOfWork.BranchInvoiceRepository.AddAsync(newInvoice).ConfigureAwait(false);

                await _unitOfWork.CommitAsync().ConfigureAwait(false);
                transaction.Commit();
                _logger.LogInformation($"Create export successfully!!!");
                return branchExport.ExportId;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                transaction.Rollback();
                return Guid.Empty;
            }
            finally
            {
                if (transaction.Connection != null) transaction.Connection.Close();
            }
        }

    }
}
