﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using WMS.Business.Abstractions.Service;
using WMS.Business.Dtos.BaseImportDto;
using WMS.Business.Dtos.CommonDto;
using WMS.Business.Dtos.S3Dto;
using WMS.Business.Dtos.WarehouseDto;
using WMS.Business.Utils.ExtensionMethods;
using WMS.Business.Utils.StringUtils;
using WMS.Domain.Abstractions;
using WMS.Domain.Constant;
using WMS.Domain.Entities;
using WMS.Domain.Enums;

namespace WMS.Business.Services
{
    public class BaseImportService : IBaseImportService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<BaseImportService> _logger;
        private readonly IMapper _mapper;
        private readonly IS3StorageService _s3StorageService;
        private readonly IDateService _dateService;
        private readonly IProductService _productService;
        private readonly IInvoiceService _invoiceService;

        public BaseImportService(IUnitOfWork unitOfWork,
            ILogger<BaseImportService> logger,
            IMapper mapper,
            IS3StorageService s3StorageService,
            IDateService dateService,
            IProductService productService,
            IInvoiceService invoiceService)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
            _mapper = mapper;
            _s3StorageService = s3StorageService;
            _dateService = dateService;
            _productService = productService;
            _invoiceService = invoiceService;
        }

        public async Task<Guid?> CreateBaseImport(CreateBaseImportRequestDto entity, string currentUser)
        {
            entity.RequestDate = DateTimeExtension.ConvertUTCToLocalTimeZone(entity.RequestDate);
            if (await _unitOfWork.UserRepository.FindObject(x => x.UserId.Equals(entity.ImportBy)).ConfigureAwait(false) == null) return null;
            if (entity.RequestDate.Date < DateTime.Now.Date || !entity.ProductsList.Any()) return null;

            //check current manager exist
            var assignBy = await _unitOfWork.UserRepository.FindObject(x => x.Username.Equals(currentUser)
                && x.Status == (short)UserStatusEnum.Active).ConfigureAwait(false);
            if (assignBy == null) return null;

            //check warehouse of this manager exist
            var warehouse = (await _unitOfWork.BaseWarehouseRepository.All().ConfigureAwait(false)).FirstOrDefault();
            if (warehouse == null) return null;

            //check supplier exist
            var supplier = await _unitOfWork.SupplierRepository.GetById(entity.SupplierId).ConfigureAwait(false);
            if (supplier == null || !supplier.Status) return null;

            using var transaction = _unitOfWork.BeginTransaction();
            try
            {
                //add base import
                var baseImport = _mapper.Map<WmsBaseImport>(entity);
                baseImport.ImportCode = StringHelper.GenerateSpecifyId(50, "BAIM");
                baseImport.WarehouseId = warehouse.WarehouseId;
                baseImport.AssignBy = assignBy.UserId;
                baseImport.PaymentStatus = PaymentStatusConstant.NotPaid;
                baseImport.ImportStatus = ImportStatusConstant.NotImported;
                baseImport.PaidAmount = 0;
                baseImport.CreateDate = DateTime.Now;
                baseImport.CreateUser = assignBy.Username;
                baseImport.UpdateDate = DateTime.Now;
                baseImport.UpdateUser = assignBy.Username;

                //calculate price of each product and total price
                decimal totalPrice = 0;
                int totalNumberOfItems = 0;
                foreach (var product in entity.ProductsList)
                {
                    //check product exist
                    var exist = await _unitOfWork.ProductRepository.GetById(product.ProductId).ConfigureAwait(false);
                    if (exist == null) return null;

                    //calculate
                    totalPrice += product.UnitPrice * product.ImportedQuantity;
                    totalNumberOfItems += product.ImportedQuantity;
                }
                decimal extraFeeForEachItem = baseImport.ExtraCost / totalNumberOfItems;

                baseImport.TotalPrice = totalPrice;
                baseImport.TotalCost = totalPrice + entity.ExtraCost;
                await _unitOfWork.BaseImportRepository.AddAsync(baseImport).ConfigureAwait(false);

                //add base import products
                var baseImportProducts = _mapper.Map<IEnumerable<WmsBaseImportProduct>>(entity.ProductsList);
                foreach (var product in baseImportProducts)
                {
                    product.ImportId = baseImport.ImportId;
                    product.CostPrice = product.UnitPrice + extraFeeForEachItem;
                    product.StockQuantity = 0;
                    await _unitOfWork.BaseImportProductRepository.AddAsync(product).ConfigureAwait(false);
                }

                //add debt
                var debt = await _unitOfWork.BaseDebtRepository.FindObject(x => x.CreditorId == supplier.SupplierId).ConfigureAwait(false);
                if (debt == null)
                {
                    debt = new WmsBaseDebt()
                    {
                        CreditorId = supplier.SupplierId,
                        DebtAmount = 0,
                        DebtorId = warehouse.WarehouseId,
                    };
                    await _unitOfWork.BaseDebtRepository.AddAsync(debt).ConfigureAwait(false);
                }

                await _unitOfWork.CommitAsync().ConfigureAwait(false);
                transaction.Commit();
                _logger.LogInformation($"Create import successfully!!!");
                return baseImport.ImportId;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                transaction.Rollback();
                return null;
            }
            finally
            {
                if (transaction.Connection != null) transaction.Connection.Close();
            }
        }

        public async Task<bool> UpdateBaseImport(UpdateBaseImportRequestDto entity, string currentUser)
        {
            entity.RequestDate = DateTimeExtension.ConvertUTCToLocalTimeZone(entity.RequestDate);
            if (await _unitOfWork.UserRepository.FindObject(x => x.UserId.Equals(entity.ImportBy)).ConfigureAwait(false) == null) return false;
            if (entity.RequestDate.Date < DateTime.Now.Date || !entity.ProductsList.Any()) return false;

            //check manager exist
            var assignBy = await _unitOfWork.UserRepository.FindObject(x => x.Username.Equals(currentUser)
                && x.Status == (short)UserStatusEnum.Active).ConfigureAwait(false);
            if (assignBy == null) return false;

            //check warehouse of this manager exist
            var warehouse = (await _unitOfWork.BaseWarehouseRepository.All().ConfigureAwait(false)).FirstOrDefault();
            if (warehouse == null) return false;

            //check supplier exist
            var supplier = await _unitOfWork.SupplierRepository.GetById(entity.SupplierId).ConfigureAwait(false);
            if (supplier == null || !supplier.Status) return false;

            using var transaction = _unitOfWork.BeginTransaction();
            try
            {
                //if this import has been delivered or paid, the manager can not update it
                var baseImport = await _unitOfWork.BaseImportRepository.FindObject(x => x.ImportId.Equals(entity.ImportId)).ConfigureAwait(false);
                if (baseImport == null || baseImport.ImportStatus != ImportStatusConstant.NotImported
                    || baseImport.PaymentStatus != PaymentStatusConstant.NotPaid) return false;

                //update base import             
                baseImport.AssignBy = assignBy.UserId;
                baseImport.ImportBy = entity.ImportBy;
                baseImport.SupplierId = entity.SupplierId;
                baseImport.RequestDate = entity.RequestDate;
                baseImport.ExtraCost = entity.ExtraCost;
                baseImport.Note = entity.Note;
                baseImport.UpdateDate = DateTime.Now;
                baseImport.UpdateUser = assignBy.Username;

                //calculate price of each product and total price
                decimal totalPrice = 0;
                int totalNumberOfItems = 0;
                foreach (var product in entity.ProductsList)
                {
                    //check product exist
                    var exist = await _unitOfWork.ProductRepository.GetById(product.ProductId).ConfigureAwait(false);
                    if (exist == null) return false;

                    totalPrice += product.UnitPrice * product.ImportedQuantity;
                    totalNumberOfItems += product.ImportedQuantity;
                }
                decimal extraFeeForEachItem = baseImport.ExtraCost / totalNumberOfItems;

                baseImport.TotalPrice = totalPrice;
                baseImport.TotalCost = totalPrice + entity.ExtraCost;
                _unitOfWork.BaseImportRepository.Update(baseImport);

                //delete old base import products
                var products = await _unitOfWork.BaseImportProductRepository.FindMultiple(x => x.ImportId.Equals(baseImport.ImportId))
                    .ConfigureAwait(false);
                foreach (var product in products)
                {
                    _unitOfWork.BaseImportProductRepository.Delete(product);
                }

                //add new base import products
                var baseImportProducts = _mapper.Map<IEnumerable<WmsBaseImportProduct>>(entity.ProductsList);
                foreach (var product in baseImportProducts)
                {
                    product.ImportId = baseImport.ImportId;
                    product.CostPrice = product.UnitPrice + extraFeeForEachItem;
                    product.StockQuantity = 0;
                    await _unitOfWork.BaseImportProductRepository.AddAsync(product).ConfigureAwait(false);
                }

                //add debt
                var debt = await _unitOfWork.BaseDebtRepository.FindObject(x => x.CreditorId == supplier.SupplierId).ConfigureAwait(false);
                if (debt == null)
                {
                    debt = new WmsBaseDebt()
                    {
                        CreditorId = supplier.SupplierId,
                        DebtAmount = 0,
                        DebtorId = warehouse.WarehouseId,
                    };
                    await _unitOfWork.BaseDebtRepository.AddAsync(debt).ConfigureAwait(false);
                }

                await _unitOfWork.CommitAsync().ConfigureAwait(false);
                transaction.Commit();
                _logger.LogInformation($"Update import successfully!!!");
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                transaction.Rollback();
                return false;
            }
            finally
            {
                if (transaction.Connection != null) transaction.Connection.Close();
            }
        }

        public async Task<bool> UpdateBaseImportNote(UpdateBaseImportNoteRequestDto entity, string currentUser)
        {
            var user = await _unitOfWork.UserRepository.FindObject(x => x.Username.Equals(currentUser)
                && x.Status == (short)UserStatusEnum.Active).ConfigureAwait(false);
            if (user == null) return false;

            using var transaction = _unitOfWork.BeginTransaction();
            try
            {
                var baseImport = await _unitOfWork.BaseImportRepository.FindObject(x => x.ImportId.Equals(entity.ImportId)).ConfigureAwait(false);
                if (baseImport == null) return false;
                baseImport.Note = entity.Note;
                baseImport.UpdateDate = DateTime.Now;
                baseImport.UpdateUser = user.Username;

                _unitOfWork.BaseImportRepository.Update(baseImport);
                await _unitOfWork.CommitAsync().ConfigureAwait(false);
                transaction.Commit();
                _logger.LogInformation($"Update import successfully!!!");
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                transaction.Rollback();
                return false;
            }
            finally
            {
                if (transaction.Connection != null) transaction.Connection.Close();
            }
        }

        /// <summary>
        /// GetBaseImportList
        /// </summary>
        /// <param name="requestDto"></param>
        /// <returns>ViewPaging<GetBaseImportResponseDto></returns>
        public async Task<ViewPaging<GetBaseImportResponseDto>> GetBaseImportList(GetBaseImportRequestDto requestDto)
        {
            try
            {
                //Convert enum date -> dateTime
                var dateFilter = _dateService.GetDateTimeByDateFilter(requestDto.DateFilter);
                var fullList = _unitOfWork.BaseImportRepository.FilterSearchBaseImport(requestDto.SearchWord,
                                                                                       requestDto.ImportStatus,
                                                                                       requestDto.PaymentStatus,
                                                                                       requestDto.SupplierId,
                                                                                       dateFilter.StartDate,
                                                                                       dateFilter.EndDate);

                var filteredList = fullList
                    .Skip(requestDto.PagingRequest.PageSize * (requestDto.PagingRequest.CurrentPage - 1))
                    .Take(requestDto.PagingRequest.PageSize);

                var pagination = new Pagination(await fullList.CountAsync(), requestDto.PagingRequest.CurrentPage,
                    requestDto.PagingRequest.PageRange, requestDto.PagingRequest.PageSize);

                var result = _mapper.Map<IEnumerable<GetBaseImportResponseDto>>(filteredList);

                foreach (var baseImport in result)
                {
                    foreach (var baseImportProduct in baseImport.WmsBaseImportProducts)
                    {
                        var productImageUrl = _s3StorageService.GetFileUrl(new S3RequestData()
                        {
                            Name = baseImportProduct!.ProductImage,
                        });
                        baseImportProduct.ProductImage = productImageUrl;
                    }
                }
                return new ViewPaging<GetBaseImportResponseDto>(result, pagination);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return null;
            }
        }

        public async Task<bool> PayForBaseImport(PayForBaseImportRequestDto entity, string currentUser)
        {
            var user = await _unitOfWork.UserRepository.FindObject(x => x.Username.Equals(currentUser)
                && x.Status == (short)UserStatusEnum.Active).ConfigureAwait(false);
            if (user == null) return false;

            var baseImport = await _unitOfWork.BaseImportRepository.FindObject(x => x.ImportId.Equals(entity.ImportId)).ConfigureAwait(false);
            if (baseImport == null || baseImport.PaymentStatus == PaymentStatusConstant.Paid || entity.PayAmount <= 0
                || baseImport.ImportStatus != ImportStatusConstant.Imported) return false;

            //find debt and supplier
            var supplier = await _unitOfWork.SupplierRepository.GetById(baseImport.SupplierId).ConfigureAwait(false);
            if (supplier == null || !supplier.Status) return false;
            var debt = await _unitOfWork.BaseDebtRepository.FindObject(x => x.CreditorId == supplier.SupplierId).ConfigureAwait(false);
            if (debt == null) return false;

            //update import
            var paid = baseImport.PaidAmount + entity.PayAmount;
            if (paid > baseImport.TotalCost) return false;

            baseImport.PaidAmount += entity.PayAmount;
            if (baseImport.PaidAmount == baseImport.TotalCost) baseImport.PaymentStatus = PaymentStatusConstant.Paid;
            else baseImport.PaymentStatus = PaymentStatusConstant.PartlyPaid;

            using var transaction = _unitOfWork.BeginTransaction();
            try
            {
                //update import
                baseImport.UpdateDate = DateTime.Now;
                baseImport.UpdateUser = user.Username;
                _unitOfWork.BaseImportRepository.Update(baseImport);

                //update debt
                debt.DebtAmount -= entity.PayAmount;
                _unitOfWork.BaseDebtRepository.Update(debt);

                //add new invoice
                var invoice = new WmsBaseInvoice()
                {
                    DebtId = debt.DebtId,
                    Type = InvoiceTypeConstant.PaymentVoucher,
                    Amount = entity.PayAmount,
                    Description = $"Trả {entity.PayAmount} VND cho đơn nhập hàng từ NCC {supplier.SupplierName}",
                    ImportId = entity.ImportId,
                    CreateDate = DateTime.Now,
                    CreateUser = user.Username,
                    UpdateDate = DateTime.Now,
                    UpdateUser = user.Username,
                    BalanceChange = debt.DebtAmount,
                    InvoiceCode = StringHelper.GenerateSpecifyId(50, "BAINV"),
                };
                await _unitOfWork.BaseInvoiceRepository.AddAsync(invoice).ConfigureAwait(false);

                await _unitOfWork.CommitAsync().ConfigureAwait(false);
                transaction.Commit();
                _logger.LogInformation($"Pay import successfully!!!");
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                transaction.Rollback();
                return false;
            }
            finally
            {
                if (transaction.Connection != null) transaction.Connection.Close();
            }
        }

        public async Task<bool> ChangeBaseImportStatus(Guid importId, string currentUser)
        {
            var user = await _unitOfWork.UserRepository.FindObject(x => x.Username.Equals(currentUser)
                && x.Status == (short)UserStatusEnum.Active).ConfigureAwait(false);
            if (user == null) return false;

            var baseImport = await _unitOfWork.BaseImportRepository.FindObject(x => x.ImportId.Equals(importId)).ConfigureAwait(false);
            if (baseImport == null) return false;

            //find debt and supplier
            var supplier = await _unitOfWork.SupplierRepository.GetById(baseImport.SupplierId).ConfigureAwait(false);
            if (supplier == null || !supplier.Status) return false;
            var debt = await _unitOfWork.BaseDebtRepository.FindObject(x => x.CreditorId == supplier.SupplierId).ConfigureAwait(false);
            if (debt == null) return false;

            using var transaction = _unitOfWork.BeginTransaction();
            try
            {
                //update status
                baseImport.ImportedDate = DateTime.Now;
                baseImport.ImportStatus = ImportStatusConstant.Imported;
                baseImport.UpdateDate = DateTime.Now;
                baseImport.UpdateUser = user.Username;

                _unitOfWork.BaseImportRepository.Update(baseImport);
                await _unitOfWork.CommitAsync().ConfigureAwait(false);

                //update debt
                if (baseImport.PaymentStatus == PaymentStatusConstant.NotPaid
                    || baseImport.PaymentStatus == PaymentStatusConstant.PartlyPaid)
                    debt.DebtAmount += (baseImport.TotalCost - baseImport.PaidAmount);
                _unitOfWork.BaseDebtRepository.Update(debt);

                //update stock
                var products = await _unitOfWork.BaseImportProductRepository
                    .FindMultiple(x => x.ImportId.Equals(importId)).ConfigureAwait(false);
                foreach (var product in products)
                {
                    product.StockQuantity = product.ImportedQuantity;
                    _unitOfWork.BaseImportProductRepository.Update(product);

                    //change product price
                    var existProduct = await _unitOfWork.ProductRepository.GetById(product.ProductId).ConfigureAwait(false);
                    existProduct.CostPrice = _unitOfWork.ProductRepository.GetCostPriceByProductId(product.ProductId);
                    _unitOfWork.ProductRepository.Update(existProduct);
                }

                await _unitOfWork.BaseInvoiceRepository.AddAsync(new WmsBaseInvoice()
                {
                    DebtId = debt.DebtId,
                    Type = InvoiceTypeConstant.ReceiptVoucher,
                    Amount = baseImport.TotalCost,
                    Description = $"Nhận hàng trị giá {baseImport.TotalCost} VND từ NCC {supplier.SupplierName}",
                    ImportId = baseImport.ImportId,
                    BalanceChange = debt.DebtAmount,
                    InvoiceCode = StringHelper.GenerateSpecifyId(50, "BAINV"),
                }).ConfigureAwait(false);

                await _unitOfWork.CommitAsync().ConfigureAwait(false);
                transaction.Commit();
                _logger.LogInformation($"Update import successfully!!!");
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                transaction.Rollback();
                return false;
            }
            finally
            {
                if (transaction.Connection != null) transaction.Connection.Close();
            }
        }

        /// <summary>
        /// Get Base Import Details by base import id
        /// </summary>
        /// <param name="baseImportId"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<GetBaseImportDetailsResponseDto?> GetBaseImportDetails(Guid baseImportId)
        {
            try
            {
                var baseImport = await _unitOfWork.BaseImportRepository.GetBaseImportDetailsById(baseImportId).ConfigureAwait(false);
                var result = _mapper.Map<GetBaseImportDetailsResponseDto>(baseImport);

                foreach (var baseImportProduct in result.WmsBaseImportProducts)
                {
                    var productImageUrl = _s3StorageService.GetFileUrl(new S3RequestData()
                    {
                        Name = baseImportProduct.ProductImage,
                    });
                    baseImportProduct.ProductImage = productImageUrl;
                }
                //list product return by base import id
                result.ProductReturns = await _productService.GetProductReturnToSupplierByImportId(baseImportId).ConfigureAwait(false);

                //get invoice
                var invoices = await _invoiceService.GetBaseInvoiceByImportId(baseImportId).ConfigureAwait(false);
                result.Invoices = invoices.OrderByDescending(i => i.UpdateDate);
                //var baseProductReturn = await _unitOfWork.BaseProductReturnRepository
                //    .FindMultiple(bpr => bpr.ImportId.Equals(baseImportId))
                //    .ConfigureAwait(false);
                //var invoiceReturn = await _invoiceService.GetBaseInvoiceByReturnId(baseProductReturn.Select(bpr => bpr.ProductReturnId)).ConfigureAwait(false);
                //result.Invoices = result.Invoices.Concat(invoiceReturn).OrderByDescending(i => i.UpdateDate);

                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return null;
            }

        }

        public async Task<bool> PayForBaseProductReturn(PayForBaseProductReturnRequestDto entity, string currentUser)
        {
            var user = await _unitOfWork.UserRepository.FindObject(x => x.Username.Equals(currentUser)
                && x.Status == (short)UserStatusEnum.Active).ConfigureAwait(false);
            if (user == null) return false;

            var baseReturn = await _unitOfWork.BaseProductReturnRepository
                .FindObject(x => x.ProductReturnId.Equals(entity.BaseProductReturnId))
                .ConfigureAwait(false);
            if (baseReturn == null || baseReturn.PaymentStatus == PaymentStatusConstant.Paid || entity.PayAmount <= 0
                || baseReturn.Status != ProductReturnStatusConstant.Returned) return false;

            var baseImport = await _unitOfWork.BaseImportRepository
                .FindObject(x => x.ImportId.Equals(baseReturn.ImportId))
                .ConfigureAwait(false);
            if (baseImport == null) return false;

            //find debt and supplier
            var supplier = await _unitOfWork.SupplierRepository.GetById(baseImport.SupplierId).ConfigureAwait(false);
            if (supplier == null || !supplier.Status) return false;

            var debt = await _unitOfWork.BaseDebtRepository.FindObject(x => x.CreditorId == supplier.SupplierId).ConfigureAwait(false);
            if (debt == null) return false;

            //update return
            var paid = baseReturn.PaidAmount + entity.PayAmount;
            if (paid > baseReturn.TotalPrice) return false;

            baseReturn.PaidAmount += entity.PayAmount;
            if (baseReturn.PaidAmount == baseReturn.TotalPrice) baseReturn.PaymentStatus = PaymentStatusConstant.Paid;
            else baseReturn.PaymentStatus = PaymentStatusConstant.PartlyPaid;

            using var transaction = _unitOfWork.BeginTransaction();
            try
            {
                //update return
                baseReturn.UpdateDate = DateTime.Now;
                baseReturn.UpdateUser = user.Username;
                _unitOfWork.BaseProductReturnRepository.Update(baseReturn);

                //update debt
                debt.DebtAmount += entity.PayAmount;
                _unitOfWork.BaseDebtRepository.Update(debt);

                //add new invoice
                var invoice = new WmsBaseInvoice()
                {
                    DebtId = debt.DebtId,
                    Type = InvoiceTypeConstant.ReceiptVoucher,
                    Amount = entity.PayAmount,
                    Description = $"Nhận {entity.PayAmount} VND cho đơn trả hàng tới NCC {supplier.SupplierName}",
                    ReturnId = entity.BaseProductReturnId,
                    CreateDate = DateTime.Now,
                    CreateUser = user.Username,
                    UpdateDate = DateTime.Now,
                    UpdateUser = user.Username,
                    BalanceChange = debt.DebtAmount,
                    InvoiceCode = StringHelper.GenerateSpecifyId(50, "BAINV"),
                };
                await _unitOfWork.BaseInvoiceRepository.AddAsync(invoice).ConfigureAwait(false);

                await _unitOfWork.CommitAsync().ConfigureAwait(false);
                transaction.Commit();
                _logger.LogInformation($"Pay for return successfully!!!");
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                transaction.Rollback();
                return false;
            }
            finally
            {
                if (transaction.Connection != null) transaction.Connection.Close();
            }

        }


    }
}
