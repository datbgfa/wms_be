﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using WMS.Business.Abstractions.Service;
using WMS.Business.Dtos.BranchRequestDto;
using WMS.Business.Dtos.CommonDto;
using WMS.Business.Dtos.S3Dto;
using WMS.Domain.Abstractions;

namespace WMS.Business.Services
{
    public class BranchRequestService : IBranchRequestService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<BranchRequestService> _logger;
        private readonly IMapper _mapper;
        private readonly IDateService _dateService;
        private readonly IS3StorageService _s3StorageService;
        private readonly IWarehouseService _warehouseService;

        public BranchRequestService(IUnitOfWork unitOfWork,
            ILogger<BranchRequestService> logger,
            IMapper mapper,
            IDateService dateService,
            IS3StorageService s3StorageService,
            IWarehouseService warehouseService)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
            _mapper = mapper;
            _dateService = dateService;
            _s3StorageService = s3StorageService;
            _warehouseService = warehouseService;
        }

        /// <summary>
        /// FilterBranchRequest
        /// </summary>
        /// <param name="requestDto"></param>
        /// <param name="currentUser"></param>
        /// <returns>Task<ViewPaging<GetBranchRequestListReponseDto>></returns>
        public async Task<ViewPaging<GetBranchRequestReponseDto>> FilterBranchRequest(GetBranchRequestListRequestDto requestDto, string currentUser)
        {
            try
            {
                //Convert enum date -> dateTime◘
                var dateFilter = _dateService.GetDateTimeByDateFilter(requestDto.DateFilter);
                //GetWarehouseIdByUsername
                var warehouseId = await _warehouseService.GetWarehouseIdByUsername(currentUser).ConfigureAwait(false);
                //FilderBranchRequest
                var fullList = _unitOfWork.BranchRequestRepository.FilterBranchRequest(warehouseId,
                                                                                             requestDto.SearchWord,
                                                                                             requestDto.ApprovedSatus,
                                                                                             dateFilter.StartDate,
                                                                                             dateFilter.EndDate);
                var filteredList = fullList
                    .Skip(requestDto.PagingRequest.PageSize * (requestDto.PagingRequest.CurrentPage - 1))
                    .Take(requestDto.PagingRequest.PageSize);

                var pagination = new Pagination(fullList.Count(), requestDto.PagingRequest.CurrentPage,
                    requestDto.PagingRequest.PageRange, requestDto.PagingRequest.PageSize);

                var result = _mapper.Map<IEnumerable<GetBranchRequestReponseDto>>(filteredList);

                foreach (var baseRequest in result)
                {
                    foreach (var baseRequestProduct in baseRequest.WmsBranchProductRequests)
                    {
                        var productImageUrl = _s3StorageService.GetFileUrl(new S3RequestData()
                        {
                            Name = baseRequestProduct.ProductImage,
                        });
                        baseRequestProduct.ProductImage = productImageUrl;
                    }
                }
                return new ViewPaging<GetBranchRequestReponseDto>(result, pagination);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return null;
            }
        }

        public async Task<GetBranchRequestReponseDto> GetBranchRequestDetails(Guid branchRequestId)
        {

            try
            {
                var branchRequest = await _unitOfWork.BranchRequestRepository.GetBranchRequestDetails(branchRequestId).ConfigureAwait(false);
                var result = _mapper.Map<GetBranchRequestReponseDto>(branchRequest);
                foreach (var baseRequestProduct in result.WmsBranchProductRequests)
                {
                    var productImageUrl = _s3StorageService.GetFileUrl(new S3RequestData()
                    {
                        Name = baseRequestProduct.ProductImage,
                    });
                    baseRequestProduct.ProductImage = productImageUrl;
                }
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return null;
            }

        }
    }
}
