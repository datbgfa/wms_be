﻿using Microsoft.Extensions.DependencyInjection;
using WMS.Business.Abstractions.Service;
using WMS.Business.Dtos.Mapper;
using WMS.Business.Services;
using WMS.Business.Utils.OptionUtils;

namespace WMS.Business
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddBusiness(this IServiceCollection services)
        {
            services.AddAutoMapper(config =>
            {
                config.AddProfile<DomainToDtoProfile>();
                config.AddProfile<DtoToDomainProfile>();
            });

            #region Option setup
            services.ConfigureOptions<ConnectionStringsOptionsSetup>();
            services.ConfigureOptions<JwtOptionsSetup>();
            services.ConfigureOptions<ObjectStorageOptionsSetup>();
            #endregion

            #region Service
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<ITokenService, TokenService>();
            services.AddScoped<IEmailService, EmailService>();
            services.AddScoped<IS3StorageService, S3StorageService>();
            services.AddScoped<IBranchWarehouseService, BranchWarehouseService>();
            services.AddScoped<IWarehouseService, WarehouseService>();
            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<IEmployeeService, EmployeeService>();
            services.AddScoped<ICategoryService, CategoryService>();
            services.AddScoped<IBaseImportService, BaseImportService>();
            services.AddScoped<ISupplierService, SupplierService>();
            services.AddScoped<IBranchRequestService, BranchRequestService>();
            services.AddScoped<IDateService, DateService>();
            services.AddScoped<IBaseExportService, BaseExportService>();
            services.AddScoped<IBranchImportService, BranchImportService>();
            services.AddScoped<IBranchExportService, BranchExportService>();
            services.AddScoped<IStockCheckSheetService, StockCheckSheetService>();
            services.AddScoped<IInvoiceService, InvoiceService>();
            services.AddScoped<IStockCheckingService, StockCheckingService>();
            services.AddScoped<IPaymentInvoiceService, PaymentInvoiceService>();

            #endregion

            return services;
        }
    }
}