﻿using WMS.Business.Dtos.BaseImportDto;
using WMS.Business.Dtos.CommonDto;
using WMS.Business.Dtos.WarehouseDto;

namespace WMS.Business.Abstractions.Service
{
    public interface IBaseImportService
    {
        Task<Guid?> CreateBaseImport(CreateBaseImportRequestDto entity, string currentUser);
        Task<ViewPaging<GetBaseImportResponseDto>> GetBaseImportList(GetBaseImportRequestDto requestDto);
        Task<bool> UpdateBaseImport(UpdateBaseImportRequestDto entity, string currentUser);
        Task<bool> UpdateBaseImportNote(UpdateBaseImportNoteRequestDto entity, string currentUser);
        Task<bool> PayForBaseImport(PayForBaseImportRequestDto entity, string currentUser);
        Task<bool> ChangeBaseImportStatus(Guid importId, string currentUser);
        Task<GetBaseImportDetailsResponseDto> GetBaseImportDetails(Guid baseImportId);
        Task<bool> PayForBaseProductReturn(PayForBaseProductReturnRequestDto entity, string currentUser);
    }
}
