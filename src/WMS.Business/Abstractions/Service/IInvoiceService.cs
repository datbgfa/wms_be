﻿using WMS.Business.Dtos.CommonDto;
using WMS.Business.Dtos.InvoiceDto;

namespace WMS.Business.Abstractions.Service
{
    public interface IInvoiceService
    {
        public Task<IEnumerable<InvoiceResponseDto>> GetBaseInvoiceByImportId(Guid importId);
        public Task<IEnumerable<InvoiceResponseDto>> GetBaseInvoiceByReturnId(IEnumerable<Guid> returnId);
        public Task<IEnumerable<InvoiceResponseDto>> GetBranchInvoiceByReturnId(IEnumerable<Guid> returnId);
        public Task<IEnumerable<InvoiceResponseDto>> GetBaseInvoiceByBranchProductReturnId(IEnumerable<Guid> branchProductReturnId);
        public Task<IEnumerable<InvoiceResponseDto>> GetBaseInvoiceByExportId(Guid exportId);

        public Task<IEnumerable<InvoiceResponseDto>> GetBranchInvoiceByImportId(Guid importId);

        public Task<IEnumerable<InvoiceResponseDto>> GetBranchInvoiceByExportId(Guid exportId);

        public Task<ViewPaging<InvoiceDataResponseDto>> GetBaseInvoice(InvoiceDataRequestDto userRq);
        public Task<ViewPaging<InvoiceDataResponseDto>> GetBranchInvoice(InvoiceDataRequestDto userRq, string currentUser);
        public Task<DashboardResponseDto> ViewDashboard(DateTime? fromRq, DateTime? toRq, string currentUser);
    }
}
