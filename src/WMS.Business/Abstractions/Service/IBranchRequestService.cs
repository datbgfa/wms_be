﻿using WMS.Business.Dtos.BranchRequestDto;
using WMS.Business.Dtos.CommonDto;

namespace WMS.Business.Abstractions.Service
{
    public interface IBranchRequestService
    {
        Task<ViewPaging<GetBranchRequestReponseDto>> FilterBranchRequest(GetBranchRequestListRequestDto requestDto, string currentUser);
        Task<GetBranchRequestReponseDto> GetBranchRequestDetails(Guid id);
    }
}
