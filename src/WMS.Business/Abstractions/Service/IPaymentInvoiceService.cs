﻿using WMS.Business.Dtos.InvoiceDto;

namespace WMS.Business.Abstractions.Service
{
    public interface IPaymentInvoiceService
    {
        Task<bool> CreatePaymentInvoice(CreatePaymentInvoiceRequestDto userRq, string currentUser);

        Task<bool> CreateReceiptInvoice(CreateReceiptInvoiceRequestDto userRq, string currentUser);
    }
}
