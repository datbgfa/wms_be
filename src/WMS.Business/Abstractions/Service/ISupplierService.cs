﻿using Microsoft.AspNetCore.Http;
using WMS.Business.Dtos.CommonDto;
using WMS.Business.Dtos.SupplierDto;

namespace WMS.Business.Abstractions.Service
{
    public interface ISupplierService
    {
        Task<ViewPaging<SupplierResponseDto>> GetSuppliersList(SupplierRequestDto entity);

        Task<SupplierResponseDto?> GetBySupplierId(int id);

        Task<bool> AddSupplier(AddSupplierRequestDto newSupplierDto, IFormFile logoSupplier);

        Task<bool> UpdateSupplier(AddSupplierRequestDto entity, IFormFile logoSupplier);

        Task<bool> UpdateStatus(UpdateSupplierStatusRequestDto entity);

        Task<List<SupplierHistoryImportResponseDto>> GetHistoryImport(int supplierId);

        Task<List<SupplierDebtResponseDto>> GetDebts(int supplierId);

        Task<List<SupplierHaveDebtResponseDto>> GetSupplierHaveDebt();
        Task<IEnumerable<SupplierSimpleResponseDto>> GetAllSuppliers();
        Task<IEnumerable<SupplierResponseDto>> GetAllActiveSuppliers(string? searchWord);
    }
}
