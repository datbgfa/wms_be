﻿using WMS.Business.Dtos.BranchWarehouseDto;
using WMS.Business.Dtos.CommonDto;

namespace WMS.Business.Abstractions.Service
{
    public interface IBranchWarehouseService
    {
        Task<ViewPaging<ViewBranchWarehousesListResponseDto>> ViewBranchWarehouseList(ViewBranchWarehousesListRequestDto entity);
        Task<ViewBranchWarehouseDetailsResponseDto> ViewBranchWarehouseDetails(Guid warehouseId);
        Task<bool> UpdateBranchWarehouseDetails(UpdateBranchWarehouseDetailsRequestDto entity);
        Task<bool> CreateBranchWarehouse(CreateBranchWarehouseRequestDto entity);
        Task<IEnumerable<ViewBranchWarehousesListResponseDto>> ViewAllBranchWarehousesList(bool? hasOwner);
        Task<IEnumerable<ViewBranchWarehousesListResponseDto>> GetAllBranchWarehouse();
        Task<List<BranchWarehouseHaveDebtResponseDto>> GetBranchHaveDebt();
    }
}
