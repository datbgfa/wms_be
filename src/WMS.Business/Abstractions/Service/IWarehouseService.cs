﻿using WMS.Business.Dtos.BranchWarehouseDto;
using WMS.Business.Dtos.WarehouseDto;

namespace WMS.Business.Abstractions.Service
{
    public interface IWarehouseService
    {
        Task<ViewWarehouseProfileResponseDto> ViewWarehouseProfile(string currentUser);
        Task<bool> EditWarehouseProfile(EditWarehouseProfileRequestDto newWarehouseInformation, string currentUser);
        Task<Guid> GetWarehouseIdByUsername(string username);
        Task<IEnumerable<ViewBranchWarehousesListResponseDto>> ViewAllBaseWarehousesList();
        Task<ViewBranchWarehousesListResponseDto> GetBranchWarehouseByUsername(string currentUser);
        Task<ViewBranchWarehousesListResponseDto> GetBranchWarehouseByCurrentUser(string currentUser);
    }
}
