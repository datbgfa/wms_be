﻿namespace WMS.Business.Abstractions.Service
{
    public interface IEmailService
    {
        Task<bool> SendEmail(string emailTo, string subject, string message);
    }
}
