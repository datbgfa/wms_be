﻿using WMS.Business.Dtos.CommonDto;
using WMS.Business.Dtos.EmployeeDto;

namespace WMS.Business.Abstractions.Service
{
    public interface IEmployeeService
    {
        Task<ViewPaging<ViewEmployeesListResponseDto>> ViewEmployeesList(ViewEmployeesListRequestDto entity, string currentUser);
        Task<ViewEmployeeDetailsResponseDto> ViewEmployeeDetails(Guid employeeId, string currentUser);
        Task<bool> CreateEmployee(CreateEmployeeRequestDto entity, string currentUser);
        Task<bool> UpdateEmployeeStatus(UpdateEmployeeStatusRequestDto entity, string currentUser);
        Task<IEnumerable<ViewEmployeesListResponseDto>> GetAllBaseEmployee();
        Task<IEnumerable<ViewEmployeesListResponseDto>> GetAllBranchEmployee(string currentUser);
    }
}
