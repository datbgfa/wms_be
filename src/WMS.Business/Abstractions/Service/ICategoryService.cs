﻿using WMS.Business.Dtos.CategoryDto;
using WMS.Business.Dtos.CommonDto;

namespace WMS.Business.Abstractions.Service
{
    public interface ICategoryService
    {
        Task<ViewPaging<CategoryResponseDto>> GetAllWithPaging(CategoryRequestDto entity);

        Task<bool> AddCategory(string name);

        Task<bool> UpdateCategory(UpdateCategoryDto category);
        Task<IEnumerable<CategoryResponseDto>> GetAll();
    }
}
