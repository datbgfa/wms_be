﻿using WMS.Business.Dtos.BaseExportDto;
using WMS.Business.Dtos.CommonDto;
using WMS.Business.Dtos.WarehouseDto;

namespace WMS.Business.Abstractions.Service
{
    public interface IBaseExportService
    {
        Task<bool> PayForBaseExport(PayForBaseExportRequestDto entity, string currentUser);
        Task<bool> UpdateBaseExport(UpdateBaseExportRequestDto entity, string currentUser);
        Task<ViewPaging<GetBaseExportResponseDto>> GetExportsList(GetBaseExportRequestDto requestDto);
        Task<GetBaseExportResponseDetailsDto> GetExportsDetails(Guid exportId);
    }
}
