﻿using WMS.Business.Dtos.BranchImportDto;
using WMS.Business.Dtos.BranchWarehouseDto;
using WMS.Business.Dtos.CommonDto;

namespace WMS.Business.Abstractions.Service
{
    public interface IBranchImportService
    {
        Task<Guid?> CreateBranchImportRequest(CreateBranchImportRequestRequestDto entity, string currentUser);
        Task<bool> UpdateBranchImportRequest(UpdateBranchImportRequestRequestDto entity, string currentUser);
        Task<bool> ApproveBranchImportRequest(Guid requestId, string currentUser);
        Task<bool> RefuseBranchImportRequest(Guid requestId, string currentUser);
        Task<bool> ChangeBranchImportStatus(Guid importId, string currentUser);
        Task<ViewPaging<GetBranchImportResponseDto>> GetBranchImportList(GetBranchImportRequestDto requestDto, string currentUser);
        Task<GetBranchImportDetailsResponseDto> GetBranchImportDetails(Guid branchImportId);
        Task<bool> PayForBranchProductReturn(PayForBranchProductReturnRequestDto entity, string currentUser);
    }
}
