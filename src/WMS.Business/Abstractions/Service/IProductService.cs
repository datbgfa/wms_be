﻿using Microsoft.AspNetCore.Http;
using WMS.Business.Dtos.CommonDto;
using WMS.Business.Dtos.ProductDto;
using WMS.Business.Dtos.ProductReturnFromBranchDto;

namespace WMS.Business.Abstractions.Service
{
    public interface IProductService
    {
        Task<GetProductDetailResponseDto?> GetProductDetail(Guid productId);

        Task<ViewPaging<GetProductListResponseDto>> GetProductList(GetProductListRequestDto requestDto);

        Task<bool> CreateProduct(CreateProductRequestDto newProductDto, IFormFile imageProduct);

        Task<bool> UpdateProductDetail(UpdateProductDetailRequestDto requestDto);

        Task<bool> CreateBaseReturn(CreateProducReturnSupplierRequestDto userRq, string currentUser);

        Task<ViewPaging<GetProductsReturnFromBranchResponseDto>> GetProductReturnFromBranch(GetProductReturnFromBranchRequestDto requestDto);

        Task<List<GetProductsReturnFromBranchResponseDto>> GetProductReturnFromBranchByImportId(Guid importId);

        Task<ViewPaging<ProductReturnSupplierResponseDto>> GetProductReturnToSupplier(PagingRequest pagingRequest);

        Task<IEnumerable<ProductReturnSupplierResponseDto>> GetProductReturnToSupplierByImportId(Guid importId);

        Task<IEnumerable<SearchProductByNameResponseDto>> SearchProductByName(string name);

        Task<ViewPaging<GetStockQuantityResponseDto>> GetAllStockQuantity(GetStockQuantityRequestDto entity, string currentUser);

        Task<List<GetAllImportsByIdResponseDto>> GetAllStockQuantityProductId(GetStockQuantityByIdRequestDto entity, string currentUser);

        Task<bool> UpdateBranchReturn(UpdateStatusProductReturnFromBranchRequestDto entity);

        Task<IEnumerable<GetBranchProductInStockByNameResponseDto>> GetBranchProductInStockByName(string searchWord, string currentUser);

        Task<bool> CreateBranchReturn(CreateProductReturnFromBranchToBaseRequestDto userRq);
        Task<ViewPaging<GetProductListResponseDto>> GetBranchWarehouseProducts(string currentUser, GetProductListRequestDto requestDto);
        Task<GetProductDetailResponseDto> GetBranchWarehouseProductDetails(string currentUser, Guid productId);
        Task<bool> UpdateBranchWarehouseProduct(string currentUser, Guid productId, decimal exportPrice);
    }
}
