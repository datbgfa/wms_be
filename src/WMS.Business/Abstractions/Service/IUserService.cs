﻿using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using WMS.Business.Dtos.CommonDto;
using WMS.Business.Dtos.UserDto;
using WMS.Domain.Entities;

namespace WMS.Business.Abstractions.Service
{
    public interface IUserService
    {
        Task<WmsUser?> FindUserById(Guid userId);
        Task<ProfileInformationResponseDto?> GetProfileInformationByUsername(string username);
        Task<bool?> EditProfileInformationByUsername(EditProfileInformationRequestDto editProfile, IFormFile AvatarImage, string currentUser);
        Task<WmsUser?> FindUserByUsername(string username);
        Task<WmsUser?> FindUserByEmail(string email);
        Task AccountRegister();
        Task<WmsUser?> AccountLogin(UserLoginRequestDto rqDto);
        HashedPasswordDto GenerateHashedPassword(string originalPassword);
        Task<ViewPaging<ViewAccountsListResponseDto>> ViewAccountsList(ViewAccountsListRequestDto entity);
        Task<bool> ResetPassword(ResetPasswordRequestDto entity);
        Task<bool> ChangePassword(ChangePasswordRequestDto entity);
        Task<bool> Logout(string username);
        Task<Guid> UpdateUserStatus(ChangeUserStatusRequestDto userRq);
        Task<Guid?> CreateAccount(CreateUserAccountRequestDto userRq);
        Task<IEnumerable<UserRoleResponseDto>> GetUserRoleByUsername(string? username);
        Task<ClaimsIdentity?> GetUserPermissionsIdentity(string username);
    }
}
