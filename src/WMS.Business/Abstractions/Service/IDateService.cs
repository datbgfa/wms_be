﻿using WMS.Business.Dtos.FilterDto;

namespace WMS.Business.Abstractions.Service
{
    public interface IDateService
    {
        public DateFilterRequestDto GetDateTimeByDateFilter(short dateFilter);
    }
}
