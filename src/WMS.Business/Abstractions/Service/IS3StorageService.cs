﻿using WMS.Business.Dtos.S3Dto;

namespace WMS.Business.Abstractions.Service
{
    public interface IS3StorageService
    {
        public Task<bool> UploadFileAsync(S3RequestData obj);
        public Task<bool> DeleteFileAsync(S3RequestData obj);
        public string GetFileUrl(S3RequestData obj);
        public Task<bool> UploadFolderAsync(S3RequestDirectory obj);
        public Task<Stream> DownloadFileContent(S3RequestDirectory obj);
        public Task<bool> DeleteDirectory(S3RequestDirectory obj);
    }
}
