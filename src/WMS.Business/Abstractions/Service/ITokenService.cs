﻿using System.Security.Claims;
using WMS.Domain.Entities;

namespace WMS.Business.Abstractions.Service
{
    public interface ITokenService
    {
        string GenerateAccessToken(IEnumerable<Claim> claims);
        string GenerateRefreshToken();
        ClaimsPrincipal GetPrincipalFromExpriedToken(string token);
        Task<bool> SaveRefreshToken(Guid userId, string refreshToken, string accessToken, DateTime expTime);
        Task<WmsToken?> GetTokenByUserId(Guid userId);
        bool RevokeAccessToken(string? token);
        bool IsBlacklistedToken(string? token);
        Task<WmsToken?> IsValidRefreshToken(string refreshToken);
        Task<bool> UpdateAccessToken(Guid userId, string accessToken);
    }
}
