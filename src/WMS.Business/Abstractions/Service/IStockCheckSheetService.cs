﻿using WMS.Business.Dtos.CommonDto;
using WMS.Business.Dtos.StockCheckSheetDto;

namespace WMS.Business.Abstractions.Service
{
    public interface IStockCheckSheetService
    {
        Task<ViewPaging<StockCheckSheetResponseDto>> GetAllStockCheckSheet(StockCheckSheetRequestDto entity, string currentUser);
        Task<StockCheckSheetResponseDto> GetStockCheckSheetById(StockCheckSheetDetailsRequestDto entity, string currentUser);
        Task<List<StockCheckSheetDetailsResponseDto>> GetAllStockCheckSheetDetails(StockCheckSheetDetailsRequestDto entity, string currentUser);
        Task<List<StockCheckSheetImportsResponseDto>> GetAllStockCheckSheetImports(StockCheckSheetImportsRequestDto entity, string currentUser);
        Task<bool> AddStockCheckSheet(CreateStockCheckSheetRequestDto entity, string currentUser);
        Task<List<GetImportsByProductResponseDto>> GetImportsByProductId(StockCheckSheetImportsRequestDto entity, string currentUser);
    }
}
