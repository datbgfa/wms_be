﻿using WMS.Business.Dtos.BaseExportDto;
using WMS.Business.Dtos.BranchExportDto;
using WMS.Business.Dtos.CommonDto;

namespace WMS.Business.Abstractions.Service
{
    public interface IBranchExportService
    {
        Task<ViewPaging<GetBranchExportResponseDto>> GetExportsList(GetBranchExportRequestDto requestDto, string currentUser);
        Task<GetBranchExportResponseDto> GetExportsDetails(Guid exportId);
        Task<Guid> CreateBranchExport(CreateBranchExportRequestDto requestDto, string currentUser);
    }
}
