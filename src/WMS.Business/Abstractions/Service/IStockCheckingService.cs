﻿using WMS.Business.Dtos.StockCheckSheetDto;

namespace WMS.Business.Abstractions.Service
{
    public interface IStockCheckingService
    {
        Task<IEnumerable<GetProductByWarehouseIdAndProductIdResponseDto>> GetProductQuantity(IEnumerable<Guid> productIds, string currentUser);
    }
}
