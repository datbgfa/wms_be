﻿namespace WMS.Business.Dtos.UserDto
{
    public class UserLoginResponseDto
    {
        public UserLoginResponseDto(string accessToken, string refreshToken)
        {
            AccessToken = accessToken;
            RefreshToken = refreshToken;
        }

        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
    }
}
