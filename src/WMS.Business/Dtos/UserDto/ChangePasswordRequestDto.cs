﻿using System.ComponentModel.DataAnnotations;

namespace WMS.Business.Dtos.UserDto
{
    public class ChangePasswordRequestDto
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public string OldPassword { get; set; }
        [Required]
        //[RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,32}$",
        // ErrorMessage = "Password must be 8-32 characters, has number, uppercase letter and special character")]
        public string NewPassword { get; set; }
        [Required]
        [Compare(nameof(NewPassword), ErrorMessage = "New passwords mismatch")]
        public string ReEnterNewPassword { get; set; }

        public ChangePasswordRequestDto()
        {
        }

        public ChangePasswordRequestDto(string username, string oldPassword, string newPassword, string reEnterNewPassword)
        {
            Username = username;
            OldPassword = oldPassword;
            NewPassword = newPassword;
            ReEnterNewPassword = reEnterNewPassword;
        }
    }
}
