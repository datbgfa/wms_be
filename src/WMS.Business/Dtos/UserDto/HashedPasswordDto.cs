﻿namespace WMS.Business.Dtos.UserDto
{
    public class HashedPasswordDto
    {
        public string HashedPassword { get; set; }
        public string SaltKey { get; set; }

        public HashedPasswordDto(string hashedPassword, string saltKey)
        {
            HashedPassword = hashedPassword;
            SaltKey = saltKey;
        }
    }
}
