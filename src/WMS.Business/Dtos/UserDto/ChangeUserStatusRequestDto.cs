﻿using System.ComponentModel.DataAnnotations;
using WMS.Domain.Enums;

namespace WMS.Business.Dtos.UserDto
{
    public class ChangeUserStatusRequestDto
    {
        [Required]
        public string Username { get; set; } = null!;

        [Required]
        [EnumDataType(typeof(UserStatusEnum))]
        public short UserStatus { get; set; }
    }
}
