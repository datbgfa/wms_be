﻿using System.ComponentModel.DataAnnotations;

namespace WMS.Business.Dtos.UserDto
{
    public class EditProfileInformationRequestDto
    {
        [Required]
        public string Fullname { get; set; }
        [Required]
        public string Email { get; set; }
        public bool Gender { get; set; }
        [Required]
        public string? PhoneNumber { get; set; }
        public string Birthdate { get; set; }
        public string Address { get; set; }
    }
}
