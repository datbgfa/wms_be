﻿using System.ComponentModel.DataAnnotations;

namespace WMS.Business.Dtos.UserDto
{
    public class UserLoginRequestDto
    {
        [Required(ErrorMessage = "Username is required!")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Password is required!")]
        public string Password { get; set; }

        public UserLoginRequestDto(string userName, string password)
        {
            UserName = userName;
            Password = password;
        }
    }
}
