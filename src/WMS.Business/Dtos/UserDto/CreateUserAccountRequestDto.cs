﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace WMS.Business.Dtos.UserDto
{
    public class CreateUserAccountRequestDto
    {
        [Required]
        [MaxLength(100)]
        public string Username { get; set; } = null!;

        [Required]
        public string Fullname { get; set; } = null!;

        [Required]
        public bool Gender { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; } = null!;

        [Required]
        public string Address { get; set; } = null!;

        [Required]
        public string PhoneNumber { get; set; } = null!;

        [Required]
        public DateTime Birthdate { get; set; }
        public Guid? Supervisor { get; set; }
        public DateTime? JoinDate { get; set; }
        public IFormFile? AvatarImage { get; set; }

        public int RoleId { get; set; }
        public Guid? WarehouseId { get; set; }
    }
}
