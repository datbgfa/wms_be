﻿namespace WMS.Business.Dtos.UserDto
{
    public class ProfileInformationResponseDto
    {
        public Guid UserId { get; set; }
        public string Username { get; set; }
        public string Fullname { get; set; }
        public string Email { get; set; }
        public bool Gender { get; set; }
        public string PhoneNumber { get; set; }
        public DateOnly Birthdate { get; set; }
        public string Address { get; set; }
        public string Avatar { get; set; }
    }
}
