﻿using System.ComponentModel.DataAnnotations;

namespace WMS.Business.Dtos.UserDto
{
    public class ResetPasswordRequestDto
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public string Email { get; set; }

        public ResetPasswordRequestDto()
        {
        }

        public ResetPasswordRequestDto(string username, string email)
        {
            Username = username;
            Email = email;
        }
    }
}
