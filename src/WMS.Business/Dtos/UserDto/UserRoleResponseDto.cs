﻿namespace WMS.Business.Dtos.UserDto
{
    public class UserRoleResponseDto
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; } = null!;
        public string? RoleDescription { get; set; }
    }
}
