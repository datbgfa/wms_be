﻿using System.ComponentModel.DataAnnotations;

namespace WMS.Business.Dtos.UserDto
{
    public class RefreshTokenRequestDto
    {
        [Required]
        public string RefreshToken { get; set; } = null!;
    }
}
