﻿namespace WMS.Business.Dtos.EmployeeDto
{
    public class UpdateEmployeeStatusRequestDto
    {
        public Guid EmployeeId { get; set; }
        public short Status { get; set; }
    }
}
