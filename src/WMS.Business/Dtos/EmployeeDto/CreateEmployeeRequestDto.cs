﻿using System.ComponentModel.DataAnnotations;
using WMS.Business.Dtos.UserDto;

namespace WMS.Business.Dtos.EmployeeDto
{
    public class CreateEmployeeRequestDto
    {
        [Required]
        public CreateUserAccountRequestDto CreateUser { get; set; } = null!;
    }
}
