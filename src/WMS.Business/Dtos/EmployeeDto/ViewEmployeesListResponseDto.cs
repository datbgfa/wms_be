﻿namespace WMS.Business.Dtos.EmployeeDto
{
    public class ViewEmployeesListResponseDto
    {
        public Guid UserId { get; set; }
        public string Username { get; set; } = null!;
        public string Fullname { get; set; } = null!;
        public bool Gender { get; set; }
        public string Email { get; set; } = null!;
        public string Address { get; set; } = null!;
        public string PhoneNumber { get; set; } = null!;
        public DateTime Birthdate { get; set; }
        public short Status { get; set; }
        public Guid? Supervisor { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime JoinDate { get; set; }
        public string Avatar { get; set; } = null!;
        public string? AvatarImage { get; set; }
        public string? Role { get; set; }
        public string? WarehouseName { get; set; }
    }
}
