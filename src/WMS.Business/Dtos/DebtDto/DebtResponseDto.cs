﻿namespace WMS.Business.Dtos.DebtDto
{
    public class DebtResponseDto
    {
        public Guid DebtId { get; set; }
        public int CreditorId { get; set; }
        public Guid DebtorId { get; set; }
        public decimal DebtAmount { get; set; }
    }
}
