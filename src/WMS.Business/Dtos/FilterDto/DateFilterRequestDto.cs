﻿namespace WMS.Business.Dtos.FilterDto
{
    public class DateFilterRequestDto
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public DateFilterRequestDto(DateTime startDate, DateTime endDate)
        {
            StartDate = startDate;
            EndDate = endDate;
        }

        public DateFilterRequestDto()
        {
        }
    }
}
