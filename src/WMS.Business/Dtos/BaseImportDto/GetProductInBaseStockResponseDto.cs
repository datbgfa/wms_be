﻿namespace WMS.Business.Dtos.BaseImportDto
{
    public class GetProductInBaseStockResponseDto
    {
        public Guid ProductId { get; set; }
        public string SKU { get; set; } = null!;
        public string ProductName { get; set; } = null!;
        public string? ProductDescription { get; set; }
        public string ProductImage { get; set; } = null!;
        public string ProductImageLink { get; set; } = null!;
        public decimal UnitPrice { get; set; }
        public decimal SalePrice { get; set; }
        public decimal ExportPrice { get; set; }

        public Guid BaseImportId { get; set; }
        public int StockQuantity { get; set; }
    }
}
