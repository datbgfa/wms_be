﻿namespace WMS.Business.Dtos.BaseImportDto
{
    public class GetBaseImportResponseDto
    {
        public Guid ImportId { get; set; }
        public string ImportCode { get; set; } = null!;
        public string? NameAssignBy { get; set; }
        public string? NameImportBy { get; set; }
        public DateTime ImportedDate { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal TotalCost { get; set; }
        public short ImportStatus { get; set; }
        public short PaymentStatus { get; set; }
        public string? SupplierName { get; set; }
        public IEnumerable<BaseImportProductResponseDto>? WmsBaseImportProducts { get; set; }
    }
    public class BaseImportProductResponseDto
    {
        public string? ProductName { get; set; }
        public string? ProductImage { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal CostPrice { get; set; }
        public int ImportedQuantity { get; set; }
        public int StockQuantity { get; set; }

    }
}
