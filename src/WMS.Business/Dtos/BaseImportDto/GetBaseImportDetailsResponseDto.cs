﻿using WMS.Business.Dtos.InvoiceDto;
using WMS.Business.Dtos.ProductDto;

namespace WMS.Business.Dtos.BaseImportDto
{
    public class GetBaseImportDetailsResponseDto
    {
        public Guid ImportId { get; set; }
        public string ImportCode { get; set; } = null!;
        public string? NameAssignBy { get; set; }
        public string? NameImportBy { get; set; }
        public Guid ImportBy { get; set; }
        public int SupplierId { get; set; }
        public DateTime ImportedDate { get; set; }
        public DateTime RequestDate { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal ExtraCost { get; set; }
        public decimal PaidAmount { get; set; }
        public short ImportStatus { get; set; }
        public short PaymentStatus { get; set; }
        public string? Note { get; set; }
        public Guid AssignBy { get; set; }
        public IEnumerable<BaseImportProductDetailsResponseDto>? WmsBaseImportProducts { get; set; }
        public IEnumerable<ProductReturnSupplierResponseDto> ProductReturns { get; set; }
        public IEnumerable<InvoiceResponseDto> Invoices { get; set; }
    }
    public class BaseImportProductDetailsResponseDto
    {
        public Guid ProductId { get; set; }
        public string? ProductName { get; set; }
        public string? ProductImage { get; set; }
        public decimal CostPrice { get; set; }
        public decimal UnitPrice { get; set; }
        public int ImportedQuantity { get; set; }
        public int StockQuantity { get; set; }
    }
}
