﻿using System.ComponentModel.DataAnnotations;

namespace WMS.Business.Dtos.BaseImportDto
{
    public class PayForBaseProductReturnRequestDto
    {
        [Required]
        public Guid BaseProductReturnId { get; set; }
        public decimal PayAmount { get; set; }
    }
}
