﻿using System.ComponentModel.DataAnnotations;
using WMS.Business.Dtos.CommonDto;

namespace WMS.Business.Dtos.BranchRequestDto
{
    public class GetBranchRequestListRequestDto
    {
        public string? SearchWord { get; set; }
        [Required]
        public PagingRequest PagingRequest { get; set; } = null!;
        public short? ApprovedSatus { get; set; }
        /// <summary>
        /// None = 0
        /// ThisWeek = 1
        /// ThisMonth = 2
        /// ThisYear = 3
        /// </summary>
        public short DateFilter { get; set; } = 0;
    }
}
