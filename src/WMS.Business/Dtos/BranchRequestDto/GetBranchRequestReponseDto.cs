﻿namespace WMS.Business.Dtos.BranchRequestDto
{
    public class GetBranchRequestReponseDto
    {
        public Guid RequestId { get; set; }
        public string RequestCode { get; set; } = null!;
        public string? NameApprovedBy { get; set; }
        public string NameRequestBy { get; set; }
        public DateTime RequestDate { get; set; }
        public decimal TotalPrice { get; set; }
        public short ApprovalStatus { get; set; }
        public Guid? ApprovedBy { get; set; }
        public Guid RequestBy { get; set; }
        public virtual ICollection<BranchProductRequestReponseDto>? WmsBranchProductRequests { get; set; }
    }
    public class BranchProductRequestReponseDto
    {
        public Guid ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductImage { get; set; }
        public decimal UnitPrice { get; set; }
        public int RequestQuantity { get; set; }
        public decimal TotalPrice { get; set; }
    }
}
