﻿using System.ComponentModel.DataAnnotations;

namespace WMS.Business.Dtos.WarehouseDto
{
    public class UpdateBaseExportRequestDto
    {
        [Required]
        public Guid ExportId { get; set; }
        public Guid? ExportBy { get; set; }
        [Required]
        public DateTime ExportDate { get; set; }
        public decimal ExtraCost { get; set; }
        public short ExportStatus { get; set; }
        public IEnumerable<UpdateBaseExportProductRequestDto> ProductsList { get; set; }
    }
}
