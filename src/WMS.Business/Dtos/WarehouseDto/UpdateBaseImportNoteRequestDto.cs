﻿using System.ComponentModel.DataAnnotations;

namespace WMS.Business.Dtos.WarehouseDto
{
    public class UpdateBaseImportNoteRequestDto
    {
        [Required]
        public Guid ImportId { get; set; }
        public string? Note { get; set; }
    }
}
