﻿using System.ComponentModel.DataAnnotations;

namespace WMS.Business.Dtos.WarehouseDto
{
    public class PayForBaseImportRequestDto
    {
        [Required]
        public Guid ImportId { get; set; }
        [Range(0, double.MaxValue)]
        public decimal PayAmount { get; set; }
    }
}
