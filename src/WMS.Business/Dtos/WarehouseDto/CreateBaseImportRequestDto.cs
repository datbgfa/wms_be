﻿using System.ComponentModel.DataAnnotations;

namespace WMS.Business.Dtos.WarehouseDto
{
    public class CreateBaseImportRequestDto
    {
        [Required]
        public Guid ImportBy { get; set; }
        public int SupplierId { get; set; }
        public DateTime RequestDate { get; set; }
        [Range(0, double.MaxValue)]
        public decimal ExtraCost { get; set; }
        public string? Note { get; set; }
        public IEnumerable<CreateBaseImportProductRequestDto> ProductsList { get; set; }
    }
}
