﻿namespace WMS.Business.Dtos.WarehouseDto
{
    public class ViewWarehouseProfileResponseDto
    {
        public Guid WarehouseId { get; set; }
        public string WarehouseName { get; set; } = null!;
        public string NameOfBranchWarehouseDirector { get; set; } = null!;
        public string PhoneNumber { get; set; } = null!;
        public string Email { get; set; } = null!;
        public string Address { get; set; } = null!;
    }
}
