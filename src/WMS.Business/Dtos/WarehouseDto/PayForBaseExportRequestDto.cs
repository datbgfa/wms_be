﻿using System.ComponentModel.DataAnnotations;

namespace WMS.Business.Dtos.WarehouseDto
{
    public class PayForBaseExportRequestDto
    {
        [Required]
        public Guid ExportId { get; set; }
        [Range(0, double.MaxValue)]
        public decimal PayAmount { get; set; }
    }
}
