﻿using System.ComponentModel.DataAnnotations;

namespace WMS.Business.Dtos.WarehouseDto
{
    public class UpdateBaseExportProductRequestDto
    {
        [Required]
        public Guid ProductId { get; set; }
        public int Quantity { get; set; }
    }
}
