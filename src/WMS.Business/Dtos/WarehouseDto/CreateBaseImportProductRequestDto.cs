﻿using System.ComponentModel.DataAnnotations;

namespace WMS.Business.Dtos.WarehouseDto
{
    public class CreateBaseImportProductRequestDto
    {
        [Required]
        public Guid ProductId { get; set; }
        [Range(0, double.MaxValue)]
        public decimal UnitPrice { get; set; }
        [Range(0, int.MaxValue)]
        public int ImportedQuantity { get; set; }
    }
}
