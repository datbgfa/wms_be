﻿namespace WMS.Business.Dtos.S3Dto
{
    public class S3RequestDirectory
    {
        public string BucketName { get; set; } = null!;
        public string Directory { get; set; } = null!;
        public string Prefix { get; set; } = null!;
    }
}
