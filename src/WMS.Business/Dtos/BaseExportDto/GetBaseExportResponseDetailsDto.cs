﻿using WMS.Business.Dtos.InvoiceDto;
using WMS.Business.Dtos.ProductDto;

namespace WMS.Business.Dtos.BaseExportDto
{
    public class GetBaseExportResponseDetailsDto
    {
        public Guid ExportId { get; set; }
        public string ExportCode { get; set; } = null!;
        public Guid BranchWarehouseId { get; set; }
        public string? NameBranchWarehouse { get; set; }
        public short WarehouseStatus { get; set; }
        public string? NameAssignBy { get; set; }
        public string? NameExportBy { get; set; }
        public DateTime ExportDate { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal ExtraCost { get; set; }
        public decimal PaidAmount { get; set; }
        public decimal TotalCost { get; set; }
        public short PaymentStatus { get; set; }
        public short ExportStatus { get; set; }
        public Guid? AssignBy { get; set; }
        public Guid? ExportBy { get; set; }
        public IEnumerable<BaseExportProductResponseDto>? WmsBaseExportProducts { get; set; }
        public DebtBranchResponseDto DebtBranchResponseDto { get; set; }
        public List<GetProductsReturnFromBranchResponseDto> ProductReturnFromBranch { get; set; }
        public IEnumerable<InvoiceResponseDto> Invoices { get; set; }

        public GetBaseExportResponseDetailsDto()
        {
        }
    }
    public class DebtBranchResponseDto
    {
        public decimal DebtAmount { get; set; }
        public int TotalImport { get; set; }
        public int TotalReturnProduct { get; set; }
    }
}
