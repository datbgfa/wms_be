﻿namespace WMS.Business.Dtos.BaseExportDto
{
    public class GetBaseExportResponseDto
    {
        public Guid ExportId { get; set; }
        public string ExportCode { get; set; } = null!;
        public string? NameBranchWarehouse { get; set; }
        public string? NameAssignBy { get; set; }
        public string? NameExportBy { get; set; }
        public DateTime ExportDate { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal ExtraCost { get; set; }
        public decimal PaidAmount { get; set; }
        public decimal TotalCost { get; set; }
        public short PaymentStatus { get; set; }
        public short ExportStatus { get; set; }
        public int NumberOfProductReturnRequested { get; set; }
        public IEnumerable<BaseExportProductResponseDto>? WmsBaseExportProducts { get; set; }

        public GetBaseExportResponseDto()
        {
        }

        public GetBaseExportResponseDto(Guid exportId, string? nameBranchWarehouse, string? nameAssignBy, string? nameExportBy, DateTime exportDate, decimal totalPrice, decimal extraCost, decimal paidAmount, decimal totalCost, short paymentStatus, short exportStatus, int numberOfProductReturnRequested, IEnumerable<BaseExportProductResponseDto>? wmsBaseExportProducts)
        {
            ExportId = exportId;
            NameBranchWarehouse = nameBranchWarehouse;
            NameAssignBy = nameAssignBy;
            NameExportBy = nameExportBy;
            ExportDate = exportDate;
            TotalPrice = totalPrice;
            ExtraCost = extraCost;
            PaidAmount = paidAmount;
            TotalCost = totalCost;
            PaymentStatus = paymentStatus;
            ExportStatus = exportStatus;
            NumberOfProductReturnRequested = numberOfProductReturnRequested;
            WmsBaseExportProducts = wmsBaseExportProducts;
        }
    }

}
