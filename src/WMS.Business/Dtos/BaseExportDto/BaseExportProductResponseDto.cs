﻿namespace WMS.Business.Dtos.BaseExportDto
{
    public class BaseExportProductResponseDto
    {
        public Guid ProductId { get; set; }
        public string? ProductName { get; set; }
        public string? ProductImage { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal CostPrice { get; set; }
        public decimal TotalPrice { get; set; }
        public int Quantity { get; set; }
        public IEnumerable<BaseExportProductDetailsResponseDto> ExportProductDetails { get; set; }

        public BaseExportProductResponseDto()
        {
        }

        public BaseExportProductResponseDto(Guid productId, string? productName, string? productImage, decimal unitPrice, decimal costPrice, decimal totalPrice, int quantity)
        {
            ProductId = productId;
            ProductName = productName;
            ProductImage = productImage;
            UnitPrice = unitPrice;
            CostPrice = costPrice;
            TotalPrice = totalPrice;
            Quantity = quantity;
        }
    }



}
