﻿namespace WMS.Business.Dtos.BaseExportDto
{
    public class BaseExportProductDetailsResponseDto
    {
        public Guid ImportId { get; set; }
        public int Quantity { get; set; }
        public string ImportCode { get; set; }
        public string ExportCodeItem { get; set; }

    }
}
