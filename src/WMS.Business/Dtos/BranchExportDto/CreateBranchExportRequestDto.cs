﻿namespace WMS.Business.Dtos.BranchExportDto
{
    public class CreateBranchExportRequestDto
    {
        public Guid ExportBy { get; set; }
        public DateTime ExportDate { get; set; }

        public IEnumerable<CreateBranchExportProductDto>? BranchExportProducts { get; set; }
    }
    public class CreateBranchExportProductDto
    {
        public Guid ProductId { get; set; }
        public int Quantity { get; set; }
    }
}
