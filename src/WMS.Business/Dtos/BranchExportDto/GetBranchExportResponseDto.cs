﻿using WMS.Business.Dtos.InvoiceDto;

namespace WMS.Business.Dtos.BaseExportDto
{
    public class GetBranchExportResponseDto
    {
        public Guid ExportId { get; set; }
        public string ExportCode { get; set; } = null!;
        public string? NameAssignBy { get; set; }
        public string? NameExportBy { get; set; }
        public DateTime ExportDate { get; set; }
        public decimal TotalPrice { get; set; }

        public short ApprovalStatus { get; set; }
        public Guid AssignBy { get; set; }
        public Guid ExportBy { get; set; }
        public IEnumerable<BranchExportProductResponseDto>? WmsBranchExportProducts { get; set; }
        public IEnumerable<InvoiceResponseDto> Invoices { get; set; }
    }
    public class BranchExportProductResponseDto
    {
        public Guid ProductId { get; set; }
        public string? ProductName { get; set; }
        public string? ProductImage { get; set; }
        public decimal UnitPrice { get; set; }
        public int Quantity { get; set; }
        public decimal TotalPrice { get; set; }
        public IEnumerable<BranchExportProductDetailsResponseDto> BranchExportProductDetails { get; set; }

        public BranchExportProductResponseDto()
        {
        }

        public BranchExportProductResponseDto(Guid productId, string? productName, string? productImage, decimal unitPrice, int quantity, decimal totalPrice)
        {
            ProductId = productId;
            ProductName = productName;
            ProductImage = productImage;
            UnitPrice = unitPrice;
            Quantity = quantity;
            TotalPrice = totalPrice;
        }
    }
    public class BranchExportProductDetailsResponseDto
    {
        public int Quantity { get; set; }
        public Guid ImportId { get; set; }
        public string ImportCode { get; set; }
    }
}
