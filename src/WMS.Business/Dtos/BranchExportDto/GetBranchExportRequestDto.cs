﻿using System.ComponentModel.DataAnnotations;
using WMS.Business.Dtos.CommonDto;

namespace WMS.Business.Dtos.BaseExportDto
{
    public class GetBranchExportRequestDto
    {
        public string? SearchWord { get; set; }
        [Required]
        public PagingRequest PagingRequest { get; set; } = null!;
        /// <summary>
        /// None = 0
        /// ThisWeek = 1
        /// ThisMonth = 2
        /// ThisYear = 3
        /// </summary>
        public short DateFilter { get; set; } = 0;

    }
}
