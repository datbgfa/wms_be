﻿namespace WMS.Business.Dtos.GlobalResponseDto
{
    public class ApiFormatResponse
    {
        public int Code { get; set; }
        public bool Success { get; set; }
        public object? MetaData { get; set; }

        public ApiFormatResponse(int code, bool success, object? metaData)
        {
            Code = code;
            Success = success;
            MetaData = metaData;
        }
    }
}
