﻿using System.ComponentModel.DataAnnotations;
using WMS.Business.Dtos.CommonDto;

namespace WMS.Business.Dtos.CategoryDto
{
    public class CategoryRequestDto
    {
        public string? SearchWord { get; set; }
        [Required]
        public PagingRequest PagingRequest { get; set; } = null!;
        public bool? Visible { get; set; }
    }
}
