﻿using System.ComponentModel.DataAnnotations;

namespace WMS.Business.Dtos.CategoryDto
{
    public class AddCategoryDto
    {
        public string CategoryName { get; set; }
    }

    public class UpdateCategoryDto
    {
        public int CategoryId { get; set; }

        [Required]
        [MaxLength(100)]
        public string CategoryName { get; set; }
        public bool Visible { get; set; }

        public UpdateCategoryDto()
        {
        }

        public UpdateCategoryDto(int categoryId, string categoryName, bool visible)
        {
            CategoryId = categoryId;
            CategoryName = categoryName;
            Visible = visible;
        }
    }
}
