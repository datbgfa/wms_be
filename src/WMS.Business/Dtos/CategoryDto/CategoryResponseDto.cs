﻿namespace WMS.Business.Dtos.CategoryDto
{
    public class CategoryResponseDto
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public bool Visible { get; set; }

    }
}
