﻿namespace WMS.Business.Dtos.CommonDto
{
    public class ViewPaging<T> where T : class
    {
        public IEnumerable<T> Items { get; set; }
        public Pagination Pagination { get; set; }

        public ViewPaging(IEnumerable<T> items, Pagination pagination)
        {
            Items = items;
            Pagination = pagination;
        }
    }
}
