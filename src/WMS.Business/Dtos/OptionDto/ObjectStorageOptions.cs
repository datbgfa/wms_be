﻿namespace WMS.Business.Dtos.OptionDto
{
    public class ObjectStorageOptions
    {
        public const string Section = "ObjectStorage";

        public string? AccessId { get; set; }
        public string? AccessKey { get; set; }
        public string? Url { get; set; }
        public string? Bucket { get; set; }
    }
}
