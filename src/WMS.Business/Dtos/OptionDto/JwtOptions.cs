﻿namespace WMS.Business.Dtos.OptionDto
{
    public class JwtOptions
    {
        public const string Section = "Jwt";

        public string? ValidAudience { get; set; }
        public string? ValidIssuer { get; set; }
        public string? AccessTokenSecret { get; set; }
        public string? RefreshTokenSecret { get; set; }
        public double RefreshTokenExpriedTimeInDays { get; set; }
        public double AccessTokenExpriedTimeInMinutes { get; set; }
    }
}
