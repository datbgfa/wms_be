﻿namespace WMS.Business.Dtos.StockCheckSheetDto
{
    public class GetProductByWarehouseIdAndProductIdResponseDto
    {
        public Guid ProductId { get; set; }
        public string SKU { get; set; } = null!;
        public string ProductName { get; set; } = null!;
        public int TotalQuantity { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal CostPrice { get; set; }
        public decimal ExportPrice { get; set; }
        public bool? Visible { get; set; }
    }
}
