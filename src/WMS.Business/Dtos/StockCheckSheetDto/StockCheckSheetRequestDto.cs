﻿using System.ComponentModel.DataAnnotations;
using WMS.Business.Dtos.CommonDto;

namespace WMS.Business.Dtos.StockCheckSheetDto
{
    public class StockCheckSheetRequestDto
    {
        public int RoleId { get; set; }
        public string? SearchWord { get; set; }
        [Required]
        public PagingRequest PagingRequest { get; set; } = null!;
        public short DateFilter { get; set; } = 0;
    }

    public class StockCheckSheetDetailsRequestDto
    {
        public int RoleId { get; set; }

        public Guid CheckId { get; set; }
    }

    public class StockCheckSheetImportsRequestDto
    {
        public int RoleId { get; set; }

        public Guid CheckId { get; set; }

        public Guid ProductId { get; set; }
    }

    public class StockCheckSheetResponseDto
    {
        public Guid CheckId { get; set; }
        public string CheckBy { get; set; }
        public string StockCheckSheetCode { get; set; }
        public DateTime CheckDate { get; set; }
        public string? Description { get; set; }
        public short Status { get; set; }
    }

    public class StockCheckSheetDetailsResponseDto
    {
        public Guid CheckId { get; set; }
        public Guid ProductId { get; set; }
        public string SKU { get; set; } = null!;
        public string ProductName { get; set; } = null!;
        public string ProductImage { get; set; } = null!;
        public int Quantity { get; set; }
        public int RealQuantity { get; set; }
        public int Difference { get; set; }
    }

    public class StockCheckSheetImportsResponseDto
    {
        public string ImportCode { get; set; } = null!;
        public DateTime ImportedDate { get; set; }
        public int RealQuantity { get; set; }
        public int StockQuantity { get; set; }
        public int Difference { get; set; }
    }

    public class CreateStockCheckSheetRequestDto
    {
        public int RoleId { get; set; }
        public string? Description { get; set; }
        public List<AddStockCheckSheetDetailRequestDto> StockCheckSheetDetails { get; set; } = null!;
    }

    public class AddStockCheckSheetRequestDto
    {
        public Guid CheckId { get; set; }
        public Guid CheckBy { get; set; }
        public DateTime CheckDate { get; set; }
        public string? Description { get; set; }
        public short Status { get; set; }
        public string StockCheckSheetCode { get; set; } = null!;
    }

    public class AddStockCheckSheetDetailRequestDto
    {
        public Guid CheckId { get; set; }
        public Guid ProductId { get; set; }
        public Guid ImportId { get; set; }
        public Guid BaseImportId { get; set; }
        public int RealQuantity { get; set; }
        public int StockQuantity { get; set; }
    }

    public class GetImportsByProductResponseDto
    {
        public Guid ProductId { get; set; }
        public Guid ImportId { get; set; }
        public Guid BaseImportId { get; set; }
        public string ExportCodeItem { get; set; } = null!;
        public string SKU { get; set; } = null!;
        public string ImportCode { get; set; } = null!;
        public string ProductName { get; set; } = null!;
        public string ProductImage { get; set; } = null!;
        public int StockQuantity { get; set; }
    }
}
