﻿using WMS.Business.Dtos.BaseExportDto;
using WMS.Business.Dtos.InvoiceDto;
using WMS.Business.Dtos.ProductDto;

namespace WMS.Business.Dtos.BranchImportDto
{
    public class GetBranchImportDetailsResponseDto
    {
        public Guid ImportId { get; set; }
        public string ImportCode { get; set; } = null!;
        public string? NameAssignBy { get; set; }
        public string? NameImportBy { get; set; }
        public DateTime ImportedDate { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal TotalCost { get; set; }
        public decimal ExtraCost { get; set; }
        public decimal PaidAmount { get; set; }
        public short ImportStatus { get; set; }
        public short PaymentStatus { get; set; }
        public string? Note { get; set; }
        public string? NameBaseWarehouse { get; set; }
        public Guid? AssignBy { get; set; }
        public Guid? ImportBy { get; set; }
        public DebtBranchResponseDto DebtBranchResponseDto { get; set; }
        public IEnumerable<BranchImportProductResponseDto>? WmsBranchImportProducts { get; set; }
        public List<GetProductsReturnFromBranchResponseDto>? ProductReturns { get; set; }
        public IEnumerable<InvoiceResponseDto> Invoices { get; set; }

    }
}
