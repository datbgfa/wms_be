﻿namespace WMS.Business.Dtos.BranchImportDto
{
    public class GetBranchImportResponseDto
    {
        public Guid ImportId { get; set; }
        public string ImportCode { get; set; } = null!;
        public string? NameAssignBy { get; set; }
        public string? NameImportBy { get; set; }
        public DateTime ImportedDate { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal TotalCost { get; set; }
        public decimal PaidAmount { get; set; }
        public short ImportStatus { get; set; }
        public short PaymentStatus { get; set; }
        public int NumberOfProductReturnRequested { get; set; }
        public string ExportCode { get; set; }
        public IEnumerable<BranchImportProductResponseDto>? WmsBranchImportProducts { get; set; }
    }
    public class BranchImportProductResponseDto
    {
        public Guid ProductId { get; set; }
        public string? ProductName { get; set; }
        public string? ProductImage { get; set; }
        public decimal UnitPrice { get; set; }
        public int ImportedQuantity { get; set; }
        public int StockQuantity { get; set; }
        public IEnumerable<BranchImportProductResponseList> BranchImportProductList { get; set; }

        public BranchImportProductResponseDto()
        {
        }

        public BranchImportProductResponseDto(Guid productId, string? productName, string? productImage, decimal unitPrice, int importedQuantity, int stockQuantity)
        {
            ProductId = productId;
            ProductName = productName;
            ProductImage = productImage;
            UnitPrice = unitPrice;
            ImportedQuantity = importedQuantity;
            StockQuantity = stockQuantity;
        }
    }
    public class BranchImportProductResponseList
    {
        public int ImportedQuantity { get; set; }
        public int StockQuantity { get; set; }
        public string ExportCodeItem { get; set; }
        public decimal UnitPrice { get; set; }

    }
}
