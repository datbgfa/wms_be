﻿using System.ComponentModel.DataAnnotations;

namespace WMS.Business.Dtos.BranchImportDto
{
    public class PayForBranchProductReturnRequestDto
    {
        [Required]
        public Guid BranchProductReturnId { get; set; }
        public decimal PayAmount { get; set; }
    }
}
