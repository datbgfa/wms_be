﻿using System.ComponentModel.DataAnnotations;
using WMS.Business.Dtos.CommonDto;

namespace WMS.Business.Dtos.BranchImportDto
{
    public class GetBranchImportRequestDto
    {
        public string? SearchWord { get; set; }
        [Required]
        public PagingRequest PagingRequest { get; set; } = null!;
        public short? ImportStatus { get; set; }
        public short? PaymentStatus { get; set; }
        /// <summary>
        /// None = 0
        /// ThisWeek = 1
        /// ThisMonth = 2
        /// ThisYear = 3
        /// </summary>
        public short DateFilter { get; set; } = 0;

    }
}
