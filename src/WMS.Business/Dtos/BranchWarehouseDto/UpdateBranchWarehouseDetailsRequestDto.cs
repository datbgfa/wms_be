﻿using System.ComponentModel.DataAnnotations;

namespace WMS.Business.Dtos.BranchWarehouseDto
{
    public class UpdateBranchWarehouseDetailsRequestDto
    {
        [Required]
        public Guid WarehouseId { get; set; }
        public string? Note { get; set; }
        public short Status { get; set; }
    }
}
