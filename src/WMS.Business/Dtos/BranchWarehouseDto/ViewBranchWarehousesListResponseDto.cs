﻿namespace WMS.Business.Dtos.BranchWarehouseDto
{
    public class ViewBranchWarehousesListResponseDto
    {
        public Guid WarehouseId { get; set; }
        public string WarehouseOwnerName { get; set; }
        public string WarehouseName { get; set; } = null!;
        public string PhoneNumber { get; set; } = null!;
        public string Email { get; set; } = null!;
        public string Address { get; set; } = null!;
        public short Status { get; set; }
        public string? Note { get; set; }
    }
}
