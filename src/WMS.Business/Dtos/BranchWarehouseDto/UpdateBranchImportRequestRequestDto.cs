﻿using System.ComponentModel.DataAnnotations;

namespace WMS.Business.Dtos.BranchWarehouseDto
{
    public class UpdateBranchImportRequestRequestDto
    {
        [Required]
        public Guid RequestId { get; set; }
        [Required]
        public DateTime RequestDate { get; set; }
        [Required]
        public Guid ImportBy { get; set; }
        [Required]
        public IEnumerable<CreateBranchImportRequestProductRequestDto> ProductRequests { get; set; }
    }
}
