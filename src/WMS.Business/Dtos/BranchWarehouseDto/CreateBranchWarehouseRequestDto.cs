﻿using System.ComponentModel.DataAnnotations;

namespace WMS.Business.Dtos.BranchWarehouseDto
{
    public class CreateBranchWarehouseRequestDto
    {
        //[Required]
        //public Guid WarehouseOwner { get; set; }
        [Required]
        public string WarehouseName { get; set; } = null!;
        [Required]
        [Phone]
        public string PhoneNumber { get; set; } = null!;
        [Required]
        [EmailAddress]
        public string Email { get; set; } = null!;
        [Required]
        public string Address { get; set; } = null!;
        public short Status { get; set; }
        public string? Note { get; set; }
    }
}
