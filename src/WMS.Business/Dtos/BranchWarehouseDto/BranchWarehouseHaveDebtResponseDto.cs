﻿namespace WMS.Business.Dtos.BranchWarehouseDto
{
    public class BranchWarehouseHaveDebtResponseDto
    {
        public Guid WarehouseId { get; set; }
        public string WarehouseName { get; set; } = null!;
        public decimal DebtAmount { get; set; }
    }
}
