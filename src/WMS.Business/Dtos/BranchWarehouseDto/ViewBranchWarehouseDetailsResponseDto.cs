﻿namespace WMS.Business.Dtos.BranchWarehouseDto
{
    public class ViewBranchWarehouseDetailsResponseDto
    {
        public Guid WarehouseId { get; set; }
        public string WarehouseOwnerName { get; set; }
        public string WarehouseName { get; set; } = null!;
        public string PhoneNumber { get; set; } = null!;
        public string Email { get; set; } = null!;
        public string Address { get; set; } = null!;
        public short Status { get; set; }
        public string? Note { get; set; }
        public BranchDebtDto? Debts { get; set; }
        public IEnumerable<BaseInvoiceDto>? Invoices { get; set; }
        public IEnumerable<BaseHistoryExportResponseDto>? Exports { get; set; }
    }

    public class BranchDebtDto
    {
        public Guid DebtId { get; set; }
        public decimal DebtAmount { get; set; }
    }

    public class BranchInvoiceDto
    {
        public Guid InvoiceId { get; set; }
        public string InvoiceCode { get; set; }
        public short Type { get; set; }
        public decimal Amount { get; set; }
        public decimal BalanceChange { get; set; }
        public string CreateUser { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public string Description { get; set; }
    }
    public class BaseInvoiceDto
    {
        public Guid InvoiceId { get; set; }
        public string InvoiceCode { get; set; }
        public short Type { get; set; }
        public decimal Amount { get; set; }
        public decimal BalanceChange { get; set; }
        public string CreateUser { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public string Description { get; set; }
    }

    public class BaseHistoryExportResponseDto
    {
        public Guid ExportId { get; set; }
        public string ExportCode { get; set; }
        public DateTime ExportDate { get; set; }
        public decimal PaidAmount { get; set; }
        public decimal TotalCost { get; set; }
        public short ExportStatus { get; set; }
        public short PaymentStatus { get; set; }
    }
}
