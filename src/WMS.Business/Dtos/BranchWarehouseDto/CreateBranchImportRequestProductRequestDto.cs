﻿namespace WMS.Business.Dtos.BranchWarehouseDto
{
    public class CreateBranchImportRequestProductRequestDto
    {
        public Guid ProductId { get; set; }
        public int RequestQuantity { get; set; }
    }
}
