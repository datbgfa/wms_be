﻿using System.ComponentModel.DataAnnotations;

namespace WMS.Business.Dtos.BranchWarehouseDto
{
    public class ChangeBranchImportStatusRequestDto
    {
        [Required]
        public Guid ImportId { get; set; }
        public Guid? ImportBy { get; set; }
        public short Status { get; set; }
    }
}
