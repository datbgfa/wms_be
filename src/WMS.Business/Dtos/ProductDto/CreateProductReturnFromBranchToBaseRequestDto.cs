﻿using System.ComponentModel.DataAnnotations;

namespace WMS.Business.Dtos.ProductDto
{
    public class CreateProductReturnFromBranchToBaseRequestDto
    {
        public string? Description { get; set; }
        [Required]
        public Guid ImportId { get; set; }

        public List<CreateProductReturnDetailsFromBranchToBaseRequestDto> ProductRequest { get; set; } = null!;
    }


    public class CreateProductReturnDetailsFromBranchToBaseRequestDto
    {
        [Required]
        public Guid ProductId { get; set; }
        public int RequestQuantity { get; set; }
        [Required]
        public string ExportCodeItem { get; set; }
    }
}
