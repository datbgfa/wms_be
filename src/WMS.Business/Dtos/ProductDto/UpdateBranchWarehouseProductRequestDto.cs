﻿using System.ComponentModel.DataAnnotations;

namespace WMS.Business.Dtos.ProductDto
{
    public class UpdateBranchWarehouseProductRequestDto
    {
        [Required]
        public Guid ProductId { get; set; }
        [Range(0, double.MaxValue)]
        public decimal ExportPrice { get; set; }
    }
}
