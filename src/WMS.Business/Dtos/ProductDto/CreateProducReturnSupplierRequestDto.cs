﻿using System.ComponentModel.DataAnnotations;

namespace WMS.Business.Dtos.ProductDto
{
    public class CreateProducReturnSupplierRequestDto
    {
        public string? Description { get; set; }
        [Required]
        public Guid ImportId { get; set; }
        [Required]

        public List<CreateProductReturnSupplierRequestDetailDto> ProductRequest { get; set; } = null!;
    }

    public class CreateProductReturnSupplierRequestDetailDto
    {
        [Required]
        public Guid ProductId { get; set; }
        public int RequestQuantity { get; set; }
    }
}
