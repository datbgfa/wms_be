﻿namespace WMS.Business.Dtos.ProductDto
{
    public class SearchProductByNameResponseDto
    {
        public Guid ProductId { get; set; }
        public int CategoryId { get; set; }
        public string SKU { get; set; } = null!;
        public string ProductName { get; set; } = null!;
        public decimal UnitPrice { get; set; }
        public decimal CostPrice { get; set; }
        public decimal ExportPrice { get; set; }
        public string ProductImage { get; set; } = null!;
    }
}
