﻿namespace WMS.Business.Dtos.ProductDto
{
    public class GetProductsReturnFromBranchResponseDto
    {
        public Guid ProductReturnId { get; set; }
        public Guid ImportId { get; set; }
        public Guid? BaseExportId { get; set; }
        public string? Description { get; set; }
        public short Status { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal PaidAmount { get; set; }
        public short PaymentStatus { get; set; }
        public string ReturnCode { get; set; } = null!;
        public string? BranchWarehouseName { get; set; }

        public string CreateUser { get; set; } = null!;
        public DateTime CreateDate { get; set; }
        public string UpdateUser { get; set; } = null!;
        public DateTime UpdateDate { get; set; }

        public List<ProductReturnDetailFromBranchResponseDto>? ReturnDetails { get; set; }
    }

    public class ProductReturnDetailFromBranchResponseDto
    {
        public Guid BprdId { get; set; }
        public Guid ProductReturnId { get; set; }
        public Guid ImportId { get; set; }
        public Guid BaseImportId { get; set; }
        public string ProductName { get; set; } = null!;
        public decimal CostPrice { get; set; }
        public decimal UnitPrice { get; set; }
        public int RequestQuantity { get; set; }
    }
}
