﻿using Microsoft.AspNetCore.Http;

namespace WMS.Business.Dtos.ProductDto
{
    public class UpdateProductDetailRequestDto
    {
        public Guid ProductId { get; set; }
        public int CategoryId { get; set; }
        public string SKU { get; set; } = null!;
        public string ProductName { get; set; } = null!;
        public string? ProductDescription { get; set; }
        public IFormFile? ProductImage { get; set; }
        public bool? Visible { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal SalePrice { get; set; }
        public decimal ExportPrice { get; set; }
        public string UnitType { get; set; } = string.Empty!;
        public short Status { get; set; }

        public List<ProductAttributeResponseDto>? Attributes { get; set; }
    }
}
