﻿using System.ComponentModel.DataAnnotations;

namespace WMS.Business.Dtos.ProductDto
{
    public class CreateProductRequestDto
    {
        public int CategoryId { get; set; }
        public string? SKU { get; set; }
        [Required]
        public string ProductName { get; set; } = null!;
        public string? ProductDescription { get; set; }
        public bool? Visible { get; set; }
        [Range(0, double.MaxValue)]
        public decimal UnitPrice { get; set; }
        [Range(0, double.MaxValue)]
        public decimal SalePrice { get; set; }
        [Range(0, double.MaxValue)]
        public decimal ExportPrice { get; set; }
        public string? UnitType { get; set; }
        public IEnumerable<CreateProductAttributeValueDto>? ProductAttributeValueDtos { get; set; }

    }
    public class CreateProductAttributeValueDto
    {
        public int AttributeId { get; set; }
        public string? Value { get; set; }
    }
}
