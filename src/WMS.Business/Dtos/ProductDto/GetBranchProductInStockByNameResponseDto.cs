﻿namespace WMS.Business.Dtos.ProductDto
{
    public class GetBranchProductInStockByNameResponseDto
    {
        public Guid ProductId { get; set; }
        public string SKU { get; set; } = null!;
        public string ProductName { get; set; } = null!;
        public string? ProductDescription { get; set; }
        public string ProductImageLink { get; set; } = null!;
        public decimal CostPrice { get; set; }
        public decimal ExportPrice { get; set; }
        public string UnitType { get; set; }
        public int Quantity { get; set; }
    }
}
