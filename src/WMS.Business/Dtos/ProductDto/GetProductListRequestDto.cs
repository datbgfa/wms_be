﻿using System.ComponentModel.DataAnnotations;
using WMS.Business.Dtos.CommonDto;

namespace WMS.Business.Dtos.ProductDto
{
    public class GetProductListRequestDto
    {
        public string? SearchWord { get; set; }
        [Required]
        public PagingRequest PagingRequest { get; set; } = null!;
        public bool? Visible { get; set; }
        public int? CategoryId { get; set; }
    }
}
