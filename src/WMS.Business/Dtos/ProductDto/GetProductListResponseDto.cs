﻿namespace WMS.Business.Dtos.ProductDto
{
    public class GetProductListResponseDto
    {
        public Guid ProductId { get; set; }
        public int CategoryId { get; set; }
        public string SKU { get; set; } = null!;
        public string ProductName { get; set; } = null!;
        public string? ProductDescription { get; set; }
        public string ProductImage { get; set; } = null!;
        public bool? Visible { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal CostPrice { get; set; }
        public decimal ExportPrice { get; set; }
        public string UnitType { get; set; } = string.Empty!;
        public short Status { get; set; }
        public string CategoryName { get; set; }

    }
}
