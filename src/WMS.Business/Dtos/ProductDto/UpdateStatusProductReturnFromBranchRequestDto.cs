﻿namespace WMS.Business.Dtos.ProductDto
{
    public class UpdateStatusProductReturnFromBranchRequestDto
    {
        public Guid ReturnId { get; set; }
        public short Status { get; set; }
    }
}
