﻿using System.ComponentModel.DataAnnotations;
using WMS.Business.Dtos.CommonDto;

namespace WMS.Business.Dtos.ProductDto
{
    public class GetStockQuantityResponseDto
    {
        public Guid ProductId { get; set; }
        public Guid ImportId { get; set; }
        public string SKU { get; set; } = null!;
        public string CategoryName { get; set; }
        public string ProductName { get; set; } = null!;
        public string ProductImage { get; set; } = null!;
        public decimal UnitPrice { get; set; }
        public int InventoryNumber { get; set; }

    }

    public class GetAllImportsByIdResponseDto
    {
        public Guid ProductId { get; set; }
        public Guid ImportId { get; set; }
        public string ImportCode { get; set; } = null!;
        public string BaseImportCode { get; set; } = null!;
        public int ImportedQuantity { get; set; }
        public int StockQuantity { get; set; }
    }

    public class GetStockQuantityRequestDto
    {
        public int RoleId { get; set; }
        public string? SearchWord { get; set; }
        [Required]
        public PagingRequest PagingRequest { get; set; } = null!;
        public int CategoryId { get; set; }
    }

    public class GetStockQuantityByIdRequestDto
    {
        public int RoleId { get; set; }
        public Guid ProductId { get; set; }
    }


}
