﻿namespace WMS.Business.Dtos.ProductDto
{
    public class GetProductDetailResponseDto
    {
        public Guid ProductId { get; set; }
        public int CategoryId { get; set; }
        public string SKU { get; set; } = null!;
        public string ProductName { get; set; } = null!;
        public string? ProductDescription { get; set; }
        public string ProductImage { get; set; } = null!;
        public bool? Visible { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal CostPrice { get; set; }
        public decimal ExportPrice { get; set; }
        public string? UnitType { get; set; }
        public short Status { get; set; }

    }

    public class ProductAttributeResponseDto
    {
        public int AttributeId { get; set; }
        public string AttributeName { get; set; }
        public string Value { get; set; }
        public bool? IsDelete { get; set; }

        public ProductAttributeResponseDto(int attributeId, string attributeName, string value, bool? isDelete)
        {
            AttributeId = attributeId;
            AttributeName = attributeName;
            Value = value;
            IsDelete = isDelete;
        }
    }
}
