﻿namespace WMS.Business.Dtos.ProductDto
{
    public class ProductReturnSupplierResponseDto
    {
        public Guid ImportId { get; set; }
        public Guid ProductReturnId { get; set; }
        public string? Description { get; set; }
        public string CreateUser { get; set; } = null!;
        public DateTime CreateDate { get; set; }
        public string UpdateUser { get; set; } = null!;
        public decimal TotalPrice { get; set; }
        public DateTime UpdateDate { get; set; }
        public short Status { get; set; }
        public short PaymentStatus { get; set; }
        public string ReturnCode { get; set; } = null!;
        public decimal PaidAmount { get; set; }
        public IList<ProductReturnSupplierDetailResponseDto>? ReturnDetails { get; set; }
    }

    public class ProductReturnSupplierDetailResponseDto
    {
        public Guid BprdId { get; set; }
        public Guid ImportId { get; set; }
        public Guid ProductId { get; set; }
        public string ProductName { get; set; } = null!;
        public decimal CostPrice { get; set; }
        public decimal UnitPrice { get; set; }
        public int RequestQuantity { get; set; }
    }
}
