﻿using System.ComponentModel.DataAnnotations;
using WMS.Business.Dtos.CommonDto;

namespace WMS.Business.Dtos.ProductReturnFromBranchDto
{
    public class GetProductReturnFromBranchRequestDto
    {
        public string? SearchWord { get; set; }
        [Required]
        public PagingRequest PagingRequest { get; set; } = null!;
        public Guid? BranchWarehouseId { get; set; }
        public short DateFilter { get; set; } = 0;
        public short? PaymentStatus { get; set; }
        public short? Status { get; set; }
    }
}
