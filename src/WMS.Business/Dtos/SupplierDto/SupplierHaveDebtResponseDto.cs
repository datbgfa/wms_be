﻿namespace WMS.Business.Dtos.SupplierDto
{
    public class SupplierHaveDebtResponseDto
    {
        public int SupplierId { get; set; }
        public string SupplierName { get; set; } = null!;
        public decimal DebtAmount { get; set; }
    }
}
