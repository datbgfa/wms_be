﻿using WMS.Business.Dtos.DebtDto;

namespace WMS.Business.Dtos.SupplierDto
{
    public class SupplierResponseDto
    {
        public int SupplierId { get; set; }
        public string SupplierName { get; set; } = null!;
        public string Address { get; set; } = null!;
        public string PhoneNumber { get; set; } = null!;
        public string Email { get; set; } = null!;
        public string? OtherInformation { get; set; }
        public string Logo { get; set; } = null!;
        public bool Status { get; set; }
        public DateTime CooperatedTime { get; set; }
        public decimal DebtAmount { get; set; }
        public int TotalImport { get; set; }
        public int TotalProductReturn { get; set; }
        public virtual ICollection<DebtRequestDto>? BaseDebts { get; set; }

    }
    public class SupplierSimpleResponseDto
    {
        public int SupplierId { get; set; }
        public string SupplierName { get; set; } = null!;
    }

    public class SupplierHistoryImportResponseDto
    {
        public Guid ImportId { get; set; }
        public string? ImportCode { get; set; }
        public string? NameBase { get; set; }
        public DateTime ImportedDate { get; set; }
        public decimal TotalPrice { get; set; }
        public short ImportStatus { get; set; }
        public short PaymentStatus { get; set; }
        public int SupplierId { get; set; }
        public string? SupplierName { get; set; }
    }

    public class SupplierDebtResponseDto
    {
        public string? InvoiceCode { get; set; }
        public string CreateUser { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public string? Description { get; set; }
        public int SupplierId { get; set; }
        public decimal Amount { get; set; }
        public decimal BalanceChange { get; set; }
    }
}
