﻿namespace WMS.Business.Dtos.SupplierDto
{
    public class UpdateSupplierStatusRequestDto
    {
        public int SupplierId { get; set; }

        public bool Status { get; set; }
    }
}
