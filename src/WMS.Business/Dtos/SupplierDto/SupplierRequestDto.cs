﻿using System.ComponentModel.DataAnnotations;
using WMS.Business.Dtos.CommonDto;

namespace WMS.Business.Dtos.SupplierDto
{
    public class SupplierRequestDto
    {
        public string? SearchWord { get; set; }
        [Required]
        public PagingRequest PagingRequest { get; set; } = null!;
    }
}
