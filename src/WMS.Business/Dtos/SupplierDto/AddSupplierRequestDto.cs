﻿using System.ComponentModel.DataAnnotations;

namespace WMS.Business.Dtos.SupplierDto
{
    public class AddSupplierRequestDto
    {
        public int SupplierId { get; set; }
        [Required(AllowEmptyStrings = false)]
        [MaxLength(100)]
        public string SupplierName { get; set; } = null!;

        [MaxLength(1000)]
        public string Address { get; set; } = null!;

        [Required(AllowEmptyStrings = false)]
        [MaxLength(15)]
        public string PhoneNumber { get; set; } = null!;

        [Required(AllowEmptyStrings = false)]
        [MaxLength(320)]
        public string Email { get; set; } = null!;

        [MaxLength(255)]
        public string? OtherInformation { get; set; }

        [MaxLength(1000)]
        public string? Logo { get; set; }

        public DateTime CooperatedTime { get; set; }

        public decimal DebtAmount { get; set; }
    }
}
