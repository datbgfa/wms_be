﻿namespace WMS.Business.Dtos.InvoiceDto
{
    public class InvoiceResponseDto
    {
        public Guid InvoiceId { get; set; }
        public short Type { get; set; }
        public decimal Amount { get; set; }
        public string? Description { get; set; }
        public decimal BalanceChange { get; set; }
        public DateTime UpdateDate { get; set; }
        public string InvoiceCode { get; set; }
    }
}
