﻿namespace WMS.Business.Dtos.InvoiceDto
{
    public class DashboardResponseDto
    {
        public int TotalImports { get; set; }
        public int TotalExports { get; set; }
        //only used for base dashboard
        public decimal TotalBaseDebt { get; set; }
        //branch dashboard only display total branch debt
        public decimal TotalBranchDebt { get; set; }
        public int TotalProductsInStock { get; set; }
        public int TotalProductsExport { get; set; }
        public IEnumerable<TrendingProductsResponseDto> TrendingProducts { get; set; }
        public IEnumerable<StatisticsResponseDto> Statistics { get; set; }
    }

    public class TrendingProductsResponseDto
    {
        public Guid ProductId { get; set; }
        public string ProductName { get; set; }
        public string ImageLink { get; set; }
        public int TotalProducts { get; set; }
    }

    public class StatisticsResponseDto
    {
        public string Month { get; set; }
        public int Year { get; set; }
        public decimal MoneyReceived { get; set; }
        public decimal MoneyPaid { get; set; }
    }
}
