﻿using System.ComponentModel.DataAnnotations;
using WMS.Business.Dtos.CommonDto;

namespace WMS.Business.Dtos.InvoiceDto
{
    public class InvoiceDataRequestDto : PagingRequest
    {
        public DateTime? From { get; set; }
        public DateTime? To { get; set; }
        [Required]
        public short Type { get; set; }
        [Required]
        public string InvoiceFor { get; set; } = null!;
    }
}
