﻿using System.ComponentModel.DataAnnotations;
using WMS.Business.Utils.CustomValidation;

namespace WMS.Business.Dtos.InvoiceDto
{
    public class CreateReceiptInvoiceRequestDto
    {
        //[Required]
        //public Guid DebtId { get; set; }
        public int? SupplierId { get; set; }
        public Guid? BranchId { get; set; }
        /// <summary>
        /// 1 Create Invoice For Base Return
        /// 2 Create Invoice For Base Export
        /// </summary>
        [Required]
        public short Type { get; set; }
        [Required]
        [GreaterThanDecimal(0)]
        public decimal Amount { get; set; }
    }
}
