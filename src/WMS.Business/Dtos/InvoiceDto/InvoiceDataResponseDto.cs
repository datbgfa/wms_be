﻿namespace WMS.Business.Dtos.InvoiceDto
{
    public class InvoiceDataResponseDto
    {
        public Guid InvoiceId { get; set; }
        public string InvoiceCode { get; set; } = null!;
        public short Type { get; set; }
        public decimal Amount { get; set; }
        public string? Description { get; set; }
        public decimal BalanceChange { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
