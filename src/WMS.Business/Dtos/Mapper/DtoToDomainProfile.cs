﻿using AutoMapper;
using WMS.Business.Dtos.BranchExportDto;
using WMS.Business.Dtos.BranchWarehouseDto;
using WMS.Business.Dtos.CategoryDto;
using WMS.Business.Dtos.DebtDto;
using WMS.Business.Dtos.ProductDto;
using WMS.Business.Dtos.StockCheckSheetDto;
using WMS.Business.Dtos.SupplierDto;
using WMS.Business.Dtos.UserDto;
using WMS.Business.Dtos.WarehouseDto;
using WMS.Domain.Entities;

namespace WMS.Business.Dtos.Mapper
{
    public class DtoToDomainProfile : Profile
    {
        public DtoToDomainProfile()
        {
            CreateMap<EditProfileInformationRequestDto, WmsUser>();

            CreateMap<CreateUserAccountRequestDto, WmsUser>();

            CreateMap<CreateBranchWarehouseRequestDto, WmsBranchWarehouse>();

            CreateMap<CreateProductRequestDto, WmsProduct>();

            CreateMap<CreateProductAttributeValueDto, WmsProductAttribute>();

            CreateMap<UpdateCategoryDto, WmsCategory>();

            CreateMap<CategoryResponseDto, WmsCategory>();

            CreateMap<CreateBaseImportRequestDto, WmsBaseImport>();

            CreateMap<CreateBaseImportProductRequestDto, WmsBaseImportProduct>();

            CreateMap<DebtRequestDto, WmsBaseDebt>();

            CreateMap<AddSupplierRequestDto, WmsSupplier>();

            CreateMap<SupplierResponseDto, WmsSupplier>();

            CreateMap<UpdateProductDetailRequestDto, WmsProduct>();

            CreateMap<UpdateBaseImportRequestDto, WmsBaseImport>();

            CreateMap<CreateProducReturnSupplierRequestDto, WmsBaseProductReturn>();

            CreateMap<CreateProductReturnSupplierRequestDetailDto, WmsBaseProductReturnDetail>();

            CreateMap<CreateBranchImportRequestProductRequestDto, WmsBranchProductRequest>();

            CreateMap<UpdateBaseExportProductRequestDto, WmsBaseExportProduct>();

            CreateMap<CreateUserAccountRequestDto, WmsUserRole>();
            CreateMap<SupplierHistoryImportResponseDto, WmsBaseImport>();
            CreateMap<SupplierDebtResponseDto, WmsBaseInvoice>();
            CreateMap<CreateBranchExportRequestDto, WmsBranchExport>();
            CreateMap<CreateBranchExportProductDto, WmsBranchExportProduct>();

            CreateMap<GetStockQuantityResponseDto, WmsProduct>();
            CreateMap<GetAllImportsByIdResponseDto, WmsBaseImportProduct>();
            CreateMap<GetAllImportsByIdResponseDto, WmsBranchImportProduct>();

            CreateMap<StockCheckSheetResponseDto, WmsBaseStockCheckSheet>();
            CreateMap<StockCheckSheetDetailsResponseDto, WmsBaseStockCheckSheetDetail>();

            CreateMap<StockCheckSheetResponseDto, WmsBranchStockCheckSheet>();
            CreateMap<StockCheckSheetDetailsResponseDto, WmsBranchStockCheckSheetDetail>();

            CreateMap<CreateProductReturnFromBranchToBaseRequestDto, WmsBranchProductReturn>();
            CreateMap<CreateProductReturnDetailsFromBranchToBaseRequestDto, WmsBranchProductReturnDetail>();

            CreateMap<AddStockCheckSheetRequestDto, WmsBaseStockCheckSheet>();
            CreateMap<AddStockCheckSheetDetailRequestDto, WmsBaseStockCheckSheetDetail>()
                .ForMember(dest => dest.BaseQuantity, opt => opt.MapFrom(src => src.StockQuantity));

            CreateMap<AddStockCheckSheetRequestDto, WmsBranchStockCheckSheet>();
            CreateMap<AddStockCheckSheetDetailRequestDto, WmsBranchStockCheckSheetDetail>()
                 .ForMember(dest => dest.BranchQuantity, opt => opt.MapFrom(src => src.StockQuantity));

        }
    }
}
