﻿using AutoMapper;
using WMS.Business.Dtos.BaseExportDto;
using WMS.Business.Dtos.BaseImportDto;
using WMS.Business.Dtos.BranchImportDto;
using WMS.Business.Dtos.BranchRequestDto;
using WMS.Business.Dtos.BranchWarehouseDto;
using WMS.Business.Dtos.CategoryDto;
using WMS.Business.Dtos.DebtDto;
using WMS.Business.Dtos.EmployeeDto;
using WMS.Business.Dtos.InvoiceDto;
using WMS.Business.Dtos.ProductDto;
using WMS.Business.Dtos.StockCheckSheetDto;
using WMS.Business.Dtos.SupplierDto;
using WMS.Business.Dtos.UserDto;
using WMS.Business.Dtos.WarehouseDto;
using WMS.Domain.Constant;
using WMS.Domain.Entities;

namespace WMS.Business.Dtos.Mapper
{
    public class DomainToDtoProfile : Profile
    {
        public DomainToDtoProfile()
        {
            CreateMap<WmsUser, ViewAccountsListResponseDto>();

            CreateMap<WmsUser, ProfileInformationResponseDto>()
                .ForMember(dest => dest.Birthdate, opt => opt.MapFrom(src => DateOnly.FromDateTime(src.Birthdate)));

            CreateMap<WmsBranchWarehouse, ViewBranchWarehousesListResponseDto>()
                .ForMember(dest => dest.WarehouseOwnerName, opt => opt.MapFrom(src => src.WmsWarehouseOwner == null ? "" : src.WmsWarehouseOwner.Fullname));

            CreateMap<WmsBranchWarehouse, ViewBranchWarehouseDetailsResponseDto>()
                .ForMember(dest => dest.WarehouseOwnerName, opt => opt.MapFrom(src => src.WmsWarehouseOwner == null ? "" : src.WmsWarehouseOwner.Fullname));

            CreateMap<WmsBaseWarehouse, ViewBranchWarehousesListResponseDto>()
               .ForMember(dest => dest.WarehouseOwnerName, opt => opt.MapFrom(src => src.WmsWarehouseOwner == null ? "" : src.WmsWarehouseOwner.Fullname));

            CreateMap<WmsBranchWarehouse, ViewWarehouseProfileResponseDto>();

            CreateMap<WmsBaseWarehouse, ViewWarehouseProfileResponseDto>();

            CreateMap<WmsUser, ViewEmployeesListResponseDto>()
                .ForMember(dest => dest.Role, opt => opt.MapFrom(src => src.UserRole == null ? "" : src.UserRole.First().Role.RoleDescription))
                .ForMember(dest => dest.WarehouseName, opt => opt.MapFrom(src => src.WmsBaseWarehouseUsers == null && src.WmsBranchWarehouseUsers == null ? "" : 
                (src.WmsBranchWarehouseUsers != null && src.WmsBranchWarehouseUsers.Count > 0 ? src.WmsBranchWarehouseUsers.First().WmsBranchWarehouse.WarehouseName : src.WmsBaseWarehouseUsers.First().WmsBaseWarehouse.WarehouseName)  ));

            CreateMap<WmsUser, ViewEmployeeDetailsResponseDto>();
                

            CreateMap<WmsProduct, GetProductDetailResponseDto>();

            CreateMap<WmsProduct, GetProductListResponseDto>()
                .ForMember(dest => dest.CategoryName, opt => opt.MapFrom(src => src.Category == null ? "-------" : src.Category.CategoryName));


            CreateMap<WmsCategory, UpdateCategoryDto>();

            CreateMap<WmsCategory, CategoryResponseDto>();

            CreateMap<WmsRole, UserRoleResponseDto>();

            CreateMap<WmsBaseDebt, DebtRequestDto>();

            CreateMap<WmsSupplier, AddSupplierRequestDto>();

            CreateMap<WmsSupplier, SupplierResponseDto>()
                .ForMember(dest => dest.BaseDebts, opt => opt.MapFrom(src => src.WmsBaseDebts))
                .ForMember(dest => dest.DebtAmount, opt => opt.MapFrom(src => src.WmsBaseDebts == null ? 0 : src.WmsBaseDebts.Sum(x => x.DebtAmount)))
                .ForMember(dest => dest.TotalImport, opt => opt.MapFrom(src => src.WmsBaseDebts == null ? 0 : src.WmsBaseImports!.Count()))
                .ForMember(dest => dest.TotalProductReturn, opt => opt.MapFrom(src => GetTotalProductReturn(src.WmsBaseImports!)));

            CreateMap<WmsBaseImport, GetBaseImportResponseDto>()
                .ForMember(dest => dest.NameAssignBy, opt => opt.MapFrom(src => src.UserAssignBy == null ? "--------" : src.UserAssignBy!.Fullname))
                .ForMember(dest => dest.NameImportBy, opt => opt.MapFrom(src => src.UserImportBy == null ? "--------" : src.UserImportBy!.Fullname))
                .ForMember(dest => dest.SupplierName, opt => opt.MapFrom(src => src.WmsSupplier == null ? "--------" : src.WmsSupplier!.SupplierName));

            CreateMap<WmsBaseImport, SupplierHistoryImportResponseDto>()
                .ForMember(dest => dest.ImportCode, opt => opt.MapFrom(src => src.ImportCode))
               .ForMember(dest => dest.NameBase, opt => opt.MapFrom(src => src.WmsBaseWarehouse!.WarehouseName))
               .ForMember(dest => dest.SupplierName, opt => opt.MapFrom(src => src.WmsSupplier!.SupplierName));

            CreateMap<WmsBaseInvoice, SupplierDebtResponseDto>()
                .ForMember(dest => dest.InvoiceCode, opt => opt.MapFrom(src => src.InvoiceCode))
               .ForMember(dest => dest.Amount, opt => opt.MapFrom(src => src.Amount))
                .ForMember(dest => dest.BalanceChange, opt => opt.MapFrom(src => src.BalanceChange));

            CreateMap<WmsBaseImportProduct, BaseImportProductResponseDto>();

            CreateMap<WmsBranchProductReturnDetail, ProductReturnDetailFromBranchResponseDto>()
                .ForMember(dest => dest.ProductName, opt => opt.MapFrom(src => src.WmsProduct!.ProductName == null ? "--------" : src.WmsProduct.ProductName));

            CreateMap<WmsBranchProductReturn, GetProductsReturnFromBranchResponseDto>()
                .ForMember(dest => dest.ReturnDetails, opt => opt.MapFrom(src => src.WmsBranchProductReturnDetails))
                .ForMember(dest => dest.BranchWarehouseName, opt => opt.MapFrom(src => src.WmsBranchImport == null ? "--------" :
                                                                                src.WmsBranchImport.WmsBranchWarehouse == null ? "--------" :
                                                                                src.WmsBranchImport.WmsBranchWarehouse.WarehouseName));

            CreateMap<WmsBaseProductReturnDetail, ProductReturnSupplierDetailResponseDto>()
                .ForMember(dest => dest.ProductName, opt => opt.MapFrom(src => src.WmsProduct!.ProductName == null ? "--------" : src.WmsProduct.ProductName));

            CreateMap<WmsBaseProductReturn, ProductReturnSupplierResponseDto>()
                .ForMember(dest => dest.ReturnDetails, opt => opt.MapFrom(src => src.WmsBaseProductReturnDetails));

            CreateMap<WmsBaseImportProduct, BaseImportProductResponseDto>()
                .ForMember(dest => dest.ProductName, opt => opt.MapFrom(src => src.WmsProduct == null ? "--------" : src.WmsProduct.ProductName))
                .ForMember(dest => dest.ProductImage, opt => opt.MapFrom(src => src.WmsProduct == null ? "--------" : src.WmsProduct.ProductImage));

            CreateMap<WmsProductAttribute, ProductAttributeResponseDto>()
                .ForMember(dest => dest.AttributeName, opt => opt.MapFrom(src => src.Attribute == null ? "--------" : src.Attribute.AttributeName))
                .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Value))
                .ForMember(dest => dest.AttributeId, opt => opt.MapFrom(src => src.AttributeId));

            CreateMap<WmsBaseImport, GetBaseImportDetailsResponseDto>()
                .ForMember(dest => dest.NameAssignBy, opt => opt.MapFrom(src => src.UserAssignBy == null ? "--------" : src.UserAssignBy.Fullname))
                .ForMember(dest => dest.NameImportBy, opt => opt.MapFrom(src => src.UserImportBy == null ? "--------" : src.UserImportBy.Fullname));

            CreateMap<WmsBaseImportProduct, BaseImportProductDetailsResponseDto>()
                .ForMember(dest => dest.ProductName, opt => opt.MapFrom(src => src.WmsProduct == null ? "--------" : src.WmsProduct.ProductName))
                .ForMember(dest => dest.ProductImage, opt => opt.MapFrom(src => src.WmsProduct == null ? "--------" : src.WmsProduct.ProductImage));

            CreateMap<WmsProduct, SearchProductByNameResponseDto>();

            CreateMap<WmsBranchRequest, GetBranchRequestReponseDto>()
                .ForMember(dest => dest.NameApprovedBy, opt => opt.MapFrom(src => src.UserApprovedBy == null ? "--------" : src.UserApprovedBy.Fullname))
                .ForMember(dest => dest.NameRequestBy, opt => opt.MapFrom(src => src.UserRequestBy == null ? "---------" : src.UserRequestBy.Fullname))
                ;
            CreateMap<WmsBranchProductRequest, BranchProductRequestReponseDto>()
                .ForMember(dest => dest.ProductName, opt => opt.MapFrom(src => src.WmsProduct == null ? "--------" : src.WmsProduct.ProductName))
                .ForMember(dest => dest.ProductImage, opt => opt.MapFrom(src => src.WmsProduct == null ? "--------" : src.WmsProduct.ProductImage))
                .ForMember(dest => dest.TotalPrice, opt => opt.MapFrom(src => src.UnitPrice * src.RequestQuantity))
                ;

            CreateMap<WmsBaseExport, GetBaseExportResponseDto>()
                .ForMember(dest => dest.NameBranchWarehouse, opt => opt.MapFrom(src => src.WmsBranchWarehouse == null ? "--------" : src.WmsBranchWarehouse.WarehouseName))
                .ForMember(dest => dest.NameAssignBy, opt => opt.MapFrom(src => src.UserAssign == null ? "--------" : src.UserAssign.Fullname))
                .ForMember(dest => dest.NameExportBy, opt => opt.MapFrom(src => src.UserExport == null ? "--------" : src.UserExport.Fullname))
                .ForMember(dest => dest.NumberOfProductReturnRequested, opt => opt.MapFrom(src => src.WmsBranchProductReturns == null ? 0 : src.WmsBranchProductReturns
                                                                                                                                            .Where(bpr => bpr.Status
                                                                                                                                            .Equals(ProductReturnStatusConstant.Requested)).Count()));
            CreateMap<WmsBaseExport, GetBaseExportResponseDetailsDto>()
                .ForMember(dest => dest.NameBranchWarehouse, opt => opt.MapFrom(src => src.WmsBranchWarehouse == null ? "--------" : src.WmsBranchWarehouse.WarehouseName))
                .ForMember(dest => dest.WarehouseStatus, opt => opt.MapFrom(src => src.WmsBranchWarehouse == null ? 0 : src.WmsBranchWarehouse.Status))
                .ForMember(dest => dest.NameAssignBy, opt => opt.MapFrom(src => src.UserAssign == null ? "--------" : src.UserAssign.Fullname))
                .ForMember(dest => dest.NameExportBy, opt => opt.MapFrom(src => src.UserExport == null ? "--------" : src.UserExport.Fullname))
                ;

            CreateMap<WmsBaseExportProduct, BaseExportProductResponseDto>()
                .ForMember(dest => dest.ProductName, opt => opt.MapFrom(src => src.WmsProduct == null ? "--------" : src.WmsProduct.ProductName))
                .ForMember(dest => dest.ProductImage, opt => opt.MapFrom(src => src.WmsProduct == null ? "--------" : src.WmsProduct.ProductImage))
                .ForMember(dest => dest.TotalPrice, opt => opt.MapFrom(src => src.UnitPrice * src.Quantity))
                ;

            CreateMap<WmsBranchImport, GetBranchImportResponseDto>()
                .ForMember(dest => dest.NameAssignBy, opt => opt.MapFrom(src => src.UserAssignBy == null ? "--------" : src.UserAssignBy.Fullname))
                .ForMember(dest => dest.NameImportBy, opt => opt.MapFrom(src => src.UserImportBy == null ? "--------" : src.UserImportBy.Fullname))
                .ForMember(dest => dest.NumberOfProductReturnRequested, opt => opt.MapFrom(src => src.WmsBranchProductReturns == null ? 0 : src.WmsBranchProductReturns
                                                                                                                                            .Where(bpr => bpr.Status
                                                                                                                                            .Equals(ProductReturnStatusConstant.Requested)).Count()));

            CreateMap<WmsBranchImport, GetBranchImportDetailsResponseDto>()
                .ForMember(dest => dest.NameAssignBy, opt => opt.MapFrom(src => src.UserAssignBy == null ? "--------" : src.UserAssignBy.Fullname))
                .ForMember(dest => dest.NameBaseWarehouse, opt => opt.MapFrom(src => src.WmsBaseWarehouse == null ? "--------" : src.WmsBaseWarehouse.WarehouseName))
                .ForMember(dest => dest.NameImportBy, opt => opt.MapFrom(src => src.UserImportBy == null ? "--------" : src.UserImportBy.Fullname));

            CreateMap<WmsBranchImportProduct, BranchImportProductResponseDto>()
                .ForMember(dest => dest.ProductName, opt => opt.MapFrom(src => src.WmsProduct == null ? "--------" : src.WmsProduct.ProductName))
                .ForMember(dest => dest.ProductImage, opt => opt.MapFrom(src => src.WmsProduct == null ? "--------" : src.WmsProduct.ProductImage));

            CreateMap<WmsBranchExport, GetBranchExportResponseDto>()
                .ForMember(dest => dest.NameAssignBy, opt => opt.MapFrom(src => src.UserAssign == null ? "--------" : src.UserAssign.Fullname))
                .ForMember(dest => dest.NameExportBy, opt => opt.MapFrom(src => src.UserExport == null ? "--------" : src.UserExport.Fullname));

            CreateMap<WmsBranchExportProduct, BranchExportProductResponseDto>()
                .ForMember(dest => dest.ProductName, opt => opt.MapFrom(src => src.WmsProduct == null ? "--------" : src.WmsProduct.ProductName))
                .ForMember(dest => dest.ProductImage, opt => opt.MapFrom(src => src.WmsProduct == null ? "--------" : src.WmsProduct.ProductImage))
                .ForMember(dest => dest.TotalPrice, opt => opt.MapFrom(src => src.Quantity * src.UnitPrice));

            CreateMap<WmsProduct, GetStockQuantityResponseDto>();

            CreateMap<WmsBaseImportProduct, GetAllImportsByIdResponseDto>()
                  .ForMember(dest => dest.ImportCode, opt => opt.MapFrom(src => src.WmsBaseImport.ImportCode));

            CreateMap<WmsBranchImportProduct, GetAllImportsByIdResponseDto>()
                  .ForMember(dest => dest.ImportCode, opt => opt.MapFrom(src => src.WmsBranchImport.ImportCode))
                  .ForMember(dest => dest.BaseImportCode, opt => opt.MapFrom(src => src.WmsBaseImport.ImportCode));

            CreateMap<WmsBaseStockCheckSheet, StockCheckSheetResponseDto>()
                .ForMember(dest => dest.CheckBy, opt => opt.MapFrom(src => src.UserCheck.Username));
            CreateMap<WmsBaseStockCheckSheetDetail, StockCheckSheetDetailsResponseDto>()
                .ForMember(dest => dest.SKU, opt => opt.MapFrom(src => src.WmsProduct.SKU))
                 .ForMember(dest => dest.ProductImage, opt => opt.MapFrom(src => src.WmsProduct.ProductImage))
                 .ForMember(dest => dest.ProductName, opt => opt.MapFrom(src => src.WmsProduct.ProductName))
                  .ForMember(dest => dest.Quantity, opt => opt.MapFrom(src => src.BaseQuantity));

            CreateMap<WmsBranchStockCheckSheet, StockCheckSheetResponseDto>()
                 .ForMember(dest => dest.CheckBy, opt => opt.MapFrom(src => src.UserCheck.Username));
            CreateMap<WmsBranchStockCheckSheetDetail, StockCheckSheetDetailsResponseDto>()
                 .ForMember(dest => dest.SKU, opt => opt.MapFrom(src => src.WmsProduct.SKU))
                 .ForMember(dest => dest.ProductImage, opt => opt.MapFrom(src => src.WmsProduct.ProductImage))
                 .ForMember(dest => dest.ProductName, opt => opt.MapFrom(src => src.WmsProduct.ProductName))
                  .ForMember(dest => dest.Quantity, opt => opt.MapFrom(src => src.BranchQuantity));

            CreateMap<WmsBaseExportProduct, BaseExportProductDetailsResponseDto>()
                .ForMember(dest => dest.ImportId, opt => opt.MapFrom(src => src.BaseImportId))
                .ForMember(dest => dest.Quantity, opt => opt.MapFrom(src => src.Quantity))
                 .ForMember(dest => dest.ImportCode, opt => opt.MapFrom(src => src.WmsBaseImport == null ? "--------" : src.WmsBaseImport.ImportCode));

            CreateMap<WmsBranchExportProduct, BranchExportProductDetailsResponseDto>()
                .ForMember(dest => dest.ImportId, opt => opt.MapFrom(src => src.ImportId))
                .ForMember(dest => dest.Quantity, opt => opt.MapFrom(src => src.Quantity))
                 .ForMember(dest => dest.ImportCode, opt => opt.MapFrom(src => src.WmsBranchImport == null ? "--------" : src.WmsBranchImport.ImportCode));

            CreateMap<WmsBranchImportProduct, BranchImportProductResponseList>();

            CreateMap<WmsBaseInvoice, InvoiceResponseDto>();

            CreateMap<WmsBranchInvoice, InvoiceResponseDto>();

            CreateMap<WmsProduct, GetBranchProductInStockByNameResponseDto>();

            CreateMap<WmsBaseStockCheckSheet, AddStockCheckSheetRequestDto>();
            CreateMap<WmsBaseStockCheckSheetDetail, AddStockCheckSheetDetailRequestDto>()
                .ForMember(dest => dest.StockQuantity, opt => opt.MapFrom(src => src.BaseQuantity))
                 .ForMember(dest => dest.BaseImportId, opt => opt.Ignore());

            CreateMap<WmsBranchStockCheckSheet, AddStockCheckSheetRequestDto>();
            CreateMap<WmsBranchStockCheckSheetDetail, AddStockCheckSheetDetailRequestDto>()
                .ForMember(dest => dest.StockQuantity, opt => opt.MapFrom(src => src.BranchQuantity));

            CreateMap<WmsBaseInvoice, InvoiceDataResponseDto>();
            CreateMap<WmsBranchInvoice, InvoiceDataResponseDto>();

            CreateMap<WmsBranchDebt, BranchDebtDto>();
            CreateMap<WmsBranchInvoice, BranchInvoiceDto>();
            CreateMap<WmsBaseExport, BaseHistoryExportResponseDto>();
            CreateMap<WmsSupplier, SupplierSimpleResponseDto>();
            CreateMap<WmsBaseInvoice, BaseInvoiceDto>();

            CreateMap<WmsSupplier, SupplierHaveDebtResponseDto>()
                .ForMember(dest => dest.DebtAmount, opt => opt.MapFrom(src => src.WmsBaseDebts!.FirstOrDefault()!.DebtAmount));
            CreateMap<WmsBranchWarehouse, BranchWarehouseHaveDebtResponseDto>()
                .ForMember(dest => dest.DebtAmount, opt => opt.MapFrom(src => src.WmsBranchDebts!.FirstOrDefault()!.DebtAmount));
        }

        public int GetTotalProductReturn(ICollection<WmsBaseImport> baseImport)
        {
            int count = 0;
            if (baseImport != null)
            {
                foreach (var item in baseImport)
                {
                    count += item.WmsBaseProductReturns!.Count();
                }
            }
            return count;
        }

    }
}
