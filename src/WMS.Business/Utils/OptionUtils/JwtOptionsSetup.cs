﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using WMS.Business.Dtos.OptionDto;

namespace WMS.Business.Utils.OptionUtils
{
    public class JwtOptionsSetup : IConfigureOptions<JwtOptions>
    {
        private readonly IConfiguration _configuration;

        public JwtOptionsSetup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void Configure(JwtOptions options)
        {
            _configuration.GetSection(JwtOptions.Section)
                .Bind(options);
        }
    }
}
