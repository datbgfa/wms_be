﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using WMS.Business.Dtos.OptionDto;

namespace WMS.Business.Utils.OptionUtils
{
    public class ConnectionStringsOptionsSetup : IConfigureOptions<ConnectionStringsOptions>
    {
        private readonly IConfiguration _configuration;

        public ConnectionStringsOptionsSetup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void Configure(ConnectionStringsOptions options)
        {
            _configuration.GetSection("ConnectionStrings")
                .Bind(options);
        }
    }
}
