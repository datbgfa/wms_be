﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using WMS.Business.Dtos.OptionDto;

namespace WMS.Business.Utils.OptionUtils
{
    public class ObjectStorageOptionsSetup : IConfigureOptions<ObjectStorageOptions>
    {
        private readonly IConfiguration _configuration;

        public ObjectStorageOptionsSetup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void Configure(ObjectStorageOptions options)
        {
            _configuration.GetSection(ObjectStorageOptions.Section)
                .Bind(options);
        }
    }
}
