﻿using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace WMS.Business.Utils.StringUtils
{
    public static class StringHelper
    {
        /// <summary>
        ///     Convert string to integer
        /// </summary>
        /// <param name="enteredString"></param>
        /// <returns>integer number, deafault return 0</returns>
        public static int ConvertToInt(string enteredString)
        {
            int value;
            return int.TryParse(enteredString, out value) ? value : 0;
        }

        public static bool IsValidEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                return false;

            try
            {
                // Normalize the domain
                email = Regex.Replace(email, @"(@)(.+)$", DomainMapper,
                                      RegexOptions.None, TimeSpan.FromMilliseconds(200));

                // Examines the domain part of the email and normalizes it.
                string DomainMapper(Match match)
                {
                    // Use IdnMapping class to convert Unicode domain names.
                    var idn = new IdnMapping();

                    // Pull out and process domain name (throws ArgumentException on invalid)
                    string domainName = idn.GetAscii(match.Groups[2].Value);

                    return match.Groups[1].Value + domainName;
                }
            }
            catch (RegexMatchTimeoutException e)
            {
                return false;
            }
            catch (ArgumentException e)
            {
                return false;
            }

            try
            {
                return Regex.IsMatch(email,
                    @"^[^@\s]+@[^@\s]+\.[^@\s]+$",
                    RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }

        public static bool IsValidUsername(string username)
        {
            if (string.IsNullOrWhiteSpace(username))
            {
                return false;
            }

            try
            {
                return Regex.IsMatch(username, @"^[A-Za-z0-9]+$",
                    RegexOptions.Compiled, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }

        /// <summary>
        ///     Check string is integer or not
        /// </summary>
        /// <param name="enteredString"></param>
        /// <returns>true or false</returns>
        public static bool IsIntNumber(string enteredString)
        {
            try
            {
                Convert.ToInt32(enteredString, CultureInfo.CurrentCulture);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        ///     Generate uuid v4 string
        /// </summary>
        /// <returns>uuid string</returns>
        public static string GenerateUUID()
        {
            Guid uuid = Guid.NewGuid();
            return uuid.ToString();
        }

        /// <summary>
        ///     Convert string to base64 format
        /// </summary>
        /// <param name="enteredString"></param>
        /// <returns>Base64 format string</returns>
        public static string ConvertStringToBase64(string enteredString)
        {
            var stringBytes = Encoding.UTF8.GetBytes(enteredString);
            return Convert.ToBase64String(stringBytes);
        }

        /// <summary>
        ///     Convert base64 string to original
        /// </summary>
        /// <param name="base64String"></param>
        /// <returns>Original string</returns>
        public static string ConvertBase64ToString(string base64String)
        {
            var stringBytes = Convert.FromBase64String(base64String);
            return Encoding.UTF8.GetString(stringBytes);
        }

        /// <summary>
        ///     Generate random string
        /// </summary>
        /// <param name="length"></param>
        /// <param name="hasNumber"></param>
        /// <param name="hasUppercaseChar"></param>
        /// <param name="hasSpecialChar"></param>
        /// <returns>Random string</returns>
        public static string RandomStringGenerate(int length, bool hasNumber = false, bool hasUppercaseChar = false, bool hasSpecialChar = false)
        {
            var random = new Random();
            var seed = random.Next(1, int.MaxValue);
            const string allowedChars = "abcdefghijkmnopqrstuvwxyz";
            const string upperChars = "ABCDEFGHJKLMNOPQRSTUVWXYZ";
            const string numberChars = "0123456789";
            const string specialChars = @"!#$%&'()*+,-./:;<=>?@[\]_";
            var chars = new char[length];
            var rd = new Random(seed);
            for (var i = 0; i < length; i++)
            {
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            }
            var randomIndexs = new List<int>();
            var randomIndex = -1;
            if (hasNumber)
            {
                do
                {
                    randomIndex = rd.Next(0, length);
                    if (!randomIndexs.Contains(randomIndex))
                    {
                        randomIndexs.Add(randomIndex);
                        break;
                    }
                } while (randomIndexs.Contains(randomIndex));
                chars[randomIndex] = numberChars[rd.Next(0, numberChars.Length)];
            }
            if (hasUppercaseChar)
            {
                do
                {
                    randomIndex = rd.Next(0, length);
                    if (!randomIndexs.Contains(randomIndex))
                    {
                        randomIndexs.Add(randomIndex);
                        break;
                    }
                } while (randomIndexs.Contains(randomIndex));
                chars[randomIndex] = upperChars[rd.Next(0, upperChars.Length)];
            }
            if (hasSpecialChar)
            {
                do
                {
                    randomIndex = rd.Next(0, length);
                    if (!randomIndexs.Contains(randomIndex))
                    {
                        randomIndexs.Add(randomIndex);
                        break;
                    }
                } while (randomIndexs.Contains(randomIndex));
                chars[randomIndex] = specialChars[rd.Next(0, specialChars.Length)];
            }
            return new string(chars);
        }

        /// <summary>
        /// GeneratePassword
        /// </summary>
        /// <param name="length"></param>
        /// <param name="lowerChar"></param>
        /// <param name="upperchar"></param>
        /// <param name="numeric"></param>
        /// <param name="special"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public static string GeneratePassword(int length, int lowerChar, int upperchar, int numeric, int special)
        {
            const string LowercaseChars = "abcdefghijklmnopqrstuvwxyz";
            const string UppercaseChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            const string NumericChars = "0123456789";
            const string SpecialChars = "!@#$%^&*()-_+=<>?";

            if (length < lowerChar + upperchar + numeric + special)
            {
                throw new ArgumentException("The total number of characters exceeds the desired password length.");
            }

            var random = new Random();
            var passwordChars = new char[length];

            for (int i = 0; i < lowerChar; i++)
            {
                passwordChars[i] = LowercaseChars[random.Next(LowercaseChars.Length)];
            }

            for (int i = 0; i < upperchar; i++)
            {
                passwordChars[lowerChar + i] = UppercaseChars[random.Next(UppercaseChars.Length)];
            }

            for (int i = 0; i < numeric; i++)
            {
                passwordChars[lowerChar + upperchar + i] = NumericChars[random.Next(NumericChars.Length)];
            }

            for (int i = 0; i < special; i++)
            {
                passwordChars[lowerChar + upperchar + numeric + i] = SpecialChars[random.Next(SpecialChars.Length)];
            }

            for (int i = lowerChar + upperchar + numeric + special; i < length; i++)
            {
                int charSet = random.Next(4); // Randomly choose from lowercase, uppercase, numeric, or special characters
                switch (charSet)
                {
                    case 0:
                        passwordChars[i] = LowercaseChars[random.Next(LowercaseChars.Length)];
                        break;
                    case 1:
                        passwordChars[i] = UppercaseChars[random.Next(UppercaseChars.Length)];
                        break;
                    case 2:
                        passwordChars[i] = NumericChars[random.Next(NumericChars.Length)];
                        break;
                    case 3:
                        passwordChars[i] = SpecialChars[random.Next(SpecialChars.Length)];
                        break;
                }
            }

            // Shuffle the password characters to randomize the order
            for (int i = passwordChars.Length - 1; i > 0; i--)
            {
                int j = random.Next(0, i + 1);
                (passwordChars[i], passwordChars[j]) = (passwordChars[j], passwordChars[i]);
            }

            return new string(passwordChars);
        }

        public static string GenerateSpecifyId(int length, string prefix)
        {
            try
            {
                string currentDate = DateTime.Now.ToString("ddMMyy");
                string randomPart = Guid.NewGuid().ToString("N").Substring(0, 8);
                int validPrefixLength = length - currentDate.Length - randomPart.Length - 2;
                if (prefix.Length > validPrefixLength)
                {
                    throw new ArgumentOutOfRangeException(nameof(prefix));
                }

                string result = string.Format("{0}_{1}_{2}", prefix, currentDate, randomPart);

                return result;
            }
            catch
            {
                return string.Empty;
            }
        }
    }
}
