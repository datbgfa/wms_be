﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System.Text;

namespace WMS.Business.Utils.StringUtils
{
    public static class HashingHelper
    {

        /// <summary>
        /// Encrypt password with SHA256 algorithm
        /// </summary>
        /// <param name="originPassword"></param>
        /// <param name="salt"></param>
        /// <returns>Encrypt password string, default return empty string</returns>
        public static string EncryptPassword(string? originPassword, string salt)
        {
            if (string.IsNullOrWhiteSpace(originPassword) || string.IsNullOrWhiteSpace(salt))
            {
                throw new ArgumentNullException(nameof(originPassword), " is null or blank!");
            }
            byte[] saltkey = Encoding.UTF8.GetBytes(salt);
            var encryptPassword = KeyDerivation.Pbkdf2(
                password: originPassword,
                salt: saltkey,
                prf: KeyDerivationPrf.HMACSHA256,
                iterationCount: 10000,
                numBytesRequested: 256 / 8);

            string result = Convert.ToHexString(encryptPassword).ToLower();

            return result;
        }


        /// <summary>
        /// Compare password encrypt string
        /// </summary>
        /// <param name="originPassword"></param>
        /// <param name="storedPassword"></param>
        /// <param name="salt"></param>
        /// <returns>True or false</returns>
        public static bool VerifyPassword(string originPassword, string storedPassword, string salt)
        {
            try
            {
                string encryptPassword = EncryptPassword(originPassword, salt);
                return storedPassword.Equals(encryptPassword);
            }
            catch (Exception ex)
            {
                return false;
            }
        }

    }
}
