﻿using System.ComponentModel.DataAnnotations;

namespace WMS.Business.Utils.CustomValidation
{
    public class GreaterThanDecimalAttribute : ValidationAttribute
    {
        private readonly int _value;

        public GreaterThanDecimalAttribute(int value)
        {
            _value = value;
        }

        public override bool IsValid(object? value)
        {
            if (value == null) return false;
            return Convert.ToDecimal(value) > _value;
        }
    }
}
