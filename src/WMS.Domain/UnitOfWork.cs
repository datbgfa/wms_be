﻿using Microsoft.EntityFrameworkCore.Storage;
using System.Data;
using WMS.Domain.Abstractions;
using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Repositories;

namespace WMS.Domain
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private bool disposed = false;

        private readonly AppDbContext _context;

        public UnitOfWork(AppDbContext context)
        {
            _context = context;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed && disposing)
            {
                _context.Dispose();
            }
            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IDbTransaction BeginTransaction()
        {
            var transaction = _context.Database.BeginTransaction();
            return transaction.GetDbTransaction();
        }

        public async Task CommitAsync()
        {
            await _context.SaveChangesAsync().ConfigureAwait(false);
        }

        public void Commit()
        {
            _context.SaveChanges();
        }

        #region UnitOfWork members
        private IWmsUserRepository _userRepository;
        private IWmsTokenRepository _tokenRepository;
        private IWmsBranchWarehouseRepository _branchWarehouseRepository;
        private IWmsBaseWarehouseRepository _baseWarehouseRepository;
        private IWmsProductRepository _wmsProductRepository;
        private IWmsUserRoleRepository _userRoleRepository;
        private IWmsProductAttributeRepository _productAttributeRepository;
        private IWmsCategoryRepository _categoryRepository;
        private IWmsBranchWarehouseUserRepository _branchWarehouseUserRepository;
        private IWmsBaseWarehouseUserRepository _baseWarehouseUserRepository;
        private IWmsBaseExportProductRepository _baseExportProductRepository;
        private IWmsBaseImportProductRepository _baseImportProductRepository;
        private IWmsBaseExportRepository _baseExportRepository;
        private IWmsBaseImportRepository _baseImportRepository;
        private IWmsBaseDebtRepository _baseDebtRepository;
        private IWmsBaseInvoiceRepository _baseInvoiceRepository;
        private IWmsBaseProductReturnRepository _baseProductReturnRepository;
        private IWmsBaseProductReturnDetailRepository _baseProductReturnDetailRepository;
        private IWmsSupplierRepository _supplierRepository;
        private IWmsBranchRequestRepository _branchRequestRepository;
        private IWmsBranchProductRequestRepository _branchProductRequestRepository;
        private IWmsBranchImportRepository _branchImportRepository;
        private IWmsBranchDebtRepository _branchDebtRepository;
        private IWmsBranchImportProductRepository _branchImportProductRepository;
        private IWmsBranchInvoiceRepository _branchInvoiceRepository;
        private IWmsBranchProductReturnDetailRepository _branchProductReturnDetailRepository;
        private IWmsBranchExportRepository _branchExportRepository;
        private IWmsBranchExportProductRepository _branchExportProductRepository;
        private IWmsBranchProductReturnRepository _branchProductReturnRepository;
        private IWmsBaseStockCheckSheetRepository _baseStockCheckSheetRepository;
        private IWmsBaseStockCheckSheetDetailRepository _baseStockCheckSheetDetailRepository;
        private IWmsBranchStockCheckSheetRepository _branchStockCheckSheetRepository;
        private IWmsBranchStockCheckSheetDetailRepository _branchStockCheckSheetDetailRepository;
        private IWmsBranchWarehouseProductRepository _branchWarehouseProductRepository;

        #endregion

        #region Unitofwork getters
        public IWmsUserRepository UserRepository { get { return _userRepository ??= new WmsUserRepository(_context); } }
        public IWmsTokenRepository TokenRepository { get { return _tokenRepository ??= new WmsTokenRepository(_context); } }
        public IWmsBranchWarehouseRepository BranchWarehouseRepository { get { return _branchWarehouseRepository ??= new WmsBranchWarehouseRepository(_context); } }
        public IWmsBaseWarehouseRepository BaseWarehouseRepository { get { return _baseWarehouseRepository ??= new WmsBaseWarehouseRepository(_context); } }
        public IWmsUserRoleRepository UserRoleRepository { get { return _userRoleRepository ??= new WmsUserRoleRepository(_context); } }
        public IWmsProductRepository ProductRepository { get { return _wmsProductRepository ??= new WmsProductRepository(_context); } }
        public IWmsProductAttributeRepository ProductAttributeRepository { get { return _productAttributeRepository ??= new WmsProductAttributeRepository(_context); } }
        public IWmsCategoryRepository CategoryRepository { get { return _categoryRepository ??= new WmsCategoryRepository(_context); } }
        public IWmsBranchWarehouseUserRepository BranchWarehouseUserRepository { get { return _branchWarehouseUserRepository ??= new WmsBranchWarehouseUserRepository(_context); } }
        public IWmsBaseWarehouseUserRepository BaseWarehouseUserRepository { get { return _baseWarehouseUserRepository ??= new WmsBaseWarehouseUserRepository(_context); } }
        public IWmsBaseExportProductRepository BaseExportProductRepository { get { return _baseExportProductRepository ??= new WmsBaseExportProductRepository(_context); } }
        public IWmsBaseImportProductRepository BaseImportProductRepository { get { return _baseImportProductRepository ??= new WmsBaseImportProductRepository(_context); } }
        public IWmsBaseExportRepository BaseExportRepository { get { return _baseExportRepository ??= new WmsBaseExportRepository(_context); } }
        public IWmsBaseImportRepository BaseImportRepository { get { return _baseImportRepository ??= new WmsBaseImportRepository(_context); } }
        public IWmsBaseDebtRepository BaseDebtRepository { get { return _baseDebtRepository ??= new WmsBaseDebtRepository(_context); } }
        public IWmsBaseInvoiceRepository BaseInvoiceRepository { get { return _baseInvoiceRepository ??= new WmsBaseInvoiceRepository(_context); } }
        public IWmsBaseProductReturnRepository BaseProductReturnRepository { get { return _baseProductReturnRepository ??= new WmsBaseProductReturnRepository(_context); } }
        public IWmsBaseProductReturnDetailRepository BaseProductReturnDetailRepository { get { return _baseProductReturnDetailRepository ??= new WmsBaseProductReturnDetailRepository(_context); } }
        public IWmsSupplierRepository SupplierRepository { get { return _supplierRepository ??= new WmsSupplierRepository(_context); } }
        public IWmsBranchRequestRepository BranchRequestRepository { get { return _branchRequestRepository ??= new WmsBranchRequestRepository(_context); } }
        public IWmsBranchProductRequestRepository BranchProductRequestRepository { get { return _branchProductRequestRepository ??= new WmsBranchProductRequestRepository(_context); } }
        public IWmsBranchImportRepository BranchImportRepository { get { return _branchImportRepository ??= new WmsBranchImportRepository(_context); } }
        public IWmsBranchImportProductRepository BranchImportProductRepository { get { return _branchImportProductRepository ??= new WmsBranchImportProductRepository(_context); } }
        public IWmsBranchDebtRepository BranchDebtRepository { get { return _branchDebtRepository ??= new WmsBranchDebtRepository(_context); } }
        public IWmsBranchInvoiceRepository BranchInvoiceRepository { get { return _branchInvoiceRepository ??= new WmsBranchInvoiceRepository(_context); } }
        public IWmsBranchProductReturnDetailRepository BranchProductReturnDetailRepository { get { return _branchProductReturnDetailRepository ??= new WmsBranchProductReturnDetailRepository(_context); } }
        public IWmsBranchExportRepository BranchExportRepository { get { return _branchExportRepository ??= new WmsBranchExportRepository(_context); } }
        public IWmsBranchExportProductRepository BranchExportProductRepository { get { return _branchExportProductRepository ??= new WmsBranchExportProductRepository(_context); } }
        public IWmsBranchProductReturnRepository BranchProductReturnRepository { get { return _branchProductReturnRepository ??= new WmsBranchProductReturnRepository(_context); } }
        public IWmsBaseStockCheckSheetRepository BaseStockCheckSheetRepository { get { return _baseStockCheckSheetRepository ??= new WmsBaseStockCheckSheetRepository(_context); } }
        public IWmsBaseStockCheckSheetDetailRepository BaseStockCheckSheetDetailRepository { get { return _baseStockCheckSheetDetailRepository ??= new WmsBaseStockCheckSheetDetailRepository(_context); } }
        public IWmsBranchStockCheckSheetRepository BranchStockCheckSheetRepository { get { return _branchStockCheckSheetRepository ??= new WmsBranchStockCheckSheetRepository(_context); } }
        public IWmsBranchStockCheckSheetDetailRepository BranchStockCheckSheetDetailRepository { get { return _branchStockCheckSheetDetailRepository ??= new WmsBranchStockCheckSheetDetailRepository(_context); } }
        public IWmsBranchWarehouseProductRepository BranchWarehouseProductRepository { get { return _branchWarehouseProductRepository ??= new WmsBranchWarehouseProductRepository(_context); } }
        #endregion
    }
}
