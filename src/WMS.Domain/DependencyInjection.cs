﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WMS.Domain.Abstractions;
using WMS.Domain.EfInterceptors;

namespace WMS.Domain
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddDomain(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddDbContext<AppDbContext>(
                (provider, options) =>
                {
                    options.UseNpgsql(
                        configuration.GetConnectionString("Postgres")!,
                        config =>
                        {
                            config.MigrationsAssembly(nameof(Domain));
                        }).AddInterceptors(new SoftDateInterceptor(provider.GetService<IHttpContextAccessor>()!));
                });
            services.AddScoped<DbInitializer>();
            return services;
        }
    }
}