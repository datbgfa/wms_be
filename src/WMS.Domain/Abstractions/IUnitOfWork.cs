﻿using System.Data;
using WMS.Domain.Abstractions.Repository;

namespace WMS.Domain.Abstractions
{
    public interface IUnitOfWork
    {
        public Task CommitAsync();

        public void Commit();

        public IDbTransaction BeginTransaction();

        public IWmsUserRepository UserRepository { get; }
        public IWmsTokenRepository TokenRepository { get; }
        public IWmsBranchWarehouseRepository BranchWarehouseRepository { get; }
        public IWmsBaseWarehouseRepository BaseWarehouseRepository { get; }
        public IWmsProductRepository ProductRepository { get; }
        public IWmsUserRoleRepository UserRoleRepository { get; }
        public IWmsProductAttributeRepository ProductAttributeRepository { get; }
        public IWmsCategoryRepository CategoryRepository { get; }
        public IWmsBaseWarehouseUserRepository BaseWarehouseUserRepository { get; }
        public IWmsBranchWarehouseUserRepository BranchWarehouseUserRepository { get; }
        public IWmsBaseExportProductRepository BaseExportProductRepository { get; }
        public IWmsBaseImportProductRepository BaseImportProductRepository { get; }
        public IWmsBaseExportRepository BaseExportRepository { get; }
        public IWmsBaseImportRepository BaseImportRepository { get; }
        public IWmsBaseDebtRepository BaseDebtRepository { get; }
        public IWmsBaseInvoiceRepository BaseInvoiceRepository { get; }
        public IWmsBaseProductReturnRepository BaseProductReturnRepository { get; }
        public IWmsBaseProductReturnDetailRepository BaseProductReturnDetailRepository { get; }
        public IWmsSupplierRepository SupplierRepository { get; }
        public IWmsBranchRequestRepository BranchRequestRepository { get; }
        public IWmsBranchProductRequestRepository BranchProductRequestRepository { get; }
        public IWmsBranchImportRepository BranchImportRepository { get; }
        public IWmsBranchDebtRepository BranchDebtRepository { get; }
        public IWmsBranchImportProductRepository BranchImportProductRepository { get; }
        public IWmsBranchInvoiceRepository BranchInvoiceRepository { get; }
        public IWmsBranchProductReturnDetailRepository BranchProductReturnDetailRepository { get; }
        public IWmsBranchExportRepository BranchExportRepository { get; }
        public IWmsBranchExportProductRepository BranchExportProductRepository { get; }
        public IWmsBranchProductReturnRepository BranchProductReturnRepository { get; }
        public IWmsBaseStockCheckSheetRepository BaseStockCheckSheetRepository { get; }
        public IWmsBaseStockCheckSheetDetailRepository BaseStockCheckSheetDetailRepository { get; }
        public IWmsBranchStockCheckSheetRepository BranchStockCheckSheetRepository { get; }
        public IWmsBranchStockCheckSheetDetailRepository BranchStockCheckSheetDetailRepository { get; }
        public IWmsBranchWarehouseProductRepository BranchWarehouseProductRepository { get; }
    }
}
