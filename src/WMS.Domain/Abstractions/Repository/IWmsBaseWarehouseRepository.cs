﻿using WMS.Domain.Entities;

namespace WMS.Domain.Abstractions.Repository
{
    public interface IWmsBaseWarehouseRepository : IRepositoryBase<WmsBaseWarehouse>
    {
        Task<WmsBaseWarehouse> FindBaseWarehouseByWarehouseOwner(Guid warehouseOwner);
        IQueryable<WmsBaseWarehouse> GetBaseWarehouseIncludingOwner();
    }
}
