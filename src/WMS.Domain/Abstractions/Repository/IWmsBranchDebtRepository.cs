﻿using WMS.Domain.Entities;

namespace WMS.Domain.Abstractions.Repository
{
    public interface IWmsBranchDebtRepository : IRepositoryBase<WmsBranchDebt>
    {
        Task<List<WmsBranchWarehouse>> GetBranchHaveDebt();
    }
}
