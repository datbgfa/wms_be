﻿using WMS.Domain.Entities;

namespace WMS.Domain.Abstractions.Repository
{
    public interface IWmsBaseStockCheckSheetRepository : IRepositoryBase<WmsBaseStockCheckSheet>
    {
        IQueryable<WmsBaseStockCheckSheet> SearchBaseStockCheckSheet(string? searchWord, DateTime startDate, DateTime endDate);
        IQueryable<WmsBaseStockCheckSheet> GetStockCheckSheetById(Guid checkId);
        //IQueryable<WmsBaseStockCheckSheet> GetStockCheckSheetByCode(string code);
    }
}
