﻿using WMS.Domain.Entities;

namespace WMS.Domain.Abstractions.Repository
{
    public interface IWmsBaseInvoiceRepository : IRepositoryBase<WmsBaseInvoice>
    {
        IQueryable<WmsBaseInvoice> GetDebt(int supplierId);
        IQueryable<WmsBaseInvoice> GetBaseInvoices(short? type, DateTime? from, DateTime? to, string invoiceFor, int skip, int take);
        Task<int> CountBaseInvoices(short? type, DateTime? from, DateTime? to, string invoiceFor);
        decimal GetMoneyPaid(DateTime from, DateTime to);
        decimal GetMoneyReceived(DateTime from, DateTime to);
    }
}
