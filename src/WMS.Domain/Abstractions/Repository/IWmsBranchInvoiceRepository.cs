﻿using WMS.Domain.Entities;

namespace WMS.Domain.Abstractions.Repository
{
    public interface IWmsBranchInvoiceRepository : IRepositoryBase<WmsBranchInvoice>
    {
        IQueryable<WmsBranchInvoice> GetBranchInvoices(short? type, DateTime? from, DateTime? to, string invoiceFor, int skip, int take, Guid branchId);
        Task<int> CountBranchInvoices(short? type, DateTime? from, DateTime? to, string invoiceFor, Guid branchId);
        decimal GetMoneyPaid(DateTime from, DateTime to, Guid warehouseId);
        decimal GetMoneyReceived(DateTime from, DateTime to, Guid warehouseId);
    }
}
