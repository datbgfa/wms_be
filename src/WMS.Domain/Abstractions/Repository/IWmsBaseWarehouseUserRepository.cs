﻿using WMS.Domain.Entities;

namespace WMS.Domain.Abstractions.Repository
{
    public interface IWmsBaseWarehouseUserRepository : IRepositoryBase<WmsBaseWarehouseUser>
    {
    }
}
