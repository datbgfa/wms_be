﻿using WMS.Domain.Entities;

namespace WMS.Domain.Abstractions.Repository
{
    public interface IWmsBranchStockCheckSheetRepository : IRepositoryBase<WmsBranchStockCheckSheet>
    {
        IQueryable<WmsBranchStockCheckSheet> SearchBranchStockCheckSheet(string? searchWord, Guid warehouseId, DateTime startDate, DateTime endDate);
        IQueryable<WmsBranchStockCheckSheet> GetStockCheckSheetById(Guid checkId, Guid warehouseId);

        //IQueryable<WmsBranchStockCheckSheet> GetStockCheckSheetByCode(string code);
    }
}
