﻿using WMS.Domain.Entities;

namespace WMS.Domain.Abstractions.Repository
{
    public interface IWmsBranchImportProductRepository : IRepositoryBase<WmsBranchImportProduct>
    {
        int GetBranchQuantityByProductId(Guid productId, Guid warehouseId);
        IQueryable<WmsBranchImportProduct> GetBranchStockOrderByImportDate(Guid productId, Guid warehouseId);
        Task<IEnumerable<WmsBranchImportProduct>> GetBranchImportProductByImportIdAndProductId(Guid branchImportId, Guid productId);

        //IQueryable<WmsBranchImportProduct> GetByConditions(Guid warehouseId, Guid productId);
    }
}
