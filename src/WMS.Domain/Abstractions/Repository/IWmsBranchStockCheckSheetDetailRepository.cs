﻿using WMS.Domain.Entities;

namespace WMS.Domain.Abstractions.Repository
{
    public interface IWmsBranchStockCheckSheetDetailRepository : IRepositoryBase<WmsBranchStockCheckSheetDetail>
    {
        IQueryable<Object> GetAllStockCheckSheetDetail(Guid checkId, Guid warehouseId);

        IQueryable<Object> GetStockCheckSheetImports(Guid checkId, Guid productId, Guid warehouseId);
        IQueryable<Object> GetImportsByProductId(Guid productId, Guid warehouseId);
    }
}
