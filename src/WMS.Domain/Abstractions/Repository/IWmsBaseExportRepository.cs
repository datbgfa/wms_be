﻿using WMS.Domain.Entities;

namespace WMS.Domain.Abstractions.Repository
{
    public interface IWmsBaseExportRepository : IRepositoryBase<WmsBaseExport>
    {
        Task<IQueryable<WmsBaseExport>> FilterSearchBaseExport(string? searchWord, short? exportStatus, short? paymentStatus, Guid? branchWarehouseId, DateTime startDate, DateTime endDate);
        Task<WmsBaseExport> GetBaseExportDetailsById(Guid exportId);

        Task<List<WmsBaseExport>> GetBaseExportByBranchWarehouseId(Guid bwId);
    }
}
