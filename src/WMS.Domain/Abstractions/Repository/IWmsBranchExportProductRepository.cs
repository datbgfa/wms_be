﻿using WMS.Domain.Entities;

namespace WMS.Domain.Abstractions.Repository
{
    public interface IWmsBranchExportProductRepository : IRepositoryBase<WmsBranchExportProduct>
    {
        //bool UpdateRange(IEnumerable<WmsBranchExportProduct> exportProducts);
        Task<IEnumerable<WmsBranchExportProduct>> GetBaseExportProductByExportIdProductIdAndListBaseImportId(Guid exportId, Guid productId, IEnumerable<Guid> branchImportId);
    }
}
