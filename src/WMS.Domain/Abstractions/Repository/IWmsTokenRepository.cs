using WMS.Domain.Entities;

namespace WMS.Domain.Abstractions.Repository
{
    public interface IWmsTokenRepository : IRepositoryBase<WmsToken>
    {
    }
}
