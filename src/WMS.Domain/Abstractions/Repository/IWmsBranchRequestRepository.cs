﻿using WMS.Domain.Entities;

namespace WMS.Domain.Abstractions.Repository
{
    public interface IWmsBranchRequestRepository : IRepositoryBase<WmsBranchRequest>
    {
        IQueryable<WmsBranchRequest> FilterBranchRequest(Guid warehouseId, string? searchWord, short? approvedSatus, DateTime startDate, DateTime endDate);
        Task<WmsBranchRequest> GetBranchRequestDetails(Guid branchRequestId);
    }
}
