﻿using WMS.Domain.Entities;

namespace WMS.Domain.Abstractions.Repository
{
    public interface IWmsBaseStockCheckSheetDetailRepository : IRepositoryBase<WmsBaseStockCheckSheetDetail>
    {
        IQueryable<Object> GetAllStockCheckSheetDetail(Guid checkId);
        IQueryable<Object> GetStockCheckSheetImports(Guid checkId, Guid productId);
        IQueryable<Object> GetImportsByProductId(Guid productId);
    }
}
