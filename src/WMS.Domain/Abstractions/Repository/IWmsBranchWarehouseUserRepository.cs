﻿using WMS.Domain.Entities;

namespace WMS.Domain.Abstractions.Repository
{
    public interface IWmsBranchWarehouseUserRepository : IRepositoryBase<WmsBranchWarehouseUser>
    {
        IQueryable<WmsBranchWarehouseUser> GetBranchWarehouseByUserId(Guid currentUser);
    }
}
