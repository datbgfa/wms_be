﻿using WMS.Domain.Entities;

namespace WMS.Domain.Abstractions.Repository
{
    public interface IWmsBranchWarehouseRepository : IRepositoryBase<WmsBranchWarehouse>
    {
        IQueryable<WmsBranchWarehouse> SearchBranchWarehousesList(string? searchWord, short? status);
        Task<WmsBranchWarehouse> FindBranchWarehouseByWarehouseOwner(Guid warehouseOwner);
        IQueryable<WmsBranchWarehouse> GetBranchWarehousesIncludingOwner(bool? hasOwner);
        IQueryable<WmsBranchWarehouse> GetBranchWarehouseByUsername(string username);
        IQueryable<WmsBranchWarehouse> GetBranchWarehouseByCurrentUser(string username);
    }
}
