﻿using WMS.Domain.Entities;

namespace WMS.Domain.Abstractions.Repository
{
    public interface IWmsProductAttributeRepository : IRepositoryBase<WmsProductAttribute>
    {
        IQueryable<WmsProductAttribute> GetProductAttibute(Guid productId);
    }
}
