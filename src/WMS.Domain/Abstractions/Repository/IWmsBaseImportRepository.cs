﻿using WMS.Domain.Entities;

namespace WMS.Domain.Abstractions.Repository
{
    public interface IWmsBaseImportRepository : IRepositoryBase<WmsBaseImport>
    {
        IQueryable<WmsBaseImport> FilterSearchBaseImport(string? searchWord,
                                                               short? importStatus,
                                                               short? paymentStatus,
                                                               int? supplierId,
                                                               DateTime? startDate,
                                                               DateTime? endDate);
        Task<WmsBaseImport> GetBaseImportDetailsById(Guid baseImportId);

        IQueryable<WmsBaseImport> GetHistoryImport(int supplierId);
        IEnumerable<WmsBaseImportProduct> GetBaseImportByWarehouseIdAndProductIds(Guid warehouseId, IEnumerable<Guid> productIds);

        Task<List<WmsBaseImport>> GetBaseImportBySupplierId(int supplierId);
    }
}
