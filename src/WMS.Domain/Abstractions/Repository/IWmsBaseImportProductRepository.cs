﻿using WMS.Domain.Entities;

namespace WMS.Domain.Abstractions.Repository
{
    public interface IWmsBaseImportProductRepository : IRepositoryBase<WmsBaseImportProduct>
    {
        IQueryable<WmsBaseImportProduct> GetBaseStockOrderByImportDate(Guid productId);

        //IQueryable<WmsBaseImportProduct> GetByConditions(Guid productId);
    }
}
