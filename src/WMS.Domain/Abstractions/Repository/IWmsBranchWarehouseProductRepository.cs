﻿using WMS.Domain.Entities;

namespace WMS.Domain.Abstractions.Repository
{
    public interface IWmsBranchWarehouseProductRepository : IRepositoryBase<WmsBranchWarehouseProduct>
    {
        decimal GetCostPriceByProductId(Guid productId, Guid warehouseId);
        Task<IEnumerable<WmsProduct>> GetProductListPaging(Guid warehouseId, int? categoryId, string? searchWord, int currentPage, int pageSize);
    }
}
