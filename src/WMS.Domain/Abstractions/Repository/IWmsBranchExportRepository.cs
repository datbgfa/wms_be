﻿using WMS.Domain.Entities;

namespace WMS.Domain.Abstractions.Repository
{
    public interface IWmsBranchExportRepository : IRepositoryBase<WmsBranchExport>
    {
        Task<IQueryable<WmsBranchExport>> FilterSearchBranchExport(string? searchWord, Guid warehouseId, DateTime startDate, DateTime endDate);
        Task<WmsBranchExport> GetBranchExportDetailsById(Guid branchExportId);
    }
}
