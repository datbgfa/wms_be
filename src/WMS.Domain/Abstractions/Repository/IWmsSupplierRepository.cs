﻿using WMS.Domain.Entities;

namespace WMS.Domain.Abstractions.Repository
{
    public interface IWmsSupplierRepository : IRepositoryBase<WmsSupplier>
    {
        IQueryable<WmsSupplier> SearchSuppliersList(string? searchWord);

        Task<WmsSupplier> GetSupplierDetails(int supplierId);

        Task<List<WmsSupplier>> GetSupplierHaveDebt();
    }
}
