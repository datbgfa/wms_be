﻿using WMS.Domain.Entities;

namespace WMS.Domain.Abstractions.Repository
{
    public interface IWmsBranchProductReturnDetailRepository : IRepositoryBase<WmsBranchProductReturnDetail>
    {
    }
}
