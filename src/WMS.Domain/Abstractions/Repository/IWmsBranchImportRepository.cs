﻿using WMS.Domain.Entities;

namespace WMS.Domain.Abstractions.Repository
{
    public interface IWmsBranchImportRepository : IRepositoryBase<WmsBranchImport>
    {
        IQueryable<WmsBranchImport> FilterBranchImport(Guid warehouseId, string? searchWord, short? importStatus, short? paymentStatus, DateTime startDate, DateTime endDate);
        Task<WmsBranchImport> GetBranchRequestDetails(Guid branchImportId);
        IEnumerable<WmsBranchImportProduct> GetBranchImportByWarehouseIdAndProductIds(Guid warehouseId, IEnumerable<Guid> productIds);
    }
}
