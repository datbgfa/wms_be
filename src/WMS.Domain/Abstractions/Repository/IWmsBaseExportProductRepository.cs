﻿using WMS.Domain.Entities;


namespace WMS.Domain.Abstractions.Repository
{
    public interface IWmsBaseExportProductRepository : IRepositoryBase<WmsBaseExportProduct>
    {
        Task<List<WmsBaseExportProduct>> GetBaseExportProductByExportIdProductIdAndListBaseImportId(Guid exportId, Guid productId, IEnumerable<Guid> baseImportId);

    }
}
