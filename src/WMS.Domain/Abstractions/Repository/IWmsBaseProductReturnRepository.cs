﻿using WMS.Domain.Entities;

namespace WMS.Domain.Abstractions.Repository
{
    public interface IWmsBaseProductReturnRepository : IRepositoryBase<WmsBaseProductReturn>
    {
        Task<List<WmsBaseProductReturn>> GetProductReturnToSupplier(int skip, int take);
        Task<List<WmsBaseProductReturn>> GetProductReturnToSupplierByImportId(Guid importId);
        Task<int> CountProductReturnToSupplierByImportId(Guid importId);
        Task<List<WmsBaseProductReturn>> GetProductReturnBySupplierId(int supplierId);
    }
}
