﻿using WMS.Domain.Entities;

namespace WMS.Domain.Abstractions.Repository
{
    public interface IWmsBranchProductReturnRepository : IRepositoryBase<WmsBranchProductReturn>
    {
        Task<List<WmsBranchProductReturn>> GetProductsReturnFromBranchByImportId(Guid importId);
        Task<List<WmsBranchProductReturn>> GetProductsReturnFromBranch(
            int skipPage, 
            int pageSize, 
            string? searchWord, 
            Guid? branchWarehouseId, 
            DateTime startDate, 
            DateTime endDate, 
            short? paymentStatus,
            short? status);
        ValueTask<int> CountProductReturnFromBranch(
            string? searchWord, 
            Guid? branchWarehouseId, 
            DateTime startDate, 
            DateTime endDate,
            short? paymentStatus,
            short? status);
        Task<List<WmsBranchProductReturn>> GetBranchProducReturnByBranchId(Guid branchId);
    }
}
