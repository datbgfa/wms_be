﻿using WMS.Domain.Entities;

namespace WMS.Domain.Abstractions.Repository
{
    public interface IWmsCategoryRepository : IRepositoryBase<WmsCategory>
    {
        IQueryable<WmsCategory> SearchAndFilterCategory(string searchWord, bool? visible);
    }
}
