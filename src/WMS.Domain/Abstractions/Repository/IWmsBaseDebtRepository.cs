﻿using WMS.Domain.Entities;

namespace WMS.Domain.Abstractions.Repository
{
    public interface IWmsBaseDebtRepository : IRepositoryBase<WmsBaseDebt>
    {
    }
}
