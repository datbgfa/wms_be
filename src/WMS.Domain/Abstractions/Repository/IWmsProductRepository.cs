﻿using WMS.Domain.Entities;

namespace WMS.Domain.Abstractions.Repository
{
    public interface IWmsProductRepository : IRepositoryBase<WmsProduct>
    {
        Task<List<WmsProduct>> GetProductListPaging(int? categoryId, string? searchWord, bool? visible, int currentPage, int pageSize);
        Task<IEnumerable<WmsProduct>> GetProductByName(string name);
        IQueryable<Object> SearchStockQuantityForBase(string? searchWord, int? categoryID);
        IQueryable<Object> SearchStockQuantityForBranch(string? searchWord, Guid warehouseId, int? categoryID);
        IQueryable<WmsBaseImportProduct> GetAllStockQuantityProductIdForBase(Guid productId);
        IQueryable<WmsBranchImportProduct> GetAllStockQuantityProductIdForBranch(Guid productId, Guid warehouseId);
        decimal GetCostPriceByProductId(Guid productId);
        int TotalProductsInStock(int type, DateTime from, DateTime to, Guid? warehouseId);
        int TotalProductsExported(int type, DateTime from, DateTime to, Guid? warehouseId);
        IEnumerable<IGrouping<Guid, WmsBaseExportProduct>> GetBaseTrendingProducts(DateTime from, DateTime to);
        IEnumerable<IGrouping<Guid, WmsBranchExportProduct>> GetBranchTrendingProducts(DateTime from, DateTime to, Guid warehouseId);
    }
}
