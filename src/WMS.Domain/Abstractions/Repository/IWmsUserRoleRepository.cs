﻿using WMS.Domain.Entities;

namespace WMS.Domain.Abstractions.Repository
{
    public interface IWmsUserRoleRepository : IRepositoryBase<WmsUserRole>
    {
    }
}
