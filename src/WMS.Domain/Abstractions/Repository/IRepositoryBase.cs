﻿using System.Linq.Expressions;

namespace WMS.Domain.Abstractions.Repository
{
    public interface IRepositoryBase<T> where T : class
    {
        Task<IEnumerable<T>> All();
        Task<T?> GetById(Guid id);
        Task<T?> GetById(int id);
        Task AddAsync(T entity);
        Task AddRangeAsync(IEnumerable<T> entity);
        void Delete(T entity);
        Task<IEnumerable<T>> FindMultiple(Expression<Func<T, bool>> predicate);
        Task<T?> FindObject(Expression<Func<T, bool>> predicate);
        Task<int> Count();
        void Update(T entity);
        string Unaccent(string text);
    }
}
