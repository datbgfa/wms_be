﻿using System.Security.Claims;
using WMS.Domain.Entities;

namespace WMS.Domain.Abstractions.Repository
{
    public interface IWmsUserRepository : IRepositoryBase<WmsUser>
    {
        IQueryable<WmsUser> SearchAccountsList(string? searchWord, short? status);
        Task<WmsUser> GetUserContainRoleByUsername(string username);
        IQueryable<WmsUser> SearchEmployeesList(string? searchWord, short? status, int type, string currentUser);
        IQueryable<WmsUser> GetUserWithRoleByUsername(string username, short? status);
        IQueryable<WmsUser> GetUserWithRoleByUserid(Guid userid, short? status);
        IQueryable<WmsUser> GetUserWithWarehouseUsersByUserid(Guid userid, short? status);
        Task<IEnumerable<WmsRole>> GetUserRoleByUsername(string username);
        Task<List<Claim>> GetUserRoleClaim(string username);
        IQueryable<WmsUser> GetAllBaseEmployee();
        IQueryable<WmsUser> GetAllBranchEmployee(Guid userid);
        IQueryable<WmsUser> GetAllBranchUser(Guid warehouseId);
    }
}
