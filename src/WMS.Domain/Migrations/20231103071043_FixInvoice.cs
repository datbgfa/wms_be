﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WMS.Domain.Migrations
{
    public partial class FixInvoice : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BaseExport_WMS_User_AssignBy",
                table: "WMS_BaseExport");

            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BaseExport_WMS_User_ExportBy",
                table: "WMS_BaseExport");

            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BranchImport_WMS_User_AssignBy",
                table: "WMS_BranchImport");

            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BranchImport_WMS_User_ImportBy",
                table: "WMS_BranchImport");

            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BranchRequest_WMS_User_AssignBy",
                table: "WMS_BranchRequest");

            migrationBuilder.DropTable(
                name: "WMS_BaseWarehouseProduct");

            migrationBuilder.DropIndex(
                name: "IX_WMS_BranchRequest_AssignBy",
                table: "WMS_BranchRequest");

            migrationBuilder.DropColumn(
                name: "AssignBy",
                table: "WMS_BranchRequest");

            migrationBuilder.DropColumn(
                name: "ExtraCost",
                table: "WMS_BranchRequest");

            migrationBuilder.DropColumn(
                name: "UnitType",
                table: "WMS_BranchProductReturnDetail");

            migrationBuilder.DropColumn(
                name: "CostPrice",
                table: "WMS_BranchProductRequest");

            migrationBuilder.DropColumn(
                name: "UnitType",
                table: "WMS_BranchImportProduct");

            migrationBuilder.DropColumn(
                name: "UnitType",
                table: "WMS_BranchExportProduct");

            migrationBuilder.DropColumn(
                name: "UnitType",
                table: "WMS_BaseProductReturnDetail");

            migrationBuilder.DropColumn(
                name: "UnitType",
                table: "WMS_BaseImportProduct");

            migrationBuilder.DropColumn(
                name: "UnitType",
                table: "WMS_BaseExportProduct");

            migrationBuilder.DropColumn(
                name: "ApprovalStatus",
                table: "WMS_BaseExport");

            migrationBuilder.RenameColumn(
                name: "CreateAt",
                table: "WMS_BranchProductReturn",
                newName: "UpdateDate");

            migrationBuilder.RenameColumn(
                name: "CreatedTime",
                table: "WMS_BranchInvoice",
                newName: "UpdateDate");

            migrationBuilder.RenameColumn(
                name: "CreateAt",
                table: "WMS_BaseProductReturn",
                newName: "UpdateDate");

            migrationBuilder.RenameColumn(
                name: "CreatedTime",
                table: "WMS_BaseInvoice",
                newName: "UpdateDate");

            migrationBuilder.RenameColumn(
                name: "ShippingStatus",
                table: "WMS_BaseExport",
                newName: "ExportStatus");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_Token",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(2023, 11, 3, 7, 10, 43, 241, DateTimeKind.Utc).AddTicks(2952),
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldDefaultValue: new DateTime(2023, 11, 2, 8, 40, 22, 714, DateTimeKind.Utc).AddTicks(9412));

            migrationBuilder.AlterColumn<string>(
                name: "UnitType",
                table: "WMS_Product",
                type: "text",
                nullable: false,
                oldClrType: typeof(short),
                oldType: "smallint");

            migrationBuilder.AddColumn<short>(
                name: "ApprovalStatus",
                table: "WMS_BranchRequest",
                type: "smallint",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.AddColumn<Guid>(
                name: "ApprovedBy",
                table: "WMS_BranchRequest",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_BranchRequest",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "CreateUser",
                table: "WMS_BranchRequest",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdateDate",
                table: "WMS_BranchRequest",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "UpdateUser",
                table: "WMS_BranchRequest",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_BranchProductReturn",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "CreateUser",
                table: "WMS_BranchProductReturn",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<decimal>(
                name: "PaidAmount",
                table: "WMS_BranchProductReturn",
                type: "money",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<short>(
                name: "PaymentStatus",
                table: "WMS_BranchProductReturn",
                type: "smallint",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.AddColumn<decimal>(
                name: "TotalPrice",
                table: "WMS_BranchProductReturn",
                type: "money",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "UpdateUser",
                table: "WMS_BranchProductReturn",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<decimal>(
                name: "Amount",
                table: "WMS_BranchInvoice",
                type: "numeric",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_BranchInvoice",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "CreateUser",
                table: "WMS_BranchInvoice",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<Guid>(
                name: "ExportId",
                table: "WMS_BranchInvoice",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ImportId",
                table: "WMS_BranchInvoice",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ReturnId",
                table: "WMS_BranchInvoice",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdateUser",
                table: "WMS_BranchInvoice",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<DateTime>(
                name: "ImportedDate",
                table: "WMS_BranchImport",
                type: "timestamp without time zone",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone");

            migrationBuilder.AlterColumn<Guid>(
                name: "ImportBy",
                table: "WMS_BranchImport",
                type: "uuid",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AlterColumn<Guid>(
                name: "AssignBy",
                table: "WMS_BranchImport",
                type: "uuid",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AddColumn<Guid>(
                name: "BaseExportId",
                table: "WMS_BranchImport",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_BranchImport",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "CreateUser",
                table: "WMS_BranchImport",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<decimal>(
                name: "TotalCost",
                table: "WMS_BranchImport",
                type: "money",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdateDate",
                table: "WMS_BranchImport",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "UpdateUser",
                table: "WMS_BranchImport",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_BranchExport",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "CreateUser",
                table: "WMS_BranchExport",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<decimal>(
                name: "TotalCost",
                table: "WMS_BranchExport",
                type: "money",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdateDate",
                table: "WMS_BranchExport",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "UpdateUser",
                table: "WMS_BranchExport",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_BaseProductReturn",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "CreateUser",
                table: "WMS_BaseProductReturn",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<decimal>(
                name: "PaidAmount",
                table: "WMS_BaseProductReturn",
                type: "money",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<short>(
                name: "PaymentStatus",
                table: "WMS_BaseProductReturn",
                type: "smallint",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.AddColumn<decimal>(
                name: "TotalPrice",
                table: "WMS_BaseProductReturn",
                type: "money",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "UpdateUser",
                table: "WMS_BaseProductReturn",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<decimal>(
                name: "Amount",
                table: "WMS_BaseInvoice",
                type: "numeric",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddColumn<Guid>(
                name: "BranchDebtId",
                table: "WMS_BaseInvoice",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_BaseInvoice",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "CreateUser",
                table: "WMS_BaseInvoice",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<Guid>(
                name: "ExportId",
                table: "WMS_BaseInvoice",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ImportId",
                table: "WMS_BaseInvoice",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ReturnId",
                table: "WMS_BaseInvoice",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdateUser",
                table: "WMS_BaseInvoice",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_BaseImport",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "CreateUser",
                table: "WMS_BaseImport",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<decimal>(
                name: "TotalCost",
                table: "WMS_BaseImport",
                type: "money",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdateDate",
                table: "WMS_BaseImport",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "UpdateUser",
                table: "WMS_BaseImport",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<decimal>(
                name: "CostPrice",
                table: "WMS_BaseExportProduct",
                type: "numeric",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AlterColumn<DateTime>(
                name: "ExportDate",
                table: "WMS_BaseExport",
                type: "timestamp without time zone",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone");

            migrationBuilder.AlterColumn<Guid>(
                name: "ExportBy",
                table: "WMS_BaseExport",
                type: "uuid",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AlterColumn<Guid>(
                name: "AssignBy",
                table: "WMS_BaseExport",
                type: "uuid",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AddColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_BaseExport",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "CreateUser",
                table: "WMS_BaseExport",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<decimal>(
                name: "ExtraCost",
                table: "WMS_BaseExport",
                type: "money",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<Guid>(
                name: "RequestId",
                table: "WMS_BaseExport",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<decimal>(
                name: "TotalCost",
                table: "WMS_BaseExport",
                type: "money",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdateDate",
                table: "WMS_BaseExport",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "UpdateUser",
                table: "WMS_BaseExport",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchRequest_ApprovedBy",
                table: "WMS_BranchRequest",
                column: "ApprovedBy");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchInvoice_ExportId",
                table: "WMS_BranchInvoice",
                column: "ExportId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchInvoice_ImportId",
                table: "WMS_BranchInvoice",
                column: "ImportId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchInvoice_ReturnId",
                table: "WMS_BranchInvoice",
                column: "ReturnId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchImport_BaseExportId",
                table: "WMS_BranchImport",
                column: "BaseExportId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BaseInvoice_BranchDebtId",
                table: "WMS_BaseInvoice",
                column: "BranchDebtId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BaseInvoice_ExportId",
                table: "WMS_BaseInvoice",
                column: "ExportId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BaseInvoice_ImportId",
                table: "WMS_BaseInvoice",
                column: "ImportId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BaseInvoice_ReturnId",
                table: "WMS_BaseInvoice",
                column: "ReturnId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BaseExport_RequestId",
                table: "WMS_BaseExport",
                column: "RequestId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BaseExport_WMS_BranchRequest_RequestId",
                table: "WMS_BaseExport",
                column: "RequestId",
                principalTable: "WMS_BranchRequest",
                principalColumn: "RequestId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BaseExport_WMS_User_AssignBy",
                table: "WMS_BaseExport",
                column: "AssignBy",
                principalTable: "WMS_User",
                principalColumn: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BaseExport_WMS_User_ExportBy",
                table: "WMS_BaseExport",
                column: "ExportBy",
                principalTable: "WMS_User",
                principalColumn: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BaseInvoice_WMS_BaseExport_ExportId",
                table: "WMS_BaseInvoice",
                column: "ExportId",
                principalTable: "WMS_BaseExport",
                principalColumn: "ExportId");

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BaseInvoice_WMS_BaseImport_ImportId",
                table: "WMS_BaseInvoice",
                column: "ImportId",
                principalTable: "WMS_BaseImport",
                principalColumn: "ImportId");

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BaseInvoice_WMS_BaseProductReturn_ReturnId",
                table: "WMS_BaseInvoice",
                column: "ReturnId",
                principalTable: "WMS_BaseProductReturn",
                principalColumn: "ProductReturnId");

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BaseInvoice_WMS_BranchDebt_BranchDebtId",
                table: "WMS_BaseInvoice",
                column: "BranchDebtId",
                principalTable: "WMS_BranchDebt",
                principalColumn: "DebtId");

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BranchImport_WMS_BaseExport_BaseExportId",
                table: "WMS_BranchImport",
                column: "BaseExportId",
                principalTable: "WMS_BaseExport",
                principalColumn: "ExportId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BranchImport_WMS_User_AssignBy",
                table: "WMS_BranchImport",
                column: "AssignBy",
                principalTable: "WMS_User",
                principalColumn: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BranchImport_WMS_User_ImportBy",
                table: "WMS_BranchImport",
                column: "ImportBy",
                principalTable: "WMS_User",
                principalColumn: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BranchInvoice_WMS_BranchExport_ExportId",
                table: "WMS_BranchInvoice",
                column: "ExportId",
                principalTable: "WMS_BranchExport",
                principalColumn: "ExportId");

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BranchInvoice_WMS_BranchImport_ImportId",
                table: "WMS_BranchInvoice",
                column: "ImportId",
                principalTable: "WMS_BranchImport",
                principalColumn: "ImportId");

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BranchInvoice_WMS_BranchProductReturn_ReturnId",
                table: "WMS_BranchInvoice",
                column: "ReturnId",
                principalTable: "WMS_BranchProductReturn",
                principalColumn: "ProductReturnId");

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BranchRequest_WMS_User_ApprovedBy",
                table: "WMS_BranchRequest",
                column: "ApprovedBy",
                principalTable: "WMS_User",
                principalColumn: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BaseExport_WMS_BranchRequest_RequestId",
                table: "WMS_BaseExport");

            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BaseExport_WMS_User_AssignBy",
                table: "WMS_BaseExport");

            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BaseExport_WMS_User_ExportBy",
                table: "WMS_BaseExport");

            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BaseInvoice_WMS_BaseExport_ExportId",
                table: "WMS_BaseInvoice");

            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BaseInvoice_WMS_BaseImport_ImportId",
                table: "WMS_BaseInvoice");

            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BaseInvoice_WMS_BaseProductReturn_ReturnId",
                table: "WMS_BaseInvoice");

            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BaseInvoice_WMS_BranchDebt_BranchDebtId",
                table: "WMS_BaseInvoice");

            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BranchImport_WMS_BaseExport_BaseExportId",
                table: "WMS_BranchImport");

            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BranchImport_WMS_User_AssignBy",
                table: "WMS_BranchImport");

            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BranchImport_WMS_User_ImportBy",
                table: "WMS_BranchImport");

            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BranchInvoice_WMS_BranchExport_ExportId",
                table: "WMS_BranchInvoice");

            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BranchInvoice_WMS_BranchImport_ImportId",
                table: "WMS_BranchInvoice");

            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BranchInvoice_WMS_BranchProductReturn_ReturnId",
                table: "WMS_BranchInvoice");

            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BranchRequest_WMS_User_ApprovedBy",
                table: "WMS_BranchRequest");

            migrationBuilder.DropIndex(
                name: "IX_WMS_BranchRequest_ApprovedBy",
                table: "WMS_BranchRequest");

            migrationBuilder.DropIndex(
                name: "IX_WMS_BranchInvoice_ExportId",
                table: "WMS_BranchInvoice");

            migrationBuilder.DropIndex(
                name: "IX_WMS_BranchInvoice_ImportId",
                table: "WMS_BranchInvoice");

            migrationBuilder.DropIndex(
                name: "IX_WMS_BranchInvoice_ReturnId",
                table: "WMS_BranchInvoice");

            migrationBuilder.DropIndex(
                name: "IX_WMS_BranchImport_BaseExportId",
                table: "WMS_BranchImport");

            migrationBuilder.DropIndex(
                name: "IX_WMS_BaseInvoice_BranchDebtId",
                table: "WMS_BaseInvoice");

            migrationBuilder.DropIndex(
                name: "IX_WMS_BaseInvoice_ExportId",
                table: "WMS_BaseInvoice");

            migrationBuilder.DropIndex(
                name: "IX_WMS_BaseInvoice_ImportId",
                table: "WMS_BaseInvoice");

            migrationBuilder.DropIndex(
                name: "IX_WMS_BaseInvoice_ReturnId",
                table: "WMS_BaseInvoice");

            migrationBuilder.DropIndex(
                name: "IX_WMS_BaseExport_RequestId",
                table: "WMS_BaseExport");

            migrationBuilder.DropColumn(
                name: "ApprovalStatus",
                table: "WMS_BranchRequest");

            migrationBuilder.DropColumn(
                name: "ApprovedBy",
                table: "WMS_BranchRequest");

            migrationBuilder.DropColumn(
                name: "CreateDate",
                table: "WMS_BranchRequest");

            migrationBuilder.DropColumn(
                name: "CreateUser",
                table: "WMS_BranchRequest");

            migrationBuilder.DropColumn(
                name: "UpdateDate",
                table: "WMS_BranchRequest");

            migrationBuilder.DropColumn(
                name: "UpdateUser",
                table: "WMS_BranchRequest");

            migrationBuilder.DropColumn(
                name: "CreateDate",
                table: "WMS_BranchProductReturn");

            migrationBuilder.DropColumn(
                name: "CreateUser",
                table: "WMS_BranchProductReturn");

            migrationBuilder.DropColumn(
                name: "PaidAmount",
                table: "WMS_BranchProductReturn");

            migrationBuilder.DropColumn(
                name: "PaymentStatus",
                table: "WMS_BranchProductReturn");

            migrationBuilder.DropColumn(
                name: "TotalPrice",
                table: "WMS_BranchProductReturn");

            migrationBuilder.DropColumn(
                name: "UpdateUser",
                table: "WMS_BranchProductReturn");

            migrationBuilder.DropColumn(
                name: "CreateDate",
                table: "WMS_BranchInvoice");

            migrationBuilder.DropColumn(
                name: "CreateUser",
                table: "WMS_BranchInvoice");

            migrationBuilder.DropColumn(
                name: "ExportId",
                table: "WMS_BranchInvoice");

            migrationBuilder.DropColumn(
                name: "ImportId",
                table: "WMS_BranchInvoice");

            migrationBuilder.DropColumn(
                name: "ReturnId",
                table: "WMS_BranchInvoice");

            migrationBuilder.DropColumn(
                name: "UpdateUser",
                table: "WMS_BranchInvoice");

            migrationBuilder.DropColumn(
                name: "BaseExportId",
                table: "WMS_BranchImport");

            migrationBuilder.DropColumn(
                name: "CreateDate",
                table: "WMS_BranchImport");

            migrationBuilder.DropColumn(
                name: "CreateUser",
                table: "WMS_BranchImport");

            migrationBuilder.DropColumn(
                name: "TotalCost",
                table: "WMS_BranchImport");

            migrationBuilder.DropColumn(
                name: "UpdateDate",
                table: "WMS_BranchImport");

            migrationBuilder.DropColumn(
                name: "UpdateUser",
                table: "WMS_BranchImport");

            migrationBuilder.DropColumn(
                name: "CreateDate",
                table: "WMS_BranchExport");

            migrationBuilder.DropColumn(
                name: "CreateUser",
                table: "WMS_BranchExport");

            migrationBuilder.DropColumn(
                name: "TotalCost",
                table: "WMS_BranchExport");

            migrationBuilder.DropColumn(
                name: "UpdateDate",
                table: "WMS_BranchExport");

            migrationBuilder.DropColumn(
                name: "UpdateUser",
                table: "WMS_BranchExport");

            migrationBuilder.DropColumn(
                name: "CreateDate",
                table: "WMS_BaseProductReturn");

            migrationBuilder.DropColumn(
                name: "CreateUser",
                table: "WMS_BaseProductReturn");

            migrationBuilder.DropColumn(
                name: "PaidAmount",
                table: "WMS_BaseProductReturn");

            migrationBuilder.DropColumn(
                name: "PaymentStatus",
                table: "WMS_BaseProductReturn");

            migrationBuilder.DropColumn(
                name: "TotalPrice",
                table: "WMS_BaseProductReturn");

            migrationBuilder.DropColumn(
                name: "UpdateUser",
                table: "WMS_BaseProductReturn");

            migrationBuilder.DropColumn(
                name: "BranchDebtId",
                table: "WMS_BaseInvoice");

            migrationBuilder.DropColumn(
                name: "CreateDate",
                table: "WMS_BaseInvoice");

            migrationBuilder.DropColumn(
                name: "CreateUser",
                table: "WMS_BaseInvoice");

            migrationBuilder.DropColumn(
                name: "ExportId",
                table: "WMS_BaseInvoice");

            migrationBuilder.DropColumn(
                name: "ImportId",
                table: "WMS_BaseInvoice");

            migrationBuilder.DropColumn(
                name: "ReturnId",
                table: "WMS_BaseInvoice");

            migrationBuilder.DropColumn(
                name: "UpdateUser",
                table: "WMS_BaseInvoice");

            migrationBuilder.DropColumn(
                name: "CreateDate",
                table: "WMS_BaseImport");

            migrationBuilder.DropColumn(
                name: "CreateUser",
                table: "WMS_BaseImport");

            migrationBuilder.DropColumn(
                name: "TotalCost",
                table: "WMS_BaseImport");

            migrationBuilder.DropColumn(
                name: "UpdateDate",
                table: "WMS_BaseImport");

            migrationBuilder.DropColumn(
                name: "UpdateUser",
                table: "WMS_BaseImport");

            migrationBuilder.DropColumn(
                name: "CostPrice",
                table: "WMS_BaseExportProduct");

            migrationBuilder.DropColumn(
                name: "CreateDate",
                table: "WMS_BaseExport");

            migrationBuilder.DropColumn(
                name: "CreateUser",
                table: "WMS_BaseExport");

            migrationBuilder.DropColumn(
                name: "ExtraCost",
                table: "WMS_BaseExport");

            migrationBuilder.DropColumn(
                name: "RequestId",
                table: "WMS_BaseExport");

            migrationBuilder.DropColumn(
                name: "TotalCost",
                table: "WMS_BaseExport");

            migrationBuilder.DropColumn(
                name: "UpdateDate",
                table: "WMS_BaseExport");

            migrationBuilder.DropColumn(
                name: "UpdateUser",
                table: "WMS_BaseExport");

            migrationBuilder.RenameColumn(
                name: "UpdateDate",
                table: "WMS_BranchProductReturn",
                newName: "CreateAt");

            migrationBuilder.RenameColumn(
                name: "UpdateDate",
                table: "WMS_BranchInvoice",
                newName: "CreatedTime");

            migrationBuilder.RenameColumn(
                name: "UpdateDate",
                table: "WMS_BaseProductReturn",
                newName: "CreateAt");

            migrationBuilder.RenameColumn(
                name: "UpdateDate",
                table: "WMS_BaseInvoice",
                newName: "CreatedTime");

            migrationBuilder.RenameColumn(
                name: "ExportStatus",
                table: "WMS_BaseExport",
                newName: "ShippingStatus");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_Token",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(2023, 11, 2, 8, 40, 22, 714, DateTimeKind.Utc).AddTicks(9412),
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldDefaultValue: new DateTime(2023, 11, 3, 7, 10, 43, 241, DateTimeKind.Utc).AddTicks(2952));

            migrationBuilder.AlterColumn<short>(
                name: "UnitType",
                table: "WMS_Product",
                type: "smallint",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text");

            migrationBuilder.AddColumn<Guid>(
                name: "AssignBy",
                table: "WMS_BranchRequest",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<decimal>(
                name: "ExtraCost",
                table: "WMS_BranchRequest",
                type: "money",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<short>(
                name: "UnitType",
                table: "WMS_BranchProductReturnDetail",
                type: "smallint",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.AddColumn<decimal>(
                name: "CostPrice",
                table: "WMS_BranchProductRequest",
                type: "money",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AlterColumn<int>(
                name: "Amount",
                table: "WMS_BranchInvoice",
                type: "integer",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "numeric");

            migrationBuilder.AddColumn<short>(
                name: "UnitType",
                table: "WMS_BranchImportProduct",
                type: "smallint",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.AlterColumn<DateTime>(
                name: "ImportedDate",
                table: "WMS_BranchImport",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "ImportBy",
                table: "WMS_BranchImport",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                oldClrType: typeof(Guid),
                oldType: "uuid",
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "AssignBy",
                table: "WMS_BranchImport",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                oldClrType: typeof(Guid),
                oldType: "uuid",
                oldNullable: true);

            migrationBuilder.AddColumn<short>(
                name: "UnitType",
                table: "WMS_BranchExportProduct",
                type: "smallint",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.AddColumn<short>(
                name: "UnitType",
                table: "WMS_BaseProductReturnDetail",
                type: "smallint",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.AlterColumn<int>(
                name: "Amount",
                table: "WMS_BaseInvoice",
                type: "integer",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "numeric");

            migrationBuilder.AddColumn<short>(
                name: "UnitType",
                table: "WMS_BaseImportProduct",
                type: "smallint",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.AddColumn<short>(
                name: "UnitType",
                table: "WMS_BaseExportProduct",
                type: "smallint",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.AlterColumn<DateTime>(
                name: "ExportDate",
                table: "WMS_BaseExport",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "ExportBy",
                table: "WMS_BaseExport",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                oldClrType: typeof(Guid),
                oldType: "uuid",
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "AssignBy",
                table: "WMS_BaseExport",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                oldClrType: typeof(Guid),
                oldType: "uuid",
                oldNullable: true);

            migrationBuilder.AddColumn<short>(
                name: "ApprovalStatus",
                table: "WMS_BaseExport",
                type: "smallint",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.CreateTable(
                name: "WMS_BaseWarehouseProduct",
                columns: table => new
                {
                    ProductId = table.Column<Guid>(type: "uuid", nullable: false),
                    WarehouseId = table.Column<Guid>(type: "uuid", nullable: false),
                    BpId = table.Column<Guid>(type: "uuid", nullable: false),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: true, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WMS_BaseWarehouseProduct", x => new { x.ProductId, x.WarehouseId });
                    table.ForeignKey(
                        name: "FK_WMS_BaseWarehouseProduct_WMS_BaseWarehouse_WarehouseId",
                        column: x => x.WarehouseId,
                        principalTable: "WMS_BaseWarehouse",
                        principalColumn: "WarehouseId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WMS_BaseWarehouseProduct_WMS_Product_ProductId",
                        column: x => x.ProductId,
                        principalTable: "WMS_Product",
                        principalColumn: "ProductId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchRequest_AssignBy",
                table: "WMS_BranchRequest",
                column: "AssignBy");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BaseWarehouseProduct_WarehouseId",
                table: "WMS_BaseWarehouseProduct",
                column: "WarehouseId");

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BaseExport_WMS_User_AssignBy",
                table: "WMS_BaseExport",
                column: "AssignBy",
                principalTable: "WMS_User",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BaseExport_WMS_User_ExportBy",
                table: "WMS_BaseExport",
                column: "ExportBy",
                principalTable: "WMS_User",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BranchImport_WMS_User_AssignBy",
                table: "WMS_BranchImport",
                column: "AssignBy",
                principalTable: "WMS_User",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BranchImport_WMS_User_ImportBy",
                table: "WMS_BranchImport",
                column: "ImportBy",
                principalTable: "WMS_User",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BranchRequest_WMS_User_AssignBy",
                table: "WMS_BranchRequest",
                column: "AssignBy",
                principalTable: "WMS_User",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
