﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WMS.Domain.Migrations
{
    public partial class AddImportExportRef : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BaseStockCheckSheet_WMS_BaseImport_ImportId",
                table: "WMS_BaseStockCheckSheet");

            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BranchInvoice_WMS_BranchDebt_DebtId",
                table: "WMS_BranchInvoice");

            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BranchStockCheckSheet_WMS_BranchImport_ImportId",
                table: "WMS_BranchStockCheckSheet");

            migrationBuilder.DropIndex(
                name: "IX_WMS_BranchStockCheckSheet_ImportId",
                table: "WMS_BranchStockCheckSheet");

            migrationBuilder.DropPrimaryKey(
                name: "PK_WMS_BranchInvoice",
                table: "WMS_BranchInvoice");

            migrationBuilder.DropPrimaryKey(
                name: "PK_WMS_BranchExportProduct",
                table: "WMS_BranchExportProduct");

            migrationBuilder.DropIndex(
                name: "IX_WMS_BaseStockCheckSheet_ImportId",
                table: "WMS_BaseStockCheckSheet");

            migrationBuilder.DropPrimaryKey(
                name: "PK_WMS_BaseInvoice",
                table: "WMS_BaseInvoice");

            migrationBuilder.DropColumn(
                name: "ImportId",
                table: "WMS_BranchStockCheckSheet");

            migrationBuilder.DropColumn(
                name: "UnitType",
                table: "WMS_BranchProductRequest");

            migrationBuilder.DropColumn(
                name: "PaidAmount",
                table: "WMS_BranchExport");

            migrationBuilder.DropColumn(
                name: "PaymentStatus",
                table: "WMS_BranchExport");

            migrationBuilder.DropColumn(
                name: "ShippingStatus",
                table: "WMS_BranchExport");

            migrationBuilder.DropColumn(
                name: "TotalCost",
                table: "WMS_BranchExport");

            migrationBuilder.DropColumn(
                name: "ImportId",
                table: "WMS_BaseStockCheckSheet");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_Token",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(2023, 11, 8, 18, 52, 56, 23, DateTimeKind.Utc).AddTicks(7449),
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldDefaultValue: new DateTime(2023, 11, 3, 7, 10, 43, 241, DateTimeKind.Utc).AddTicks(2952));

            migrationBuilder.AddColumn<Guid>(
                name: "ImportId",
                table: "WMS_BranchStockCheckSheetDetail",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "BaseExportId",
                table: "WMS_BranchProductReturn",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ImportId",
                table: "WMS_BranchProductReturn",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AlterColumn<Guid>(
                name: "DebtId",
                table: "WMS_BranchInvoice",
                type: "uuid",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AddColumn<Guid>(
                name: "ImportId",
                table: "WMS_BranchExportProduct",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "ImportId",
                table: "WMS_BaseStockCheckSheetDetail",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "BranchProductReturnId",
                table: "WMS_BaseInvoice",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AlterColumn<DateTime>(
                name: "ImportedDate",
                table: "WMS_BaseImport",
                type: "timestamp without time zone",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone");

            migrationBuilder.AddColumn<DateTime>(
                name: "RequestDate",
                table: "WMS_BaseImport",
                type: "timestamp without time zone",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_WMS_BranchInvoice",
                table: "WMS_BranchInvoice",
                column: "InvoiceId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_WMS_BranchExportProduct",
                table: "WMS_BranchExportProduct",
                columns: new[] { "ExportId", "ProductId", "ImportId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_WMS_BaseInvoice",
                table: "WMS_BaseInvoice",
                column: "InvoiceId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchStockCheckSheetDetail_ImportId",
                table: "WMS_BranchStockCheckSheetDetail",
                column: "ImportId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchProductReturn_BaseExportId",
                table: "WMS_BranchProductReturn",
                column: "BaseExportId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchProductReturn_ImportId",
                table: "WMS_BranchProductReturn",
                column: "ImportId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchExportProduct_ImportId",
                table: "WMS_BranchExportProduct",
                column: "ImportId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BaseStockCheckSheetDetail_ImportId",
                table: "WMS_BaseStockCheckSheetDetail",
                column: "ImportId");

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BaseInvoice_WMS_BranchProductReturn_ReturnId",
                table: "WMS_BaseInvoice",
                column: "ReturnId",
                principalTable: "WMS_BranchProductReturn",
                principalColumn: "ProductReturnId");

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BaseStockCheckSheetDetail_WMS_BaseImport_ImportId",
                table: "WMS_BaseStockCheckSheetDetail",
                column: "ImportId",
                principalTable: "WMS_BaseImport",
                principalColumn: "ImportId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BranchExportProduct_WMS_BranchImport_ImportId",
                table: "WMS_BranchExportProduct",
                column: "ImportId",
                principalTable: "WMS_BranchImport",
                principalColumn: "ImportId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BranchInvoice_WMS_BranchDebt_DebtId",
                table: "WMS_BranchInvoice",
                column: "DebtId",
                principalTable: "WMS_BranchDebt",
                principalColumn: "DebtId");

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BranchProductReturn_WMS_BaseExport_BaseExportId",
                table: "WMS_BranchProductReturn",
                column: "BaseExportId",
                principalTable: "WMS_BaseExport",
                principalColumn: "ExportId");

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BranchProductReturn_WMS_BranchImport_ImportId",
                table: "WMS_BranchProductReturn",
                column: "ImportId",
                principalTable: "WMS_BranchImport",
                principalColumn: "ImportId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BranchStockCheckSheetDetail_WMS_BranchImport_ImportId",
                table: "WMS_BranchStockCheckSheetDetail",
                column: "ImportId",
                principalTable: "WMS_BranchImport",
                principalColumn: "ImportId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BaseInvoice_WMS_BranchProductReturn_ReturnId",
                table: "WMS_BaseInvoice");

            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BaseStockCheckSheetDetail_WMS_BaseImport_ImportId",
                table: "WMS_BaseStockCheckSheetDetail");

            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BranchExportProduct_WMS_BranchImport_ImportId",
                table: "WMS_BranchExportProduct");

            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BranchInvoice_WMS_BranchDebt_DebtId",
                table: "WMS_BranchInvoice");

            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BranchProductReturn_WMS_BaseExport_BaseExportId",
                table: "WMS_BranchProductReturn");

            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BranchProductReturn_WMS_BranchImport_ImportId",
                table: "WMS_BranchProductReturn");

            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BranchStockCheckSheetDetail_WMS_BranchImport_ImportId",
                table: "WMS_BranchStockCheckSheetDetail");

            migrationBuilder.DropIndex(
                name: "IX_WMS_BranchStockCheckSheetDetail_ImportId",
                table: "WMS_BranchStockCheckSheetDetail");

            migrationBuilder.DropIndex(
                name: "IX_WMS_BranchProductReturn_BaseExportId",
                table: "WMS_BranchProductReturn");

            migrationBuilder.DropIndex(
                name: "IX_WMS_BranchProductReturn_ImportId",
                table: "WMS_BranchProductReturn");

            migrationBuilder.DropPrimaryKey(
                name: "PK_WMS_BranchInvoice",
                table: "WMS_BranchInvoice");

            migrationBuilder.DropPrimaryKey(
                name: "PK_WMS_BranchExportProduct",
                table: "WMS_BranchExportProduct");

            migrationBuilder.DropIndex(
                name: "IX_WMS_BranchExportProduct_ImportId",
                table: "WMS_BranchExportProduct");

            migrationBuilder.DropIndex(
                name: "IX_WMS_BaseStockCheckSheetDetail_ImportId",
                table: "WMS_BaseStockCheckSheetDetail");

            migrationBuilder.DropPrimaryKey(
                name: "PK_WMS_BaseInvoice",
                table: "WMS_BaseInvoice");

            migrationBuilder.DropColumn(
                name: "ImportId",
                table: "WMS_BranchStockCheckSheetDetail");

            migrationBuilder.DropColumn(
                name: "BaseExportId",
                table: "WMS_BranchProductReturn");

            migrationBuilder.DropColumn(
                name: "ImportId",
                table: "WMS_BranchProductReturn");

            migrationBuilder.DropColumn(
                name: "ImportId",
                table: "WMS_BranchExportProduct");

            migrationBuilder.DropColumn(
                name: "ImportId",
                table: "WMS_BaseStockCheckSheetDetail");

            migrationBuilder.DropColumn(
                name: "BranchProductReturnId",
                table: "WMS_BaseInvoice");

            migrationBuilder.DropColumn(
                name: "RequestDate",
                table: "WMS_BaseImport");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_Token",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(2023, 11, 3, 7, 10, 43, 241, DateTimeKind.Utc).AddTicks(2952),
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldDefaultValue: new DateTime(2023, 11, 8, 18, 52, 56, 23, DateTimeKind.Utc).AddTicks(7449));

            migrationBuilder.AddColumn<Guid>(
                name: "ImportId",
                table: "WMS_BranchStockCheckSheet",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<decimal>(
                name: "UnitType",
                table: "WMS_BranchProductRequest",
                type: "numeric",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AlterColumn<Guid>(
                name: "DebtId",
                table: "WMS_BranchInvoice",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                oldClrType: typeof(Guid),
                oldType: "uuid",
                oldNullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "PaidAmount",
                table: "WMS_BranchExport",
                type: "money",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<short>(
                name: "PaymentStatus",
                table: "WMS_BranchExport",
                type: "smallint",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.AddColumn<short>(
                name: "ShippingStatus",
                table: "WMS_BranchExport",
                type: "smallint",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.AddColumn<decimal>(
                name: "TotalCost",
                table: "WMS_BranchExport",
                type: "money",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<Guid>(
                name: "ImportId",
                table: "WMS_BaseStockCheckSheet",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AlterColumn<DateTime>(
                name: "ImportedDate",
                table: "WMS_BaseImport",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_WMS_BranchInvoice",
                table: "WMS_BranchInvoice",
                columns: new[] { "InvoiceId", "DebtId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_WMS_BranchExportProduct",
                table: "WMS_BranchExportProduct",
                columns: new[] { "ExportId", "ProductId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_WMS_BaseInvoice",
                table: "WMS_BaseInvoice",
                columns: new[] { "InvoiceId", "DebtId" });

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchStockCheckSheet_ImportId",
                table: "WMS_BranchStockCheckSheet",
                column: "ImportId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BaseStockCheckSheet_ImportId",
                table: "WMS_BaseStockCheckSheet",
                column: "ImportId");

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BaseStockCheckSheet_WMS_BaseImport_ImportId",
                table: "WMS_BaseStockCheckSheet",
                column: "ImportId",
                principalTable: "WMS_BaseImport",
                principalColumn: "ImportId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BranchInvoice_WMS_BranchDebt_DebtId",
                table: "WMS_BranchInvoice",
                column: "DebtId",
                principalTable: "WMS_BranchDebt",
                principalColumn: "DebtId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BranchStockCheckSheet_WMS_BranchImport_ImportId",
                table: "WMS_BranchStockCheckSheet",
                column: "ImportId",
                principalTable: "WMS_BranchImport",
                principalColumn: "ImportId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
