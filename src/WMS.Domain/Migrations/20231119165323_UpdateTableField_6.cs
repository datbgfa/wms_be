﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WMS.Domain.Migrations
{
    public partial class UpdateTableField_6 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_WMS_BranchStockCheckSheetDetail",
                table: "WMS_BranchStockCheckSheetDetail");

            migrationBuilder.DropPrimaryKey(
                name: "PK_WMS_BaseStockCheckSheetDetail",
                table: "WMS_BaseStockCheckSheetDetail");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_Token",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(2023, 11, 19, 16, 53, 22, 734, DateTimeKind.Utc).AddTicks(4050),
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldDefaultValue: new DateTime(2023, 11, 16, 19, 2, 58, 324, DateTimeKind.Utc).AddTicks(5244));

            migrationBuilder.AddColumn<Guid>(
                name: "BaseImportId",
                table: "WMS_BranchStockCheckSheetDetail",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<string>(
                name: "StockCheckSheetCode",
                table: "WMS_BranchStockCheckSheet",
                type: "character varying(50)",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "BranchQuantity",
                table: "WMS_BaseStockCheckSheetDetail",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "RealQuantity",
                table: "WMS_BaseStockCheckSheetDetail",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "StockCheckSheetCode",
                table: "WMS_BaseStockCheckSheet",
                type: "character varying(50)",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddPrimaryKey(
                name: "PK_WMS_BranchStockCheckSheetDetail",
                table: "WMS_BranchStockCheckSheetDetail",
                columns: new[] { "CheckId", "ProductId", "ImportId", "BaseImportId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_WMS_BaseStockCheckSheetDetail",
                table: "WMS_BaseStockCheckSheetDetail",
                columns: new[] { "CheckId", "ProductId", "ImportId" });

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchStockCheckSheetDetail_BaseImportId",
                table: "WMS_BranchStockCheckSheetDetail",
                column: "BaseImportId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchStockCheckSheet_StockCheckSheetCode",
                table: "WMS_BranchStockCheckSheet",
                column: "StockCheckSheetCode",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BaseStockCheckSheet_StockCheckSheetCode",
                table: "WMS_BaseStockCheckSheet",
                column: "StockCheckSheetCode",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BranchStockCheckSheetDetail_WMS_BaseImport_BaseImportId",
                table: "WMS_BranchStockCheckSheetDetail",
                column: "BaseImportId",
                principalTable: "WMS_BaseImport",
                principalColumn: "ImportId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BranchStockCheckSheetDetail_WMS_BaseImport_BaseImportId",
                table: "WMS_BranchStockCheckSheetDetail");

            migrationBuilder.DropPrimaryKey(
                name: "PK_WMS_BranchStockCheckSheetDetail",
                table: "WMS_BranchStockCheckSheetDetail");

            migrationBuilder.DropIndex(
                name: "IX_WMS_BranchStockCheckSheetDetail_BaseImportId",
                table: "WMS_BranchStockCheckSheetDetail");

            migrationBuilder.DropIndex(
                name: "IX_WMS_BranchStockCheckSheet_StockCheckSheetCode",
                table: "WMS_BranchStockCheckSheet");

            migrationBuilder.DropPrimaryKey(
                name: "PK_WMS_BaseStockCheckSheetDetail",
                table: "WMS_BaseStockCheckSheetDetail");

            migrationBuilder.DropIndex(
                name: "IX_WMS_BaseStockCheckSheet_StockCheckSheetCode",
                table: "WMS_BaseStockCheckSheet");

            migrationBuilder.DropColumn(
                name: "BaseImportId",
                table: "WMS_BranchStockCheckSheetDetail");

            migrationBuilder.DropColumn(
                name: "StockCheckSheetCode",
                table: "WMS_BranchStockCheckSheet");

            migrationBuilder.DropColumn(
                name: "BranchQuantity",
                table: "WMS_BaseStockCheckSheetDetail");

            migrationBuilder.DropColumn(
                name: "RealQuantity",
                table: "WMS_BaseStockCheckSheetDetail");

            migrationBuilder.DropColumn(
                name: "StockCheckSheetCode",
                table: "WMS_BaseStockCheckSheet");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_Token",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(2023, 11, 16, 19, 2, 58, 324, DateTimeKind.Utc).AddTicks(5244),
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldDefaultValue: new DateTime(2023, 11, 19, 16, 53, 22, 734, DateTimeKind.Utc).AddTicks(4050));

            migrationBuilder.AddPrimaryKey(
                name: "PK_WMS_BranchStockCheckSheetDetail",
                table: "WMS_BranchStockCheckSheetDetail",
                columns: new[] { "CheckId", "ProductId", "ImportId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_WMS_BaseStockCheckSheetDetail",
                table: "WMS_BaseStockCheckSheetDetail",
                columns: new[] { "CheckId", "ProductId" });
        }
    }
}
