﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WMS.Domain.Migrations
{
    public partial class TokenCurrentAccessToken : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Token",
                table: "WMS_Token",
                type: "character varying(100)",
                maxLength: 100,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_Token",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(2023, 11, 2, 8, 40, 22, 714, DateTimeKind.Utc).AddTicks(9412),
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldDefaultValue: new DateTime(2023, 10, 26, 21, 28, 24, 89, DateTimeKind.Utc).AddTicks(6229));

            migrationBuilder.AddColumn<string>(
                name: "CurrentAccessToken",
                table: "WMS_Token",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_Token_UserId",
                table: "WMS_Token",
                column: "UserId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_WMS_Token_UserId",
                table: "WMS_Token");

            migrationBuilder.DropColumn(
                name: "CurrentAccessToken",
                table: "WMS_Token");

            migrationBuilder.AlterColumn<string>(
                name: "Token",
                table: "WMS_Token",
                type: "text",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "character varying(100)",
                oldMaxLength: 100);

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_Token",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(2023, 10, 26, 21, 28, 24, 89, DateTimeKind.Utc).AddTicks(6229),
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldDefaultValue: new DateTime(2023, 11, 2, 8, 40, 22, 714, DateTimeKind.Utc).AddTicks(9412));
        }
    }
}
