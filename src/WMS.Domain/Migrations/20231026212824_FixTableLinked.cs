﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WMS.Domain.Migrations
{
    public partial class FixTableLinked : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BaseDebt_WMS_Supplier_CreditorSupplierId",
                table: "WMS_BaseDebt");

            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BaseImport_WMS_Supplier_WmsSupplierSupplierId",
                table: "WMS_BaseImport");

            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BranchImport_WMS_Supplier_SupplierId",
                table: "WMS_BranchImport");

            migrationBuilder.DropIndex(
                name: "IX_WMS_BranchImport_SupplierId",
                table: "WMS_BranchImport");

            migrationBuilder.DropPrimaryKey(
                name: "PK_WMS_BaseImport",
                table: "WMS_BaseImport");

            migrationBuilder.DropIndex(
                name: "IX_WMS_BaseImport_WmsSupplierSupplierId",
                table: "WMS_BaseImport");

            migrationBuilder.DropIndex(
                name: "IX_WMS_BaseDebt_CreditorSupplierId",
                table: "WMS_BaseDebt");

            migrationBuilder.DropColumn(
                name: "SupplierId",
                table: "WMS_BranchImport");

            migrationBuilder.DropColumn(
                name: "RequestId",
                table: "WMS_BaseImport");

            migrationBuilder.DropColumn(
                name: "WmsSupplierSupplierId",
                table: "WMS_BaseImport");

            migrationBuilder.DropColumn(
                name: "CreditorSupplierId",
                table: "WMS_BaseDebt");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_Token",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(2023, 10, 26, 21, 28, 24, 89, DateTimeKind.Utc).AddTicks(6229),
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldDefaultValue: new DateTime(2023, 10, 24, 9, 47, 38, 403, DateTimeKind.Utc).AddTicks(6434));

            migrationBuilder.AddColumn<Guid>(
                name: "BaseWarehouseId",
                table: "WMS_BranchImport",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<short>(
                name: "ImportStatus",
                table: "WMS_BranchImport",
                type: "smallint",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.AddColumn<string>(
                name: "Note",
                table: "WMS_BranchImport",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "PaymentStatus",
                table: "WMS_BranchImport",
                type: "smallint",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.AddColumn<Guid>(
                name: "ImportId",
                table: "WMS_BaseProductReturn",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<short>(
                name: "ImportStatus",
                table: "WMS_BaseImport",
                type: "smallint",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.AddColumn<string>(
                name: "Note",
                table: "WMS_BaseImport",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "PaymentStatus",
                table: "WMS_BaseImport",
                type: "smallint",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.AddColumn<Guid>(
                name: "BranchWarehouseId",
                table: "WMS_BaseExport",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.DropColumn(
                name: "CreditorId",
                table: "WMS_BaseDebt");

            migrationBuilder.AddColumn<int>(
                name: "CreditorId",
                table: "WMS_BaseDebt",
                type: "integer",
                nullable: false);

            migrationBuilder.AddPrimaryKey(
                name: "PK_WMS_BaseImport",
                table: "WMS_BaseImport",
                columns: new[] { "ImportId", "WarehouseId" });

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchImport_BaseWarehouseId",
                table: "WMS_BranchImport",
                column: "BaseWarehouseId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BaseProductReturn_ImportId",
                table: "WMS_BaseProductReturn",
                column: "ImportId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BaseImport_SupplierId",
                table: "WMS_BaseImport",
                column: "SupplierId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BaseExport_BranchWarehouseId",
                table: "WMS_BaseExport",
                column: "BranchWarehouseId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BaseDebt_CreditorId",
                table: "WMS_BaseDebt",
                column: "CreditorId");

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BaseDebt_WMS_Supplier_CreditorId",
                table: "WMS_BaseDebt",
                column: "CreditorId",
                principalTable: "WMS_Supplier",
                principalColumn: "SupplierId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BaseExport_WMS_BranchWarehouse_BranchWarehouseId",
                table: "WMS_BaseExport",
                column: "BranchWarehouseId",
                principalTable: "WMS_BranchWarehouse",
                principalColumn: "WarehouseId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BaseImport_WMS_Supplier_SupplierId",
                table: "WMS_BaseImport",
                column: "SupplierId",
                principalTable: "WMS_Supplier",
                principalColumn: "SupplierId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BaseProductReturn_WMS_BaseImport_ImportId",
                table: "WMS_BaseProductReturn",
                column: "ImportId",
                principalTable: "WMS_BaseImport",
                principalColumn: "ImportId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BranchImport_WMS_BaseWarehouse_BaseWarehouseId",
                table: "WMS_BranchImport",
                column: "BaseWarehouseId",
                principalTable: "WMS_BaseWarehouse",
                principalColumn: "WarehouseId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BaseDebt_WMS_Supplier_CreditorId",
                table: "WMS_BaseDebt");

            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BaseExport_WMS_BranchWarehouse_BranchWarehouseId",
                table: "WMS_BaseExport");

            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BaseImport_WMS_Supplier_SupplierId",
                table: "WMS_BaseImport");

            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BaseProductReturn_WMS_BaseImport_ImportId",
                table: "WMS_BaseProductReturn");

            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BranchImport_WMS_BaseWarehouse_BaseWarehouseId",
                table: "WMS_BranchImport");

            migrationBuilder.DropIndex(
                name: "IX_WMS_BranchImport_BaseWarehouseId",
                table: "WMS_BranchImport");

            migrationBuilder.DropIndex(
                name: "IX_WMS_BaseProductReturn_ImportId",
                table: "WMS_BaseProductReturn");

            migrationBuilder.DropPrimaryKey(
                name: "PK_WMS_BaseImport",
                table: "WMS_BaseImport");

            migrationBuilder.DropIndex(
                name: "IX_WMS_BaseImport_SupplierId",
                table: "WMS_BaseImport");

            migrationBuilder.DropIndex(
                name: "IX_WMS_BaseExport_BranchWarehouseId",
                table: "WMS_BaseExport");

            migrationBuilder.DropIndex(
                name: "IX_WMS_BaseDebt_CreditorId",
                table: "WMS_BaseDebt");

            migrationBuilder.DropColumn(
                name: "BaseWarehouseId",
                table: "WMS_BranchImport");

            migrationBuilder.DropColumn(
                name: "ImportStatus",
                table: "WMS_BranchImport");

            migrationBuilder.DropColumn(
                name: "Note",
                table: "WMS_BranchImport");

            migrationBuilder.DropColumn(
                name: "PaymentStatus",
                table: "WMS_BranchImport");

            migrationBuilder.DropColumn(
                name: "ImportId",
                table: "WMS_BaseProductReturn");

            migrationBuilder.DropColumn(
                name: "ImportStatus",
                table: "WMS_BaseImport");

            migrationBuilder.DropColumn(
                name: "Note",
                table: "WMS_BaseImport");

            migrationBuilder.DropColumn(
                name: "PaymentStatus",
                table: "WMS_BaseImport");

            migrationBuilder.DropColumn(
                name: "BranchWarehouseId",
                table: "WMS_BaseExport");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_Token",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(2023, 10, 24, 9, 47, 38, 403, DateTimeKind.Utc).AddTicks(6434),
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldDefaultValue: new DateTime(2023, 10, 26, 21, 28, 24, 89, DateTimeKind.Utc).AddTicks(6229));

            migrationBuilder.AddColumn<int>(
                name: "SupplierId",
                table: "WMS_BranchImport",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<Guid>(
                name: "RequestId",
                table: "WMS_BaseImport",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<int>(
                name: "WmsSupplierSupplierId",
                table: "WMS_BaseImport",
                type: "integer",
                nullable: true);

            migrationBuilder.DropColumn(
                name: "CreditorId",
                table: "WMS_BaseDebt");

            migrationBuilder.AddColumn<int>(
                name: "CreditorId",
                table: "WMS_BaseDebt",
                type: "integer",
                nullable: false);

            migrationBuilder.AddColumn<int>(
                name: "CreditorSupplierId",
                table: "WMS_BaseDebt",
                type: "integer",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_WMS_BaseImport",
                table: "WMS_BaseImport",
                columns: new[] { "ImportId", "WarehouseId", "RequestId" });

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchImport_SupplierId",
                table: "WMS_BranchImport",
                column: "SupplierId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BaseImport_WmsSupplierSupplierId",
                table: "WMS_BaseImport",
                column: "WmsSupplierSupplierId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BaseDebt_CreditorSupplierId",
                table: "WMS_BaseDebt",
                column: "CreditorSupplierId");

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BaseDebt_WMS_Supplier_CreditorSupplierId",
                table: "WMS_BaseDebt",
                column: "CreditorSupplierId",
                principalTable: "WMS_Supplier",
                principalColumn: "SupplierId");

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BaseImport_WMS_Supplier_WmsSupplierSupplierId",
                table: "WMS_BaseImport",
                column: "WmsSupplierSupplierId",
                principalTable: "WMS_Supplier",
                principalColumn: "SupplierId");

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BranchImport_WMS_Supplier_SupplierId",
                table: "WMS_BranchImport",
                column: "SupplierId",
                principalTable: "WMS_Supplier",
                principalColumn: "SupplierId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
