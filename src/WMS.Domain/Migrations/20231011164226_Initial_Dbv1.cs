﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace WMS.Domain.Migrations
{
    public partial class Initial_Dbv1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "WMS_Action",
                columns: table => new
                {
                    ActionId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ActionName = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    ActionDescription = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: false),
                    CreateUser = table.Column<string>(type: "text", nullable: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    UpdateUser = table.Column<string>(type: "text", nullable: false),
                    UpdateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WMS_Action", x => x.ActionId);
                });

            migrationBuilder.CreateTable(
                name: "WMS_Role",
                columns: table => new
                {
                    RoleId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    RoleName = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    RoleDescription = table.Column<string>(type: "character varying(500)", maxLength: 500, nullable: true),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: false),
                    CreateUser = table.Column<string>(type: "text", nullable: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    UpdateUser = table.Column<string>(type: "text", nullable: false),
                    UpdateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WMS_Role", x => x.RoleId);
                });

            migrationBuilder.CreateTable(
                name: "WMS_User",
                columns: table => new
                {
                    UserId = table.Column<Guid>(type: "uuid", nullable: false),
                    Username = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    HashedPassword = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    SaltKey = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: false),
                    Fullname = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    Gender = table.Column<bool>(type: "boolean", nullable: false),
                    Email = table.Column<string>(type: "character varying(320)", maxLength: 320, nullable: false),
                    Address = table.Column<string>(type: "character varying(15)", maxLength: 15, nullable: false),
                    PhoneNumber = table.Column<string>(type: "text", nullable: false),
                    Birthdate = table.Column<DateTime>(type: "date", nullable: false),
                    Status = table.Column<short>(type: "smallint", nullable: false),
                    Supervisor = table.Column<Guid>(type: "uuid", nullable: true),
                    Avatar = table.Column<string>(type: "character varying(1000)", maxLength: 1000, nullable: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    JoinDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WMS_User", x => x.UserId);
                    table.ForeignKey(
                        name: "FK_WMS_User_WMS_User_Supervisor",
                        column: x => x.Supervisor,
                        principalTable: "WMS_User",
                        principalColumn: "UserId");
                });

            migrationBuilder.CreateTable(
                name: "WMS_RoleAction",
                columns: table => new
                {
                    RoleId = table.Column<int>(type: "integer", nullable: false),
                    ActionId = table.Column<int>(type: "integer", maxLength: 30, nullable: false),
                    RoleActionId = table.Column<int>(type: "integer", nullable: false),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WMS_RoleAction", x => new { x.RoleId, x.ActionId });
                    table.ForeignKey(
                        name: "FK_WMS_RoleAction_WMS_Action_ActionId",
                        column: x => x.ActionId,
                        principalTable: "WMS_Action",
                        principalColumn: "ActionId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WMS_RoleAction_WMS_Role_RoleId",
                        column: x => x.RoleId,
                        principalTable: "WMS_Role",
                        principalColumn: "RoleId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WMS_Token",
                columns: table => new
                {
                    UserId = table.Column<Guid>(type: "uuid", nullable: false),
                    Token = table.Column<string>(type: "text", nullable: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false, defaultValue: new DateTime(2023, 10, 11, 16, 42, 26, 207, DateTimeKind.Utc).AddTicks(2302)),
                    ExpriedDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WMS_Token", x => x.UserId);
                    table.ForeignKey(
                        name: "FK_WMS_Token_WMS_User_UserId",
                        column: x => x.UserId,
                        principalTable: "WMS_User",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WMS_UserRole",
                columns: table => new
                {
                    RoleId = table.Column<int>(type: "integer", nullable: false),
                    UserId = table.Column<Guid>(type: "uuid", nullable: false),
                    UserRoleId = table.Column<Guid>(type: "uuid", nullable: false),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WMS_UserRole", x => new { x.RoleId, x.UserId });
                    table.ForeignKey(
                        name: "FK_WMS_UserRole_WMS_Role_RoleId",
                        column: x => x.RoleId,
                        principalTable: "WMS_Role",
                        principalColumn: "RoleId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WMS_UserRole_WMS_User_UserId",
                        column: x => x.UserId,
                        principalTable: "WMS_User",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WMS_RoleAction_ActionId",
                table: "WMS_RoleAction",
                column: "ActionId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_User_Supervisor",
                table: "WMS_User",
                column: "Supervisor");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_User_Username",
                table: "WMS_User",
                column: "Username",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_WMS_UserRole_UserId",
                table: "WMS_UserRole",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WMS_RoleAction");

            migrationBuilder.DropTable(
                name: "WMS_Token");

            migrationBuilder.DropTable(
                name: "WMS_UserRole");

            migrationBuilder.DropTable(
                name: "WMS_Action");

            migrationBuilder.DropTable(
                name: "WMS_Role");

            migrationBuilder.DropTable(
                name: "WMS_User");
        }
    }
}
