﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WMS.Domain.Migrations
{
    public partial class UpdateTableField_3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_Token",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(2023, 11, 15, 9, 21, 23, 735, DateTimeKind.Utc).AddTicks(2790),
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldDefaultValue: new DateTime(2023, 11, 15, 9, 14, 35, 455, DateTimeKind.Utc).AddTicks(2705));

            migrationBuilder.AddColumn<string>(
                name: "ReturnCode",
                table: "WMS_BranchProductReturn",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "ReturnCode",
                table: "WMS_BaseProductReturn",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchProductReturn_ReturnCode",
                table: "WMS_BranchProductReturn",
                column: "ReturnCode",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BaseProductReturn_ReturnCode",
                table: "WMS_BaseProductReturn",
                column: "ReturnCode",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_WMS_BranchProductReturn_ReturnCode",
                table: "WMS_BranchProductReturn");

            migrationBuilder.DropIndex(
                name: "IX_WMS_BaseProductReturn_ReturnCode",
                table: "WMS_BaseProductReturn");

            migrationBuilder.DropColumn(
                name: "ReturnCode",
                table: "WMS_BranchProductReturn");

            migrationBuilder.DropColumn(
                name: "ReturnCode",
                table: "WMS_BaseProductReturn");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_Token",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(2023, 11, 15, 9, 14, 35, 455, DateTimeKind.Utc).AddTicks(2705),
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldDefaultValue: new DateTime(2023, 11, 15, 9, 21, 23, 735, DateTimeKind.Utc).AddTicks(2790));
        }
    }
}
