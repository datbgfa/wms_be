﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WMS.Domain.Migrations
{
    public partial class AddWarehouseUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Quantiy",
                table: "WMS_BranchExportProduct",
                newName: "Quantity");

            migrationBuilder.RenameColumn(
                name: "Quantiy",
                table: "WMS_BaseExportProduct",
                newName: "Quantity");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_Token",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(2023, 10, 24, 9, 47, 38, 403, DateTimeKind.Utc).AddTicks(6434),
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldDefaultValue: new DateTime(2023, 10, 19, 8, 18, 13, 215, DateTimeKind.Utc).AddTicks(7716));

            migrationBuilder.CreateTable(
                name: "WMS_BaseWarehouseUser",
                columns: table => new
                {
                    UserId = table.Column<Guid>(type: "uuid", nullable: false),
                    BaseWarehouseId = table.Column<Guid>(type: "uuid", nullable: false),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WMS_BaseWarehouseUser", x => new { x.UserId, x.BaseWarehouseId });
                    table.ForeignKey(
                        name: "FK_WMS_BaseWarehouseUser_WMS_BaseWarehouse_BaseWarehouseId",
                        column: x => x.BaseWarehouseId,
                        principalTable: "WMS_BaseWarehouse",
                        principalColumn: "WarehouseId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WMS_BaseWarehouseUser_WMS_User_UserId",
                        column: x => x.UserId,
                        principalTable: "WMS_User",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WMS_BranchWarehouseUser",
                columns: table => new
                {
                    UserId = table.Column<Guid>(type: "uuid", nullable: false),
                    BranchWarehouseId = table.Column<Guid>(type: "uuid", nullable: false),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WMS_BranchWarehouseUser", x => new { x.UserId, x.BranchWarehouseId });
                    table.ForeignKey(
                        name: "FK_WMS_BranchWarehouseUser_WMS_BranchWarehouse_BranchWarehouse~",
                        column: x => x.BranchWarehouseId,
                        principalTable: "WMS_BranchWarehouse",
                        principalColumn: "WarehouseId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WMS_BranchWarehouseUser_WMS_User_UserId",
                        column: x => x.UserId,
                        principalTable: "WMS_User",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BaseWarehouseUser_BaseWarehouseId",
                table: "WMS_BaseWarehouseUser",
                column: "BaseWarehouseId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchWarehouseUser_BranchWarehouseId",
                table: "WMS_BranchWarehouseUser",
                column: "BranchWarehouseId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WMS_BaseWarehouseUser");

            migrationBuilder.DropTable(
                name: "WMS_BranchWarehouseUser");

            migrationBuilder.RenameColumn(
                name: "Quantity",
                table: "WMS_BranchExportProduct",
                newName: "Quantiy");

            migrationBuilder.RenameColumn(
                name: "Quantity",
                table: "WMS_BaseExportProduct",
                newName: "Quantiy");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_Token",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(2023, 10, 19, 8, 18, 13, 215, DateTimeKind.Utc).AddTicks(7716),
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldDefaultValue: new DateTime(2023, 10, 24, 9, 47, 38, 403, DateTimeKind.Utc).AddTicks(6434));
        }
    }
}
