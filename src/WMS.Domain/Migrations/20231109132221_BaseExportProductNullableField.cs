﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WMS.Domain.Migrations
{
    public partial class BaseExportProductNullableField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BaseExportProduct_WMS_BaseImport_BaseImportId",
                table: "WMS_BaseExportProduct");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_Token",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(2023, 11, 9, 13, 22, 21, 325, DateTimeKind.Utc).AddTicks(3431),
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldDefaultValue: new DateTime(2023, 11, 9, 8, 42, 3, 427, DateTimeKind.Utc).AddTicks(4793));

            migrationBuilder.AlterColumn<Guid>(
                name: "BaseImportId",
                table: "WMS_BaseExportProduct",
                type: "uuid",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BaseExportProduct_WMS_BaseImport_BaseImportId",
                table: "WMS_BaseExportProduct",
                column: "BaseImportId",
                principalTable: "WMS_BaseImport",
                principalColumn: "ImportId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BaseExportProduct_WMS_BaseImport_BaseImportId",
                table: "WMS_BaseExportProduct");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_Token",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(2023, 11, 9, 8, 42, 3, 427, DateTimeKind.Utc).AddTicks(4793),
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldDefaultValue: new DateTime(2023, 11, 9, 13, 22, 21, 325, DateTimeKind.Utc).AddTicks(3431));

            migrationBuilder.AlterColumn<Guid>(
                name: "BaseImportId",
                table: "WMS_BaseExportProduct",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                oldClrType: typeof(Guid),
                oldType: "uuid",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BaseExportProduct_WMS_BaseImport_BaseImportId",
                table: "WMS_BaseExportProduct",
                column: "BaseImportId",
                principalTable: "WMS_BaseImport",
                principalColumn: "ImportId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
