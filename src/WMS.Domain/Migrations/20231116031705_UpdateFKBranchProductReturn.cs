﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WMS.Domain.Migrations
{
    public partial class UpdateFKBranchProductReturn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BaseInvoice_WMS_BranchProductReturn_ReturnId",
                table: "WMS_BaseInvoice");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_Token",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(2023, 11, 16, 3, 17, 4, 944, DateTimeKind.Utc).AddTicks(9967),
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldDefaultValue: new DateTime(2023, 11, 15, 9, 21, 23, 735, DateTimeKind.Utc).AddTicks(2790));

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BaseInvoice_BranchProductReturnId",
                table: "WMS_BaseInvoice",
                column: "BranchProductReturnId");

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BaseInvoice_WMS_BranchProductReturn_BranchProductReturn~",
                table: "WMS_BaseInvoice",
                column: "BranchProductReturnId",
                principalTable: "WMS_BranchProductReturn",
                principalColumn: "ProductReturnId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BaseInvoice_WMS_BranchProductReturn_BranchProductReturn~",
                table: "WMS_BaseInvoice");

            migrationBuilder.DropIndex(
                name: "IX_WMS_BaseInvoice_BranchProductReturnId",
                table: "WMS_BaseInvoice");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_Token",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(2023, 11, 15, 9, 21, 23, 735, DateTimeKind.Utc).AddTicks(2790),
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldDefaultValue: new DateTime(2023, 11, 16, 3, 17, 4, 944, DateTimeKind.Utc).AddTicks(9967));

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BaseInvoice_WMS_BranchProductReturn_ReturnId",
                table: "WMS_BaseInvoice",
                column: "ReturnId",
                principalTable: "WMS_BranchProductReturn",
                principalColumn: "ProductReturnId");
        }
    }
}
