﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace WMS.Domain.Migrations
{
    public partial class AddBranchTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "IsDelete",
                table: "WMS_UserRole",
                type: "boolean",
                nullable: true,
                defaultValue: false,
                oldClrType: typeof(bool),
                oldType: "boolean",
                oldDefaultValue: false);

            migrationBuilder.AlterColumn<DateTime>(
                name: "JoinDate",
                table: "WMS_User",
                type: "timestamp without time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_User",
                type: "timestamp without time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone");

            migrationBuilder.AlterColumn<DateTime>(
                name: "ExpriedDate",
                table: "WMS_Token",
                type: "timestamp without time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_Token",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(2023, 10, 17, 20, 45, 56, 974, DateTimeKind.Utc).AddTicks(4881),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2023, 10, 14, 18, 32, 36, 25, DateTimeKind.Utc).AddTicks(5818));

            migrationBuilder.AlterColumn<bool>(
                name: "IsDelete",
                table: "WMS_RoleAction",
                type: "boolean",
                nullable: true,
                defaultValue: false,
                oldClrType: typeof(bool),
                oldType: "boolean",
                oldDefaultValue: false);

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdateDate",
                table: "WMS_Role",
                type: "timestamp without time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone");

            migrationBuilder.AlterColumn<bool>(
                name: "IsDelete",
                table: "WMS_Role",
                type: "boolean",
                nullable: true,
                oldClrType: typeof(bool),
                oldType: "boolean");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_Role",
                type: "timestamp without time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone");

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdateDate",
                table: "WMS_Action",
                type: "timestamp without time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone");

            migrationBuilder.AlterColumn<bool>(
                name: "IsDelete",
                table: "WMS_Action",
                type: "boolean",
                nullable: true,
                oldClrType: typeof(bool),
                oldType: "boolean");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_Action",
                type: "timestamp without time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone");

            migrationBuilder.CreateTable(
                name: "WMS_Attribute",
                columns: table => new
                {
                    AttributeId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    AttributeName = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    Visible = table.Column<bool>(type: "boolean", nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WMS_Attribute", x => x.AttributeId);
                });

            migrationBuilder.CreateTable(
                name: "WMS_BaseWarehouse",
                columns: table => new
                {
                    WarehouseId = table.Column<Guid>(type: "uuid", nullable: false),
                    WarehouseOwner = table.Column<Guid>(type: "uuid", nullable: true),
                    WarehouseName = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    PhoneNumber = table.Column<string>(type: "character varying(15)", unicode: false, maxLength: 15, nullable: false),
                    Email = table.Column<string>(type: "character varying(320)", unicode: false, maxLength: 320, nullable: false),
                    Address = table.Column<string>(type: "text", nullable: false),
                    Status = table.Column<short>(type: "smallint", nullable: false),
                    Logo = table.Column<string>(type: "character varying(1000)", maxLength: 1000, nullable: false),
                    WmsUserUserId = table.Column<Guid>(type: "uuid", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WMS_BaseWarehouse", x => x.WarehouseId);
                    table.ForeignKey(
                        name: "FK_WMS_BaseWarehouse_WMS_User_WmsUserUserId",
                        column: x => x.WmsUserUserId,
                        principalTable: "WMS_User",
                        principalColumn: "UserId");
                });

            migrationBuilder.CreateTable(
                name: "WMS_BranchProductReturn",
                columns: table => new
                {
                    ProductReturnId = table.Column<Guid>(type: "uuid", nullable: false),
                    Description = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    CreateAt = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Status = table.Column<short>(type: "smallint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WMS_BranchProductReturn", x => x.ProductReturnId);
                });

            migrationBuilder.CreateTable(
                name: "WMS_BranchWarehouse",
                columns: table => new
                {
                    WarehouseId = table.Column<Guid>(type: "uuid", nullable: false),
                    WarehouseOwner = table.Column<Guid>(type: "uuid", nullable: true),
                    WarehouseName = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    PhoneNumber = table.Column<string>(type: "character varying(15)", unicode: false, maxLength: 15, nullable: false),
                    Email = table.Column<string>(type: "character varying(320)", unicode: false, maxLength: 320, nullable: false),
                    Address = table.Column<string>(type: "text", nullable: false),
                    Status = table.Column<short>(type: "smallint", nullable: false),
                    Logo = table.Column<string>(type: "character varying(1000)", maxLength: 1000, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WMS_BranchWarehouse", x => x.WarehouseId);
                    table.ForeignKey(
                        name: "FK_WMS_BranchWarehouse_WMS_User_WarehouseOwner",
                        column: x => x.WarehouseOwner,
                        principalTable: "WMS_User",
                        principalColumn: "UserId");
                });

            migrationBuilder.CreateTable(
                name: "WMS_Category",
                columns: table => new
                {
                    CategoryId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CategoryName = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    Visible = table.Column<bool>(type: "boolean", nullable: true, defaultValue: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WMS_Category", x => x.CategoryId);
                });

            migrationBuilder.CreateTable(
                name: "WMS_Supplier",
                columns: table => new
                {
                    SupplierId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    SupplierName = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: false),
                    Address = table.Column<string>(type: "character varying(1000)", maxLength: 1000, nullable: false),
                    PhoneNumber = table.Column<string>(type: "character varying(15)", maxLength: 15, nullable: false),
                    Email = table.Column<string>(type: "character varying(320)", maxLength: 320, nullable: false),
                    OtherInformation = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    Logo = table.Column<string>(type: "character varying(1000)", maxLength: 1000, nullable: false),
                    Status = table.Column<bool>(type: "boolean", nullable: false),
                    CooperatedTime = table.Column<DateTime>(type: "date", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WMS_Supplier", x => x.SupplierId);
                });

            migrationBuilder.CreateTable(
                name: "WMS_BranchDebt",
                columns: table => new
                {
                    DebtId = table.Column<Guid>(type: "uuid", nullable: false),
                    CreditorId = table.Column<Guid>(type: "uuid", nullable: false),
                    DebtorId = table.Column<Guid>(type: "uuid", nullable: false),
                    DebtAmount = table.Column<decimal>(type: "money", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WMS_BranchDebt", x => x.DebtId);
                    table.ForeignKey(
                        name: "FK_WMS_BranchDebt_WMS_BaseWarehouse_CreditorId",
                        column: x => x.CreditorId,
                        principalTable: "WMS_BaseWarehouse",
                        principalColumn: "WarehouseId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WMS_BranchDebt_WMS_BranchWarehouse_DebtorId",
                        column: x => x.DebtorId,
                        principalTable: "WMS_BranchWarehouse",
                        principalColumn: "WarehouseId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WMS_BranchExport",
                columns: table => new
                {
                    ExportId = table.Column<Guid>(type: "uuid", nullable: false),
                    WarehouseId = table.Column<Guid>(type: "uuid", nullable: false),
                    AssignBy = table.Column<Guid>(type: "uuid", nullable: false),
                    ExportBy = table.Column<Guid>(type: "uuid", nullable: false),
                    ExportDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    TotalPrice = table.Column<decimal>(type: "money", nullable: false),
                    PaidAmount = table.Column<decimal>(type: "money", nullable: false),
                    ShippingStatus = table.Column<short>(type: "smallint", nullable: false),
                    PaymentStatus = table.Column<short>(type: "smallint", nullable: false),
                    ApprovalStatus = table.Column<short>(type: "smallint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WMS_BranchExport", x => new { x.ExportId, x.WarehouseId });
                    table.UniqueConstraint("AK_WMS_BranchExport_ExportId", x => x.ExportId);
                    table.ForeignKey(
                        name: "FK_WMS_BranchExport_WMS_BranchWarehouse_WarehouseId",
                        column: x => x.WarehouseId,
                        principalTable: "WMS_BranchWarehouse",
                        principalColumn: "WarehouseId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WMS_BranchExport_WMS_User_AssignBy",
                        column: x => x.AssignBy,
                        principalTable: "WMS_User",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WMS_BranchExport_WMS_User_ExportBy",
                        column: x => x.ExportBy,
                        principalTable: "WMS_User",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WMS_BranchRequest",
                columns: table => new
                {
                    RequestId = table.Column<Guid>(type: "uuid", nullable: false),
                    WarehouseId = table.Column<Guid>(type: "uuid", nullable: false),
                    AssignBy = table.Column<Guid>(type: "uuid", nullable: false),
                    RequestBy = table.Column<Guid>(type: "uuid", nullable: false),
                    RequestDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    TotalPrice = table.Column<decimal>(type: "money", nullable: false),
                    ExtraCost = table.Column<decimal>(type: "money", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WMS_BranchRequest", x => new { x.RequestId, x.WarehouseId });
                    table.UniqueConstraint("AK_WMS_BranchRequest_RequestId", x => x.RequestId);
                    table.ForeignKey(
                        name: "FK_WMS_BranchRequest_WMS_BranchWarehouse_WarehouseId",
                        column: x => x.WarehouseId,
                        principalTable: "WMS_BranchWarehouse",
                        principalColumn: "WarehouseId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WMS_BranchRequest_WMS_User_AssignBy",
                        column: x => x.AssignBy,
                        principalTable: "WMS_User",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WMS_BranchRequest_WMS_User_RequestBy",
                        column: x => x.RequestBy,
                        principalTable: "WMS_User",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WMS_CategoryAttribute",
                columns: table => new
                {
                    CategoryId = table.Column<int>(type: "integer", nullable: false),
                    AttributeId = table.Column<int>(type: "integer", nullable: false),
                    CaId = table.Column<Guid>(type: "uuid", nullable: false),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: true, defaultValue: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WMS_CategoryAttribute", x => new { x.CategoryId, x.AttributeId });
                    table.ForeignKey(
                        name: "FK_WMS_CategoryAttribute_WMS_Attribute_AttributeId",
                        column: x => x.AttributeId,
                        principalTable: "WMS_Attribute",
                        principalColumn: "AttributeId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WMS_CategoryAttribute_WMS_Category_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "WMS_Category",
                        principalColumn: "CategoryId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WMS_Product",
                columns: table => new
                {
                    ProductId = table.Column<Guid>(type: "uuid", nullable: false),
                    CategoryId = table.Column<int>(type: "integer", nullable: false),
                    SKU = table.Column<string>(type: "character varying(255)", unicode: false, maxLength: 255, nullable: false),
                    ProductName = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    ProductDescription = table.Column<string>(type: "character varying(500)", maxLength: 500, nullable: true),
                    ProductImage = table.Column<string>(type: "character varying(1000)", maxLength: 1000, nullable: false),
                    Visible = table.Column<bool>(type: "boolean", nullable: false, defaultValue: true),
                    UnitPrice = table.Column<decimal>(type: "money", nullable: false),
                    SalePrice = table.Column<decimal>(type: "money", nullable: false),
                    ExportPrice = table.Column<decimal>(type: "money", nullable: false),
                    UnitType = table.Column<short>(type: "smallint", nullable: false),
                    Status = table.Column<short>(type: "smallint", nullable: false),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: true),
                    CreateUser = table.Column<string>(type: "text", nullable: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdateUser = table.Column<string>(type: "text", nullable: false),
                    UpdateDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WMS_Product", x => x.ProductId);
                    table.ForeignKey(
                        name: "FK_WMS_Product_WMS_Category_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "WMS_Category",
                        principalColumn: "CategoryId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WMS_BranchInvoice",
                columns: table => new
                {
                    InvoiceId = table.Column<Guid>(type: "uuid", nullable: false),
                    DebtId = table.Column<Guid>(type: "uuid", nullable: false),
                    CreatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Type = table.Column<short>(type: "smallint", nullable: false),
                    Amount = table.Column<int>(type: "integer", nullable: false),
                    Description = table.Column<string>(type: "character varying(1000)", maxLength: 1000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WMS_BranchInvoice", x => new { x.InvoiceId, x.DebtId });
                    table.ForeignKey(
                        name: "FK_WMS_BranchInvoice_WMS_BranchDebt_DebtId",
                        column: x => x.DebtId,
                        principalTable: "WMS_BranchDebt",
                        principalColumn: "DebtId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WMS_BranchImport",
                columns: table => new
                {
                    ImportId = table.Column<Guid>(type: "uuid", nullable: false),
                    WarehouseId = table.Column<Guid>(type: "uuid", nullable: false),
                    RequestId = table.Column<Guid>(type: "uuid", nullable: false),
                    AssignBy = table.Column<Guid>(type: "uuid", nullable: false),
                    ImportBy = table.Column<Guid>(type: "uuid", nullable: false),
                    SupplierId = table.Column<int>(type: "integer", nullable: false),
                    ImportedDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    TotalPrice = table.Column<decimal>(type: "money", nullable: false),
                    ExtraCost = table.Column<decimal>(type: "money", nullable: false),
                    PaidAmount = table.Column<decimal>(type: "money", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WMS_BranchImport", x => new { x.ImportId, x.WarehouseId, x.RequestId });
                    table.UniqueConstraint("AK_WMS_BranchImport_ImportId", x => x.ImportId);
                    table.ForeignKey(
                        name: "FK_WMS_BranchImport_WMS_BranchRequest_RequestId",
                        column: x => x.RequestId,
                        principalTable: "WMS_BranchRequest",
                        principalColumn: "RequestId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WMS_BranchImport_WMS_BranchWarehouse_WarehouseId",
                        column: x => x.WarehouseId,
                        principalTable: "WMS_BranchWarehouse",
                        principalColumn: "WarehouseId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WMS_BranchImport_WMS_Supplier_SupplierId",
                        column: x => x.SupplierId,
                        principalTable: "WMS_Supplier",
                        principalColumn: "SupplierId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WMS_BranchImport_WMS_User_AssignBy",
                        column: x => x.AssignBy,
                        principalTable: "WMS_User",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WMS_BranchImport_WMS_User_ImportBy",
                        column: x => x.ImportBy,
                        principalTable: "WMS_User",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WMS_BranchExportProduct",
                columns: table => new
                {
                    ExportId = table.Column<Guid>(type: "uuid", nullable: false),
                    ProductId = table.Column<Guid>(type: "uuid", nullable: false),
                    UnitPrice = table.Column<decimal>(type: "money", nullable: false),
                    UnitType = table.Column<short>(type: "smallint", nullable: false),
                    Quantiy = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WMS_BranchExportProduct", x => new { x.ExportId, x.ProductId });
                    table.ForeignKey(
                        name: "FK_WMS_BranchExportProduct_WMS_BranchExport_ExportId",
                        column: x => x.ExportId,
                        principalTable: "WMS_BranchExport",
                        principalColumn: "ExportId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WMS_BranchExportProduct_WMS_Product_ProductId",
                        column: x => x.ProductId,
                        principalTable: "WMS_Product",
                        principalColumn: "ProductId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WMS_BranchProductRequest",
                columns: table => new
                {
                    RequestId = table.Column<Guid>(type: "uuid", nullable: false),
                    ProductId = table.Column<Guid>(type: "uuid", nullable: false),
                    CostPrice = table.Column<decimal>(type: "money", nullable: false),
                    UnitPrice = table.Column<decimal>(type: "money", nullable: false),
                    UnitType = table.Column<decimal>(type: "numeric", nullable: false),
                    RequestQuantity = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WMS_BranchProductRequest", x => new { x.RequestId, x.ProductId });
                    table.ForeignKey(
                        name: "FK_WMS_BranchProductRequest_WMS_BranchRequest_RequestId",
                        column: x => x.RequestId,
                        principalTable: "WMS_BranchRequest",
                        principalColumn: "RequestId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WMS_BranchProductRequest_WMS_Product_ProductId",
                        column: x => x.ProductId,
                        principalTable: "WMS_Product",
                        principalColumn: "ProductId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WMS_ProductAttribute",
                columns: table => new
                {
                    ProductId = table.Column<Guid>(type: "uuid", nullable: false),
                    AttributeId = table.Column<int>(type: "integer", nullable: false),
                    PavId = table.Column<Guid>(type: "uuid", nullable: false),
                    Value = table.Column<string>(type: "character varying(500)", maxLength: 500, nullable: false),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: true, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WMS_ProductAttribute", x => new { x.ProductId, x.AttributeId });
                    table.ForeignKey(
                        name: "FK_WMS_ProductAttribute_WMS_Attribute_AttributeId",
                        column: x => x.AttributeId,
                        principalTable: "WMS_Attribute",
                        principalColumn: "AttributeId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WMS_ProductAttribute_WMS_Product_ProductId",
                        column: x => x.ProductId,
                        principalTable: "WMS_Product",
                        principalColumn: "ProductId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WMS_ProductSupplier",
                columns: table => new
                {
                    ProductId = table.Column<Guid>(type: "uuid", nullable: false),
                    SupplierId = table.Column<int>(type: "integer", nullable: false),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: true, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WMS_ProductSupplier", x => new { x.ProductId, x.SupplierId });
                    table.ForeignKey(
                        name: "FK_WMS_ProductSupplier_WMS_Product_ProductId",
                        column: x => x.ProductId,
                        principalTable: "WMS_Product",
                        principalColumn: "ProductId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WMS_ProductSupplier_WMS_Supplier_SupplierId",
                        column: x => x.SupplierId,
                        principalTable: "WMS_Supplier",
                        principalColumn: "SupplierId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WMS_BranchImportProduct",
                columns: table => new
                {
                    ImportId = table.Column<Guid>(type: "uuid", nullable: false),
                    ProductId = table.Column<Guid>(type: "uuid", nullable: false),
                    CostPrice = table.Column<decimal>(type: "money", nullable: false),
                    UnitPrice = table.Column<decimal>(type: "money", nullable: false),
                    UnitType = table.Column<short>(type: "smallint", nullable: false),
                    ImportedQuantity = table.Column<int>(type: "integer", nullable: false),
                    StockQuantity = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WMS_BranchImportProduct", x => new { x.ImportId, x.ProductId });
                    table.ForeignKey(
                        name: "FK_WMS_BranchImportProduct_WMS_BranchImport_ImportId",
                        column: x => x.ImportId,
                        principalTable: "WMS_BranchImport",
                        principalColumn: "ImportId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WMS_BranchImportProduct_WMS_Product_ProductId",
                        column: x => x.ProductId,
                        principalTable: "WMS_Product",
                        principalColumn: "ProductId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WMS_BranchProductReturnDetail",
                columns: table => new
                {
                    ProductReturnId = table.Column<Guid>(type: "uuid", nullable: false),
                    ImportId = table.Column<Guid>(type: "uuid", nullable: false),
                    ProductId = table.Column<Guid>(type: "uuid", nullable: false),
                    BprdId = table.Column<Guid>(type: "uuid", nullable: false),
                    CostPrice = table.Column<decimal>(type: "money", nullable: false),
                    UnitPrice = table.Column<decimal>(type: "money", nullable: false),
                    UnitType = table.Column<short>(type: "smallint", nullable: false),
                    RequestQuantity = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WMS_BranchProductReturnDetail", x => new { x.ProductReturnId, x.ImportId, x.ProductId });
                    table.ForeignKey(
                        name: "FK_WMS_BranchProductReturnDetail_WMS_BranchImport_ImportId",
                        column: x => x.ImportId,
                        principalTable: "WMS_BranchImport",
                        principalColumn: "ImportId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WMS_BranchProductReturnDetail_WMS_BranchProductReturn_Produ~",
                        column: x => x.ProductReturnId,
                        principalTable: "WMS_BranchProductReturn",
                        principalColumn: "ProductReturnId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WMS_BranchProductReturnDetail_WMS_Product_ProductId",
                        column: x => x.ProductId,
                        principalTable: "WMS_Product",
                        principalColumn: "ProductId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WMS_BranchStockCheckSheet",
                columns: table => new
                {
                    CheckId = table.Column<Guid>(type: "uuid", nullable: false),
                    CheckBy = table.Column<Guid>(type: "uuid", nullable: false),
                    ImportId = table.Column<Guid>(type: "uuid", nullable: false),
                    CheckDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Description = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    Status = table.Column<short>(type: "smallint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WMS_BranchStockCheckSheet", x => x.CheckId);
                    table.ForeignKey(
                        name: "FK_WMS_BranchStockCheckSheet_WMS_BranchImport_ImportId",
                        column: x => x.ImportId,
                        principalTable: "WMS_BranchImport",
                        principalColumn: "ImportId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WMS_BranchStockCheckSheet_WMS_User_CheckBy",
                        column: x => x.CheckBy,
                        principalTable: "WMS_User",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WMS_BranchStockCheckSheetDetail",
                columns: table => new
                {
                    CheckId = table.Column<Guid>(type: "uuid", nullable: false),
                    ProductId = table.Column<Guid>(type: "uuid", nullable: false),
                    Quantity = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WMS_BranchStockCheckSheetDetail", x => new { x.CheckId, x.ProductId });
                    table.ForeignKey(
                        name: "FK_WMS_BranchStockCheckSheetDetail_WMS_BranchStockCheckSheet_C~",
                        column: x => x.CheckId,
                        principalTable: "WMS_BranchStockCheckSheet",
                        principalColumn: "CheckId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WMS_BranchStockCheckSheetDetail_WMS_Product_ProductId",
                        column: x => x.ProductId,
                        principalTable: "WMS_Product",
                        principalColumn: "ProductId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WMS_User_Email",
                table: "WMS_User",
                column: "Email",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_WMS_User_Username_Email",
                table: "WMS_User",
                columns: new[] { "Username", "Email" });

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BaseWarehouse_WmsUserUserId",
                table: "WMS_BaseWarehouse",
                column: "WmsUserUserId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchDebt_CreditorId",
                table: "WMS_BranchDebt",
                column: "CreditorId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchDebt_DebtorId",
                table: "WMS_BranchDebt",
                column: "DebtorId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchExport_AssignBy",
                table: "WMS_BranchExport",
                column: "AssignBy");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchExport_ExportBy",
                table: "WMS_BranchExport",
                column: "ExportBy");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchExport_WarehouseId",
                table: "WMS_BranchExport",
                column: "WarehouseId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchExportProduct_ProductId",
                table: "WMS_BranchExportProduct",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchImport_AssignBy",
                table: "WMS_BranchImport",
                column: "AssignBy");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchImport_ImportBy",
                table: "WMS_BranchImport",
                column: "ImportBy");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchImport_RequestId",
                table: "WMS_BranchImport",
                column: "RequestId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchImport_SupplierId",
                table: "WMS_BranchImport",
                column: "SupplierId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchImport_WarehouseId",
                table: "WMS_BranchImport",
                column: "WarehouseId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchImportProduct_ProductId",
                table: "WMS_BranchImportProduct",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchInvoice_DebtId",
                table: "WMS_BranchInvoice",
                column: "DebtId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchProductRequest_ProductId",
                table: "WMS_BranchProductRequest",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchProductReturnDetail_ImportId",
                table: "WMS_BranchProductReturnDetail",
                column: "ImportId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchProductReturnDetail_ProductId",
                table: "WMS_BranchProductReturnDetail",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchRequest_AssignBy",
                table: "WMS_BranchRequest",
                column: "AssignBy");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchRequest_RequestBy",
                table: "WMS_BranchRequest",
                column: "RequestBy");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchRequest_WarehouseId",
                table: "WMS_BranchRequest",
                column: "WarehouseId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchStockCheckSheet_CheckBy",
                table: "WMS_BranchStockCheckSheet",
                column: "CheckBy");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchStockCheckSheet_ImportId",
                table: "WMS_BranchStockCheckSheet",
                column: "ImportId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchStockCheckSheetDetail_ProductId",
                table: "WMS_BranchStockCheckSheetDetail",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchWarehouse_WarehouseOwner",
                table: "WMS_BranchWarehouse",
                column: "WarehouseOwner");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_CategoryAttribute_AttributeId",
                table: "WMS_CategoryAttribute",
                column: "AttributeId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_Product_CategoryId",
                table: "WMS_Product",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_ProductAttribute_AttributeId",
                table: "WMS_ProductAttribute",
                column: "AttributeId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_ProductSupplier_SupplierId",
                table: "WMS_ProductSupplier",
                column: "SupplierId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WMS_BranchExportProduct");

            migrationBuilder.DropTable(
                name: "WMS_BranchImportProduct");

            migrationBuilder.DropTable(
                name: "WMS_BranchInvoice");

            migrationBuilder.DropTable(
                name: "WMS_BranchProductRequest");

            migrationBuilder.DropTable(
                name: "WMS_BranchProductReturnDetail");

            migrationBuilder.DropTable(
                name: "WMS_BranchStockCheckSheetDetail");

            migrationBuilder.DropTable(
                name: "WMS_CategoryAttribute");

            migrationBuilder.DropTable(
                name: "WMS_ProductAttribute");

            migrationBuilder.DropTable(
                name: "WMS_ProductSupplier");

            migrationBuilder.DropTable(
                name: "WMS_BranchExport");

            migrationBuilder.DropTable(
                name: "WMS_BranchDebt");

            migrationBuilder.DropTable(
                name: "WMS_BranchProductReturn");

            migrationBuilder.DropTable(
                name: "WMS_BranchStockCheckSheet");

            migrationBuilder.DropTable(
                name: "WMS_Attribute");

            migrationBuilder.DropTable(
                name: "WMS_Product");

            migrationBuilder.DropTable(
                name: "WMS_BaseWarehouse");

            migrationBuilder.DropTable(
                name: "WMS_BranchImport");

            migrationBuilder.DropTable(
                name: "WMS_Category");

            migrationBuilder.DropTable(
                name: "WMS_BranchRequest");

            migrationBuilder.DropTable(
                name: "WMS_Supplier");

            migrationBuilder.DropTable(
                name: "WMS_BranchWarehouse");

            migrationBuilder.DropIndex(
                name: "IX_WMS_User_Email",
                table: "WMS_User");

            migrationBuilder.DropIndex(
                name: "IX_WMS_User_Username_Email",
                table: "WMS_User");

            migrationBuilder.AlterColumn<bool>(
                name: "IsDelete",
                table: "WMS_UserRole",
                type: "boolean",
                nullable: false,
                defaultValue: false,
                oldClrType: typeof(bool),
                oldType: "boolean",
                oldNullable: true,
                oldDefaultValue: false);

            migrationBuilder.AlterColumn<DateTime>(
                name: "JoinDate",
                table: "WMS_User",
                type: "timestamp with time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_User",
                type: "timestamp with time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone");

            migrationBuilder.AlterColumn<DateTime>(
                name: "ExpriedDate",
                table: "WMS_Token",
                type: "timestamp with time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_Token",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2023, 10, 14, 18, 32, 36, 25, DateTimeKind.Utc).AddTicks(5818),
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldDefaultValue: new DateTime(2023, 10, 17, 20, 45, 56, 974, DateTimeKind.Utc).AddTicks(4881));

            migrationBuilder.AlterColumn<bool>(
                name: "IsDelete",
                table: "WMS_RoleAction",
                type: "boolean",
                nullable: false,
                defaultValue: false,
                oldClrType: typeof(bool),
                oldType: "boolean",
                oldNullable: true,
                oldDefaultValue: false);

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdateDate",
                table: "WMS_Role",
                type: "timestamp with time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone");

            migrationBuilder.AlterColumn<bool>(
                name: "IsDelete",
                table: "WMS_Role",
                type: "boolean",
                nullable: false,
                defaultValue: false,
                oldClrType: typeof(bool),
                oldType: "boolean",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_Role",
                type: "timestamp with time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone");

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdateDate",
                table: "WMS_Action",
                type: "timestamp with time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone");

            migrationBuilder.AlterColumn<bool>(
                name: "IsDelete",
                table: "WMS_Action",
                type: "boolean",
                nullable: false,
                defaultValue: false,
                oldClrType: typeof(bool),
                oldType: "boolean",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_Action",
                type: "timestamp with time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone");
        }
    }
}
