﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WMS.Domain.Migrations
{
    public partial class UpdateTableField_1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BaseExportProduct_WMS_BaseImport_BaseImportId",
                table: "WMS_BaseExportProduct");

            migrationBuilder.DropPrimaryKey(
                name: "PK_WMS_BranchStockCheckSheetDetail",
                table: "WMS_BranchStockCheckSheetDetail");

            migrationBuilder.DropPrimaryKey(
                name: "PK_WMS_BranchImportProduct",
                table: "WMS_BranchImportProduct");

            migrationBuilder.DropPrimaryKey(
                name: "PK_WMS_BaseExportProduct",
                table: "WMS_BaseExportProduct");

            migrationBuilder.RenameColumn(
                name: "SalePrice",
                table: "WMS_Product",
                newName: "CostPrice");

            migrationBuilder.RenameColumn(
                name: "Quantity",
                table: "WMS_BranchStockCheckSheetDetail",
                newName: "RealQuantity");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_Token",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(2023, 11, 14, 9, 35, 39, 246, DateTimeKind.Utc).AddTicks(950),
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldDefaultValue: new DateTime(2023, 11, 9, 13, 22, 21, 325, DateTimeKind.Utc).AddTicks(3431));

            migrationBuilder.AddColumn<int>(
                name: "BranchQuantity",
                table: "WMS_BranchStockCheckSheetDetail",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "RequestCode",
                table: "WMS_BranchRequest",
                type: "character varying(50)",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "ImportCode",
                table: "WMS_BranchImport",
                type: "character varying(50)",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "ExportCode",
                table: "WMS_BranchExport",
                type: "character varying(50)",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<Guid>(
                name: "BranchProductReturnId",
                table: "WMS_BaseInvoice",
                type: "uuid",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AddColumn<string>(
                name: "ImportCode",
                table: "WMS_BaseImport",
                type: "character varying(50)",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<Guid>(
                name: "BaseImportId",
                table: "WMS_BaseExportProduct",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                oldClrType: typeof(Guid),
                oldType: "uuid",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ExportCode",
                table: "WMS_BaseExport",
                type: "character varying(50)",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddPrimaryKey(
                name: "PK_WMS_BranchStockCheckSheetDetail",
                table: "WMS_BranchStockCheckSheetDetail",
                columns: new[] { "CheckId", "ProductId", "ImportId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_WMS_BranchImportProduct",
                table: "WMS_BranchImportProduct",
                columns: new[] { "ImportId", "ProductId", "BaseImportId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_WMS_BaseExportProduct",
                table: "WMS_BaseExportProduct",
                columns: new[] { "ExportId", "ProductId", "BaseImportId" });

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchWarehouseProduct_BpId",
                table: "WMS_BranchWarehouseProduct",
                column: "BpId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchRequest_RequestCode",
                table: "WMS_BranchRequest",
                column: "RequestCode",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchProductReturnDetail_BprdId",
                table: "WMS_BranchProductReturnDetail",
                column: "BprdId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchImport_ImportCode",
                table: "WMS_BranchImport",
                column: "ImportCode",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchExport_ExportCode",
                table: "WMS_BranchExport",
                column: "ExportCode",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BaseProductReturnDetail_BprdId",
                table: "WMS_BaseProductReturnDetail",
                column: "BprdId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BaseImport_ImportCode",
                table: "WMS_BaseImport",
                column: "ImportCode",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BaseExport_ExportCode",
                table: "WMS_BaseExport",
                column: "ExportCode",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BaseExportProduct_WMS_BaseImport_BaseImportId",
                table: "WMS_BaseExportProduct",
                column: "BaseImportId",
                principalTable: "WMS_BaseImport",
                principalColumn: "ImportId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BaseExportProduct_WMS_BaseImport_BaseImportId",
                table: "WMS_BaseExportProduct");

            migrationBuilder.DropIndex(
                name: "IX_WMS_BranchWarehouseProduct_BpId",
                table: "WMS_BranchWarehouseProduct");

            migrationBuilder.DropPrimaryKey(
                name: "PK_WMS_BranchStockCheckSheetDetail",
                table: "WMS_BranchStockCheckSheetDetail");

            migrationBuilder.DropIndex(
                name: "IX_WMS_BranchRequest_RequestCode",
                table: "WMS_BranchRequest");

            migrationBuilder.DropIndex(
                name: "IX_WMS_BranchProductReturnDetail_BprdId",
                table: "WMS_BranchProductReturnDetail");

            migrationBuilder.DropPrimaryKey(
                name: "PK_WMS_BranchImportProduct",
                table: "WMS_BranchImportProduct");

            migrationBuilder.DropIndex(
                name: "IX_WMS_BranchImport_ImportCode",
                table: "WMS_BranchImport");

            migrationBuilder.DropIndex(
                name: "IX_WMS_BranchExport_ExportCode",
                table: "WMS_BranchExport");

            migrationBuilder.DropIndex(
                name: "IX_WMS_BaseProductReturnDetail_BprdId",
                table: "WMS_BaseProductReturnDetail");

            migrationBuilder.DropIndex(
                name: "IX_WMS_BaseImport_ImportCode",
                table: "WMS_BaseImport");

            migrationBuilder.DropPrimaryKey(
                name: "PK_WMS_BaseExportProduct",
                table: "WMS_BaseExportProduct");

            migrationBuilder.DropIndex(
                name: "IX_WMS_BaseExport_ExportCode",
                table: "WMS_BaseExport");

            migrationBuilder.DropColumn(
                name: "BranchQuantity",
                table: "WMS_BranchStockCheckSheetDetail");

            migrationBuilder.DropColumn(
                name: "RequestCode",
                table: "WMS_BranchRequest");

            migrationBuilder.DropColumn(
                name: "ImportCode",
                table: "WMS_BranchImport");

            migrationBuilder.DropColumn(
                name: "ExportCode",
                table: "WMS_BranchExport");

            migrationBuilder.DropColumn(
                name: "ImportCode",
                table: "WMS_BaseImport");

            migrationBuilder.DropColumn(
                name: "ExportCode",
                table: "WMS_BaseExport");

            migrationBuilder.RenameColumn(
                name: "CostPrice",
                table: "WMS_Product",
                newName: "SalePrice");

            migrationBuilder.RenameColumn(
                name: "RealQuantity",
                table: "WMS_BranchStockCheckSheetDetail",
                newName: "Quantity");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_Token",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(2023, 11, 9, 13, 22, 21, 325, DateTimeKind.Utc).AddTicks(3431),
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldDefaultValue: new DateTime(2023, 11, 14, 9, 35, 39, 246, DateTimeKind.Utc).AddTicks(950));

            migrationBuilder.AlterColumn<Guid>(
                name: "BranchProductReturnId",
                table: "WMS_BaseInvoice",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                oldClrType: typeof(Guid),
                oldType: "uuid",
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "BaseImportId",
                table: "WMS_BaseExportProduct",
                type: "uuid",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AddPrimaryKey(
                name: "PK_WMS_BranchStockCheckSheetDetail",
                table: "WMS_BranchStockCheckSheetDetail",
                columns: new[] { "CheckId", "ProductId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_WMS_BranchImportProduct",
                table: "WMS_BranchImportProduct",
                columns: new[] { "ImportId", "ProductId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_WMS_BaseExportProduct",
                table: "WMS_BaseExportProduct",
                columns: new[] { "ExportId", "ProductId" });

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BaseExportProduct_WMS_BaseImport_BaseImportId",
                table: "WMS_BaseExportProduct",
                column: "BaseImportId",
                principalTable: "WMS_BaseImport",
                principalColumn: "ImportId");
        }
    }
}
