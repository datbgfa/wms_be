﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WMS.Domain.Migrations
{
    public partial class FieldNoteInBranchWarehouse : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BranchWarehouse_WMS_User_WarehouseOwner",
                table: "WMS_BranchWarehouse");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_Token",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(2023, 10, 19, 8, 18, 13, 215, DateTimeKind.Utc).AddTicks(7716),
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldDefaultValue: new DateTime(2023, 10, 18, 20, 47, 27, 935, DateTimeKind.Utc).AddTicks(6324));

            migrationBuilder.AlterColumn<Guid>(
                name: "WarehouseOwner",
                table: "WMS_BranchWarehouse",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                oldClrType: typeof(Guid),
                oldType: "uuid",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Note",
                table: "WMS_BranchWarehouse",
                type: "character varying(1000)",
                maxLength: 1000,
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BranchWarehouse_WMS_User_WarehouseOwner",
                table: "WMS_BranchWarehouse",
                column: "WarehouseOwner",
                principalTable: "WMS_User",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BranchWarehouse_WMS_User_WarehouseOwner",
                table: "WMS_BranchWarehouse");

            migrationBuilder.DropColumn(
                name: "Note",
                table: "WMS_BranchWarehouse");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_Token",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(2023, 10, 18, 20, 47, 27, 935, DateTimeKind.Utc).AddTicks(6324),
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldDefaultValue: new DateTime(2023, 10, 19, 8, 18, 13, 215, DateTimeKind.Utc).AddTicks(7716));

            migrationBuilder.AlterColumn<Guid>(
                name: "WarehouseOwner",
                table: "WMS_BranchWarehouse",
                type: "uuid",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BranchWarehouse_WMS_User_WarehouseOwner",
                table: "WMS_BranchWarehouse",
                column: "WarehouseOwner",
                principalTable: "WMS_User",
                principalColumn: "UserId");
        }
    }
}
