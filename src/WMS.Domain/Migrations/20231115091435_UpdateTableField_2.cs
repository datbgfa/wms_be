﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WMS.Domain.Migrations
{
    public partial class UpdateTableField_2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BranchWarehouse_WMS_User_WarehouseOwner",
                table: "WMS_BranchWarehouse");

            migrationBuilder.DropColumn(
                name: "CostPrice",
                table: "WMS_BaseExportProduct");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_Token",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(2023, 11, 15, 9, 14, 35, 455, DateTimeKind.Utc).AddTicks(2705),
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldDefaultValue: new DateTime(2023, 11, 14, 9, 35, 39, 246, DateTimeKind.Utc).AddTicks(950));

            migrationBuilder.AddColumn<decimal>(
                name: "CostPrice",
                table: "WMS_BranchWarehouseProduct",
                type: "money",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "ExportPrice",
                table: "WMS_BranchWarehouseProduct",
                type: "money",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AlterColumn<Guid>(
                name: "WarehouseOwner",
                table: "WMS_BranchWarehouse",
                type: "uuid",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AddColumn<decimal>(
                name: "BalanceChange",
                table: "WMS_BranchInvoice",
                type: "money",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "BalanceChange",
                table: "WMS_BaseInvoice",
                type: "money",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BranchWarehouse_WMS_User_WarehouseOwner",
                table: "WMS_BranchWarehouse",
                column: "WarehouseOwner",
                principalTable: "WMS_User",
                principalColumn: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BranchWarehouse_WMS_User_WarehouseOwner",
                table: "WMS_BranchWarehouse");

            migrationBuilder.DropColumn(
                name: "CostPrice",
                table: "WMS_BranchWarehouseProduct");

            migrationBuilder.DropColumn(
                name: "ExportPrice",
                table: "WMS_BranchWarehouseProduct");

            migrationBuilder.DropColumn(
                name: "BalanceChange",
                table: "WMS_BranchInvoice");

            migrationBuilder.DropColumn(
                name: "BalanceChange",
                table: "WMS_BaseInvoice");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_Token",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(2023, 11, 14, 9, 35, 39, 246, DateTimeKind.Utc).AddTicks(950),
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldDefaultValue: new DateTime(2023, 11, 15, 9, 14, 35, 455, DateTimeKind.Utc).AddTicks(2705));

            migrationBuilder.AlterColumn<Guid>(
                name: "WarehouseOwner",
                table: "WMS_BranchWarehouse",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                oldClrType: typeof(Guid),
                oldType: "uuid",
                oldNullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "CostPrice",
                table: "WMS_BaseExportProduct",
                type: "numeric",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BranchWarehouse_WMS_User_WarehouseOwner",
                table: "WMS_BranchWarehouse",
                column: "WarehouseOwner",
                principalTable: "WMS_User",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
