﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WMS.Domain.Migrations
{
    public partial class FixTableField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BranchQuantity",
                table: "WMS_BaseStockCheckSheetDetail");

            migrationBuilder.RenameColumn(
                name: "Quantity",
                table: "WMS_BaseStockCheckSheetDetail",
                newName: "BaseQuantity");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_Token",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(2023, 11, 19, 17, 31, 30, 295, DateTimeKind.Utc).AddTicks(5391),
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldDefaultValue: new DateTime(2023, 11, 19, 16, 53, 22, 734, DateTimeKind.Utc).AddTicks(4050));

            migrationBuilder.AddColumn<string>(
                name: "ExportCodeItem",
                table: "WMS_BranchProductReturnDetail",
                type: "character varying(50)",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchProductReturnDetail_ExportCodeItem",
                table: "WMS_BranchProductReturnDetail",
                column: "ExportCodeItem",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_WMS_BranchProductReturnDetail_ExportCodeItem",
                table: "WMS_BranchProductReturnDetail");

            migrationBuilder.DropColumn(
                name: "ExportCodeItem",
                table: "WMS_BranchProductReturnDetail");

            migrationBuilder.RenameColumn(
                name: "BaseQuantity",
                table: "WMS_BaseStockCheckSheetDetail",
                newName: "Quantity");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_Token",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(2023, 11, 19, 16, 53, 22, 734, DateTimeKind.Utc).AddTicks(4050),
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldDefaultValue: new DateTime(2023, 11, 19, 17, 31, 30, 295, DateTimeKind.Utc).AddTicks(5391));

            migrationBuilder.AddColumn<int>(
                name: "BranchQuantity",
                table: "WMS_BaseStockCheckSheetDetail",
                type: "integer",
                nullable: false,
                defaultValue: 0);
        }
    }
}
