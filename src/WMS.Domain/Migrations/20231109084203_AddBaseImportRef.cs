﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WMS.Domain.Migrations
{
    public partial class AddBaseImportRef : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BaseInvoice_WMS_BaseDebt_DebtId",
                table: "WMS_BaseInvoice");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_Token",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(2023, 11, 9, 8, 42, 3, 427, DateTimeKind.Utc).AddTicks(4793),
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldDefaultValue: new DateTime(2023, 11, 8, 18, 52, 56, 23, DateTimeKind.Utc).AddTicks(7449));

            migrationBuilder.AddColumn<Guid>(
                name: "BaseImportId",
                table: "WMS_BranchProductReturnDetail",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "BaseImportId",
                table: "WMS_BranchImportProduct",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AlterColumn<Guid>(
                name: "DebtId",
                table: "WMS_BaseInvoice",
                type: "uuid",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AddColumn<Guid>(
                name: "BaseImportId",
                table: "WMS_BaseExportProduct",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AlterColumn<string>(
                name: "ActionDescription",
                table: "WMS_Action",
                type: "character varying(100)",
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(100)",
                oldMaxLength: 100);

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchProductReturnDetail_BaseImportId",
                table: "WMS_BranchProductReturnDetail",
                column: "BaseImportId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchImportProduct_BaseImportId",
                table: "WMS_BranchImportProduct",
                column: "BaseImportId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BaseExportProduct_BaseImportId",
                table: "WMS_BaseExportProduct",
                column: "BaseImportId");

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BaseExportProduct_WMS_BaseImport_BaseImportId",
                table: "WMS_BaseExportProduct",
                column: "BaseImportId",
                principalTable: "WMS_BaseImport",
                principalColumn: "ImportId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BaseInvoice_WMS_BaseDebt_DebtId",
                table: "WMS_BaseInvoice",
                column: "DebtId",
                principalTable: "WMS_BaseDebt",
                principalColumn: "DebtId");

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BranchImportProduct_WMS_BaseImport_BaseImportId",
                table: "WMS_BranchImportProduct",
                column: "BaseImportId",
                principalTable: "WMS_BaseImport",
                principalColumn: "ImportId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BranchProductReturnDetail_WMS_BaseImport_BaseImportId",
                table: "WMS_BranchProductReturnDetail",
                column: "BaseImportId",
                principalTable: "WMS_BaseImport",
                principalColumn: "ImportId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BaseExportProduct_WMS_BaseImport_BaseImportId",
                table: "WMS_BaseExportProduct");

            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BaseInvoice_WMS_BaseDebt_DebtId",
                table: "WMS_BaseInvoice");

            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BranchImportProduct_WMS_BaseImport_BaseImportId",
                table: "WMS_BranchImportProduct");

            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BranchProductReturnDetail_WMS_BaseImport_BaseImportId",
                table: "WMS_BranchProductReturnDetail");

            migrationBuilder.DropIndex(
                name: "IX_WMS_BranchProductReturnDetail_BaseImportId",
                table: "WMS_BranchProductReturnDetail");

            migrationBuilder.DropIndex(
                name: "IX_WMS_BranchImportProduct_BaseImportId",
                table: "WMS_BranchImportProduct");

            migrationBuilder.DropIndex(
                name: "IX_WMS_BaseExportProduct_BaseImportId",
                table: "WMS_BaseExportProduct");

            migrationBuilder.DropColumn(
                name: "BaseImportId",
                table: "WMS_BranchProductReturnDetail");

            migrationBuilder.DropColumn(
                name: "BaseImportId",
                table: "WMS_BranchImportProduct");

            migrationBuilder.DropColumn(
                name: "BaseImportId",
                table: "WMS_BaseExportProduct");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_Token",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(2023, 11, 8, 18, 52, 56, 23, DateTimeKind.Utc).AddTicks(7449),
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldDefaultValue: new DateTime(2023, 11, 9, 8, 42, 3, 427, DateTimeKind.Utc).AddTicks(4793));

            migrationBuilder.AlterColumn<Guid>(
                name: "DebtId",
                table: "WMS_BaseInvoice",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                oldClrType: typeof(Guid),
                oldType: "uuid",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ActionDescription",
                table: "WMS_Action",
                type: "character varying(100)",
                maxLength: 100,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "character varying(100)",
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BaseInvoice_WMS_BaseDebt_DebtId",
                table: "WMS_BaseInvoice",
                column: "DebtId",
                principalTable: "WMS_BaseDebt",
                principalColumn: "DebtId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
