﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WMS.Domain.Migrations
{
    public partial class AddBaseTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BaseWarehouse_WMS_User_WmsUserUserId",
                table: "WMS_BaseWarehouse");

            migrationBuilder.DropIndex(
                name: "IX_WMS_BaseWarehouse_WmsUserUserId",
                table: "WMS_BaseWarehouse");

            migrationBuilder.DropColumn(
                name: "Logo",
                table: "WMS_BranchWarehouse");

            migrationBuilder.DropColumn(
                name: "Logo",
                table: "WMS_BaseWarehouse");

            migrationBuilder.DropColumn(
                name: "WmsUserUserId",
                table: "WMS_BaseWarehouse");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_Token",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(2023, 10, 18, 20, 47, 27, 935, DateTimeKind.Utc).AddTicks(6324),
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldDefaultValue: new DateTime(2023, 10, 17, 20, 45, 56, 974, DateTimeKind.Utc).AddTicks(4881));

            migrationBuilder.CreateTable(
                name: "WMS_BaseDebt",
                columns: table => new
                {
                    DebtId = table.Column<Guid>(type: "uuid", nullable: false),
                    CreditorId = table.Column<Guid>(type: "uuid", nullable: false),
                    DebtorId = table.Column<Guid>(type: "uuid", nullable: false),
                    DebtAmount = table.Column<decimal>(type: "money", nullable: false),
                    CreditorSupplierId = table.Column<int>(type: "integer", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WMS_BaseDebt", x => x.DebtId);
                    table.ForeignKey(
                        name: "FK_WMS_BaseDebt_WMS_BaseWarehouse_DebtorId",
                        column: x => x.DebtorId,
                        principalTable: "WMS_BaseWarehouse",
                        principalColumn: "WarehouseId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WMS_BaseDebt_WMS_Supplier_CreditorSupplierId",
                        column: x => x.CreditorSupplierId,
                        principalTable: "WMS_Supplier",
                        principalColumn: "SupplierId");
                });

            migrationBuilder.CreateTable(
                name: "WMS_BaseExport",
                columns: table => new
                {
                    ExportId = table.Column<Guid>(type: "uuid", nullable: false),
                    WarehouseId = table.Column<Guid>(type: "uuid", nullable: false),
                    AssignBy = table.Column<Guid>(type: "uuid", nullable: false),
                    ExportBy = table.Column<Guid>(type: "uuid", nullable: false),
                    ExportDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    TotalPrice = table.Column<decimal>(type: "money", nullable: false),
                    PaidAmount = table.Column<decimal>(type: "money", nullable: false),
                    ShippingStatus = table.Column<short>(type: "smallint", nullable: false),
                    PaymentStatus = table.Column<short>(type: "smallint", nullable: false),
                    ApprovalStatus = table.Column<short>(type: "smallint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WMS_BaseExport", x => new { x.ExportId, x.WarehouseId });
                    table.UniqueConstraint("AK_WMS_BaseExport_ExportId", x => x.ExportId);
                    table.ForeignKey(
                        name: "FK_WMS_BaseExport_WMS_BaseWarehouse_WarehouseId",
                        column: x => x.WarehouseId,
                        principalTable: "WMS_BaseWarehouse",
                        principalColumn: "WarehouseId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WMS_BaseExport_WMS_User_AssignBy",
                        column: x => x.AssignBy,
                        principalTable: "WMS_User",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WMS_BaseExport_WMS_User_ExportBy",
                        column: x => x.ExportBy,
                        principalTable: "WMS_User",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WMS_BaseImport",
                columns: table => new
                {
                    ImportId = table.Column<Guid>(type: "uuid", nullable: false),
                    WarehouseId = table.Column<Guid>(type: "uuid", nullable: false),
                    RequestId = table.Column<Guid>(type: "uuid", nullable: false),
                    AssignBy = table.Column<Guid>(type: "uuid", nullable: false),
                    ImportBy = table.Column<Guid>(type: "uuid", nullable: false),
                    SupplierId = table.Column<int>(type: "integer", nullable: false),
                    ImportedDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    TotalPrice = table.Column<decimal>(type: "money", nullable: false),
                    ExtraCost = table.Column<decimal>(type: "money", nullable: false),
                    PaidAmount = table.Column<decimal>(type: "money", nullable: false),
                    WmsSupplierSupplierId = table.Column<int>(type: "integer", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WMS_BaseImport", x => new { x.ImportId, x.WarehouseId, x.RequestId });
                    table.UniqueConstraint("AK_WMS_BaseImport_ImportId", x => x.ImportId);
                    table.ForeignKey(
                        name: "FK_WMS_BaseImport_WMS_BaseWarehouse_WarehouseId",
                        column: x => x.WarehouseId,
                        principalTable: "WMS_BaseWarehouse",
                        principalColumn: "WarehouseId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WMS_BaseImport_WMS_Supplier_WmsSupplierSupplierId",
                        column: x => x.WmsSupplierSupplierId,
                        principalTable: "WMS_Supplier",
                        principalColumn: "SupplierId");
                    table.ForeignKey(
                        name: "FK_WMS_BaseImport_WMS_User_AssignBy",
                        column: x => x.AssignBy,
                        principalTable: "WMS_User",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WMS_BaseImport_WMS_User_ImportBy",
                        column: x => x.ImportBy,
                        principalTable: "WMS_User",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WMS_BaseProductReturn",
                columns: table => new
                {
                    ProductReturnId = table.Column<Guid>(type: "uuid", nullable: false),
                    Description = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    CreateAt = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Status = table.Column<short>(type: "smallint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WMS_BaseProductReturn", x => x.ProductReturnId);
                });

            migrationBuilder.CreateTable(
                name: "WMS_BaseWarehouseProduct",
                columns: table => new
                {
                    ProductId = table.Column<Guid>(type: "uuid", nullable: false),
                    WarehouseId = table.Column<Guid>(type: "uuid", nullable: false),
                    BpId = table.Column<Guid>(type: "uuid", nullable: false),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: true, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WMS_BaseWarehouseProduct", x => new { x.ProductId, x.WarehouseId });
                    table.ForeignKey(
                        name: "FK_WMS_BaseWarehouseProduct_WMS_BaseWarehouse_WarehouseId",
                        column: x => x.WarehouseId,
                        principalTable: "WMS_BaseWarehouse",
                        principalColumn: "WarehouseId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WMS_BaseWarehouseProduct_WMS_Product_ProductId",
                        column: x => x.ProductId,
                        principalTable: "WMS_Product",
                        principalColumn: "ProductId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WMS_BranchWarehouseProduct",
                columns: table => new
                {
                    ProductId = table.Column<Guid>(type: "uuid", nullable: false),
                    WarehouseId = table.Column<Guid>(type: "uuid", nullable: false),
                    BpId = table.Column<Guid>(type: "uuid", nullable: false),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: true, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WMS_BranchWarehouseProduct", x => new { x.ProductId, x.WarehouseId });
                    table.ForeignKey(
                        name: "FK_WMS_BranchWarehouseProduct_WMS_BranchWarehouse_WarehouseId",
                        column: x => x.WarehouseId,
                        principalTable: "WMS_BranchWarehouse",
                        principalColumn: "WarehouseId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WMS_BranchWarehouseProduct_WMS_Product_ProductId",
                        column: x => x.ProductId,
                        principalTable: "WMS_Product",
                        principalColumn: "ProductId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WMS_BaseInvoice",
                columns: table => new
                {
                    InvoiceId = table.Column<Guid>(type: "uuid", nullable: false),
                    DebtId = table.Column<Guid>(type: "uuid", nullable: false),
                    CreatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Type = table.Column<short>(type: "smallint", nullable: false),
                    Amount = table.Column<int>(type: "integer", nullable: false),
                    Description = table.Column<string>(type: "character varying(1000)", maxLength: 1000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WMS_BaseInvoice", x => new { x.InvoiceId, x.DebtId });
                    table.ForeignKey(
                        name: "FK_WMS_BaseInvoice_WMS_BaseDebt_DebtId",
                        column: x => x.DebtId,
                        principalTable: "WMS_BaseDebt",
                        principalColumn: "DebtId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WMS_BaseExportProduct",
                columns: table => new
                {
                    ExportId = table.Column<Guid>(type: "uuid", nullable: false),
                    ProductId = table.Column<Guid>(type: "uuid", nullable: false),
                    UnitPrice = table.Column<decimal>(type: "money", nullable: false),
                    UnitType = table.Column<short>(type: "smallint", nullable: false),
                    Quantiy = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WMS_BaseExportProduct", x => new { x.ExportId, x.ProductId });
                    table.ForeignKey(
                        name: "FK_WMS_BaseExportProduct_WMS_BaseExport_ExportId",
                        column: x => x.ExportId,
                        principalTable: "WMS_BaseExport",
                        principalColumn: "ExportId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WMS_BaseExportProduct_WMS_Product_ProductId",
                        column: x => x.ProductId,
                        principalTable: "WMS_Product",
                        principalColumn: "ProductId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WMS_BaseImportProduct",
                columns: table => new
                {
                    ImportId = table.Column<Guid>(type: "uuid", nullable: false),
                    ProductId = table.Column<Guid>(type: "uuid", nullable: false),
                    CostPrice = table.Column<decimal>(type: "money", nullable: false),
                    UnitPrice = table.Column<decimal>(type: "money", nullable: false),
                    UnitType = table.Column<short>(type: "smallint", nullable: false),
                    ImportedQuantity = table.Column<int>(type: "integer", nullable: false),
                    StockQuantity = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WMS_BaseImportProduct", x => new { x.ImportId, x.ProductId });
                    table.ForeignKey(
                        name: "FK_WMS_BaseImportProduct_WMS_BaseImport_ImportId",
                        column: x => x.ImportId,
                        principalTable: "WMS_BaseImport",
                        principalColumn: "ImportId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WMS_BaseImportProduct_WMS_Product_ProductId",
                        column: x => x.ProductId,
                        principalTable: "WMS_Product",
                        principalColumn: "ProductId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WMS_BaseStockCheckSheet",
                columns: table => new
                {
                    CheckId = table.Column<Guid>(type: "uuid", nullable: false),
                    CheckBy = table.Column<Guid>(type: "uuid", nullable: false),
                    ImportId = table.Column<Guid>(type: "uuid", nullable: false),
                    CheckDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Description = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    Status = table.Column<short>(type: "smallint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WMS_BaseStockCheckSheet", x => x.CheckId);
                    table.ForeignKey(
                        name: "FK_WMS_BaseStockCheckSheet_WMS_BaseImport_ImportId",
                        column: x => x.ImportId,
                        principalTable: "WMS_BaseImport",
                        principalColumn: "ImportId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WMS_BaseStockCheckSheet_WMS_User_CheckBy",
                        column: x => x.CheckBy,
                        principalTable: "WMS_User",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WMS_BaseProductReturnDetail",
                columns: table => new
                {
                    ProductReturnId = table.Column<Guid>(type: "uuid", nullable: false),
                    ImportId = table.Column<Guid>(type: "uuid", nullable: false),
                    ProductId = table.Column<Guid>(type: "uuid", nullable: false),
                    BprdId = table.Column<Guid>(type: "uuid", nullable: false),
                    CostPrice = table.Column<decimal>(type: "money", nullable: false),
                    UnitPrice = table.Column<decimal>(type: "money", nullable: false),
                    UnitType = table.Column<short>(type: "smallint", nullable: false),
                    RequestQuantity = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WMS_BaseProductReturnDetail", x => new { x.ProductReturnId, x.ImportId, x.ProductId });
                    table.ForeignKey(
                        name: "FK_WMS_BaseProductReturnDetail_WMS_BaseImport_ImportId",
                        column: x => x.ImportId,
                        principalTable: "WMS_BaseImport",
                        principalColumn: "ImportId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WMS_BaseProductReturnDetail_WMS_BaseProductReturn_ProductRe~",
                        column: x => x.ProductReturnId,
                        principalTable: "WMS_BaseProductReturn",
                        principalColumn: "ProductReturnId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WMS_BaseProductReturnDetail_WMS_Product_ProductId",
                        column: x => x.ProductId,
                        principalTable: "WMS_Product",
                        principalColumn: "ProductId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WMS_BaseStockCheckSheetDetail",
                columns: table => new
                {
                    CheckId = table.Column<Guid>(type: "uuid", nullable: false),
                    ProductId = table.Column<Guid>(type: "uuid", nullable: false),
                    Quantity = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WMS_BaseStockCheckSheetDetail", x => new { x.CheckId, x.ProductId });
                    table.ForeignKey(
                        name: "FK_WMS_BaseStockCheckSheetDetail_WMS_BaseStockCheckSheet_Check~",
                        column: x => x.CheckId,
                        principalTable: "WMS_BaseStockCheckSheet",
                        principalColumn: "CheckId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WMS_BaseStockCheckSheetDetail_WMS_Product_ProductId",
                        column: x => x.ProductId,
                        principalTable: "WMS_Product",
                        principalColumn: "ProductId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BaseWarehouse_WarehouseOwner",
                table: "WMS_BaseWarehouse",
                column: "WarehouseOwner");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BaseDebt_CreditorSupplierId",
                table: "WMS_BaseDebt",
                column: "CreditorSupplierId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BaseDebt_DebtorId",
                table: "WMS_BaseDebt",
                column: "DebtorId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BaseExport_AssignBy",
                table: "WMS_BaseExport",
                column: "AssignBy");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BaseExport_ExportBy",
                table: "WMS_BaseExport",
                column: "ExportBy");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BaseExport_WarehouseId",
                table: "WMS_BaseExport",
                column: "WarehouseId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BaseExportProduct_ProductId",
                table: "WMS_BaseExportProduct",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BaseImport_AssignBy",
                table: "WMS_BaseImport",
                column: "AssignBy");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BaseImport_ImportBy",
                table: "WMS_BaseImport",
                column: "ImportBy");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BaseImport_WarehouseId",
                table: "WMS_BaseImport",
                column: "WarehouseId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BaseImport_WmsSupplierSupplierId",
                table: "WMS_BaseImport",
                column: "WmsSupplierSupplierId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BaseImportProduct_ProductId",
                table: "WMS_BaseImportProduct",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BaseInvoice_DebtId",
                table: "WMS_BaseInvoice",
                column: "DebtId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BaseProductReturnDetail_ImportId",
                table: "WMS_BaseProductReturnDetail",
                column: "ImportId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BaseProductReturnDetail_ProductId",
                table: "WMS_BaseProductReturnDetail",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BaseStockCheckSheet_CheckBy",
                table: "WMS_BaseStockCheckSheet",
                column: "CheckBy");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BaseStockCheckSheet_ImportId",
                table: "WMS_BaseStockCheckSheet",
                column: "ImportId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BaseStockCheckSheetDetail_ProductId",
                table: "WMS_BaseStockCheckSheetDetail",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BaseWarehouseProduct_WarehouseId",
                table: "WMS_BaseWarehouseProduct",
                column: "WarehouseId");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchWarehouseProduct_WarehouseId",
                table: "WMS_BranchWarehouseProduct",
                column: "WarehouseId");

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BaseWarehouse_WMS_User_WarehouseOwner",
                table: "WMS_BaseWarehouse",
                column: "WarehouseOwner",
                principalTable: "WMS_User",
                principalColumn: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WMS_BaseWarehouse_WMS_User_WarehouseOwner",
                table: "WMS_BaseWarehouse");

            migrationBuilder.DropTable(
                name: "WMS_BaseExportProduct");

            migrationBuilder.DropTable(
                name: "WMS_BaseImportProduct");

            migrationBuilder.DropTable(
                name: "WMS_BaseInvoice");

            migrationBuilder.DropTable(
                name: "WMS_BaseProductReturnDetail");

            migrationBuilder.DropTable(
                name: "WMS_BaseStockCheckSheetDetail");

            migrationBuilder.DropTable(
                name: "WMS_BaseWarehouseProduct");

            migrationBuilder.DropTable(
                name: "WMS_BranchWarehouseProduct");

            migrationBuilder.DropTable(
                name: "WMS_BaseExport");

            migrationBuilder.DropTable(
                name: "WMS_BaseDebt");

            migrationBuilder.DropTable(
                name: "WMS_BaseProductReturn");

            migrationBuilder.DropTable(
                name: "WMS_BaseStockCheckSheet");

            migrationBuilder.DropTable(
                name: "WMS_BaseImport");

            migrationBuilder.DropIndex(
                name: "IX_WMS_BaseWarehouse_WarehouseOwner",
                table: "WMS_BaseWarehouse");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_Token",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(2023, 10, 17, 20, 45, 56, 974, DateTimeKind.Utc).AddTicks(4881),
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldDefaultValue: new DateTime(2023, 10, 18, 20, 47, 27, 935, DateTimeKind.Utc).AddTicks(6324));

            migrationBuilder.AddColumn<string>(
                name: "Logo",
                table: "WMS_BranchWarehouse",
                type: "character varying(1000)",
                maxLength: 1000,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Logo",
                table: "WMS_BaseWarehouse",
                type: "character varying(1000)",
                maxLength: 1000,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<Guid>(
                name: "WmsUserUserId",
                table: "WMS_BaseWarehouse",
                type: "uuid",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BaseWarehouse_WmsUserUserId",
                table: "WMS_BaseWarehouse",
                column: "WmsUserUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_WMS_BaseWarehouse_WMS_User_WmsUserUserId",
                table: "WMS_BaseWarehouse",
                column: "WmsUserUserId",
                principalTable: "WMS_User",
                principalColumn: "UserId");
        }
    }
}
