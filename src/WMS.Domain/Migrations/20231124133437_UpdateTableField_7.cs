﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WMS.Domain.Migrations
{
    public partial class UpdateTableField_7 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_WMS_BranchProductReturnDetail",
                table: "WMS_BranchProductReturnDetail");

            migrationBuilder.DropIndex(
                name: "IX_WMS_BranchProductReturnDetail_ExportCodeItem",
                table: "WMS_BranchProductReturnDetail");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_Token",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(2023, 11, 24, 13, 34, 36, 678, DateTimeKind.Utc).AddTicks(7536),
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldDefaultValue: new DateTime(2023, 11, 19, 17, 31, 30, 295, DateTimeKind.Utc).AddTicks(5391));

            migrationBuilder.AddPrimaryKey(
                name: "PK_WMS_BranchProductReturnDetail",
                table: "WMS_BranchProductReturnDetail",
                columns: new[] { "ProductReturnId", "ImportId", "ProductId", "ExportCodeItem" });

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchProductReturnDetail_ExportCodeItem",
                table: "WMS_BranchProductReturnDetail",
                column: "ExportCodeItem");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_WMS_BranchProductReturnDetail",
                table: "WMS_BranchProductReturnDetail");

            migrationBuilder.DropIndex(
                name: "IX_WMS_BranchProductReturnDetail_ExportCodeItem",
                table: "WMS_BranchProductReturnDetail");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_Token",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(2023, 11, 19, 17, 31, 30, 295, DateTimeKind.Utc).AddTicks(5391),
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldDefaultValue: new DateTime(2023, 11, 24, 13, 34, 36, 678, DateTimeKind.Utc).AddTicks(7536));

            migrationBuilder.AddPrimaryKey(
                name: "PK_WMS_BranchProductReturnDetail",
                table: "WMS_BranchProductReturnDetail",
                columns: new[] { "ProductReturnId", "ImportId", "ProductId" });

            migrationBuilder.CreateIndex(
                name: "IX_WMS_BranchProductReturnDetail_ExportCodeItem",
                table: "WMS_BranchProductReturnDetail",
                column: "ExportCodeItem",
                unique: true);
        }
    }
}
