﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WMS.Domain.Migrations
{
    public partial class UpdateTableField_4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_Token",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(2023, 11, 16, 15, 42, 35, 27, DateTimeKind.Utc).AddTicks(5047),
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldDefaultValue: new DateTime(2023, 11, 16, 3, 17, 4, 944, DateTimeKind.Utc).AddTicks(9967));

            migrationBuilder.AddColumn<string>(
                name: "InvoiceCode",
                table: "WMS_BranchInvoice",
                type: "character varying(50)",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "ExportCodeItem",
                table: "WMS_BranchImportProduct",
                type: "character varying(50)",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "ExportCode",
                table: "WMS_BranchImport",
                type: "character varying(50)",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "InvoiceCode",
                table: "WMS_BaseInvoice",
                type: "character varying(50)",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_WMS_Product_SKU",
                table: "WMS_Product",
                column: "SKU",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_WMS_Product_SKU",
                table: "WMS_Product");

            migrationBuilder.DropColumn(
                name: "InvoiceCode",
                table: "WMS_BranchInvoice");

            migrationBuilder.DropColumn(
                name: "ExportCodeItem",
                table: "WMS_BranchImportProduct");

            migrationBuilder.DropColumn(
                name: "ExportCode",
                table: "WMS_BranchImport");

            migrationBuilder.DropColumn(
                name: "InvoiceCode",
                table: "WMS_BaseInvoice");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreateDate",
                table: "WMS_Token",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(2023, 11, 16, 3, 17, 4, 944, DateTimeKind.Utc).AddTicks(9967),
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldDefaultValue: new DateTime(2023, 11, 16, 15, 42, 35, 27, DateTimeKind.Utc).AddTicks(5047));
        }
    }
}
