﻿namespace WMS.Domain.Enums
{
    public enum DateFilter
    {
        None = 0,
        ThisWeek = 1,
        ThisMonth = 2,
        ThisYear = 3,
    }
}
