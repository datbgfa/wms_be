﻿namespace WMS.Domain.Enums
{
    public enum UserStatusEnum
    {
        Disable = 0,
        Active = 1,
    }
}
