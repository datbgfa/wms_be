﻿using Microsoft.EntityFrameworkCore;
using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Entities;

namespace WMS.Domain.Repositories
{
    public class WmsProductAttributeRepository : BaseRepository<WmsProductAttribute>, IWmsProductAttributeRepository
    {
        private readonly AppDbContext _context;

        public WmsProductAttributeRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }

        public IQueryable<WmsProductAttribute> GetProductAttibute(Guid productId)
        {
            return _context.WmsProductAttributes
                .AsNoTracking()
                .Include(x => x.Attribute)
                .Where(x => x.ProductId == productId);
        }
    }
}
