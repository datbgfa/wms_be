﻿using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Entities;

namespace WMS.Domain.Repositories
{
    public class WmsCategoryRepository : BaseRepository<WmsCategory>, IWmsCategoryRepository
    {
        private readonly AppDbContext _context;

        public WmsCategoryRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }

        public IQueryable<WmsCategory> SearchAndFilterCategory(string searchWord, bool? visible)
        {
            var result = _context.WmsCategories.AsQueryable();

            if (!string.IsNullOrWhiteSpace(searchWord))
                result = result.Where(x => _context.Unaccent(x.CategoryName).ToLower()
                .Contains(_context.Unaccent(searchWord).ToLower()));

            if (visible != null)
            {
                result = result.Where(c => c.Visible.Equals(visible));
            }

            return result;
        }
    }
}
