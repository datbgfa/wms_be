﻿using Microsoft.EntityFrameworkCore;
using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Constant;
using WMS.Domain.Entities;

namespace WMS.Domain.Repositories
{
    public class WmsBranchInvoiceRepository : BaseRepository<WmsBranchInvoice>, IWmsBranchInvoiceRepository
    {
        private readonly AppDbContext _context;

        public WmsBranchInvoiceRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<int> CountBranchInvoices(short? type, DateTime? from, DateTime? to, string invoiceFor, Guid branchId)
        {
            var result = _context.WmsBranchInvoices
                .AsNoTracking()
                .OrderByDescending(x => x.CreateDate)
                .AsQueryable();

            if (type is not null)
            {
                result = result.Where(x => x.Type == type);
            }

            if (from is not null)
            {
                result = result.Where(x => x.CreateDate >= from);
            }

            if (to is not null)
            {
                result = result.Where(x => x.CreateDate <= to);
            }

            result = invoiceFor switch
            {
                "EXPORT" => result.Include(x => x.WmsBranchExport)
                            .Where(x => x.ExportId != null && x.WmsBranchExport!.WarehouseId.Equals(branchId)),
                "IMPORT" => result.Include(x => x.WmsBranchImport)
                            .Where(x => x.ImportId != null && x.WmsBranchImport!.WarehouseId.Equals(branchId)),
                "RETURN" => result.Include(x => x.WmsBranchProductReturn)
                            .ThenInclude(x => x.WmsBranchImport)
                            .Where(x => x.ReturnId != null && x.WmsBranchProductReturn!.WmsBranchImport!.WarehouseId.Equals(branchId)),
                _ => result
            };

            return await result.CountAsync();
        }

        public IQueryable<WmsBranchInvoice> GetBranchInvoices(short? type, DateTime? from, DateTime? to, string invoiceFor, int skip, int take, Guid branchId)
        {
            var result = _context.WmsBranchInvoices
                .AsNoTracking()
                .OrderByDescending(x => x.CreateDate)
                .AsQueryable();

            if (type is not null)
            {
                result = result.Where(x => x.Type == type);
            }

            if (from is not null)
            {
                result = result.Where(x => x.CreateDate >= from);
            }

            if (to is not null)
            {
                result = result.Where(x => x.CreateDate <= to);
            }

            result = invoiceFor switch
            {
                "EXPORT" => result.Include(x => x.WmsBranchExport)
                            .Where(x => x.ExportId != null && x.WmsBranchExport!.WarehouseId.Equals(branchId)),
                "IMPORT" => result.Include(x => x.WmsBranchImport)
                            .Where(x => x.ImportId != null && x.WmsBranchImport!.WarehouseId.Equals(branchId)),
                "RETURN" => result.Include(x => x.WmsBranchProductReturn)
                            .ThenInclude(x => x.WmsBranchImport)
                            .Where(x => x.ReturnId != null && x.WmsBranchProductReturn!.WmsBranchImport!.WarehouseId.Equals(branchId)),
                _ => throw new ArgumentException($"invoiceFor need is EXPORT, IMPORT or RETURN!")
            };

            return result.Skip(skip).Take(take);
        }

        public decimal GetMoneyPaid(DateTime from, DateTime to, Guid warehouseId)
        {
            return _context.WmsBranchInvoices
                .Include(x => x.WmsBranchImport)
                .Include(x => x.WmsBranchProductReturn).ThenInclude(x => x.WmsBranchImport).ThenInclude(x => x.WmsBranchWarehouse)
                .Where(x => x.CreateDate.Date >= from.Date && x.CreateDate.Date <= to.Date
                && x.Type == InvoiceTypeConstant.PaymentVoucher
                && (x.WmsBranchImport!.WarehouseId == warehouseId || x.WmsBranchProductReturn!.WmsBranchImport!.WmsBranchWarehouse!.WarehouseId == warehouseId))
                .AsNoTracking().Sum(x => x.Amount);
        }

        public decimal GetMoneyReceived(DateTime from, DateTime to, Guid warehouseId)
        {
            return _context.WmsBranchInvoices
                .Include(x => x.WmsBranchExport)
                .Include(x => x.WmsBranchProductReturn).ThenInclude(x => x.WmsBranchImport).ThenInclude(x => x.WmsBranchWarehouse)
                .Where(x => x.CreateDate.Date >= from.Date && x.CreateDate.Date <= to.Date
                && x.Type == InvoiceTypeConstant.ReceiptVoucher
                && (x.WmsBranchExport!.WarehouseId == warehouseId || x.WmsBranchProductReturn!.WmsBranchImport!.WmsBranchWarehouse!.WarehouseId == warehouseId))
                .AsNoTracking().Sum(x => x.Amount);
        }
    }
}
