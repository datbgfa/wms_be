﻿using Microsoft.EntityFrameworkCore;
using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Constant;
using WMS.Domain.Entities;

namespace WMS.Domain.Repositories
{
    public class WmsBaseInvoiceRepository : BaseRepository<WmsBaseInvoice>, IWmsBaseInvoiceRepository
    {
        private readonly AppDbContext _context;

        public WmsBaseInvoiceRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }

        public IQueryable<WmsBaseInvoice> GetDebt(int supplierId)
        {
            var result = from bi in _context.WmsBaseInvoices
                         join bd in _context.WmsBaseDebts
                         on bi.DebtId equals bd.DebtId
                         join bw in _context.WmsBaseWarehouses
                         on bd.DebtorId equals bw.WarehouseId
                         join s in _context.WmsSuppliers
                         on bd.CreditorId equals s.SupplierId
                         where s.SupplierId == supplierId
                         select bi;
            return result;
        }

        public IQueryable<WmsBaseInvoice> GetBaseInvoices(short? type, DateTime? from, DateTime? to, string invoiceFor, int skip, int take)
        {
            var result = _context.WmsBaseInvoices
                .AsNoTracking()
                .OrderByDescending(x => x.CreateDate)
                .AsQueryable();

            if (type is not null)
            {
                result = result.Where(x => x.Type == type);
            }

            if (from is not null)
            {
                result = result.Where(x => x.CreateDate >= from);
            }

            if (to is not null)
            {
                result = result.Where(x => x.CreateDate <= to);
            }

            result = invoiceFor switch
            {
                "EXPORT" => result.Where(x => x.ExportId != null),
                "IMPORT" => result.Where(x => x.ImportId != null),
                "RETURN" => result.Where(x => x.ReturnId != null),
                "BRETURN" => result.Where(x => x.BranchProductReturnId != null),
                _ => throw new ArgumentException($"invoiceFor need is EXPORT, IMPORT, RETURN or BRETURN!")
            };

            return result.Skip(skip).Take(take);
        }

        public async Task<int> CountBaseInvoices(short? type, DateTime? from, DateTime? to, string invoiceFor)
        {
            var result = _context.WmsBaseInvoices
                .AsNoTracking()
                .OrderByDescending(x => x.CreateDate)
                .AsQueryable();

            if (type is not null)
            {
                result = result.Where(x => x.Type == type);
            }

            if (from is not null)
            {
                result = result.Where(x => x.CreateDate >= from);
            }

            if (to is not null)
            {
                result = result.Where(x => x.CreateDate <= to);
            }

            result = invoiceFor switch
            {
                "EXPORT" => result.Where(x => x.ExportId != null),
                "IMPORT" => result.Where(x => x.ImportId != null),
                "RETURN" => result.Where(x => x.ReturnId != null),
                "BRETURN" => result.Where(x => x.BranchProductReturnId != null),
                _ => throw new ArgumentException($"invoiceFor need is EXPORT, IMPORT, RETURN or BRETURN!")
            };

            return await result.CountAsync();
        }


        public decimal GetMoneyPaid(DateTime from, DateTime to)
        {
            return _context.WmsBaseInvoices.Where(x => x.CreateDate.Date >= from.Date && x.CreateDate.Date <= to.Date
                && x.Type == InvoiceTypeConstant.PaymentVoucher)
                .AsNoTracking().Sum(x => x.Amount);
        }

        public decimal GetMoneyReceived(DateTime from, DateTime to)
        {
            return _context.WmsBaseInvoices.Where(x => x.CreateDate.Date >= from.Date && x.CreateDate.Date <= to.Date
                && x.Type == InvoiceTypeConstant.ReceiptVoucher)
                .AsNoTracking().Sum(x => x.Amount);
        }
    }
}
