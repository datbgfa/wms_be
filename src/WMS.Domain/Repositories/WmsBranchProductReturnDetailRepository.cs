﻿using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Entities;

namespace WMS.Domain.Repositories
{
    public class WmsBranchProductReturnDetailRepository : BaseRepository<WmsBranchProductReturnDetail>, IWmsBranchProductReturnDetailRepository
    {
        private readonly AppDbContext _context;

        public WmsBranchProductReturnDetailRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }

    }
}
