﻿using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Entities;

namespace WMS.Domain.Repositories
{
    public class WmsUserRoleRepository : BaseRepository<WmsUserRole>, IWmsUserRoleRepository
    {
        private readonly AppDbContext _context;

        public WmsUserRoleRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }
    }
}
