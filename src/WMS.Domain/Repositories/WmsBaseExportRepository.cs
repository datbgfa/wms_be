﻿using Microsoft.EntityFrameworkCore;
using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Constant;
using WMS.Domain.Entities;

namespace WMS.Domain.Repositories
{
    public class WmsBaseExportRepository : BaseRepository<WmsBaseExport>, IWmsBaseExportRepository
    {
        private readonly AppDbContext _context;

        public WmsBaseExportRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<IQueryable<WmsBaseExport>> FilterSearchBaseExport(string? searchWord, short? exportStatus, short? paymentStatus, Guid? branchWarehouseId, DateTime startDate, DateTime endDate)
        {
            var result = _context.Set<WmsBaseExport>()
            .Include(be => be.WmsBranchWarehouse)
            .Include(be => be.UserAssign)
            .Include(be => be.UserExport)
            .Include(be => be.WmsBranchProductReturns)
            .Include(be => be.WmsBaseExportProducts).ThenInclude(bep => bep!.WmsProduct)
            .AsQueryable();

            if (!string.IsNullOrEmpty(searchWord))
            {
                searchWord = searchWord.Trim();
                result = result.Where(bi => _context.Unaccent(bi.UserExport.Fullname).ToLower().Contains(_context.Unaccent(searchWord).ToLower()) ||
                                            _context.Unaccent(bi.ExportCode).ToLower().Contains(_context.Unaccent(searchWord).ToLower()));
            }

            if (branchWarehouseId != null && !branchWarehouseId.Equals(Guid.Empty))
            {
                result = result.Where(bi => bi.BranchWarehouseId.Equals(branchWarehouseId));
            }

            if (exportStatus != null)
            {
                result = result.Where(bi => bi.ExportStatus.Equals(exportStatus));
            }

            if (paymentStatus != null)
            {
                result = result.Where(bi => bi.PaymentStatus.Equals(paymentStatus));
            }

            if (startDate != default(DateTime) && endDate != default(DateTime))
            {
                result = result.Where(bi => bi.ExportDate >= startDate && bi.ExportDate <= endDate);
            }

            return result.OrderByDescending(be => be.CreateDate);
        }

        public async Task<WmsBaseExport> GetBaseExportDetailsById(Guid exportId)
        {
            var result = await _context.Set<WmsBaseExport>()
                 .Include(bi => bi.WmsBranchWarehouse)
                 .Include(bi => bi.UserAssign)
                 .Include(bi => bi.UserExport)
                 .Include(bi => bi.WmsBaseExportProducts).ThenInclude(bip => bip!.WmsProduct)
                 .FirstOrDefaultAsync(be => be.ExportId.Equals(exportId))
                 .ConfigureAwait(false);
            return result;
        }

        public async Task<List<WmsBaseExport>> GetBaseExportByBranchWarehouseId(Guid bwId)
        {
            var query = from baseExport in _context.WmsBaseExports.AsNoTracking()
                        where baseExport.PaymentStatus != PaymentStatusConstant.Paid
                        && baseExport.ExportStatus == ExportStatusConstant.Exported
                        && baseExport.BranchWarehouseId.Equals(bwId)
                        orderby baseExport.CreateDate ascending
                        select baseExport;

            return await query.ToListAsync().ConfigureAwait(false);
        }
    }
}
