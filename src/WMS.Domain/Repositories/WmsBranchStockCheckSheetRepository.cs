﻿using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Entities;

namespace WMS.Domain.Repositories
{
    public class WmsBranchStockCheckSheetRepository : BaseRepository<WmsBranchStockCheckSheet>, IWmsBranchStockCheckSheetRepository
    {
        private readonly AppDbContext _context;

        public WmsBranchStockCheckSheetRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }

        public IQueryable<WmsBranchStockCheckSheet> GetStockCheckSheetById(Guid checkId, Guid warehouseId)
        {
            var result = from wbscs in _context.WmsBranchStockCheckSheets
                         join wu in _context.WmsUsers on wbscs.CheckBy equals wu.UserId
                         join wbwu in _context.WmsBranchWarehouseUsers on wu.UserId equals wbwu.UserId
                         where wbscs.CheckId == checkId && wbwu.BranchWarehouseId == warehouseId
                         select wbscs;
            return result;
        }

        public IQueryable<WmsBranchStockCheckSheet> SearchBranchStockCheckSheet(string? searchWord, Guid warehouseId, DateTime startDate, DateTime endDate)
        {
            var result = from wbscs in _context.WmsBranchStockCheckSheets
                         join wu in _context.WmsUsers on wbscs.CheckBy equals wu.UserId
                         join wbwu in _context.WmsBranchWarehouseUsers on wu.UserId equals wbwu.UserId
                         where wbwu.BranchWarehouseId == warehouseId
                         select wbscs; ;
            if (!string.IsNullOrWhiteSpace(searchWord))
                result = result.Where(x => _context.Unaccent(x.StockCheckSheetCode).ToLower().Contains(_context.Unaccent(searchWord).ToLower()) ||
                                           _context.Unaccent(x.UserCheck.Username).ToLower().Contains(_context.Unaccent(searchWord).ToLower()));

            if (startDate != default(DateTime) && endDate != default(DateTime))
            {
                result = result.Where(bi => bi.CheckDate >= startDate && bi.CheckDate <= endDate);
            }
            return result.AsQueryable()
                .OrderByDescending(x => x.CheckDate);

        }

        //public IQueryable<WmsBranchStockCheckSheet> GetStockCheckSheetByCode(string code)
        //{
        //    var result = _context.WmsBranchStockCheckSheets.Include(x => x.UserCheck)
        //        .Where(x => x.StockCheckSheetCode == code).AsQueryable();
        //    return result;
        //}
    }
}
