﻿using Microsoft.EntityFrameworkCore;
using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Entities;

namespace WMS.Domain.Repositories
{
    public class WmsBranchImportRepository : BaseRepository<WmsBranchImport>, IWmsBranchImportRepository
    {
        private readonly AppDbContext _context;

        public WmsBranchImportRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }

        public IQueryable<WmsBranchImport> FilterBranchImport(Guid warehouseId, string? searchWord, short? importStatus, short? paymentStatus, DateTime startDate, DateTime endDate)
        {
            var result = _context.Set<WmsBranchImport>()
           .Include(bi => bi.UserAssignBy)
           .Include(bi => bi.UserImportBy)
           .Include(bi => bi.WmsBranchProductReturns)
           .Include(bi => bi.WmsBranchImportProducts).ThenInclude(bip => bip!.WmsProduct)
           .Where(bi => bi.WarehouseId.Equals(warehouseId))
           .AsQueryable();

            if (!string.IsNullOrEmpty(searchWord))
            {
                searchWord = searchWord.Trim();
                result = result.Where(bi => _context.Unaccent(bi.ImportCode).ToLower().Contains(_context.Unaccent(searchWord).ToLower()) ||
                                            _context.Unaccent(bi.UserImportBy.Fullname).ToLower().Contains(_context.Unaccent(searchWord).ToLower()));
            }

            if (importStatus != null)
            {
                result = result.Where(bi => bi.ImportStatus.Equals(importStatus));
            }
            if (paymentStatus != null)
            {
                result = result.Where(bi => bi.PaymentStatus.Equals(paymentStatus));
            }
            if (startDate != default(DateTime) && endDate != default(DateTime))
            {
                result = result.Where(bi => bi.ImportedDate >= startDate && bi.ImportedDate <= endDate);
            }
            return result.OrderByDescending(be => be.CreateDate);
        }

        public async Task<WmsBranchImport> GetBranchRequestDetails(Guid branchImportId)
        {
            var result = await _context.Set<WmsBranchImport>()
            .Include(bi => bi.UserAssignBy)
            .Include(bi => bi.UserImportBy)
            .Include(bi => bi.WmsBaseWarehouse)
            .Include(bi => bi.WmsBranchImportProducts).ThenInclude(bip => bip!.WmsProduct)
            .FirstOrDefaultAsync(bi => bi.ImportId.Equals(branchImportId));
            return result;
        }
        public IEnumerable<WmsBranchImportProduct> GetBranchImportByWarehouseIdAndProductIds(Guid warehouseId, IEnumerable<Guid> productIds)
        {
            var branchImportProduct = _context.Set<WmsBranchImport>()
                .Include(bi => bi.WmsBranchImportProducts)
                    .ThenInclude(bip => bip.WmsProduct)
                    .ThenInclude(p => p.WmsBranchWarehouseProducts)
                .Where(bi => bi.WarehouseId.Equals(warehouseId))
                .SelectMany(bi => bi.WmsBranchImportProducts);
            var result = branchImportProduct.Where(bip => productIds.Contains(bip.ProductId));
            return result;
        }
    }
}
