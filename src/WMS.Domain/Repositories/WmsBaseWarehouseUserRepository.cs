﻿using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Entities;

namespace WMS.Domain.Repositories
{
    public class WmsBaseWarehouseUserRepository : BaseRepository<WmsBaseWarehouseUser>, IWmsBaseWarehouseUserRepository
    {
        private readonly AppDbContext _context;

        public WmsBaseWarehouseUserRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }
    }
}
