﻿using Microsoft.EntityFrameworkCore;
using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Entities;

namespace WMS.Domain.Repositories
{
    public class WmsBranchExportProductRepository : BaseRepository<WmsBranchExportProduct>, IWmsBranchExportProductRepository
    {
        private readonly AppDbContext _context;

        public WmsBranchExportProductRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<IEnumerable<WmsBranchExportProduct>> GetBaseExportProductByExportIdProductIdAndListBaseImportId(Guid exportId, Guid productId, IEnumerable<Guid> branchImportId)
        {
            var result = await _context.Set<WmsBranchExportProduct>().Include(bep => bep.WmsBranchImport)
                                                              .Where(bep => bep.ProductId.Equals(productId) &&
                                                                            bep.ExportId.Equals(exportId) &&
                                                                            branchImportId.Contains(bep.ImportId))
                                                              .ToListAsync().ConfigureAwait(false);
            return result;
        }

        //public bool UpdateRange(IEnumerable<WmsBranchExportProduct> exportProducts)
        //{
        //    try
        //    {
        //        if (exportProducts == null) return false;
        //        _context.UpdateRange(exportProducts);
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //}

    }
}
