﻿using Microsoft.EntityFrameworkCore;
using System.Security.Claims;
using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Constant;
using WMS.Domain.Entities;
using WMS.Domain.Enums;

namespace WMS.Domain.Repositories
{
    public class WmsUserRepository : BaseRepository<WmsUser>, IWmsUserRepository
    {
        private readonly AppDbContext _context;

        public WmsUserRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }

        public IQueryable<WmsUser> SearchAccountsList(string? searchWord, short? status)
        {
            var result = _context.WmsUsers.AsQueryable();

            if (!string.IsNullOrWhiteSpace(searchWord))
                result = result.Where(x => _context.Unaccent(x.Fullname).ToLower()
                .Contains(_context.Unaccent(searchWord).ToLower()));

            if (status != null) result = result.Where(x => x.Status == status);
            return result;
        }

        public async Task<WmsUser> GetUserContainRoleByUsername(string username)
        {
            var user = await _context.Set<WmsUser>()
                .Include(u => u.UserRole)
                .FirstOrDefaultAsync(u => u.Username.Equals(username) &&
                                          u.Status == (short)UserStatusEnum.Active)
                .ConfigureAwait(false);
            return user;
        }

        public IQueryable<WmsUser> SearchEmployeesList(string? searchWord, short? status, int type, string currentUser)
        {
            var result = _context.WmsUsers.Include(x => x.UserRole).ThenInclude(x => x.Role)
                .Include(x => x.WmsBranchWarehouseUsers).ThenInclude(x => x.WmsBranchWarehouse)
                .Include(x => x.WmsBaseWarehouseUsers).ThenInclude(x => x.WmsBaseWarehouse).AsQueryable();

            if (!string.IsNullOrWhiteSpace(searchWord))
                result = result.Where(x => _context.Unaccent(x.Fullname).ToLower()
                .Contains(_context.Unaccent(searchWord).ToLower())
                || x.PhoneNumber.Contains(searchWord)
                || x.Email.ToLower().Contains(searchWord.ToLower()));

            if (status != null) result = result.Where(x => x.Status == status);

            //find employees in manager's warehouse
            var warehouseOwnerId = _context.WmsUsers.FirstOrDefault(x => x.Username.Equals(currentUser)).UserId;

            if (type == CommonConstant.BaseWarehouseManager)
            {
                result = result.Where(x => x.UserRole != null
                && x.UserRole.FirstOrDefault(ur => ur.RoleId.Equals(CommonConstant.BaseWarehouseEmployee)) != null);

                var warehouseId = _context.WmsBaseWarehouses.Where(x => x.WarehouseOwner.Equals(warehouseOwnerId))
                    .FirstOrDefault().WarehouseId;

                result = result.Where(x => x.WmsBaseWarehouseUsers.FirstOrDefault(bwu => bwu.BaseWarehouseId.Equals(warehouseId)) != null);
            }


            if (type == CommonConstant.BranchWarehouseDirector)
            {
                result = result.Where(x => x.UserRole != null
                    && (x.UserRole.FirstOrDefault(ur => ur.RoleId.Equals(CommonConstant.BranchWarehouseEmployee)) != null
                    || x.UserRole.FirstOrDefault(ur => ur.RoleId.Equals(CommonConstant.BranchWarehouseManager)) != null));

                var warehouseId = _context.WmsBranchWarehouses.Where(x => x.WarehouseOwner.Equals(warehouseOwnerId))
                    .FirstOrDefault().WarehouseId;

                result = result.Where(x => x.WmsBranchWarehouseUsers.FirstOrDefault(bwu => bwu.BranchWarehouseId.Equals(warehouseId)) != null);
            }

            return result.OrderBy(x => x.JoinDate);
        }

        public IQueryable<WmsUser> GetUserWithRoleByUsername(string username, short? status)
        {
            var result = _context.WmsUsers.Where(x => x.Username.Equals(username));
            if (status != null) result = result.Where(x => x.Status == status.Value);
            return result
                .Include(x => x.UserRole);
        }

        public IQueryable<WmsUser> GetUserWithRoleByUserid(Guid userid, short? status)
        {
            var result = _context.WmsUsers.Where(x => x.UserId.Equals(userid));
            if (status != null) result = result.Where(x => x.Status == status.Value);
            return result
                .Include(x => x.UserRole);
        }

        public IQueryable<WmsUser> GetUserWithWarehouseUsersByUserid(Guid userid, short? status)
        {
            var result = _context.WmsUsers.Where(x => x.UserId.Equals(userid));
            if (status != null) result = result.Where(x => x.Status == status.Value);
            return result
                .Include(x => x.WmsBranchWarehouseUsers)
                .Include(x => x.WmsBaseWarehouseUsers);
        }

        public async Task<IEnumerable<WmsRole>> GetUserRoleByUsername(string username)
        {
            var roles = from user in _context.WmsUsers.AsNoTracking()
                        join userRole in _context.WmsUserRoles.AsNoTracking()
                         on user.UserId equals userRole.UserId
                        join role in _context.WmsRoles.AsNoTracking()
                         on userRole.RoleId equals role.RoleId
                        where user.Username.Equals(username) && (!userRole.IsDelete!.Value)
                        select role;

            return await roles.ToListAsync();
        }

        public async Task<List<Claim>> GetUserRoleClaim(string username)
        {
            var query = from user in _context.WmsUsers.AsNoTracking()
                        join userRole in _context.WmsUserRoles.AsNoTracking()
                            on user.UserId equals userRole.UserId
                        join role in _context.WmsRoles.AsNoTracking()
                            on userRole.RoleId equals role.RoleId
                        where user.Username.Equals(username) && user.Status == (short)UserStatusEnum.Active
                        select new Claim(ClaimTypes.Role, role.RoleName);

            return await query.ToListAsync();
        }

        public IQueryable<WmsUser> GetAllBaseEmployee()
        {
            var result = _context.WmsUsers.Include(x => x.UserRole)
                .Include(x => x.WmsBaseWarehouseUsers).AsQueryable();

            return result.Where(x => x.WmsBaseWarehouseUsers != null && x.WmsBaseWarehouseUsers.Any() && x.Status == 1);
        }

        public IQueryable<WmsUser> GetAllBranchEmployee(Guid userid)
        {
            var warehouse = _context.WmsBranchWarehouseUsers.FirstOrDefault(x => x.UserId.Equals(userid));
            if (warehouse == null) return null;

            var result = _context.WmsUsers.Include(x => x.UserRole)
                .Include(x => x.WmsBranchWarehouseUsers).AsQueryable();

            return result.Where(x => x.WmsBranchWarehouseUsers != null && x.WmsBranchWarehouseUsers.Any()
            && x.WmsBranchWarehouseUsers.FirstOrDefault()!.BranchWarehouseId.Equals(warehouse.BranchWarehouseId) && x.Status == 1);
        }

        public IQueryable<WmsUser> GetAllBranchUser(Guid warehouseId)
        {
            return _context.WmsUsers.Include(x => x.WmsBranchWarehouseUsers)
                .Where(x => x.WmsBranchWarehouseUsers != null && x.WmsBranchWarehouseUsers.Any() && x.WmsBranchWarehouseUsers.First().BranchWarehouseId == warehouseId);      
        }
    }
}
