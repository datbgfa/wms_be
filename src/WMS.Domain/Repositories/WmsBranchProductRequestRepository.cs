﻿using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Entities;

namespace WMS.Domain.Repositories
{
    public class WmsBranchProductRequestRepository : BaseRepository<WmsBranchProductRequest>, IWmsBranchProductRequestRepository
    {
        private readonly AppDbContext _context;

        public WmsBranchProductRequestRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }
    }
}
