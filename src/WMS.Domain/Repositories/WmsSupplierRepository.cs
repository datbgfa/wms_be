﻿using Microsoft.EntityFrameworkCore;
using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Entities;

namespace WMS.Domain.Repositories
{
    public class WmsSupplierRepository : BaseRepository<WmsSupplier>, IWmsSupplierRepository
    {
        private readonly AppDbContext _context;

        public WmsSupplierRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }

        public IQueryable<WmsSupplier> SearchSuppliersList(string? searchWord)
        {
            var result = _context.WmsSuppliers.Include(x => x.WmsBaseDebts).AsQueryable();

            if (!string.IsNullOrWhiteSpace(searchWord))
                result = result.Where(x => _context.Unaccent(x.SupplierName).ToLower()
                .Contains(_context.Unaccent(searchWord).ToLower()));

            return result;
        }

        public async Task<WmsSupplier> GetSupplierDetails(int supplierId)
        {
            var result = await _context.WmsSuppliers?
                         .Include(x => x.WmsBaseDebts)
                         .Include(x => x.WmsBaseImports)
                         .ThenInclude(x => x.WmsBaseProductReturns)
                         .Where(x => x.SupplierId == supplierId)
                         .FirstOrDefaultAsync();
            return result;
        }

        public async Task<List<WmsSupplier>> GetSupplierHaveDebt()
        {
            var result = from supplier in _context.WmsSuppliers.AsNoTracking()
                         join baseDebt in _context.WmsBaseDebts.AsNoTracking()
                         on supplier.SupplierId equals baseDebt.CreditorId
                         where baseDebt.DebtAmount > 0
                         select new WmsSupplier
                         {
                             SupplierId = supplier.SupplierId,
                             SupplierName = supplier.SupplierName,
                             WmsBaseDebts = _context.WmsBaseDebts.AsNoTracking()
                                .Where(x => x.CreditorId == supplier.SupplierId)
                                .ToList()
                         };

            return await result.ToListAsync();
        }
    }
}
