﻿using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Entities;

namespace WMS.Domain.Repositories
{
    internal class WmsBaseProductReturnDetailRepository : BaseRepository<WmsBaseProductReturnDetail>, IWmsBaseProductReturnDetailRepository
    {
        private readonly AppDbContext _context;

        public WmsBaseProductReturnDetailRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }
    }
}
