﻿using Microsoft.EntityFrameworkCore;
using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Entities;

namespace WMS.Domain.Repositories
{
    public class WmsBranchExportRepository : BaseRepository<WmsBranchExport>, IWmsBranchExportRepository
    {
        private readonly AppDbContext _context;

        public WmsBranchExportRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<IQueryable<WmsBranchExport>> FilterSearchBranchExport(string? searchWord, Guid warehouseId, DateTime startDate, DateTime endDate)
        {
            var result = _context.Set<WmsBranchExport>()
           .Include(bi => bi.UserExport)
           .Include(bi => bi.UserAssign)
           .Include(bi => bi.WmsBranchExportProducts).ThenInclude(bip => bip!.WmsProduct)
           .Where(bi => bi.WarehouseId.Equals(warehouseId))
           .AsQueryable();

            if (!string.IsNullOrEmpty(searchWord))
            {
                searchWord = searchWord.Trim();
                result = result.Where(bi => _context.Unaccent(bi.UserExport.Fullname).ToLower().Contains(_context.Unaccent(searchWord).ToLower()) ||
                                            _context.Unaccent(bi.ExportCode).ToLower().Contains(_context.Unaccent(searchWord).ToLower()));
            }
            if (startDate != default(DateTime) && endDate != default(DateTime))
            {
                result = result.Where(bi => bi.ExportDate >= startDate && bi.ExportDate <= endDate);
            }
            return result.OrderByDescending(be => be.CreateDate);
        }

        public async Task<WmsBranchExport> GetBranchExportDetailsById(Guid branchExportId)
        {
            var result = await _context.Set<WmsBranchExport>()
            .Include(bi => bi.UserExport)
            .Include(bi => bi.UserAssign)
            .Include(bi => bi.WmsBranchExportProducts).ThenInclude(bip => bip!.WmsProduct)
            .FirstOrDefaultAsync(bi => bi.ExportId.Equals(branchExportId))
            .ConfigureAwait(false);
            return result;
        }
    }
}
