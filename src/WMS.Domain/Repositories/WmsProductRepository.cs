﻿using Microsoft.EntityFrameworkCore;
using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Constant;
using WMS.Domain.Entities;

namespace WMS.Domain.Repositories
{
    public class WmsProductRepository : BaseRepository<WmsProduct>, IWmsProductRepository
    {
        private readonly AppDbContext _appDbContext;

        public WmsProductRepository(AppDbContext context) : base(context)
        {
            _appDbContext = context;
        }

        public async Task<List<WmsProduct>> GetProductListPaging(int? categoryId, string? searchWord, bool? visible, int currentPage, int pageSize)
        {
            var data = _appDbContext.Set<WmsProduct>().Include(p => p.Category).AsQueryable();

            if (!categoryId.Equals(null) && !categoryId.Equals(0))
            {
                data = data.Where(x => x.CategoryId.Equals(categoryId));
            }

            if (!string.IsNullOrEmpty(searchWord))
            {
                data = data.Where(bi => _context.Unaccent(bi.ProductName).ToLower().Contains(_context.Unaccent(searchWord).ToLower()) ||
                                            _context.Unaccent(bi.SKU).ToLower().Contains(_context.Unaccent(searchWord).ToLower()));
            }
            if (visible != null)
            {
                data = data.Where(p => p.Visible.Equals(visible));
            }

            return await data.AsNoTracking()
                .OrderByDescending(x => x.CreateDate)
                .Skip(currentPage)
                .Take(pageSize)
                .ToListAsync().ConfigureAwait(false);
        }

        public async Task<IEnumerable<WmsProduct>> GetProductByName(string name)
        {
            var data = await _appDbContext.WmsProducts
                .AsNoTracking()
                .Where(x =>
                    _appDbContext
                    .Unaccent(x.ProductName)
                    .ToLower()
                    .Contains(
                        _appDbContext
                        .Unaccent(name)
                        .ToLower()))
                .ToListAsync();

            return data;
        }

        public IQueryable<Object> SearchStockQuantityForBase(string? searchWord, int? categoryID)
        {

            var result = from wp in _context.WmsProducts
                         join wbip in _context.WmsBaseImportProducts on wp.ProductId equals wbip.ProductId
                         join c in _context.WmsCategories on wp.CategoryId equals c.CategoryId
                         group new { wp, wbip, c } by wp.ProductId into g
                         select new
                         {
                             ProductId = g.Key,
                             ImportId = g.First().wbip.ImportId,
                             SKU = g.First().wp.SKU,
                             CategoryName = g.First().c.CategoryName,
                             ProductName = g.First().wp.ProductName,
                             ProductImage = g.First().wp.ProductImage,
                             UnitPrice = g.First().wp.UnitPrice,
                             InventoryNumber = g.Sum(x => x.wbip.StockQuantity),
                             CategoryId = g.First().wp.CategoryId,
                         };

            if (!string.IsNullOrWhiteSpace(searchWord))

                result = result.Where(x => _context.Unaccent(x.ProductName).ToLower().Contains(_context.Unaccent(searchWord).ToLower()) ||
                _context.Unaccent(x.SKU).ToLower().Contains(_context.Unaccent(searchWord).ToLower())
                );

            if (!categoryID.Equals(null) && !categoryID.Equals(0))
            {
                result = result.Where(x => x.CategoryId.Equals(categoryID));
            }

            return result;
        }

        public IQueryable<Object> SearchStockQuantityForBranch(string? searchWord, Guid warehouseId, int? categoryID)
        {

            var result = from wp in _context.WmsProducts
                         join c in _context.WmsCategories on wp.CategoryId equals c.CategoryId
                         join wbip in _context.WmsBranchImportProducts on wp.ProductId equals wbip.ProductId
                         join bi in _context.WmsBranchImports on wbip.ImportId equals bi.ImportId
                         join bw in _context.WmsBranchWarehouses on bi.WarehouseId equals bw.WarehouseId
                         where bw.WarehouseId == warehouseId
                         group new { wp, wbip, c } by wp.ProductId into g
                         select new
                         {
                             ProductId = g.Key,
                             ImportId = g.First().wbip.ImportId,
                             SKU = g.First().wp.SKU,
                             CategoryName = g.First().c.CategoryName,
                             ProductName = g.First().wp.ProductName,
                             ProductImage = g.First().wp.ProductImage,
                             UnitPrice = g.First().wp.UnitPrice,
                             InventoryNumber = g.Sum(x => x.wbip.StockQuantity),
                             CategoryId = g.First().wp.CategoryId,
                         };

            if (!string.IsNullOrWhiteSpace(searchWord))
                result = result.Where(x => _context.Unaccent(x.ProductName).ToLower().Contains(_context.Unaccent(searchWord).ToLower()) ||
                _context.Unaccent(x.SKU).ToLower().Contains(_context.Unaccent(searchWord).ToLower())
                );

            if (!categoryID.Equals(null) && !categoryID.Equals(0))
            {
                result = result.Where(x => x.CategoryId.Equals(categoryID));
            }

            return result;
        }

        public IQueryable<WmsBaseImportProduct> GetAllStockQuantityProductIdForBase(Guid productId)
        {
            var result = _context.WmsBaseImportProducts.Include(x => x.WmsProduct).Include(x => x.WmsBaseImport).Where(x => x.ProductId == productId);
            return result;
        }

        public IQueryable<WmsBranchImportProduct> GetAllStockQuantityProductIdForBranch(Guid productId, Guid warehouseId)
        {
            var result = _context.WmsBranchImportProducts
                .Include(x => x.WmsBaseImport)
                .Include(x => x.WmsProduct)
                .Include(wbip => wbip.WmsBranchImport)
                .ThenInclude(bi => bi.WmsBranchWarehouse)
            .Where(wbip => wbip.ProductId == productId && wbip.WmsBranchImport.WmsBranchWarehouse.WarehouseId == warehouseId);
            return result;
        }

        public decimal GetCostPriceByProductId(Guid productId)
        {
            var listProduct = _context.WmsBaseImportProducts.Include(x => x.WmsBaseImport)
                .Where(x => x.WmsBaseImport.ImportStatus == ImportStatusConstant.Imported
                && x.ProductId.Equals(productId));
            return listProduct.Sum(x => x.CostPrice) / listProduct.Count();
        }

        public int TotalProductsInStock(int type, DateTime from, DateTime to, Guid? warehouseId)
        {
            if (type == 0)
            {
                return _context.WmsBaseImportProducts.Include(x => x.WmsBaseImport)
                    .Where(x => x.WmsBaseImport!.ImportStatus == ImportStatusConstant.Imported
                    && x.WmsBaseImport.ImportedDate!.Value.Date >= from.Date && x.WmsBaseImport.ImportedDate!.Value.Date <= to.Date)
                    .Sum(x => x.StockQuantity);
            }
            else
            {
                return _context.WmsBranchImportProducts.Include(x => x.WmsBranchImport)
                    .Where(x => x.WmsBranchImport!.ImportStatus == ImportStatusConstant.Imported
                    && x.WmsBranchImport.ImportedDate!.Value.Date >= from.Date && x.WmsBranchImport.ImportedDate!.Value.Date <= to.Date
                    && x.WmsBranchImport.WarehouseId == warehouseId)
                    .Sum(x => x.StockQuantity);
            }
        }

        public int TotalProductsExported(int type, DateTime from, DateTime to, Guid? warehouseId)
        {
            if (type == 0)
            {
                return _context.WmsBaseExportProducts.Include(x => x.WmsBaseExport)
                    .Where(x => (x.WmsBaseExport!.ExportStatus == ExportStatusConstant.Exported || x.WmsBaseExport!.ExportStatus == ExportStatusConstant.Confirmed)
                     && x.WmsBaseExport.ExportDate!.Value.Date >= from.Date && x.WmsBaseExport.ExportDate!.Value.Date <= to.Date)
                    .Sum(x => x.Quantity);
            }
            else
            {
                return _context.WmsBranchExportProducts.Include(x => x.WmsBranchExport)
                    .Where(x => x.WmsBranchExport!.ApprovalStatus == ExportStatusConstant.Exported
                     && x.WmsBranchExport.ExportDate.Date >= from.Date && x.WmsBranchExport.ExportDate.Date <= to.Date 
                     && x.WmsBranchExport.WarehouseId == warehouseId)
                    .Sum(x => x.Quantity);
            }
        }

        public IEnumerable<IGrouping<Guid, WmsBaseExportProduct>> GetBaseTrendingProducts(DateTime from, DateTime to)
        {
            return _context.WmsBaseExportProducts.Include(x => x.WmsBaseExport).ToList()
                .Where(x => (x.WmsBaseExport!.ExportStatus == ExportStatusConstant.Exported || x.WmsBaseExport.ExportStatus == ExportStatusConstant.Confirmed)
                && x.WmsBaseExport.ExportDate!.Value.Date >= from && x.WmsBaseExport.ExportDate!.Value.Date <= to.Date)
                .GroupBy(x => x.ProductId)
                .OrderByDescending(x => x.Sum(g => g.Quantity))
                .Take(3);
        }

        public IEnumerable<IGrouping<Guid, WmsBranchExportProduct>> GetBranchTrendingProducts(DateTime from, DateTime to, Guid warehouseId)
        {
            return _context.WmsBranchExportProducts.Include(x => x.WmsBranchExport).ToList()
                .Where(x => (x.WmsBranchExport!.ApprovalStatus == ExportStatusConstant.Exported || x.WmsBranchExport.ApprovalStatus == ExportStatusConstant.Confirmed)
                && x.WmsBranchExport.ExportDate.Date >= from.Date && x.WmsBranchExport.ExportDate.Date <= to.Date && x.WmsBranchExport.WarehouseId.Equals(warehouseId))
                .GroupBy(x => x.ProductId)
                .OrderByDescending(x => x.Sum(g => g.Quantity))
                .Take(3);
        }

    }
}
