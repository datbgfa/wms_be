﻿using Microsoft.EntityFrameworkCore;
using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Entities;
using WMS.Domain.Enums;

namespace WMS.Domain.Repositories
{
    public class WmsBranchWarehouseRepository : BaseRepository<WmsBranchWarehouse>, IWmsBranchWarehouseRepository
    {
        private readonly AppDbContext _context;

        public WmsBranchWarehouseRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }

        public IQueryable<WmsBranchWarehouse> SearchBranchWarehousesList(string? searchWord, short? status)
        {
            var result = _context.WmsBranchWarehouses.AsQueryable();

            if (!string.IsNullOrWhiteSpace(searchWord))
                result = result.Where(x => _context.Unaccent(x.WarehouseName).ToLower()
                .Contains(_context.Unaccent(searchWord).ToLower())
                || x.PhoneNumber.Contains(searchWord)
                || x.Email.ToLower().Contains(searchWord.ToLower()));

            if (status != null) { result = result.Where(x => x.Status == status); }
            return result.Include(x => x.WmsWarehouseOwner);
        }

        public async Task<WmsBranchWarehouse> FindBranchWarehouseByWarehouseOwner(Guid warehouseOwner)
        {
            var result = await FindObject(bw => bw.WarehouseOwner.Equals(warehouseOwner)
                 && bw.Status == (short)UserStatusEnum.Active)
                .ConfigureAwait(false);
            return result;
        }

        public IQueryable<WmsBranchWarehouse> GetBranchWarehousesIncludingOwner(bool? hasOwner)
        {
            if (hasOwner == null)
            {
                return _context.WmsBranchWarehouses.Include(x => x.WmsWarehouseOwner);
            }
            else
            {
                if (hasOwner.Value) return _context.WmsBranchWarehouses.Where(x => x.WarehouseOwner != null).Include(x => x.WmsWarehouseOwner);
                else return _context.WmsBranchWarehouses.Where(x => x.WarehouseOwner == null).Include(x => x.WmsWarehouseOwner);
            }
        }

        public IQueryable<WmsBranchWarehouse> GetBranchWarehouseByUsername(string username)
        {
            var result = _context.WmsBranchWarehouses.Include(x => x.WmsBranchWarehouseUsers).ThenInclude(x => x.WmsUser)
                .Where(x => x.WmsWarehouseOwner.Username.ToLower().Equals(username.ToLower())).AsQueryable();
            return result;
        }

        public IQueryable<WmsBranchWarehouse> GetBranchWarehouseByCurrentUser(string username)
        {
            var result = from bw in _context.WmsBranchWarehouses
                         join bwu in _context.WmsBranchWarehouseUsers on bw.WarehouseId equals bwu.BranchWarehouseId
                         join u in _context.WmsUsers on bwu.UserId equals u.UserId
                         where u.Username == username
                         select bw;
            return result;
        }
    }
}
