﻿using Microsoft.EntityFrameworkCore;
using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Entities;
using WMS.Domain.Enums;

namespace WMS.Domain.Repositories
{
    public class WmsBaseWarehouseRepository : BaseRepository<WmsBaseWarehouse>, IWmsBaseWarehouseRepository
    {
        private readonly AppDbContext _context;

        public WmsBaseWarehouseRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }
        public async Task<WmsBaseWarehouse> FindBaseWarehouseByWarehouseOwner(Guid warehouseOwner)
        {
            var result = await FindObject(bw => bw.WarehouseOwner.Equals(warehouseOwner)
                 && bw.Status == (short)UserStatusEnum.Active)
                .ConfigureAwait(false);
            return result;
        }

        public IQueryable<WmsBaseWarehouse> GetBaseWarehouseIncludingOwner()
        {
            return _context.WmsBaseWarehouses.Include(x => x.WmsWarehouseOwner);
        }
    }
}
