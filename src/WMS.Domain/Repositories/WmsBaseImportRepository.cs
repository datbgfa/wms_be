﻿using Microsoft.EntityFrameworkCore;
using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Constant;
using WMS.Domain.Entities;

namespace WMS.Domain.Repositories
{
    public class WmsBaseImportRepository : BaseRepository<WmsBaseImport>, IWmsBaseImportRepository
    {
        private readonly AppDbContext _context;

        public WmsBaseImportRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }

        /// <summary>
        /// FilderSearchBaseImport
        /// </summary>
        /// <param name="searchWord"></param>
        /// <param name="importStatus"></param>
        /// <param name="paymentStatus"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns>IQueryable<WmsBaseImport</returns>
        public IQueryable<WmsBaseImport> FilterSearchBaseImport(string? searchWord, short? importStatus, short? paymentStatus, int? supplierId, DateTime? startDate, DateTime? endDate)
        {

            var result = _context.Set<WmsBaseImport>()
            .Include(bi => bi.UserAssignBy)
            .Include(bi => bi.UserImportBy)
            .Include(bi => bi.WmsSupplier)
            .Include(bi => bi.WmsBaseProductReturns)
            .Include(bi => bi.WmsBaseImportProducts).ThenInclude(bip => bip!.WmsProduct)
            .AsQueryable();

            if (!string.IsNullOrEmpty(searchWord))
            {
                searchWord = searchWord.Trim();
                result = result.Where(bi => _context.Unaccent(bi.ImportCode).ToLower().Contains(_context.Unaccent(searchWord).ToLower()) ||
                                            _context.Unaccent(bi.UserImportBy.Fullname).ToLower().Contains(_context.Unaccent(searchWord).ToLower()));
            }

            if (supplierId != null && !supplierId.Equals(CommonConstant.Zero))
            {
                result = result.Where(bi => bi.SupplierId.Equals(supplierId));
            }

            if (importStatus != null)
            {
                result = result.Where(bi => bi.ImportStatus.Equals(importStatus));
            }

            if (paymentStatus != null)
            {
                result = result.Where(bi => bi.PaymentStatus.Equals(paymentStatus));
            }

            if (startDate != default(DateTime) && endDate != default(DateTime))
            {
                result = result.Where(bi => bi.ImportedDate >= startDate && bi.ImportedDate <= endDate);
            }

            return result.OrderByDescending(be => be.CreateDate);
        }

        public async Task<WmsBaseImport> GetBaseImportDetailsById(Guid baseImportId)
        {
            var result = await _context.Set<WmsBaseImport>()
                .Include(bi => bi.UserAssignBy)
                .Include(bi => bi.UserImportBy)
                .Include(bi => bi.WmsSupplier)
                .Include(bi => bi.WmsBaseImportProducts).ThenInclude(bip => bip!.WmsProduct)
                .FirstOrDefaultAsync(bi => bi.ImportId.Equals(baseImportId))
                .ConfigureAwait(false);
            return result;
        }

        public IQueryable<WmsBaseImport> GetHistoryImport(int supplierId)
        {
            var result = _context.WmsBaseImports
                .Include(x => x.WmsSupplier)
                .Include(x => x.WmsBaseWarehouse)
                .Where(x => x.SupplierId == supplierId);
            return result;
        }

        public IEnumerable<WmsBaseImportProduct> GetBaseImportByWarehouseIdAndProductIds(Guid warehouseId, IEnumerable<Guid> productIds)
        {
            var baseImportProduct = _context.Set<WmsBaseImport>()
                .Include(bi => bi.WmsBaseImportProducts).ThenInclude(bip => bip.WmsProduct)
                .Where(bi => bi.WarehouseId.Equals(warehouseId))
                .SelectMany(bi => bi.WmsBaseImportProducts);
            var result = baseImportProduct.Where(bip => productIds.Contains(bip.ProductId));
            return result;
        }

        public async Task<List<WmsBaseImport>> GetBaseImportBySupplierId(int supplierId)
        {
            var query = from baseImport in _context.WmsBaseImports.AsNoTracking()
                        where baseImport.PaymentStatus != PaymentStatusConstant.Paid
                        && baseImport.SupplierId == supplierId
                        && baseImport.ImportStatus == ImportStatusConstant.Imported
                        orderby baseImport.CreateDate ascending
                        select baseImport;

            return await query.ToListAsync().ConfigureAwait(false);
        }
    }
}
