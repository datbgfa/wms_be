﻿using Microsoft.EntityFrameworkCore;
using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Entities;

namespace WMS.Domain.Repositories
{
    public class WmsBranchDebtRepository : BaseRepository<WmsBranchDebt>, IWmsBranchDebtRepository
    {
        private readonly AppDbContext _context;

        public WmsBranchDebtRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<List<WmsBranchWarehouse>> GetBranchHaveDebt()
        {
            var query = from branchWarehouse in _context.WmsBranchWarehouses.AsNoTracking()
                        join branchDebt in _context.WmsBranchDebts.AsNoTracking()
                        on branchWarehouse.WarehouseId equals branchDebt.DebtorId
                        where branchDebt.DebtAmount > 0
                        select new WmsBranchWarehouse
                        {
                            WarehouseId = branchWarehouse.WarehouseId,
                            WarehouseName = branchWarehouse.WarehouseName,
                            WmsBranchDebts = _context.WmsBranchDebts.AsNoTracking()
                                .Where(x => x.DebtorId == branchDebt.DebtorId)
                                .ToList()
                        };

            return await query.ToListAsync();
        }
    }
}
