﻿using Microsoft.EntityFrameworkCore;
using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Entities;

namespace WMS.Domain.Repositories
{
    public class WmsBranchRequestRepository : BaseRepository<WmsBranchRequest>, IWmsBranchRequestRepository
    {
        private readonly AppDbContext _context;

        public WmsBranchRequestRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }

        public IQueryable<WmsBranchRequest> FilterBranchRequest(Guid warehouseId, string? searchWord, short? approvedSatus, DateTime startDate, DateTime endDate)
        {

            var result = _context.Set<WmsBranchRequest>()
       .Include(bi => bi.UserApprovedBy)
       .Include(bi => bi.UserRequestBy)
       .Include(bi => bi.WmsBranchProductRequests).ThenInclude(bip => bip!.WmsProduct).Where(br => br.WarehouseId.Equals(warehouseId))
       .AsQueryable();

            if (!string.IsNullOrEmpty(searchWord))
            {
                searchWord = searchWord.Trim();
                result = result.Where(bi => _context.Unaccent(bi.RequestCode).ToLower().Contains(_context.Unaccent(searchWord).ToLower()) ||
                                            _context.Unaccent(bi.UserRequestBy.Fullname).ToLower().Contains(_context.Unaccent(searchWord).ToLower()));
            }

            if (approvedSatus != null)
            {
                result = result.Where(bi => bi.ApprovalStatus.Equals(approvedSatus));
            }

            if (startDate != default(DateTime) && endDate != default(DateTime))
            {
                result = result.Where(bi => bi.RequestDate >= startDate && bi.RequestDate <= endDate);
            }
            return result.OrderByDescending(be => be.CreateDate);
        }
        public async Task<WmsBranchRequest> GetBranchRequestDetails(Guid branchRequestId)
        {
            var result = await _context.Set<WmsBranchRequest>()
            .Include(bi => bi.UserApprovedBy)
            .Include(bi => bi.UserRequestBy)
            .Include(bi => bi.WmsBranchProductRequests).ThenInclude(bip => bip!.WmsProduct)
            .FirstOrDefaultAsync(br => br.RequestId.Equals(branchRequestId))
            .ConfigureAwait(false);
            return result;
        }
    }
}
