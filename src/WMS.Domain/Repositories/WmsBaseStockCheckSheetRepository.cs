﻿using Microsoft.EntityFrameworkCore;
using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Entities;

namespace WMS.Domain.Repositories
{
    public class WmsBaseStockCheckSheetRepository : BaseRepository<WmsBaseStockCheckSheet>, IWmsBaseStockCheckSheetRepository
    {
        private readonly AppDbContext _context;

        public WmsBaseStockCheckSheetRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }

        public IQueryable<WmsBaseStockCheckSheet> GetStockCheckSheetById(Guid checkId)
        {
            var result = from wbscs in _context.WmsBaseStockCheckSheets
                         join wu in _context.WmsUsers on wbscs.CheckBy equals wu.UserId
                         where wbscs.CheckId == checkId
                         select wbscs;
            return result;
        }

        public IQueryable<WmsBaseStockCheckSheet> SearchBaseStockCheckSheet(string? searchWord, DateTime startDate, DateTime endDate)
        {
            var result = _context.WmsBaseStockCheckSheets
                .Include(x => x.UserCheck)
                .OrderByDescending(x => x.CheckDate)
                .AsQueryable();

            if (!string.IsNullOrWhiteSpace(searchWord))
                result = result.Where(x => _context.Unaccent(x.StockCheckSheetCode).ToLower().Contains(_context.Unaccent(searchWord).ToLower()) ||
                 _context.Unaccent(x.UserCheck.Username).ToLower().Contains(_context.Unaccent(searchWord).ToLower()));

            if (startDate != default(DateTime) && endDate != default(DateTime))
            {
                result = result.Where(bi => bi.CheckDate >= startDate && bi.CheckDate <= endDate);
            }

            return result;
        }

        //public IQueryable<WmsBaseStockCheckSheet> GetStockCheckSheetByCode(string code)
        //{
        //    var result = _context.WmsBaseStockCheckSheets.Include(x => x.UserCheck)
        //        .Where(x => x.StockCheckSheetCode == code).AsQueryable();
        //    return result;
        //}
    }
}
