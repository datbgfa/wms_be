﻿using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Constant;
using WMS.Domain.Entities;

namespace WMS.Domain.Repositories
{
    public class WmsBranchStockCheckSheetDetailRepository : BaseRepository<WmsBranchStockCheckSheetDetail>, IWmsBranchStockCheckSheetDetailRepository
    {
        private readonly AppDbContext _context;

        public WmsBranchStockCheckSheetDetailRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }

        public IQueryable<Object> GetAllStockCheckSheetDetail(Guid checkId, Guid warehouseId)
        {
            var result = from wbscsd in _context.WmsBranchStockCheckSheetDetails
                         join wp in _context.WmsProducts on wbscsd.ProductId equals wp.ProductId
                         join wbwp in _context.WmsBranchWarehouseProducts on wp.ProductId equals wbwp.ProductId
                         where wbwp.WarehouseId == warehouseId && wbscsd.CheckId == checkId
                         group new { wbscsd, wp } by new
                         {
                             wbscsd.CheckId,
                             wp.ProductId,
                             wp.SKU,
                             wp.ProductName,
                             wp.ProductImage
                         } into grouped
                         select new
                         {
                             CheckId = grouped.Key.CheckId,
                             ProductId = grouped.Key.ProductId,
                             SKU = grouped.Key.SKU,
                             ProductName = grouped.Key.ProductName,
                             ProductImage = grouped.Key.ProductImage,
                             Quantity = grouped.Sum(x => x.wbscsd.BranchQuantity),
                             RealQuantity = grouped.Sum(x => x.wbscsd.RealQuantity),
                             Difference = grouped.Sum(x => x.wbscsd.RealQuantity) - grouped.Sum(x => x.wbscsd.BranchQuantity)
                         };
            return result;
        }

        public IQueryable<Object> GetImportsByProductId(Guid productId, Guid warehouseId)
        {
            var result = from wbip in _context.WmsBranchImportProducts
                         join wp in _context.WmsProducts on wbip.ProductId equals wp.ProductId
                         join bi in _context.WmsBranchImports on wbip.ImportId equals bi.ImportId
                         where wbip.ProductId == productId && bi.WarehouseId == warehouseId
                         && bi.ImportStatus == ImportStatusConstant.Imported
                         select new
                         {
                             ImportId = wbip.ImportId,
                             ProductId = wbip.ProductId,
                             ImportCode = bi.ImportCode,
                             SKU = wp.SKU,
                             ProductName = wp.ProductName,
                             ProductImage = wp.ProductImage,
                             StockQuantity = wbip.StockQuantity,
                             BaseImportId = wbip.BaseImportId,
                             ExportCodeItem = wbip.ExportCodeItem,
                         };
            return result;
        }

        public IQueryable<Object> GetStockCheckSheetImports(Guid checkId, Guid productId, Guid warehouseId)
        {
            var result = from bscsd in _context.WmsBranchStockCheckSheetDetails
                         join bi in _context.WmsBranchImports on bscsd.ImportId equals bi.ImportId
                         where bscsd.ProductId == productId && bi.WarehouseId == warehouseId && bscsd.CheckId == checkId
                         select new
                         {
                             ImportCode = bi.ImportCode,
                             ImportedDate = bi.ImportedDate,
                             RealQuantity = bscsd.RealQuantity,
                             StockQuantity = bscsd.BranchQuantity,
                             Difference = bscsd.RealQuantity - bscsd.BranchQuantity
                         };
            return result;
        }
    }
}
