﻿using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Entities;

namespace WMS.Domain.Repositories
{
    public class WmsBaseDebtRepository : BaseRepository<WmsBaseDebt>, IWmsBaseDebtRepository
    {
        private readonly AppDbContext _context;

        public WmsBaseDebtRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }

    }
}
