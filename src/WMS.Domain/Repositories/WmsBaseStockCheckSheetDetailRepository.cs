﻿using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Constant;
using WMS.Domain.Entities;

namespace WMS.Domain.Repositories
{
    public class WmsBaseStockCheckSheetDetailRepository : BaseRepository<WmsBaseStockCheckSheetDetail>, IWmsBaseStockCheckSheetDetailRepository
    {
        private readonly AppDbContext _context;

        public WmsBaseStockCheckSheetDetailRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }

        public IQueryable<Object> GetAllStockCheckSheetDetail(Guid checkId)
        {
            var result = from wbscsd in _context.WmsBaseStockCheckSheetDetails
                         join wp in _context.WmsProducts on wbscsd.ProductId equals wp.ProductId
                         where wbscsd.CheckId == checkId
                         group new { wbscsd, wp } by new
                         {
                             wbscsd.CheckId,
                             wp.ProductId,
                             wp.SKU,
                             wp.ProductName,
                             wp.ProductImage
                         } into grouped
                         select new
                         {
                             grouped.Key.CheckId,
                             grouped.Key.ProductId,
                             grouped.Key.SKU,
                             grouped.Key.ProductName,
                             grouped.Key.ProductImage,
                             Quantity = grouped.Sum(x => x.wbscsd.BaseQuantity),
                             RealQuantity = grouped.Sum(x => x.wbscsd.RealQuantity),
                             Difference = grouped.Sum(x => x.wbscsd.RealQuantity) - grouped.Sum(x => x.wbscsd.BaseQuantity)
                         };
            return result;
        }

        public IQueryable<Object> GetImportsByProductId(Guid productId)
        {
            var result = from wbip in _context.WmsBaseImportProducts
                         join wp in _context.WmsProducts on wbip.ProductId equals wp.ProductId
                         join wbi in _context.WmsBaseImports on wbip.ImportId equals wbi.ImportId
                         where wbip.ProductId == productId && wbi.ImportStatus == ImportStatusConstant.Imported
                         select new
                         {
                             ImportId = wbip.ImportId,
                             ProductId = wbip.ProductId,
                             ImportCode = wbi.ImportCode,
                             SKU = wp.SKU,
                             ProductName = wp.ProductName,
                             ProductImage = wp.ProductImage,
                             StockQuantity = wbip.StockQuantity
                         };
            return result;
        }

        public IQueryable<Object> GetStockCheckSheetImports(Guid checkId, Guid productId)
        {
            var result = from bscsd in _context.WmsBaseStockCheckSheetDetails
                         join wbi in _context.WmsBaseImports on bscsd.ImportId equals wbi.ImportId
                         where bscsd.ProductId == productId && bscsd.CheckId == checkId
                         select new
                         {
                             ImportCode = wbi.ImportCode,
                             ImportedDate = wbi.ImportedDate,
                             RealQuantity = bscsd.RealQuantity,
                             StockQuantity = bscsd.BaseQuantity,
                             Difference = bscsd.RealQuantity - bscsd.BaseQuantity,
                         };
            return result;
        }
    }
}
