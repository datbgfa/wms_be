﻿using Microsoft.EntityFrameworkCore;
using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Constant;
using WMS.Domain.Entities;

namespace WMS.Domain.Repositories
{
    public class WmsBranchProductReturnRepository : BaseRepository<WmsBranchProductReturn>, IWmsBranchProductReturnRepository
    {
        private readonly AppDbContext _context;

        public WmsBranchProductReturnRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<List<WmsBranchProductReturn>> GetProductsReturnFromBranch(
            int skipPage, 
            int pageSize, 
            string? searchWord, 
            Guid? branchWarehouseId, 
            DateTime startDate, 
            DateTime endDate,
            short? paymentStatus,
            short? status)
        {
            var data = _context.WmsBranchProductReturns
             .Include(x => x.WmsBranchProductReturnDetails!).ThenInclude(x => x.WmsProduct)
             .Include(x => x.WmsBranchImport).ThenInclude(x => x.WmsBranchWarehouse).AsQueryable();

            if (!string.IsNullOrEmpty(searchWord))
            {
                data = data.Where(bi => _context.Unaccent(bi.ReturnCode).ToLower().Contains(_context.Unaccent(searchWord).ToLower()));
            }

            if (branchWarehouseId != null && !branchWarehouseId.Equals(Guid.Empty))
            {
                data = data.Where(bi => bi.WmsBranchImport.WmsBranchWarehouse.WarehouseId.Equals(branchWarehouseId));
            }

            if (startDate != default(DateTime) && endDate != default(DateTime))
            {
                data = data.Where(bi => bi.CreateDate >= startDate && bi.CreateDate <= endDate);
            }

            if (paymentStatus is not null)
            {
                data = data.Where(bi => bi.PaymentStatus == paymentStatus);
            }

            if (status is not null)
            {
                data = data.Where(bi => bi.Status == status);
            }
            var result = await data.OrderByDescending(x => x.CreateDate).Skip(skipPage)
                .Take(pageSize).ToListAsync().ConfigureAwait(false);

            return result;
        }

        public async ValueTask<int> CountProductReturnFromBranch(
            string? searchWord, 
            Guid? branchWarehouseId, 
            DateTime startDate, 
            DateTime endDate,
            short? paymentStatus,
            short? status)
        {
            var data = _context.WmsBranchProductReturns
             .Include(x => x.WmsBranchProductReturnDetails!).ThenInclude(x => x.WmsProduct)
             .Include(x => x.WmsBranchImport).ThenInclude(x => x.WmsBranchWarehouse).AsQueryable();

            if (!string.IsNullOrEmpty(searchWord))
            {
                data = data.Where(bi => _context.Unaccent(bi.ReturnCode).ToLower().Contains(_context.Unaccent(searchWord).ToLower()));
            }

            if (branchWarehouseId != null && !branchWarehouseId.Equals(Guid.Empty))
            {
                data = data.Where(bi => bi.WmsBranchImport!.WmsBranchWarehouse!.WarehouseId.Equals(branchWarehouseId));
            }

            if (startDate != default(DateTime) && endDate != default(DateTime))
            {
                data = data.Where(bi => bi.CreateDate >= startDate && bi.CreateDate <= endDate);
            }

            if (paymentStatus is not null)
            {
                data = data.Where(bi => bi.PaymentStatus == paymentStatus);
            }

            if (status is not null)
            {
                data = data.Where(bi => bi.Status == status);
            }

            var result = await data
                .OrderByDescending(x => x.CreateDate)
                .CountAsync()
                .ConfigureAwait(false);
            return result;
        }

        /// <summary>
        /// Get Products Return From Branch By ImportId
        /// </summary>
        /// <param name="branchId"></param>
        /// <param name="skip"></param>
        /// <param name="take"></param>
        /// <returns></returns>
        public async Task<List<WmsBranchProductReturn>> GetProductsReturnFromBranchByImportId(Guid importId)
        {
            var data = await _context.WmsBranchProductReturns
                .Where(x => x.ImportId.Equals(importId))
                .Include(x => x.WmsBranchProductReturnDetails!)
                .ThenInclude(x => x.WmsProduct)
                .OrderByDescending(x => x.CreateDate)
                .ToListAsync()
                .ConfigureAwait(false);

            return data;
        }

        public async Task<List<WmsBranchProductReturn>> GetBranchProducReturnByBranchId(Guid branchId)
        {
            var query = from branchReturn in _context.WmsBranchProductReturns.AsNoTracking()
                        join branchImport in _context.WmsBranchImports.AsNoTracking()
                            on branchReturn.ImportId equals branchImport.ImportId
                        where branchReturn.PaymentStatus != PaymentStatusConstant.Paid
                        && branchImport.WarehouseId.Equals(branchId)
                        && branchReturn.Status == ProductReturnStatusConstant.Returned
                        orderby branchReturn.CreateDate ascending
                        select branchReturn;

            return await query.ToListAsync().ConfigureAwait(false);
        }
    }
}
