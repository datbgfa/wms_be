﻿using Microsoft.EntityFrameworkCore;
using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Constant;
using WMS.Domain.Entities;

namespace WMS.Domain.Repositories
{
    public class WmsBranchWarehouseProductRepository : BaseRepository<WmsBranchWarehouseProduct>, IWmsBranchWarehouseProductRepository
    {
        private readonly AppDbContext _context;

        public WmsBranchWarehouseProductRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }

        public decimal GetCostPriceByProductId(Guid productId, Guid warehouseId)
        {
            var listProduct = _context.WmsBranchImportProducts.Include(x => x.WmsBranchImport)
                .Where(x => x.WmsBranchImport.ImportStatus == ImportStatusConstant.Imported
                && x.ProductId.Equals(productId) && x.WmsBranchImport.WarehouseId.Equals(warehouseId)).ToList();
            var groups = listProduct.GroupBy(x => x.ImportId);
            decimal sum = 0;
            foreach (var g in groups)
            {
                sum += g.First().CostPrice;
            }
            return sum / groups.Count();
        }

        public async Task<IEnumerable<WmsProduct>> GetProductListPaging(Guid warehouseId, int? categoryId, string? searchWord, int currentPage, int pageSize)
        {
            var data = _context.Set<WmsBranchWarehouseProduct>()
                .Include(bwp => bwp.WmsProduct).ThenInclude(p => p.Category)
                .Where(bwp => bwp.WarehouseId == warehouseId).AsQueryable();
            if (!categoryId.Equals(null) && !categoryId.Equals(Guid.Empty))
            {
                data = data.Where(x => x.WmsProduct.CategoryId.Equals(categoryId));
            }

            if (!string.IsNullOrEmpty(searchWord))
            {
                data = data.Where(bi => _context.Unaccent(bi.WmsProduct.ProductName).ToLower().Contains(_context.Unaccent(searchWord).ToLower()) ||
                                            _context.Unaccent(bi.WmsProduct.SKU).ToLower().Contains(_context.Unaccent(searchWord).ToLower()));
            }

            var result = await data.AsNoTracking()
                .OrderByDescending(x => x.WmsProduct.CreateDate)
                .Skip(currentPage)
                .Take(pageSize)
                .ToListAsync().ConfigureAwait(false);

            return result.Select(bwp => new WmsProduct()
            {
                ProductId = bwp.WmsProduct.ProductId,
                CategoryId = bwp.WmsProduct.CategoryId,
                SKU = bwp.WmsProduct.SKU,
                ProductName = bwp.WmsProduct.ProductName,
                ProductDescription = bwp.WmsProduct.ProductDescription,
                ProductImage = bwp.WmsProduct.ProductImage,
                Visible = bwp.WmsProduct.Visible,
                UnitPrice = bwp.WmsProduct.ExportPrice,
                CostPrice = bwp.CostPrice,
                ExportPrice = bwp.ExportPrice,
                Status = bwp.WmsProduct.Status,
                Category = bwp.WmsProduct.Category,
                UnitType = bwp.WmsProduct.UnitType
            });
        }
    }
}
