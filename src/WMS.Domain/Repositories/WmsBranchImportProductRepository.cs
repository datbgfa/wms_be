﻿using Microsoft.EntityFrameworkCore;
using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Constant;
using WMS.Domain.Entities;

namespace WMS.Domain.Repositories
{
    public class WmsBranchImportProductRepository : BaseRepository<WmsBranchImportProduct>, IWmsBranchImportProductRepository
    {
        private readonly AppDbContext _context;

        public WmsBranchImportProductRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<IEnumerable<WmsBranchImportProduct>> GetBranchImportProductByImportIdAndProductId(Guid branchImportId, Guid productId)
        {
            var result = await _context.WmsBranchImportProducts.Where(bip => bip.ImportId.Equals(branchImportId) && productId.Equals(bip.ProductId))
                .ToListAsync()
                .ConfigureAwait(false);
            return result;
        }

        public int GetBranchQuantityByProductId(Guid productId, Guid warehouseId)
        {
            var listProduct = _context.WmsBranchImportProducts.Include(x => x.WmsBranchImport)
                .Where(x => x.StockQuantity > 0
                && x.ProductId.Equals(productId) && x.WmsBranchImport!.WarehouseId.Equals(warehouseId));
            return listProduct.Sum(x => x.StockQuantity);
        }

        public IQueryable<WmsBranchImportProduct> GetBranchStockOrderByImportDate(Guid productId, Guid warehouseId)
        {
            return _context.WmsBranchImportProducts.Include(x => x.WmsBranchImport)
                .Where(x => x.ProductId.Equals(productId) && x.WmsBranchImport!.WarehouseId.Equals(warehouseId)
                && x.StockQuantity > 0 && x.WmsBranchImport.ImportStatus == ImportStatusConstant.Imported)
                .OrderBy(x => x.WmsBranchImport!.ImportedDate);
        }

        //public IQueryable<WmsBranchImportProduct> GetByConditions(Guid warehouseId, Guid productId)
        //{
        //    var result = from wbip in _context.WmsBranchImportProducts
        //                 join wbi in _context.WmsBranchImports on wbip.ImportId equals wbi.ImportId
        //                 where wbip.ProductId == productId && wbi.WarehouseId == warehouseId && wbi.ImportStatus == 1
        //                 select wbip;
        //    return result;
        //}
    }
}
