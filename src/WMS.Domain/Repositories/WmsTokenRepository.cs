using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Entities;

namespace WMS.Domain.Repositories
{
    public class WmsTokenRepository : BaseRepository<WmsToken>, IWmsTokenRepository
    {
        private readonly AppDbContext _context;

        public WmsTokenRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }
    }
}
