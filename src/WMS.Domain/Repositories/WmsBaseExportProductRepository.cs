﻿using Microsoft.EntityFrameworkCore;
using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Entities;

namespace WMS.Domain.Repositories
{
    public class WmsBaseExportProductRepository : BaseRepository<WmsBaseExportProduct>, IWmsBaseExportProductRepository
    {
        private readonly AppDbContext _context;

        public WmsBaseExportProductRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }
        public async Task<List<WmsBaseExportProduct>> GetBaseExportProductByExportIdProductIdAndListBaseImportId(Guid exportId, Guid productId, IEnumerable<Guid> baseImportId)
        {
            var result = await _context.Set<WmsBaseExportProduct>().Include(bep => bep.WmsBaseImport)
                                                              .Where(bep => bep.ProductId.Equals(productId) &&
                                                                            bep.ExportId.Equals(exportId) &&
                                                                            baseImportId.Contains(bep.BaseImportId))
                                                              .ToListAsync().ConfigureAwait(false);
            return result;
        }
    }
}
