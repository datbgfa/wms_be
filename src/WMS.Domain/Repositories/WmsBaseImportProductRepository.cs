﻿using Microsoft.EntityFrameworkCore;
using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Constant;
using WMS.Domain.Entities;

namespace WMS.Domain.Repositories
{
    public class WmsBaseImportProductRepository : BaseRepository<WmsBaseImportProduct>, IWmsBaseImportProductRepository
    {
        private readonly AppDbContext _context;

        public WmsBaseImportProductRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }

        public IQueryable<WmsBaseImportProduct> GetBaseStockOrderByImportDate(Guid productId)
        {
            return _context.WmsBaseImportProducts.Include(x => x.WmsBaseImport)
                .Where(x => x.ProductId.Equals(productId) && x.StockQuantity > 0 && x.WmsBaseImport.ImportStatus == ImportStatusConstant.Imported)
                .OrderBy(x => x.WmsBaseImport.ImportedDate);
        }

        //public IQueryable<WmsBaseImportProduct> GetByConditions(Guid productId)
        //{
        //    var result = from wbip in _context.WmsBaseImportProducts
        //                 join wbi in _context.WmsBaseImports on wbip.ImportId equals wbi.ImportId
        //                 where wbip.ProductId == productId && wbi.ImportStatus == 1
        //                 select wbip;
        //    return result;
        //}
    }
}

