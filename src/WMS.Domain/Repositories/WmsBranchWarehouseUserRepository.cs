﻿using Microsoft.EntityFrameworkCore;
using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Entities;

namespace WMS.Domain.Repositories
{
    public class WmsBranchWarehouseUserRepository : BaseRepository<WmsBranchWarehouseUser>, IWmsBranchWarehouseUserRepository
    {
        private readonly AppDbContext _context;

        public WmsBranchWarehouseUserRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }

        public IQueryable<WmsBranchWarehouseUser> GetBranchWarehouseByUserId(Guid currentUser)
        {
            var result = _context.WmsBranchWarehouseUsers.Include(x => x.WmsUser).Where(x => x.UserId == currentUser).AsQueryable();
            return result;
        }
    }
}
