﻿using Microsoft.EntityFrameworkCore;
using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Constant;
using WMS.Domain.Entities;

namespace WMS.Domain.Repositories
{
    public class WmsBaseProductReturnRepository : BaseRepository<WmsBaseProductReturn>, IWmsBaseProductReturnRepository
    {
        private readonly AppDbContext _context;

        public WmsBaseProductReturnRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<List<WmsBaseProductReturn>> GetProductReturnToSupplier(int skip, int take)
        {
            var data = await _context.WmsBaseProductReturns
                .Include(x => x.WmsBaseProductReturnDetails!)
                .ThenInclude(x => x.WmsProduct)
                .OrderByDescending(x => x.CreateDate)
                .Skip(skip)
                .Take(take)
                .ToListAsync();

            return data;
        }

        public async Task<List<WmsBaseProductReturn>> GetProductReturnToSupplierByImportId(Guid importId)
        {
            var data = await _context.WmsBaseProductReturns
                .Include(x => x.WmsBaseProductReturnDetails!)
                .ThenInclude(x => x.WmsProduct)
                .Where(x => x.ImportId.Equals(importId))
                .OrderByDescending(x => x.CreateDate)
                .ToListAsync();

            return data;
        }

        public async Task<int> CountProductReturnToSupplierByImportId(Guid importId)
        {
            var count = await _context.WmsBaseProductReturns
                .Where(x => x.ImportId.Equals(importId))
                .CountAsync();

            return count;
        }

        public async Task<List<WmsBaseProductReturn>> GetProductReturnBySupplierId(int supplierId)
        {
            var query = from baseReturn in _context.WmsBaseProductReturns.AsNoTracking()
                        join baseImport in _context.WmsBaseImports.AsNoTracking()
                            on baseReturn.ImportId equals baseImport.ImportId
                        where baseImport.SupplierId == supplierId
                        && baseReturn.PaymentStatus != PaymentStatusConstant.Paid
                        && baseReturn.Status == ProductReturnStatusConstant.Returned
                        orderby baseReturn.CreateDate ascending
                        select baseReturn;

            return await query.ToListAsync().ConfigureAwait(false);
        }
    }
}
