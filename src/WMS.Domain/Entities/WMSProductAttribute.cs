﻿using WMS.Domain.Entities.Common;

namespace WMS.Domain.Entities
{
    public class WmsProductAttribute : ISoftStatusTracking
    {
        public Guid PavId { get; set; }
        public Guid ProductId { get; set; }
        public int AttributeId { get; set; }
        public string Value { get; set; } = null!;
        public bool? IsDelete { get; set; }

        public WmsProductAttribute()
        {
        }

        public virtual WmsProduct? Product { get; set; }
        public virtual WmsAttribute? Attribute { get; set; }
    }
}
