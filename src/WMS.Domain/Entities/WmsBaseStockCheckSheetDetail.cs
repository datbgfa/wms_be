﻿namespace WMS.Domain.Entities
{
    public class WmsBaseStockCheckSheetDetail
    {
        public Guid CheckId { get; set; }
        public Guid ProductId { get; set; }
        public Guid ImportId { get; set; }
        public int RealQuantity { get; set; }
        public int BaseQuantity { get; set; }

        public virtual WmsBaseStockCheckSheet? WmsBaseStockCheckSheet { get; set; }
        public virtual WmsBaseImport? WmsBaseImport { get; set; }
        public virtual WmsProduct? WmsProduct { get; set; }
    }
}
