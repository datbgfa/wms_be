﻿using WMS.Domain.Entities.Common;

namespace WMS.Domain.Entities
{
    public class WmsBaseProductReturn : IDateTracking
    {
        public Guid ProductReturnId { get; set; }
        public string? Description { get; set; }
        public short Status { get; set; }
        public Guid ImportId { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal PaidAmount { get; set; }
        public short PaymentStatus { get; set; }
        public string ReturnCode { get; set; } = null!;

        public string CreateUser { get; set; } = null!;
        public DateTime CreateDate { get; set; }
        public string UpdateUser { get; set; } = null!;
        public DateTime UpdateDate { get; set; }

        public virtual ICollection<WmsBaseProductReturnDetail>? WmsBaseProductReturnDetails { get; set; }
        public virtual ICollection<WmsBaseInvoice>? WmsBaseInvoices { get; set; }
        public virtual WmsBaseImport? WmsBaseImport { get; set; }

        public WmsBaseProductReturn()
        {
        }

        public WmsBaseProductReturn(Guid productReturnId,
            string? description,
            short status,
            Guid importId,
            decimal totalPrice,
            decimal paidAmount,
            short paymentStatus,
            string createUser,
            DateTime createDate,
            string updateUser,
            DateTime updateDate)
        {
            ProductReturnId = productReturnId;
            Description = description;
            Status = status;
            ImportId = importId;
            TotalPrice = totalPrice;
            PaidAmount = paidAmount;
            PaymentStatus = paymentStatus;
            CreateUser = createUser;
            CreateDate = createDate;
            UpdateUser = updateUser;
            UpdateDate = updateDate;
        }
    }
}
