﻿using WMS.Domain.Entities.Common;

namespace WMS.Domain.Entities
{
    public class WmsBranchWarehouseUser : ISoftStatusTracking
    {
        public Guid UserId { get; set; }
        public Guid BranchWarehouseId { get; set; }
        public bool? IsDelete { get; set; }

        public virtual WmsBranchWarehouse? WmsBranchWarehouse { get; set; }
        public virtual WmsUser? WmsUser { get; set; }

        public WmsBranchWarehouseUser()
        {

        }

        public WmsBranchWarehouseUser(Guid userId, Guid branchWarehouseId, bool? isDelete)
        {
            UserId = userId;
            BranchWarehouseId = branchWarehouseId;
            IsDelete = isDelete;
        }
    }
}
