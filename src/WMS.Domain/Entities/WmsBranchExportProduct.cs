﻿namespace WMS.Domain.Entities
{
    public class WmsBranchExportProduct
    {
        #region PrimaryKey
        public Guid ExportId { get; set; }
        public Guid ProductId { get; set; }
        public Guid ImportId { get; set; }
        #endregion
        public decimal UnitPrice { get; set; }
        public int Quantity { get; set; }

        #region Navigation
        public virtual WmsBranchExport? WmsBranchExport { get; set; }
        public virtual WmsProduct? WmsProduct { get; set; }
        public virtual WmsBranchImport? WmsBranchImport { get; set; }
        #endregion
    }
}
