﻿namespace WMS.Domain.Entities
{
    public class WmsBaseProductReturnDetail
    {
        public Guid BprdId { get; set; }
        public Guid ProductReturnId { get; set; }
        public Guid ImportId { get; set; }
        public Guid ProductId { get; set; }
        public decimal CostPrice { get; set; }
        public decimal UnitPrice { get; set; }
        public int RequestQuantity { get; set; }

        #region Navigation
        public virtual WmsBaseProductReturn? WmsBaseProductReturn { get; set; }
        public virtual WmsBaseImport? WmsBaseImport { get; set; }
        public virtual WmsProduct? WmsProduct { get; set; }
        #endregion
    }
}
