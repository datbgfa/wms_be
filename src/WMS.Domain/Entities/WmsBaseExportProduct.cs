﻿namespace WMS.Domain.Entities
{
    public class WmsBaseExportProduct
    {
        #region Primary Key
        public Guid ExportId { get; set; }
        public Guid ProductId { get; set; }
        public Guid BaseImportId { get; set; }
        #endregion
        public decimal UnitPrice { get; set; }
        public int Quantity { get; set; }
        public string ExportCodeItem { get; set; } = null!;


        #region Navigation
        public virtual WmsBaseExport? WmsBaseExport { get; set; }
        public virtual WmsProduct? WmsProduct { get; set; }
        public virtual WmsBaseImport? WmsBaseImport { get; set; }
        #endregion
    }
}
