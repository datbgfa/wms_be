﻿namespace WMS.Domain.Entities
{
    public class WmsCategory
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; } = null!;
        public bool? Visible { get; set; }

        public WmsCategory()
        {
        }

        public virtual ICollection<WmsCategoryAttribute>? CategoryAttributes { get; set; }
        public virtual ICollection<WmsProduct>? WmsProducts { get; set; }
    }
}
