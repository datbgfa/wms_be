﻿using WMS.Domain.Entities.Common;

namespace WMS.Domain.Entities
{
    public class WmsBranchInvoice : IDateTracking
    {
        #region PrimaryKey
        public Guid InvoiceId { get; set; }
        #endregion
        public Guid? DebtId { get; set; }
        public Guid? ImportId { get; set; }
        public Guid? ExportId { get; set; }
        public Guid? ReturnId { get; set; }
        public short Type { get; set; }
        public decimal Amount { get; set; }
        public string? Description { get; set; }
        public decimal BalanceChange { get; set; }
        public string InvoiceCode { get; set; } = null!;

        public string CreateUser { get; set; } = null!;
        public DateTime CreateDate { get; set; }
        public string UpdateUser { get; set; } = null!;
        public DateTime UpdateDate { get; set; }

        #region Navigation
        public virtual WmsBranchDebt? WmsBranchDebt { get; set; }
        public virtual WmsBranchImport? WmsBranchImport { get; set; }
        public virtual WmsBranchExport? WmsBranchExport { get; set; }
        public virtual WmsBranchProductReturn? WmsBranchProductReturn { get; set; }
        #endregion
    }
}
