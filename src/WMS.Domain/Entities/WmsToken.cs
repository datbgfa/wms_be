﻿namespace WMS.Domain.Entities
{
    public class WmsToken
    {
        public Guid UserId { get; set; }
        public string Token { get; set; }
        public string CurrentAccessToken { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ExpriedDate { get; set; }

        public WmsUser? User { get; set; }

        public WmsToken()
        {
        }

        public WmsToken(Guid userId, string token, string currentAccessToken, DateTime createDate, DateTime expriedDate)
        {
            UserId = userId;
            Token = token;
            CurrentAccessToken = currentAccessToken;
            CreateDate = createDate;
            ExpriedDate = expriedDate;
        }
    }
}
