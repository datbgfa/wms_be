﻿using WMS.Domain.Entities.Common;

namespace WMS.Domain.Entities
{
    public class WmsBranchWarehouseProduct : ISoftStatusTracking
    {
        public Guid BpId { get; set; }
        public Guid ProductId { get; set; }
        public Guid WarehouseId { get; set; }
        public bool? IsDelete { get; set; }
        public decimal CostPrice { get; set; }
        public decimal ExportPrice { get; set; }

        public virtual WmsProduct? WmsProduct { get; set; }
        public virtual WmsBranchWarehouse? Warehouse { get; set; }
    }
}
