﻿using WMS.Domain.Entities.Common;

namespace WMS.Domain.Entities
{
    public class WmsBranchProductReturn : IDateTracking
    {
        public Guid ProductReturnId { get; set; }
        public Guid ImportId { get; set; }
        public Guid? BaseExportId { get; set; }
        public string? Description { get; set; }
        public short Status { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal PaidAmount { get; set; }
        public short PaymentStatus { get; set; }
        public string ReturnCode { get; set; } = null!;

        public string CreateUser { get; set; } = null!;
        public DateTime CreateDate { get; set; }
        public string UpdateUser { get; set; } = null!;
        public DateTime UpdateDate { get; set; }

        public virtual ICollection<WmsBranchProductReturnDetail>? WmsBranchProductReturnDetails { get; set; }
        public virtual ICollection<WmsBranchInvoice>? WmsBranchInvoices { get; set; }
        public virtual ICollection<WmsBaseInvoice>? WmsBaseInvoices { get; set; }
        public virtual WmsBranchImport? WmsBranchImport { get; set; }
        public virtual WmsBaseExport? WmsBaseExport { get; set; }

        public WmsBranchProductReturn()
        {
        }

        public WmsBranchProductReturn(Guid productReturnId,
            Guid importId,
            Guid? baseExportId,
            string? description,
            short status,
            decimal totalPrice,
            decimal paidAmount,
            short paymentStatus,
            string createUser,
            DateTime createDate,
            string updateUser,
            DateTime updateDate)
        {
            ProductReturnId = productReturnId;
            ImportId = importId;
            BaseExportId = baseExportId;
            Description = description;
            Status = status;
            TotalPrice = totalPrice;
            PaidAmount = paidAmount;
            PaymentStatus = paymentStatus;
            CreateUser = createUser;
            CreateDate = createDate;
            UpdateUser = updateUser;
            UpdateDate = updateDate;
        }

        public WmsBranchProductReturn(Guid productReturnId, Guid importId, Guid? baseExportId, string? description, short status, decimal totalPrice, decimal paidAmount, short paymentStatus, string returnCode, string createUser, DateTime createDate, string updateUser, DateTime updateDate)
        {
            ProductReturnId = productReturnId;
            ImportId = importId;
            BaseExportId = baseExportId;
            Description = description;
            Status = status;
            TotalPrice = totalPrice;
            PaidAmount = paidAmount;
            PaymentStatus = paymentStatus;
            ReturnCode = returnCode;
            CreateUser = createUser;
            CreateDate = createDate;
            UpdateUser = updateUser;
            UpdateDate = updateDate;
        }
    }
}
