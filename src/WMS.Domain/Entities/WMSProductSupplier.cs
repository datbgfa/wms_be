﻿using WMS.Domain.Entities.Common;

namespace WMS.Domain.Entities
{
    public class WmsProductSupplier : ISoftStatusTracking
    {
        public Guid ProductId { get; set; }
        public int SupplierId { get; set; }
        public bool? IsDelete { get; set; }

        public WmsProductSupplier()
        {
        }

        public virtual WmsProduct? Product { get; set; }
        public virtual WmsSupplier? Supplier { get; set; }
    }
}
