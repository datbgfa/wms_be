﻿using WMS.Domain.Entities.Common;

namespace WMS.Domain.Entities
{
    public class WmsProduct : BaseEntity
    {
        public Guid ProductId { get; set; }
        public int CategoryId { get; set; }
        public string SKU { get; set; } = null!;
        public string ProductName { get; set; } = null!;
        public string? ProductDescription { get; set; }
        public string ProductImage { get; set; } = null!;
        public bool? Visible { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal CostPrice { get; set; }
        public decimal ExportPrice { get; set; }
        public string UnitType { get; set; }
        public short Status { get; set; }

        public WmsProduct()
        {
        }

        public WmsProduct(Guid productId, int categoryId, string sKU, string productName, string? productDescription, string productImage, bool? visible, decimal unitPrice, decimal salePrice, decimal exportPrice, string unitType, short status)
        {
            ProductId = productId;
            CategoryId = categoryId;
            SKU = sKU;
            ProductName = productName;
            ProductDescription = productDescription;
            ProductImage = productImage;
            Visible = visible;
            UnitPrice = unitPrice;
            CostPrice = salePrice;
            ExportPrice = exportPrice;
            UnitType = unitType;
            Status = status;
        }

        public virtual ICollection<WmsBranchExportProduct>? WmsBranchExportProducts { get; set; }
        public virtual ICollection<WmsBranchImportProduct>? WmsBranchImportProducts { get; set; }
        public virtual ICollection<WmsBranchProductRequest>? WmsBranchProductRequests { get; set; }
        public virtual ICollection<WmsBranchProductReturnDetail>? WmsBranchProductReturnDetails { get; set; }
        public virtual ICollection<WmsBranchStockCheckSheetDetail>? WmsBranchStockCheckSheetDetails { get; set; }

        public virtual ICollection<WmsBaseExportProduct>? WmsBaseExportProducts { get; set; }
        public virtual ICollection<WmsBaseImportProduct>? WmsBaseImportProducts { get; set; }
        public virtual ICollection<WmsBaseProductReturnDetail>? WmsBaseProductReturnDetails { get; set; }
        public virtual ICollection<WmsBaseStockCheckSheetDetail>? WmsBaseStockCheckSheetDetails { get; set; }

        public virtual ICollection<WmsProductSupplier>? ProductSuppliers { get; set; }
        public virtual ICollection<WmsProductAttribute>? ProductAttributes { get; set; }

        public virtual ICollection<WmsBranchWarehouseProduct>? WmsBranchWarehouseProducts { get; set; }

        public virtual WmsCategory? Category { get; set; }
    }
}
