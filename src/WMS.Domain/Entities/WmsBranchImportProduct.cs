﻿namespace WMS.Domain.Entities
{
    public class WmsBranchImportProduct
    {
        #region PrimaryKey
        public Guid ImportId { get; set; }
        public Guid BaseImportId { get; set; }
        public Guid ProductId { get; set; }
        #endregion
        public decimal CostPrice { get; set; }
        public decimal UnitPrice { get; set; }
        public int ImportedQuantity { get; set; }
        public int StockQuantity { get; set; }
        public string ExportCodeItem { get; set; } = null!;


        #region Navigation
        public virtual WmsBranchImport? WmsBranchImport { get; set; }
        public virtual WmsBaseImport? WmsBaseImport { get; set; }
        public virtual WmsProduct? WmsProduct { get; set; }
        #endregion
    }
}
