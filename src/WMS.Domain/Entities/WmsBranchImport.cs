﻿using WMS.Domain.Entities.Common;

namespace WMS.Domain.Entities
{
    public class WmsBranchImport : IDateTracking
    {
        public Guid ImportId { get; set; }
        public Guid WarehouseId { get; set; } //branch warehouse
        public Guid RequestId { get; set; }
        #region Unique
        public string ImportCode { get; set; } = null!;
        #endregion
        public Guid? AssignBy { get; set; }
        public Guid? ImportBy { get; set; }
        public Guid BaseWarehouseId { get; set; }
        public DateTime? ImportedDate { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal ExtraCost { get; set; }
        public decimal PaidAmount { get; set; }
        public decimal TotalCost { get; set; }
        public string? Note { get; set; }
        public short ImportStatus { get; set; }
        public short PaymentStatus { get; set; }
        public Guid BaseExportId { get; set; }
        public string ExportCode { get; set; } = null!;

        public string CreateUser { get; set; } = null!;
        public DateTime CreateDate { get; set; }
        public string UpdateUser { get; set; } = null!;
        public DateTime UpdateDate { get; set; }

        public virtual ICollection<WmsBranchProductReturnDetail>? WmsBranchProductReturnDetails { get; set; }
        public virtual ICollection<WmsBranchImportProduct>? WmsBranchImportProducts { get; set; }
        public virtual ICollection<WmsBranchStockCheckSheetDetail>? WmsBranchStockCheckSheetDetails { get; set; }
        public virtual ICollection<WmsBranchInvoice>? WmsBranchInvoices { get; set; }
        public virtual ICollection<WmsBranchProductReturn>? WmsBranchProductReturns { get; set; }
        public virtual ICollection<WmsBranchExportProduct>? WmsBranchExportProducts { get; set; }
        public virtual WmsBranchRequest? WmsBranchRequest { get; set; }
        public virtual WmsUser? UserAssignBy { get; set; }
        public virtual WmsUser? UserImportBy { get; set; }
        public virtual WmsBranchWarehouse? WmsBranchWarehouse { get; set; }
        public virtual WmsBaseWarehouse? WmsBaseWarehouse { get; set; }
        public virtual WmsBaseExport? WmsBaseExport { get; set; }

        public WmsBranchImport()
        {
        }

        public WmsBranchImport(Guid importId, Guid warehouseId,
            Guid requestId, string importCode,
            Guid? assignBy, Guid? importBy,
            Guid baseWarehouseId, DateTime? importedDate,
            decimal totalPrice, decimal extraCost,
            decimal paidAmount, decimal totalCost,
            string? note, short importStatus,
            short paymentStatus, Guid baseExportId,
            string createUser, DateTime createDate,
            string updateUser, DateTime updateDate)
        {
            ImportId = importId;
            WarehouseId = warehouseId;
            RequestId = requestId;
            ImportCode = importCode;
            AssignBy = assignBy;
            ImportBy = importBy;
            BaseWarehouseId = baseWarehouseId;
            ImportedDate = importedDate;
            TotalPrice = totalPrice;
            ExtraCost = extraCost;
            PaidAmount = paidAmount;
            TotalCost = totalCost;
            Note = note;
            ImportStatus = importStatus;
            PaymentStatus = paymentStatus;
            BaseExportId = baseExportId;
            CreateUser = createUser;
            CreateDate = createDate;
            UpdateUser = updateUser;
            UpdateDate = updateDate;
        }

        public WmsBranchImport(Guid importId, Guid warehouseId, Guid requestId, string importCode, Guid? assignBy, Guid? importBy, Guid baseWarehouseId, DateTime? importedDate, decimal totalPrice, decimal extraCost, decimal paidAmount, decimal totalCost, string? note, short importStatus, short paymentStatus, Guid baseExportId, string exportCode, string createUser, DateTime createDate, string updateUser, DateTime updateDate)
        {
            ImportId = importId;
            WarehouseId = warehouseId;
            RequestId = requestId;
            ImportCode = importCode;
            AssignBy = assignBy;
            ImportBy = importBy;
            BaseWarehouseId = baseWarehouseId;
            ImportedDate = importedDate;
            TotalPrice = totalPrice;
            ExtraCost = extraCost;
            PaidAmount = paidAmount;
            TotalCost = totalCost;
            Note = note;
            ImportStatus = importStatus;
            PaymentStatus = paymentStatus;
            BaseExportId = baseExportId;
            ExportCode = exportCode;
            CreateUser = createUser;
            CreateDate = createDate;
            UpdateUser = updateUser;
            UpdateDate = updateDate;
        }
    }
}
