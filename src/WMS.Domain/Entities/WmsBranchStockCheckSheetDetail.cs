﻿namespace WMS.Domain.Entities
{
    public class WmsBranchStockCheckSheetDetail
    {
        public Guid CheckId { get; set; }
        public Guid ProductId { get; set; }
        public Guid ImportId { get; set; }
        public Guid BaseImportId { get; set; }
        public int RealQuantity { get; set; }
        public int BranchQuantity { get; set; }

        public virtual WmsBaseImport? WmsBaseImport { get; set; }
        public virtual WmsBranchStockCheckSheet? WmsBranchStockCheckSheet { get; set; }
        public virtual WmsBranchImport? WmsBranchImport { get; set; }
        public virtual WmsProduct? WmsProduct { get; set; }
    }
}
