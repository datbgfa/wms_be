﻿namespace WMS.Domain.Entities.Common
{
    public interface ISoftStatusTracking
    {
        public bool? IsDelete { get; set; }
    }
}
