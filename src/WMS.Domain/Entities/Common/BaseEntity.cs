﻿namespace WMS.Domain.Entities.Common
{
    public class BaseEntity : IDateTracking, ISoftStatusTracking
    {
        public bool? IsDelete { get; set; }
        public string CreateUser { get; set; } = string.Empty!;
        public DateTime CreateDate { get; set; }
        public string UpdateUser { get; set; } = string.Empty!;
        public DateTime UpdateDate { get; set; }
    }
}
