﻿namespace WMS.Domain.Entities
{
    public class WmsBranchDebt
    {
        public Guid DebtId { get; set; }
        public Guid CreditorId { get; set; }
        public Guid DebtorId { get; set; }
        public decimal DebtAmount { get; set; }

        public virtual WmsBranchWarehouse? Debtor { get; set; }
        public virtual WmsBaseWarehouse? Creditor { get; set; }
        public virtual ICollection<WmsBranchInvoice>? WmsBranchInvoices { get; set; }
        public virtual ICollection<WmsBaseInvoice>? WmsBaseInvoices { get; set; }
    }
}
