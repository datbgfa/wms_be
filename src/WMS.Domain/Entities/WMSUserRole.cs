﻿using WMS.Domain.Entities.Common;

namespace WMS.Domain.Entities
{
    public class WmsUserRole : ISoftStatusTracking
    {
        public Guid UserRoleId { get; set; }
        public int RoleId { get; set; }
        public Guid UserId { get; set; }
        public bool? IsDelete { get; set; }

        public virtual WmsUser? User { get; set; }
        public virtual WmsRole? Role { get; set; }

        public WmsUserRole()
        {
        }

        public WmsUserRole(Guid userRoleId, int roleId, Guid userId, bool? isDelete)
        {
            UserRoleId = userRoleId;
            RoleId = roleId;
            UserId = userId;
            IsDelete = isDelete;
        }
    }
}
