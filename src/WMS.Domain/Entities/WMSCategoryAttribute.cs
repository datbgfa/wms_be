﻿using WMS.Domain.Entities.Common;

namespace WMS.Domain.Entities
{
    public class WmsCategoryAttribute : ISoftStatusTracking
    {
        public Guid CaId { get; set; }
        public int CategoryId { get; set; }
        public int AttributeId { get; set; }
        public bool? IsDelete { get; set; }

        public WmsCategoryAttribute()
        {
        }

        public virtual WmsCategory? Category { get; set; }
        public virtual WmsAttribute? Attribute { get; set; }
    }
}
