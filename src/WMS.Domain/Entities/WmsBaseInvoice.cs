﻿using WMS.Domain.Entities.Common;

namespace WMS.Domain.Entities
{
    public class WmsBaseInvoice : IDateTracking
    {
        #region PrimaryKey
        public Guid InvoiceId { get; set; }
        #endregion
        public short Type { get; set; }
        public decimal Amount { get; set; }
        public string? Description { get; set; }
        public Guid? ImportId { get; set; }
        public Guid? ReturnId { get; set; }
        public Guid? ExportId { get; set; }
        public Guid? BranchDebtId { get; set; }
        public Guid? DebtId { get; set; }
        public Guid? BranchProductReturnId { get; set; }
        public decimal BalanceChange { get; set; }
        public string InvoiceCode { get; set; } = null!;

        public string CreateUser { get; set; } = null!;
        public DateTime CreateDate { get; set; }
        public string UpdateUser { get; set; } = null!;
        public DateTime UpdateDate { get; set; }

        public virtual WmsBaseDebt? WmsBaseDebt { get; set; }
        public virtual WmsBaseImport? WmsBaseImport { get; set; }
        public virtual WmsBaseProductReturn? WmsBaseProductReturn { get; set; }
        public virtual WmsBaseExport? WmsBaseExport { get; set; }
        public virtual WmsBranchDebt? WmsBranchDebt { get; set; }
        public virtual WmsBranchProductReturn? WmsBranchProductReturn { get; set; }
    }
}
