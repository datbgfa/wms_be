﻿namespace WMS.Domain.Entities
{
    public class WmsBaseWarehouseUser
    {
        public Guid UserId { get; set; }
        public Guid BaseWarehouseId { get; set; }
        public bool? IsDelete { get; set; }

        public virtual WmsBaseWarehouse? WmsBaseWarehouse { get; set; }
        public virtual WmsUser? WmsUser { get; set; }

        public WmsBaseWarehouseUser()
        {

        }

        public WmsBaseWarehouseUser(Guid userId, Guid baseWarehouseId, bool? isDelete)
        {
            UserId = userId;
            BaseWarehouseId = baseWarehouseId;
            IsDelete = isDelete;
        }
    }
}
