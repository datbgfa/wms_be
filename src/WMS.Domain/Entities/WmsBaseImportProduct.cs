﻿namespace WMS.Domain.Entities
{
    public class WmsBaseImportProduct
    {
        public Guid ImportId { get; set; }
        public Guid ProductId { get; set; }
        public decimal CostPrice { get; set; }
        public decimal UnitPrice { get; set; }
        public int ImportedQuantity { get; set; }
        public int StockQuantity { get; set; }

        public virtual WmsBaseImport? WmsBaseImport { get; set; }
        public virtual WmsProduct? WmsProduct { get; set; }
    }
}
