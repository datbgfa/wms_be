﻿using WMS.Domain.Entities.Common;

namespace WMS.Domain.Entities
{
    public class WmsBaseImport : IDateTracking
    {
        #region PrimaryKey
        public Guid ImportId { get; set; }
        public Guid WarehouseId { get; set; }
        #endregion
        #region Unique
        public string ImportCode { get; set; } = null!;
        #endregion
        public Guid AssignBy { get; set; }
        public Guid ImportBy { get; set; }
        public int SupplierId { get; set; }
        public DateTime? ImportedDate { get; set; }
        public DateTime? RequestDate { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal ExtraCost { get; set; }
        public decimal TotalCost { get; set; }
        public decimal PaidAmount { get; set; }
        public string? Note { get; set; }
        public short ImportStatus { get; set; }
        public short PaymentStatus { get; set; }

        public string CreateUser { get; set; } = null!;
        public DateTime CreateDate { get; set; }
        public string UpdateUser { get; set; } = null!;
        public DateTime UpdateDate { get; set; }

        #region Navigation
        public virtual ICollection<WmsBranchStockCheckSheetDetail>? WmsBranchStockCheckSheetDetails { get; set; }
        public virtual ICollection<WmsBaseProductReturnDetail>? WmsBaseProductReturnDetails { get; set; }
        public virtual ICollection<WmsBaseStockCheckSheetDetail>? WmsBaseStockCheckSheetDetails { get; set; }
        public virtual ICollection<WmsBaseImportProduct>? WmsBaseImportProducts { get; set; }
        public virtual ICollection<WmsBaseProductReturn>? WmsBaseProductReturns { get; set; }
        public virtual ICollection<WmsBaseInvoice>? WmsBaseInvoices { get; set; }
        public virtual ICollection<WmsBranchImportProduct>? WmsBranchImportProducts { get; set; }
        public virtual ICollection<WmsBranchProductReturnDetail>? WmsBranchProductReturnDetails { get; set; }
        public virtual ICollection<WmsBaseExportProduct>? WmsBaseExportProducts { get; set; }
        public virtual WmsSupplier? WmsSupplier { get; set; }
        public virtual WmsUser? UserAssignBy { get; set; }
        public virtual WmsUser? UserImportBy { get; set; }
        public virtual WmsBaseWarehouse? WmsBaseWarehouse { get; set; }


        #endregion

        public WmsBaseImport()
        {
        }

        public WmsBaseImport(Guid importId, Guid warehouseId, string importCode, Guid assignBy, Guid importBy, int supplierId, DateTime? importedDate, DateTime? requestDate, decimal totalPrice, decimal extraCost, decimal totalCost, decimal paidAmount, string? note, short importStatus, short paymentStatus, string createUser, DateTime createDate, string updateUser, DateTime updateDate)
        {
            ImportId = importId;
            WarehouseId = warehouseId;
            ImportCode = importCode;
            AssignBy = assignBy;
            ImportBy = importBy;
            SupplierId = supplierId;
            ImportedDate = importedDate;
            RequestDate = requestDate;
            TotalPrice = totalPrice;
            ExtraCost = extraCost;
            TotalCost = totalCost;
            PaidAmount = paidAmount;
            Note = note;
            ImportStatus = importStatus;
            PaymentStatus = paymentStatus;
            CreateUser = createUser;
            CreateDate = createDate;
            UpdateUser = updateUser;
            UpdateDate = updateDate;
        }

        public WmsBaseImport(Guid importId,
            Guid warehouseId, Guid assignBy,
            Guid importBy, int supplierId,
            DateTime? importedDate,
            DateTime? requestDate,
            decimal totalPrice,
            decimal extraCost,
            decimal totalCost,
            decimal paidAmount,
            string? note,
            short importStatus,
            short paymentStatus,
            string createUser,
            DateTime createDate,
            string updateUser,
            DateTime updateDate)
        {
            ImportId = importId;
            WarehouseId = warehouseId;
            AssignBy = assignBy;
            ImportBy = importBy;
            SupplierId = supplierId;
            ImportedDate = importedDate;
            RequestDate = requestDate;
            TotalPrice = totalPrice;
            ExtraCost = extraCost;
            TotalCost = totalCost;
            PaidAmount = paidAmount;
            Note = note;
            ImportStatus = importStatus;
            PaymentStatus = paymentStatus;
            CreateUser = createUser;
            CreateDate = createDate;
            UpdateUser = updateUser;
            UpdateDate = updateDate;
        }
    }
}
