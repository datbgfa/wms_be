﻿using WMS.Domain.Entities.Common;

namespace WMS.Domain.Entities
{
    public class WmsBranchRequest : IDateTracking
    {
        public Guid RequestId { get; set; }
        public Guid WarehouseId { get; set; }
        public Guid? ApprovedBy { get; set; }
        public Guid RequestBy { get; set; }
        public string RequestCode { get; set; } = null!;
        public DateTime RequestDate { get; set; }
        public decimal TotalPrice { get; set; }
        public short ApprovalStatus { get; set; }

        public string CreateUser { get; set; } = null!;
        public DateTime CreateDate { get; set; }
        public string UpdateUser { get; set; } = null!;
        public DateTime UpdateDate { get; set; }

        public virtual ICollection<WmsBranchProductRequest>? WmsBranchProductRequests { get; set; }
        public virtual WmsBranchWarehouse? WmsBranchWarehouse { get; set; }
        public virtual WmsUser? UserApprovedBy { get; set; }
        public virtual WmsUser? UserRequestBy { get; set; }
        public virtual WmsBranchImport? WmsBranchImport { get; set; }
        public virtual WmsBaseExport? WmsBaseExport { get; set; }

        public WmsBranchRequest()
        {
        }

        public WmsBranchRequest(Guid requestId, Guid warehouseId,
            Guid? approvedBy, Guid requestBy, string requestCode,
            DateTime requestDate, decimal totalPrice,
            short approvalStatus, string createUser,
            DateTime createDate, string updateUser, DateTime updateDate)
        {
            RequestId = requestId;
            WarehouseId = warehouseId;
            ApprovedBy = approvedBy;
            RequestBy = requestBy;
            RequestCode = requestCode;
            RequestDate = requestDate;
            TotalPrice = totalPrice;
            ApprovalStatus = approvalStatus;
            CreateUser = createUser;
            CreateDate = createDate;
            UpdateUser = updateUser;
            UpdateDate = updateDate;
        }
    }
}
