﻿namespace WMS.Domain.Entities
{
    public class WmsBaseDebt
    {
        #region PrimaryKey
        public Guid DebtId { get; set; }
        #endregion
        public int CreditorId { get; set; }
        public Guid DebtorId { get; set; }
        public decimal DebtAmount { get; set; }

        #region Navigation
        public virtual WmsBaseWarehouse? Debtor { get; set; }
        public virtual WmsSupplier? Creditor { get; set; }
        public virtual ICollection<WmsBaseInvoice>? WmsBaseInvoices { get; set; }
        #endregion
    }
}
