﻿using WMS.Domain.Entities.Common;

namespace WMS.Domain.Entities
{
    public class WmsBranchExport : IDateTracking
    {
        public Guid ExportId { get; set; }
        public Guid WarehouseId { get; set; }
        #region Unique
        public string ExportCode { get; set; } = null!;
        #endregion
        public Guid AssignBy { get; set; }
        public Guid ExportBy { get; set; }
        public DateTime ExportDate { get; set; }
        public decimal TotalPrice { get; set; }
        public short ApprovalStatus { get; set; }
        public string CreateUser { get; set; } = null!;
        public DateTime CreateDate { get; set; }
        public string UpdateUser { get; set; } = null!;
        public DateTime UpdateDate { get; set; }

        public virtual ICollection<WmsBranchExportProduct>? WmsBranchExportProducts { get; set; }
        public virtual ICollection<WmsBranchInvoice>? WmsBranchInvoices { get; set; }
        public virtual WmsBranchWarehouse? WmsBranchWarehouse { get; set; }
        public virtual WmsUser? UserAssign { get; set; }
        public virtual WmsUser? UserExport { get; set; }

    }
}
