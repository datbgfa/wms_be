﻿namespace WMS.Domain.Entities
{
    public class WmsBaseWarehouse
    {
        public Guid WarehouseId { get; set; }
        public Guid? WarehouseOwner { get; set; }
        public string WarehouseName { get; set; } = null!;
        public string PhoneNumber { get; set; } = null!;
        public string Email { get; set; } = null!;
        public string Address { get; set; } = null!;
        public short Status { get; set; }

        public virtual ICollection<WmsBaseDebt>? WmsBaseDebts { get; set; }
        public virtual ICollection<WmsBaseImport>? WmsBaseImports { get; set; }
        public virtual ICollection<WmsBaseExport>? WmsBaseExports { get; set; }
        public virtual ICollection<WmsBaseWarehouseUser>? WmsBaseWarehouseUsers { get; set; }
        public virtual WmsUser? WmsWarehouseOwner { get; set; }
        public virtual ICollection<WmsBranchImport>? WmsBranchImports { get; set; }

        public WmsBaseWarehouse()
        {
        }

        public WmsBaseWarehouse(Guid warehouseId, Guid? warehouseOwner, string warehouseName, string phoneNumber, string email, string address, short status, ICollection<WmsBaseDebt>? wmsBaseDebts, ICollection<WmsBaseImport>? wmsBaseImports, ICollection<WmsBaseExport>? wmsBaseExports, ICollection<WmsBaseWarehouseUser>? wmsBaseWarehouseUsers, WmsUser? wmsWarehouseOwner, ICollection<WmsBranchImport>? wmsBranchImports)
        {
            WarehouseId = warehouseId;
            WarehouseOwner = warehouseOwner;
            WarehouseName = warehouseName;
            PhoneNumber = phoneNumber;
            Email = email;
            Address = address;
            Status = status;
            WmsBaseDebts = wmsBaseDebts;
            WmsBaseImports = wmsBaseImports;
            WmsBaseExports = wmsBaseExports;
            WmsBaseWarehouseUsers = wmsBaseWarehouseUsers;
            WmsWarehouseOwner = wmsWarehouseOwner;
            WmsBranchImports = wmsBranchImports;
        }

        public WmsBaseWarehouse(Guid warehouseId, Guid? warehouseOwner,
            string warehouseName, string phoneNumber,
            string email, string address, short status)
        {
            WarehouseId = warehouseId;
            WarehouseOwner = warehouseOwner;
            WarehouseName = warehouseName;
            PhoneNumber = phoneNumber;
            Email = email;
            Address = address;
            Status = status;
        }
    }
}
