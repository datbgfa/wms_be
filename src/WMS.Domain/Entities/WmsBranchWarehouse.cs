﻿namespace WMS.Domain.Entities
{
    public class WmsBranchWarehouse
    {
        public Guid WarehouseId { get; set; }
        public Guid? WarehouseOwner { get; set; }
        public string WarehouseName { get; set; } = null!;
        public string PhoneNumber { get; set; } = null!;
        public string Email { get; set; } = null!;
        public string Address { get; set; } = null!;
        public short Status { get; set; }
        public string? Note { get; set; }

        public virtual ICollection<WmsBranchRequest>? WmsBranchRequests { get; set; }
        public virtual ICollection<WmsBranchDebt>? WmsBranchDebts { get; set; }
        public virtual ICollection<WmsBranchImport>? WmsBranchImports { get; set; }
        public virtual ICollection<WmsBranchExport>? WmsBranchExports { get; set; }
        public virtual ICollection<WmsBranchWarehouseProduct>? WmsBranchWarehouseProducts { get; set; }
        public virtual ICollection<WmsBranchWarehouseUser>? WmsBranchWarehouseUsers { get; set; }
        public virtual ICollection<WmsBaseExport>? WmsBaseExports { get; set; }
        public virtual WmsUser? WmsWarehouseOwner { get; set; }

        public WmsBranchWarehouse()
        {
        }

        public WmsBranchWarehouse(Guid warehouseId, Guid? warehouseOwner, string warehouseName, string phoneNumber, string email, string address, short status, string? note)
        {
            WarehouseId = warehouseId;
            WarehouseOwner = warehouseOwner;
            WarehouseName = warehouseName;
            PhoneNumber = phoneNumber;
            Email = email;
            Address = address;
            Status = status;
            Note = note;
        }
    }
}
