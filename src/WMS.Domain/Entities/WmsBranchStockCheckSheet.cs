﻿namespace WMS.Domain.Entities
{
    public class WmsBranchStockCheckSheet
    {
        public Guid CheckId { get; set; }
        public Guid CheckBy { get; set; }
        public DateTime CheckDate { get; set; }
        public string? Description { get; set; }
        public short Status { get; set; }
        public string StockCheckSheetCode { get; set; } = null!;

        public virtual ICollection<WmsBranchStockCheckSheetDetail>? WmsBranchStockCheckSheetDetails { get; set; }
        public virtual WmsUser? UserCheck { get; set; }
    }
}
