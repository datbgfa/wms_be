﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.ValueGeneration;

namespace WMS.Domain.Entities.EntitiyConfiguration
{
    public class BranchProductReturnConfiguration : IEntityTypeConfiguration<WmsBranchProductReturn>
    {
        public void Configure(EntityTypeBuilder<WmsBranchProductReturn> builder)
        {
            builder.ToTable("WMS_BranchProductReturn");

            builder.HasKey(f => f.ProductReturnId);

            builder.HasIndex(f => f.ReturnCode)
                .IsUnique();

            builder.Property(f => f.ReturnCode)
                .HasMaxLength(50);

            builder.Property(f => f.ProductReturnId)
                .HasValueGenerator<GuidValueGenerator>();

            builder.Property(f => f.TotalPrice)
                .HasColumnType("money");

            builder.Property(f => f.PaidAmount)
                .HasColumnType("money");

            builder.Property(f => f.Description)
                .HasMaxLength(255);

            builder.HasMany(o => o.WmsBranchProductReturnDetails)
                .WithOne(o => o.WmsBranchProductReturn)
                .HasForeignKey(f => f.ProductReturnId)
                .HasPrincipalKey(f => f.ProductReturnId);

            builder.HasMany(o => o.WmsBranchInvoices)
                .WithOne(o => o.WmsBranchProductReturn)
                .HasForeignKey(f => f.ReturnId)
                .HasPrincipalKey(f => f.ProductReturnId);

            builder.HasMany(o => o.WmsBaseInvoices)
                .WithOne(o => o.WmsBranchProductReturn)
                .HasForeignKey(f => f.BranchProductReturnId)
                .HasPrincipalKey(f => f.ProductReturnId);

        }
    }
}
