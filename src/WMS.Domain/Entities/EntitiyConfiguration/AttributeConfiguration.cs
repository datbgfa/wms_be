﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace WMS.Domain.Entities.EntitiyConfiguration
{
    public class AttributeConfiguration : IEntityTypeConfiguration<WmsAttribute>
    {
        public void Configure(EntityTypeBuilder<WmsAttribute> builder)
        {
            builder.ToTable("WMS_Attribute");

            builder.HasKey(x => x.AttributeId);

            builder.Property(x => x.AttributeName)
                .HasMaxLength(255)
                .IsRequired();

            builder.Property(x => x.Visible)
                .HasDefaultValue(false);
        }
    }
}
