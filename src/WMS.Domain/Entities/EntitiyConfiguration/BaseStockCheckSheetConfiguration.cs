﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.ValueGeneration;

namespace WMS.Domain.Entities.EntitiyConfiguration
{
    public class BaseStockCheckSheetConfiguration : IEntityTypeConfiguration<WmsBaseStockCheckSheet>
    {
        public void Configure(EntityTypeBuilder<WmsBaseStockCheckSheet> builder)
        {
            builder.ToTable("WMS_BaseStockCheckSheet");

            builder.HasKey(f => f.CheckId);

            builder.HasIndex(f => f.StockCheckSheetCode)
                .IsUnique();

            builder.Property(f => f.StockCheckSheetCode)
                .HasMaxLength(50);

            builder.Property(f => f.CheckId)
               .HasValueGenerator<GuidValueGenerator>();

            builder.Property(f => f.Description)
                .HasMaxLength(255);

            builder.HasMany(o => o.WmsBaseStockCheckSheetDetails)
                .WithOne(o => o.WmsBaseStockCheckSheet)
                .HasForeignKey(f => f.CheckId)
                .HasPrincipalKey(f => f.CheckId);

            builder.HasOne(o => o.UserCheck)
                .WithMany(o => o.WmsBaseStockCheckSheets)
                .HasForeignKey(f => f.CheckBy)
                .HasPrincipalKey(f => f.UserId);
        }
    }
}
