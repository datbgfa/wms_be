﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.ValueGeneration;

namespace WMS.Domain.Entities.EntitiyConfiguration
{
    public class BranchRequestConfiguration : IEntityTypeConfiguration<WmsBranchRequest>
    {
        public void Configure(EntityTypeBuilder<WmsBranchRequest> builder)
        {
            builder.ToTable("WMS_BranchRequest");

            builder.HasKey(f => new { f.RequestId, f.WarehouseId });

            builder.Property(f => f.RequestId)
                .HasValueGenerator<GuidValueGenerator>();

            builder.HasIndex(f => f.RequestCode)
                .IsUnique();

            builder.Property(f => f.RequestCode)
                .HasMaxLength(50);

            builder.Property(f => f.TotalPrice)
                .HasColumnType("money")
                .IsRequired();

            builder.HasOne(o => o.UserApprovedBy)
                .WithMany(o => o.BranchRequestsApprove)
                .HasForeignKey(f => f.ApprovedBy)
                .HasPrincipalKey(f => f.UserId);

            builder.HasOne(o => o.UserRequestBy)
                .WithMany(o => o.BranchRequestsRequest)
                .HasForeignKey(f => f.RequestBy)
                .HasPrincipalKey(f => f.UserId);

            builder.HasMany(o => o.WmsBranchProductRequests)
                .WithOne(o => o.WmsBranchRequest)
                .HasForeignKey(f => f.RequestId)
                .HasPrincipalKey(f => f.RequestId);

            builder.HasOne(o => o.WmsBranchImport)
                .WithOne(o => o.WmsBranchRequest)
                .HasForeignKey<WmsBranchImport>(f => f.RequestId)
                .HasPrincipalKey<WmsBranchRequest>(f => f.RequestId);
        }
    }
}
