﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.ValueGeneration;

namespace WMS.Domain.Entities.EntitiyConfiguration
{
    public class BranchWarehouseConfiguration : IEntityTypeConfiguration<WmsBranchWarehouse>
    {
        public void Configure(EntityTypeBuilder<WmsBranchWarehouse> builder)
        {
            builder.ToTable("WMS_BranchWarehouse");

            builder.HasKey(f => f.WarehouseId);

            builder.Property(f => f.WarehouseId)
                .HasValueGenerator<GuidValueGenerator>();

            builder.Property(f => f.WarehouseName)
                .HasMaxLength(100)
                .IsRequired();

            builder.Property(f => f.PhoneNumber)
                .HasMaxLength(15)
                .IsUnicode(false)
                .IsRequired();

            builder.Property(f => f.Email)
                .HasMaxLength(320)
                .IsUnicode(false)
                .IsRequired();

            builder.Property(f => f.Note)
                .HasMaxLength(1000);

            builder.HasOne(o => o.WmsWarehouseOwner)
                .WithMany(o => o.WmsBranchWarehouses)
                .HasForeignKey(f => f.WarehouseOwner)
                .HasPrincipalKey(f => f.UserId);

            builder.HasMany(o => o.WmsBranchRequests)
                .WithOne(o => o.WmsBranchWarehouse)
                .HasForeignKey(f => f.WarehouseId)
                .HasPrincipalKey(f => f.WarehouseId);

            builder.HasMany(o => o.WmsBranchDebts)
                .WithOne(o => o.Debtor)
                .HasForeignKey(f => f.DebtorId)
                .HasPrincipalKey(f => f.WarehouseId);

            builder.HasMany(o => o.WmsBranchImports)
                .WithOne(o => o.WmsBranchWarehouse)
                .HasForeignKey(f => f.WarehouseId)
                .HasPrincipalKey(f => f.WarehouseId);

            builder.HasMany(o => o.WmsBranchExports)
                .WithOne(o => o.WmsBranchWarehouse)
                .HasForeignKey(f => f.WarehouseId)
                .HasPrincipalKey(f => f.WarehouseId);

            builder.HasMany(o => o.WmsBranchWarehouseProducts)
                .WithOne(o => o.Warehouse)
                .HasForeignKey(f => f.WarehouseId)
                .HasPrincipalKey(f => f.WarehouseId);

            builder.HasMany(o => o.WmsBaseExports)
                .WithOne(o => o.WmsBranchWarehouse)
                .HasForeignKey(f => f.BranchWarehouseId)
                .HasPrincipalKey(f => f.WarehouseId);
        }
    }
}
