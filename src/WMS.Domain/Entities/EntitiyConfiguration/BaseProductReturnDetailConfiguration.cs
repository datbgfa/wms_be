﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.ValueGeneration;

namespace WMS.Domain.Entities.EntitiyConfiguration
{
    public class BaseProductReturnDetailConfiguration : IEntityTypeConfiguration<WmsBaseProductReturnDetail>
    {
        public void Configure(EntityTypeBuilder<WmsBaseProductReturnDetail> builder)
        {
            builder.ToTable("WMS_BaseProductReturnDetail");

            builder.HasKey(f => new { f.ProductReturnId, f.ImportId, f.ProductId });

            builder.Property(f => f.BprdId)
                .HasValueGenerator<GuidValueGenerator>();

            builder.HasIndex(f => f.BprdId)
                .IsUnique();

            builder.Property(f => f.CostPrice)
                .HasColumnType("money")
                .IsRequired();

            builder.Property(f => f.UnitPrice)
                .HasColumnType("money")
                .IsRequired();

            builder.HasOne(o => o.WmsProduct)
                .WithMany(o => o.WmsBaseProductReturnDetails)
                .HasForeignKey(f => f.ProductId)
                .HasPrincipalKey(f => f.ProductId);
        }
    }
}
