﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.ValueGeneration;

namespace WMS.Domain.Entities.EntitiyConfiguration
{
    public class BranchInvoiceConfiguration : IEntityTypeConfiguration<WmsBranchInvoice>
    {
        public void Configure(EntityTypeBuilder<WmsBranchInvoice> builder)
        {
            builder.ToTable("WMS_BranchInvoice");

            builder.HasKey(f => f.InvoiceId);

            builder.Property(f => f.InvoiceId)
                .HasValueGenerator<GuidValueGenerator>();

            builder.Property(f => f.InvoiceCode)
                .HasMaxLength(50);

            builder.Property(f => f.BalanceChange)
                .HasColumnType("money");

            builder.Property(f => f.Amount)
                .HasColumnType("money");

            builder.Property(f => f.Description)
                .HasMaxLength(1000);
        }
    }
}
