﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.ValueGeneration;

namespace WMS.Domain.Entities.EntitiyConfiguration
{
    public class UserConfiguration : IEntityTypeConfiguration<WmsUser>
    {
        public void Configure(EntityTypeBuilder<WmsUser> builder)
        {
            builder.ToTable("WMS_User");

            builder.HasKey(x => x.UserId);

            builder.Property(m => m.UserId)
                    .HasValueGenerator<GuidValueGenerator>();

            builder.Property(m => m.Username)
                .HasMaxLength(100)
                .IsRequired(true);

            builder.Property(p => p.HashedPassword)
                .HasMaxLength(255)
                .IsRequired();

            builder.Property(p => p.SaltKey)
                .HasMaxLength(50)
                .IsRequired();

            builder.Property(p => p.Fullname)
                .HasMaxLength(100)
                .IsRequired();

            builder.Property(p => p.Email)
                .HasMaxLength(320)
                .IsRequired();

            builder.Property(p => p.Address)
                .HasMaxLength(1000);

            builder.Property(p => p.Birthdate)
                .HasColumnType("date");

            builder.Property(p => p.Avatar)
                .HasMaxLength(1000);

            builder.Property(p => p.PhoneNumber)
                .HasMaxLength(15);

            #region Constraint
            builder.HasOne(e => e.User)
                .WithMany(e => e.Users)
                .HasForeignKey(e => e.Supervisor);

            builder.HasMany(e => e.UserRole)
                .WithOne(e => e.User)
                .HasForeignKey(p => p.UserId)
                .HasPrincipalKey(e => e.UserId);

            #endregion

            builder.HasIndex(m => m.Username).IsUnique();
            builder.HasIndex(m => m.Email).IsUnique();
            builder.HasIndex(m => new { m.Username, m.Email });
        }
    }
}
