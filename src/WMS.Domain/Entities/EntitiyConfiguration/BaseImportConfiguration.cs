﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.ValueGeneration;

namespace WMS.Domain.Entities.EntitiyConfiguration
{
    public class BaseImportConfiguration : IEntityTypeConfiguration<WmsBaseImport>
    {
        public void Configure(EntityTypeBuilder<WmsBaseImport> builder)
        {
            builder.ToTable("WMS_BaseImport");

            builder.HasKey(f => new { f.ImportId, f.WarehouseId });

            #region Index
            builder.HasIndex(f => f.ImportCode)
                .IsUnique();
            #endregion

            #region Property
            builder.Property(f => f.ImportId)
                .HasValueGenerator<GuidValueGenerator>();

            builder.Property(f => f.TotalPrice)
                .HasColumnType("money");

            builder.Property(f => f.ExtraCost)
                .HasColumnType("money");

            builder.Property(f => f.PaidAmount)
                .HasColumnType("money");

            builder.Property(f => f.TotalCost)
                .HasColumnType("money");

            builder.Property(f => f.ImportCode)
                .HasMaxLength(50);
            #endregion

            #region Reference
            builder.HasOne(o => o.UserImportBy)
                .WithMany(o => o.BaseImportsImport)
                .HasForeignKey(f => f.ImportBy)
                .HasPrincipalKey(f => f.UserId);

            builder.HasOne(o => o.UserAssignBy)
                .WithMany(o => o.BaseImportsAssign)
                .HasForeignKey(f => f.AssignBy)
                .HasPrincipalKey(f => f.UserId);

            builder.HasOne(o => o.WmsSupplier)
                .WithMany(o => o.WmsBaseImports)
                .HasForeignKey(f => f.SupplierId)
                .HasPrincipalKey(f => f.SupplierId);

            builder.HasMany(o => o.WmsBaseImportProducts)
                .WithOne(o => o.WmsBaseImport)
                .HasForeignKey(f => f.ImportId)
                .HasPrincipalKey(f => f.ImportId);

            builder.HasMany(o => o.WmsBaseProductReturnDetails)
                .WithOne(o => o.WmsBaseImport)
                .HasForeignKey(f => f.ImportId)
                .HasPrincipalKey(f => f.ImportId);

            builder.HasMany(o => o.WmsBaseStockCheckSheetDetails)
                .WithOne(o => o.WmsBaseImport)
                .HasForeignKey(f => f.ImportId)
                .HasPrincipalKey(f => f.ImportId);

            builder.HasMany(o => o.WmsBaseInvoices)
                .WithOne(o => o.WmsBaseImport)
                .HasForeignKey(f => f.ImportId)
                .HasPrincipalKey(f => f.ImportId);

            builder.HasMany(o => o.WmsBranchImportProducts)
                .WithOne(o => o.WmsBaseImport)
                .HasForeignKey(f => f.BaseImportId)
                .HasPrincipalKey(f => f.ImportId);

            builder.HasMany(o => o.WmsBranchProductReturnDetails)
                .WithOne(o => o.WmsBaseImport)
                .HasForeignKey(f => f.BaseImportId)
                .HasPrincipalKey(f => f.ImportId);

            builder.HasMany(o => o.WmsBaseExportProducts)
                .WithOne(o => o.WmsBaseImport)
                .HasForeignKey(f => f.BaseImportId)
                .HasPrincipalKey(f => f.ImportId);

            builder.HasMany(o => o.WmsBranchStockCheckSheetDetails)
                .WithOne(o => o.WmsBaseImport)
                .HasForeignKey(f => f.BaseImportId)
                .HasPrincipalKey(f => f.ImportId);
            #endregion
        }
    }
}
