﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace WMS.Domain.Entities.EntitiyConfiguration
{
    public class ProductConfiguration : IEntityTypeConfiguration<WmsProduct>
    {
        public void Configure(EntityTypeBuilder<WmsProduct> builder)
        {
            builder.ToTable("WMS_Product");

            builder.HasKey(x => x.ProductId);

            builder.Property(x => x.ProductId)
                .ValueGeneratedOnAdd();

            builder.Property(f => f.CategoryId)
                .IsRequired();

            builder.HasIndex(f => f.SKU)
                .IsUnique();

            builder.Property(f => f.SKU)
                .HasMaxLength(255)
                .IsUnicode(false)
                .IsRequired();

            builder.Property(f => f.ProductName)
                .HasMaxLength(100)
                .IsRequired();

            builder.Property(f => f.ProductDescription)
                .HasMaxLength(500);

            builder.Property(f => f.ProductImage)
                .HasMaxLength(1000)
                .IsRequired();

            builder.Property(f => f.Visible)
                .HasDefaultValue(true)
                .IsRequired();

            builder.Property(f => f.UnitPrice)
                .HasColumnType("money")
                .IsRequired();

            builder.Property(f => f.CostPrice)
                .HasColumnType("money")
                .IsRequired();

            builder.Property(f => f.ExportPrice)
                .HasColumnType("money")
                .IsRequired();

            builder.HasMany(o => o.WmsBranchProductRequests)
                .WithOne(o => o.WmsProduct)
                .HasForeignKey(f => f.ProductId)
                .HasPrincipalKey(f => f.ProductId);

            builder.HasMany(o => o.WmsBranchImportProducts)
                .WithOne(o => o.WmsProduct)
                .HasForeignKey(f => f.ProductId)
                .HasPrincipalKey(f => f.ProductId);

            builder.HasMany(o => o.WmsBranchExportProducts)
                .WithOne(o => o.WmsProduct)
                .HasForeignKey(f => f.ProductId)
                .HasPrincipalKey(f => f.ProductId);

            builder.HasMany(o => o.WmsBranchWarehouseProducts)
                .WithOne(o => o.WmsProduct)
                .HasForeignKey(f => f.ProductId)
                .HasPrincipalKey(f => f.ProductId);
        }
    }
}
