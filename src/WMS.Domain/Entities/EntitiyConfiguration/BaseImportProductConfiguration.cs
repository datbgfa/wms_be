﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace WMS.Domain.Entities.EntitiyConfiguration
{
    public class BaseImportProductConfiguration : IEntityTypeConfiguration<WmsBaseImportProduct>
    {
        public void Configure(EntityTypeBuilder<WmsBaseImportProduct> builder)
        {
            builder.ToTable("WMS_BaseImportProduct");

            builder.HasKey(f => new { f.ImportId, f.ProductId });

            builder.Property(f => f.CostPrice)
                .HasColumnType("money")
                .IsRequired();

            builder.Property(f => f.UnitPrice)
                .HasColumnType("money")
                .IsRequired();

            builder.HasOne(o => o.WmsProduct)
                .WithMany(o => o.WmsBaseImportProducts)
                .HasForeignKey(o => o.ProductId)
                .HasPrincipalKey(o => o.ProductId);

        }
    }
}
