﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.ValueGeneration;

namespace WMS.Domain.Entities.EntitiyConfiguration
{
    public class BranchExportConfiguration : IEntityTypeConfiguration<WmsBranchExport>
    {
        public void Configure(EntityTypeBuilder<WmsBranchExport> builder)
        {
            builder.ToTable("WMS_BranchExport");

            builder.HasKey(f => new { f.ExportId, f.WarehouseId });

            builder.Property(f => f.ExportId)
                .HasValueGenerator<GuidValueGenerator>();

            builder.HasIndex(f => f.ExportCode)
                .IsUnique();

            builder.Property(f => f.ExportCode)
                .HasMaxLength(50);

            builder.Property(f => f.TotalPrice)
                .HasColumnType("money");

            builder.HasMany(o => o.WmsBranchExportProducts)
                .WithOne(o => o.WmsBranchExport)
                .HasForeignKey(f => f.ExportId)
                .HasPrincipalKey(f => f.ExportId);

            builder.HasOne(o => o.UserAssign)
                .WithMany(o => o.BranchExportsAssign)
                .HasForeignKey(f => f.AssignBy)
                .HasPrincipalKey(f => f.UserId);

            builder.HasOne(o => o.UserExport)
                .WithMany(o => o.BranchExportsExport)
                .HasForeignKey(f => f.ExportBy)
                .HasPrincipalKey(f => f.UserId);

            builder.HasMany(o => o.WmsBranchInvoices)
                .WithOne(o => o.WmsBranchExport)
                .HasForeignKey(f => f.ExportId)
                .HasPrincipalKey(f => f.ExportId);
        }
    }
}
