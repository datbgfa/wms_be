﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace WMS.Domain.Entities.EntitiyConfiguration
{
    public class BranchImportProductConfiguration : IEntityTypeConfiguration<WmsBranchImportProduct>
    {
        public void Configure(EntityTypeBuilder<WmsBranchImportProduct> builder)
        {
            builder.ToTable("WMS_BranchImportProduct");

            builder.HasKey(f => new { f.ImportId, f.ProductId, f.BaseImportId });

            builder.Property(f => f.CostPrice)
                .HasColumnType("money")
                .IsRequired();

            builder.Property(f => f.UnitPrice)
                .HasColumnType("money")
                .IsRequired();

            builder.Property(f => f.ExportCodeItem)
                .HasMaxLength(50);
        }
    }
}
