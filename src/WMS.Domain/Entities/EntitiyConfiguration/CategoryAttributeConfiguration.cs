﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.ValueGeneration;

namespace WMS.Domain.Entities.EntitiyConfiguration
{
    public class CategoryAttributeConfiguration : IEntityTypeConfiguration<WmsCategoryAttribute>
    {
        public void Configure(EntityTypeBuilder<WmsCategoryAttribute> builder)
        {
            builder.ToTable("WMS_CategoryAttribute");

            builder.HasKey(x => new { x.CategoryId, x.AttributeId });

            builder.Property(f => f.CaId)
                .HasValueGenerator<GuidValueGenerator>();

            builder.Property(f => f.IsDelete)
                .HasDefaultValue(true);

            builder.HasOne(x => x.Category)
                .WithMany(x => x.CategoryAttributes)
                .HasForeignKey(x => x.CategoryId);

            builder.HasOne(x => x.Attribute)
                .WithMany(x => x.CategoryAttributes)
                .HasForeignKey(x => x.AttributeId);

        }
    }
}
