﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace WMS.Domain.Entities.EntitiyConfiguration
{
    public class BaseWarehouseUserConfiguration : IEntityTypeConfiguration<WmsBaseWarehouseUser>
    {
        public void Configure(EntityTypeBuilder<WmsBaseWarehouseUser> builder)
        {
            builder.ToTable("WMS_BaseWarehouseUser");

            builder.HasKey(f => new { f.UserId, f.BaseWarehouseId });

            builder.Property(f => f.UserId)
                .IsRequired();

            builder.Property(f => f.BaseWarehouseId)
                .IsRequired();

            builder.HasOne(o => o.WmsBaseWarehouse)
                .WithMany(o => o.WmsBaseWarehouseUsers)
                .HasForeignKey(f => f.BaseWarehouseId)
                .HasPrincipalKey(f => f.WarehouseId);

            builder.HasOne(o => o.WmsUser)
                .WithMany(o => o.WmsBaseWarehouseUsers)
                .HasForeignKey(f => f.UserId)
                .HasPrincipalKey(f => f.UserId);
        }
    }
}
