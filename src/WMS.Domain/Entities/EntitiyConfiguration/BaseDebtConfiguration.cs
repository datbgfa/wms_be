﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.ValueGeneration;

namespace WMS.Domain.Entities.EntitiyConfiguration
{
    public class BaseDebtConfiguration : IEntityTypeConfiguration<WmsBaseDebt>
    {
        public void Configure(EntityTypeBuilder<WmsBaseDebt> builder)
        {
            builder.ToTable("WMS_BaseDebt");

            builder.HasKey(f => f.DebtId);

            builder.Property(f => f.DebtId)
                .HasValueGenerator<GuidValueGenerator>();

            builder.Property(f => f.DebtAmount)
                .HasColumnType("money");

            builder.HasMany(o => o.WmsBaseInvoices)
                .WithOne(o => o.WmsBaseDebt)
                .HasForeignKey(f => f.DebtId)
                .HasPrincipalKey(f => f.DebtId);

            builder.HasOne(o => o.Creditor)
                .WithMany(o => o.WmsBaseDebts)
                .HasForeignKey(f => f.CreditorId)
                .HasPrincipalKey(f => f.SupplierId);

            builder.HasOne(o => o.Debtor)
                .WithMany(o => o.WmsBaseDebts)
                .HasForeignKey(f => f.DebtorId)
                .HasPrincipalKey(f => f.WarehouseId);

            builder.HasMany(o => o.WmsBaseInvoices)
                .WithOne(o => o.WmsBaseDebt)
                .HasForeignKey(f => f.DebtId)
                .HasPrincipalKey(f => f.DebtId);
        }
    }
}
