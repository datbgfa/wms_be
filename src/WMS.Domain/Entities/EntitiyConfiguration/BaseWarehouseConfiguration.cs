﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.ValueGeneration;

namespace WMS.Domain.Entities.EntitiyConfiguration
{
    public class BaseWarehouseConfiguration : IEntityTypeConfiguration<WmsBaseWarehouse>
    {
        public void Configure(EntityTypeBuilder<WmsBaseWarehouse> builder)
        {
            builder.ToTable("WMS_BaseWarehouse");

            builder.HasKey(f => f.WarehouseId);

            builder.Property(f => f.WarehouseId)
                .HasValueGenerator<GuidValueGenerator>();

            builder.Property(f => f.WarehouseName)
                .HasMaxLength(100)
                .IsRequired();

            builder.Property(f => f.PhoneNumber)
                .HasMaxLength(15)
                .IsUnicode(false)
                .IsRequired();

            builder.Property(f => f.Email)
                .HasMaxLength(320)
                .IsUnicode(false)
                .IsRequired();

            builder.HasOne(o => o.WmsWarehouseOwner)
                .WithMany(o => o.WmsBaseWarehouses)
                .HasForeignKey(f => f.WarehouseOwner)
                .HasPrincipalKey(f => f.UserId);

            builder.HasMany(o => o.WmsBaseImports)
                .WithOne(o => o.WmsBaseWarehouse)
                .HasForeignKey(f => f.WarehouseId)
                .HasPrincipalKey(f => f.WarehouseId);
        }
    }
}
