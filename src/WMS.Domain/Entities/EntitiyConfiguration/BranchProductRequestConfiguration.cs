﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace WMS.Domain.Entities.EntitiyConfiguration
{
    public class BranchProductRequestConfiguration : IEntityTypeConfiguration<WmsBranchProductRequest>
    {
        public void Configure(EntityTypeBuilder<WmsBranchProductRequest> builder)
        {
            builder.ToTable("WMS_BranchProductRequest");

            builder.HasKey(f => new { f.RequestId, f.ProductId });

            builder.Property(f => f.UnitPrice)
                .HasColumnType("money")
                .IsRequired();
        }
    }
}
