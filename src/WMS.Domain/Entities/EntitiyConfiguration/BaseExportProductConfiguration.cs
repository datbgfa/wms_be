﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace WMS.Domain.Entities.EntitiyConfiguration
{
    public class BaseExportProductConfiguration : IEntityTypeConfiguration<WmsBaseExportProduct>
    {
        public void Configure(EntityTypeBuilder<WmsBaseExportProduct> builder)
        {
            builder.ToTable("WMS_BaseExportProduct");

            builder.HasKey(f => new { f.ExportId, f.ProductId, f.BaseImportId });

            builder.Property(f => f.UnitPrice)
                .HasColumnType("money");

            builder.Property(f => f.ExportCodeItem)
                .HasMaxLength(50);
        }
    }
}
