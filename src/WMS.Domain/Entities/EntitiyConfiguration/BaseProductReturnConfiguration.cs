﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.ValueGeneration;

namespace WMS.Domain.Entities.EntitiyConfiguration
{
    public class BaseProductReturnConfiguration : IEntityTypeConfiguration<WmsBaseProductReturn>
    {
        public void Configure(EntityTypeBuilder<WmsBaseProductReturn> builder)
        {
            builder.ToTable("WMS_BaseProductReturn");

            builder.HasKey(f => f.ProductReturnId);

            builder.HasIndex(f => f.ReturnCode)
                .IsUnique();

            builder.Property(f => f.ReturnCode)
                .HasMaxLength(50);

            builder.Property(f => f.ProductReturnId)
                .HasValueGenerator<GuidValueGenerator>();

            builder.Property(f => f.TotalPrice)
                .HasColumnType("money");

            builder.Property(f => f.PaidAmount)
                .HasColumnType("money");

            builder.Property(f => f.Description)
                .HasMaxLength(255);

            builder.HasMany(o => o.WmsBaseProductReturnDetails)
                .WithOne(o => o.WmsBaseProductReturn)
                .HasForeignKey(f => f.ProductReturnId)
                .HasPrincipalKey(f => f.ProductReturnId);

            builder.HasOne(o => o.WmsBaseImport)
                .WithMany(o => o.WmsBaseProductReturns)
                .HasForeignKey(f => f.ImportId)
                .HasPrincipalKey(f => f.ImportId);

            builder.HasMany(o => o.WmsBaseInvoices)
                .WithOne(o => o.WmsBaseProductReturn)
                .HasForeignKey(f => f.ReturnId)
                .HasPrincipalKey(f => f.ProductReturnId);
        }
    }
}
