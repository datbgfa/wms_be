﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.ValueGeneration;

namespace WMS.Domain.Entities.EntitiyConfiguration
{
    public class UserRoleConfiguration : IEntityTypeConfiguration<WmsUserRole>
    {
        public void Configure(EntityTypeBuilder<WmsUserRole> builder)
        {
            builder.ToTable("WMS_UserRole");

            builder.HasKey(p => new { p.RoleId, p.UserId });

            builder.Property(x => x.IsDelete)
                .HasDefaultValue(false);

            builder.Property(m => m.UserRoleId)
                    .HasValueGenerator<GuidValueGenerator>();

            builder.Property(p => p.IsDelete)
                .HasDefaultValue(false);
        }
    }
}
