﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.ValueGeneration;

namespace WMS.Domain.Entities.EntitiyConfiguration
{
    public class BranchProductReturnDetailConfiguration : IEntityTypeConfiguration<WmsBranchProductReturnDetail>
    {
        public void Configure(EntityTypeBuilder<WmsBranchProductReturnDetail> builder)
        {
            builder.ToTable("WMS_BranchProductReturnDetail");

            builder.HasKey(f => new { f.ProductReturnId, f.ImportId, f.ProductId, f.ExportCodeItem });

            builder.HasIndex(f => f.ExportCodeItem);

            builder.Property(f => f.ExportCodeItem)
                .HasMaxLength(50);

            builder.Property(f => f.BprdId)
                .HasValueGenerator<GuidValueGenerator>();

            builder.HasIndex(f => f.BprdId)
                .IsUnique();

            builder.Property(f => f.CostPrice)
                .HasColumnType("money")
                .IsRequired();

            builder.Property(f => f.UnitPrice)
                .HasColumnType("money")
                .IsRequired();

            builder.HasOne(o => o.WmsProduct)
                .WithMany(o => o.WmsBranchProductReturnDetails)
                .HasForeignKey(f => f.ProductId)
                .HasPrincipalKey(f => f.ProductId);
        }
    }
}
