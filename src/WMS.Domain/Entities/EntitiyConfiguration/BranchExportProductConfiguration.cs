﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace WMS.Domain.Entities.EntitiyConfiguration
{
    public class BranchExportProductConfiguration : IEntityTypeConfiguration<WmsBranchExportProduct>
    {
        public void Configure(EntityTypeBuilder<WmsBranchExportProduct> builder)
        {
            builder.ToTable("WMS_BranchExportProduct");

            builder.HasKey(f => new { f.ExportId, f.ProductId, f.ImportId });

            builder.Property(f => f.UnitPrice)
                .HasColumnType("money");
        }
    }
}
