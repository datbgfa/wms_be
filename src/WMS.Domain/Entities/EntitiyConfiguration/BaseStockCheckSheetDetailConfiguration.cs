﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace WMS.Domain.Entities.EntitiyConfiguration
{
    public class BaseStockCheckSheetDetailConfiguration : IEntityTypeConfiguration<WmsBaseStockCheckSheetDetail>
    {
        public void Configure(EntityTypeBuilder<WmsBaseStockCheckSheetDetail> builder)
        {
            builder.ToTable("WMS_BaseStockCheckSheetDetail");

            builder.HasKey(f => new { f.CheckId, f.ProductId, f.ImportId });

            builder.HasOne(o => o.WmsProduct)
                .WithMany(o => o.WmsBaseStockCheckSheetDetails)
                .HasForeignKey(f => f.ProductId)
                .HasPrincipalKey(f => f.ProductId);
        }
    }
}
