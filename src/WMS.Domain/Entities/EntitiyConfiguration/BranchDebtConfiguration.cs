﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.ValueGeneration;

namespace WMS.Domain.Entities.EntitiyConfiguration
{
    public class BranchDebtConfiguration : IEntityTypeConfiguration<WmsBranchDebt>
    {
        public void Configure(EntityTypeBuilder<WmsBranchDebt> builder)
        {
            builder.ToTable("WMS_BranchDebt");

            builder.HasKey(f => f.DebtId);

            builder.Property(f => f.DebtId)
                .HasValueGenerator<GuidValueGenerator>();

            builder.Property(f => f.DebtAmount)
                .HasColumnType("money");

            builder.HasMany(o => o.WmsBranchInvoices)
                .WithOne(o => o.WmsBranchDebt)
                .HasForeignKey(f => f.DebtId)
                .HasPrincipalKey(f => f.DebtId);

            builder.HasMany(o => o.WmsBaseInvoices)
                .WithOne(o => o.WmsBranchDebt)
                .HasForeignKey(f => f.BranchDebtId)
                .HasPrincipalKey(f => f.DebtId);
        }
    }
}
