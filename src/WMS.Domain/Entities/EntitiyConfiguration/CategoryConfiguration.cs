﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace WMS.Domain.Entities.EntitiyConfiguration
{
    public class CategoryConfiguration : IEntityTypeConfiguration<WmsCategory>
    {
        public void Configure(EntityTypeBuilder<WmsCategory> builder)
        {
            builder.ToTable("WMS_Category");

            builder.HasKey(x => x.CategoryId);

            builder.Property(x => x.CategoryName)
                .HasMaxLength(100)
                .IsRequired();

            builder.Property(x => x.Visible)
                .HasDefaultValue(true);
        }
    }
}
