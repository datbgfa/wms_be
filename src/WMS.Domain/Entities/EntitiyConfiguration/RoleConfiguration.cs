﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace WMS.Domain.Entities.EntitiyConfiguration
{
    public class RoleConfiguration : IEntityTypeConfiguration<WmsRole>
    {
        public void Configure(EntityTypeBuilder<WmsRole> builder)
        {
            builder.ToTable("WMS_Role");

            builder.HasKey(e => e.RoleId);

            builder.Property(p => p.RoleName)
                .HasMaxLength(255)
                .IsRequired();

            builder.Property(p => p.RoleDescription)
                .HasMaxLength(500);

            builder.HasMany(e => e.UserRole)
                .WithOne(e => e.Role)
                .HasForeignKey(p => p.RoleId)
                .HasPrincipalKey(p => p.RoleId);
        }
    }
}
