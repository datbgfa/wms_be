﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace WMS.Domain.Entities.EntitiyConfiguration
{
    public class ProductSupplierConfiguration : IEntityTypeConfiguration<WmsProductSupplier>
    {
        public void Configure(EntityTypeBuilder<WmsProductSupplier> builder)
        {
            builder.ToTable("WMS_ProductSupplier");

            builder.HasKey(x => new { x.ProductId, x.SupplierId });

            builder.Property(x => x.IsDelete)
                .HasDefaultValue(false);

            builder.HasOne(x => x.Product)
                .WithMany(x => x.ProductSuppliers)
                .HasForeignKey(x => x.ProductId);

            builder.HasOne(x => x.Supplier)
                .WithMany(x => x.ProductSuppliers)
                .HasForeignKey(x => x.SupplierId);
        }
    }
}
