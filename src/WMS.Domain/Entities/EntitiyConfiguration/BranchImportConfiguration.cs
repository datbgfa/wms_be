﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.ValueGeneration;

namespace WMS.Domain.Entities.EntitiyConfiguration
{
    public class BranchImportConfiguration : IEntityTypeConfiguration<WmsBranchImport>
    {
        public void Configure(EntityTypeBuilder<WmsBranchImport> builder)
        {
            builder.ToTable("WMS_BranchImport");

            builder.HasKey(f => new { f.ImportId, f.WarehouseId, f.RequestId });
            #region Index
            builder.HasIndex(f => f.BaseExportId)
                .IsUnique();

            builder.HasIndex(f => f.ImportCode)
                .IsUnique();
            #endregion

            #region Property
            builder.Property(f => f.ImportId)
                .HasValueGenerator<GuidValueGenerator>();

            builder.Property(f => f.TotalPrice)
                .HasColumnType("money");

            builder.Property(f => f.ExtraCost)
                .HasColumnType("money");

            builder.Property(f => f.PaidAmount)
                .HasColumnType("money");

            builder.Property(f => f.TotalCost)
                .HasColumnType("money");

            builder.Property(f => f.ImportCode)
                .HasMaxLength(50);

            builder.Property(f => f.ExportCode)
                .HasMaxLength(50);
            #endregion

            #region Reference
            builder.HasOne(o => o.UserImportBy)
                .WithMany(o => o.BranchImportsImport)
                .HasForeignKey(f => f.ImportBy)
                .HasPrincipalKey(f => f.UserId);

            builder.HasOne(o => o.UserAssignBy)
                .WithMany(o => o.BranchImportsAssign)
                .HasForeignKey(f => f.AssignBy)
                .HasPrincipalKey(f => f.UserId);

            builder.HasOne(o => o.WmsBaseWarehouse)
               .WithMany(o => o.WmsBranchImports)
               .HasForeignKey(f => f.BaseWarehouseId)
               .HasPrincipalKey(f => f.WarehouseId);

            builder.HasMany(o => o.WmsBranchImportProducts)
                .WithOne(o => o.WmsBranchImport)
                .HasForeignKey(f => f.ImportId)
                .HasPrincipalKey(f => f.ImportId);

            builder.HasMany(o => o.WmsBranchProductReturnDetails)
                .WithOne(o => o.WmsBranchImport)
                .HasForeignKey(f => f.ImportId)
                .HasPrincipalKey(f => f.ImportId);

            builder.HasMany(o => o.WmsBranchStockCheckSheetDetails)
                .WithOne(o => o.WmsBranchImport)
                .HasForeignKey(f => f.ImportId)
                .HasPrincipalKey(f => f.ImportId);

            builder.HasMany(o => o.WmsBranchProductReturns)
                .WithOne(o => o.WmsBranchImport)
                .HasForeignKey(f => f.ImportId)
                .HasPrincipalKey(f => f.ImportId);

            builder.HasMany(o => o.WmsBranchInvoices)
                .WithOne(o => o.WmsBranchImport)
                .HasForeignKey(f => f.ImportId)
                .HasPrincipalKey(f => f.ImportId);

            builder.HasMany(o => o.WmsBranchExportProducts)
                .WithOne(o => o.WmsBranchImport)
                .HasForeignKey(f => f.ImportId)
                .HasPrincipalKey(f => f.ImportId);

            builder.HasOne(o => o.WmsBaseExport)
                .WithOne(o => o.WmsBranchImport)
                .HasForeignKey<WmsBranchImport>(f => f.BaseExportId)
                .HasPrincipalKey<WmsBaseExport>(f => f.ExportId);
            #endregion
        }
    }
}
