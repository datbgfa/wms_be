﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace WMS.Domain.Entities.EntitiyConfiguration
{
    public class SupplierConfiguration : IEntityTypeConfiguration<WmsSupplier>
    {
        public void Configure(EntityTypeBuilder<WmsSupplier> builder)
        {
            builder.ToTable("WMS_Supplier");

            builder.HasKey(p => p.SupplierId);

            builder.Property(p => p.SupplierName)
                .HasMaxLength(200)
                .IsRequired();

            builder.Property(p => p.Email)
                .HasMaxLength(320)
                .IsRequired();

            builder.Property(p => p.PhoneNumber)
                .HasMaxLength(15)
                .IsRequired();

            builder.Property(p => p.Address)
                .HasMaxLength(1000)
                .IsRequired();

            builder.Property(p => p.OtherInformation)
                .HasMaxLength(255);

            builder.Property(p => p.CooperatedTime)
                .HasColumnType("date")
                .IsRequired();

            builder.Property(p => p.Logo)
                .HasMaxLength(1000);
        }
    }
}
