﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace WMS.Domain.Entities.EntitiyConfiguration
{
    public class BranchStockCheckSheetDetailConfiguration : IEntityTypeConfiguration<WmsBranchStockCheckSheetDetail>
    {
        public void Configure(EntityTypeBuilder<WmsBranchStockCheckSheetDetail> builder)
        {
            builder.ToTable("WMS_BranchStockCheckSheetDetail");

            builder.HasKey(f => new { f.CheckId, f.ProductId, f.ImportId, f.BaseImportId });

            builder.Property(f => f.BranchQuantity)
                .IsRequired();

            builder.Property(f => f.RealQuantity)
                .IsRequired();

            builder.HasOne(o => o.WmsProduct)
                .WithMany(o => o.WmsBranchStockCheckSheetDetails)
                .HasForeignKey(f => f.ProductId)
                .HasPrincipalKey(f => f.ProductId);
        }
    }
}
