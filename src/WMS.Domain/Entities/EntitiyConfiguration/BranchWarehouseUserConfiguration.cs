﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace WMS.Domain.Entities.EntitiyConfiguration
{
    public class BranchWarehouseUserConfiguration : IEntityTypeConfiguration<WmsBranchWarehouseUser>
    {
        public void Configure(EntityTypeBuilder<WmsBranchWarehouseUser> builder)
        {
            builder.ToTable("WMS_BranchWarehouseUser");

            builder.HasKey(f => new { f.UserId, f.BranchWarehouseId });

            builder.Property(f => f.UserId)
                .IsRequired();

            builder.Property(f => f.BranchWarehouseId)
                .IsRequired();

            builder.HasOne(o => o.WmsBranchWarehouse)
                .WithMany(o => o.WmsBranchWarehouseUsers)
                .HasForeignKey(f => f.BranchWarehouseId)
                .HasPrincipalKey(f => f.WarehouseId);

            builder.HasOne(o => o.WmsUser)
                .WithMany(o => o.WmsBranchWarehouseUsers)
                .HasForeignKey(f => f.UserId)
                .HasPrincipalKey(f => f.UserId);
        }
    }
}
