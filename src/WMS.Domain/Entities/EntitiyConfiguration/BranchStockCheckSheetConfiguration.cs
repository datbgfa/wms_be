﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.ValueGeneration;

namespace WMS.Domain.Entities.EntitiyConfiguration
{
    public class BranchStockCheckSheetConfiguration : IEntityTypeConfiguration<WmsBranchStockCheckSheet>
    {
        public void Configure(EntityTypeBuilder<WmsBranchStockCheckSheet> builder)
        {
            builder.ToTable("WMS_BranchStockCheckSheet");

            builder.HasKey(f => f.CheckId);

            builder.HasIndex(f => f.StockCheckSheetCode)
                .IsUnique();

            builder.Property(f => f.StockCheckSheetCode)
                .HasMaxLength(50);

            builder.Property(f => f.CheckId)
               .HasValueGenerator<GuidValueGenerator>();

            builder.Property(f => f.Description)
                .HasMaxLength(255);

            builder.HasMany(o => o.WmsBranchStockCheckSheetDetails)
                .WithOne(o => o.WmsBranchStockCheckSheet)
                .HasForeignKey(f => f.CheckId)
                .HasPrincipalKey(f => f.CheckId);

            builder.HasOne(o => o.UserCheck)
                .WithMany(o => o.WmsBranchStockCheckSheets)
                .HasForeignKey(f => f.CheckBy)
                .HasPrincipalKey(f => f.UserId);
        }
    }
}
