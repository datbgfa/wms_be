﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.ValueGeneration;

namespace WMS.Domain.Entities.EntitiyConfiguration
{
    public class ProductAttributeConfiguration : IEntityTypeConfiguration<WmsProductAttribute>
    {
        public void Configure(EntityTypeBuilder<WmsProductAttribute> builder)
        {
            builder.ToTable("WMS_ProductAttribute");

            builder.HasKey(x => new { x.ProductId, x.AttributeId });

            builder.Property(f => f.PavId)
                .HasValueGenerator<GuidValueGenerator>();

            builder.Property(f => f.Value)
                .HasMaxLength(500)
                .IsRequired();

            builder.Property(f => f.IsDelete)
                .HasDefaultValue(false);

            builder.HasOne(x => x.Product)
                .WithMany(x => x.ProductAttributes)
                .HasForeignKey(x => x.ProductId)
                .HasPrincipalKey(x => x.ProductId);

            builder.HasOne(x => x.Attribute)
                .WithMany(x => x.ProductAttributes)
                .HasForeignKey(x => x.AttributeId)
                .HasPrincipalKey(x => x.AttributeId);
        }
    }
}
