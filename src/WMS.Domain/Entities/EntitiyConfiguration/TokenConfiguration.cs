﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace WMS.Domain.Entities.EntitiyConfiguration
{
    public class TokenConfiguration : IEntityTypeConfiguration<WmsToken>
    {
        public void Configure(EntityTypeBuilder<WmsToken> builder)
        {
            builder.ToTable("WMS_Token");

            builder.HasKey(f => f.UserId);

            builder.Property(f => f.Token)
                .HasMaxLength(100);

            builder.Property(f => f.CreateDate)
                .HasDefaultValue(DateTime.UtcNow);

            builder.HasIndex(f => f.UserId)
                .IsUnique();

            builder.HasOne(e => e.User)
                .WithOne(e => e.Token)
                .HasForeignKey<WmsToken>(f => f.UserId)
                .HasPrincipalKey<WmsUser>(f => f.UserId);
        }
    }
}
