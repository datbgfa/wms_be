﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.ValueGeneration;

namespace WMS.Domain.Entities.EntitiyConfiguration
{
    public class BranchWarehouseProductConfiguration : IEntityTypeConfiguration<WmsBranchWarehouseProduct>
    {
        public void Configure(EntityTypeBuilder<WmsBranchWarehouseProduct> builder)
        {
            builder.ToTable("WMS_BranchWarehouseProduct");

            builder.HasKey(f => new { f.ProductId, f.WarehouseId });

            builder.Property(f => f.BpId)
                .HasValueGenerator<GuidValueGenerator>();

            builder.HasIndex(f => f.BpId)
                .IsUnique();

            builder.Property(f => f.CostPrice)
                .HasColumnType("money");

            builder.Property(f => f.ExportPrice)
                .HasColumnType("money");

            builder.Property(f => f.IsDelete)
                .HasDefaultValue(false);
        }
    }
}
