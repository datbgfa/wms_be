﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.ValueGeneration;

namespace WMS.Domain.Entities.EntitiyConfiguration
{
    public class BaseExportConfiguration : IEntityTypeConfiguration<WmsBaseExport>
    {
        public void Configure(EntityTypeBuilder<WmsBaseExport> builder)
        {
            builder.ToTable("WMS_BaseExport");

            builder.HasKey(f => new { f.ExportId, f.WarehouseId });

            builder.Property(f => f.ExportId)
                .HasValueGenerator<GuidValueGenerator>();

            builder.HasIndex(f => f.ExportCode)
                .IsUnique();

            builder.Property(f => f.ExportCode)
                .HasMaxLength(50);

            builder.Property(f => f.TotalPrice)
                .HasColumnType("money");

            builder.Property(f => f.PaidAmount)
                .HasColumnType("money");

            builder.Property(f => f.ExtraCost)
                .HasColumnType("money");

            builder.Property(f => f.TotalCost)
                .HasColumnType("money");

            builder.HasMany(o => o.WmsBaseExportProducts)
                .WithOne(o => o.WmsBaseExport)
                .HasForeignKey(f => f.ExportId)
                .HasPrincipalKey(f => f.ExportId);

            builder.HasOne(o => o.UserAssign)
                .WithMany(o => o.BaseExportsAssign)
                .HasForeignKey(f => f.AssignBy)
                .HasPrincipalKey(f => f.UserId);

            builder.HasOne(o => o.UserExport)
                .WithMany(o => o.BaseExportsExport)
                .HasForeignKey(f => f.ExportBy)
                .HasPrincipalKey(f => f.UserId);

            builder.HasMany(o => o.WmsBranchProductReturns)
                .WithOne(o => o.WmsBaseExport)
                .HasForeignKey(f => f.BaseExportId)
                .HasPrincipalKey(f => f.ExportId);

            builder.HasMany(o => o.WmsBaseInvoices)
                .WithOne(o => o.WmsBaseExport)
                .HasForeignKey(f => f.ExportId)
                .HasPrincipalKey(f => f.ExportId);

            builder.HasOne(o => o.WmsBranchRequest)
                .WithOne(o => o.WmsBaseExport)
                .HasForeignKey<WmsBaseExport>(f => f.RequestId)
                .HasPrincipalKey<WmsBranchRequest>(f => f.RequestId);
        }
    }
}
