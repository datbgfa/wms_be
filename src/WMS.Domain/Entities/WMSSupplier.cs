﻿namespace WMS.Domain.Entities
{
    public class WmsSupplier
    {
        public int SupplierId { get; set; }
        public string SupplierName { get; set; } = null!;
        public string Address { get; set; } = null!;
        public string PhoneNumber { get; set; } = null!;
        public string Email { get; set; } = null!;
        public string? OtherInformation { get; set; }
        public string Logo { get; set; } = null!;
        public bool Status { get; set; }
        public DateTime CooperatedTime { get; set; }

        public WmsSupplier()
        {
        }

        public WmsSupplier(int supplierId, string supplierName, string address, string phoneNumber, string email, string? otherInformation, string logo, bool status, DateTime cooperatedTime)
        {
            SupplierId = supplierId;
            SupplierName = supplierName;
            Address = address;
            PhoneNumber = phoneNumber;
            Email = email;
            OtherInformation = otherInformation;
            Logo = logo;
            Status = status;
            CooperatedTime = cooperatedTime;
        }

        public virtual ICollection<WmsProductSupplier>? ProductSuppliers { get; set; }
        public virtual ICollection<WmsBaseImport>? WmsBaseImports { get; set; }
        public virtual ICollection<WmsBaseDebt>? WmsBaseDebts { get; set; }
    }
}
