﻿namespace WMS.Domain.Entities
{
    public class WmsBranchProductReturnDetail
    {
        public Guid BprdId { get; set; }
        public Guid ProductReturnId { get; set; }
        public Guid ImportId { get; set; }
        public Guid BaseImportId { get; set; }
        public Guid ProductId { get; set; }
        public decimal CostPrice { get; set; }
        public decimal UnitPrice { get; set; }
        public int RequestQuantity { get; set; }
        public string ExportCodeItem { get; set; } = null!;

        #region Navigation
        public virtual WmsBranchProductReturn? WmsBranchProductReturn { get; set; }
        public virtual WmsBranchImport? WmsBranchImport { get; set; }
        public virtual WmsBaseImport? WmsBaseImport { get; set; }
        public virtual WmsProduct? WmsProduct { get; set; }
        #endregion
    }
}
