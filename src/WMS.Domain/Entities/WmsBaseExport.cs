﻿using WMS.Domain.Entities.Common;

namespace WMS.Domain.Entities
{
    public class WmsBaseExport : IDateTracking
    {
        #region PrimaryKey
        public Guid WarehouseId { get; set; }
        public Guid ExportId { get; set; }
        #endregion
        #region Unique
        public string ExportCode { get; set; } = null!;
        #endregion
        public Guid? AssignBy { get; set; }
        public Guid? ExportBy { get; set; }
        public DateTime? ExportDate { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal ExtraCost { get; set; }
        public decimal PaidAmount { get; set; }
        public decimal TotalCost { get; set; }
        public short PaymentStatus { get; set; }
        public short ExportStatus { get; set; }
        public Guid BranchWarehouseId { get; set; }
        public Guid RequestId { get; set; }

        public string CreateUser { get; set; } = null!;
        public DateTime CreateDate { get; set; }
        public string UpdateUser { get; set; } = null!;
        public DateTime UpdateDate { get; set; }

        #region Navigation
        public virtual ICollection<WmsBaseExportProduct>? WmsBaseExportProducts { get; set; }
        public virtual ICollection<WmsBaseInvoice>? WmsBaseInvoices { get; set; }
        public virtual ICollection<WmsBranchProductReturn>? WmsBranchProductReturns { get; set; }
        public virtual WmsUser? UserAssign { get; set; }
        public virtual WmsUser? UserExport { get; set; }
        public virtual WmsBranchImport? WmsBranchImport { get; set; }
        public virtual WmsBranchWarehouse? WmsBranchWarehouse { get; set; }
        public virtual WmsBranchRequest? WmsBranchRequest { get; set; }
        #endregion
    }
}
