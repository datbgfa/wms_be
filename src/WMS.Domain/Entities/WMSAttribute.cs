﻿namespace WMS.Domain.Entities
{
    public class WmsAttribute
    {
        #region PrimaryKey
        public int AttributeId { get; set; }
        #endregion
        public string AttributeName { get; set; } = null!;
        public bool Visible { get; set; }

        #region Navigation
        public virtual ICollection<WmsCategoryAttribute>? CategoryAttributes { get; set; }
        public virtual ICollection<WmsProductAttribute>? ProductAttributes { get; set; }
        #endregion

        public WmsAttribute()
        {
        }
    }
}
