﻿namespace WMS.Domain.Entities
{
    public class WmsBranchProductRequest
    {
        public Guid RequestId { get; set; }
        public Guid ProductId { get; set; }
        public decimal UnitPrice { get; set; }
        public int RequestQuantity { get; set; }

        public virtual WmsBranchRequest? WmsBranchRequest { get; set; }
        public virtual WmsProduct? WmsProduct { get; set; }
    }
}
