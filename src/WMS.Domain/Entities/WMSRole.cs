﻿using WMS.Domain.Entities.Common;

namespace WMS.Domain.Entities
{
    public class WmsRole : BaseEntity
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; } = null!;
        public string? RoleDescription { get; set; }

        public virtual ICollection<WmsUserRole>? UserRole { get; set; }

        public WmsRole()
        {

        }

        public WmsRole(int roleId, string roleName, string? roleDescription)
        {
            RoleId = roleId;
            RoleName = roleName;
            RoleDescription = roleDescription;
        }
    }
}
