﻿namespace WMS.Domain.Entities
{
    public class WmsUser
    {
        public Guid UserId { get; set; }
        public string Username { get; set; } = null!;
        public string HashedPassword { get; set; } = null!;
        public string SaltKey { get; set; } = null!;
        public string Fullname { get; set; } = null!;
        public bool Gender { get; set; }
        public string Email { get; set; } = null!;
        public string Address { get; set; } = null!;
        public string PhoneNumber { get; set; } = null!;
        public DateTime Birthdate { get; set; }
        public short Status { get; set; }
        public Guid? Supervisor { get; set; }
        public string Avatar { get; set; } = null!;
        public DateTime CreateDate { get; set; }
        public DateTime JoinDate { get; set; }

        public WmsUser()
        {

        }

        public WmsUser(Guid userId,
            string username,
            string hashedPassword,
            string saltKey,
            string fullname,
            bool gender,
            string email,
            string address,
            string phoneNumber,
            DateTime birthdate,
            short status,
            Guid? supervisor,
            string avatar,
            DateTime createDate,
            DateTime joinDate)
        {
            UserId = userId;
            Username = username;
            HashedPassword = hashedPassword;
            SaltKey = saltKey;
            Fullname = fullname;
            Gender = gender;
            Email = email;
            Address = address;
            PhoneNumber = phoneNumber;
            Birthdate = birthdate;
            Status = status;
            Supervisor = supervisor;
            Avatar = avatar;
            CreateDate = createDate;
            JoinDate = joinDate;
        }

        public virtual ICollection<WmsUserRole>? UserRole { get; set; }
        public virtual ICollection<WmsUser>? Users { get; set; }
        public virtual ICollection<WmsBaseWarehouse>? WmsBaseWarehouses { get; set; }
        public virtual ICollection<WmsBranchWarehouse>? WmsBranchWarehouses { get; set; }

        public virtual ICollection<WmsBranchRequest>? BranchRequestsApprove { get; set; }
        public virtual ICollection<WmsBranchRequest>? BranchRequestsRequest { get; set; }

        public virtual ICollection<WmsBranchImport>? BranchImportsAssign { get; set; }
        public virtual ICollection<WmsBranchImport>? BranchImportsImport { get; set; }

        public virtual ICollection<WmsBranchExport>? BranchExportsAssign { get; set; }
        public virtual ICollection<WmsBranchExport>? BranchExportsExport { get; set; }

        public virtual ICollection<WmsBaseImport>? BaseImportsAssign { get; set; }
        public virtual ICollection<WmsBaseImport>? BaseImportsImport { get; set; }

        public virtual ICollection<WmsBaseExport>? BaseExportsAssign { get; set; }
        public virtual ICollection<WmsBaseExport>? BaseExportsExport { get; set; }

        public virtual ICollection<WmsBranchStockCheckSheet>? WmsBranchStockCheckSheets { get; set; }
        public virtual ICollection<WmsBaseStockCheckSheet>? WmsBaseStockCheckSheets { get; set; }

        public virtual ICollection<WmsBaseWarehouseUser>? WmsBaseWarehouseUsers { get; set; }
        public virtual ICollection<WmsBranchWarehouseUser>? WmsBranchWarehouseUsers { get; set; }

        public virtual WmsToken? Token { get; set; }
        public virtual WmsUser? User { get; set; }
    }
}
