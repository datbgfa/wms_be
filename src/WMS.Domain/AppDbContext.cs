﻿using Microsoft.EntityFrameworkCore;
using WMS.Domain.Entities;
using WMS.Domain.Entities.EntitiyConfiguration;

namespace WMS.Domain
{
    public class AppDbContext : DbContext
    {
        #region DbSet(Total 41 tables)

        #region Common
        public virtual DbSet<WmsAttribute> WmsAttributes { get; set; }
        public virtual DbSet<WmsCategory> WmsCategories { get; set; }
        public virtual DbSet<WmsCategoryAttribute> WmsCategoryAttributes { get; set; }
        public virtual DbSet<WmsProduct> WmsProducts { get; set; }
        public virtual DbSet<WmsProductAttribute> WmsProductAttributes { get; set; }
        public virtual DbSet<WmsProductSupplier> WmsProductSuppliers { get; set; }
        public virtual DbSet<WmsRole> WmsRoles { get; set; }
        public virtual DbSet<WmsSupplier> WmsSuppliers { get; set; }
        public virtual DbSet<WmsToken> WmsTokens { get; set; }
        public virtual DbSet<WmsUser> WmsUsers { get; set; }
        public virtual DbSet<WmsUserRole> WmsUserRoles { get; set; }
        #endregion

        #region Base
        public virtual DbSet<WmsBaseDebt> WmsBaseDebts { get; set; }
        public virtual DbSet<WmsBaseExport> WmsBaseExports { get; set; }
        public virtual DbSet<WmsBaseExportProduct> WmsBaseExportProducts { get; set; }
        public virtual DbSet<WmsBaseImport> WmsBaseImports { get; set; }
        public virtual DbSet<WmsBaseImportProduct> WmsBaseImportProducts { get; set; }
        public virtual DbSet<WmsBaseInvoice> WmsBaseInvoices { get; set; }
        public virtual DbSet<WmsBaseProductReturn> WmsBaseProductReturns { get; set; }
        public virtual DbSet<WmsBaseProductReturnDetail> WmsBaseProductReturnDetails { get; set; }
        public virtual DbSet<WmsBaseStockCheckSheet> WmsBaseStockCheckSheets { get; set; }
        public virtual DbSet<WmsBaseStockCheckSheetDetail> WmsBaseStockCheckSheetDetails { get; set; }
        public virtual DbSet<WmsBaseWarehouse> WmsBaseWarehouses { get; set; }
        public virtual DbSet<WmsBaseWarehouseUser> WmsBaseWarehouseUsers { get; set; }
        #endregion

        #region Branch
        public virtual DbSet<WmsBranchDebt> WmsBranchDebts { get; set; }
        public virtual DbSet<WmsBranchExport> WmsBranchExports { get; set; }
        public virtual DbSet<WmsBranchExportProduct> WmsBranchExportProducts { get; set; }
        public virtual DbSet<WmsBranchImport> WmsBranchImports { get; set; }
        public virtual DbSet<WmsBranchImportProduct> WmsBranchImportProducts { get; set; }
        public virtual DbSet<WmsBranchInvoice> WmsBranchInvoices { get; set; }
        public virtual DbSet<WmsBranchProductRequest> WmsBranchProductRequests { get; set; }
        public virtual DbSet<WmsBranchProductReturn> WmsBranchProductReturns { get; set; }
        public virtual DbSet<WmsBranchProductReturnDetail> WmsBranchProductReturnDetails { get; set; }
        public virtual DbSet<WmsBranchRequest> WmsBranchRequests { get; set; }
        public virtual DbSet<WmsBranchStockCheckSheet> WmsBranchStockCheckSheets { get; set; }
        public virtual DbSet<WmsBranchStockCheckSheetDetail> WmsBranchStockCheckSheetDetails { get; set; }
        public virtual DbSet<WmsBranchWarehouse> WmsBranchWarehouses { get; set; }
        public virtual DbSet<WmsBranchWarehouseProduct> WmsBranchWarehouseProducts { get; set; }
        public virtual DbSet<WmsBranchWarehouseUser> WmsBranchWarehouseUsers { get; set; }
        #endregion
        #endregion

        public AppDbContext(DbContextOptions options) : base(options)
        {
            AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new AttributeConfiguration());

            modelBuilder.ApplyConfiguration(new BaseDebtConfiguration());
            modelBuilder.ApplyConfiguration(new BaseExportConfiguration());
            modelBuilder.ApplyConfiguration(new BaseExportProductConfiguration());
            modelBuilder.ApplyConfiguration(new BaseImportConfiguration());
            modelBuilder.ApplyConfiguration(new BaseImportProductConfiguration());
            modelBuilder.ApplyConfiguration(new BaseInvoiceConfiguration());
            modelBuilder.ApplyConfiguration(new BaseProductReturnConfiguration());
            modelBuilder.ApplyConfiguration(new BaseProductReturnDetailConfiguration());
            modelBuilder.ApplyConfiguration(new BaseStockCheckSheetConfiguration());
            modelBuilder.ApplyConfiguration(new BaseStockCheckSheetDetailConfiguration());
            modelBuilder.ApplyConfiguration(new BaseWarehouseConfiguration());

            modelBuilder.ApplyConfiguration(new BranchDebtConfiguration());
            modelBuilder.ApplyConfiguration(new BranchExportConfiguration());
            modelBuilder.ApplyConfiguration(new BranchExportProductConfiguration());
            modelBuilder.ApplyConfiguration(new BranchImportConfiguration());
            modelBuilder.ApplyConfiguration(new BranchImportProductConfiguration());
            modelBuilder.ApplyConfiguration(new BranchInvoiceConfiguration());
            modelBuilder.ApplyConfiguration(new BranchProductRequestConfiguration());
            modelBuilder.ApplyConfiguration(new BranchProductReturnConfiguration());
            modelBuilder.ApplyConfiguration(new BranchProductReturnDetailConfiguration());
            modelBuilder.ApplyConfiguration(new BranchRequestConfiguration());
            modelBuilder.ApplyConfiguration(new BranchStockCheckSheetConfiguration());
            modelBuilder.ApplyConfiguration(new BranchStockCheckSheetDetailConfiguration());
            modelBuilder.ApplyConfiguration(new BranchWarehouseProductConfiguration());
            modelBuilder.ApplyConfiguration(new BranchWarehouseConfiguration());

            modelBuilder.ApplyConfiguration(new CategoryConfiguration());
            modelBuilder.ApplyConfiguration(new CategoryAttributeConfiguration());
            modelBuilder.ApplyConfiguration(new ProductConfiguration());
            modelBuilder.ApplyConfiguration(new ProductAttributeConfiguration());
            modelBuilder.ApplyConfiguration(new ProductSupplierConfiguration());
            modelBuilder.ApplyConfiguration(new TokenConfiguration());
            modelBuilder.ApplyConfiguration(new RoleConfiguration());
            modelBuilder.ApplyConfiguration(new SupplierConfiguration());
            modelBuilder.ApplyConfiguration(new UserConfiguration());
            modelBuilder.ApplyConfiguration(new UserRoleConfiguration());

            modelBuilder.ApplyConfiguration(new BaseWarehouseUserConfiguration());
            modelBuilder.ApplyConfiguration(new BranchWarehouseUserConfiguration());

            base.OnModelCreating(modelBuilder);
        }

        [DbFunction(name: "unaccent", IsBuiltIn = true)]
        public string Unaccent(string text)
        {
            throw new NotImplementedException();
        }
    }
}
