﻿using WMS.Domain.Entities;
using WMS.Domain.Enums;

namespace WMS.Domain
{
    public class DbInitializer
    {
        private readonly AppDbContext _context;

        public DbInitializer(AppDbContext context)
        {
            _context = context;
        }

        public async Task Seed()
        {
            if (!_context.WmsUsers.Any())
            {
                var rootUser = new WmsUser(Guid.Empty, "root",
                    "839bac28e6908ccf760c0304439b0493a7e9490136f84ccf5ee07b26244b8258",
                    "9UDWv5BFHXZpDpbkmXpEs6dq33qjXKyb",
                    "Admin",
                    true,
                    "admin@gmail.com",
                    "base",
                    "123456789",
                    DateTime.Now,
                    (short)UserStatusEnum.Active,
                    null,
                    string.Empty,
                    DateTime.UtcNow,
                    DateTime.UtcNow);
                await _context.AddAsync(rootUser);
            }
            await _context.SaveChangesAsync();
        }
    }
}
