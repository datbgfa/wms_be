﻿namespace WMS.Domain.Constant
{
    public static class CommonConstant
    {
        public const int Administrator = 1;
        public const int BaseWarehouseManager = 2;
        public const int BaseWarehouseEmployee = 3;
        public const int BranchWarehouseDirector = 4;
        public const int BranchWarehouseManager = 5;
        public const int BranchWarehouseEmployee = 6;
        public const string DefaultMaleAvatar = "Avatar/default_male.jpg";
        public const string DefaultFemaleAvatar = "Avatar/default_female.jpg";
        public const string Admin = "Admin";
        public const string BaseManager = "Base_WM";
        public const string BaseEmployee = "Base_WE";
        public const string BranchDirector = "Branch_WD";
        public const string BranchEmployee = "Branch_WE";
        public const string BranchManager = "Branch_WM";

        public const int Zero = 0;
    }
}




