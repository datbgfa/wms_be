﻿namespace WMS.Domain.Constant
{
    public static class ImportStatusConstant
    {
        public const int NotImported = 1;
        public const int Imported = 2;
    }

    public static class PaymentStatusConstant
    {
        public const int NotPaid = 1;
        public const int PartlyPaid = 2;
        public const int Paid = 3;
    }

    public static class InvoiceTypeConstant
    {
        public const int ReceiptVoucher = 1;
        public const int PaymentVoucher = 2;
    }

    public static class ExportStatusConstant
    {
        public const int Requested = 1;
        public const int Confirmed = 2;
        public const int Exported = 3;
        public const int Refused = 4;
    }

    public static class BranchRequestApprovalStatusConstant
    {
        public const int NotApproved = 0;
        public const int Approved = 1;
        public const int Refused = 2;
        public const int RefusedByBaseWarehouse = 3;
    }

    public static class ProductReturnStatusConstant
    {
        public const int Requested = 0;
        public const int Returned = 1;
        public const int Refused = 2;
    }
}
