﻿using WMS.Domain.Entities;

namespace WMS.UnitTest.DummiesData
{
    public static class BaseImportData
    {
        public static List<WmsBaseImport> Data => new List<WmsBaseImport>
        {
            new WmsBaseImport(Guid.Parse("aa7778c5-00f0-47fd-b40d-0e4b7fbb2739"), Guid.Parse("ba7778c1-00f0-47fd-b40d-0e4b7fbb2739"), Guid.Parse("ca7778c5-00f0-47fd-b40d-0e4b7fbb2739"), Guid.Parse("ca7778c5-00f0-47fd-b40d-0e4b7fbb2739"), 1, new DateTime(DateTime.Now.AddDays(10).Ticks, DateTimeKind.Unspecified), new DateTime(DateTime.Now.AddDays(10).Ticks, DateTimeKind.Unspecified), 200000, 20000, 220000, 0, "", 1, 1, "", DateTime.Now, "", DateTime.Now),
        };
    }
}
