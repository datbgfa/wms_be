﻿using WMS.Domain.Entities;

namespace WMS.UnitTest.DummiesData
{
    public static class BranchReturnData
    {
        public static List<WmsBranchProductReturn> Data => new List<WmsBranchProductReturn>
        {
            new WmsBranchProductReturn(Guid.Parse("bb2978c5-20f0-47fd-b50d-0e4b7fbb2089"), Guid.Parse("aa7778c5-00f0-47fd-b40d-0e4b7fbb2738"), Guid.Parse("aa7778c5-00f0-47fd-b40d-0e4b7fbb2737"), "", 1, 100000, 0, 1, "", DateTime.Now, "", DateTime.Now),
        };
    }





    public static class BranchRequestData
    {
        public static List<WmsBranchRequest> Data => new List<WmsBranchRequest>
        {
            new WmsBranchRequest(Guid.Parse("a23d504e-8e0b-47b9-a51c-50adf96a2339"), Guid.Parse("bb2978c5-20f0-47fd-b50d-0e4b7fbb2089"), null, Guid.Parse("bb2978c5-20f0-47fd-b50d-0e4b7fbb2089"), "BRRQ_1", DateTime.Now, 200000, 0, "", DateTime.Now, "", DateTime.Now),
        };
    }
}
