﻿using WMS.Domain.Entities;

namespace WMS.UnitTest.DummiesData
{
    public static class UserRoleData
    {
        public static List<WmsUserRole> Data =>
            new List<WmsUserRole>
            {
                new WmsUserRole(Guid.Parse("191b9ff8-897e-4dca-bd52-f04f335c32a6"), 1, Guid.Parse("e1f51373-3d51-492c-99c1-0f2e481938c7"), false),
                new WmsUserRole(Guid.Parse("0e690675-5e1e-4493-a71f-7b870e24a1ef"), 2, Guid.Parse("60b5f8df-1685-4e35-aded-8ec8b4dff558"), false),
                new WmsUserRole(Guid.Parse("b2814252-59c8-48cc-b215-6c2754a28f31"), 2, Guid.Parse("91bc5828-904d-44bd-881c-2382810b0354"), true),
            };
    }
}
