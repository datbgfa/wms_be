﻿using WMS.Domain.Entities;

namespace WMS.UnitTest.DummiesData
{
    public static class RoleData
    {
        public static List<WmsRole> Data =>
            new List<WmsRole>
            {
                new WmsRole(1, "PERMISSION1", "Permission 1"),
                new WmsRole(2, "PERMISSION2", "Permission 2"),
                new WmsRole(3, "PERMISSION3", "Permission 3"),
            };
    }
}
