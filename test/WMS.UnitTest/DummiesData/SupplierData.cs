﻿using WMS.Domain.Entities;

namespace WMS.UnitTest.DummiesData
{
    public static class SupplierData
    {
        public static IEnumerable<WmsSupplier> Data => new List<WmsSupplier>
        {
            new WmsSupplier(1, "Oyonder", "Room 1064", "1796384556", "jherche0@craigslist.org", "Zachar Bay Seaplane Base", "http://dummyimage.com/150x150.png/dddddd/000000", true, Convert.ToDateTime("2022-08-04")),
            new WmsSupplier(2, "Skilith", "Suite 34", "6156553618", "bdarbishire1@comcast.net", "", "http://dummyimage.com/150x150.png/5fa2dd/ffffff", false, Convert.ToDateTime("2022-05-26")),
            new WmsSupplier(3, "Topiclounge", "PO Box 2955", "2664961016", "cdelucia2@moonfruit.com", "Whiteman Airport", "http://dummyimage.com/150x150.png/cc0000/ffffff", false, Convert.ToDateTime("2023-09-09")),
            new WmsSupplier(4, "Ozu", "PO Box 76019", "9588692306", "njoysey3@cdc.gov", "Lewiston Nez Perce County Airport", "http://dummyimage.com/150x150.png/dddddd/000000", true, Convert.ToDateTime("2022-05-26")),
            new WmsSupplier(5, "Zoombeat", "PO Box 9870", "4071109095", "mburrill4@sourceforge.net", "Gorakhpur Airport", "http://dummyimage.com/150x150.png/dddddd/000000", false, Convert.ToDateTime("2022-03-11")),
            new WmsSupplier(6, "Rooxo", "PO Box 80005", "7938756905", "sperton5@reuters.com", "Takapoto Airport", "http://dummyimage.com/150x150.png/dddddd/000000", true, Convert.ToDateTime("2023-07-10")),
            new WmsSupplier(7, "Youspan", "16th Floor", "7832720955", "iphizacklea6@linkedin.com", "Rocky Mountain House Airport", "http://dummyimage.com/150x150.png/cc0000/ffffff", true, Convert.ToDateTime("2023-03-28")),
            new WmsSupplier(8, "Tagpad", "Apt 1636", "2207603124", "npinar7@accuweather.com", "", "http://dummyimage.com/150x150.png/5fa2dd/ffffff", false, Convert.ToDateTime("2023-04-20")),
            new WmsSupplier(9, "Skippad", "PO Box 79445", "5784239946", "lmateo8@amazon.de", "Deadman's Cay Airport", "http://dummyimage.com/150x150.png/ff4444/ffffff", true, Convert.ToDateTime("2021-11-07")),
            new WmsSupplier(10, "Omba", "8th Floor", "9622606999", "awhitfield9@foxnews.com", "Boun Neau Airport", "http://dummyimage.com/150x150.png/ff4444/ffffff", false, Convert.ToDateTime("2023-08-11")),
            new WmsSupplier(11, "Feedfish", "Apt 1047", "2988414262", "cruggieroa@seesaa.net", "Kautokeino Air Base", "http://dummyimage.com/150x150.png/5fa2dd/ffffff", false, Convert.ToDateTime("2023-05-09")),
            new WmsSupplier(12, "Flashpoint", "Apt 703", "2068175043", "sbinfieldb@ustream.tv", "Angads Airport", "http://dummyimage.com/150x150.png/5fa2dd/ffffff", false, Convert.ToDateTime("2022-09-08")),
            new WmsSupplier(13, "Youspan", "Apt 561", "4395835289", "framelc@ustream.tv", "Vitória da Conquista Airport", "http://dummyimage.com/150x150.png/5fa2dd/ffffff", false, Convert.ToDateTime("2022-09-07")),
            new WmsSupplier(14, "Lajo", "PO Box 36188", "6565443021", "ffretwelld@t-online.de", "", "http://dummyimage.com/150x150.png/5fa2dd/ffffff", true, Convert.ToDateTime("2022-11-27")),
            new WmsSupplier(15, "Meetz", "Room 406", "4508332206", "fblackmoree@shop-pro.jp", "", "http://dummyimage.com/150x150.png/ff4444/ffffff", false, Convert.ToDateTime("2022-03-04")),
            new WmsSupplier(16, "Feedfire", "14th Floor", "1275919565", "sflynnf@mayoclinic.com", "Izhevsk Airport", "http://dummyimage.com/150x150.png/ff4444/ffffff", false, Convert.ToDateTime("2022-04-04")),
            new WmsSupplier(17, "Cogilith", "Room 1778", "7286813003", "mgilhooleyg@altervista.org", "", "http://dummyimage.com/150x150.png/ff4444/ffffff", false, Convert.ToDateTime("2023-09-24")),
            new WmsSupplier(18, "Chatterbridge", "Suite 67", "3626715084", "jnockh@earthlink.net", "Campo Mourão Airport", "http://dummyimage.com/150x150.png/cc0000/ffffff", false, Convert.ToDateTime("2022-09-17")),
            new WmsSupplier(19, "Youspan", "17th Floor", "4446793286", "rlandsburyi@foxnews.com", "Ottawa / Rockcliffe Airport", "http://dummyimage.com/150x150.png/cc0000/ffffff", false, Convert.ToDateTime("2021-11-02")),
            new WmsSupplier(20, "Devcast", "Room 47", "3443903305", "ncussenj@skype.com", "", "http://dummyimage.com/150x150.png/dddddd/000000", true, Convert.ToDateTime("2022-05-19")),
        };
    }
}