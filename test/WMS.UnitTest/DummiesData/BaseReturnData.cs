﻿using WMS.Domain.Entities;

namespace WMS.UnitTest.DummiesData
{
    public static class BaseReturnData
    {
        public static List<WmsBaseProductReturn> Data => new List<WmsBaseProductReturn>
        {
            new WmsBaseProductReturn(Guid.Parse("bb2978c5-20f0-47fd-b50d-0e4b7fbb2089"), "", 1, Guid.Parse("aa7778c5-00f0-47fd-b40d-0e4b7fbb2739"), 100000, 0, 1, "", DateTime.Now, "", DateTime.Now),
        };
    }
}
