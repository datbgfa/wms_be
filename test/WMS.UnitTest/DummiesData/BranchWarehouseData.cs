﻿using WMS.Domain.Entities;

namespace WMS.UnitTest.DummiesData
{
    public static class BranchWarehouseData
    {
        public static List<WmsBranchWarehouse> Data => new List<WmsBranchWarehouse>
        {
            new WmsBranchWarehouse(Guid.Parse("aa7778c5-00f0-47fd-b40d-0e4b7fbb2739"), Guid.Parse("e1f51373-3d51-492c-99c1-0f2e481938c7"), "Kho Ha Noi", "09010901", "mail@gmail.com", "address", 1, ""),
            new WmsBranchWarehouse(Guid.Parse("1ebfeed4-3edc-4dd7-98f5-9160cb86659e"), Guid.Parse("60b5f8df-1685-4e35-aded-8ec8b4dff558"), "Warehouse2", "09010901", "mail@gmail.com", "address", 1, ""),
            new WmsBranchWarehouse(Guid.Parse("bc8edbb9-7cf4-4752-9ff4-97d91714f5bb"), Guid.Parse("91bc5828-904d-44bd-881c-2382810b0354"), "Warehouse3", "09010901", "mail@gmail.com", "address", 1, ""),
            new WmsBranchWarehouse(Guid.Parse("6832a158-a83c-493c-ba06-a3b6338031cc"), Guid.Parse("90dfa7aa-b345-4399-9cae-a4ae54037b40"), "Warehouse4", "09010901", "mail@gmail.com", "address", 1, ""),
            new WmsBranchWarehouse(Guid.Parse("1992673f-9b69-42b4-9ede-621b7bb5c439"), Guid.Parse("208a3fc9-5249-4ef7-a217-778aa70cf3c4"), "Warehouse5", "09010901", "mail@gmail.com", "address", 1, ""),
            new WmsBranchWarehouse(Guid.Parse("1882673f-9b69-42b4-9ede-621b7bb5c439"), Guid.Parse("805d1859-be9a-4881-8b3a-6c2bdb34db82"), "Warehouse6", "09010901", "mail@gmail.com", "address", 1, ""),
            new WmsBranchWarehouse(Guid.Parse("b75e45e4-8ee5-4d3c-bbff-2ab01d7d9ee5"), Guid.Parse("ecb68c4f-f412-4960-b6f2-fd96ba92435e"), "Warehouse7", "09010901", "mail@gmail.com", "address", 1, ""),
            new WmsBranchWarehouse(Guid.Parse("b75e45e4-8ee5-4d3c-bbff-2ab01d7d9ee6"), Guid.Parse("1ccbdb55-5b6b-47ae-a442-0f84d2cef576"), "Warehouse8", "09010901", "mail@gmail.com", "address", 1, ""),
            new WmsBranchWarehouse(Guid.Parse("b75e45e4-8ee5-4d3c-bbff-2ab01d7d9ee7"), Guid.Parse("b08d504e-8e0b-47b9-a51c-50adf96a2338"), "Warehouse9", "09010901", "mail@gmail.com", "address", 1, ""),
            new WmsBranchWarehouse(Guid.Parse("b75e45e4-8ee5-4d3c-bbff-2ab01d7d9ee8"), Guid.Parse("388f8a9a-df4d-4bb4-bb24-3a70cc47df90"), "Warehouse10", "09010901", "mail@gmail.com", "address", 1, ""),
            new WmsBranchWarehouse(Guid.Parse("b75e45e4-8ee5-4d3c-bbff-2ab01d7d9ee9"), Guid.Parse("a9d9a0bf-400c-4cc2-b452-00b645ed6f2e"), "Warehouse11", "09010901", "mail@gmail.com", "address", 1, ""),
            new WmsBranchWarehouse(Guid.Parse("b75e45e4-8ee5-4d3c-bbff-2ab01d7d9ee9"), null, "Warehouse12", "09010901", "mail@gmail.com", "address", 1, ""),
            new WmsBranchWarehouse(Guid.Parse("b75e45e4-8ee5-4d3c-bbff-2ab01d7d9ee9"), null, "Warehouse13", "09010901", "mail@gmail.com", "address", 1, ""),
            new WmsBranchWarehouse(Guid.Parse("b75e45e4-8ee5-4d3c-bbff-2ab01d7d9ee9"), null, "Warehouse14", "09010901", "mail@gmail.com", "address", 1, ""),
        };
    }


    public static class BaseWarehouseData
    {
        public static List<WmsBaseWarehouse> Data => new List<WmsBaseWarehouse>
        {
            new WmsBaseWarehouse(Guid.Parse("aa7778c5-00f0-47fd-b40d-0e4b7fbb2739"), Guid.Parse("e1f51373-3d51-492c-99c1-0f2e481938c7"), "Kho Ha Noi", "09010901", "mail@gmail.com", "address", 1),
        };
    }
}
