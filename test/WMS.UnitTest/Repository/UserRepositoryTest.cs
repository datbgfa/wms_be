﻿using Microsoft.EntityFrameworkCore;
using WMS.Domain;
using WMS.Domain.Repositories;
using WMS.UnitTest.DummiesData;

namespace WMS.UnitTest.Repository
{
    public class UserRepositoryTest
    {
        private readonly AppDbContext _context;

        public UserRepositoryTest()
        {
            DbContextOptionsBuilder options = new DbContextOptionsBuilder()
                .UseInMemoryDatabase(
                    databaseName: Guid.NewGuid().ToString()
                );

            _context = new AppDbContext(options.Options);
            _context.AddRange(UserData.Data);
            _context.AddRange(RoleData.Data);
            _context.AddRange(UserRoleData.Data);
            _context.SaveChanges();
        }

        [Theory]
        [InlineData("nprettejohns0")]
        [InlineData("forpin1")]
        public async Task GetUserContainRoleByUsername_UsernameExist_ReturnUser(string username)
        {
            var userRepository = new WmsUserRepository(_context);
            var user = await userRepository.GetUserContainRoleByUsername(username);

            Assert.NotNull(user);
            Assert.Equal(username, user.Username);
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public async Task GetUserContainRoleByUsername_UsernameDoesNotExist_ReturnUser(string username)
        {
            var userRepository = new WmsUserRepository(_context);
            var user = await userRepository.GetUserContainRoleByUsername(username);

            Assert.Null(user);
        }

        [Theory]
        [InlineData("nprettejohns0", (short)1)]
        [InlineData("forpin1", (short)1)]
        [InlineData("ggurlingj", (short)0)]
        [InlineData("ggurlingj", null)]
        [InlineData("nprettejohns0", null)]
        public void GetUserWithRoleByUsername_UsernameAndStatusValid_ReturnUser(string username, short? status)
        {
            var userRepository = new WmsUserRepository(_context);
            var user = userRepository.GetUserWithRoleByUsername(username, status)
                .ToList();

            Assert.NotNull(user);
            Assert.Single(user);
            Assert.Single(user.Select(x => x.UserRole).ToList());
        }

        [Theory]
        [InlineData("ggurlingj", (short)1)]
        [InlineData("", (short)1)]
        [InlineData("", (short)0)]
        [InlineData("", null)]
        public void GetUserWithRoleByUsername_UsernameOrStatusNotValid_ReturnEmpty(string username, short? status)
        {
            var userRepository = new WmsUserRepository(_context);
            var user = userRepository.GetUserWithRoleByUsername(username, status)
                .ToList();

            Assert.NotNull(user);
            Assert.Empty(user);
        }

        [Theory]
        [InlineData("e1f51373-3d51-492c-99c1-0f2e481938c7", (short)1)]
        [InlineData("adcfe2f8-f44f-4c3a-bbc2-2ec31e76d796", (short)0)]
        [InlineData("e1f51373-3d51-492c-99c1-0f2e481938c7", null)]
        [InlineData("adcfe2f8-f44f-4c3a-bbc2-2ec31e76d796", null)]
        public void GetUserWithRoleByUserid_UserIdAndStatusValid_ReturnUser(Guid userId, short? status)
        {
            var userRepository = new WmsUserRepository(_context);
            var user = userRepository.GetUserWithRoleByUserid(userId, status);

            Assert.NotNull(user);
            Assert.Single(user);
            Assert.Single(user.Select(x => x.UserRole).ToList());
        }

        [Theory]
        [InlineData("adcfe2f8-f44f-4c3a-bbc2-2ec31e76d796", (short)1)]
        [InlineData("e1f51373-3d51-492c-99c1-0f2e481938c7", (short)0)]
        public void GetUserWithRoleByUserid_UserIdOrStatusNotValid_ReturnEmpty(Guid userId, short? status)
        {
            var userRepository = new WmsUserRepository(_context);
            var user = userRepository.GetUserWithRoleByUserid(userId, status);

            Assert.NotNull(user);
            Assert.Empty(user);
        }

        [Theory]
        [InlineData("nprettejohns0")]
        public async Task GetUserRoleByUsername_UsernameValid_ReturnUser(string username)
        {
            var userRepository = new WmsUserRepository(_context);
            var role = await userRepository.GetUserRoleByUsername(username);

            Assert.NotNull(role);
            Assert.NotEmpty(role);
            Assert.Single(role);
            Assert.Equal("PERMISSION1", role.FirstOrDefault()?.RoleName);
        }
    }
}
