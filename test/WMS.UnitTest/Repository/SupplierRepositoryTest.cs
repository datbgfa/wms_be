﻿using Microsoft.EntityFrameworkCore;
using System.Collections.ObjectModel;
using WMS.Domain;
using WMS.Domain.Entities;
using WMS.Domain.Repositories;
using WMS.UnitTest.DummiesData;

namespace WMS.UnitTest.Repository
{
    public class SupplierRepositoryTest
    {
        private readonly AppDbContext _context;

        public SupplierRepositoryTest()
        {
            DbContextOptionsBuilder options = new DbContextOptionsBuilder()
                .UseInMemoryDatabase(
                    databaseName: Guid.NewGuid().ToString()
                );

            _context = new AppDbContext(options.Options);
            _context.AddRange(SupplierData.Data);
            _context.SaveChanges();
        }

        [Fact]
        public async Task GetAllSuppliers_WhenCalled_ReturnsSupplierListAsync()
        {
            //Act
            var supplierRepository = new BaseRepository<WmsSupplier>(_context);
            var suppliers = await supplierRepository.All();

            //Assert
            Assert.NotNull(suppliers);
            Assert.Equal(20, suppliers.Count());
        }

        [Fact]
        public async Task GetByIdInt_WhenCalled_ReturnsSupplierAsync()
        {
            //Act
            var supplierRepository = new BaseRepository<WmsSupplier>(_context);
            var supplier = await supplierRepository.GetById(1);

            //Assert
            var expectedId = SupplierData.Data.FirstOrDefault()?.SupplierId;
            Assert.Equal(expectedId, supplier?.SupplierId);
        }

        [Fact]
        public async Task AddAsync_WhenCalled_ProperMethodCalled()
        {
            //Arrange
            var testObject = new WmsSupplier(21, "TEST", "TEST address", "123456789", "TEST@email.com", "", "TEST LOGO", true, Convert.ToDateTime("2023-11-04"));

            //Act
            var supplierRepository = new BaseRepository<WmsSupplier>(_context);
            await supplierRepository.AddAsync(testObject);
            await _context.SaveChangesAsync();

            //Assert
            Assert.Equal(21, _context.WmsSuppliers.Count());
        }

        [Fact]
        public async Task AddRangeAsync_ValidData()
        {
            //Arrange
            var testList = new Collection<WmsSupplier>()
            {
                new WmsSupplier(21, "TEST1", "TEST address", "123456789", "TEST@email.com", "", "TEST LOGO", true, Convert.ToDateTime("2023-11-04")),
                new WmsSupplier(22, "TEST2", "TEST address", "123456789", "TEST@email.com", "", "TEST LOGO", true, Convert.ToDateTime("2023-11-04")),
                new WmsSupplier(23, "TEST3", "TEST address", "123456789", "TEST@email.com", "", "TEST LOGO", true, Convert.ToDateTime("2023-11-04")),
            };

            //Act
            var supplierRepository = new BaseRepository<WmsSupplier>(_context);
            await supplierRepository.AddRangeAsync(testList);
            await _context.SaveChangesAsync();

            //Assert
            Assert.Equal(23, _context.WmsSuppliers.Count());
        }
    }
}
