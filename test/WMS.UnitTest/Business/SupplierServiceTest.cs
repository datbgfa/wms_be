﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using MockQueryable.Moq;
using Moq;
using System.Data;
using WMS.Business.Abstractions.Service;
using WMS.Business.Dtos.CommonDto;
using WMS.Business.Dtos.Mapper;
using WMS.Business.Dtos.SupplierDto;
using WMS.Business.Services;
using WMS.Domain.Abstractions;
using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Entities;
using WMS.UnitTest.DummiesData;
using WMS.UnitTest.ImageTest;

namespace WMS.UnitTest.Business
{
    public class SupplierServiceTest
    {
        private readonly SupplierService _supplierService;
        private readonly Mock<IUnitOfWork> _unitOfWork = new Mock<IUnitOfWork>();
        private readonly Mock<IS3StorageService> _s3StorageServiceMock = new Mock<IS3StorageService>();

        private readonly Mock<IEmailService> _emailServiceMock = new Mock<IEmailService>();
        private readonly IMock<ILogger<SupplierService>> _logger = new Mock<ILogger<SupplierService>>();

        private readonly Mock<IWmsTokenRepository> _tokenRepoMock = new Mock<IWmsTokenRepository>();
        private readonly Mock<IWmsUserRepository> _userRepoMock = new Mock<IWmsUserRepository>();
        private readonly Mock<IWmsSupplierRepository> _supplierRepoMock = new Mock<IWmsSupplierRepository>();
        private readonly Mock<IWmsBaseWarehouseRepository> _baseWarehouseRepoMock = new Mock<IWmsBaseWarehouseRepository>();
        private readonly Mock<IWmsBaseDebtRepository> _baseDebtRepoMock = new Mock<IWmsBaseDebtRepository>();

        private readonly IMapper _mapper;

        public SupplierServiceTest()
        {
            var mockTran = new Mock<IDbTransaction>();

            _unitOfWork.Setup(x => x.BeginTransaction()).Returns(mockTran.Object);
            _unitOfWork.Setup(x => x.UserRepository).Returns(_userRepoMock.Object);
            _unitOfWork.Setup(x => x.SupplierRepository).Returns(_supplierRepoMock.Object);
            _unitOfWork.Setup(x => x.BaseWarehouseRepository).Returns(_baseWarehouseRepoMock.Object);
            _unitOfWork.Setup(x => x.BaseDebtRepository).Returns(_baseDebtRepoMock.Object);



            var configuration = new MapperConfiguration(config =>
            {
                config.AddProfile<DomainToDtoProfile>();
                config.AddProfile<DtoToDomainProfile>();
            });
            var mapper = _mapper = new Mapper(configuration);

            _supplierService = new SupplierService(_unitOfWork.Object, mapper, _s3StorageServiceMock.Object, _logger.Object);
        }

        [Fact]
        public async Task ViewAllSuppliersList_SuppliersExist()
        {
            //Arrange
            _supplierRepoMock.Setup(x => x.All()).ReturnsAsync(SupplierData.Data);

            //Act
            var result = await _supplierService.GetAllSuppliers().ConfigureAwait(false);

            //Assert
            Assert.NotNull(result);
            Assert.NotEmpty(result);
        }

        [Fact]
        public async Task ViewAllSuppliersList_NoSupplier()
        {
            //Arrange
            _supplierRepoMock.Setup(x => x.All()).ReturnsAsync(new List<WmsSupplier>());

            //Act
            var result = await _supplierService.GetAllSuppliers().ConfigureAwait(false);

            //Assert
            Assert.Empty(result);
        }


        [Theory]
        [MemberData(nameof(GetSuppliersData))]
        public async Task GetSuppliers(SupplierRequestDto entity)
        {
            //Arrange
            _supplierRepoMock.Setup(x => x.SearchSuppliersList(It.IsAny<string>())).Returns(SupplierData.Data.BuildMock());

            //Act
            var result = await _supplierService.GetSuppliersList(entity).ConfigureAwait(false);

            //Assert
            Assert.NotNull(result);
            Assert.NotEmpty(result.Items);
        }

        public static IEnumerable<object[]> GetSuppliersData()
        {
            yield return new object[]
            {
                new SupplierRequestDto() //normal case
                {
                    SearchWord = "abc",
                    PagingRequest = new PagingRequest()
                    {
                        PageSize = 10,
                        PageRange = 10,
                        CurrentPage = 1,
                    }
                }
            };
            yield return new object[]
            {
                new SupplierRequestDto() //normal case
                {
                    SearchWord = "",
                    PagingRequest = new PagingRequest()
                    {
                        PageSize = 10,
                        PageRange = 10,
                        CurrentPage = 1,
                    }
                }
            };
            yield return new object[]
            {
                new SupplierRequestDto() //normal case
                {
                    SearchWord = null,
                    PagingRequest = new PagingRequest()
                    {
                        PageSize = 10,
                        PageRange = 10,
                        CurrentPage = 1,
                    }
                }
            };
            yield return new object[]
            {
                new SupplierRequestDto() //abnormal case
                {
                    SearchWord = "abc",
                    PagingRequest = new PagingRequest()
                    {
                        PageSize = -1,
                        PageRange = 10,
                        CurrentPage = 1,
                    }
                }
            };
            yield return new object[]
            {
                new SupplierRequestDto() //abnormal case
                {
                    SearchWord = "abc",
                    PagingRequest = new PagingRequest()
                    {
                        PageSize = 10,
                        PageRange = -1,
                        CurrentPage = 1,
                    }
                }
            };
            yield return new object[]
            {
                new SupplierRequestDto() //abnormal case
                {
                    SearchWord = "abc",
                    PagingRequest = new PagingRequest()
                    {
                        PageSize = 10,
                        PageRange = 10,
                        CurrentPage = -1,
                    }
                }
            };
        }

        [Theory]
        [InlineData(0, false)]
        [InlineData(1, true)]
        public async Task ViewSupplier(int id, bool result)
        {
            //Arrange
            _supplierRepoMock.Setup(x => x.GetSupplierDetails(It.IsAny<int>())).ReturnsAsync(SupplierData.Data.FirstOrDefault(x => x.SupplierId == id));

            //Act
            var actual = await _supplierService.GetBySupplierId(id).ConfigureAwait(false);

            //Assert
            if (result) Assert.NotNull(actual);
            else Assert.Null(actual);
        }


        [Theory]
        [MemberData(nameof(CreateSupplierData))]
        public async Task CreateSupplier(AddSupplierRequestDto entity, string image, bool result)
        {
            //Arrange
            IFormFile? file = null;
            if (image != null)
            {
                //Setup mock file using a memory stream
                var currentPath = System.IO.Directory.GetCurrentDirectory().Split("\\bin")[0];
                var sourceImg = $@"{currentPath}{image}";

                file = FileHelper.GenFormFile($"{sourceImg}", "image/jpg", "data", "d");
                // Process file
                await using var ms = new MemoryStream();
                await file.CopyToAsync(ms);

            }
            //repo
            _baseWarehouseRepoMock.Setup(x => x.All()).ReturnsAsync(BaseWarehouseData.Data);


            //Act
            var actual = await _supplierService.AddSupplier(entity, file).ConfigureAwait(false);


            //Assert
            Assert.Equal(result, actual);
            if (result)
            {
                _supplierRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsSupplier>()), Times.Once);
                _baseDebtRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBaseDebt>()), Times.Once);
            }
        }

        public static IEnumerable<object[]> CreateSupplierData()
        {
            yield return new object[]  //normal case
            {
                new AddSupplierRequestDto()
                {
                    SupplierName = "abc",
                    Address = "abc",
                    PhoneNumber = "0900000",
                    Email = "abc.abc@gmail.com",
                    OtherInformation = ""
                }, "/ImageTest/jpgReal.jpg", true
            };
            yield return new object[] //fake image
            {
                new AddSupplierRequestDto()
                {
                    SupplierName = "abc",
                    Address = "abc",
                    PhoneNumber = "0900000",
                    Email = "abc.abc@gmail.com",
                    OtherInformation = ""
                }, "/ImageTest/jpgFake.jpg", false
            };
            yield return new object[] //no image
            {
                new AddSupplierRequestDto()
                {
                    SupplierName = "abc",
                    Address = "abc",
                    PhoneNumber = "0900000",
                    Email = "abc.abc@gmail.com",
                    OtherInformation = ""
                }, null, true
            };
            yield return new object[] //no name
            {
                new AddSupplierRequestDto()
                {
                    SupplierName = "    ",
                    Address = "abc",
                    PhoneNumber = "0900000",
                    Email = "abc.abc@gmail.com",
                    OtherInformation = ""
                }, "/ImageTest/jpgReal.jpg", false
            };
            yield return new object[] //no address
            {
                new AddSupplierRequestDto()
                {
                    SupplierName = "abc",
                    Address = "   ",
                    PhoneNumber = "0900000",
                    Email = "abc.abc@gmail.com",
                    OtherInformation = ""
                }, "/ImageTest/jpgReal.jpg", true
            };
            yield return new object[] //no phone
            {
                new AddSupplierRequestDto()
                {
                    SupplierName = "abc",
                    Address = "abc",
                    PhoneNumber = "   ",
                    Email = "abc.abc@gmail.com",
                    OtherInformation = ""
                }, "/ImageTest/jpgReal.jpg", false
            };
            yield return new object[] //no mail
            {
                new AddSupplierRequestDto()
                {
                    SupplierName = "abc",
                    Address = "abc",
                    PhoneNumber = "090000",
                    Email = "   ",
                    OtherInformation = ""
                }, "/ImageTest/jpgReal.jpg", false
            };
            yield return new object[] //no other information
            {
                new AddSupplierRequestDto()
                {
                    SupplierName = "abc",
                    Address = "abc",
                    PhoneNumber = "090000",
                    Email = "abc.abc@gmail.com",
                    OtherInformation = "   "
                }, "/ImageTest/jpgReal.jpg", true
            };
        }



        [Theory]
        [MemberData(nameof(UpdateSupplierData))]
        public async Task UpdateSupplier(AddSupplierRequestDto entity, string image, bool result)
        {
            //Arrange
            IFormFile? file = null;
            if (image != null)
            {
                //Setup mock file using a memory stream
                var currentPath = System.IO.Directory.GetCurrentDirectory().Split("\\bin")[0];
                var sourceImg = $@"{currentPath}{image}";

                file = FileHelper.GenFormFile($"{sourceImg}", "image/jpg", "data", "d");
                // Process file
                await using var ms = new MemoryStream();
                await file.CopyToAsync(ms);

            }
            //repo
            _baseWarehouseRepoMock.Setup(x => x.All()).ReturnsAsync(BaseWarehouseData.Data);
            _supplierRepoMock.Setup(x => x.GetById(It.IsAny<int>())).ReturnsAsync(SupplierData.Data.FirstOrDefault(x => x.SupplierId == entity.SupplierId));


            //Act
            var actual = await _supplierService.UpdateSupplier(entity, file).ConfigureAwait(false);


            //Assert
            Assert.Equal(result, actual);
            if (result)
            {
                _supplierRepoMock.Verify(x => x.Update(It.IsAny<WmsSupplier>()), Times.Once);
            }
        }

        public static IEnumerable<object[]> UpdateSupplierData()
        {
            yield return new object[]  //normal case
            {
                new AddSupplierRequestDto()
                {
                    SupplierId = 1,
                    SupplierName = "abc",
                    Address = "abc",
                    PhoneNumber = "0900000",
                    Email = "abc.abc@gmail.com",
                    OtherInformation = ""
                }, "/ImageTest/jpgReal.jpg", true
            };
            yield return new object[] //fake image
            {
                new AddSupplierRequestDto()
                {
                    SupplierId = 1,
                    SupplierName = "abc",
                    Address = "abc",
                    PhoneNumber = "0900000",
                    Email = "abc.abc@gmail.com",
                    OtherInformation = ""
                }, "/ImageTest/jpgFake.jpg", false
            };
            yield return new object[] //no image
            {
                new AddSupplierRequestDto()
                {
                    SupplierId = 1,
                    SupplierName = "abc",
                    Address = "abc",
                    PhoneNumber = "0900000",
                    Email = "abc.abc@gmail.com",
                    OtherInformation = ""
                }, null, true
            };
            yield return new object[] //no name
            {
                new AddSupplierRequestDto()
                {
                    SupplierId = 1,
                    SupplierName = "    ",
                    Address = "abc",
                    PhoneNumber = "0900000",
                    Email = "abc.abc@gmail.com",
                    OtherInformation = ""
                }, "/ImageTest/jpgReal.jpg", false
            };
            yield return new object[] //no address
            {
                new AddSupplierRequestDto()
                {
                    SupplierId = 1,
                    SupplierName = "abc",
                    Address = "   ",
                    PhoneNumber = "0900000",
                    Email = "abc.abc@gmail.com",
                    OtherInformation = ""
                }, "/ImageTest/jpgReal.jpg", true
            };
            yield return new object[] //no phone
            {
                new AddSupplierRequestDto()
                {
                    SupplierId = 1,
                    SupplierName = "abc",
                    Address = "abc",
                    PhoneNumber = "   ",
                    Email = "abc.abc@gmail.com",
                    OtherInformation = ""
                }, "/ImageTest/jpgReal.jpg", false
            };
            yield return new object[] //no mail
            {
                new AddSupplierRequestDto()
                {
                    SupplierId = 1,
                    SupplierName = "abc",
                    Address = "abc",
                    PhoneNumber = "090000",
                    Email = "   ",
                    OtherInformation = ""
                }, "/ImageTest/jpgReal.jpg", false
            };
            yield return new object[] //no other information
            {
                new AddSupplierRequestDto()
                {
                    SupplierId = 1,
                    SupplierName = "abc",
                    Address = "abc",
                    PhoneNumber = "090000",
                    Email = "abc.abc@gmail.com",
                    OtherInformation = "   "
                }, "/ImageTest/jpgReal.jpg", true
            };
            yield return new object[] //invalid id
            {
                new AddSupplierRequestDto()
                {
                    SupplierId = 0,
                    SupplierName = "abc",
                    Address = "abc",
                    PhoneNumber = "090000",
                    Email = "abc.abc@gmail.com",
                    OtherInformation = "   "
                }, "/ImageTest/jpgReal.jpg", false
            };
        }
    }
}
