﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using MockQueryable.Moq;
using Moq;
using System.Data;
using WMS.Business.Abstractions.Service;
using WMS.Business.Dtos.FilterDto;
using WMS.Business.Dtos.Mapper;
using WMS.Business.Dtos.StockCheckSheetDto;
using WMS.Business.Services;
using WMS.Domain.Abstractions;
using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Entities;

namespace WMS.UnitTest.Business
{
    public class StockCheckSheetServiceTest
    {
        private readonly StockCheckSheetService _stockCheckSheetService;
        private readonly Mock<IUnitOfWork> _unitOfWork = new Mock<IUnitOfWork>();
        private readonly Mock<IWmsUserRepository> _userRepoMock = new Mock<IWmsUserRepository>();
        private readonly Mock<IWmsBranchWarehouseRepository> _branchWarehouseRepoMock = new Mock<IWmsBranchWarehouseRepository>();
        private readonly Mock<IWmsBaseWarehouseRepository> _baseWarehouseRepoMock = new Mock<IWmsBaseWarehouseRepository>();
        private readonly Mock<IWmsBranchExportRepository> _branchExportRepoMock = new Mock<IWmsBranchExportRepository>();
        private readonly Mock<IWmsBranchExportProductRepository> _branchExportProductRepoMock = new Mock<IWmsBranchExportProductRepository>();
        private readonly Mock<IWmsSupplierRepository> _supplierRepoMock = new Mock<IWmsSupplierRepository>();
        private readonly Mock<IWmsProductRepository> _productRepoMock = new Mock<IWmsProductRepository>();
        private readonly Mock<IWmsBranchDebtRepository> _branchDebtRepoMock = new Mock<IWmsBranchDebtRepository>();
        private readonly Mock<IWmsBranchInvoiceRepository> _branchInvoiceRepoMock = new Mock<IWmsBranchInvoiceRepository>();
        private readonly Mock<IWmsBaseInvoiceRepository> _baseInvoiceRepoMock = new Mock<IWmsBaseInvoiceRepository>();
        private readonly Mock<IWmsBranchProductReturnRepository> _branchReturnRepoMock = new Mock<IWmsBranchProductReturnRepository>();
        private readonly Mock<IWmsBranchImportRepository> _branchImportRepoMock = new Mock<IWmsBranchImportRepository>();
        private readonly Mock<IWmsBranchImportProductRepository> _branchImportProductRepoMock = new Mock<IWmsBranchImportProductRepository>();
        private readonly Mock<IWmsBranchProductReturnDetailRepository> _branchProductReturnDetailRepoMock = new Mock<IWmsBranchProductReturnDetailRepository>();
        private readonly Mock<IWmsBranchProductReturnRepository> _branchProductReturnRepoMock = new Mock<IWmsBranchProductReturnRepository>();
        private readonly Mock<IWmsBaseImportProductRepository> _baseImportProductRepoMock = new Mock<IWmsBaseImportProductRepository>();
        private readonly Mock<IWmsBranchRequestRepository> _branchRequestRepoMock = new Mock<IWmsBranchRequestRepository>();
        private readonly Mock<IWmsBaseStockCheckSheetRepository> _baseStockCheckSheetMock = new Mock<IWmsBaseStockCheckSheetRepository>();
        private readonly Mock<IWmsBranchStockCheckSheetRepository> _branchStockCheckSheetMock = new Mock<IWmsBranchStockCheckSheetRepository>();
        private readonly Mock<IWmsBranchWarehouseUserRepository> _branchWarehouseUserRepoMock = new Mock<IWmsBranchWarehouseUserRepository>();


        private readonly IMock<ILogger<StockCheckSheetService>> _logger = new Mock<ILogger<StockCheckSheetService>>();
        private readonly Mock<IS3StorageService> _storageService = new Mock<IS3StorageService>();
        private readonly Mock<IDateService> _dateService = new Mock<IDateService>();
        private readonly Mock<IProductService> _productService = new Mock<IProductService>();
        private readonly Mock<IInvoiceService> _invoiceServiceMock = new Mock<IInvoiceService>();
        private readonly Mock<IWarehouseService> _warehouseService = new Mock<IWarehouseService>();

        public StockCheckSheetServiceTest()
        {
            var mockTran = new Mock<IDbTransaction>();

            _unitOfWork.Setup(x => x.BeginTransaction()).Returns(mockTran.Object);
            _unitOfWork.Setup(x => x.UserRepository).Returns(_userRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchWarehouseRepository).Returns(_branchWarehouseRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchWarehouseRepository).Returns(_branchWarehouseRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchExportRepository).Returns(_branchExportRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchExportProductRepository).Returns(_branchExportProductRepoMock.Object);
            _unitOfWork.Setup(x => x.SupplierRepository).Returns(_supplierRepoMock.Object);
            _unitOfWork.Setup(x => x.ProductRepository).Returns(_productRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchDebtRepository).Returns(_branchDebtRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchInvoiceRepository).Returns(_branchInvoiceRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchInvoiceRepository).Returns(_branchInvoiceRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchProductReturnRepository).Returns(_branchReturnRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchImportRepository).Returns(_branchImportRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchProductReturnDetailRepository).Returns(_branchProductReturnDetailRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchProductReturnRepository).Returns(_branchProductReturnRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchImportProductRepository).Returns(_branchImportProductRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchRequestRepository).Returns(_branchRequestRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchImportProductRepository).Returns(_branchImportProductRepoMock.Object);
            _unitOfWork.Setup(x => x.BaseStockCheckSheetRepository).Returns(_baseStockCheckSheetMock.Object);
            _unitOfWork.Setup(x => x.BranchWarehouseUserRepository).Returns(_branchWarehouseUserRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchStockCheckSheetRepository).Returns(_branchStockCheckSheetMock.Object);

            var configuration = new MapperConfiguration(config =>
            {
                config.AddProfile<DomainToDtoProfile>();
                config.AddProfile<DtoToDomainProfile>();
            });
            var mapper = new Mapper(configuration);

            _stockCheckSheetService = new StockCheckSheetService(_unitOfWork.Object, mapper, _storageService.Object,
               _logger.Object, _dateService.Object);
        }

        //[Fact]
        //public async Task GetAllStockCheckSheet_ReturnsCorrectResult()
        //{
        //    // Arrange
        //    var entity = new StockCheckSheetRequestDto() { 
        //        SearchWord = "CHECK_ID_1",
        //        DateFilter = 0,
        //        PagingRequest = new WMS.Business.Dtos.CommonDto.PagingRequest
        //        {
        //            CurrentPage = 1,
        //            PageRange = 5,
        //            PageSize = 10
        //        },
        //        RoleId = 2
        //    };
        //    var currentUser = "TestUser";

        //    var dateServiceMock = new Mock<IDateService>();

        //    dateServiceMock.Setup(ds => ds.GetDateTimeByDateFilter(It.IsAny<short>()))
        //                   .Returns(new DateFilterRequestDto { StartDate = DateTime.Now, EndDate = DateTime.Now });

        //    // Set up the mocks for repository methods
        //    var baseStockCheckSheet = new List<WmsBaseStockCheckSheet>
        //    {
        //        new WmsBaseStockCheckSheet
        //        {
        //            CheckId = Guid.NewGuid(),
        //            StockCheckSheetCode = "CHECK_ID_1"
        //        },
        //        new WmsBaseStockCheckSheet
        //        {
        //            CheckId = Guid.NewGuid(),
        //            StockCheckSheetCode = "CHECK_ID_2"
        //        },
        //    };

        //    var branchStockCheckSheet = new List<WmsBranchStockCheckSheet>
        //    {
        //        new WmsBranchStockCheckSheet
        //        {
        //            CheckId = Guid.NewGuid(),
        //            StockCheckSheetCode = "CHECK_ID_1"
        //        },
        //        new WmsBranchStockCheckSheet
        //        {
        //            CheckId = Guid.NewGuid(),
        //            StockCheckSheetCode = "CHECK_ID_2"
        //        },
        //    };

        //    var userId = Guid.NewGuid();
        //    var user = new List<WmsUser>
        //    {
        //        new WmsUser
        //        {
        //            UserId = userId,
        //            Username = "Test"
        //        }
        //    };

        //    var branchWarehouseUser = new List<WmsBranchWarehouseUser>
        //    {
        //        new WmsBranchWarehouseUser
        //        {
        //            UserId = userId,
        //            BranchWarehouseId = Guid.NewGuid(),
        //            IsDelete = false,
        //        }
        //    };
        //    _baseStockCheckSheetMock.Setup(repo => repo.SearchBaseStockCheckSheet(It.IsAny<string>(), It.IsAny<DateTime>(), It.IsAny<DateTime>()))
        //                                    .Returns(baseStockCheckSheet.AsQueryable().BuildMock());
        //    _branchStockCheckSheetMock.Setup(repo => repo.SearchBranchStockCheckSheet(It.IsAny<string>(), It.IsAny<Guid>(), It.IsAny<DateTime>(), It.IsAny<DateTime>()))
        //                                      .Returns(branchStockCheckSheet.AsQueryable().BuildMock());
        //    _userRepoMock.Setup(repo => repo.GetUserWithRoleByUsername(It.IsAny<string>(), It.IsAny<short>()))
        //                      .Returns(user.AsQueryable().BuildMock());
        //    _branchWarehouseUserRepoMock.Setup(repo => repo.GetBranchWarehouseByUserId(It.IsAny<Guid>()))
        //                                    .Returns(branchWarehouseUser.AsQueryable().BuildMock());

        //    // Act
        //    var result = await _stockCheckSheetService.GetAllStockCheckSheet(entity, currentUser);

        //    Assert.NotNull(result);
        //}
    }
}
