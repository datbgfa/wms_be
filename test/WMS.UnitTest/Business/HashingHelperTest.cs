﻿using WMS.Business.Utils.StringUtils;

namespace WMS.UnitTest.Business
{
    public class HashingHelperTest
    {
        [Fact]
        public void EncryptPassword_ValidPasswordAndSalt_ReturnsExpectedHash()
        {
            // Arrange
            string originPassword = "12345";
            string salt = "abcdefghik";

            // Act
            string encryptedPassword = HashingHelper.EncryptPassword(originPassword, salt);

            // Assert
            // Ensure the result is not null or empty
            Assert.False(string.IsNullOrWhiteSpace(encryptedPassword));

            // Compare with an expected hash (you should replace this with the actual hash)
            string expectedHash = "f3dce89cf29bbed5055a9d5be8f0cf889f1098556b05740001b85f2fc97c02fe";
            Assert.Equal(expectedHash, encryptedPassword);
        }

        [Fact]
        public void EncryptPassword_NullPassword_ThrowsArgumentNullException()
        {
            // Arrange
            string originPassword = null;
            string salt = "abcdefghik";

            // Act and Assert
            Assert.Throws<ArgumentNullException>(() => HashingHelper.EncryptPassword(originPassword, salt));
        }

        [Fact]
        public void VerifyPassword_ValidPasswordAndSalt_ReturnTrue()
        {
            // Arrange
            string originPassword = "12345";
            string salt = "abcdefghik";
            string hashedPassword = "f3dce89cf29bbed5055a9d5be8f0cf889f1098556b05740001b85f2fc97c02fe";

            //Act
            var verify = HashingHelper.VerifyPassword(originPassword, hashedPassword, salt);

            //Assert
            Assert.True(verify);
        }

        [Fact]
        public void VerifyPassword_InvalidPassword_ReturnFalse()
        {
            // Arrange
            string originPassword = "12345abc";
            string salt = "abcdefghik";
            string hashedPassword = "f3dce89cf29bbed5055a9d5be8f0cf889f1098556b05740001b85f2fc97c02fe";

            //Act
            var verify = HashingHelper.VerifyPassword(originPassword, hashedPassword, salt);

            //Assert
            Assert.False(verify);
        }

        [Fact]
        public void VerifyPassword_ValidPasswordAndSaltNull_ReturnTrue()
        {
            // Arrange
            string originPassword = "12345abc";
            string salt = null;
            string hashedPassword = "f3dce89cf29bbed5055a9d5be8f0cf889f1098556b05740001b85f2fc97c02fe";

            //Act
            var verify = HashingHelper.VerifyPassword(originPassword, hashedPassword, salt);

            //Assert
            Assert.False(verify);
        }
    }
}
