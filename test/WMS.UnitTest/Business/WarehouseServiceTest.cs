﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Moq;
using System.Data;
using WMS.Business.Abstractions.Service;
using WMS.Business.Dtos.Mapper;
using WMS.Business.Dtos.WarehouseDto;
using WMS.Business.Services;
using WMS.Domain.Abstractions;
using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Entities;
using WMS.UnitTest.DummiesData;

namespace WMS.UnitTest.Business
{

    public class WarehouseServiceTest
    {
        private readonly WarehouseService _warehouseService;
        private readonly Mock<IUnitOfWork> _unitOfWork = new Mock<IUnitOfWork>();
        private readonly Mock<IS3StorageService> _s3StorageServiceMock = new Mock<IS3StorageService>();
        private readonly Mock<IWmsBaseWarehouseRepository> _baseWarehouseRepoMock = new Mock<IWmsBaseWarehouseRepository>();
        private readonly Mock<IWmsBranchWarehouseRepository> _branchWarehouseRepoMock = new Mock<IWmsBranchWarehouseRepository>();
        private readonly Mock<IWmsUserRepository> _userRepoMock = new Mock<IWmsUserRepository>();
        private readonly Mock<IUserService> _userServiceMock = new Mock<IUserService>();

        private readonly IMock<ILogger<WarehouseService>> _logger = new Mock<ILogger<WarehouseService>>();

        //PHAN NAY CHI SET UP CHUNG CHO CA SERVICE, DATA RIENG SET UP RIENG TRONG TUNG METHOD NHE
        public WarehouseServiceTest()
        {

            //Setup mock file using a memory stream
            var content = "Hello World from a Fake File";
            var fileName = "test.pdf";
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(content);
            writer.Flush();
            stream.Position = 0;

            //create FormFile with desired data
            IFormFile file = new FormFile(stream, 0, stream.Length, "id_from_form", fileName);



            var mockTran = new Mock<IDbTransaction>();

            _unitOfWork.Setup(x => x.BeginTransaction()).Returns(mockTran.Object);
            _unitOfWork.Setup(x => x.BaseWarehouseRepository).Returns(_baseWarehouseRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchWarehouseRepository).Returns(_branchWarehouseRepoMock.Object);
            _unitOfWork.Setup(x => x.UserRepository).Returns(_userRepoMock.Object);

            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(new DomainToDtoProfile()));
            var mapper = new Mapper(configuration);

            _warehouseService = new WarehouseService(_unitOfWork.Object, _logger.Object, mapper, _userServiceMock.Object);
        }

        [Theory]
        [InlineData("thond", true, 2)]
        [InlineData("thond", false, 4)]
        public async Task ViewWarehouseProfileUnitTest(string username, bool isBase, int roleId)
        {
            IEnumerable<object> list = new List<object>() { 1, 4, 5 }.AsEnumerable();

            var userRoles = new List<WmsUserRole>() {
                new WmsUserRole(){
                UserRoleId = Guid.Parse("36103f08-72ad-440b-b535-ac8f846c1b0d"),
                RoleId = roleId,
                UserId = Guid.Parse("36103f08-72ad-440b-b535-ac8f846c1b0d"),
                },
             };


            //set up for GetProfileInformationByUsername
            var userDefault = new WmsUser()
            {
                UserId = Guid.Parse("36103f08-72ad-440b-b535-ac8f846c1b0d"),
                Fullname = "Thodz",
                Email = "tkokoi123@gmail.com",
                Gender = true,
                PhoneNumber = "0349019012",
                Birthdate = DateTime.Parse("2023-10-19"),
                Address = "Ha noi",
                Avatar = "Avatar/thond_5a1c8ba4-33e0-4146-9b85-87529c4acf1e.JPG",
                Username = "thond",
                UserRole = userRoles
            };
            var branchW = new WmsBranchWarehouse()
            {
                Email = "Branch@gmail.com"
            };
            var baseW = new WmsBaseWarehouse()
            {
                Email = "Base@gmail.com"
            };


            _userRepoMock.Setup(ur => ur.GetUserContainRoleByUsername(It.IsAny<string>())).ReturnsAsync(userDefault);
            _branchWarehouseRepoMock.Setup(bwr => bwr.FindBranchWarehouseByWarehouseOwner(It.IsAny<Guid>())).ReturnsAsync(branchW);
            _baseWarehouseRepoMock.Setup(bwr => bwr.FindBaseWarehouseByWarehouseOwner(It.IsAny<Guid>())).ReturnsAsync(baseW);

            //Act
            var warehouse = await _warehouseService.ViewWarehouseProfile(username);

            if (isBase)
            {
                _baseWarehouseRepoMock.Verify(bwr => bwr.FindBaseWarehouseByWarehouseOwner(It.IsAny<Guid>()), Times.Once);
                _branchWarehouseRepoMock.Verify(bwr => bwr.FindBranchWarehouseByWarehouseOwner(It.IsAny<Guid>()), Times.Never);
                Assert.Equal(warehouse.Email, "Base@gmail.com");
            }
            else
            {
                _baseWarehouseRepoMock.Verify(bwr => bwr.FindBaseWarehouseByWarehouseOwner(It.IsAny<Guid>()), Times.Never);
                _branchWarehouseRepoMock.Verify(bwr => bwr.FindBranchWarehouseByWarehouseOwner(It.IsAny<Guid>()), Times.Once);
                Assert.Equal(warehouse.Email, "Branch@gmail.com");

            }
        }
        [Theory]
        [InlineData("thond", true, false, 2)]
        [InlineData("thond", false, true, 4)]
        [InlineData("thond", false, false, 3)]
        public async Task EditWarehouseProfileUnitTest(string username, bool isBase, bool isBranch, int roleId)
        {
            IEnumerable<object> list = new List<object>() { 1, 4, 5 }.AsEnumerable();

            var userRoles = new List<WmsUserRole>() {
                new WmsUserRole(){
                UserRoleId = Guid.Parse("36103f08-72ad-440b-b535-ac8f846c1b0d"),
                RoleId = roleId,
                UserId = Guid.Parse("36103f08-72ad-440b-b535-ac8f846c1b0d"),
                },
             };


            //set up for GetProfileInformationByUsername
            var userDefault = new WmsUser()
            {
                UserId = Guid.Parse("36103f08-72ad-440b-b535-ac8f846c1b0d"),
                Fullname = "Thodz",
                Email = "tkokoi123@gmail.com",
                Gender = true,
                PhoneNumber = "0349019012",
                Birthdate = DateTime.Parse("2023-10-19"),
                Address = "Ha noi",
                Avatar = "Avatar/thond_5a1c8ba4-33e0-4146-9b85-87529c4acf1e.JPG",
                Username = "thond",
                UserRole = userRoles
            };
            var branchW = new WmsBranchWarehouse()
            {
                WarehouseId = Guid.Parse("36103f08-72ad-440b-b535-ac8f846c1b0d"),
                Email = "Branch@gmail.com"
            };
            var baseW = new WmsBaseWarehouse()
            {
                WarehouseId = Guid.Parse("36103f08-72ad-440b-b535-ac8f846c1b0d"),
                Email = "Base@gmail.com"
            };


            _userRepoMock.Setup(ur => ur.GetUserContainRoleByUsername(It.IsAny<string>())).ReturnsAsync(userDefault);
            _branchWarehouseRepoMock.Setup(bwr => bwr.GetById(It.IsAny<Guid>())).ReturnsAsync(branchW);
            _baseWarehouseRepoMock.Setup(bwr => bwr.GetById(It.IsAny<Guid>())).ReturnsAsync(baseW);

            //Act
            var warehouse = await _warehouseService.EditWarehouseProfile(new EditWarehouseProfileRequestDto()
            {
                WarehouseId = Guid.Parse("36103f08-72ad-440b-b535-ac8f846c1b0d"),
            }, username);

            if (isBase)
            {
                _baseWarehouseRepoMock.Verify(bw => bw.GetById(It.IsAny<Guid>()), Times.Once);
                _baseWarehouseRepoMock.Verify(bwr => bwr.Update(It.IsAny<WmsBaseWarehouse>()), Times.Once);
                _branchWarehouseRepoMock.Verify(bwr => bwr.Update(It.IsAny<WmsBranchWarehouse>()), Times.Never);
                Assert.Equal(warehouse, true);
            }
            if (isBranch)
            {
                _branchWarehouseRepoMock.Verify(bw => bw.GetById(It.IsAny<Guid>()), Times.Once);
                _branchWarehouseRepoMock.Verify(bwr => bwr.Update(It.IsAny<WmsBranchWarehouse>()), Times.Once);
                _baseWarehouseRepoMock.Verify(bwr => bwr.Update(It.IsAny<WmsBaseWarehouse>()), Times.Never);
                Assert.Equal(warehouse, true);

            }
            if (roleId != 2 && roleId != 4)
            {
                Assert.Equal(warehouse, false);
            }
        }

        [Fact]
        public async Task ViewAllBaseWarehousesList_BothHasOwnerAndNoOwner()
        {
            //Arrange
            var baseWarehouse = BaseWarehouseData.Data.First();
            baseWarehouse.WmsWarehouseOwner = UserData.Data.First();
            _baseWarehouseRepoMock.Setup(x => x.GetBaseWarehouseIncludingOwner()).Returns(BaseWarehouseData.Data.AsAsyncQueryable());

            //Act
            var rs = await _warehouseService.ViewAllBaseWarehousesList().ConfigureAwait(false);

            //Assert
            Assert.NotNull(rs);
        }
    }
}
