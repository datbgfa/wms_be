﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using MockQueryable.Moq;
using Moq;
using MoqExpression;
using System.Data;
using System.Dynamic;
using System.Linq.Expressions;
using WMS.Business.Abstractions.Service;
using WMS.Business.Dtos.BaseImportDto;
using WMS.Business.Dtos.CommonDto;
using WMS.Business.Dtos.FilterDto;
using WMS.Business.Dtos.Mapper;
using WMS.Business.Dtos.ProductDto;
using WMS.Business.Dtos.ProductReturnFromBranchDto;
using WMS.Business.Dtos.S3Dto;
using WMS.Business.Services;
using WMS.Domain.Abstractions;
using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Constant;
using WMS.Domain.Entities;
using WMS.UnitTest.DummiesData;
using WMS.UnitTest.ImageTest;

namespace WMS.UnitTest.Business
{

    public class ProductServiceTest
    {
        private readonly ProductService _productService;
        private readonly Mock<IUnitOfWork> _unitOfWork = new Mock<IUnitOfWork>();
        private readonly Mock<IS3StorageService> _s3StorageServiceMock = new Mock<IS3StorageService>();
        private readonly Mock<IWmsProductRepository> _productRepoMock = new Mock<IWmsProductRepository>();
        private readonly Mock<IWmsProductAttributeRepository> _productAttributeRepoMock = new Mock<IWmsProductAttributeRepository>();
        private readonly IMock<ILogger<ProductService>> _logger = new Mock<ILogger<ProductService>>();
        private readonly Mock<IWmsUserRepository> _userRepoMock = new Mock<IWmsUserRepository>();
        private readonly Mock<IWmsBaseWarehouseRepository> _baseWarehouseRepoMock = new Mock<IWmsBaseWarehouseRepository>();
        private readonly Mock<IWmsBranchWarehouseRepository> _branchWarehouseRepoMock = new Mock<IWmsBranchWarehouseRepository>();
        private readonly Mock<IWmsBaseImportRepository> _baseImportRepoMock = new Mock<IWmsBaseImportRepository>();
        private readonly Mock<IWmsBaseImportProductRepository> _baseImportProductRepoMock = new Mock<IWmsBaseImportProductRepository>();
        private readonly Mock<IWmsSupplierRepository> _supplierRepoMock = new Mock<IWmsSupplierRepository>();
        private readonly Mock<IWmsBaseDebtRepository> _baseDebtRepoMock = new Mock<IWmsBaseDebtRepository>();
        private readonly Mock<IWmsBaseInvoiceRepository> _baseInvoiceRepoMock = new Mock<IWmsBaseInvoiceRepository>();
        private readonly Mock<IWmsBranchInvoiceRepository> _branchInvoiceRepoMock = new Mock<IWmsBranchInvoiceRepository>();
        private readonly Mock<IWmsBaseProductReturnRepository> _baseReturnRepoMock = new Mock<IWmsBaseProductReturnRepository>();
        private readonly Mock<IWmsCategoryRepository> _categoryRepoMock = new Mock<IWmsCategoryRepository>();
        private readonly Mock<IWmsBaseProductReturnDetailRepository> _baseReturnDetailRepoMock = new Mock<IWmsBaseProductReturnDetailRepository>();
        private readonly Mock<IDateService> _dateService = new Mock<IDateService>();
        private readonly Mock<IWarehouseService> _warehouseService = new Mock<IWarehouseService>();
        private readonly Mock<IWmsBranchProductReturnRepository> _branchProductReturnRepoMock = new Mock<IWmsBranchProductReturnRepository>();
        private readonly Mock<IWmsBranchProductReturnDetailRepository> _branchProductReturnDetailRepoMock = new Mock<IWmsBranchProductReturnDetailRepository>();
        private readonly Mock<IWmsBranchImportRepository> _branchImportRepoMock = new Mock<IWmsBranchImportRepository>();
        private readonly Mock<IWmsBranchImportProductRepository> _branchImportProductRepoMock = new Mock<IWmsBranchImportProductRepository>();
        private readonly Mock<IWmsBranchDebtRepository> _branchDebtRepoMock = new Mock<IWmsBranchDebtRepository>();
        private readonly Mock<IWmsBaseExportRepository> _baseExportRepoMock = new Mock<IWmsBaseExportRepository>();


        public ProductServiceTest()
        {

            //Setup mock file using a memory stream
            var content = "Hello World from a Fake File";
            var fileName = "test.pdf";
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(content);
            writer.Flush();
            stream.Position = 0;

            //create FormFile with desired data
            IFormFile file = new FormFile(stream, 0, stream.Length, "id_from_form", fileName);



            var mockTran = new Mock<IDbTransaction>();

            _unitOfWork.Setup(x => x.BeginTransaction()).Returns(mockTran.Object);
            _unitOfWork.Setup(x => x.ProductRepository).Returns(_productRepoMock.Object);
            _unitOfWork.Setup(x => x.ProductAttributeRepository).Returns(_productAttributeRepoMock.Object);
            _unitOfWork.Setup(x => x.UserRepository).Returns(_userRepoMock.Object);
            _unitOfWork.Setup(x => x.BaseWarehouseRepository).Returns(_baseWarehouseRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchWarehouseRepository).Returns(_branchWarehouseRepoMock.Object);
            _unitOfWork.Setup(x => x.BaseImportRepository).Returns(_baseImportRepoMock.Object);
            _unitOfWork.Setup(x => x.BaseImportProductRepository).Returns(_baseImportProductRepoMock.Object);
            _unitOfWork.Setup(x => x.SupplierRepository).Returns(_supplierRepoMock.Object);
            _unitOfWork.Setup(x => x.ProductRepository).Returns(_productRepoMock.Object);
            _unitOfWork.Setup(x => x.BaseDebtRepository).Returns(_baseDebtRepoMock.Object);
            _unitOfWork.Setup(x => x.BaseInvoiceRepository).Returns(_baseInvoiceRepoMock.Object);
            _unitOfWork.Setup(x => x.BaseProductReturnRepository).Returns(_baseReturnRepoMock.Object);
            _unitOfWork.Setup(x => x.BaseProductReturnDetailRepository).Returns(_baseReturnDetailRepoMock.Object);
            _unitOfWork.Setup(x => x.CategoryRepository).Returns(_categoryRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchProductReturnRepository).Returns(_branchProductReturnRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchProductReturnDetailRepository).Returns(_branchProductReturnDetailRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchImportRepository).Returns(_branchImportRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchImportProductRepository).Returns(_branchImportProductRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchDebtRepository).Returns(_branchDebtRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchInvoiceRepository).Returns(_branchInvoiceRepoMock.Object);
            _unitOfWork.Setup(x => x.BaseExportRepository).Returns(_baseExportRepoMock.Object);

            var configuration = new MapperConfiguration(config =>
            {
                config.AddProfile<DomainToDtoProfile>();
                config.AddProfile<DtoToDomainProfile>();
            });
            var mapper = new Mapper(configuration);

            _productService = new ProductService(_unitOfWork.Object, mapper, _s3StorageServiceMock.Object, _logger.Object, _dateService.Object, _warehouseService.Object);
        }

        [Theory]
        [InlineData("/ImageTest/jpgReal.jpg")]
        [InlineData("/ImageTest/jpgFake.jpg")]
        public async Task CreateProductUnitTest(string nameImg)
        {
            // Arrange.
            var currentPath = System.IO.Directory.GetCurrentDirectory().Split("\\bin")[0];
            var sourceImg = $@"{currentPath}{nameImg}";

            var ifromFile = FileHelper.GenFormFile($"{sourceImg}", "image/jpg", "data", "d");
            // Process file
            await using var ms = new MemoryStream();
            await ifromFile.CopyToAsync(ms);

            var createProductRequestDto = new CreateProductRequestDto()
            {
                ProductName = "Ao hoa",
                CategoryId = 1,
            };
            _categoryRepoMock.Setup(x => x.GetById(1)).ReturnsAsync(new WmsCategory() { CategoryId = 1 });

            //Act
            var resultCreateProduct = await _productService.CreateProduct(createProductRequestDto, ifromFile).ConfigureAwait(false);


            if (nameImg.Equals("/ImageTest/jpgFake.jpg"))
            {
                _s3StorageServiceMock.Verify(s3 => s3.UploadFileAsync(It.IsAny<S3RequestData>()), Times.Never);
                _unitOfWork.Verify(uow => uow.ProductRepository.AddAsync(It.IsAny<WmsProduct>()), Times.Never);
                //_unitOfWork.Verify(uow => uow.ProductAttributeRepository.AddRangeAsync(It.IsAny<IEnumerable<WmsProductAttribute>>()), Times.Never);
                Assert.False(resultCreateProduct);
            }
            if (nameImg.Equals("/ImageTest/jpgReal.jpg"))
            {
                _s3StorageServiceMock.Verify(s3 => s3.UploadFileAsync(It.IsAny<S3RequestData>()), Times.Once);
                _unitOfWork.Verify(uow => uow.ProductRepository.AddAsync(It.IsAny<WmsProduct>()), Times.Once);
                //_unitOfWork.Verify(uow => uow.ProductAttributeRepository.AddRangeAsync(It.IsAny<IEnumerable<WmsProductAttribute>>()), Times.Once);
                Assert.True(resultCreateProduct);
            }
        }

        [Fact]
        public async Task CreateProductUnitTest_CategoryDoesNotExist()
        {
            // Arrange.
            var createProductRequestDto = new CreateProductRequestDto()
            {
                ProductName = "Ao hoa",
                CategoryId = 0,
            };

            var resultCreateProduct = await _productService.CreateProduct(createProductRequestDto, null).ConfigureAwait(false);

            _s3StorageServiceMock.Verify(s3 => s3.UploadFileAsync(It.IsAny<S3RequestData>()), Times.Never);
            _unitOfWork.Verify(uow => uow.ProductRepository.AddAsync(It.IsAny<WmsProduct>()), Times.Never);
            Assert.False(resultCreateProduct);
        }

        [Fact]
        public async Task CreateBaseReturn_ImportNotPaid()
        {
            //Arrange
            var baseImport = BaseImportData.Data.First();
            baseImport.ImportStatus = ImportStatusConstant.Imported;

            var entity = new CreateProducReturnSupplierRequestDto()
            {
                ImportId = baseImport.ImportId,
                ProductRequest = new List<CreateProductReturnSupplierRequestDetailDto>()
                {
                    new CreateProductReturnSupplierRequestDetailDto(){ ProductId = Guid.Parse("b08d504e-8e0b-47b9-a51c-50adf96a2338"), RequestQuantity = 1}
                }
            };

            var baseImportProducts = new List<WmsBaseImportProduct>() { new WmsBaseImportProduct()
                {
                    ImportId = baseImport.ImportId,
                    ProductId = Guid.Parse("b08d504e-8e0b-47b9-a51c-50adf96a2338"),
                    CostPrice = 55000,
                    UnitPrice = 50000,
                    ImportedQuantity = 4,
                    StockQuantity = 4,
                }
            };
            var baseDebt = new WmsBaseDebt()
            {
                DebtAmount = 500000,
            };

            _supplierRepoMock.Setup(x => x.GetById(It.IsAny<int>())).ReturnsAsync(SupplierData.Data.First());
            _baseImportProductRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBaseImportProduct, bool>>>()))
                .ReturnsAsync(baseImportProducts.First);
            _baseImportRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBaseImport, bool>>>())).ReturnsAsync(baseImport);
            _baseDebtRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBaseDebt, bool>>>())).ReturnsAsync(baseDebt);

            //Act
            var result = await _productService.CreateBaseReturn(entity, UserData.Data.First().Fullname);

            //Assert
            Assert.True(result);
            Assert.Equal(450000, baseDebt.DebtAmount);
            _baseDebtRepoMock.Verify(x => x.Update(It.IsAny<WmsBaseDebt>()), Times.Once);
            _baseReturnRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBaseProductReturn>()), Times.Once);
            _baseInvoiceRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBaseInvoice>()), Times.Exactly(3));
            _baseReturnDetailRepoMock.Verify(x => x.AddRangeAsync(It.IsAny<IEnumerable<WmsBaseProductReturnDetail>>()), Times.Once);
            _baseImportProductRepoMock.Verify(x => x.Update(It.IsAny<WmsBaseImportProduct>()), Times.Exactly(entity.ProductRequest.Count()));
            Assert.Equal(50000, baseImport.PaidAmount);
        }

        [Fact]
        public async Task CreateBaseReturn_ImportIsPaid()  //fail
        {
            //Arrange
            var baseImport = BaseImportData.Data.First();
            baseImport.ImportStatus = ImportStatusConstant.Imported;
            baseImport.PaymentStatus = PaymentStatusConstant.Paid;
            baseImport.PaidAmount = 220000;

            var entity = new CreateProducReturnSupplierRequestDto()
            {
                ImportId = baseImport.ImportId,
                ProductRequest = new List<CreateProductReturnSupplierRequestDetailDto>()
                {
                    new CreateProductReturnSupplierRequestDetailDto(){ ProductId = Guid.Parse("b08d504e-8e0b-47b9-a51c-50adf96a2338"), RequestQuantity = 1}
                }
            };

            var baseImportProducts = new List<WmsBaseImportProduct>() { new WmsBaseImportProduct()
                {
                    ImportId = baseImport.ImportId,
                    ProductId = Guid.Parse("b08d504e-8e0b-47b9-a51c-50adf96a2338"),
                    CostPrice = 55000,
                    UnitPrice = 50000,
                    ImportedQuantity = 4,
                    StockQuantity = 4,
                }
            };
            var baseDebt = new WmsBaseDebt()
            {
                DebtAmount = 500000,
            };

            _supplierRepoMock.Setup(x => x.GetById(It.IsAny<int>())).ReturnsAsync(SupplierData.Data.First());
            _baseImportProductRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBaseImportProduct, bool>>>()))
                .ReturnsAsync(baseImportProducts.First);
            _baseImportRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBaseImport, bool>>>())).ReturnsAsync(baseImport);
            _baseDebtRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBaseDebt, bool>>>())).ReturnsAsync(baseDebt);

            //Act
            var result = await _productService.CreateBaseReturn(entity, UserData.Data.First().Fullname);

            //Assert
            Assert.True(result);
            Assert.Equal(450000, baseDebt.DebtAmount);
            _baseDebtRepoMock.Verify(x => x.Update(It.IsAny<WmsBaseDebt>()), Times.Once);
            _baseReturnRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBaseProductReturn>()), Times.Once);
            _baseInvoiceRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBaseInvoice>()), Times.Once);
            _baseReturnDetailRepoMock.Verify(x => x.AddRangeAsync(It.IsAny<IEnumerable<WmsBaseProductReturnDetail>>()), Times.Once);
            _baseImportProductRepoMock.Verify(x => x.Update(It.IsAny<WmsBaseImportProduct>()), Times.Exactly(entity.ProductRequest.Count()));
            Assert.Equal(220000, baseImport.PaidAmount);
        }

        [Fact]
        public async Task CreateBaseReturn_InvalidImportId()
        {
            //Arrange
            var baseImport = BaseImportData.Data.First();
            baseImport.ImportStatus = ImportStatusConstant.Imported;
            var entity = new CreateProducReturnSupplierRequestDto()
            {
                ImportId = Guid.Empty,
                ProductRequest = new List<CreateProductReturnSupplierRequestDetailDto>()
                {
                    new CreateProductReturnSupplierRequestDetailDto(){ ProductId = Guid.Parse("b08d504e-8e0b-47b9-a51c-50adf96a2338"), RequestQuantity = 1}
                }
            };
            var baseImportProducts = new List<WmsBaseImportProduct>() { new WmsBaseImportProduct()
                {
                    ImportId = baseImport.ImportId,
                    ProductId = Guid.Parse("b08d504e-8e0b-47b9-a51c-50adf96a2338"),
                    CostPrice = 55000,
                    UnitPrice = 50000,
                    ImportedQuantity = 4,
                    StockQuantity = 4,
                }
            };
            var baseDebt = new WmsBaseDebt()
            {
                DebtAmount = 500000,
            };

            _supplierRepoMock.Setup(x => x.GetById(It.IsAny<int>())).ReturnsAsync(SupplierData.Data.First());
            _baseImportProductRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBaseImportProduct, bool>>>()))
                .ReturnsAsync(baseImportProducts.First);
            _baseImportRepoMock.Setup(x => x.FindObject(
                MoqHelper.IsExpression<WmsBaseImport>(x => x.ImportId.Equals(baseImport.ImportId)))).ReturnsAsync(baseImport);
            _baseDebtRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBaseDebt, bool>>>())).ReturnsAsync(baseDebt);

            //Act
            var result = await _productService.CreateBaseReturn(entity, UserData.Data.First().Fullname);

            //Assert
            Assert.False(result);
            Assert.Equal(500000, baseDebt.DebtAmount);
            _baseDebtRepoMock.Verify(x => x.Update(It.IsAny<WmsBaseDebt>()), Times.Never);
            _baseReturnRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBaseProductReturn>()), Times.Never);
            _baseInvoiceRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBaseInvoice>()), Times.Never);
            _baseReturnDetailRepoMock.Verify(x => x.AddRangeAsync(It.IsAny<IEnumerable<WmsBaseProductReturnDetail>>()), Times.Never);
            _baseImportProductRepoMock.Verify(x => x.Update(It.IsAny<WmsBaseImportProduct>()), Times.Never);
            Assert.Equal(0, baseImport.PaidAmount);
        }

        [Fact]
        public async Task CreateBaseReturn_InvalidProductId()
        {
            //Arrange
            var baseImport = BaseImportData.Data.First();
            baseImport.ImportStatus = ImportStatusConstant.Imported;
            var entity = new CreateProducReturnSupplierRequestDto()
            {
                ImportId = baseImport.ImportId,
                ProductRequest = new List<CreateProductReturnSupplierRequestDetailDto>()
                {
                    new CreateProductReturnSupplierRequestDetailDto(){ ProductId = Guid.Empty, RequestQuantity = 1}
                }
            };
            var baseImportProducts = new List<WmsBaseImportProduct>() { new WmsBaseImportProduct()
                {
                    ImportId = baseImport.ImportId,
                    ProductId = Guid.Parse("b08d504e-8e0b-47b9-a51c-50adf96a2338"),
                    CostPrice = 55000,
                    UnitPrice = 50000,
                    ImportedQuantity = 4,
                    StockQuantity = 4,
                }
            };
            var baseDebt = new WmsBaseDebt()
            {
                DebtAmount = 500000,
            };

            _supplierRepoMock.Setup(x => x.GetById(It.IsAny<int>())).ReturnsAsync(SupplierData.Data.First());
            _baseImportProductRepoMock.Setup(x => x.FindObject(
                MoqHelper.IsExpression<WmsBaseImportProduct>(x => x.ImportId.Equals(baseImport.ImportId)
                && x.ProductId.Equals(baseImportProducts.First().ProductId)
                )))
                .ReturnsAsync(baseImportProducts.First);
            _baseImportRepoMock.Setup(x => x.FindObject(
                MoqHelper.IsExpression<WmsBaseImport>(x => x.ImportId.Equals(baseImport.ImportId)))).ReturnsAsync(baseImport);
            _baseDebtRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBaseDebt, bool>>>())).ReturnsAsync(baseDebt);

            //Act
            var result = await _productService.CreateBaseReturn(entity, UserData.Data.First().Fullname);

            //Assert
            Assert.False(result);
            Assert.Equal(500000, baseDebt.DebtAmount);
            _baseDebtRepoMock.Verify(x => x.Update(It.IsAny<WmsBaseDebt>()), Times.Never);
            _baseReturnRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBaseProductReturn>()), Times.Never);
            _baseInvoiceRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBaseInvoice>()), Times.Never);
            _baseReturnDetailRepoMock.Verify(x => x.AddRangeAsync(It.IsAny<IEnumerable<WmsBaseProductReturnDetail>>()), Times.Never);
            _baseImportProductRepoMock.Verify(x => x.Update(It.IsAny<WmsBaseImportProduct>()), Times.Never);
            Assert.Equal(0, baseImport.PaidAmount);
        }

        [Fact]
        public async Task CreateBaseReturn_ListProductEmpty()
        {
            //Arrange
            var baseImport = BaseImportData.Data.First();
            baseImport.ImportStatus = ImportStatusConstant.Imported;
            var entity = new CreateProducReturnSupplierRequestDto()
            {
                ImportId = baseImport.ImportId,
                ProductRequest = new List<CreateProductReturnSupplierRequestDetailDto>()
                {

                }
            };
            var baseImportProducts = new List<WmsBaseImportProduct>() { new WmsBaseImportProduct()
                {
                    ImportId = baseImport.ImportId,
                    ProductId = Guid.Parse("b08d504e-8e0b-47b9-a51c-50adf96a2338"),
                    CostPrice = 55000,
                    UnitPrice = 50000,
                    ImportedQuantity = 4,
                    StockQuantity = 4,
                }
            };
            var baseDebt = new WmsBaseDebt()
            {
                DebtAmount = 500000,
            };

            _supplierRepoMock.Setup(x => x.GetById(It.IsAny<int>())).ReturnsAsync(SupplierData.Data.First());
            _baseImportProductRepoMock.Setup(x => x.FindObject(
                MoqHelper.IsExpression<WmsBaseImportProduct>(x => x.ImportId.Equals(baseImport.ImportId)
                && x.ProductId.Equals(baseImportProducts.First().ProductId)
                )))
                .ReturnsAsync(baseImportProducts.First);
            _baseImportRepoMock.Setup(x => x.FindObject(
                MoqHelper.IsExpression<WmsBaseImport>(x => x.ImportId.Equals(baseImport.ImportId)))).ReturnsAsync(baseImport);
            _baseDebtRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBaseDebt, bool>>>())).ReturnsAsync(baseDebt);

            //Act
            var result = await _productService.CreateBaseReturn(entity, UserData.Data.First().Fullname);

            //Assert
            Assert.False(result);
            Assert.Equal(500000, baseDebt.DebtAmount);
            _baseDebtRepoMock.Verify(x => x.Update(It.IsAny<WmsBaseDebt>()), Times.Never);
            _baseReturnRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBaseProductReturn>()), Times.Never);
            _baseInvoiceRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBaseInvoice>()), Times.Never);
            _baseReturnDetailRepoMock.Verify(x => x.AddRangeAsync(It.IsAny<IEnumerable<WmsBaseProductReturnDetail>>()), Times.Never);
            _baseImportProductRepoMock.Verify(x => x.Update(It.IsAny<WmsBaseImportProduct>()), Times.Never);
            Assert.Equal(0, baseImport.PaidAmount);
        }

        [Fact]
        public async Task GetProductList_SearchWordExist_Returns_ViewPaging()
        {
            // Arrange
            var requestDto = new GetProductListRequestDto
            {
                SearchWord = "product1",
                PagingRequest = new PagingRequest
                {
                    CurrentPage = 1,
                    PageRange = 5,
                    PageSize = 10
                },
                Visible = true,
                CategoryId = 1
            };

            var productList = new List<WmsProduct>
            {
                new WmsProduct(Guid.NewGuid(), 1, "sku1", "product1", "test", "/abc/a", true, 1000, 1000, 1000, "kg", 1),
                new WmsProduct(Guid.NewGuid(), 2, "sku2", "product2", "test", "/abc/a", true, 1000, 1000, 1000, "kg", 2),
                new WmsProduct(Guid.NewGuid(), 3, "sku3", "product3", "test", "/abc/a", true, 1000, 1000, 1000, "kg", 2),
            };

            _productRepoMock.Setup(p => p.GetProductListPaging(
                requestDto.CategoryId,
                requestDto.SearchWord,
                requestDto.Visible,
                It.IsAny<int>(),
                It.IsAny<int>()))
                .ReturnsAsync(productList.Where(x => x.ProductName.Contains(requestDto.SearchWord)).ToList());

            var s3FileUrl = "https://example.com/image.jpg";
            _s3StorageServiceMock.Setup(s => s.GetFileUrl(It.IsAny<S3RequestData>())).Returns(s3FileUrl);

            // Act
            var result = await _productService.GetProductList(requestDto);

            // Assert
            Assert.NotNull(result);
            Assert.Single(result.Items);
            Assert.Equal(1, result.Pagination.TotalPage);
            Assert.IsType<ViewPaging<GetProductListResponseDto>>(result);
        }

        [Fact]
        public async Task GetProductList_SearchWordNonExist_Returns_ViewPaging()
        {
            // Arrange
            var requestDto = new GetProductListRequestDto
            {
                SearchWord = "aaaaa",
                PagingRequest = new PagingRequest
                {
                    CurrentPage = 1,
                    PageRange = 5,
                    PageSize = 10
                },
                Visible = true,
                CategoryId = 1
            };

            var productList = new List<WmsProduct>
            {
                new WmsProduct(Guid.NewGuid(), 1, "sku1", "product1", "test", "/abc/a", true, 1000, 1000, 1000, "kg", 1),
                new WmsProduct(Guid.NewGuid(), 2, "sku2", "product2", "test", "/abc/a", true, 1000, 1000, 1000, "kg", 2),
                new WmsProduct(Guid.NewGuid(), 3, "sku3", "product3", "test", "/abc/a", true, 1000, 1000, 1000, "kg", 2),
            };

            _productRepoMock.Setup(p => p.GetProductListPaging(
                requestDto.CategoryId,
                requestDto.SearchWord,
                requestDto.Visible,
                It.IsAny<int>(),
                It.IsAny<int>()))
                .ReturnsAsync(productList.Where(x => x.ProductName.Contains(requestDto.SearchWord)).ToList());

            var s3FileUrl = "https://example.com/image.jpg";
            _s3StorageServiceMock.Setup(s => s.GetFileUrl(It.IsAny<S3RequestData>())).Returns(s3FileUrl);

            // Act
            var result = await _productService.GetProductList(requestDto);

            // Assert
            Assert.NotNull(result);
            Assert.Empty(result.Items);
            Assert.Equal(0, result.Pagination.TotalPage);
            Assert.IsType<ViewPaging<GetProductListResponseDto>>(result);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("10c48c7c-48b0-4ea1-9e2c-95f6a219dc6a")]
        public async Task GetProductDetail_Returns_Null_When_Product_NotFound(Guid productId)
        {
            // Arrange
            _unitOfWork.Setup(uow => uow.ProductRepository.GetById(productId))
                          .ReturnsAsync((WmsProduct)null);

            // Act
            var result = await _productService.GetProductDetail(productId);

            // Assert
            Assert.Null(result);
        }

        [Fact]
        public async Task GetProductDetail_Returns_Product_Detail_When_Found()
        {
            // Arrange
            var productId = Guid.NewGuid();
            var mockProductInfo = new WmsProduct(productId, 1, "sku1", "product1", "test", "/abc/a", true, 1000, 1000, 1000, "kg", 1);

            _unitOfWork.Setup(uow => uow.ProductRepository.GetById(productId))
                          .ReturnsAsync(mockProductInfo);

            _s3StorageServiceMock.Setup(s3 => s3.GetFileUrl(It.IsAny<S3RequestData>()))
                                .Returns("https://example.com/product-image.jpg");

            // Act
            var result = await _productService.GetProductDetail(productId);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(productId, result.ProductId);
            Assert.Equal("https://example.com/product-image.jpg", result.ProductImage);
        }

        [Fact]
        public async Task UpdateProductDetail_ProductExists_ReturnsTrue()
        {
            // Arrange
            var productId = Guid.NewGuid();
            var productToUpdate = new WmsProduct(productId, 1, "sku1", "product1", "test", "/abc/a", true, 1000, 1000, 1000, "kg", 1);

            // Simulate finding the product by ID
            _productRepoMock.Setup(repo => repo.FindObject(It.IsAny<Expression<Func<WmsProduct, bool>>>()))
                .ReturnsAsync(productToUpdate);

            var updateRequestDto = new UpdateProductDetailRequestDto
            {
                ProductId = productId,
                CategoryId = 1,
                SKU = "SKU_Test",
                ProductName = "Test_Product",
                ProductDescription = "Test",
                Visible = true,
                UnitPrice = 1000,
                SalePrice = 1000,
                ExportPrice = 1000,
                UnitType = "kg",
                Status = 1
            };

            // Act
            var result = await _productService.UpdateProductDetail(updateRequestDto);

            // Assert
            Assert.True(result);
            _productRepoMock.Verify(repo => repo.Update(It.IsAny<WmsProduct>()), Times.Once);
            _unitOfWork.Verify(uow => uow.CommitAsync(), Times.Once);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("10c48c7c-48b0-4ea1-9e2c-95f6a219dc6a")]
        public async Task UpdateProductDetail_ProductNotExist_ReturnsFalse(Guid productId)
        {
            // Arrange
            _productRepoMock.Setup(repo => repo.FindObject(It.IsAny<Expression<Func<WmsProduct, bool>>>()))
                .ReturnsAsync(default(WmsProduct));

            var updateRequestDto = new UpdateProductDetailRequestDto
            {
                ProductId = productId,
                CategoryId = 1,
                SKU = "SKU_Test",
                ProductName = "Test_Product",
                ProductDescription = "Test",
                Visible = true,
                UnitPrice = 1000,
                SalePrice = 1000,
                ExportPrice = 1000,
                UnitType = "kg",
                Status = 1
            };

            // Act
            var result = await _productService.UpdateProductDetail(updateRequestDto);

            // Assert
            Assert.False(result);
            _productRepoMock.Verify(repo => repo.Update(It.IsAny<WmsProduct>()), Times.Never);
            _unitOfWork.Verify(uow => uow.CommitAsync(), Times.Never);
        }


        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        public async Task UpdateBranchReturn_NormalCase(short status)
        {
            //Arrange
            _branchProductReturnRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchProductReturn, bool>>>())).ReturnsAsync(new WmsBranchProductReturn() { Status = 0 });
            _branchImportRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchImport, bool>>>())).ReturnsAsync(new WmsBranchImport());
            _branchImportProductRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchImportProduct, bool>>>())).ReturnsAsync(new WmsBranchImportProduct());
            _branchWarehouseRepoMock.Setup(x => x.GetById(It.IsAny<Guid>())).ReturnsAsync(new WmsBranchWarehouse());
            _branchProductReturnDetailRepoMock.Setup(x => x.FindMultiple(It.IsAny<Expression<Func<WmsBranchProductReturnDetail, bool>>>())).ReturnsAsync(
                                new List<WmsBranchProductReturnDetail>()
                                {
                                    new WmsBranchProductReturnDetail(),
                                }
                            );
            if (status == 1)
            {
                _branchDebtRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchDebt, bool>>>())).ReturnsAsync(new WmsBranchDebt());
                _baseImportProductRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBaseImportProduct, bool>>>())).ReturnsAsync(new WmsBaseImportProduct());
                _baseExportRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBaseExport, bool>>>())).ReturnsAsync(new WmsBaseExport());
            }


            //Act
            var actual = await _productService.UpdateBranchReturn(new UpdateStatusProductReturnFromBranchRequestDto()
            {
                ReturnId = Guid.NewGuid(),
                Status = status
            }).ConfigureAwait(false);

            //Assert
            Assert.True(actual);
            _branchProductReturnRepoMock.Verify(x => x.Update(It.IsAny<WmsBranchProductReturn>()), Times.Once);
            if (status == 2)
            {
                _branchImportProductRepoMock.Verify(x => x.Update(It.IsAny<WmsBranchImportProduct>()), Times.Once);
            }
            if (status == 1)
            {
                _branchDebtRepoMock.Verify(x => x.Update(It.IsAny<WmsBranchDebt>()), Times.Once);
                _baseImportProductRepoMock.Verify(x => x.Update(It.IsAny<WmsBaseImportProduct>()), Times.Once);
                _baseExportRepoMock.Verify(x => x.Update(It.IsAny<WmsBaseExport>()), Times.Once);
                _branchImportRepoMock.Verify(x => x.Update(It.IsAny<WmsBranchImport>()), Times.Once);
                _branchInvoiceRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBranchInvoice>()), Times.AtLeastOnce());
                _baseInvoiceRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBaseInvoice>()), Times.AtLeastOnce());
            }
        }



        [Fact]
        public async Task UpdateBranchReturn_IdNotExist()
        {
            //Arrange
            //_branchProductReturnRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchProductReturn, bool>>>())).ReturnsAsync(new WmsBranchProductReturn() { Status = 0 });
            _branchImportRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchImport, bool>>>())).ReturnsAsync(new WmsBranchImport());
            _branchImportProductRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchImportProduct, bool>>>())).ReturnsAsync(new WmsBranchImportProduct());
            _branchWarehouseRepoMock.Setup(x => x.GetById(It.IsAny<Guid>())).ReturnsAsync(new WmsBranchWarehouse());
            _branchProductReturnDetailRepoMock.Setup(x => x.FindMultiple(It.IsAny<Expression<Func<WmsBranchProductReturnDetail, bool>>>())).ReturnsAsync(new List<WmsBranchProductReturnDetail>() { new WmsBranchProductReturnDetail(), });
            _branchDebtRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchDebt, bool>>>())).ReturnsAsync(new WmsBranchDebt());
            _baseImportProductRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBaseImportProduct, bool>>>())).ReturnsAsync(new WmsBaseImportProduct());


            //Act
            var actual = await _productService.UpdateBranchReturn(new UpdateStatusProductReturnFromBranchRequestDto()
            {
                ReturnId = Guid.Empty,
                Status = 1
            }).ConfigureAwait(false);

            //Assert
            Assert.False(actual);
            _branchProductReturnRepoMock.Verify(x => x.Update(It.IsAny<WmsBranchProductReturn>()), Times.Never);
        }

        [Fact]
        public async Task UpdateBranchReturn_InvalidStatus()
        {
            //Arrange
            _branchProductReturnRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchProductReturn, bool>>>())).ReturnsAsync(new WmsBranchProductReturn() { Status = 0 });
            _branchImportRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchImport, bool>>>())).ReturnsAsync(new WmsBranchImport());
            _branchImportProductRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchImportProduct, bool>>>())).ReturnsAsync(new WmsBranchImportProduct());
            _branchWarehouseRepoMock.Setup(x => x.GetById(It.IsAny<Guid>())).ReturnsAsync(new WmsBranchWarehouse());
            _branchProductReturnDetailRepoMock.Setup(x => x.FindMultiple(It.IsAny<Expression<Func<WmsBranchProductReturnDetail, bool>>>())).ReturnsAsync(new List<WmsBranchProductReturnDetail>() { new WmsBranchProductReturnDetail(), });
            _branchDebtRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchDebt, bool>>>())).ReturnsAsync(new WmsBranchDebt());
            _baseImportProductRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBaseImportProduct, bool>>>())).ReturnsAsync(new WmsBaseImportProduct());


            //Act
            var actual = await _productService.UpdateBranchReturn(new UpdateStatusProductReturnFromBranchRequestDto()
            {
                ReturnId = Guid.NewGuid(),
                Status = 999
            }).ConfigureAwait(false);

            //Assert
            Assert.False(actual);
            _branchProductReturnRepoMock.Verify(x => x.Update(It.IsAny<WmsBranchProductReturn>()), Times.Never);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        public async Task UpdateBranchReturn_RefusedOrReturned(short status)
        {
            //Arrange
            _branchProductReturnRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchProductReturn, bool>>>())).ReturnsAsync(new WmsBranchProductReturn() { Status = status });
            _branchImportRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchImport, bool>>>())).ReturnsAsync(new WmsBranchImport());
            _branchImportProductRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchImportProduct, bool>>>())).ReturnsAsync(new WmsBranchImportProduct());
            _branchWarehouseRepoMock.Setup(x => x.GetById(It.IsAny<Guid>())).ReturnsAsync(new WmsBranchWarehouse());
            _branchProductReturnDetailRepoMock.Setup(x => x.FindMultiple(It.IsAny<Expression<Func<WmsBranchProductReturnDetail, bool>>>())).ReturnsAsync(new List<WmsBranchProductReturnDetail>() { new WmsBranchProductReturnDetail(), });
            _branchDebtRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchDebt, bool>>>())).ReturnsAsync(new WmsBranchDebt());
            _baseImportProductRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBaseImportProduct, bool>>>())).ReturnsAsync(new WmsBaseImportProduct());


            //Act
            var actual = await _productService.UpdateBranchReturn(new UpdateStatusProductReturnFromBranchRequestDto()
            {
                ReturnId = Guid.NewGuid(),
                Status = 1
            }).ConfigureAwait(false);

            //Assert
            Assert.False(actual);
            _branchProductReturnRepoMock.Verify(x => x.Update(It.IsAny<WmsBranchProductReturn>()), Times.Never);
        }

        [Fact]
        public async Task GetProductReturnFromBranch_SearchWordExist_Success()
        {
            // Arrange
            var requestDto = new GetProductReturnFromBranchRequestDto
            {
                SearchWord = "RETURN_1",
                DateFilter = 0,
                PagingRequest = new PagingRequest
                {
                    CurrentPage = 1,
                    PageRange = 5,
                    PageSize = 10
                }
            };

            var expectedPagination = new Pagination(1, 1, 5, 10);

            // Mocking date service
            _dateService.Setup(ds => ds.GetDateTimeByDateFilter(It.IsAny<short>()))
                .Returns(new DateFilterRequestDto(DateTime.Now.AddDays(-7), DateTime.Now));

            // Mocking Unit of Work methods
            var branchProductReturn = new List<WmsBranchProductReturn> {
                new WmsBranchProductReturn(Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), "test1", 1, 1000, 1000, 1, "RETURN_1", "test", DateTime.Now, "test", DateTime.Now),
                new WmsBranchProductReturn(Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), "test2", 1, 1000, 1000, 1, "RETURN_2", "test", DateTime.Now, "test", DateTime.Now),
            };

            var expectedProductReturn = branchProductReturn.Where(x => x.ReturnCode.Contains(requestDto.SearchWord)).ToList();
            _unitOfWork.Setup(uow => uow.BranchProductReturnRepository.GetProductsReturnFromBranch(
                It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string?>(), It.IsAny<Guid?>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<short?>(), It.IsAny<short?>()))
                .ReturnsAsync(expectedProductReturn);

            _unitOfWork.Setup(uow => uow.BranchProductReturnRepository.CountProductReturnFromBranch(
                It.IsAny<string?>(), It.IsAny<Guid?>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<short?>(), It.IsAny<short?>()))
                .ReturnsAsync(expectedProductReturn.Count);

            // Act
            var result = await _productService.GetProductReturnFromBranch(requestDto);

            // Assert
            Assert.NotNull(result);
            Assert.NotEmpty(result.Items);
            Assert.Equal(expectedPagination.TotalItem, result.Pagination.TotalItem);
            Assert.Equal(expectedPagination.CurrentPage, result.Pagination.CurrentPage);
            Assert.IsType<ViewPaging<GetProductsReturnFromBranchResponseDto>>(result);
        }

        [Fact]
        public async Task GetProductReturnFromBranch_SearchWordNonExist_Failed()
        {
            // Arrange
            var requestDto = new GetProductReturnFromBranchRequestDto
            {
                SearchWord = "RETURN_5",
                DateFilter = 0,
                PagingRequest = new PagingRequest
                {
                    CurrentPage = 1,
                    PageRange = 5,
                    PageSize = 10
                }
            };

            var expectedPagination = new Pagination(0, 1, 5, 10);

            // Mocking date service
            _dateService.Setup(ds => ds.GetDateTimeByDateFilter(It.IsAny<short>()))
                .Returns(new DateFilterRequestDto(DateTime.Now.AddDays(-7), DateTime.Now));

            // Mocking Unit of Work methods
            var branchProductReturn = new List<WmsBranchProductReturn> {
                new WmsBranchProductReturn(Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), "test1", 1, 1000, 1000, 1, "RETURN_1", "test", DateTime.Now, "test", DateTime.Now),
                new WmsBranchProductReturn(Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), "test2", 1, 1000, 1000, 1, "RETURN_2", "test", DateTime.Now, "test", DateTime.Now),
            };

            var expectedProductReturn = branchProductReturn.Where(x => x.ReturnCode.Contains(requestDto.SearchWord)).ToList();
            _unitOfWork.Setup(uow => uow.BranchProductReturnRepository.GetProductsReturnFromBranch(
                It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string?>(), It.IsAny<Guid?>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<short?>(), It.IsAny<short?>()))
                .ReturnsAsync(expectedProductReturn);

            _unitOfWork.Setup(uow => uow.BranchProductReturnRepository.CountProductReturnFromBranch(
                It.IsAny<string?>(), It.IsAny<Guid?>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<short?>(), It.IsAny<short?>()))
                .ReturnsAsync(expectedProductReturn.Count);

            // Act
            var result = await _productService.GetProductReturnFromBranch(requestDto);

            // Assert
            Assert.NotNull(result);
            Assert.Empty(result.Items);
            Assert.Equal(expectedPagination.TotalItem, result.Pagination.TotalItem);
            Assert.Equal(expectedPagination.CurrentPage, result.Pagination.CurrentPage);
            Assert.IsType<ViewPaging<GetProductsReturnFromBranchResponseDto>>(result);
        }

        [Fact]
        public async Task GetProductReturnFromBranch_StatusExist_Success()
        {
            // Arrange
            var requestDto = new GetProductReturnFromBranchRequestDto
            {
                SearchWord = "RETURN_1",
                DateFilter = 0,
                PagingRequest = new PagingRequest
                {
                    CurrentPage = 1,
                    PageRange = 5,
                    PageSize = 10
                },
                Status = 1
            };

            var expectedPagination = new Pagination(2, 1, 5, 10);

            // Mocking date service
            _dateService.Setup(ds => ds.GetDateTimeByDateFilter(It.IsAny<short>()))
                .Returns(new DateFilterRequestDto(DateTime.Now.AddDays(-7), DateTime.Now));

            // Mocking Unit of Work methods
            var branchProductReturn = new List<WmsBranchProductReturn> {
                new WmsBranchProductReturn(Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), "test1", 1, 1000, 1000, 1, "RETURN_1", "test", DateTime.Now, "test", DateTime.Now),
                new WmsBranchProductReturn(Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), "test2", 1, 1000, 1000, 1, "RETURN_2", "test", DateTime.Now, "test", DateTime.Now),
            };

            var expectedProductReturn = branchProductReturn.Where(x => x.Status == requestDto.Status).ToList();
            _unitOfWork.Setup(uow => uow.BranchProductReturnRepository.GetProductsReturnFromBranch(
                It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string?>(), It.IsAny<Guid?>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<short?>(), It.IsAny<short?>()))
                .ReturnsAsync(expectedProductReturn);

            _unitOfWork.Setup(uow => uow.BranchProductReturnRepository.CountProductReturnFromBranch(
                It.IsAny<string?>(), It.IsAny<Guid?>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<short?>(), It.IsAny<short?>()))
                .ReturnsAsync(expectedProductReturn.Count);

            // Act
            var result = await _productService.GetProductReturnFromBranch(requestDto);

            // Assert
            Assert.NotNull(result);
            Assert.NotEmpty(result.Items);
            Assert.Equal(expectedPagination.TotalItem, result.Pagination.TotalItem);
            Assert.Equal(expectedPagination.CurrentPage, result.Pagination.CurrentPage);
            Assert.IsType<ViewPaging<GetProductsReturnFromBranchResponseDto>>(result);
        }

        [Fact]
        public async Task GetProductReturnFromBranch_StatusNonExist_Failed()
        {
            // Arrange
            var requestDto = new GetProductReturnFromBranchRequestDto
            {
                SearchWord = "RETURN_5",
                DateFilter = 0,
                PagingRequest = new PagingRequest
                {
                    CurrentPage = 1,
                    PageRange = 5,
                    PageSize = 10
                },
                Status = 2
            };

            var expectedPagination = new Pagination(0, 1, 5, 10);

            // Mocking date service
            _dateService.Setup(ds => ds.GetDateTimeByDateFilter(It.IsAny<short>()))
                .Returns(new DateFilterRequestDto(DateTime.Now.AddDays(-7), DateTime.Now));

            // Mocking Unit of Work methods
            var branchProductReturn = new List<WmsBranchProductReturn> {
                new WmsBranchProductReturn(Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), "test1", 1, 1000, 1000, 1, "RETURN_1", "test", DateTime.Now, "test", DateTime.Now),
                new WmsBranchProductReturn(Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), "test2", 1, 1000, 1000, 1, "RETURN_2", "test", DateTime.Now, "test", DateTime.Now),
            };

            var expectedProductReturn = branchProductReturn.Where(x => x.Status == requestDto.Status).ToList();
            _unitOfWork.Setup(uow => uow.BranchProductReturnRepository.GetProductsReturnFromBranch(
                It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string?>(), It.IsAny<Guid?>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<short?>(), It.IsAny<short?>()))
                .ReturnsAsync(expectedProductReturn);

            _unitOfWork.Setup(uow => uow.BranchProductReturnRepository.CountProductReturnFromBranch(
                It.IsAny<string?>(), It.IsAny<Guid?>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<short?>(), It.IsAny<short?>()))
                .ReturnsAsync(expectedProductReturn.Count);

            // Act
            var result = await _productService.GetProductReturnFromBranch(requestDto);

            // Assert
            Assert.NotNull(result);
            Assert.Empty(result.Items);
            Assert.Equal(expectedPagination.TotalItem, result.Pagination.TotalItem);
            Assert.Equal(expectedPagination.CurrentPage, result.Pagination.CurrentPage);
            Assert.IsType<ViewPaging<GetProductsReturnFromBranchResponseDto>>(result);
        }

        [Fact]
        public async Task GetProductReturnFromBranch_PaymentStatusExist_Success()
        {
            // Arrange
            var requestDto = new GetProductReturnFromBranchRequestDto
            {
                SearchWord = "RETURN_1",
                DateFilter = 0,
                PagingRequest = new PagingRequest
                {
                    CurrentPage = 1,
                    PageRange = 5,
                    PageSize = 10
                },
                PaymentStatus = 1
            };

            var expectedPagination = new Pagination(2, 1, 5, 10);

            // Mocking date service
            _dateService.Setup(ds => ds.GetDateTimeByDateFilter(It.IsAny<short>()))
                .Returns(new DateFilterRequestDto(DateTime.Now.AddDays(-7), DateTime.Now));

            // Mocking Unit of Work methods
            var branchProductReturn = new List<WmsBranchProductReturn> {
                new WmsBranchProductReturn(Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), "test1", 1, 1000, 1000, 1, "RETURN_1", "test", DateTime.Now, "test", DateTime.Now),
                new WmsBranchProductReturn(Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), "test2", 1, 1000, 1000, 1, "RETURN_2", "test", DateTime.Now, "test", DateTime.Now),
            };

            var expectedProductReturn = branchProductReturn.Where(x => x.PaymentStatus == requestDto.PaymentStatus).ToList();
            _unitOfWork.Setup(uow => uow.BranchProductReturnRepository.GetProductsReturnFromBranch(
                It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string?>(), It.IsAny<Guid?>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<short?>(), It.IsAny<short?>()))
                .ReturnsAsync(expectedProductReturn);

            _unitOfWork.Setup(uow => uow.BranchProductReturnRepository.CountProductReturnFromBranch(
                It.IsAny<string?>(), It.IsAny<Guid?>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<short?>(), It.IsAny<short?>()))
                .ReturnsAsync(expectedProductReturn.Count);

            // Act
            var result = await _productService.GetProductReturnFromBranch(requestDto);

            // Assert
            Assert.NotNull(result);
            Assert.NotEmpty(result.Items);
            Assert.Equal(expectedPagination.TotalItem, result.Pagination.TotalItem);
            Assert.Equal(expectedPagination.CurrentPage, result.Pagination.CurrentPage);
            Assert.IsType<ViewPaging<GetProductsReturnFromBranchResponseDto>>(result);
        }

        [Fact]
        public async Task GetProductReturnFromBranch_PaymentStatusNonExist_Failed()
        {
            // Arrange
            var requestDto = new GetProductReturnFromBranchRequestDto
            {
                SearchWord = "RETURN_1",
                DateFilter = 0,
                PagingRequest = new PagingRequest
                {
                    CurrentPage = 1,
                    PageRange = 5,
                    PageSize = 10
                },
                PaymentStatus = 2
            };

            var expectedPagination = new Pagination(0, 1, 5, 10);

            // Mocking date service
            _dateService.Setup(ds => ds.GetDateTimeByDateFilter(It.IsAny<short>()))
                .Returns(new DateFilterRequestDto(DateTime.Now.AddDays(-7), DateTime.Now));

            // Mocking Unit of Work methods
            var branchProductReturn = new List<WmsBranchProductReturn> {
                new WmsBranchProductReturn(Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), "test1", 1, 1000, 1000, 1, "RETURN_1", "test", DateTime.Now, "test", DateTime.Now),
                new WmsBranchProductReturn(Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), "test2", 1, 1000, 1000, 1, "RETURN_2", "test", DateTime.Now, "test", DateTime.Now),
            };

            var expectedProductReturn = branchProductReturn.Where(x => x.PaymentStatus == requestDto.PaymentStatus).ToList();
            _unitOfWork.Setup(uow => uow.BranchProductReturnRepository.GetProductsReturnFromBranch(
                It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string?>(), It.IsAny<Guid?>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<short?>(), It.IsAny<short?>()))
                .ReturnsAsync(expectedProductReturn);

            _unitOfWork.Setup(uow => uow.BranchProductReturnRepository.CountProductReturnFromBranch(
                It.IsAny<string?>(), It.IsAny<Guid?>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<short?>(), It.IsAny<short?>()))
                .ReturnsAsync(expectedProductReturn.Count);

            // Act
            var result = await _productService.GetProductReturnFromBranch(requestDto);

            // Assert
            Assert.NotNull(result);
            Assert.Empty(result.Items);
            Assert.Equal(expectedPagination.TotalItem, result.Pagination.TotalItem);
            Assert.Equal(expectedPagination.CurrentPage, result.Pagination.CurrentPage);
            Assert.IsType<ViewPaging<GetProductsReturnFromBranchResponseDto>>(result);
        }

        //[Fact]
        //public async Task GetAllStockQuantity_WhenCalled_ReturnsViewPaging()
        //{
        //    // Arrange
        //    var entity = new GetStockQuantityRequestDto
        //    {
        //        SearchWord = "",
        //        CategoryId = 1,
        //        PagingRequest = new PagingRequest
        //        {
        //            CurrentPage = 1,
        //            PageRange = 5,
        //            PageSize = 10
        //        },
        //        RoleId = 1
        //    };

        //    var currentUser = "testUser";
        //    dynamic anonymousObject = new ExpandoObject();
        //    anonymousObject.ProductId = Guid.NewGuid();
        //    anonymousObject.ImportId = Guid.NewGuid();
        //    anonymousObject.SKU = "SKU_TEST";
        //    anonymousObject.CategoryName = "test";
        //    anonymousObject.ProductName = "test";
        //    anonymousObject.ProductImage = "";
        //    anonymousObject.UnitPrice = 1;
        //    anonymousObject.InventoryNumber = 1;
        //    anonymousObject.CategoryId = 1;

        //    _unitOfWork.Setup(u => u.ProductRepository.SearchStockQuantityForBase(It.IsAny<string>(), It.IsAny<int>()))
        //        .Returns(anonymousObject);

        //    _unitOfWork.Setup(u => u.UserRepository.GetUserWithRoleByUsername(currentUser, 1))
        //        .Returns(new List<WmsUser>()
        //        {
        //           new WmsUser
        //           {
        //                UserId = Guid.NewGuid(),
        //                Username = "user",
        //                Email = "user@gmail.com"
        //           }
        //        }.AsQueryable().BuildMock());


        //    // Act
        //    var result = await _productService.GetAllStockQuantity(entity, currentUser);

        //    // Assert
        //    Assert.NotNull(result);
        //    // Add more assertions based on your expected behavior
        //}
    }
}
