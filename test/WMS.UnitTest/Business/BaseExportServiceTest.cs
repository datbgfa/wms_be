﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using MockQueryable.Moq;
using Moq;
using MoqExpression;
using System.Data;
using System.Linq.Expressions;
using WMS.Business.Abstractions.Service;
using WMS.Business.Dtos.BaseExportDto;
using WMS.Business.Dtos.CommonDto;
using WMS.Business.Dtos.FilterDto;
using WMS.Business.Dtos.InvoiceDto;
using WMS.Business.Dtos.Mapper;
using WMS.Business.Dtos.ProductDto;
using WMS.Business.Dtos.S3Dto;
using WMS.Business.Dtos.WarehouseDto;
using WMS.Business.Services;
using WMS.Domain.Abstractions;
using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Entities;
using WMS.Domain.Enums;
using WMS.UnitTest.DummiesData;
namespace WMS.UnitTest.Business
{
    public class BaseExportServiceTest
    {
        private readonly BaseExportService _baseExportService;
        private readonly Mock<IUnitOfWork> _unitOfWork = new Mock<IUnitOfWork>();
        private readonly Mock<IWmsUserRepository> _userRepoMock = new Mock<IWmsUserRepository>();
        private readonly Mock<IWmsBaseWarehouseRepository> _baseWarehouseRepoMock = new Mock<IWmsBaseWarehouseRepository>();
        private readonly Mock<IWmsBranchWarehouseRepository> _branchWarehouseRepoMock = new Mock<IWmsBranchWarehouseRepository>();
        private readonly Mock<IWmsBaseExportRepository> _baseExportRepoMock = new Mock<IWmsBaseExportRepository>();
        private readonly Mock<IWmsBaseExportProductRepository> _baseExportProductRepoMock = new Mock<IWmsBaseExportProductRepository>();
        private readonly Mock<IWmsSupplierRepository> _supplierRepoMock = new Mock<IWmsSupplierRepository>();
        private readonly Mock<IWmsProductRepository> _productRepoMock = new Mock<IWmsProductRepository>();
        private readonly Mock<IWmsBranchDebtRepository> _branchDebtRepoMock = new Mock<IWmsBranchDebtRepository>();
        private readonly Mock<IWmsBaseInvoiceRepository> _baseInvoiceRepoMock = new Mock<IWmsBaseInvoiceRepository>();
        private readonly Mock<IWmsBranchInvoiceRepository> _branchInvoiceRepoMock = new Mock<IWmsBranchInvoiceRepository>();
        private readonly Mock<IWmsBaseProductReturnRepository> _baseReturnRepoMock = new Mock<IWmsBaseProductReturnRepository>();
        private readonly Mock<IWmsBranchImportRepository> _branchImportRepoMock = new Mock<IWmsBranchImportRepository>();
        private readonly Mock<IWmsBranchImportProductRepository> _branchImportProductRepoMock = new Mock<IWmsBranchImportProductRepository>();
        private readonly Mock<IWmsBranchProductReturnDetailRepository> _branchProductReturnDetailRepoMock = new Mock<IWmsBranchProductReturnDetailRepository>();
        private readonly Mock<IWmsBranchProductReturnRepository> _branchProductReturnRepoMock = new Mock<IWmsBranchProductReturnRepository>();
        private readonly Mock<IWmsBaseImportProductRepository> _baseImportProductRepoMock = new Mock<IWmsBaseImportProductRepository>();
        private readonly Mock<IWmsBranchRequestRepository> _branchRequestRepoMock = new Mock<IWmsBranchRequestRepository>();


        private readonly IMock<ILogger<BaseExportService>> _logger = new Mock<ILogger<BaseExportService>>();
        private readonly Mock<IS3StorageService> _storageService = new Mock<IS3StorageService>();
        private readonly Mock<IDateService> _dateService = new Mock<IDateService>();
        private readonly Mock<IProductService> _productService = new Mock<IProductService>();
        private readonly Mock<IInvoiceService> _invoiceServiceMock = new Mock<IInvoiceService>();


        public BaseExportServiceTest()
        {
            var mockTran = new Mock<IDbTransaction>();

            _unitOfWork.Setup(x => x.BeginTransaction()).Returns(mockTran.Object);
            _unitOfWork.Setup(x => x.UserRepository).Returns(_userRepoMock.Object);
            _unitOfWork.Setup(x => x.BaseWarehouseRepository).Returns(_baseWarehouseRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchWarehouseRepository).Returns(_branchWarehouseRepoMock.Object);
            _unitOfWork.Setup(x => x.BaseExportRepository).Returns(_baseExportRepoMock.Object);
            _unitOfWork.Setup(x => x.BaseExportProductRepository).Returns(_baseExportProductRepoMock.Object);
            _unitOfWork.Setup(x => x.SupplierRepository).Returns(_supplierRepoMock.Object);
            _unitOfWork.Setup(x => x.ProductRepository).Returns(_productRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchDebtRepository).Returns(_branchDebtRepoMock.Object);
            _unitOfWork.Setup(x => x.BaseInvoiceRepository).Returns(_baseInvoiceRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchInvoiceRepository).Returns(_branchInvoiceRepoMock.Object);
            _unitOfWork.Setup(x => x.BaseProductReturnRepository).Returns(_baseReturnRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchImportRepository).Returns(_branchImportRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchProductReturnDetailRepository).Returns(_branchProductReturnDetailRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchProductReturnRepository).Returns(_branchProductReturnRepoMock.Object);
            _unitOfWork.Setup(x => x.BaseImportProductRepository).Returns(_baseImportProductRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchRequestRepository).Returns(_branchRequestRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchImportProductRepository).Returns(_branchImportProductRepoMock.Object);

            var configuration = new MapperConfiguration(config =>
            {
                config.AddProfile<DomainToDtoProfile>();
                config.AddProfile<DtoToDomainProfile>();
            });
            var mapper = new Mapper(configuration);

            _baseExportService = new BaseExportService(_unitOfWork.Object, _logger.Object, mapper,
               _dateService.Object, _storageService.Object, _productService.Object, _invoiceServiceMock.Object);
        }

        [Theory]
        [InlineData(200000, 2, true)]
        [InlineData(220000, 3, true)]
        [InlineData(999999, 1, false)]
        [InlineData(0, 1, false)]
        public async Task PayForBaseExport(decimal payAmount, short status, bool result)
        {
            //Arrange
            var baseExport = new WmsBaseExport()
            {
                ExportId = Guid.NewGuid(),
                TotalCost = 220000,
                PaymentStatus = 1,
                PaidAmount = 0,
                BranchWarehouseId = Guid.NewGuid(),
                ExportStatus = 3
            };
            var branchImport = new WmsBranchImport()
            {
                ImportId = Guid.NewGuid(),
                BaseExportId = baseExport.ExportId,
                TotalCost = 220000,
                PaymentStatus = 1,
                PaidAmount = 0,
            };
            var branchDebt = new WmsBranchDebt()
            {
                DebtAmount = 220000,
            };

            var entity = new PayForBaseExportRequestDto() { ExportId = baseExport.ExportId, PayAmount = payAmount };

            _userRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsUser, bool>>>()))
                .ReturnsAsync(UserData.Data.First());
            _baseExportRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsBaseExport>(x =>
                x.ExportId.Equals(baseExport.ExportId)))).ReturnsAsync(baseExport);
            _branchImportRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsBranchImport>(x =>
                x.BaseExportId.Equals(baseExport.ExportId)))).ReturnsAsync(branchImport);
            _branchDebtRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchDebt, bool>>>())).ReturnsAsync(branchDebt);
            _branchWarehouseRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchWarehouse, bool>>>())).ReturnsAsync(new WmsBranchWarehouse()
            {
                WarehouseName = "Kho HN1",
            });

            //Act
            var actualResult = await _baseExportService.PayForBaseExport(entity, UserData.Data.First().Username);

            //Assert
            Assert.Equal(result, actualResult);
            Assert.Equal(status, baseExport.PaymentStatus);
            Assert.Equal(status, branchImport.PaymentStatus);
            if (result)
            {
                _baseExportRepoMock.Verify(x => x.Update(It.IsAny<WmsBaseExport>()), Times.Once);
                _branchDebtRepoMock.Verify(x => x.Update(It.IsAny<WmsBranchDebt>()), Times.Once);
                _baseInvoiceRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBaseInvoice>()), Times.Once);
                _branchInvoiceRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBranchInvoice>()), Times.Once);
                Assert.Equal(payAmount, baseExport.PaidAmount);
                Assert.Equal(payAmount, branchImport.PaidAmount);
                Assert.Equal(220000 - payAmount, branchDebt.DebtAmount);
            }
            else
            {
                _baseExportRepoMock.Verify(x => x.Update(It.IsAny<WmsBaseExport>()), Times.Never);
                _branchDebtRepoMock.Verify(x => x.Update(It.IsAny<WmsBranchDebt>()), Times.Never);
                _baseInvoiceRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBaseInvoice>()), Times.Never);
                _branchInvoiceRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBranchInvoice>()), Times.Never);
                Assert.Equal(0, baseExport.PaidAmount);
                Assert.Equal(0, branchImport.PaidAmount);
                Assert.Equal(220000, branchDebt.DebtAmount);
            }
        }

        [Fact]
        public async Task PayForBaseExport_ExportIdNotExist()
        {
            //Arrange
            var baseExport = new WmsBaseExport()
            {
                ExportId = Guid.NewGuid(),
                TotalCost = 220000,
                PaymentStatus = 1,
                PaidAmount = 0,
                BranchWarehouseId = Guid.NewGuid(),
            };
            var branchImport = new WmsBranchImport()
            {
                ImportId = Guid.NewGuid(),
                BaseExportId = baseExport.ExportId,
                TotalCost = 220000,
                PaymentStatus = 1,
                PaidAmount = 0,
            };
            var branchDebt = new WmsBranchDebt()
            {
                DebtAmount = 220000,
            };

            var entity = new PayForBaseExportRequestDto() { ExportId = Guid.Empty, PayAmount = 200000 };

            _userRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsUser, bool>>>()))
                .ReturnsAsync(UserData.Data.First());
            _baseExportRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsBaseExport>(x =>
                x.ExportId.Equals(baseExport.ExportId)))).ReturnsAsync(baseExport);
            _branchImportRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsBranchImport>(x =>
                x.BaseExportId.Equals(baseExport.ExportId)))).ReturnsAsync(branchImport);
            _branchDebtRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchDebt, bool>>>())).ReturnsAsync(branchDebt);
            _branchWarehouseRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchWarehouse, bool>>>())).ReturnsAsync(new WmsBranchWarehouse()
            {
                WarehouseName = "Kho HN1",
            });


            //Act
            var actualResult = await _baseExportService.PayForBaseExport(entity, UserData.Data.First().Username);

            //Assert
            Assert.False(actualResult);
            Assert.Equal(1, baseExport.PaymentStatus);
            Assert.Equal(1, branchImport.PaymentStatus);
            _baseExportRepoMock.Verify(x => x.Update(It.IsAny<WmsBaseExport>()), Times.Never);
            _branchDebtRepoMock.Verify(x => x.Update(It.IsAny<WmsBranchDebt>()), Times.Never);
            _baseInvoiceRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBaseInvoice>()), Times.Never);
            _branchInvoiceRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBranchInvoice>()), Times.Never);
            Assert.Equal(0, baseExport.PaidAmount);
            Assert.Equal(0, branchImport.PaidAmount);
            Assert.Equal(220000, branchDebt.DebtAmount);
        }

        [Fact]
        public async Task PayForBaseExport_ExportIsPaid()
        {
            //Arrange
            var baseExport = new WmsBaseExport()
            {
                ExportId = Guid.NewGuid(),
                TotalCost = 220000,
                PaymentStatus = 3,
                PaidAmount = 220000,
                BranchWarehouseId = Guid.NewGuid(),
            };
            var branchImport = new WmsBranchImport()
            {
                ImportId = Guid.NewGuid(),
                BaseExportId = baseExport.ExportId,
                TotalCost = 220000,
                PaymentStatus = 3,
                PaidAmount = 220000,
            };
            var branchDebt = new WmsBranchDebt()
            {
                DebtAmount = 0,
            };

            var entity = new PayForBaseExportRequestDto() { ExportId = baseExport.ExportId, PayAmount = 200000 };

            _userRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsUser, bool>>>()))
                .ReturnsAsync(UserData.Data.First());
            _baseExportRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsBaseExport>(x =>
                x.ExportId.Equals(baseExport.ExportId)))).ReturnsAsync(baseExport);
            _branchImportRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsBranchImport>(x =>
                x.BaseExportId.Equals(baseExport.ExportId)))).ReturnsAsync(branchImport);
            _branchDebtRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchDebt, bool>>>())).ReturnsAsync(branchDebt);
            _branchWarehouseRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchWarehouse, bool>>>())).ReturnsAsync(new WmsBranchWarehouse()
            {
                WarehouseName = "Kho HN1",
            });


            //Act
            var actualResult = await _baseExportService.PayForBaseExport(entity, UserData.Data.First().Username);

            //Assert
            Assert.False(actualResult);
            Assert.Equal(3, baseExport.PaymentStatus);
            Assert.Equal(3, branchImport.PaymentStatus);
            _baseExportRepoMock.Verify(x => x.Update(It.IsAny<WmsBaseExport>()), Times.Never);
            _branchDebtRepoMock.Verify(x => x.Update(It.IsAny<WmsBranchDebt>()), Times.Never);
            _baseInvoiceRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBaseInvoice>()), Times.Never);
            _branchInvoiceRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBranchInvoice>()), Times.Never);
            Assert.Equal(220000, baseExport.PaidAmount);
            Assert.Equal(220000, branchImport.PaidAmount);
            Assert.Equal(0, branchDebt.DebtAmount);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(4)]
        public async Task PayForBaseExport_ExportIsNotExported(int status)
        {
            //Arrange
            var baseExport = new WmsBaseExport()
            {
                ExportId = Guid.NewGuid(),
                TotalCost = 220000,
                PaymentStatus = 1,
                PaidAmount = 0,
                BranchWarehouseId = Guid.NewGuid(),
                ExportStatus = 4,
            };
            var branchImport = new WmsBranchImport()
            {
                ImportId = Guid.NewGuid(),
                BaseExportId = baseExport.ExportId,
                TotalCost = 220000,
                PaymentStatus = 3,
                PaidAmount = 220000,
            };
            var branchDebt = new WmsBranchDebt()
            {
                DebtAmount = 0,
            };

            var entity = new PayForBaseExportRequestDto() { ExportId = baseExport.ExportId, PayAmount = 200000 };

            _userRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsUser, bool>>>()))
                .ReturnsAsync(UserData.Data.First());
            _baseExportRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsBaseExport>(x =>
                x.ExportId.Equals(baseExport.ExportId)))).ReturnsAsync(baseExport);
            _branchImportRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsBranchImport>(x =>
                x.BaseExportId.Equals(baseExport.ExportId)))).ReturnsAsync(branchImport);
            _branchDebtRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchDebt, bool>>>())).ReturnsAsync(branchDebt);
            _branchWarehouseRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchWarehouse, bool>>>())).ReturnsAsync(new WmsBranchWarehouse()
            {
                WarehouseName = "Kho HN1",
            });


            //Act
            var actualResult = await _baseExportService.PayForBaseExport(entity, UserData.Data.First().Username);

            //Assert
            Assert.False(actualResult);
            Assert.Equal(1, baseExport.PaymentStatus);
            Assert.Equal(3, branchImport.PaymentStatus);
            _baseExportRepoMock.Verify(x => x.Update(It.IsAny<WmsBaseExport>()), Times.Never);
            _branchDebtRepoMock.Verify(x => x.Update(It.IsAny<WmsBranchDebt>()), Times.Never);
            _baseInvoiceRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBaseInvoice>()), Times.Never);
            _branchInvoiceRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBranchInvoice>()), Times.Never);
            Assert.Equal(0, baseExport.PaidAmount);
            Assert.Equal(220000, branchImport.PaidAmount);
            Assert.Equal(0, branchDebt.DebtAmount);
        }

        [Fact]
        public async Task GetExportDetails_Should_Return_Result()
        {
            // Arrange
            var exportId = Guid.Parse("fbfa709c-e42b-4b04-baa9-ca5f40864258");

            var baseExport = new WmsBaseExport()
            {
                ExportId = exportId,
                TotalCost = 220000,
                PaymentStatus = 1,
                PaidAmount = 0,
                BranchWarehouseId = Guid.NewGuid(),
                ExportStatus = 3
            };

            _baseExportRepoMock.Setup(repo => repo.GetBaseExportDetailsById(It.IsAny<Guid>()))
                .ReturnsAsync(baseExport);

            _productService.Setup(service => service.GetProductReturnFromBranchByImportId(It.IsAny<Guid>()))
                .ReturnsAsync(new List<GetProductsReturnFromBranchResponseDto>());

            _invoiceServiceMock.Setup(service => service.GetBaseInvoiceByExportId(It.IsAny<Guid>()))
                .ReturnsAsync(new List<InvoiceResponseDto>());

            // Act
            var result = await _baseExportService.GetExportsDetails(exportId);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(exportId, result.ExportId);
            Assert.Equal(baseExport.TotalCost, result.TotalCost);
        }

        [Fact]
        public async Task GetExportDetails_ShouldNot_Return_Result()
        {
            // Arrange
            var exportId = Guid.Empty;

            var baseExport = new WmsBaseExport()
            {
                ExportId = Guid.NewGuid(),
                TotalCost = 220000,
                PaymentStatus = 1,
                PaidAmount = 0,
                BranchWarehouseId = Guid.NewGuid(),
                ExportStatus = 3
            };

            if (exportId != Guid.Empty) _baseExportRepoMock.Setup(repo => repo.GetBaseExportDetailsById(It.IsAny<Guid>()))
                .ReturnsAsync(baseExport);

            _productService.Setup(service => service.GetProductReturnFromBranchByImportId(It.IsAny<Guid>()))
                .ReturnsAsync(new List<GetProductsReturnFromBranchResponseDto>());

            _invoiceServiceMock.Setup(service => service.GetBaseInvoiceByExportId(It.IsAny<Guid>()))
                .ReturnsAsync(new List<InvoiceResponseDto>());

            // Act
            var result = await _baseExportService.GetExportsDetails(exportId);

            // Assert
            Assert.Null(result);
        }

        [Fact]
        public async Task FilterSearchBaseExport_SearchWordExist_Returns_ViewPaging()
        {
            // Arrange
            var requestDto = new GetBaseExportRequestDto()
            {
                SearchWord = "EXPORT_CODE_1",
                ExportStatus = 1,
                PaymentStatus = 1,
                PagingRequest = new PagingRequest
                {
                    CurrentPage = 1,
                    PageRange = 5,
                    PageSize = 10
                },
                BranchWarehouseId = Guid.NewGuid(),
                DateFilter = 1
            };

            // Setup the mocked date filter response
            var startDate = DateTime.Now.AddDays(-7);
            var endDate = DateTime.Now;
            _dateService.Setup(x => x.GetDateTimeByDateFilter(It.IsAny<short>()))
                           .Returns(new DateFilterRequestDto { StartDate = startDate, EndDate = endDate });

            var baseExports = new List<WmsBaseExport>
            {
                new WmsBaseExport
                {
                    ExportCode = "EXPORT_CODE_1",
                    ExportStatus = 1,
                    PaymentStatus = 1,
                },
                new WmsBaseExport
                {
                    ExportCode = "EXPORT_CODE_2",
                    ExportStatus = 1,
                    PaymentStatus = 1,
                },
            };
            var expectedBaseExports = baseExports.Where(x => x.ExportCode.Equals(requestDto.SearchWord))
                .AsQueryable()
                .BuildMock();

            // Setup the repository method call
            _baseExportRepoMock.Setup(x => x.FilterSearchBaseExport(
                                                requestDto.SearchWord,
                                                requestDto.ExportStatus,
                                                requestDto.PaymentStatus,
                                                requestDto.BranchWarehouseId,
                                                startDate,
                                                endDate))
                                    .ReturnsAsync(expectedBaseExports);

            // Mock the S3 storage service response
            _storageService.Setup(x => x.GetFileUrl(It.IsAny<S3RequestData>()))
                                .Returns("mockedURL");

            // Act
            var result = await _baseExportService.GetExportsList(requestDto);

            // Assert
            Assert.NotNull(result);
            Assert.NotEmpty(result.Items);
            Assert.IsType<ViewPaging<GetBaseExportResponseDto>>(result);
        }

        [Fact]
        public async Task FilterSearchBaseExport_SearchWordNotExist_Returns_ViewPaging()
        {
            // Arrange
            var requestDto = new GetBaseExportRequestDto()
            {
                SearchWord = "EXPORT_CODE_3000",
                ExportStatus = 1,
                PaymentStatus = 1,
                PagingRequest = new PagingRequest
                {
                    CurrentPage = 1,
                    PageRange = 5,
                    PageSize = 10
                },
                BranchWarehouseId = Guid.NewGuid(),
                DateFilter = 1
            };

            // Setup the mocked date filter response
            var startDate = DateTime.Now.AddDays(-7);
            var endDate = DateTime.Now;
            _dateService.Setup(x => x.GetDateTimeByDateFilter(It.IsAny<short>()))
                           .Returns(new DateFilterRequestDto { StartDate = startDate, EndDate = endDate });

            var baseExports = new List<WmsBaseExport>
            {
                new WmsBaseExport
                {
                    ExportCode = "EXPORT_CODE_1",
                    ExportStatus = 1,
                    PaymentStatus = 1,
                },
                new WmsBaseExport
                {
                    ExportCode = "EXPORT_CODE_2",
                    ExportStatus = 1,
                    PaymentStatus = 1,
                },
            };
            var expectedBaseExports = baseExports.Where(x => x.ExportCode.Equals(requestDto.SearchWord))
                .AsQueryable()
                .BuildMock();

            // Setup the repository method call
            _baseExportRepoMock.Setup(x => x.FilterSearchBaseExport(
                                                requestDto.SearchWord,
                                                requestDto.ExportStatus,
                                                requestDto.PaymentStatus,
                                                requestDto.BranchWarehouseId,
                                                startDate,
                                                endDate))
                                    .ReturnsAsync(expectedBaseExports);

            // Mock the S3 storage service response
            _storageService.Setup(x => x.GetFileUrl(It.IsAny<S3RequestData>()))
                                .Returns("mockedURL");

            // Act
            var result = await _baseExportService.GetExportsList(requestDto);

            // Assert
            Assert.NotNull(result);
            Assert.Empty(result.Items);
            Assert.IsType<ViewPaging<GetBaseExportResponseDto>>(result);
        }

        [Fact]
        public async Task FilterSearchBaseExport_ExportStatusExist_Returns_ViewPaging()
        {
            // Arrange
            var requestDto = new GetBaseExportRequestDto()
            {
                SearchWord = "EXPORT_CODE_1",
                ExportStatus = 1,
                PaymentStatus = 1,
                PagingRequest = new PagingRequest
                {
                    CurrentPage = 1,
                    PageRange = 5,
                    PageSize = 10
                },
                BranchWarehouseId = Guid.NewGuid(),
                DateFilter = 1
            };

            // Setup the mocked date filter response
            var startDate = DateTime.Now.AddDays(-7);
            var endDate = DateTime.Now;
            _dateService.Setup(x => x.GetDateTimeByDateFilter(It.IsAny<short>()))
                           .Returns(new DateFilterRequestDto { StartDate = startDate, EndDate = endDate });

            var baseExports = new List<WmsBaseExport>
            {
                new WmsBaseExport
                {
                    ExportCode = "EXPORT_CODE_1",
                    ExportStatus = 1,
                    PaymentStatus = 1,
                },
                new WmsBaseExport
                {
                    ExportCode = "EXPORT_CODE_2",
                    ExportStatus = 1,
                    PaymentStatus = 1,
                },
            };
            var expectedBaseExports = baseExports.Where(x => x.ExportStatus == requestDto.ExportStatus)
                .AsQueryable()
                .BuildMock();

            // Setup the repository method call
            _baseExportRepoMock.Setup(x => x.FilterSearchBaseExport(
                                                requestDto.SearchWord,
                                                requestDto.ExportStatus,
                                                requestDto.PaymentStatus,
                                                requestDto.BranchWarehouseId,
                                                startDate,
                                                endDate))
                                    .ReturnsAsync(expectedBaseExports);

            // Mock the S3 storage service response
            _storageService.Setup(x => x.GetFileUrl(It.IsAny<S3RequestData>()))
                                .Returns("mockedURL");

            // Act
            var result = await _baseExportService.GetExportsList(requestDto);

            // Assert
            Assert.NotNull(result);
            Assert.NotEmpty(result.Items);
            Assert.IsType<ViewPaging<GetBaseExportResponseDto>>(result);
        }

        [Fact]
        public async Task FilterSearchBaseExport_ExportStatusNotExist_Returns_ViewPaging()
        {
            // Arrange
            var requestDto = new GetBaseExportRequestDto()
            {
                SearchWord = "EXPORT_CODE_3000",
                ExportStatus = 2,
                PaymentStatus = 1,
                PagingRequest = new PagingRequest
                {
                    CurrentPage = 1,
                    PageRange = 5,
                    PageSize = 10
                },
                BranchWarehouseId = Guid.NewGuid(),
                DateFilter = 1
            };

            // Setup the mocked date filter response
            var startDate = DateTime.Now.AddDays(-7);
            var endDate = DateTime.Now;
            _dateService.Setup(x => x.GetDateTimeByDateFilter(It.IsAny<short>()))
                           .Returns(new DateFilterRequestDto { StartDate = startDate, EndDate = endDate });

            var baseExports = new List<WmsBaseExport>
            {
                new WmsBaseExport
                {
                    ExportCode = "EXPORT_CODE_1",
                    ExportStatus = 1,
                    PaymentStatus = 1,
                },
                new WmsBaseExport
                {
                    ExportCode = "EXPORT_CODE_2",
                    ExportStatus = 1,
                    PaymentStatus = 1,
                },
            };
            var expectedBaseExports = baseExports.Where(x => x.ExportStatus == requestDto.ExportStatus)
                .AsQueryable()
                .BuildMock();

            // Setup the repository method call
            _baseExportRepoMock.Setup(x => x.FilterSearchBaseExport(
                                                requestDto.SearchWord,
                                                requestDto.ExportStatus,
                                                requestDto.PaymentStatus,
                                                requestDto.BranchWarehouseId,
                                                startDate,
                                                endDate))
                                    .ReturnsAsync(expectedBaseExports);

            // Mock the S3 storage service response
            _storageService.Setup(x => x.GetFileUrl(It.IsAny<S3RequestData>()))
                                .Returns("mockedURL");

            // Act
            var result = await _baseExportService.GetExportsList(requestDto);

            // Assert
            Assert.NotNull(result);
            Assert.Empty(result.Items);
            Assert.IsType<ViewPaging<GetBaseExportResponseDto>>(result);
        }

        [Theory]
        [MemberData(nameof(UpdateBaseExportData))]
        public async Task UpdateBaseExport(UpdateBaseExportRequestDto entity, short status, int stock, bool result)
        {
            //Arrange
            var baseExport = new WmsBaseExport()
            {
                ExportId = Guid.NewGuid(),
                BranchWarehouseId = Guid.NewGuid(),
                ExportStatus = status,
                WmsBaseExportProducts = new List<WmsBaseExportProduct>() { new WmsBaseExportProduct() { ProductId = Guid.NewGuid() } }
            };

            var branchRequest = new WmsBranchRequest() { UpdateUser = "assignBy", };

            _userRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsUser>(f => f.Username.Equals("nprettejohns0") && f.Status == (short)UserStatusEnum.Active)))
               .ReturnsAsync(UserData.Data.FirstOrDefault());
            _userRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsUser>(f => f.Username == branchRequest.UpdateUser)))
               .ReturnsAsync(new WmsUser());
            if (entity.ExportBy != Guid.Empty) _userRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsUser>(f => f.UserId.Equals(entity.ExportBy) && f.Status == (short)UserStatusEnum.Active)))
               .ReturnsAsync(new WmsUser());
            _baseWarehouseRepoMock.Setup(x => x.All())
                .ReturnsAsync(new List<WmsBaseWarehouse>() { new WmsBaseWarehouse() { WarehouseId = BranchWarehouseData.Data.First().WarehouseId } });
            if (entity.ExportId != Guid.Empty) _baseExportRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsBaseExport>(f => f.ExportId.Equals(entity.ExportId)))).ReturnsAsync(baseExport);

            _baseExportProductRepoMock.Setup(x => x.FindMultiple(It.IsAny<Expression<Func<WmsBaseExportProduct, bool>>>()))
              .ReturnsAsync(baseExport.WmsBaseExportProducts);
            _baseImportProductRepoMock.Setup(x => x.FindMultiple(It.IsAny<Expression<Func<WmsBaseImportProduct, bool>>>()))
              .ReturnsAsync(new List<WmsBaseImportProduct>() { new WmsBaseImportProduct() { StockQuantity = stock } });
            _baseImportProductRepoMock.Setup(x => x.GetBaseStockOrderByImportDate(It.IsAny<Guid>()))
              .Returns((new List<WmsBaseImportProduct>() { new WmsBaseImportProduct() { StockQuantity = stock } }).AsEnumerable().BuildMock());
            _branchRequestRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchRequest, bool>>>()))
              .ReturnsAsync(branchRequest);

            if (entity.ProductsList.First().ProductId != Guid.Empty)
            {
                _productRepoMock.Setup(x => x.GetById(It.IsAny<Guid>())).ReturnsAsync(new WmsProduct() { Status = 1 });
            }

            //Act
            var actual = await _baseExportService.UpdateBaseExport(entity, UserData.Data.First().Username).ConfigureAwait(false);

            //Assert
            Assert.Equal(result, actual);
            if (result)
            {
                _baseExportRepoMock.Verify(x => x.Update(It.IsAny<WmsBaseExport>()), Times.Once);

            }
        }

        public static IEnumerable<object[]> UpdateBaseExportData()
        {
            yield return new object[]
            {
                new UpdateBaseExportRequestDto() //normal case
                {
                    ExportId = Guid.NewGuid(),
                    ExportBy = UserData.Data.First().UserId,
                    ExportDate = new DateTime(DateTime.Now.AddDays(10).Ticks, DateTimeKind.Unspecified),
                    ExportStatus = 2,
                    ProductsList = new List<UpdateBaseExportProductRequestDto>(){ new UpdateBaseExportProductRequestDto() { ProductId = Guid.NewGuid(), Quantity = 5} }
                }, 1, 10, true
            };
            yield return new object[]
            {
                new UpdateBaseExportRequestDto() //normal case
                {
                    ExportId = Guid.NewGuid(),
                    ExportBy = UserData.Data.First().UserId,
                    ExportDate = new DateTime(DateTime.Now.AddDays(10).Ticks, DateTimeKind.Unspecified),
                    ExportStatus = 4,
                    ProductsList = new List<UpdateBaseExportProductRequestDto>(){ new UpdateBaseExportProductRequestDto() { ProductId = Guid.NewGuid(), Quantity = 5} }
                }, 1, 10, true
            };
            yield return new object[]
            {
                new UpdateBaseExportRequestDto() //export id invalid
                {
                    ExportId = Guid.Empty,
                    ExportBy = UserData.Data.First().UserId,
                    ExportDate = new DateTime(DateTime.Now.AddDays(10).Ticks, DateTimeKind.Unspecified),
                    ExportStatus = 2,
                    ProductsList = new List<UpdateBaseExportProductRequestDto>(){ new UpdateBaseExportProductRequestDto() { ProductId = Guid.NewGuid(), Quantity = 5} }
                }, 1, 10, false
            };
            yield return new object[]
            {
                new UpdateBaseExportRequestDto() //export by invalid
                {
                    ExportId = Guid.NewGuid(),
                    ExportBy = Guid.Empty,
                    ExportDate = new DateTime(DateTime.Now.AddDays(10).Ticks, DateTimeKind.Unspecified),
                    ExportStatus = 2,
                    ProductsList = new List<UpdateBaseExportProductRequestDto>(){ new UpdateBaseExportProductRequestDto() { ProductId = Guid.NewGuid(), Quantity = 5} }
                }, 1, 10, false
            };
            yield return new object[]
            {
                new UpdateBaseExportRequestDto() //date invalid
                {
                    ExportId = Guid.NewGuid(),
                    ExportBy = UserData.Data.First().UserId,
                    ExportDate = new DateTime(DateTime.Now.AddDays(-10).Ticks, DateTimeKind.Unspecified),
                    ExportStatus = 2,
                    ProductsList = new List<UpdateBaseExportProductRequestDto>(){ new UpdateBaseExportProductRequestDto() { ProductId = Guid.NewGuid(), Quantity = 5} }
                }, 1, 10, false
            };
            yield return new object[]
            {
                new UpdateBaseExportRequestDto() //status invalid
                {
                    ExportId = Guid.NewGuid(),
                    ExportBy = UserData.Data.First().UserId,
                    ExportDate = new DateTime(DateTime.Now.AddDays(10).Ticks, DateTimeKind.Unspecified),
                    ExportStatus = 5,
                    ProductsList = new List<UpdateBaseExportProductRequestDto>(){ new UpdateBaseExportProductRequestDto() { ProductId = Guid.NewGuid(), Quantity = 5} }
                }, 1, 10, false
            };
            yield return new object[]
            {
                new UpdateBaseExportRequestDto() //product invalid
                {
                    ExportId = Guid.NewGuid(),
                    ExportBy = UserData.Data.First().UserId,
                    ExportDate = new DateTime(DateTime.Now.AddDays(10).Ticks, DateTimeKind.Unspecified),
                    ExportStatus = 2,
                    ProductsList = new List<UpdateBaseExportProductRequestDto>(){ new UpdateBaseExportProductRequestDto() { ProductId = Guid.Empty, Quantity = 5} }
                }, 1, 10, false
            };
            yield return new object[]
            {
                new UpdateBaseExportRequestDto() //not enough quantity
                {
                    ExportId = Guid.NewGuid(),
                    ExportBy = UserData.Data.First().UserId,
                    ExportDate = new DateTime(DateTime.Now.AddDays(10).Ticks, DateTimeKind.Unspecified),
                    ExportStatus = 2,
                    ProductsList = new List<UpdateBaseExportProductRequestDto>(){ new UpdateBaseExportProductRequestDto() { ProductId = Guid.NewGuid(), Quantity = 5} }
                }, 1, 3, false
            };
            yield return new object[]
            {
                new UpdateBaseExportRequestDto() //already exported
                {
                    ExportId = Guid.NewGuid(),
                    ExportBy = UserData.Data.First().UserId,
                    ExportDate = new DateTime(DateTime.Now.AddDays(10).Ticks, DateTimeKind.Unspecified),
                    ExportStatus = 2,
                    ProductsList = new List<UpdateBaseExportProductRequestDto>(){ new UpdateBaseExportProductRequestDto() { ProductId = Guid.NewGuid(), Quantity = 5} }
                }, 3, 10, false
            };
            yield return new object[]
            {
                new UpdateBaseExportRequestDto() //already refused
                {
                    ExportId = Guid.NewGuid(),
                    ExportBy = UserData.Data.First().UserId,
                    ExportDate = new DateTime(DateTime.Now.AddDays(10).Ticks, DateTimeKind.Unspecified),
                    ExportStatus = 2,
                    ProductsList = new List<UpdateBaseExportProductRequestDto>(){ new UpdateBaseExportProductRequestDto() { ProductId = Guid.NewGuid(), Quantity = 5} }
                }, 4, 10, false
            };
            yield return new object[]
            {
                new UpdateBaseExportRequestDto() //already confirmed
                {
                    ExportId = Guid.NewGuid(),
                    ExportBy = UserData.Data.First().UserId,
                    ExportDate = new DateTime(DateTime.Now.AddDays(10).Ticks, DateTimeKind.Unspecified),
                    ExportStatus = 2,
                    ProductsList = new List<UpdateBaseExportProductRequestDto>(){ new UpdateBaseExportProductRequestDto() { ProductId = Guid.NewGuid(), Quantity = 5} }
                }, 2, 10, false
            };
        }
    }
}
