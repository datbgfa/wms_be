﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using MockQueryable.Moq;
using Moq;
using System.Data;
using WMS.Business.Dtos.CategoryDto;
using WMS.Business.Dtos.CommonDto;
using WMS.Business.Dtos.Mapper;
using WMS.Business.Services;
using WMS.Domain.Abstractions;
using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Entities;

namespace WMS.UnitTest.Business
{
    public class CategoryServiceTest
    {
        private readonly CategoryService _categoryService;
        private readonly Mock<IUnitOfWork> _unitOfWork = new Mock<IUnitOfWork>();
        private readonly Mock<IWmsCategoryRepository> _categoryRepoMock = new Mock<IWmsCategoryRepository>();
        private readonly IMock<ILogger<CategoryService>> _logger = new Mock<ILogger<CategoryService>>();

        public CategoryServiceTest()
        {
            var mockTran = new Mock<IDbTransaction>();


            var category = new WmsCategory()
            {
                CategoryId = 1,
                CategoryName = "Test",
                Visible = true,
            };

            _categoryRepoMock.Setup(x => x.GetById(It.IsAny<int>()))
                .ReturnsAsync(category);

            _unitOfWork.Setup(x => x.BeginTransaction()).Returns(mockTran.Object);
            _unitOfWork.Setup(x => x.CategoryRepository)
                .Returns(_categoryRepoMock.Object);

            var configuration = new MapperConfiguration(config =>
            {
                config.AddProfile<DomainToDtoProfile>();
                config.AddProfile<DtoToDomainProfile>();
            });
            var mapper = new Mapper(configuration);

            _categoryService = new CategoryService(_unitOfWork.Object, _logger.Object, mapper);
        }

        [Fact]
        public async Task GetAll_ReturnValue()
        {
            // Arrange
            var entity = new CategoryRequestDto()
            {
                SearchWord = "",
                PagingRequest = new WMS.Business.Dtos.CommonDto.PagingRequest()
                {
                    CurrentPage = 1,
                    PageRange = 5,
                    PageSize = 10
                },
                Visible = true,
            };

            _categoryRepoMock.Setup(x => x.SearchAndFilterCategory(entity.SearchWord, entity.Visible))
                .Returns(new List<WmsCategory>().AsQueryable());

            // Act
            var result = await _categoryService.GetAll();

            Assert.NotNull(result);
            Assert.False(result.Any());
        }

        [Fact]
        public async Task AddCategory_ReturnTrue()
        {
            // Arrange
            string categoryName = "NewCategory";

            _categoryRepoMock.Setup(repo => repo.AddAsync(It.IsAny<WmsCategory>()))
                .Returns(Task.CompletedTask);

            // Act
            var result = await _categoryService.AddCategory(categoryName);

            // Assert
            Assert.True(result);
            _categoryRepoMock.Verify(repo => repo.AddAsync(It.IsAny<WmsCategory>()), Times.Once);
            _unitOfWork.Verify(uow => uow.CommitAsync(), Times.Once);
        }

        [Fact]
        public async Task AddCategory_ReturnFalse()
        {
            // Arrange
            string categoryName = "NewCategory";

            _categoryRepoMock.Setup(repo => repo.AddAsync(It.IsAny<WmsCategory>()))
                .Returns(Task.CompletedTask);
            _categoryRepoMock.Setup(repo => repo.All())
                .ReturnsAsync(new List<WmsCategory> {
                    new WmsCategory{CategoryId = 1, CategoryName=categoryName, Visible = true}
                });

            // Act
            var result = await _categoryService.AddCategory(categoryName);

            // Assert
            Assert.False(result);
        }

        [Fact]
        public async Task UpdateCategory_ReturnTrue()
        {
            // Arrange
            var requestDto = new UpdateCategoryDto
            {
                CategoryId = 1,
                CategoryName = "Test",
                Visible = true,
            };

            var existingCategoryId = 1;
            var existingCategory = new WmsCategory { CategoryId = existingCategoryId, CategoryName = "ExistingCategory", Visible = true };
            _categoryRepoMock.Setup(repo => repo.GetById(existingCategoryId))
                                  .ReturnsAsync(existingCategory);

            // Act
            var result = await _categoryService.UpdateCategory(requestDto);

            // Assert
            Assert.True(result);
            Assert.Equal(requestDto.CategoryName, existingCategory.CategoryName);
            Assert.Equal(requestDto.Visible, existingCategory.Visible);
            _unitOfWork.Verify(uow => uow.CategoryRepository.Update(existingCategory), Times.Once);
            _unitOfWork.Verify(uow => uow.CommitAsync(), Times.Once);
        }

        [Fact]
        public async Task UpdateCategory_ReturnFalse()
        {
            // Arrange
            var requestDto = new UpdateCategoryDto
            {
                CategoryId = 1,
                CategoryName = "ExistingCategory",
                Visible = true,
            };

            var existingCategoryId = 2;
            var existingCategory = new WmsCategory { CategoryId = existingCategoryId, CategoryName = "ExistingCategory", Visible = true };
            _categoryRepoMock.Setup(repo => repo.All())
                                  .ReturnsAsync(new List<WmsCategory>() { existingCategory });

            // Act
            var result = await _categoryService.UpdateCategory(requestDto);

            // Assert
            Assert.False(result);
            _unitOfWork.Verify(uow => uow.CategoryRepository.Update(existingCategory), Times.Never);
            _unitOfWork.Verify(uow => uow.CommitAsync(), Times.Never);
        }

        [Fact]
        public async Task CheckIfCategoryExists_ReturnsTrue_WhenCategoryExists()
        {
            // Arrange
            var categoryName = "TestCategory";
            var categoryList = new List<WmsCategory>
            {
                new WmsCategory { CategoryName = "TestCategory" },
                new WmsCategory { CategoryName = "AnotherCategory" },
            };

            _categoryRepoMock.Setup(repo => repo.All()).ReturnsAsync(categoryList);

            // Act
            var result = await _categoryService.CheckDuplicateName(categoryName);

            // Assert
            Assert.True(result);
        }

        [Fact]
        public async Task CheckIfCategoryExists_ReturnsFalse_WhenCategoryDoesNotExist()
        {
            // Arrange
            var categoryName = "NonExistentCategory";
            var categoryList = new List<WmsCategory>
            {
                new WmsCategory { CategoryName = "TestCategory" },
                new WmsCategory { CategoryName = "AnotherCategory" },
            };

            _categoryRepoMock.Setup(repo => repo.All()).ReturnsAsync(categoryList);

            // Act
            var result = await _categoryService.CheckDuplicateName(categoryName);

            // Assert
            Assert.False(result);
        }

        [Fact]
        public async Task GetAllWithPaging_Returns_PagedData()
        {
            // Arrange
            var entity = new CategoryRequestDto
            {
                SearchWord = "Category",
                Visible = true,
                PagingRequest = new PagingRequest
                {
                    CurrentPage = 1,
                    PageSize = 10,
                    PageRange = 5
                }
            };

            var testData = new List<WmsCategory> // Assuming Category is the entity type
            {
                new WmsCategory { CategoryId = 1, CategoryName = "Category 1", Visible = true },
                new WmsCategory { CategoryId = 2, CategoryName = "Category 2", Visible = true },
                new WmsCategory { CategoryId = 3, CategoryName = "Category 3", Visible = true },
            }.AsQueryable().BuildMockDbSet();

            _categoryRepoMock.Setup(r => r.SearchAndFilterCategory(It.IsAny<string>(), It.IsAny<bool>()))
                .Returns(testData.Object);

            // Act
            var result = await _categoryService.GetAllWithPaging(entity);

            // Assert
            Assert.NotNull(result);
            Assert.NotNull(result.Items);
            Assert.True(result.Items.Any());
            Assert.NotNull(result.Pagination);
        }

    }
}
