﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using MockQueryable.Moq;
using Moq;
using MoqExpression;
using System.Data;
using System.Linq.Expressions;
using WMS.Business.Abstractions.Service;
using WMS.Business.Dtos.BaseImportDto;
using WMS.Business.Dtos.CommonDto;
using WMS.Business.Dtos.FilterDto;
using WMS.Business.Dtos.Mapper;
using WMS.Business.Dtos.S3Dto;
using WMS.Business.Dtos.WarehouseDto;
using WMS.Business.Services;
using WMS.Domain.Abstractions;
using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Entities;
using WMS.Domain.Enums;
using WMS.UnitTest.DummiesData;
namespace WMS.UnitTest.Business
{
    public class BaseImportServiceTest
    {
        private readonly BaseImportService _baseImportService;
        private readonly Mock<IUnitOfWork> _unitOfWork = new Mock<IUnitOfWork>();
        private readonly Mock<IWmsUserRepository> _userRepoMock = new Mock<IWmsUserRepository>();
        private readonly Mock<IWmsBaseWarehouseRepository> _baseWarehouseRepoMock = new Mock<IWmsBaseWarehouseRepository>();
        private readonly Mock<IWmsBranchWarehouseRepository> _branchWarehouseRepoMock = new Mock<IWmsBranchWarehouseRepository>();
        private readonly Mock<IWmsBaseImportRepository> _baseImportRepoMock = new Mock<IWmsBaseImportRepository>();
        private readonly Mock<IWmsBaseImportProductRepository> _baseImportProductRepoMock = new Mock<IWmsBaseImportProductRepository>();
        private readonly Mock<IWmsSupplierRepository> _supplierRepoMock = new Mock<IWmsSupplierRepository>();
        private readonly Mock<IWmsProductRepository> _productRepoMock = new Mock<IWmsProductRepository>();
        private readonly Mock<IWmsBaseDebtRepository> _baseDebtRepoMock = new Mock<IWmsBaseDebtRepository>();
        private readonly Mock<IWmsBaseInvoiceRepository> _baseInvoiceRepoMock = new Mock<IWmsBaseInvoiceRepository>();
        private readonly Mock<IWmsBaseProductReturnRepository> _baseReturnRepoMock = new Mock<IWmsBaseProductReturnRepository>();


        private readonly IMock<ILogger<BaseImportService>> _logger = new Mock<ILogger<BaseImportService>>();
        private readonly Mock<IS3StorageService> _storageService = new Mock<IS3StorageService>();
        private readonly Mock<IDateService> _dateService = new Mock<IDateService>();
        private readonly Mock<IProductService> _productService = new Mock<IProductService>();
        private readonly Mock<IInvoiceService> _invoiceServiceMock = new Mock<IInvoiceService>();

        public BaseImportServiceTest()
        {
            var mockTran = new Mock<IDbTransaction>();

            _unitOfWork.Setup(x => x.BeginTransaction()).Returns(mockTran.Object);
            _unitOfWork.Setup(x => x.UserRepository).Returns(_userRepoMock.Object);
            _unitOfWork.Setup(x => x.BaseWarehouseRepository).Returns(_baseWarehouseRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchWarehouseRepository).Returns(_branchWarehouseRepoMock.Object);
            _unitOfWork.Setup(x => x.BaseImportRepository).Returns(_baseImportRepoMock.Object);
            _unitOfWork.Setup(x => x.BaseImportProductRepository).Returns(_baseImportProductRepoMock.Object);
            _unitOfWork.Setup(x => x.SupplierRepository).Returns(_supplierRepoMock.Object);
            _unitOfWork.Setup(x => x.ProductRepository).Returns(_productRepoMock.Object);
            _unitOfWork.Setup(x => x.BaseDebtRepository).Returns(_baseDebtRepoMock.Object);
            _unitOfWork.Setup(x => x.BaseInvoiceRepository).Returns(_baseInvoiceRepoMock.Object);
            _unitOfWork.Setup(x => x.BaseProductReturnRepository).Returns(_baseReturnRepoMock.Object);


            var configuration = new MapperConfiguration(config =>
            {
                config.AddProfile<DomainToDtoProfile>();
                config.AddProfile<DtoToDomainProfile>();
            });
            var mapper = new Mapper(configuration);

            _baseImportService = new BaseImportService(_unitOfWork.Object, _logger.Object, mapper,
               _storageService.Object, _dateService.Object, _productService.Object, _invoiceServiceMock.Object);
        }

        [Fact]
        public async Task UpdateBaseImportNote_NormalCase()
        {
            //Arrange
            _userRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsUser, bool>>>())).ReturnsAsync(UserData.Data.First());
            var baseImport = BaseImportData.Data.First();
            _baseImportRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBaseImport, bool>>>())).ReturnsAsync(baseImport);

            //Act
            var result = await _baseImportService.UpdateBaseImportNote(new UpdateBaseImportNoteRequestDto()
            {
                ImportId = BaseImportData.Data.First().ImportId,
                Note = "test note",
            }, UserData.Data.First().Username);

            //Assert
            Assert.True(result);
            _baseImportRepoMock.Verify(x => x.Update(It.IsAny<WmsBaseImport>()), Times.Once);
            Assert.Equal("test note", baseImport.Note);
        }

        [Fact]
        public async Task CreateBaseImport_NormalCase()
        {
            //Arrange
            _userRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsUser, bool>>>()))
                .ReturnsAsync(UserData.Data.First());
            _baseWarehouseRepoMock.Setup(x => x.All())
                .ReturnsAsync(new List<WmsBaseWarehouse>() { new WmsBaseWarehouse() { WarehouseId = BranchWarehouseData.Data.First().WarehouseId } });
            _supplierRepoMock.Setup(x => x.GetById(1))
                .ReturnsAsync(new WmsSupplier() { SupplierId = 1, SupplierName = "Supplier A", Status = true });
            _productRepoMock.Setup(x => x.GetById(It.IsAny<Guid>())).ReturnsAsync(new WmsProduct() { });

            var entity = new CreateBaseImportRequestDto()
            {
                ImportBy = UserData.Data.ElementAt(1).UserId,
                SupplierId = 1,
                RequestDate = new DateTime(DateTime.Now.AddDays(10).Ticks, DateTimeKind.Unspecified),
                ExtraCost = 10000,
                Note = "",
                ProductsList = new List<CreateBaseImportProductRequestDto>()
                {
                    new CreateBaseImportProductRequestDto() { ProductId = Guid.Parse("b08d504e-8e0b-47b9-a51c-50adf96a2338"), ImportedQuantity = 5, UnitPrice = 100000}
                },
            };


            //Act
            var result = await _baseImportService.CreateBaseImport(entity, UserData.Data.First().Username);

            //Assert
            Assert.NotNull(result);
            _baseImportRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBaseImport>()), Times.Once);
            _baseImportProductRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBaseImportProduct>()), Times.Exactly(entity.ProductsList.Count()));
        }

        [Theory]
        [MemberData(nameof(CreateBaseImportData_AbnormalCase))]
        public async Task CreateBaseImport_AbnormalCase(CreateBaseImportRequestDto entity)
        {
            //Arrange
            _userRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsUser>(f => f.Username.Equals("nprettejohns0") && f.Status == (short)UserStatusEnum.Active)))
                .ReturnsAsync(UserData.Data.First());
            _userRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsUser>(f => f.UserId.Equals(UserData.Data.ElementAt(1).UserId))))
                .ReturnsAsync(UserData.Data.ElementAt(1));
            _baseWarehouseRepoMock.Setup(x => x.FindBaseWarehouseByWarehouseOwner(It.IsAny<Guid>()))
                .ReturnsAsync(new WmsBaseWarehouse() { WarehouseId = BranchWarehouseData.Data.First().WarehouseId });
            _supplierRepoMock.Setup(x => x.GetById(1))
                .ReturnsAsync(new WmsSupplier() { SupplierId = 1, SupplierName = "Supplier A", Status = true });
            _productRepoMock.Setup(x => x.GetById(Guid.Parse("b08d504e-8e0b-47b9-a51c-50adf96a2338"))).ReturnsAsync(new WmsProduct() { });

            //Act
            var result = await _baseImportService.CreateBaseImport(entity, UserData.Data.First().Username);

            //Assert
            Assert.Null(result);
            _baseImportRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBaseImport>()), Times.Never);
            _baseImportProductRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBaseImportProduct>()), Times.Never);
        }

        public static IEnumerable<object[]> CreateBaseImportData_AbnormalCase()
        {
            yield return new object[]  //list empty - fail
            {
                new CreateBaseImportRequestDto()
                {
                    ImportBy = UserData.Data.ElementAt(1).UserId,
                    SupplierId = 1,
                    RequestDate = new DateTime(DateTime.Now.AddDays(10).Ticks, DateTimeKind.Unspecified),
                    ExtraCost = 100,
                    Note = "",
                    ProductsList = new List<CreateBaseImportProductRequestDto>()
                    {

                    },
                },
            };
            yield return new object[]  //user does not exist - fail
            {
                new CreateBaseImportRequestDto()
                {
                    ImportBy = Guid.Empty,
                    SupplierId = 1,
                    RequestDate = new DateTime(DateTime.Now.AddDays(10).Ticks, DateTimeKind.Unspecified),
                    ExtraCost = 100,
                    Note = "",
                    ProductsList = new List<CreateBaseImportProductRequestDto>()
                    {
                        new CreateBaseImportProductRequestDto() { ProductId = Guid.Parse("b08d504e-8e0b-47b9-a51c-50adf96a2338"), ImportedQuantity = 5, UnitPrice = 100000}
                    },
                },
            };
            yield return new object[]  //date is invalid - fail
            {
                new CreateBaseImportRequestDto()
                {
                    ImportBy = UserData.Data.ElementAt(1).UserId,
                    SupplierId = 1,
                    RequestDate = new DateTime(DateTime.Now.AddDays(-10).Ticks, DateTimeKind.Unspecified),
                    ExtraCost = 100,
                    Note = "",
                    ProductsList = new List<CreateBaseImportProductRequestDto>()
                    {
                        new CreateBaseImportProductRequestDto() { ProductId = Guid.Parse("b08d504e-8e0b-47b9-a51c-50adf96a2338"), ImportedQuantity = 5, UnitPrice = 100000}
                    },
                },
            };
            yield return new object[]  //supplier does not exist
            {
                new CreateBaseImportRequestDto()
                {
                    ImportBy = UserData.Data.ElementAt(1).UserId,
                    SupplierId = 0,
                    RequestDate = new DateTime(DateTime.Now.AddDays(10).Ticks, DateTimeKind.Unspecified),
                    ExtraCost = 100,
                    Note = "",
                    ProductsList = new List<CreateBaseImportProductRequestDto>()
                    {
                        new CreateBaseImportProductRequestDto() { ProductId = Guid.Parse("b08d504e-8e0b-47b9-a51c-50adf96a2338"), ImportedQuantity = 5, UnitPrice = 100000}
                    },
                },
            };
            yield return new object[]  //product does not exist
            {
                new CreateBaseImportRequestDto()
                {
                    ImportBy = UserData.Data.ElementAt(1).UserId,
                    SupplierId = 0,
                    RequestDate = new DateTime(DateTime.Now.AddDays(10).Ticks, DateTimeKind.Unspecified),
                    ExtraCost = 100,
                    Note = "",
                    ProductsList = new List<CreateBaseImportProductRequestDto>()
                    {
                        new CreateBaseImportProductRequestDto() { ProductId = Guid.Parse("b08d504e-8e0b-47b9-a51c-50adf96a2332"), ImportedQuantity = 5, UnitPrice = 100000}
                    },
                },
            };
        }

        [Fact]
        public async Task ChangeBaseImportStatus_NormalCase()
        {
            //Arrange
            var baseImport = BaseImportData.Data.First();
            var baseImportProducts = new List<WmsBaseImportProduct>() { new WmsBaseImportProduct()
                {
                    ImportId = baseImport.ImportId,
                    ProductId = Guid.Parse("b08d504e-8e0b-47b9-a51c-50adf96a2338"),
                    CostPrice = 55000,
                    UnitPrice = 50000,
                    ImportedQuantity = 4,
                    StockQuantity = 0,
                }
            };
            var baseDebt = new WmsBaseDebt()
            {
                DebtAmount = 0,
            };
            var product = new WmsProduct()
            {
                CostPrice = 0,
            };
            _userRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsUser, bool>>>()))
                .ReturnsAsync(UserData.Data.First());
            _baseWarehouseRepoMock.Setup(x => x.FindBaseWarehouseByWarehouseOwner(It.IsAny<Guid>()))
                .ReturnsAsync(new WmsBaseWarehouse() { WarehouseId = BranchWarehouseData.Data.First().WarehouseId });
            _supplierRepoMock.Setup(x => x.GetById(1))
                .ReturnsAsync(new WmsSupplier() { SupplierId = 1, SupplierName = "Supplier A", Status = true });
            _productRepoMock.Setup(x => x.GetById(It.IsAny<Guid>())).ReturnsAsync(product);
            _productRepoMock.Setup(x => x.GetCostPriceByProductId(It.IsAny<Guid>())).Returns(60000);
            _baseImportRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBaseImport, bool>>>())).ReturnsAsync(baseImport);
            _baseImportProductRepoMock.Setup(x => x.FindMultiple(
                MoqHelper.IsExpression<WmsBaseImportProduct>(x => x.ImportId.Equals(baseImport.ImportId))))
                .ReturnsAsync(baseImportProducts);
            _baseImportProductRepoMock.Setup(x => x.FindMultiple(
                MoqHelper.IsExpression<WmsBaseImportProduct>(x => x.ImportId.Equals(baseImport.ImportId))))
                .ReturnsAsync(baseImportProducts);
            _baseDebtRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBaseDebt, bool>>>())).ReturnsAsync(baseDebt);


            //Act
            var result = await _baseImportService.ChangeBaseImportStatus(baseImport.ImportId, UserData.Data.First().Username);

            //Assert
            Assert.True(result);
            _baseImportRepoMock.Verify(x => x.Update(It.IsAny<WmsBaseImport>()), Times.Once);
            _baseDebtRepoMock.Verify(x => x.Update(It.IsAny<WmsBaseDebt>()), Times.Once);
            _baseImportProductRepoMock.Verify(x => x.Update(It.IsAny<WmsBaseImportProduct>()), Times.Exactly(baseImportProducts.Count));
            Assert.Equal(2, baseImport.ImportStatus);
            Assert.Equal(220000, baseDebt.DebtAmount);
            foreach (var p in baseImportProducts)
            {
                Assert.Equal(p.ImportedQuantity, p.StockQuantity);
            }
            _productRepoMock.Verify(x => x.Update(It.IsAny<WmsProduct>()), Times.Exactly(baseImportProducts.Count));
            Assert.Equal(60000, product.CostPrice);
        }

        [Fact]
        public async Task ChangeBaseImportStatus_InvalidImportId()
        {
            //Arrange
            var baseImport = BaseImportData.Data.First();
            var baseImportProducts = new List<WmsBaseImportProduct>() { new WmsBaseImportProduct()
                {
                    ImportId = baseImport.ImportId,
                    ProductId = Guid.Parse("b08d504e-8e0b-47b9-a51c-50adf96a2338"),
                    CostPrice = 55000,
                    UnitPrice = 50000,
                    ImportedQuantity = 4,
                    StockQuantity = 0,
                }
            };
            var baseDebt = new WmsBaseDebt()
            {
                DebtAmount = 0,
            };
            _userRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsUser, bool>>>()))
                .ReturnsAsync(UserData.Data.First());
            _baseWarehouseRepoMock.Setup(x => x.FindBaseWarehouseByWarehouseOwner(It.IsAny<Guid>()))
                .ReturnsAsync(new WmsBaseWarehouse() { WarehouseId = BranchWarehouseData.Data.First().WarehouseId });
            _supplierRepoMock.Setup(x => x.GetById(1))
                .ReturnsAsync(new WmsSupplier() { SupplierId = 1, SupplierName = "Supplier A", Status = true });
            _productRepoMock.Setup(x => x.GetById(It.IsAny<Guid>())).ReturnsAsync(new WmsProduct() { });
            _baseImportRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsBaseImport>(x =>
            x.ImportId.Equals(Guid.Parse("aa7778c5-00f0-47fd-b40d-0e4b7fbb2739"))))).ReturnsAsync(baseImport);
            _baseImportProductRepoMock.Setup(x => x.FindMultiple(It.IsAny<Expression<Func<WmsBaseImportProduct, bool>>>()))
                .ReturnsAsync(baseImportProducts);
            _baseDebtRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBaseDebt, bool>>>())).ReturnsAsync(baseDebt);

            //Act
            var result = await _baseImportService.ChangeBaseImportStatus(baseImport.ImportId, UserData.Data.First().Username);

            //Assert
            Assert.False(result);
            _baseImportRepoMock.Verify(x => x.Update(It.IsAny<WmsBaseImport>()), Times.Never);
            _baseDebtRepoMock.Verify(x => x.Update(It.IsAny<WmsBaseDebt>()), Times.Never);
            _baseImportProductRepoMock.Verify(x => x.Update(It.IsAny<WmsBaseImportProduct>()), Times.Never);
            Assert.Equal(1, baseImport.ImportStatus);
            Assert.Equal(0, baseDebt.DebtAmount);
            foreach (var p in baseImportProducts)
            {
                Assert.Equal(0, p.StockQuantity);
            }
        }

        [Fact]
        public async Task UpdateBaseImport_NormalCase()
        {
            //Arrange
            var baseImport = BaseImportData.Data.First();
            var baseImportProducts = new List<WmsBaseImportProduct>() { new WmsBaseImportProduct()
                {
                    ImportId = baseImport.ImportId,
                    ProductId = Guid.Parse("b08d504e-8e0b-47b9-a51c-50adf96a2338"),
                    CostPrice = 55000,
                    UnitPrice = 50000,
                    ImportedQuantity = 4,
                    StockQuantity = 0,
                }
            };

            var entity = new UpdateBaseImportRequestDto()
            {
                ImportId = baseImport.ImportId,
                ImportBy = UserData.Data.ElementAt(1).UserId,
                SupplierId = 1,
                RequestDate = new DateTime(DateTime.Now.AddDays(10).Ticks, DateTimeKind.Unspecified),
                ExtraCost = 10000,
                Note = "",
                ProductsList = new List<CreateBaseImportProductRequestDto>()
                {
                    new CreateBaseImportProductRequestDto() { ProductId = Guid.Parse("b08d504e-8e0b-47b9-a51c-50adf96a2338"), ImportedQuantity = 5, UnitPrice = 100000}
                },
            };

            _userRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsUser>(f => f.UserId.Equals(entity.ImportBy))))
                .ReturnsAsync(UserData.Data.ElementAt(1));
            _userRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsUser>(x => x.Username.Equals("nprettejohns0")
                && x.Status == (short)UserStatusEnum.Active)))
                .ReturnsAsync(UserData.Data.First());
            _baseWarehouseRepoMock.Setup(x => x.All())
                .ReturnsAsync(new List<WmsBaseWarehouse>() { new WmsBaseWarehouse() { WarehouseId = BranchWarehouseData.Data.First().WarehouseId } });
            _supplierRepoMock.Setup(x => x.GetById(1))
                .ReturnsAsync(new WmsSupplier() { SupplierId = 1, SupplierName = "Supplier A", Status = true });
            _productRepoMock.Setup(x => x.GetById(It.IsAny<Guid>())).ReturnsAsync(new WmsProduct() { });
            _baseImportRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsBaseImport>(x =>
                x.ImportId.Equals(baseImport.ImportId)))).ReturnsAsync(baseImport);
            _baseImportProductRepoMock.Setup(x => x.FindMultiple(MoqHelper.IsExpression<WmsBaseImportProduct>(
                x => x.ImportId.Equals(baseImport.ImportId))))
                .ReturnsAsync(baseImportProducts);

            //Act
            var result = await _baseImportService.UpdateBaseImport(entity, UserData.Data.First().Username);

            //Assert
            Assert.True(result);
            _baseImportRepoMock.Verify(x => x.Update(It.IsAny<WmsBaseImport>()), Times.Once);
            _baseImportProductRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBaseImportProduct>()), Times.Exactly(entity.ProductsList.Count()));
            _baseImportProductRepoMock.Verify(x => x.Delete(It.IsAny<WmsBaseImportProduct>()), Times.Exactly(baseImportProducts.Count()));
            Assert.Equal(510000, baseImport.TotalCost);
            Assert.Equal(500000, baseImport.TotalPrice);
        }

        [Theory]
        [MemberData(nameof(UpdateBaseImportData_AbnormalCase))]
        public async Task UpdateBaseImport_AbnormalCase(UpdateBaseImportRequestDto entity)
        {
            //Arrange
            var baseImport = BaseImportData.Data.First();
            var baseImportProducts = new List<WmsBaseImportProduct>() { new WmsBaseImportProduct()
                {
                    ImportId = baseImport.ImportId,
                    ProductId = Guid.Parse("b08d504e-8e0b-47b9-a51c-50adf96a2338"),
                    CostPrice = 55000,
                    UnitPrice = 50000,
                    ImportedQuantity = 4,
                    StockQuantity = 0,
                }
            };

            _userRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsUser>(f => f.UserId
                .Equals(Guid.Parse("60b5f8df-1685-4e35-aded-8ec8b4dff558")))))
                .ReturnsAsync(UserData.Data.ElementAt(1));
            _userRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsUser>(x => x.Username.Equals("nprettejohns0")
                && x.Status == (short)UserStatusEnum.Active)))
                .ReturnsAsync(UserData.Data.First());
            _baseWarehouseRepoMock.Setup(x => x.FindBaseWarehouseByWarehouseOwner(It.IsAny<Guid>()))
                .ReturnsAsync(new WmsBaseWarehouse() { WarehouseId = BranchWarehouseData.Data.First().WarehouseId });
            _supplierRepoMock.Setup(x => x.GetById(1))
                .ReturnsAsync(new WmsSupplier() { SupplierId = 1, SupplierName = "Supplier A", Status = true });
            _productRepoMock.Setup(x => x.GetById(It.IsAny<Guid>())).ReturnsAsync(new WmsProduct() { });
            _baseImportRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsBaseImport>(x =>
                x.ImportId.Equals(baseImport.ImportId)))).ReturnsAsync(baseImport);
            _baseImportProductRepoMock.Setup(x => x.FindMultiple(MoqHelper.IsExpression<WmsBaseImportProduct>(
                x => x.ImportId.Equals(baseImport.ImportId))))
                .ReturnsAsync(baseImportProducts);

            //Act
            var result = await _baseImportService.UpdateBaseImport(entity, UserData.Data.First().Username);

            //Assert
            Assert.False(result);
            _baseImportRepoMock.Verify(x => x.Update(It.IsAny<WmsBaseImport>()), Times.Never);
            _baseImportProductRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBaseImportProduct>()), Times.Never);
            _baseImportProductRepoMock.Verify(x => x.Delete(It.IsAny<WmsBaseImportProduct>()), Times.Never);
            Assert.Equal(220000, baseImport.TotalCost);
            Assert.Equal(200000, baseImport.TotalPrice);
        }

        public static IEnumerable<object[]> UpdateBaseImportData_AbnormalCase()
        {
            yield return new object[]  //list empty - fail
            {
                new UpdateBaseImportRequestDto()
                {
                    ImportId = BaseImportData.Data.First().ImportId,
                    ImportBy = Guid.Empty,
                    SupplierId = 1,
                    RequestDate = new DateTime(DateTime.Now.AddDays(10).Ticks, DateTimeKind.Unspecified),
                    ExtraCost = 100,
                    Note = "",
                    ProductsList = new List<CreateBaseImportProductRequestDto>()
                    {

                    },
                },
            };
            yield return new object[]  //user does not exist - fail
            {
                new UpdateBaseImportRequestDto()
                {
                    ImportId = BaseImportData.Data.First().ImportId,
                    ImportBy = Guid.Empty,
                    SupplierId = 1,
                    RequestDate = new DateTime(DateTime.Now.AddDays(10).Ticks, DateTimeKind.Unspecified),
                    ExtraCost = 100,
                    Note = "",
                    ProductsList = new List<CreateBaseImportProductRequestDto>()
                    {
                        new CreateBaseImportProductRequestDto() { ProductId = Guid.Parse("b08d504e-8e0b-47b9-a51c-50adf96a2338"), ImportedQuantity = 5, UnitPrice = 100000}
                    },
                },
            };
            yield return new object[]  //date is invalid - fail
            {
                new UpdateBaseImportRequestDto()
                {
                    ImportId = BaseImportData.Data.First().ImportId,
                    ImportBy = UserData.Data.ElementAt(1).UserId,
                    SupplierId = 1,
                    RequestDate = new DateTime(DateTime.Now.AddDays(-10).Ticks, DateTimeKind.Unspecified),
                    ExtraCost = 100,
                    Note = "",
                    ProductsList = new List<CreateBaseImportProductRequestDto>()
                    {
                        new CreateBaseImportProductRequestDto() { ProductId = Guid.Parse("b08d504e-8e0b-47b9-a51c-50adf96a2338"), ImportedQuantity = 5, UnitPrice = 100000}
                    },
                },
            };
            yield return new object[]  //supplier does not exist
            {
                new UpdateBaseImportRequestDto()
                {
                    ImportId = BaseImportData.Data.First().ImportId,
                    ImportBy = UserData.Data.ElementAt(1).UserId,
                    SupplierId = 0,
                    RequestDate = new DateTime(DateTime.Now.AddDays(10).Ticks, DateTimeKind.Unspecified),
                    ExtraCost = 100,
                    Note = "",
                    ProductsList = new List<CreateBaseImportProductRequestDto>()
                    {
                        new CreateBaseImportProductRequestDto() { ProductId = Guid.Parse("b08d504e-8e0b-47b9-a51c-50adf96a2338"), ImportedQuantity = 5, UnitPrice = 100000}
                    },
                },
            };
            yield return new object[]  //product does not exist
            {
                new UpdateBaseImportRequestDto()
                {
                    ImportId = BaseImportData.Data.First().ImportId,
                    ImportBy = UserData.Data.ElementAt(1).UserId,
                    SupplierId = 0,
                    RequestDate = new DateTime(DateTime.Now.AddDays(10).Ticks, DateTimeKind.Unspecified),
                    ExtraCost = 100,
                    Note = "",
                    ProductsList = new List<CreateBaseImportProductRequestDto>()
                    {
                        new CreateBaseImportProductRequestDto() { ProductId = Guid.Parse("b08d504e-8e0b-47b9-a51c-50adf96a2332"), ImportedQuantity = 5, UnitPrice = 100000}
                    },
                },
            };
            yield return new object[]  //import does not exist
            {
                new UpdateBaseImportRequestDto()
                {
                    ImportId = Guid.Parse("c20d504e-8e0b-47b9-a51c-89adf9238932"),
                    ImportBy = UserData.Data.ElementAt(1).UserId,
                    SupplierId = 0,
                    RequestDate = new DateTime(DateTime.Now.AddDays(10).Ticks, DateTimeKind.Unspecified),
                    ExtraCost = 100,
                    Note = "",
                    ProductsList = new List<CreateBaseImportProductRequestDto>()
                    {
                        new CreateBaseImportProductRequestDto() { ProductId = Guid.Parse("b08d504e-8e0b-47b9-a51c-50adf96a2332"), ImportedQuantity = 5, UnitPrice = 100000}
                    },
                },
            };
        }

        [Theory]
        [InlineData(200000, 2)]
        [InlineData(220000, 3)]
        public async Task PayForBaseImport_NormalCase(decimal payAmount, short status)
        {
            //Arrange
            var baseImport = BaseImportData.Data.First();
            baseImport.ImportStatus = 2;
            var baseImportProducts = new List<WmsBaseImportProduct>() { new WmsBaseImportProduct()
                {
                    ImportId = baseImport.ImportId,
                    ProductId = Guid.Parse("b08d504e-8e0b-47b9-a51c-50adf96a2338"),
                    CostPrice = 55000,
                    UnitPrice = 50000,
                    ImportedQuantity = 4,
                    StockQuantity = 0,
                }
            };
            var baseDebt = new WmsBaseDebt()
            {
                DebtAmount = 220000,
            };

            var entity = new PayForBaseImportRequestDto() { ImportId = baseImport.ImportId, PayAmount = payAmount };

            _userRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsUser, bool>>>()))
                .ReturnsAsync(UserData.Data.First());
            _baseWarehouseRepoMock.Setup(x => x.FindBaseWarehouseByWarehouseOwner(It.IsAny<Guid>()))
                .ReturnsAsync(new WmsBaseWarehouse() { WarehouseId = BranchWarehouseData.Data.First().WarehouseId });
            _supplierRepoMock.Setup(x => x.GetById(1))
                .ReturnsAsync(new WmsSupplier() { SupplierId = 1, SupplierName = "Supplier A", Status = true });
            _productRepoMock.Setup(x => x.GetById(It.IsAny<Guid>())).ReturnsAsync(new WmsProduct() { });
            _baseImportRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsBaseImport>(x =>
                x.ImportId.Equals(baseImport.ImportId)))).ReturnsAsync(baseImport);
            _baseImportProductRepoMock.Setup(x => x.FindMultiple(MoqHelper.IsExpression<WmsBaseImportProduct>(
                x => x.ImportId.Equals(baseImport.ImportId))))
                .ReturnsAsync(baseImportProducts);
            _baseDebtRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBaseDebt, bool>>>())).ReturnsAsync(baseDebt);


            //Act
            var result = await _baseImportService.PayForBaseImport(entity, UserData.Data.First().Username);

            //Assert
            Assert.True(result);
            _baseImportRepoMock.Verify(x => x.Update(It.IsAny<WmsBaseImport>()), Times.Once);
            _baseDebtRepoMock.Verify(x => x.Update(It.IsAny<WmsBaseDebt>()), Times.Once);
            _baseInvoiceRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBaseInvoice>()), Times.Once);
            Assert.Equal(payAmount, baseImport.PaidAmount);
            Assert.Equal(status, baseImport.PaymentStatus);
            Assert.Equal(220000 - payAmount, baseDebt.DebtAmount);
        }

        [Fact]
        public async Task PayForBaseImport_InvalidPayAmount()
        {
            //Arrange
            var baseImport = BaseImportData.Data.First();
            var baseDebt = new WmsBaseDebt()
            {
                DebtAmount = 220000,
            };

            var entity = new PayForBaseImportRequestDto() { ImportId = baseImport.ImportId, PayAmount = 999999999 };

            _userRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsUser, bool>>>()))
                .ReturnsAsync(UserData.Data.First());
            _baseWarehouseRepoMock.Setup(x => x.FindBaseWarehouseByWarehouseOwner(It.IsAny<Guid>()))
                .ReturnsAsync(new WmsBaseWarehouse() { WarehouseId = BranchWarehouseData.Data.First().WarehouseId });
            _supplierRepoMock.Setup(x => x.GetById(1))
                .ReturnsAsync(new WmsSupplier() { SupplierId = 1, SupplierName = "Supplier A", Status = true });
            _productRepoMock.Setup(x => x.GetById(It.IsAny<Guid>())).ReturnsAsync(new WmsProduct() { });
            _baseImportRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsBaseImport>(x =>
                x.ImportId.Equals(baseImport.ImportId)))).ReturnsAsync(baseImport);
            _baseDebtRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBaseDebt, bool>>>())).ReturnsAsync(baseDebt);


            //Act
            var result = await _baseImportService.PayForBaseImport(entity, UserData.Data.First().Username);

            //Assert
            Assert.False(result);
            _baseImportRepoMock.Verify(x => x.Update(It.IsAny<WmsBaseImport>()), Times.Never);
            _baseDebtRepoMock.Verify(x => x.Update(It.IsAny<WmsBaseDebt>()), Times.Never);
            _baseInvoiceRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBaseInvoice>()), Times.Never);
            Assert.Equal(0, baseImport.PaidAmount);
            Assert.Equal(220000, baseDebt.DebtAmount);
        }

        [Fact]
        public async Task PayForBaseImport_ImportIdDoesNotExist()
        {
            //Arrange
            var baseImport = BaseImportData.Data.First();
            var baseDebt = new WmsBaseDebt()
            {
                DebtAmount = 220000,
            };

            var entity = new PayForBaseImportRequestDto() { ImportId = Guid.Empty, PayAmount = 999999999 };

            _userRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsUser, bool>>>()))
                .ReturnsAsync(UserData.Data.First());
            _baseWarehouseRepoMock.Setup(x => x.FindBaseWarehouseByWarehouseOwner(It.IsAny<Guid>()))
                .ReturnsAsync(new WmsBaseWarehouse() { WarehouseId = BranchWarehouseData.Data.First().WarehouseId });
            _supplierRepoMock.Setup(x => x.GetById(1))
                .ReturnsAsync(new WmsSupplier() { SupplierId = 1, SupplierName = "Supplier A", Status = true });
            _productRepoMock.Setup(x => x.GetById(It.IsAny<Guid>())).ReturnsAsync(new WmsProduct() { });
            _baseImportRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsBaseImport>(x =>
                x.ImportId.Equals(baseImport.ImportId)))).ReturnsAsync(baseImport);
            _baseDebtRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBaseDebt, bool>>>())).ReturnsAsync(baseDebt);


            //Act
            var result = await _baseImportService.PayForBaseImport(entity, UserData.Data.First().Username);

            //Assert
            Assert.False(result);
            _baseImportRepoMock.Verify(x => x.Update(It.IsAny<WmsBaseImport>()), Times.Never);
            _baseDebtRepoMock.Verify(x => x.Update(It.IsAny<WmsBaseDebt>()), Times.Never);
            _baseInvoiceRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBaseInvoice>()), Times.Never);
            Assert.Equal(0, baseImport.PaidAmount);
            Assert.Equal(220000, baseDebt.DebtAmount);
        }

        [Fact]
        public async Task PayForBaseImport_PayAmountIsZero()
        {
            //Arrange
            var baseImport = BaseImportData.Data.First();
            var baseDebt = new WmsBaseDebt()
            {
                DebtAmount = 220000,
            };

            var entity = new PayForBaseImportRequestDto() { ImportId = baseImport.ImportId, PayAmount = 0 };

            _userRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsUser, bool>>>()))
                .ReturnsAsync(UserData.Data.First());
            _baseWarehouseRepoMock.Setup(x => x.FindBaseWarehouseByWarehouseOwner(It.IsAny<Guid>()))
                .ReturnsAsync(new WmsBaseWarehouse() { WarehouseId = BranchWarehouseData.Data.First().WarehouseId });
            _supplierRepoMock.Setup(x => x.GetById(1))
                .ReturnsAsync(new WmsSupplier() { SupplierId = 1, SupplierName = "Supplier A", Status = true });
            _productRepoMock.Setup(x => x.GetById(It.IsAny<Guid>())).ReturnsAsync(new WmsProduct() { });
            _baseImportRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsBaseImport>(x =>
                x.ImportId.Equals(baseImport.ImportId)))).ReturnsAsync(baseImport);
            _baseDebtRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBaseDebt, bool>>>())).ReturnsAsync(baseDebt);


            //Act
            var result = await _baseImportService.PayForBaseImport(entity, UserData.Data.First().Username);

            //Assert
            Assert.False(result);
            _baseImportRepoMock.Verify(x => x.Update(It.IsAny<WmsBaseImport>()), Times.Never);
            _baseDebtRepoMock.Verify(x => x.Update(It.IsAny<WmsBaseDebt>()), Times.Never);
            _baseInvoiceRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBaseInvoice>()), Times.Never);
            Assert.Equal(0, baseImport.PaidAmount);
            Assert.Equal(220000, baseDebt.DebtAmount);
        }

        [Fact]
        public async Task PayForBaseImport_ImportIsPaid()
        {
            //Arrange
            var baseImport = BaseImportData.Data.First();
            baseImport.PaymentStatus = 2;
            baseImport.PaidAmount = 220000;
            var baseDebt = new WmsBaseDebt()
            {
                DebtAmount = 0,
            };

            var entity = new PayForBaseImportRequestDto() { ImportId = baseImport.ImportId, PayAmount = 100000 };

            _userRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsUser, bool>>>()))
                .ReturnsAsync(UserData.Data.First());
            _baseWarehouseRepoMock.Setup(x => x.FindBaseWarehouseByWarehouseOwner(It.IsAny<Guid>()))
                .ReturnsAsync(new WmsBaseWarehouse() { WarehouseId = BranchWarehouseData.Data.First().WarehouseId });
            _supplierRepoMock.Setup(x => x.GetById(1))
                .ReturnsAsync(new WmsSupplier() { SupplierId = 1, SupplierName = "Supplier A", Status = true });
            _productRepoMock.Setup(x => x.GetById(It.IsAny<Guid>())).ReturnsAsync(new WmsProduct() { });
            _baseImportRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsBaseImport>(x =>
                x.ImportId.Equals(baseImport.ImportId)))).ReturnsAsync(baseImport);
            _baseDebtRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBaseDebt, bool>>>())).ReturnsAsync(baseDebt);


            //Act
            var result = await _baseImportService.PayForBaseImport(entity, UserData.Data.First().Username);

            //Assert
            Assert.False(result);
            _baseImportRepoMock.Verify(x => x.Update(It.IsAny<WmsBaseImport>()), Times.Never);
            _baseDebtRepoMock.Verify(x => x.Update(It.IsAny<WmsBaseDebt>()), Times.Never);
            _baseInvoiceRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBaseInvoice>()), Times.Never);
            Assert.Equal(220000, baseImport.PaidAmount);
            Assert.Equal(0, baseDebt.DebtAmount);
        }

        [Theory]
        [InlineData(90000, 2, true)]
        [InlineData(100000, 3, true)]
        [InlineData(999999, 1, false)]
        [InlineData(0, 1, false)]
        public async Task PayForBaseReturn(decimal payAmount, short status, bool result)
        {
            //Arrange
            var baseImport = BaseImportData.Data.First();
            var baseDebt = new WmsBaseDebt()
            {
                DebtAmount = 500000,
            };
            var baseReturn = BaseReturnData.Data.First();


            var entity = new PayForBaseProductReturnRequestDto() { BaseProductReturnId = baseReturn.ProductReturnId, PayAmount = payAmount };

            _userRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsUser, bool>>>()))
                .ReturnsAsync(UserData.Data.First());
            _supplierRepoMock.Setup(x => x.GetById(1))
                .ReturnsAsync(new WmsSupplier() { SupplierId = 1, SupplierName = "Supplier A", Status = true });
            _baseImportRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsBaseImport>(x =>
                x.ImportId.Equals(baseImport.ImportId)))).ReturnsAsync(baseImport);
            _baseDebtRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBaseDebt, bool>>>())).ReturnsAsync(baseDebt);
            _baseReturnRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBaseProductReturn, bool>>>())).ReturnsAsync(baseReturn);


            //Act
            var actualResult = await _baseImportService.PayForBaseProductReturn(entity, UserData.Data.First().Username);

            //Assert
            Assert.Equal(result, actualResult);
            if (result)
            {
                _baseReturnRepoMock.Verify(x => x.Update(It.IsAny<WmsBaseProductReturn>()), Times.Once);
                _baseDebtRepoMock.Verify(x => x.Update(It.IsAny<WmsBaseDebt>()), Times.Once);
                _baseInvoiceRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBaseInvoice>()), Times.Once);
                Assert.Equal(payAmount, baseReturn.PaidAmount);
                Assert.Equal(status, baseReturn.PaymentStatus);
                Assert.Equal(500000 + payAmount, baseDebt.DebtAmount);
            }
            else
            {
                _baseReturnRepoMock.Verify(x => x.Update(It.IsAny<WmsBaseProductReturn>()), Times.Never);
                _baseDebtRepoMock.Verify(x => x.Update(It.IsAny<WmsBaseDebt>()), Times.Never);
                _baseInvoiceRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBaseInvoice>()), Times.Never);
                Assert.Equal(0, baseReturn.PaidAmount);
                Assert.Equal(status, baseReturn.PaymentStatus);
                Assert.Equal(500000, baseDebt.DebtAmount);
            }
        }

        [Fact]
        public async Task PayForBaseReturn_ReturnIdNotExist()
        {
            //Arrange
            var baseImport = BaseImportData.Data.First();
            var baseDebt = new WmsBaseDebt()
            {
                DebtAmount = 500000,
            };
            var baseReturn = BaseReturnData.Data.First();


            var entity = new PayForBaseProductReturnRequestDto() { BaseProductReturnId = Guid.Empty, PayAmount = 100000 };

            _userRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsUser, bool>>>()))
                .ReturnsAsync(UserData.Data.First());
            _supplierRepoMock.Setup(x => x.GetById(1))
                .ReturnsAsync(new WmsSupplier() { SupplierId = 1, SupplierName = "Supplier A", Status = true });
            _baseImportRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsBaseImport>(x =>
                x.ImportId.Equals(baseImport.ImportId)))).ReturnsAsync(baseImport);
            _baseDebtRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBaseDebt, bool>>>())).ReturnsAsync(baseDebt);
            _baseReturnRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsBaseProductReturn>(x =>
                x.ProductReturnId.Equals(Guid.Parse("bb2978c5-20f0-47fd-b50d-0e4b7fbb2089"))))).ReturnsAsync(baseReturn);


            //Act
            var actualResult = await _baseImportService.PayForBaseProductReturn(entity, UserData.Data.First().Username);

            //Assert
            Assert.False(actualResult);
            _baseReturnRepoMock.Verify(x => x.Update(It.IsAny<WmsBaseProductReturn>()), Times.Never);
            _baseDebtRepoMock.Verify(x => x.Update(It.IsAny<WmsBaseDebt>()), Times.Never);
            _baseInvoiceRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBaseInvoice>()), Times.Never);
            Assert.Equal(0, baseReturn.PaidAmount);
            Assert.Equal(1, baseReturn.PaymentStatus);
            Assert.Equal(500000, baseDebt.DebtAmount);
        }

        [Fact]
        public async Task PayForBaseReturn_ReturnIsPaid()
        {
            //Arrange
            var baseImport = BaseImportData.Data.First();
            var baseDebt = new WmsBaseDebt()
            {
                DebtAmount = 500000,
            };
            var baseReturn = BaseReturnData.Data.First();
            baseReturn.PaymentStatus = 3;
            baseReturn.PaidAmount = 100000;


            var entity = new PayForBaseProductReturnRequestDto() { BaseProductReturnId = Guid.Parse("bb2978c5-20f0-47fd-b50d-0e4b7fbb2089"), PayAmount = 100000 };

            _userRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsUser, bool>>>()))
                .ReturnsAsync(UserData.Data.First());
            _supplierRepoMock.Setup(x => x.GetById(1))
                .ReturnsAsync(new WmsSupplier() { SupplierId = 1, SupplierName = "Supplier A", Status = true });
            _baseImportRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsBaseImport>(x =>
                x.ImportId.Equals(baseImport.ImportId)))).ReturnsAsync(baseImport);
            _baseDebtRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBaseDebt, bool>>>())).ReturnsAsync(baseDebt);
            _baseReturnRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsBaseProductReturn>(x =>
                x.ProductReturnId.Equals(Guid.Parse("bb2978c5-20f0-47fd-b50d-0e4b7fbb2089"))))).ReturnsAsync(baseReturn);


            //Act
            var actualResult = await _baseImportService.PayForBaseProductReturn(entity, UserData.Data.First().Username);

            //Assert
            Assert.False(actualResult);
            _baseReturnRepoMock.Verify(x => x.Update(It.IsAny<WmsBaseProductReturn>()), Times.Never);
            _baseDebtRepoMock.Verify(x => x.Update(It.IsAny<WmsBaseDebt>()), Times.Never);
            _baseInvoiceRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBaseInvoice>()), Times.Never);
            Assert.Equal(100000, baseReturn.PaidAmount);
            Assert.Equal(3, baseReturn.PaymentStatus);
            Assert.Equal(500000, baseDebt.DebtAmount);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("10c48c7c-48b0-4ea1-9e2c-95f6a219dc6a")]
        public async Task GetBaseImport_Returns_Null_When_BaseImport_NotFound(Guid importId)
        {
            // Arrange
            _unitOfWork.Setup(uow => uow.BaseImportRepository.GetBaseImportDetailsById(importId))
                          .ReturnsAsync((WmsBaseImport)null);

            // Act
            var result = await _baseImportService.GetBaseImportDetails(importId);

            // Assert
            Assert.Null(result);
        }

        [Theory]
        [InlineData("10c48c7c-48b0-4ea1-9e2c-95f6a219dc6b")]
        public async Task GetBaseImport_Returns_Null_When_BaseImport_Founded(Guid importId)
        {
            // Arrange
            _unitOfWork.Setup(uow => uow.BaseImportRepository.GetBaseImportDetailsById(importId))
                          .ReturnsAsync(new WmsBaseImport());

            // Act
            var result = await _baseImportService.GetBaseImportDetails(importId);

            // Assert
            Assert.NotNull(result);
        }


        [Fact]
        public async Task GetBaseImportList_ImportCodeExist_ReturnsExpectedResult()
        {
            // Arrange
            var requestDto = new GetBaseImportRequestDto
            {
                SearchWord = "IMPORT_CODE_1",
                ImportStatus = 1,
                PagingRequest = new PagingRequest
                {
                    CurrentPage = 1,
                    PageRange = 5,
                    PageSize = 10
                },
                PaymentStatus = 1,
                SupplierId = 1,
                DateFilter = 0
            };

            var baseImportList = new List<WmsBaseImport>() {
                new WmsBaseImport(Guid.NewGuid(), Guid.NewGuid(), "IMPORT_CODE_1", Guid.NewGuid(), Guid.NewGuid(), 1, DateTime.Now, DateTime.Now, 10000, 10000, 10000, 10000, "test", 1, 1, "test", DateTime.Now, "test", DateTime.Now),
                new WmsBaseImport(Guid.NewGuid(), Guid.NewGuid(), "IMPORT_CODE_2", Guid.NewGuid(), Guid.NewGuid(), 1, DateTime.Now, DateTime.Now, 10000, 10000, 10000, 10000, "test", 1, 1, "test", DateTime.Now, "test", DateTime.Now)
            };

            _dateService.Setup(ds => ds.GetDateTimeByDateFilter(It.IsAny<short>()))
                .Returns(new DateFilterRequestDto(DateTime.Now.AddDays(-7), DateTime.Now));

            var data = baseImportList.AsQueryable().BuildMock();
            _baseImportRepoMock.Setup(repo => repo.FilterSearchBaseImport(
                    requestDto.SearchWord,
                    requestDto.ImportStatus,
                    requestDto.PaymentStatus,
                    requestDto.SupplierId,
                    It.IsAny<DateTime>(),
                    It.IsAny<DateTime>()
                )).Returns(data);

            _baseImportRepoMock.Setup(repo => repo.Count())
                .ReturnsAsync(baseImportList.Count);

            _storageService.Setup(service => service.GetFileUrl(It.IsAny<S3RequestData>()))
                .Returns("mockedImageUrl");

            // Act
            var result = await _baseImportService.GetBaseImportList(requestDto);

            // Assert
            Assert.NotNull(result);
            Assert.NotEmpty(result.Items);
            Assert.IsType<ViewPaging<GetBaseImportResponseDto>>(result);
        }

        [Fact]
        public async Task GetBaseImportList_ImportCodeNotExist_ReturnsExpectedResult()
        {
            // Arrange
            var requestDto = new GetBaseImportRequestDto
            {
                SearchWord = "IMPORT_CODE_3000",
                ImportStatus = 1,
                PagingRequest = new PagingRequest
                {
                    CurrentPage = 1,
                    PageRange = 5,
                    PageSize = 10
                },
                PaymentStatus = 1,
                SupplierId = 1,
                DateFilter = 0
            };

            var baseImportList = new List<WmsBaseImport>() {
                new WmsBaseImport(Guid.NewGuid(), Guid.NewGuid(), "IMPORT_CODE_1", Guid.NewGuid(), Guid.NewGuid(), 1, DateTime.Now, DateTime.Now, 10000, 10000, 10000, 10000, "test", 1, 1, "test", DateTime.Now, "test", DateTime.Now),
                new WmsBaseImport(Guid.NewGuid(), Guid.NewGuid(), "IMPORT_CODE_2", Guid.NewGuid(), Guid.NewGuid(), 1, DateTime.Now, DateTime.Now, 10000, 10000, 10000, 10000, "test", 1, 1, "test", DateTime.Now, "test", DateTime.Now)
            };

            _dateService.Setup(ds => ds.GetDateTimeByDateFilter(It.IsAny<short>()))
                .Returns(new DateFilterRequestDto(DateTime.Now.AddDays(-7), DateTime.Now));

            var data = baseImportList.Where(x => x.ImportCode.Contains(requestDto.SearchWord)).AsQueryable().BuildMock();
            _baseImportRepoMock.Setup(repo => repo.FilterSearchBaseImport(
                    requestDto.SearchWord,
                    requestDto.ImportStatus,
                    requestDto.PaymentStatus,
                    requestDto.SupplierId,
                    It.IsAny<DateTime>(),
                    It.IsAny<DateTime>()
                )).Returns(data);

            _baseImportRepoMock.Setup(repo => repo.Count())
                .ReturnsAsync(baseImportList.Count);

            _storageService.Setup(service => service.GetFileUrl(It.IsAny<S3RequestData>()))
                .Returns("mockedImageUrl");

            // Act
            var result = await _baseImportService.GetBaseImportList(requestDto);

            // Assert
            Assert.NotNull(result);
            Assert.Empty(result.Items);
            Assert.IsType<ViewPaging<GetBaseImportResponseDto>>(result);
        }

        [Fact]
        public async Task GetBaseImportList_ImportStatusNotExist_ReturnsExpectedResult()
        {
            // Arrange
            var requestDto = new GetBaseImportRequestDto
            {
                SearchWord = "IMPORT_CODE_1",
                ImportStatus = 2,
                PagingRequest = new PagingRequest
                {
                    CurrentPage = 1,
                    PageRange = 5,
                    PageSize = 10
                },
                PaymentStatus = 1,
                SupplierId = 1,
                DateFilter = 0
            };

            var baseImportList = new List<WmsBaseImport>() {
                new WmsBaseImport(Guid.NewGuid(), Guid.NewGuid(), "IMPORT_CODE_1", Guid.NewGuid(), Guid.NewGuid(), 1, DateTime.Now, DateTime.Now, 10000, 10000, 10000, 10000, "test", 1, 1, "test", DateTime.Now, "test", DateTime.Now),
                new WmsBaseImport(Guid.NewGuid(), Guid.NewGuid(), "IMPORT_CODE_2", Guid.NewGuid(), Guid.NewGuid(), 1, DateTime.Now, DateTime.Now, 10000, 10000, 10000, 10000, "test", 1, 1, "test", DateTime.Now, "test", DateTime.Now)
            };

            _dateService.Setup(ds => ds.GetDateTimeByDateFilter(It.IsAny<short>()))
                .Returns(new DateFilterRequestDto(DateTime.Now.AddDays(-7), DateTime.Now));

            var data = baseImportList.Where(x => x.ImportStatus == requestDto.ImportStatus).AsQueryable().BuildMock();
            _baseImportRepoMock.Setup(repo => repo.FilterSearchBaseImport(
                    requestDto.SearchWord,
                    requestDto.ImportStatus,
                    requestDto.PaymentStatus,
                    requestDto.SupplierId,
                    It.IsAny<DateTime>(),
                    It.IsAny<DateTime>()
                )).Returns(data);

            _baseImportRepoMock.Setup(repo => repo.Count())
                .ReturnsAsync(baseImportList.Count);

            _storageService.Setup(service => service.GetFileUrl(It.IsAny<S3RequestData>()))
                .Returns("mockedImageUrl");

            // Act
            var result = await _baseImportService.GetBaseImportList(requestDto);

            // Assert
            Assert.NotNull(result);
            Assert.Empty(result.Items);
            Assert.IsType<ViewPaging<GetBaseImportResponseDto>>(result);
        }

        [Fact]
        public async Task GetBaseImportList_PaymentStatusNotExist_ReturnsExpectedResult()
        {
            // Arrange
            var requestDto = new GetBaseImportRequestDto
            {
                SearchWord = "IMPORT_CODE_1",
                ImportStatus = 1,
                PagingRequest = new PagingRequest
                {
                    CurrentPage = 1,
                    PageRange = 5,
                    PageSize = 10
                },
                PaymentStatus = 2,
                SupplierId = 1,
                DateFilter = 0
            };

            var baseImportList = new List<WmsBaseImport>() {
                new WmsBaseImport(Guid.NewGuid(), Guid.NewGuid(), "IMPORT_CODE_1", Guid.NewGuid(), Guid.NewGuid(), 1, DateTime.Now, DateTime.Now, 10000, 10000, 10000, 10000, "test", 1, 1, "test", DateTime.Now, "test", DateTime.Now),
                new WmsBaseImport(Guid.NewGuid(), Guid.NewGuid(), "IMPORT_CODE_2", Guid.NewGuid(), Guid.NewGuid(), 1, DateTime.Now, DateTime.Now, 10000, 10000, 10000, 10000, "test", 1, 1, "test", DateTime.Now, "test", DateTime.Now)
            };

            _dateService.Setup(ds => ds.GetDateTimeByDateFilter(It.IsAny<short>()))
                .Returns(new DateFilterRequestDto(DateTime.Now.AddDays(-7), DateTime.Now));

            var data = baseImportList.Where(x => x.PaymentStatus == requestDto.PaymentStatus).AsQueryable().BuildMock();
            _baseImportRepoMock.Setup(repo => repo.FilterSearchBaseImport(
                    requestDto.SearchWord,
                    requestDto.ImportStatus,
                    requestDto.PaymentStatus,
                    requestDto.SupplierId,
                    It.IsAny<DateTime>(),
                    It.IsAny<DateTime>()
                )).Returns(data);

            _baseImportRepoMock.Setup(repo => repo.Count())
                .ReturnsAsync(baseImportList.Count);

            _storageService.Setup(service => service.GetFileUrl(It.IsAny<S3RequestData>()))
                .Returns("mockedImageUrl");

            // Act
            var result = await _baseImportService.GetBaseImportList(requestDto);

            // Assert
            Assert.NotNull(result);
            Assert.Empty(result.Items);
            Assert.IsType<ViewPaging<GetBaseImportResponseDto>>(result);
        }
    }
}
