﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using MockQueryable.Moq;
using Moq;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using WMS.Business.Abstractions.Service;
using WMS.Business.Dtos.BaseExportDto;
using WMS.Business.Dtos.BranchExportDto;
using WMS.Business.Dtos.CommonDto;
using WMS.Business.Dtos.FilterDto;
using WMS.Business.Dtos.Mapper;
using WMS.Business.Dtos.S3Dto;
using WMS.Business.Services;
using WMS.Domain.Abstractions;
using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Entities;
using WMS.Domain.Enums;
namespace WMS.UnitTest.Business
{
    public class BranchExportServiceTest
    {
        private readonly BranchExportService _branchExportService;
        private readonly Mock<IUnitOfWork> _unitOfWork = new Mock<IUnitOfWork>();
        private readonly Mock<IWmsUserRepository> _userRepoMock = new Mock<IWmsUserRepository>();
        private readonly Mock<IWmsBranchWarehouseRepository> _branchWarehouseRepoMock = new Mock<IWmsBranchWarehouseRepository>();
        private readonly Mock<IWmsBaseWarehouseRepository> _baseWarehouseRepoMock = new Mock<IWmsBaseWarehouseRepository>();
        private readonly Mock<IWmsBranchExportRepository> _branchExportRepoMock = new Mock<IWmsBranchExportRepository>();
        private readonly Mock<IWmsBranchExportProductRepository> _branchExportProductRepoMock = new Mock<IWmsBranchExportProductRepository>();
        private readonly Mock<IWmsSupplierRepository> _supplierRepoMock = new Mock<IWmsSupplierRepository>();
        private readonly Mock<IWmsProductRepository> _productRepoMock = new Mock<IWmsProductRepository>();
        private readonly Mock<IWmsBranchDebtRepository> _branchDebtRepoMock = new Mock<IWmsBranchDebtRepository>();
        private readonly Mock<IWmsBranchInvoiceRepository> _branchInvoiceRepoMock = new Mock<IWmsBranchInvoiceRepository>();
        private readonly Mock<IWmsBaseInvoiceRepository> _baseInvoiceRepoMock = new Mock<IWmsBaseInvoiceRepository>();
        private readonly Mock<IWmsBranchProductReturnRepository> _branchReturnRepoMock = new Mock<IWmsBranchProductReturnRepository>();
        private readonly Mock<IWmsBranchImportRepository> _branchImportRepoMock = new Mock<IWmsBranchImportRepository>();
        private readonly Mock<IWmsBranchImportProductRepository> _branchImportProductRepoMock = new Mock<IWmsBranchImportProductRepository>();
        private readonly Mock<IWmsBranchProductReturnDetailRepository> _branchProductReturnDetailRepoMock = new Mock<IWmsBranchProductReturnDetailRepository>();
        private readonly Mock<IWmsBranchProductReturnRepository> _branchProductReturnRepoMock = new Mock<IWmsBranchProductReturnRepository>();
        private readonly Mock<IWmsBaseImportProductRepository> _baseImportProductRepoMock = new Mock<IWmsBaseImportProductRepository>();
        private readonly Mock<IWmsBranchRequestRepository> _branchRequestRepoMock = new Mock<IWmsBranchRequestRepository>();


        private readonly IMock<ILogger<BranchExportService>> _logger = new Mock<ILogger<BranchExportService>>();
        private readonly Mock<IS3StorageService> _storageService = new Mock<IS3StorageService>();
        private readonly Mock<IDateService> _dateService = new Mock<IDateService>();
        private readonly Mock<IProductService> _productService = new Mock<IProductService>();
        private readonly Mock<IInvoiceService> _invoiceServiceMock = new Mock<IInvoiceService>();
        private readonly Mock<IWarehouseService> _warehouseService = new Mock<IWarehouseService>();


        public BranchExportServiceTest()
        {
            var mockTran = new Mock<IDbTransaction>();

            _unitOfWork.Setup(x => x.BeginTransaction()).Returns(mockTran.Object);
            _unitOfWork.Setup(x => x.UserRepository).Returns(_userRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchWarehouseRepository).Returns(_branchWarehouseRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchWarehouseRepository).Returns(_branchWarehouseRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchExportRepository).Returns(_branchExportRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchExportProductRepository).Returns(_branchExportProductRepoMock.Object);
            _unitOfWork.Setup(x => x.SupplierRepository).Returns(_supplierRepoMock.Object);
            _unitOfWork.Setup(x => x.ProductRepository).Returns(_productRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchDebtRepository).Returns(_branchDebtRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchInvoiceRepository).Returns(_branchInvoiceRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchInvoiceRepository).Returns(_branchInvoiceRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchProductReturnRepository).Returns(_branchReturnRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchImportRepository).Returns(_branchImportRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchProductReturnDetailRepository).Returns(_branchProductReturnDetailRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchProductReturnRepository).Returns(_branchProductReturnRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchImportProductRepository).Returns(_branchImportProductRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchRequestRepository).Returns(_branchRequestRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchImportProductRepository).Returns(_branchImportProductRepoMock.Object);

            var configuration = new MapperConfiguration(config =>
            {
                config.AddProfile<DomainToDtoProfile>();
                config.AddProfile<DtoToDomainProfile>();
            });
            var mapper = new Mapper(configuration);

            _branchExportService = new BranchExportService(_unitOfWork.Object, _logger.Object, mapper,
               _dateService.Object, _storageService.Object, _warehouseService.Object, _invoiceServiceMock.Object);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("10c48c7c-48b0-4ea1-9e2c-95f6a219dc6a")]
        public async Task GetBaseImport_Returns_Null_When_BaseImport_NotFound(Guid exportId)
        {
            // Arrange
            _unitOfWork.Setup(uow => uow.BranchExportRepository.GetBranchExportDetailsById(exportId))
                          .ReturnsAsync((WmsBranchExport)null);

            // Act
            var result = await _branchExportService.GetExportsDetails(exportId);

            // Assert
            Assert.Null(result);
        }

        [Theory]
        [InlineData("10c48c7c-48b0-4ea1-9e2c-95f6a219dc6b")]
        public async Task GetBaseImport_Returns_Null_When_BaseImport_Founded(Guid exportId)
        {
            // Arrange
            _unitOfWork.Setup(uow => uow.BranchExportRepository.GetBranchExportDetailsById(exportId))
                          .ReturnsAsync(new WmsBranchExport());

            // Act
            var result = await _branchExportService.GetExportsDetails(exportId);

            // Assert
            Assert.NotNull(result);
        }

        [Fact]
        public async Task GetExportsList_Returns_Valid_Result()
        {
            // Arrange
            var warehouseId = Guid.Parse("10c48c7c-48b0-4ea1-9e2c-95f6a219dc6a");
            var requestDto = new GetBranchExportRequestDto
            {
                SearchWord = "EXPORT_CODE_1",
                PagingRequest = new PagingRequest
                {
                    CurrentPage = 1,
                    PageRange = 5,
                    PageSize = 10
                },
                DateFilter = 0
            };

            var currentUser = "testUser";

            var dateFilter = new DateFilterRequestDto(DateTime.Now.AddDays(-7), DateTime.Now);

            var fullList = new List<WmsBranchExport>() {
                new WmsBranchExport
                {
                    ExportCode = "EXPORT_CODE_1",
                    WarehouseId = warehouseId
                },
                new WmsBranchExport
                {
                    ExportCode = "EXPORT_CODE_2",
                    WarehouseId = Guid.NewGuid()
                },
            };

            var expectedBranchExport = fullList.Where(x => x.ExportCode.Contains(requestDto.SearchWord)).AsQueryable().BuildMock();
            _dateService.Setup(ds => ds.GetDateTimeByDateFilter(It.IsAny<short>()))
                .Returns(dateFilter);

            _warehouseService.Setup(ws => ws.GetWarehouseIdByUsername(currentUser))
                .ReturnsAsync(warehouseId);

            _unitOfWork.Setup(uow => uow.BranchExportRepository.FilterSearchBranchExport(
                    It.IsAny<string>(),
                    warehouseId,
                    dateFilter.StartDate,
                    dateFilter.EndDate))
                .ReturnsAsync(expectedBranchExport);

            _storageService.Setup(ss => ss.GetFileUrl(It.IsAny<S3RequestData>()))
                .Returns("http://example.com/image.jpg");

            // Act
            var result = await _branchExportService.GetExportsList(requestDto, currentUser);

            // Assert
            Assert.NotNull(result);
            Assert.NotEmpty(result.Items);
            Assert.IsType<ViewPaging<GetBranchExportResponseDto>>(result);
        }

        [Fact]
        public async Task GetExportsList_ExportCodeNotExist_Returns_NotValid_Result()
        {
            // Arrange
            var warehouseId = Guid.Parse("10c48c7c-48b0-4ea1-9e2c-95f6a219dc6a");
            var requestDto = new GetBranchExportRequestDto
            {
                SearchWord = "EXPORT_CODE_15",
                PagingRequest = new PagingRequest
                {
                    CurrentPage = 1,
                    PageRange = 5,
                    PageSize = 10
                },
                DateFilter = 0
            };

            var currentUser = "testUser";

            var dateFilter = new DateFilterRequestDto(DateTime.Now.AddDays(-7), DateTime.Now);

            var fullList = new List<WmsBranchExport>() {
                new WmsBranchExport
                {
                    ExportCode = "EXPORT_CODE_1",
                    WarehouseId = warehouseId
                },
                new WmsBranchExport
                {
                    ExportCode = "EXPORT_CODE_2",
                    WarehouseId = Guid.NewGuid()
                },
            };

            var expectedBranchExport = fullList.Where(x => x.ExportCode.Contains(requestDto.SearchWord)).AsQueryable().BuildMock();
            _dateService.Setup(ds => ds.GetDateTimeByDateFilter(It.IsAny<short>()))
                .Returns(dateFilter);

            _warehouseService.Setup(ws => ws.GetWarehouseIdByUsername(currentUser))
                .ReturnsAsync(warehouseId);

            _unitOfWork.Setup(uow => uow.BranchExportRepository.FilterSearchBranchExport(
                    It.IsAny<string>(),
                    warehouseId,
                    dateFilter.StartDate,
                    dateFilter.EndDate))
                .ReturnsAsync(expectedBranchExport);

            _storageService.Setup(ss => ss.GetFileUrl(It.IsAny<S3RequestData>()))
                .Returns("http://example.com/image.jpg");

            // Act
            var result = await _branchExportService.GetExportsList(requestDto, currentUser);

            // Assert
            Assert.NotNull(result);
            Assert.Empty(result.Items);
            Assert.IsType<ViewPaging<GetBranchExportResponseDto>>(result);
        }

        //[Fact]
        //public async Task CreateBranchExport_WhenValidRequest_ReturnsExportId()
        //{
        //    var productId_1 = Guid.NewGuid();
        //    var productId_2 = Guid.NewGuid();
        //    var warehouseId = Guid.NewGuid();

        //    // Arrange
        //    var requestDto = new CreateBranchExportRequestDto
        //    {
        //        ExportDate = DateTime.Today.AddDays(1),
        //        ExportBy = Guid.NewGuid(),
        //        BranchExportProducts = new List<CreateBranchExportProductDto>
        //        {
        //            new CreateBranchExportProductDto
        //            {
        //                ProductId = Guid.NewGuid(),
        //                Quantity = 1,
        //            },
        //            new CreateBranchExportProductDto
        //            {
        //                ProductId = Guid.NewGuid(),
        //                Quantity = 1,
        //            }
        //        }
        //    };

        //    // Mock GetWarehouseIdByUsername method
        //    _warehouseService.Setup(x => x.GetWarehouseIdByUsername(It.IsAny<string>()))
        //        .ReturnsAsync(warehouseId);

        //    // Mock FindObject method in UserRepository
        //    _unitOfWork.Setup(x => x.UserRepository.FindObject(It.IsAny<Expression<Func<WmsUser, bool>>>()))
        //        .ReturnsAsync(new WmsUser { UserId = Guid.NewGuid(), Username = "testuser", Status = (short)UserStatusEnum.Active });

        //    // Mock FindObject and GetBranchStockOrderByImportDate methods in repositories
        //    _unitOfWork.Setup(x => x.BranchWarehouseProductRepository.FindObject(It.IsAny<Expression<Func<WmsBranchWarehouseProduct, bool>>>()))
        //        .ReturnsAsync(new WmsBranchWarehouseProduct {
        //                BpId = Guid.NewGuid(),
        //                ProductId = productId_1,
        //                CostPrice = 1000,
        //                ExportPrice = 2000,
        //                WarehouseId = warehouseId,
        //                IsDelete = false
        //        });

        //    var branchImportProducts = new List<WmsBranchImportProduct> {
        //            new WmsBranchImportProduct
        //            {
        //                BaseImportId = Guid.NewGuid(),
        //                ProductId = productId_1,
        //                ExportCodeItem = "EXPORT_ITEM_1",
        //                StockQuantity = 10,
        //            },
        //            new WmsBranchImportProduct
        //            {
        //                BaseImportId = Guid.NewGuid(),
        //                ProductId = productId_2,
        //                ExportCodeItem = "EXPORT_ITEM_2",
        //                StockQuantity = 10,
        //            }
        //        };

        //    _unitOfWork.Setup(x => x.BranchImportProductRepository.FindMultiple(It.IsAny<Expression<Func<WmsBranchImportProduct, bool>>>()))
        //        .ReturnsAsync(branchImportProducts);

        //    // Mock FindObject and AddAsync methods in BranchExportProductRepository
        //    _unitOfWork.Setup(x => x.BranchExportProductRepository.FindObject(It.IsAny<Expression<Func<WmsBranchExportProduct, bool>>>()))
        //        .ReturnsAsync((WmsBranchExportProduct)null); // Return null for product not found

        //    _unitOfWork.Setup(x => x.BranchExportProductRepository.AddAsync(It.IsAny<WmsBranchExportProduct>()))
        //        .Returns(Task.CompletedTask);

        //    _unitOfWork.Setup(x => x.BranchImportProductRepository.GetBranchStockOrderByImportDate(It.IsAny<Guid>(), It.IsAny<Guid>()))
        //        .Returns(branchImportProducts.Where(x => x.ProductId == productId_1).AsQueryable().BuildMock());

        //    // Mock FindObject and AddAsync methods in BranchInvoiceRepository
        //    _unitOfWork.Setup(x => x.BranchWarehouseRepository.FindObject(It.IsAny<Expression<Func<WmsBranchWarehouse, bool>>>()))
        //        .ReturnsAsync(new WmsBranchWarehouse { 
        //            WarehouseId = warehouseId,
        //        });

        //    _unitOfWork.Setup(x => x.BranchInvoiceRepository.AddAsync(It.IsAny<WmsBranchInvoice>()))
        //        .Returns(Task.CompletedTask);

        //    // Act
        //    var result = await _branchExportService.CreateBranchExport(requestDto, "testuser");

        //    // Assert
        //    Assert.NotEqual(Guid.Empty, result);
        //    _unitOfWork.Verify(uow => uow.CommitAsync(), Times.Exactly(3));
        //}
    }
}
