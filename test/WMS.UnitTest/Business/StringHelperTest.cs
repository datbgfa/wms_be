﻿using WMS.Business.Utils.StringUtils;

namespace WMS.UnitTest.Business
{
    public class StringHelperTest
    {
        [Theory]
        [InlineData("duongphhe151097@fpt.edu.vn")]
        [InlineData("duongph16@gmail.com")]
        public void ValidEmail_ReturnTrue(string email)
        {
            var valid = StringHelper.IsValidEmail(email);

            Assert.True(valid);
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        [InlineData("abc")]
        [InlineData("duongphhe151097@")]
        public void InvalidEmail_ReturnFalse(string email)
        {
            var invalid = StringHelper.IsValidEmail(email);

            Assert.False(invalid);
        }

        [Theory]
        [InlineData("root")]
        [InlineData("duongph")]
        [InlineData("Admin")]
        [InlineData("Admin01")]
        public void ValidUserName_ReturnTrue(string username)
        {
            var valid = StringHelper.IsValidUsername(username);

            Assert.True(valid);
        }

        [Theory]
        [InlineData("Duongph@123")]
        [InlineData("Duongph*123")]
        [InlineData("Duongph&123")]
        public void InvalidUserName_ReturnFalse(string username)
        {
            var invalid = StringHelper.IsValidUsername(username);

            Assert.False(invalid);
        }

        [Theory]
        [InlineData(8, false, false, false)]
        [InlineData(12, true, false, false)]
        [InlineData(16, true, true, false)]
        [InlineData(20, true, true, true)]
        public void RandomStringGenerate_GeneratesRandomStringWithSpecifiedLengthAndOptions(int length, bool hasNumber, bool hasUppercaseChar, bool hasSpecialChar)
        {
            // Act
            var generatedString = StringHelper.RandomStringGenerate(length, hasNumber, hasUppercaseChar, hasSpecialChar);

            // Assert
            Assert.Equal(length, generatedString.Length);

            if (hasNumber)
            {
                Assert.Contains(generatedString, c => char.IsDigit(c));
            }
            else
            {
                Assert.DoesNotContain(generatedString, c => char.IsDigit(c));
            }

            if (hasUppercaseChar)
            {
                Assert.Contains(generatedString, c => char.IsUpper(c));
            }
            else
            {
                Assert.DoesNotContain(generatedString, c => char.IsUpper(c));
            }

            if (hasSpecialChar)
            {
                Assert.Contains(generatedString, c => @"!#$%&'()*+,-./:;<=>?@[\]_".Contains(c));
            }
            else
            {
                Assert.DoesNotContain(generatedString, c => @"!#$%&'()*+,-./:;<=>?@[\]_".Contains(c));
            }
        }

        [Theory]
        [InlineData(12, 4, 4, 2, 2)]
        [InlineData(16, 5, 5, 3, 3)]
        [InlineData(20, 6, 6, 4, 4)]
        public void GeneratePassword_GeneratesPasswordWithSpecifiedLengthAndCharacterCounts(int length, int lowerChar, int upperchar, int numeric, int special)
        {
            const string LowercaseChars = "abcdefghijklmnopqrstuvwxyz";
            const string UppercaseChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            const string NumericChars = "0123456789";
            const string SpecialChars = "!@#$%^&*()-_+=<>?";
            // Act
            var generatedPassword = StringHelper.GeneratePassword(length, lowerChar, upperchar, numeric, special);

            // Assert
            Assert.Equal(length, generatedPassword.Length);

            int lowerCount = CountCharacters(generatedPassword, LowercaseChars);
            int upperCount = CountCharacters(generatedPassword, UppercaseChars);
            int numericCount = CountCharacters(generatedPassword, NumericChars);
            int specialCount = CountCharacters(generatedPassword, SpecialChars);

            Assert.Equal(lowerChar, lowerCount);
            Assert.Equal(upperchar, upperCount);
            Assert.Equal(numeric, numericCount);
            Assert.Equal(special, specialCount);

            // Ensure that the password contains characters only from the specified character sets.
            Assert.Equal(length, lowerCount + upperCount + numericCount + specialCount);
        }

        private int CountCharacters(string str, string charsToCount)
        {
            return str.Count(c => charsToCount.Contains(c));
        }
    }
}
