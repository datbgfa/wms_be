﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using MockQueryable.Moq;
using Moq;
using MoqExpression;
using System.Data;
using System.Linq.Expressions;
using WMS.Business.Abstractions.Service;
using WMS.Business.Dtos.BranchImportDto;
using WMS.Business.Dtos.BranchWarehouseDto;
using WMS.Business.Dtos.CommonDto;
using WMS.Business.Dtos.FilterDto;
using WMS.Business.Dtos.InvoiceDto;
using WMS.Business.Dtos.Mapper;
using WMS.Business.Dtos.ProductDto;
using WMS.Business.Dtos.S3Dto;
using WMS.Business.Services;
using WMS.Domain.Abstractions;
using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Entities;
using WMS.Domain.Enums;
using WMS.UnitTest.DummiesData;

namespace WMS.UnitTest.Business
{
    public class BranchImportServiceTest
    {
        private readonly BranchImportService _branchImportService;
        private readonly Mock<IUnitOfWork> _unitOfWork = new Mock<IUnitOfWork>();
        private readonly Mock<IWmsUserRepository> _userRepoMock = new Mock<IWmsUserRepository>();
        private readonly Mock<IWmsBaseWarehouseRepository> _baseWarehouseRepoMock = new Mock<IWmsBaseWarehouseRepository>();
        private readonly Mock<IWmsBranchWarehouseRepository> _branchWarehouseRepoMock = new Mock<IWmsBranchWarehouseRepository>();
        private readonly Mock<IWmsProductRepository> _productRepoMock = new Mock<IWmsProductRepository>();
        private readonly Mock<IWmsBranchDebtRepository> _branchDebtRepoMock = new Mock<IWmsBranchDebtRepository>();
        private readonly Mock<IWmsBaseInvoiceRepository> _baseInvoiceRepoMock = new Mock<IWmsBaseInvoiceRepository>();
        private readonly Mock<IWmsBranchInvoiceRepository> _branchInvoiceRepoMock = new Mock<IWmsBranchInvoiceRepository>();
        private readonly Mock<IWmsBranchProductReturnRepository> _branchReturnRepoMock = new Mock<IWmsBranchProductReturnRepository>();
        private readonly Mock<IWmsBranchImportRepository> _branchImportRepoMock = new Mock<IWmsBranchImportRepository>();
        private readonly Mock<IWmsBranchImportProductRepository> _branchImportProductRepoMock = new Mock<IWmsBranchImportProductRepository>();
        private readonly Mock<IWmsBranchRequestRepository> _branchRequestRepoMock = new Mock<IWmsBranchRequestRepository>();
        private readonly Mock<IWmsBranchProductRequestRepository> _branchRequestProductRepoMock = new Mock<IWmsBranchProductRequestRepository>();
        private readonly Mock<IWmsBaseImportRepository> _baseImportRepoMock = new Mock<IWmsBaseImportRepository>();
        private readonly Mock<IWmsBaseExportRepository> _baseExportRepoMock = new Mock<IWmsBaseExportRepository>();
        private readonly Mock<IWmsBaseExportProductRepository> _baseExportProductRepoMock = new Mock<IWmsBaseExportProductRepository>();
        private readonly Mock<IWmsBranchProductReturnDetailRepository> _branchReturnDetailRepoMock = new Mock<IWmsBranchProductReturnDetailRepository>();
        private readonly Mock<IWmsBaseImportProductRepository> _baseImportProductRepoMock = new Mock<IWmsBaseImportProductRepository>();
        private readonly Mock<IWmsBranchWarehouseProductRepository> _branchWarehouseProductRepoMock = new Mock<IWmsBranchWarehouseProductRepository>();
        private readonly Mock<IWmsBranchWarehouseUserRepository> _branchWarehouseUserRepoMock = new Mock<IWmsBranchWarehouseUserRepository>();


        private readonly IMock<ILogger<BranchImportService>> _logger = new Mock<ILogger<BranchImportService>>();
        private readonly IMock<ILogger<BranchRequestService>> _loggerBR = new Mock<ILogger<BranchRequestService>>();
        private readonly Mock<IS3StorageService> _storageService = new Mock<IS3StorageService>();
        private readonly Mock<IDateService> _dateService = new Mock<IDateService>();
        private readonly Mock<IProductService> _productService = new Mock<IProductService>();
        private readonly Mock<IWarehouseService> _warehouseService = new Mock<IWarehouseService>();
        private readonly Mock<IInvoiceService> _invoiceService = new Mock<IInvoiceService>();
        private readonly BranchRequestService _branchRequestService;

        public BranchImportServiceTest()
        {
            var mockTran = new Mock<IDbTransaction>();

            _unitOfWork.Setup(x => x.BeginTransaction()).Returns(mockTran.Object);
            _unitOfWork.Setup(x => x.UserRepository).Returns(_userRepoMock.Object);
            _unitOfWork.Setup(x => x.BaseWarehouseRepository).Returns(_baseWarehouseRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchWarehouseRepository).Returns(_branchWarehouseRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchImportRepository).Returns(_branchImportRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchImportProductRepository).Returns(_branchImportProductRepoMock.Object);
            _unitOfWork.Setup(x => x.ProductRepository).Returns(_productRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchDebtRepository).Returns(_branchDebtRepoMock.Object);
            _unitOfWork.Setup(x => x.BaseInvoiceRepository).Returns(_baseInvoiceRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchInvoiceRepository).Returns(_branchInvoiceRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchProductReturnRepository).Returns(_branchReturnRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchRequestRepository).Returns(_branchRequestRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchProductRequestRepository).Returns(_branchRequestProductRepoMock.Object);
            _unitOfWork.Setup(x => x.BaseImportRepository).Returns(_baseImportRepoMock.Object);
            _unitOfWork.Setup(x => x.BaseExportRepository).Returns(_baseExportRepoMock.Object);
            _unitOfWork.Setup(x => x.BaseExportProductRepository).Returns(_baseExportProductRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchProductReturnDetailRepository).Returns(_branchReturnDetailRepoMock.Object);
            _unitOfWork.Setup(x => x.BaseImportProductRepository).Returns(_baseImportProductRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchWarehouseProductRepository).Returns(_branchWarehouseProductRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchWarehouseUserRepository).Returns(_branchWarehouseUserRepoMock.Object);


            var configuration = new MapperConfiguration(config =>
            {
                config.AddProfile<DomainToDtoProfile>();
                config.AddProfile<DtoToDomainProfile>();
            });
            var mapper = new Mapper(configuration);

            _branchImportService = new BranchImportService(_unitOfWork.Object, _logger.Object, mapper,
               _storageService.Object, _dateService.Object, _warehouseService.Object, _productService.Object, _invoiceService.Object);

            _branchRequestService = new BranchRequestService(_unitOfWork.Object, _loggerBR.Object, mapper, _dateService.Object, _storageService.Object, _warehouseService.Object);
        }

        [Theory]
        [InlineData(90000, 2, true)]
        [InlineData(100000, 3, true)]
        [InlineData(999999, 1, false)]
        [InlineData(0, 1, false)]
        public async Task PayForBranchReturn(decimal payAmount, short status, bool result)
        {
            //Arrange
            var branchImport = new WmsBranchImport()
            {
                WarehouseId = Guid.Parse("ab0d7db2-e71a-4dda-a0a1-5d28f5003536"),
            };
            var branchDebt = new WmsBranchDebt()
            {
                DebtAmount = 500000,
            };
            var branchReturn = BranchReturnData.Data.First();


            var entity = new PayForBranchProductReturnRequestDto() { BranchProductReturnId = branchReturn.ProductReturnId, PayAmount = payAmount };

            _userRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsUser, bool>>>()))
                .ReturnsAsync(UserData.Data.First());
            _branchImportRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchImport, bool>>>())).ReturnsAsync(branchImport);
            _branchDebtRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchDebt, bool>>>())).ReturnsAsync(branchDebt);
            _branchReturnRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchProductReturn, bool>>>())).ReturnsAsync(branchReturn);
            _branchWarehouseRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchWarehouse, bool>>>())).ReturnsAsync(new WmsBranchWarehouse()
            {
                WarehouseName = "Kho HN1",
            });


            //Act
            var actualResult = await _branchImportService.PayForBranchProductReturn(entity, UserData.Data.First().Username);

            //Assert
            Assert.Equal(result, actualResult);
            if (result)
            {
                _branchReturnRepoMock.Verify(x => x.Update(It.IsAny<WmsBranchProductReturn>()), Times.Once);
                _branchDebtRepoMock.Verify(x => x.Update(It.IsAny<WmsBranchDebt>()), Times.Once);
                _baseInvoiceRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBaseInvoice>()), Times.Once);
                _branchInvoiceRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBranchInvoice>()), Times.Once);
                Assert.Equal(payAmount, branchReturn.PaidAmount);
                Assert.Equal(status, branchReturn.PaymentStatus);
                Assert.Equal(500000 + payAmount, branchDebt.DebtAmount);
            }
            else
            {
                _branchReturnRepoMock.Verify(x => x.Update(It.IsAny<WmsBranchProductReturn>()), Times.Never);
                _branchDebtRepoMock.Verify(x => x.Update(It.IsAny<WmsBranchDebt>()), Times.Never);
                _baseInvoiceRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBaseInvoice>()), Times.Never);
                _branchInvoiceRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBranchInvoice>()), Times.Never);
                Assert.Equal(0, branchReturn.PaidAmount);
                Assert.Equal(status, branchReturn.PaymentStatus);
                Assert.Equal(500000, branchDebt.DebtAmount);
            }
        }

        [Fact]
        public async Task PayForBranchReturn_ReturnIdNotExist()
        {
            //Arrange
            var branchImport = new WmsBranchImport()
            {
                WarehouseId = Guid.Parse("ab0d7db2-e71a-4dda-a0a1-5d28f5003536"),
            };
            var branchDebt = new WmsBranchDebt()
            {
                DebtAmount = 500000,
            };
            var branchReturn = BranchReturnData.Data.First();


            var entity = new PayForBranchProductReturnRequestDto() { BranchProductReturnId = Guid.Empty, PayAmount = 100000 };

            _userRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsUser, bool>>>()))
                .ReturnsAsync(UserData.Data.First());
            _branchImportRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchImport, bool>>>())).ReturnsAsync(branchImport);
            _branchDebtRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchDebt, bool>>>())).ReturnsAsync(branchDebt);
            _branchReturnRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsBranchProductReturn>(x =>
                x.ProductReturnId.Equals(Guid.Parse("bb2978c5-20f0-47fd-b50d-0e4b7fbb2089"))))).ReturnsAsync(branchReturn);
            _branchWarehouseRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchWarehouse, bool>>>())).ReturnsAsync(new WmsBranchWarehouse()
            {
                WarehouseName = "Kho HN1",
            });


            //Act
            var actualResult = await _branchImportService.PayForBranchProductReturn(entity, UserData.Data.First().Username);

            //Assert
            Assert.False(actualResult);
            _branchReturnRepoMock.Verify(x => x.Update(It.IsAny<WmsBranchProductReturn>()), Times.Never);
            _branchDebtRepoMock.Verify(x => x.Update(It.IsAny<WmsBranchDebt>()), Times.Never);
            _baseInvoiceRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBaseInvoice>()), Times.Never);
            _branchInvoiceRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBranchInvoice>()), Times.Never);
            Assert.Equal(0, branchReturn.PaidAmount);
            Assert.Equal(1, branchReturn.PaymentStatus);
            Assert.Equal(500000, branchDebt.DebtAmount);
        }

        [Fact]
        public async Task PayForBranchReturn_ReturnIsPaid()
        {
            //Arrange
            var branchImport = new WmsBranchImport()
            {
                WarehouseId = Guid.Parse("ab0d7db2-e71a-4dda-a0a1-5d28f5003536"),
            };
            var branchDebt = new WmsBranchDebt()
            {
                DebtAmount = 500000,
            };
            var branchReturn = BranchReturnData.Data.First();
            branchReturn.PaymentStatus = 3;
            branchReturn.PaidAmount = 100000;


            var entity = new PayForBranchProductReturnRequestDto() { BranchProductReturnId = Guid.Parse("bb2978c5-20f0-47fd-b50d-0e4b7fbb2089"), PayAmount = 100000 };

            _userRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsUser, bool>>>()))
                .ReturnsAsync(UserData.Data.First());
            _branchImportRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchImport, bool>>>())).ReturnsAsync(branchImport);
            _branchDebtRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchDebt, bool>>>())).ReturnsAsync(branchDebt);
            _branchReturnRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsBranchProductReturn>(x =>
                x.ProductReturnId.Equals(Guid.Parse("bb2978c5-20f0-47fd-b50d-0e4b7fbb2089"))))).ReturnsAsync(branchReturn);
            _branchWarehouseRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchWarehouse, bool>>>())).ReturnsAsync(new WmsBranchWarehouse()
            {
                WarehouseName = "Kho HN1",
            });


            //Act
            var actualResult = await _branchImportService.PayForBranchProductReturn(entity, UserData.Data.First().Username);

            //Assert
            Assert.False(actualResult);
            _branchReturnRepoMock.Verify(x => x.Update(It.IsAny<WmsBranchProductReturn>()), Times.Never);
            _branchDebtRepoMock.Verify(x => x.Update(It.IsAny<WmsBranchDebt>()), Times.Never);
            _baseInvoiceRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBaseInvoice>()), Times.Never);
            _branchInvoiceRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBranchInvoice>()), Times.Never);
            Assert.Equal(100000, branchReturn.PaidAmount);
            Assert.Equal(3, branchReturn.PaymentStatus);
            Assert.Equal(500000, branchDebt.DebtAmount);
        }

        [Theory]
        [MemberData(nameof(CreateBranchRequestData))]
        public async Task CreateBranchRequest(CreateBranchImportRequestRequestDto entity, bool result)
        {
            //Arrange
            _warehouseService.Setup(x => x.GetWarehouseIdByUsername(UserData.Data.First().Username)).ReturnsAsync(Guid.NewGuid());
            _userRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsUser>(f => f.Username.Equals("nprettejohns0") && f.Status == (short)UserStatusEnum.Active)))
               .ReturnsAsync(UserData.Data.First());
            _productRepoMock.Setup(x => x.GetById(Guid.Parse("b08d504e-8e0b-47b9-a51c-50adf96a2338"))).ReturnsAsync(new WmsProduct() { ExportPrice = 10000 });


            //Act
            var actualResult = await _branchImportService.CreateBranchImportRequest(entity, UserData.Data.First().Username).ConfigureAwait(false);

            //Assert

            if (result)
            {
                Assert.NotNull(actualResult);
                _branchRequestRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBranchRequest>()), Times.Once);
                _branchRequestProductRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBranchProductRequest>()), Times.Exactly(entity.ProductRequests.Count()));
            }
            else
            {
                Assert.Null(actualResult);
                _branchRequestRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBranchRequest>()), Times.Never);
                _branchRequestProductRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBranchProductRequest>()), Times.Never);
            }

        }
        public static IEnumerable<object[]> CreateBranchRequestData()
        {
            yield return new object[]   //normal case
            {
                new CreateBranchImportRequestRequestDto()
                {
                    RequestDate = new DateTime(DateTime.Now.AddDays(10).Ticks, DateTimeKind.Unspecified),
                    ProductRequests = new List<CreateBranchImportRequestProductRequestDto>()
                    {
                        new CreateBranchImportRequestProductRequestDto()
                        {
                            ProductId = Guid.Parse("b08d504e-8e0b-47b9-a51c-50adf96a2338"),
                            RequestQuantity = 10,
                        }
                    }
                }, true
            };
            yield return new object[]   //invalid date - fail
            {
                new CreateBranchImportRequestRequestDto()
                {
                    RequestDate = new DateTime(DateTime.Now.AddDays(-10).Ticks, DateTimeKind.Unspecified),
                    ProductRequests = new List<CreateBranchImportRequestProductRequestDto>()
                    {
                        new CreateBranchImportRequestProductRequestDto()
                        {
                            ProductId = Guid.Parse("b08d504e-8e0b-47b9-a51c-50adf96a2338"),
                            RequestQuantity = 10,
                        }
                    }
                }, false
            };
            yield return new object[]   //product id
            {
                new CreateBranchImportRequestRequestDto()
                {
                    RequestDate = new DateTime(DateTime.Now.AddDays(10).Ticks, DateTimeKind.Unspecified),
                    ProductRequests = new List<CreateBranchImportRequestProductRequestDto>()
                    {
                        new CreateBranchImportRequestProductRequestDto()
                        {
                            ProductId = Guid.Empty,
                            RequestQuantity = 10,
                        }
                    }
                }, false
            };
            yield return new object[]   //list empty - fail
            {
                new CreateBranchImportRequestRequestDto()
                {
                    RequestDate = new DateTime(DateTime.Now.AddDays(10).Ticks, DateTimeKind.Unspecified),
                    ProductRequests = new List<CreateBranchImportRequestProductRequestDto>()
                    {
                    }
                }, false
            };
        }

        [Theory]
        [MemberData(nameof(UpdateBranchRequestData))]
        public async Task UpdateBranchRequest(UpdateBranchImportRequestRequestDto entity, bool result)
        {
            //Arrange
            var branchRequest = new WmsBranchRequest()
            {
                RequestId = Guid.Parse("a23d504e-8e0b-47b9-a51c-50adf96a2339"),
                WmsBranchProductRequests = new List<WmsBranchProductRequest>() { new WmsBranchProductRequest(), new WmsBranchProductRequest() }
            };
            _branchRequestRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsBranchRequest>(f => f.RequestId.Equals(branchRequest.RequestId))))
                .ReturnsAsync(branchRequest);
            _branchRequestProductRepoMock.Setup(x => x.FindMultiple(MoqHelper.IsExpression<WmsBranchProductRequest>(f => f.RequestId.Equals(branchRequest.RequestId))))
                .ReturnsAsync(branchRequest.WmsBranchProductRequests);
            _userRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsUser, bool>>>()))
               .ReturnsAsync(UserData.Data.FirstOrDefault());
            _productRepoMock.Setup(x => x.GetById(Guid.Parse("b08d504e-8e0b-47b9-a51c-50adf96a2338"))).ReturnsAsync(new WmsProduct() { ExportPrice = 10000 });
            _branchWarehouseUserRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchWarehouseUser, bool>>>()))
               .ReturnsAsync(new WmsBranchWarehouseUser());


            //Act
            var actualResult = await _branchImportService.UpdateBranchImportRequest(entity, UserData.Data.First().Username).ConfigureAwait(false);

            //Assert
            Assert.Equal(result, actualResult);
            if (result)
            {
                _branchRequestRepoMock.Verify(x => x.Update(It.IsAny<WmsBranchRequest>()), Times.Once);
                _branchRequestProductRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBranchProductRequest>()), Times.Exactly(entity.ProductRequests.Count()));
                _branchRequestProductRepoMock.Verify(x => x.Delete(It.IsAny<WmsBranchProductRequest>()), Times.Exactly(branchRequest.WmsBranchProductRequests.Count()));
            }
            else
            {
                _branchRequestRepoMock.Verify(x => x.Update(It.IsAny<WmsBranchRequest>()), Times.Never);
                _branchRequestProductRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBranchProductRequest>()), Times.Never);
                _branchRequestProductRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBranchProductRequest>()), Times.Never);
            }

        }
        public static IEnumerable<object[]> UpdateBranchRequestData()
        {
            yield return new object[]  //normal
            {
                new UpdateBranchImportRequestRequestDto()
                {
                    RequestId = Guid.Parse("a23d504e-8e0b-47b9-a51c-50adf96a2339"),
                    RequestDate = new DateTime(DateTime.Now.AddDays(10).Ticks, DateTimeKind.Unspecified),
                    ProductRequests = new List<CreateBranchImportRequestProductRequestDto>()
                    {
                        new CreateBranchImportRequestProductRequestDto()
                        {
                            ProductId = Guid.Parse("b08d504e-8e0b-47b9-a51c-50adf96a2338"),
                            RequestQuantity = 10,
                        }
                    }
                }, true
            };

            yield return new object[]  //request id
            {
                new UpdateBranchImportRequestRequestDto()
                {
                    RequestId = Guid.Empty,
                    RequestDate = new DateTime(DateTime.Now.AddDays(10).Ticks, DateTimeKind.Unspecified),
                    ProductRequests = new List<CreateBranchImportRequestProductRequestDto>()
                    {
                        new CreateBranchImportRequestProductRequestDto()
                        {
                            ProductId = Guid.Parse("b08d504e-8e0b-47b9-a51c-50adf96a2338"),
                            RequestQuantity = 10,
                        }
                    }
                }, false
            };

            yield return new object[]  //date - fail
            {
                new UpdateBranchImportRequestRequestDto()
                {
                    RequestId = Guid.Parse("a23d504e-8e0b-47b9-a51c-50adf96a2339"),
                    RequestDate = new DateTime(DateTime.Now.AddDays(-10).Ticks, DateTimeKind.Unspecified),
                    ProductRequests = new List<CreateBranchImportRequestProductRequestDto>()
                    {
                        new CreateBranchImportRequestProductRequestDto()
                        {
                            ProductId = Guid.Parse("b08d504e-8e0b-47b9-a51c-50adf96a2338"),
                            RequestQuantity = 10,
                        }
                    }
                }, false
            };

            yield return new object[]  //product
            {
                new UpdateBranchImportRequestRequestDto()
                {
                    RequestId = Guid.Parse("a23d504e-8e0b-47b9-a51c-50adf96a2339"),
                    RequestDate = new DateTime(DateTime.Now.AddDays(10).Ticks, DateTimeKind.Unspecified),
                    ProductRequests = new List<CreateBranchImportRequestProductRequestDto>()
                    {
                        new CreateBranchImportRequestProductRequestDto()
                        {
                            ProductId = Guid.Empty,
                            RequestQuantity = 10,
                        }
                    }
                }, false
            };

            yield return new object[]  //list empty - fail
            {
                new UpdateBranchImportRequestRequestDto()
                {
                    RequestId = Guid.Parse("a23d504e-8e0b-47b9-a51c-50adf96a2339"),
                    RequestDate = new DateTime(DateTime.Now.AddDays(10).Ticks, DateTimeKind.Unspecified),
                    ProductRequests = new List<CreateBranchImportRequestProductRequestDto>()
                    {
                        new CreateBranchImportRequestProductRequestDto()
                        {
                        }
                    }
                }, false
            };
        }

        [Theory]
        [InlineData(0, true)]
        [InlineData(1, false)]
        [InlineData(2, false)]
        public async Task ApproveImportRequest(short status, bool result)
        {
            //Arrange
            var branchRequest = BranchRequestData.Data.First();
            branchRequest.ApprovalStatus = status;
            branchRequest.WmsBranchProductRequests = new List<WmsBranchProductRequest>() { new WmsBranchProductRequest() { ProductId = Guid.Parse("b08d504e-8e0b-47b9-a51c-50adf96a2338") } };

            var baseImport = new List<WmsBaseImport>() { BaseImportData.Data.First() };
            var query = baseImport.AsEnumerable().BuildMock();

            _branchRequestRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsBranchRequest>(f => f.RequestId.Equals(branchRequest.RequestId))))
                .ReturnsAsync(branchRequest);
            _branchRequestProductRepoMock.Setup(x => x.FindMultiple(MoqHelper.IsExpression<WmsBranchProductRequest>(f => f.RequestId.Equals(branchRequest.RequestId))))
                .ReturnsAsync(branchRequest.WmsBranchProductRequests);
            _userRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsUser, bool>>>()))
               .ReturnsAsync(UserData.Data.FirstOrDefault());
            _baseWarehouseRepoMock.Setup(x => x.GetBaseWarehouseIncludingOwner())
               .Returns((new List<WmsBaseWarehouse>() { BaseWarehouseData.Data.FirstOrDefault() }).AsEnumerable().BuildMock());
            _productRepoMock.Setup(x => x.GetById(Guid.Parse("b08d504e-8e0b-47b9-a51c-50adf96a2338"))).ReturnsAsync(new WmsProduct() { ExportPrice = 10000 });
            _baseImportRepoMock.Setup(x => x.FilterSearchBaseImport(null, null, null, null, default(DateTime), default(DateTime)))
               .Returns(query);
            _branchWarehouseUserRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchWarehouseUser, bool>>>()))
               .ReturnsAsync(new WmsBranchWarehouseUser());

            //Act
            var actualResult = await _branchImportService.ApproveBranchImportRequest(branchRequest.RequestId, UserData.Data.First().Username).ConfigureAwait(false);

            //Assert
            Assert.Equal(result, actualResult);
            if (result)
            {
                _baseExportRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBaseExport>()), Times.Once);
                _baseExportProductRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBaseExportProduct>()), Times.Once);
                _branchRequestRepoMock.Verify(x => x.Update(It.IsAny<WmsBranchRequest>()), Times.Once);
            }
            else
            {
                _baseExportRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBaseExport>()), Times.Never);
                _baseExportProductRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBaseExportProduct>()), Times.Never);
                _branchRequestRepoMock.Verify(x => x.Update(It.IsAny<WmsBranchRequest>()), Times.Never);
            }
        }

        [Fact]
        public async Task ApproveImportRequest_IdDoesNotExist()
        {
            //Arrange
            var branchRequest = BranchRequestData.Data.First();
            branchRequest.RequestId = Guid.Empty;
            branchRequest.WmsBranchProductRequests = new List<WmsBranchProductRequest>() { new WmsBranchProductRequest() { ProductId = Guid.Parse("b08d504e-8e0b-47b9-a51c-50adf96a2338") } };

            var baseImport = new List<WmsBaseImport>() { BaseImportData.Data.First() };
            var query = baseImport.AsEnumerable().BuildMock();

            _branchRequestRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsBranchRequest>(f => f.RequestId.Equals(Guid.Parse("a23d504e-8e0b-47b9-a51c-50adf96a2339")))))
                .ReturnsAsync(branchRequest);
            _branchRequestProductRepoMock.Setup(x => x.FindMultiple(MoqHelper.IsExpression<WmsBranchProductRequest>(f => f.RequestId.Equals(branchRequest.RequestId))))
                .ReturnsAsync(branchRequest.WmsBranchProductRequests);
            _userRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsUser, bool>>>()))
               .ReturnsAsync(UserData.Data.FirstOrDefault());
            _baseWarehouseRepoMock.Setup(x => x.GetBaseWarehouseIncludingOwner())
               .Returns((new List<WmsBaseWarehouse>() { BaseWarehouseData.Data.FirstOrDefault() }).AsEnumerable().BuildMock());
            _productRepoMock.Setup(x => x.GetById(Guid.Parse("b08d504e-8e0b-47b9-a51c-50adf96a2338"))).ReturnsAsync(new WmsProduct() { ExportPrice = 10000 });
            _baseImportRepoMock.Setup(x => x.FilterSearchBaseImport(null, null, null, null, default(DateTime), default(DateTime)))
               .Returns(query);

            //Act
            var actualResult = await _branchImportService.ApproveBranchImportRequest(branchRequest.RequestId, UserData.Data.First().Username).ConfigureAwait(false);

            //Assert
            Assert.Equal(false, actualResult);
            _baseExportRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBaseExport>()), Times.Never);
            _baseExportProductRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBaseExportProduct>()), Times.Never);
            _branchRequestRepoMock.Verify(x => x.Update(It.IsAny<WmsBranchRequest>()), Times.Never);
        }

        [Fact]
        public async Task GetBranchImportList_ImportCodeExist_ReturnsExpectedResult()
        {
            // Arrange
            var requestDto = new GetBranchImportRequestDto
            {
                SearchWord = "IMPORT_CODE_1",
                ImportStatus = 1,
                PagingRequest = new PagingRequest
                {
                    CurrentPage = 1,
                    PageRange = 5,
                    PageSize = 10
                },
                PaymentStatus = 1,
                DateFilter = 0
            };

            var warehouseId = Guid.NewGuid();
            var currentUser = "root";

            var baseImportList = new List<WmsBranchImport>() {
                new WmsBranchImport(Guid.NewGuid(), warehouseId, Guid.NewGuid(),"IMPORT_CODE_1", Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), DateTime.Now, 10000, 10000, 10000, 10000, "test", 1, 1, Guid.NewGuid(),"test", DateTime.Now, "test", DateTime.Now),
                new WmsBranchImport(Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(),"IMPORT_CODE_2", Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), DateTime.Now, 10000, 10000, 10000, 10000, "test", 1, 1, Guid.NewGuid(),"test", DateTime.Now, "test", DateTime.Now),
            };

            _dateService.Setup(ds => ds.GetDateTimeByDateFilter(It.IsAny<short>()))
                .Returns(new DateFilterRequestDto(DateTime.Now.AddDays(-7), DateTime.Now));

            _warehouseService.Setup(sv => sv.GetWarehouseIdByUsername(currentUser))
                .ReturnsAsync(warehouseId);

            var data = baseImportList.AsQueryable().BuildMock();
            _branchImportRepoMock.Setup(repo => repo.FilterBranchImport(
                    warehouseId,
                    requestDto.SearchWord,
                    requestDto.ImportStatus,
                    requestDto.PaymentStatus,
                    It.IsAny<DateTime>(),
                    It.IsAny<DateTime>()
                )).Returns(data);

            _branchImportRepoMock.Setup(repo => repo.Count())
                .ReturnsAsync(baseImportList.Count);

            _storageService.Setup(service => service.GetFileUrl(It.IsAny<S3RequestData>()))
                .Returns("mockedImageUrl");

            // Act
            var result = await _branchImportService.GetBranchImportList(requestDto, "root");

            // Assert
            Assert.NotNull(result);
            Assert.NotEmpty(result.Items);
            Assert.IsType<ViewPaging<GetBranchImportResponseDto>>(result);
        }

        [Fact]
        public async Task GetBranchImportList_ImportCodeNotExist_ReturnsExpectedResult()
        {
            // Arrange
            var requestDto = new GetBranchImportRequestDto
            {
                SearchWord = "IMPORT_CODE_3000",
                ImportStatus = 1,
                PagingRequest = new PagingRequest
                {
                    CurrentPage = 1,
                    PageRange = 5,
                    PageSize = 10
                },
                PaymentStatus = 1,
                DateFilter = 0
            };

            var warehouseId = Guid.NewGuid();
            var currentUser = "root";

            var baseImportList = new List<WmsBranchImport>() {
                new WmsBranchImport(Guid.NewGuid(), warehouseId, Guid.NewGuid(),"IMPORT_CODE_1", Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), DateTime.Now, 10000, 10000, 10000, 10000, "test", 1, 1, Guid.NewGuid(),"test", DateTime.Now, "test", DateTime.Now),
                new WmsBranchImport(Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(),"IMPORT_CODE_2", Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), DateTime.Now, 10000, 10000, 10000, 10000, "test", 1, 1, Guid.NewGuid(),"test", DateTime.Now, "test", DateTime.Now),
            };

            _dateService.Setup(ds => ds.GetDateTimeByDateFilter(It.IsAny<short>()))
                .Returns(new DateFilterRequestDto(DateTime.Now.AddDays(-7), DateTime.Now));

            _warehouseService.Setup(sv => sv.GetWarehouseIdByUsername(currentUser))
                .ReturnsAsync(warehouseId);

            var data = baseImportList.Where(x => x.ImportCode.Equals(requestDto.SearchWord)).AsQueryable().BuildMock();
            _branchImportRepoMock.Setup(repo => repo.FilterBranchImport(
                    warehouseId,
                    requestDto.SearchWord,
                    requestDto.ImportStatus,
                    requestDto.PaymentStatus,
                    It.IsAny<DateTime>(),
                    It.IsAny<DateTime>()
                )).Returns(data);

            _branchImportRepoMock.Setup(repo => repo.Count())
                .ReturnsAsync(baseImportList.Count);

            _storageService.Setup(service => service.GetFileUrl(It.IsAny<S3RequestData>()))
                .Returns("mockedImageUrl");

            // Act
            var result = await _branchImportService.GetBranchImportList(requestDto, "root");

            // Assert
            Assert.NotNull(result);
            Assert.Empty(result.Items);
            Assert.IsType<ViewPaging<GetBranchImportResponseDto>>(result);
        }

        [Fact]
        public async Task GetBranchImportList_ImportStatusNotExist_ReturnsExpectedResult()
        {
            // Arrange
            var requestDto = new GetBranchImportRequestDto
            {
                SearchWord = "IMPORT_CODE_1",
                ImportStatus = 2,
                PagingRequest = new PagingRequest
                {
                    CurrentPage = 1,
                    PageRange = 5,
                    PageSize = 10
                },
                PaymentStatus = 1,
                DateFilter = 0
            };

            var warehouseId = Guid.NewGuid();
            var currentUser = "root";

            var baseImportList = new List<WmsBranchImport>() {
                new WmsBranchImport(Guid.NewGuid(), warehouseId, Guid.NewGuid(),"IMPORT_CODE_1", Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), DateTime.Now, 10000, 10000, 10000, 10000, "test", 1, 1, Guid.NewGuid(),"test", DateTime.Now, "test", DateTime.Now),
                new WmsBranchImport(Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(),"IMPORT_CODE_2", Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), DateTime.Now, 10000, 10000, 10000, 10000, "test", 1, 1, Guid.NewGuid(),"test", DateTime.Now, "test", DateTime.Now),
            };

            _dateService.Setup(ds => ds.GetDateTimeByDateFilter(It.IsAny<short>()))
                .Returns(new DateFilterRequestDto(DateTime.Now.AddDays(-7), DateTime.Now));

            _warehouseService.Setup(sv => sv.GetWarehouseIdByUsername(currentUser))
                .ReturnsAsync(warehouseId);

            var data = baseImportList.Where(x => x.ImportStatus.Equals(requestDto.ImportStatus)).AsQueryable().BuildMock();
            _branchImportRepoMock.Setup(repo => repo.FilterBranchImport(
                    warehouseId,
                    requestDto.SearchWord,
                    requestDto.ImportStatus,
                    requestDto.PaymentStatus,
                    It.IsAny<DateTime>(),
                    It.IsAny<DateTime>()
                )).Returns(data);

            _branchImportRepoMock.Setup(repo => repo.Count())
                .ReturnsAsync(baseImportList.Count);

            _storageService.Setup(service => service.GetFileUrl(It.IsAny<S3RequestData>()))
                .Returns("mockedImageUrl");

            // Act
            var result = await _branchImportService.GetBranchImportList(requestDto, "root");

            // Assert
            Assert.NotNull(result);
            Assert.Empty(result.Items);
            Assert.IsType<ViewPaging<GetBranchImportResponseDto>>(result);
        }

        [Fact]
        public async Task GetBranchImportList_PaymentStatusNotExist_ReturnsExpectedResult()
        {
            // Arrange
            var requestDto = new GetBranchImportRequestDto
            {
                SearchWord = "IMPORT_CODE_1",
                ImportStatus = 1,
                PagingRequest = new PagingRequest
                {
                    CurrentPage = 1,
                    PageRange = 5,
                    PageSize = 10
                },
                PaymentStatus = 2,
                DateFilter = 0
            };

            var warehouseId = Guid.NewGuid();
            var currentUser = "root";

            var baseImportList = new List<WmsBranchImport>() {
                new WmsBranchImport(Guid.NewGuid(), warehouseId, Guid.NewGuid(),"IMPORT_CODE_1", Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), DateTime.Now, 10000, 10000, 10000, 10000, "test", 1, 1, Guid.NewGuid(),"test", DateTime.Now, "test", DateTime.Now),
                new WmsBranchImport(Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(),"IMPORT_CODE_2", Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), DateTime.Now, 10000, 10000, 10000, 10000, "test", 1, 1, Guid.NewGuid(),"test", DateTime.Now, "test", DateTime.Now),
            };

            _dateService.Setup(ds => ds.GetDateTimeByDateFilter(It.IsAny<short>()))
                .Returns(new DateFilterRequestDto(DateTime.Now.AddDays(-7), DateTime.Now));

            _warehouseService.Setup(sv => sv.GetWarehouseIdByUsername(currentUser))
                .ReturnsAsync(warehouseId);

            var data = baseImportList.Where(x => x.PaymentStatus.Equals(requestDto.PaymentStatus)).AsQueryable().BuildMock();
            _branchImportRepoMock.Setup(repo => repo.FilterBranchImport(
                    warehouseId,
                    requestDto.SearchWord,
                    requestDto.ImportStatus,
                    requestDto.PaymentStatus,
                    It.IsAny<DateTime>(),
                    It.IsAny<DateTime>()
                )).Returns(data);

            _branchImportRepoMock.Setup(repo => repo.Count())
                .ReturnsAsync(baseImportList.Count);

            _storageService.Setup(service => service.GetFileUrl(It.IsAny<S3RequestData>()))
                .Returns("mockedImageUrl");

            // Act
            var result = await _branchImportService.GetBranchImportList(requestDto, "root");

            // Assert
            Assert.NotNull(result);
            Assert.Empty(result.Items);
            Assert.IsType<ViewPaging<GetBranchImportResponseDto>>(result);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("10c48c7c-48b0-4ea1-9e2c-95f6a219dc6a")]
        public async Task GetBaseImportRequest_Returns_Null_When_BaseImport_NotFound(Guid importRequestId)
        {
            // Arrange
            _unitOfWork.Setup(uow => uow.BranchRequestRepository.GetBranchRequestDetails(importRequestId))
                          .ReturnsAsync((WmsBranchRequest)null);

            // Act
            var result = await _branchRequestService.GetBranchRequestDetails(importRequestId);

            // Assert
            Assert.Null(result);
        }

        [Theory]
        [InlineData("10c48c7c-48b0-4ea1-9e2c-95f6a219dc6b")]
        public async Task GetBaseImportRequest_Returns_Null_When_BaseImport_Founded(Guid importRequestId)
        {
            // Arrange
            _unitOfWork.Setup(uow => uow.BranchRequestRepository.GetBranchRequestDetails(importRequestId))
                          .ReturnsAsync(new WmsBranchRequest());

            // Act
            var result = await _branchRequestService.GetBranchRequestDetails(importRequestId);

            // Assert
            Assert.NotNull(result);
        }

        [Theory]
        [MemberData(nameof(ViewBranchImportListData))]
        public async Task ViewBranchImportList(GetBranchImportRequestDto entity, bool result)
        {
            //Arrange
            var list = new List<WmsBranchImport>() {
                new WmsBranchImport()
                {
                    WmsBranchImportProducts = new List<WmsBranchImportProduct>(){ new WmsBranchImportProduct() { ProductId = Guid.NewGuid() } }
                },
                new WmsBranchImport()
                {
                    WmsBranchImportProducts = new List<WmsBranchImportProduct>(){ new WmsBranchImportProduct() { ProductId = Guid.NewGuid() } }
                },
                new WmsBranchImport()
                {
                    WmsBranchImportProducts = new List<WmsBranchImportProduct>(){ new WmsBranchImportProduct() { ProductId = Guid.NewGuid() } }
                },
            };
            _branchImportRepoMock.Setup(x => x.FilterBranchImport(It.IsAny<Guid>(), entity.SearchWord, entity.ImportStatus, entity.PaymentStatus, It.IsAny<DateTime>(), It.IsAny<DateTime>()))
                .Returns(list.BuildMock());
            _warehouseService.Setup(x => x.GetWarehouseIdByUsername(It.IsAny<string>())).ReturnsAsync(Guid.NewGuid());
            _dateService.Setup(x => x.GetDateTimeByDateFilter(It.IsAny<short>())).Returns(new DateFilterRequestDto() { StartDate = DateTime.Now, EndDate = DateTime.Now });
            _storageService.Setup(x => x.GetFileUrl(It.IsAny<S3RequestData>())).Returns("image");

            //Act
            var actual = await _branchImportService.GetBranchImportList(entity, UserData.Data.First().Username).ConfigureAwait(false);

            //Assert
            if (result)
            {
                Assert.NotNull(actual);
                Assert.NotEmpty(actual.Items);
            }
            else
            {
                Assert.Null(actual);
            }
        }

        public static IEnumerable<object[]> ViewBranchImportListData()
        {
            yield return new object[]
            {
                new GetBranchImportRequestDto() //normal case
                {
                    ImportStatus = 1,
                    DateFilter = 0,
                    PaymentStatus = 1,
                    SearchWord = "BRIM",
                    PagingRequest = new PagingRequest()
                    {
                        PageSize = 10,
                        PageRange = 10,
                        CurrentPage = 1
                    }
                }, true
            };
            yield return new object[] //normal case
            {
                new GetBranchImportRequestDto()
                {
                    ImportStatus = 2,
                    DateFilter = 0,
                    PaymentStatus = 1,
                    SearchWord = "BRIM",
                    PagingRequest = new PagingRequest()
                    {
                        PageSize = 10,
                        PageRange = 10,
                        CurrentPage = 1
                    }
                }, true
            };
            yield return new object[]
            {
                new GetBranchImportRequestDto() //normal case
                {
                    ImportStatus = 1,
                    DateFilter = 0,
                    PaymentStatus = 2,
                    SearchWord = "BRIM",
                    PagingRequest = new PagingRequest()
                    {
                        PageSize = 10,
                        PageRange = 10,
                        CurrentPage = 1
                    }
                }, true
            };
            yield return new object[]
            {
                new GetBranchImportRequestDto() //normal case
                {
                    ImportStatus = 1,
                    DateFilter = 0,
                    PaymentStatus = 3,
                    SearchWord = "BRIM",
                    PagingRequest = new PagingRequest()
                    {
                        PageSize = 10,
                        PageRange = 10,
                        CurrentPage = 1
                    }
                }, true
            };
            yield return new object[]
            {
                new GetBranchImportRequestDto()  //normal case
                {
                    ImportStatus = 1,
                    DateFilter = 1,
                    PaymentStatus = 1,
                    SearchWord = null,
                    PagingRequest = new PagingRequest()
                    {
                        PageSize = 10,
                        PageRange = 10,
                        CurrentPage = 1
                    }
                }, true
            };
            yield return new object[]
            {
                new GetBranchImportRequestDto()  //normal case
                {
                    ImportStatus = 1,
                    DateFilter = 2,
                    PaymentStatus = 1,
                    SearchWord = "BRIM",
                    PagingRequest = new PagingRequest()
                    {
                        PageSize = 10,
                        PageRange = 10,
                        CurrentPage = 1
                    }
                }, true
            };
            yield return new object[]
            {
                new GetBranchImportRequestDto()  //normal case
                {
                    ImportStatus = 1,
                    DateFilter = 3,
                    PaymentStatus = 1,
                    SearchWord = "BRIM",
                    PagingRequest = new PagingRequest()
                    {
                        PageSize = 10,
                        PageRange = 10,
                        CurrentPage = 1
                    }
                }, true
            };
            yield return new object[]
            {
                new GetBranchImportRequestDto()  //normal case
                {
                    ImportStatus = 1,
                    DateFilter = 0,
                    PaymentStatus = 1,
                    SearchWord = null,
                    PagingRequest = new PagingRequest()
                    {
                        PageSize = 10,
                        PageRange = 10,
                        CurrentPage = 1
                    }
                }, true
            };
            yield return new object[]  //abnormal paging
            {
                new GetBranchImportRequestDto()
                {
                    ImportStatus = 1,
                    DateFilter = 0,
                    PaymentStatus = 1,
                    SearchWord = "BRIM",
                    PagingRequest = new PagingRequest()
                    {
                        PageSize = -1,
                        PageRange = 10,
                        CurrentPage = 1
                    }
                }, true
            };
            yield return new object[]  //abnormal paging
            {
                new GetBranchImportRequestDto()
                {
                    ImportStatus = 1,
                    DateFilter = 0,
                    PaymentStatus = 1,
                    SearchWord = "BRIM",
                    PagingRequest = new PagingRequest()
                    {
                        PageSize = 10,
                        PageRange = -1,
                        CurrentPage = 1
                    }
                }, true
            };
            yield return new object[]  //abnormal paging
            {
                new GetBranchImportRequestDto()
                {
                    ImportStatus = 1,
                    DateFilter = 0,
                    PaymentStatus = 1,
                    SearchWord = "BRIM",
                    PagingRequest = new PagingRequest()
                    {
                        PageSize = 10,
                        PageRange = 10,
                        CurrentPage = -1
                    }
                }, true
            };
            yield return new object[]
            {
                new GetBranchImportRequestDto() //invalid import status
                {
                    ImportStatus = 999,
                    DateFilter = 0,
                    PaymentStatus = 0,
                    SearchWord = "BRIM",
                    PagingRequest = new PagingRequest()
                    {
                        PageSize = 10,
                        PageRange = 10,
                        CurrentPage = 1
                    }
                }, false
            };
            yield return new object[]
            {
                new GetBranchImportRequestDto() //invalid payment status
                {
                    ImportStatus = 1,
                    DateFilter = 0,
                    PaymentStatus = 999,
                    SearchWord = "BRIM",
                    PagingRequest = new PagingRequest()
                    {
                        PageSize = 10,
                        PageRange = 10,
                        CurrentPage = 1
                    }
                }, false
            };
            yield return new object[]
            {
                new GetBranchImportRequestDto() //invalid filter date
                {
                    ImportStatus = 1,
                    DateFilter = 999,
                    PaymentStatus = 0,
                    SearchWord = "BRIM",
                    PagingRequest = new PagingRequest()
                    {
                        PageSize = 10,
                        PageRange = 10,
                        CurrentPage = 1
                    }
                }, false
            };
        }

        [Fact]
        public async Task ViewBranchImportDetails_IdExist()
        {
            //Arrange
            var id = Guid.NewGuid();
            var branchImport = new WmsBranchImport()
            {
                WmsBranchImportProducts = new List<WmsBranchImportProduct>() { new WmsBranchImportProduct() { ProductId = Guid.NewGuid() } }
            };

            _branchImportRepoMock.Setup(x => x.GetBranchRequestDetails(It.IsAny<Guid>())).ReturnsAsync(branchImport);
            _branchImportProductRepoMock.Setup(x => x.GetBranchImportProductByImportIdAndProductId(It.IsAny<Guid>(), It.IsAny<Guid>())).ReturnsAsync(branchImport.WmsBranchImportProducts);
            _branchDebtRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchDebt, bool>>>())).ReturnsAsync(new WmsBranchDebt());
            _branchImportRepoMock.Setup(x => x.FindMultiple(It.IsAny<Expression<Func<WmsBranchImport, bool>>>())).ReturnsAsync(new List<WmsBranchImport>());
            _branchReturnDetailRepoMock.Setup(x => x.FindMultiple(It.IsAny<Expression<Func<WmsBranchProductReturnDetail, bool>>>())).ReturnsAsync(new List<WmsBranchProductReturnDetail>());
            _productService.Setup(x => x.GetProductReturnFromBranchByImportId(It.IsAny<Guid>())).ReturnsAsync(new List<GetProductsReturnFromBranchResponseDto>());
            _invoiceService.Setup(x => x.GetBranchInvoiceByImportId(It.IsAny<Guid>())).ReturnsAsync(new List<InvoiceResponseDto>());
            _invoiceService.Setup(x => x.GetBranchInvoiceByReturnId(It.IsAny<IEnumerable<Guid>>())).ReturnsAsync(new List<InvoiceResponseDto>());

            //Act
            var result = await _branchImportService.GetBranchImportDetails(id).ConfigureAwait(false);

            //Assert
            Assert.NotNull(result);
        }

        [Fact]
        public async Task ViewBranchImportDetails_IdNotExist()
        {
            //Arrange
            var id = Guid.Empty;
            var list = new List<WmsBranchImport>() { new WmsBranchImport() { ImportId = Guid.NewGuid() } };
            _branchImportRepoMock.Setup(x => x.GetBranchRequestDetails(id)).ReturnsAsync(list.FirstOrDefault(x => x.ImportId == id));

            //Act
            var result = await _branchImportService.GetBranchImportDetails(id).ConfigureAwait(false);

            //Assert
            Assert.Null(result);
        }

        private List<WmsBranchImport> branchImports = new List<WmsBranchImport>()
        {
            new WmsBranchImport() { ImportId = Guid.Parse("aa7778c5-00f0-47fd-b40d-0e4b7fbb2739"), ImportStatus = 1 },
            new WmsBranchImport() { ImportId = Guid.Parse("aa7778c5-00f0-47fd-b40d-0e4b7fbb2738"), ImportStatus = 2 },
            new WmsBranchImport() { ImportId = Guid.NewGuid() },
        };

        [Fact]
        public async Task ChangeBranchImportStatus_NormalCaseWhenBranchProductNotExist()
        {
            //Arrange
            Guid importId = Guid.Parse("aa7778c5-00f0-47fd-b40d-0e4b7fbb2739");

            _branchImportRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchImport, bool>>>())).ReturnsAsync(branchImports.FirstOrDefault(x => x.ImportId == importId));
            _userRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsUser, bool>>>())).ReturnsAsync(new WmsUser());
            _branchWarehouseUserRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchWarehouseUser, bool>>>())).ReturnsAsync(new WmsBranchWarehouseUser());
            _branchDebtRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchDebt, bool>>>())).ReturnsAsync(new WmsBranchDebt());
            _baseExportRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBaseExport, bool>>>())).ReturnsAsync(new WmsBaseExport());
            _branchImportProductRepoMock.Setup(x => x.FindMultiple(It.IsAny<Expression<Func<WmsBranchImportProduct, bool>>>())).ReturnsAsync(new List<WmsBranchImportProduct>(){
                new WmsBranchImportProduct()
            });
            _baseImportProductRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBaseImportProduct, bool>>>())).ReturnsAsync(new WmsBaseImportProduct());
            _branchImportProductRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchImportProduct, bool>>>())).ReturnsAsync(new WmsBranchImportProduct());
            _branchWarehouseRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchWarehouse, bool>>>())).ReturnsAsync(new WmsBranchWarehouse());

            //Act
            var actual = await _branchImportService.ChangeBranchImportStatus(importId, UserData.Data.First().Username).ConfigureAwait(false);

            //Assert
            Assert.True(actual);
            _branchImportRepoMock.Verify(x => x.Update(It.IsAny<WmsBranchImport>()), Times.Once);
            _baseExportRepoMock.Verify(x => x.Update(It.IsAny<WmsBaseExport>()), Times.Once);
            _branchDebtRepoMock.Verify(x => x.Update(It.IsAny<WmsBranchDebt>()), Times.Once);
            _branchImportProductRepoMock.Verify(x => x.Update(It.IsAny<WmsBranchImportProduct>()), Times.Once);
            _branchWarehouseProductRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBranchWarehouseProduct>()), Times.Once);
        }

        [Fact]
        public async Task ChangeBranchImportStatus_NormalCaseWhenBranchProductExist()
        {
            //Arrange
            Guid importId = Guid.Parse("aa7778c5-00f0-47fd-b40d-0e4b7fbb2739");

            _branchImportRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchImport, bool>>>())).ReturnsAsync(branchImports.FirstOrDefault(x => x.ImportId == importId));
            _userRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsUser, bool>>>())).ReturnsAsync(new WmsUser());
            _branchWarehouseUserRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchWarehouseUser, bool>>>())).ReturnsAsync(new WmsBranchWarehouseUser());
            _branchDebtRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchDebt, bool>>>())).ReturnsAsync(new WmsBranchDebt());
            _baseExportRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBaseExport, bool>>>())).ReturnsAsync(new WmsBaseExport());
            _branchImportProductRepoMock.Setup(x => x.FindMultiple(It.IsAny<Expression<Func<WmsBranchImportProduct, bool>>>())).ReturnsAsync(new List<WmsBranchImportProduct>(){
                new WmsBranchImportProduct()
            });
            _baseImportProductRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBaseImportProduct, bool>>>())).ReturnsAsync(new WmsBaseImportProduct());
            _branchWarehouseProductRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchWarehouseProduct, bool>>>())).ReturnsAsync(new WmsBranchWarehouseProduct());
            _branchWarehouseProductRepoMock.Setup(x => x.GetCostPriceByProductId(It.IsAny<Guid>(), It.IsAny<Guid>())).Returns(100);
            _branchWarehouseRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchWarehouse, bool>>>())).ReturnsAsync(new WmsBranchWarehouse());

            //Act
            var actual = await _branchImportService.ChangeBranchImportStatus(importId, UserData.Data.First().Username).ConfigureAwait(false);

            //Assert
            Assert.True(actual);
            _branchImportRepoMock.Verify(x => x.Update(It.IsAny<WmsBranchImport>()), Times.Once);
            _baseExportRepoMock.Verify(x => x.Update(It.IsAny<WmsBaseExport>()), Times.Once);
            _branchDebtRepoMock.Verify(x => x.Update(It.IsAny<WmsBranchDebt>()), Times.Once);
            _branchImportProductRepoMock.Verify(x => x.Update(It.IsAny<WmsBranchImportProduct>()), Times.Once);
            _branchWarehouseProductRepoMock.Verify(x => x.Update(It.IsAny<WmsBranchWarehouseProduct>()), Times.Once);
        }


        [Fact]
        public async Task ChangeBranchImportStatus_IdNotExist()
        {
            //Arrange
            Guid importId = Guid.Empty;

            _branchImportRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchImport, bool>>>())).ReturnsAsync(branchImports.FirstOrDefault(x => x.ImportId == importId));
            _userRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsUser, bool>>>())).ReturnsAsync(new WmsUser());
            _branchWarehouseUserRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchWarehouseUser, bool>>>())).ReturnsAsync(new WmsBranchWarehouseUser());
            _branchDebtRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchDebt, bool>>>())).ReturnsAsync(new WmsBranchDebt());
            _baseExportRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBaseExport, bool>>>())).ReturnsAsync(new WmsBaseExport());
            _branchImportProductRepoMock.Setup(x => x.FindMultiple(It.IsAny<Expression<Func<WmsBranchImportProduct, bool>>>())).ReturnsAsync(new List<WmsBranchImportProduct>(){
                new WmsBranchImportProduct()
            });
            _baseImportProductRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBaseImportProduct, bool>>>())).ReturnsAsync(new WmsBaseImportProduct());
            _branchWarehouseProductRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchWarehouseProduct, bool>>>())).ReturnsAsync(new WmsBranchWarehouseProduct());
            _branchImportProductRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchImportProduct, bool>>>())).ReturnsAsync(new WmsBranchImportProduct());
            _branchWarehouseProductRepoMock.Setup(x => x.GetCostPriceByProductId(It.IsAny<Guid>(), It.IsAny<Guid>())).Returns(100);

            //Act
            var actual = await _branchImportService.ChangeBranchImportStatus(importId, UserData.Data.First().Username).ConfigureAwait(false);

            //Assert
            Assert.False(actual);
        }

        [Fact]
        public async Task ChangeBranchImportStatus_Imported()
        {
            //Arrange
            Guid importId = Guid.Parse("aa7778c5-00f0-47fd-b40d-0e4b7fbb2738");

            _branchImportRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchImport, bool>>>())).ReturnsAsync(branchImports.FirstOrDefault(x => x.ImportId == importId));
            _userRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsUser, bool>>>())).ReturnsAsync(new WmsUser());
            _branchWarehouseUserRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchWarehouseUser, bool>>>())).ReturnsAsync(new WmsBranchWarehouseUser());
            _branchDebtRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchDebt, bool>>>())).ReturnsAsync(new WmsBranchDebt());
            _baseExportRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBaseExport, bool>>>())).ReturnsAsync(new WmsBaseExport());
            _branchImportProductRepoMock.Setup(x => x.FindMultiple(It.IsAny<Expression<Func<WmsBranchImportProduct, bool>>>())).ReturnsAsync(new List<WmsBranchImportProduct>(){
                new WmsBranchImportProduct()
            });
            _baseImportProductRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBaseImportProduct, bool>>>())).ReturnsAsync(new WmsBaseImportProduct());
            _branchWarehouseProductRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchWarehouseProduct, bool>>>())).ReturnsAsync(new WmsBranchWarehouseProduct());
            _branchImportProductRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchImportProduct, bool>>>())).ReturnsAsync(new WmsBranchImportProduct());
            _branchWarehouseProductRepoMock.Setup(x => x.GetCostPriceByProductId(It.IsAny<Guid>(), It.IsAny<Guid>())).Returns(100);

            //Act
            var actual = await _branchImportService.ChangeBranchImportStatus(importId, UserData.Data.First().Username).ConfigureAwait(false);

            //Assert
            Assert.False(actual);
        }
    }
}
