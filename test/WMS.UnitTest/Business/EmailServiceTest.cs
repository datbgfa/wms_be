﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using WMS.Business.Services;

namespace WMS.UnitTest.Business
{
    public class EmailServiceTest
    {
        private readonly EmailService _emailService;
        private readonly Mock<ILogger<EmailService>> _logger = new Mock<ILogger<EmailService>>();


        public EmailServiceTest()
        {
            var inMemorySettings = new Dictionary<string, string> {
                {"Email:Account", "warehousemanagementsystem12@gmail.com"},
                {"Email:AppPassword", "tqmc kdto sdcu hzvq"},
            };

            IConfiguration configuration = new ConfigurationBuilder()
                .AddInMemoryCollection(inMemorySettings)
                .Build();

            _emailService = new EmailService(_logger.Object, configuration);
        }

        [Fact]
        public async Task SendEmail()
        {
            var result = await _emailService.SendEmail("minhnamtran2002@gmail.com", "Hi namtm", "hi how are you").ConfigureAwait(false);
            Assert.True(result);
        }

        [Fact]
        public async Task SendEmail_InvalidEmail()
        {
            var result = await _emailService.SendEmail("minhnamtran2002", "Hi namtm", "hi how are you").ConfigureAwait(false);
            Assert.False(result);
        }
    }
}
