﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using Moq;
using MoqExpression;
using System.Data;
using System.Linq.Expressions;
using WMS.Business.Abstractions.Service;
using WMS.Business.Dtos.Mapper;
using WMS.Business.Services;
using WMS.Domain.Abstractions;
using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Entities;

namespace WMS.UnitTest.Business
{
    public class InvoiceServiceTest
    {
        private readonly InvoiceService _invoiceService;
        private readonly Mock<IUnitOfWork> _unitOfWork = new Mock<IUnitOfWork>();
        private readonly IMock<ILogger<InvoiceService>> _logger = new Mock<ILogger<InvoiceService>>();
        private readonly Mock<IWmsUserRepository> _userRepoMock = new Mock<IWmsUserRepository>();
        private readonly Mock<IWmsBaseInvoiceRepository> _baseInvoiceRepoMock = new Mock<IWmsBaseInvoiceRepository>();
        private readonly Mock<IWmsBranchInvoiceRepository> _branchInvoiceRepoMock = new Mock<IWmsBranchInvoiceRepository>();

        private readonly Mock<IUserService> _userServiceMock = new Mock<IUserService>();
        private readonly Mock<IWarehouseService> _warehouseServiceMock = new Mock<IWarehouseService>();
        private readonly Mock<IS3StorageService> _storageService = new Mock<IS3StorageService>();

        public InvoiceServiceTest()
        {
            var mockTran = new Mock<IDbTransaction>();

            _unitOfWork.Setup(x => x.BeginTransaction()).Returns(mockTran.Object);
            _unitOfWork.Setup(x => x.UserRepository).Returns(_userRepoMock.Object);
            _unitOfWork.Setup(x => x.BaseInvoiceRepository).Returns(_baseInvoiceRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchInvoiceRepository).Returns(_branchInvoiceRepoMock.Object);

            var configuration = new MapperConfiguration(config =>
            {
                config.AddProfile<DomainToDtoProfile>();
                config.AddProfile<DtoToDomainProfile>();
            });
            var mapper = new Mapper(configuration);

            _invoiceService = new InvoiceService(_unitOfWork.Object, _logger.Object, mapper, _userServiceMock.Object, _warehouseServiceMock.Object, _storageService.Object);
        }

        [Fact]
        public async Task GetBaseInvoiceByImportId_ShouldReturnInvoicesOrderedInvoices()
        {
            // Arrange
            var importId = Guid.Parse("ead0fcb7-bdfc-4106-893a-5c1c7a709aa1");

            var invoices = new List<WmsBaseInvoice>
            {
                new WmsBaseInvoice { ImportId = Guid.Parse("ead0fcb7-bdfc-4106-893a-5c1c7a709aa1"), CreateDate = DateTime.UtcNow.AddDays(-2) },
                new WmsBaseInvoice { ImportId = Guid.Parse("fb5cfa5d-71a9-48c7-af2a-d63e4f1d4be3"), CreateDate = DateTime.UtcNow.AddDays(-1) },
                new WmsBaseInvoice { ImportId = Guid.Parse("6f17759a-8763-4272-9ba8-1d6dba5786e3"), CreateDate = DateTime.UtcNow }
            };

            _baseInvoiceRepoMock.Setup(repo => repo.FindMultiple(MoqHelper.IsExpression<WmsBaseInvoice>(bi => bi.ImportId == importId)))
                .ReturnsAsync(invoices);

            // Act
            var result = await _invoiceService.GetBaseInvoiceByImportId(importId);

            // Assert
            Assert.NotNull(result);
            Assert.NotEmpty(result);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("ead0fcb7-bdfc-4106-893a-5c1c7a709aa2")]
        public async Task GetBaseInvoiceByImportId_ShouldReturnEmptyList(Guid importId)
        {
            // Arrange
            _baseInvoiceRepoMock.Setup(repo => repo.FindMultiple(MoqHelper.IsExpression<WmsBaseInvoice>(bi => bi.ImportId == importId)))
                .ReturnsAsync(new List<WmsBaseInvoice>());

            // Act
            var result = await _invoiceService.GetBaseInvoiceByImportId(importId);

            // Assert
            Assert.NotNull(result);
            Assert.Empty(result);
        }

        [Fact]
        public async Task GetBaseInvoiceByExportId_ShouldReturnOrderedInvoices()
        {
            // Arrange
            var exportId = Guid.Parse("ead0fcb7-bdfc-4106-893a-5c1c7a709aa1");

            var invoices = new List<WmsBaseInvoice>
            {
                new WmsBaseInvoice { ExportId = Guid.Parse("ead0fcb7-bdfc-4106-893a-5c1c7a709aa1"), CreateDate = DateTime.Now.AddDays(-2) },
                new WmsBaseInvoice { ExportId = Guid.Parse("fb5cfa5d-71a9-48c7-af2a-d63e4f1d4be3"), CreateDate = DateTime.Now.AddDays(-1) },
                new WmsBaseInvoice { ExportId = Guid.Parse("6f17759a-8763-4272-9ba8-1d6dba5786e3"), CreateDate = DateTime.Now }
            };

            _baseInvoiceRepoMock.Setup(repo => repo.FindMultiple(MoqHelper.IsExpression<WmsBaseInvoice>(bi => bi.ExportId == exportId)))
                .ReturnsAsync(invoices);

            // Act
            var result = await _invoiceService.GetBaseInvoiceByExportId(exportId);

            // Assert
            Assert.NotNull(result);
            Assert.NotEmpty(result);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("ead0fcb7-bdfc-4106-893a-5c1c7a709aaa")]
        public async Task GetBaseInvoiceByExportId_ShouldReturnEmptyList(Guid exportId)
        {
            // Arrange
            _baseInvoiceRepoMock.Setup(repo => repo.FindMultiple(MoqHelper.IsExpression<WmsBaseInvoice>(bi => bi.ExportId == exportId)))
                .ReturnsAsync(new List<WmsBaseInvoice>());

            // Act
            var result = await _invoiceService.GetBaseInvoiceByExportId(exportId);

            // Assert
            Assert.NotNull(result);
            Assert.Empty(result);
        }

        [Fact]
        public async Task GetBranchInvoiceByImportId_ShouldReturnOrderedInvoices()
        {
            // Arrange
            var importId = Guid.Parse("ead0fcb7-bdfc-4106-893a-5c1c7a709aa1");

            var invoices = new List<WmsBranchInvoice>
            {
                new WmsBranchInvoice { ImportId = Guid.Parse("ead0fcb7-bdfc-4106-893a-5c1c7a709aa1"), CreateDate = DateTime.UtcNow.AddDays(-2) },
                new WmsBranchInvoice { ImportId = Guid.Parse("fb5cfa5d-71a9-48c7-af2a-d63e4f1d4be3"), CreateDate = DateTime.UtcNow.AddDays(-1) },
                new WmsBranchInvoice { ImportId = Guid.Parse("6f17759a-8763-4272-9ba8-1d6dba5786e3"), CreateDate = DateTime.UtcNow }
            };

            _branchInvoiceRepoMock.Setup(repo => repo.FindMultiple(MoqHelper.IsExpression<WmsBranchInvoice>(bi => bi.ImportId == importId)))
                .ReturnsAsync(invoices);

            // Act
            var result = await _invoiceService.GetBranchInvoiceByImportId(importId);

            // Assert
            Assert.NotNull(result);
            Assert.NotEmpty(result);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("ead0fcb7-bdfc-4106-893a-5c1c7a709aa1")]
        public async Task GetBranchInvoiceByImportId_ShouldReturnEmptyList(Guid importId)
        {
            // Arrange
            _branchInvoiceRepoMock.Setup(repo => repo.FindMultiple(MoqHelper.IsExpression<WmsBranchInvoice>(bi => bi.ImportId == importId)))
                .ReturnsAsync(new List<WmsBranchInvoice>());

            // Act
            var result = await _invoiceService.GetBranchInvoiceByImportId(importId);

            // Assert
            Assert.NotNull(result);
            Assert.Empty(result);
        }

        [Fact]
        public async Task GetBranchInvoiceByExportId_ShouldReturnOrderedInvoices()
        {
            // Arrange
            var exportId = Guid.Parse("ead0fcb7-bdfc-4106-893a-5c1c7a709aa1");

            var invoices = new List<WmsBranchInvoice>
            {
                new WmsBranchInvoice { ExportId = Guid.Parse("ead0fcb7-bdfc-4106-893a-5c1c7a709aa1"), CreateDate = DateTime.UtcNow.AddDays(-2) },
                new WmsBranchInvoice { ExportId = Guid.Parse("fb5cfa5d-71a9-48c7-af2a-d63e4f1d4be3"), CreateDate = DateTime.UtcNow.AddDays(-1) },
                new WmsBranchInvoice { ExportId = Guid.Parse("6f17759a-8763-4272-9ba8-1d6dba5786e3"), CreateDate = DateTime.UtcNow }
            };

            _branchInvoiceRepoMock.Setup(repo => repo.FindMultiple(MoqHelper.IsExpression<WmsBranchInvoice>(bi => bi.ExportId == exportId)))
                .ReturnsAsync(invoices);

            // Act
            var result = await _invoiceService.GetBranchInvoiceByExportId(exportId);

            // Assert
            Assert.NotNull(result);
            Assert.NotEmpty(result);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("ead0fcb7-bdfc-4106-893a-5c1c7a709aa1")]
        public async Task GetBranchInvoiceByExportId_ShouldReturnEmptyList(Guid exportId)
        {
            // Arrange
            _branchInvoiceRepoMock.Setup(repo => repo.FindMultiple(MoqHelper.IsExpression<WmsBranchInvoice>(bi => bi.ExportId == exportId)))
                .ReturnsAsync(new List<WmsBranchInvoice>());

            // Act
            var result = await _invoiceService.GetBranchInvoiceByExportId(exportId);

            // Assert
            Assert.NotNull(result);
            Assert.Empty(result);
        }

        [Fact]
        public async Task GetBaseInvoiceByReturnId_ShouldReturnOrderedInvoices()
        {
            // Arrange
            var returnIds = new List<Guid> {
                Guid.Parse("ead0fcb7-bdfc-4106-893a-5c1c7a709aa1"),
                Guid.Parse("fb5cfa5d-71a9-48c7-af2a-d63e4f1d4be3"),
            };

            var invoices = new List<WmsBaseInvoice> {
                new WmsBaseInvoice { ReturnId = Guid.Parse("ead0fcb7-bdfc-4106-893a-5c1c7a709aa1"), CreateDate = DateTime.UtcNow.AddDays(-2) },
                new WmsBaseInvoice { ReturnId = Guid.Parse("fb5cfa5d-71a9-48c7-af2a-d63e4f1d4be3"), CreateDate = DateTime.UtcNow.AddDays(-1) },
                new WmsBaseInvoice { ReturnId = Guid.Parse("6f17759a-8763-4272-9ba8-1d6dba5786e3"), CreateDate = DateTime.UtcNow }
            };

            _baseInvoiceRepoMock
                .Setup(repo => repo.FindMultiple(It.IsAny<Expression<Func<WmsBaseInvoice, bool>>>()))
                .ReturnsAsync(invoices.Where(bi => returnIds.Contains(bi.ReturnId.Value)));

            // Act
            var result = await _invoiceService.GetBaseInvoiceByReturnId(returnIds);

            // Assert
            Assert.NotNull(result);
            Assert.NotEmpty(result);
        }

        [Theory]
        [InlineData(null)]
        public async Task GetBaseInvoiceByReturnId_ShouldReturnEmptyList(List<Guid> returnIds)
        {
            // Arrange
            _baseInvoiceRepoMock
                .Setup(repo => repo.FindMultiple(MoqHelper.IsExpression<WmsBaseInvoice>(bi => returnIds.Contains(bi.ReturnId.Value))))
                .ReturnsAsync(new List<WmsBaseInvoice>());

            // Act
            var result = await _invoiceService.GetBaseInvoiceByReturnId(returnIds);

            // Assert
            Assert.NotNull(result);
            Assert.Empty(result);

        }

        [Fact]
        public async Task GetBaseInvoiceByReturnId_ShouldReturnEmptyList_ReturnIdNotExist()
        {
            // Arrange
            var returnIds = new List<Guid> {
                Guid.Parse("ead0fcb7-bdfc-4106-893a-5c1c7a709aa1"),
                Guid.Parse("fb5cfa5d-71a9-48c7-af2a-d63e4f1d4be3"),
            };

            _baseInvoiceRepoMock
                .Setup(repo => repo.FindMultiple(MoqHelper.IsExpression<WmsBaseInvoice>(bi => returnIds.Contains(bi.ReturnId.Value))))
                .ReturnsAsync(new List<WmsBaseInvoice>());

            // Act
            var result = await _invoiceService.GetBaseInvoiceByReturnId(returnIds);

            // Assert
            Assert.NotNull(result);
            Assert.Empty(result);

        }

        [Fact]
        public async Task GetBranchInvoiceByReturnId_ShouldReturnOrderedInvoices()
        {
            // Arrange
            var returnIds = new List<Guid> {
                Guid.Parse("ead0fcb7-bdfc-4106-893a-5c1c7a709aa1"),
                Guid.Parse("fb5cfa5d-71a9-48c7-af2a-d63e4f1d4be3"),
            };

            var invoices = new List<WmsBranchInvoice> {
                new WmsBranchInvoice { ReturnId = Guid.Parse("ead0fcb7-bdfc-4106-893a-5c1c7a709aa1"), CreateDate = DateTime.UtcNow.AddDays(-2) },
                new WmsBranchInvoice { ReturnId = Guid.Parse("fb5cfa5d-71a9-48c7-af2a-d63e4f1d4be3"), CreateDate = DateTime.UtcNow.AddDays(-1) },
                new WmsBranchInvoice { ReturnId = Guid.Parse("6f17759a-8763-4272-9ba8-1d6dba5786e3"), CreateDate = DateTime.UtcNow }
            };

            invoices = invoices.Where(bi => returnIds.Contains(bi.ReturnId.Value)).ToList();
            _branchInvoiceRepoMock
                .Setup(repo => repo.FindMultiple(It.IsAny<Expression<Func<WmsBranchInvoice, bool>>>()))
                .ReturnsAsync(invoices);

            // Act
            var result = await _invoiceService.GetBranchInvoiceByReturnId(returnIds);

            // Assert
            Assert.NotNull(result);
            Assert.NotEmpty(result);
        }

        [Fact]
        public async Task GetBranchInvoiceByReturnId_ShouldReturnEmptyList()
        {
            // Arrange
            var returnIds = new List<Guid> {
                Guid.Parse("ead0fcb7-bdfc-4106-893a-5c1c7a709aa1"),
                Guid.Parse("fb5cfa5d-71a9-48c7-af2a-d63e4f1d4be3"),
            };

            _branchInvoiceRepoMock
                .Setup(repo => repo.FindMultiple(MoqHelper.IsExpression<WmsBranchInvoice>(bi => returnIds.Contains(bi.ReturnId.Value))))
                .ReturnsAsync(new List<WmsBranchInvoice>());

            // Act
            var result = await _invoiceService.GetBranchInvoiceByReturnId(returnIds);

            // Assert
            Assert.NotNull(result);
            Assert.Empty(result);

        }

        [Fact]
        public async Task GetBranchInvoiceByReturnId_ShouldReturnEmptyList_ReturnIdNull()
        {
            // Arrange
            List<Guid> returnIds = null;

            _branchInvoiceRepoMock
                .Setup(repo => repo.FindMultiple(MoqHelper.IsExpression<WmsBranchInvoice>(bi => returnIds.Contains(bi.ReturnId.Value))))
                .ReturnsAsync(new List<WmsBranchInvoice>());

            // Act
            var result = await _invoiceService.GetBranchInvoiceByReturnId(returnIds);

            // Assert
            Assert.NotNull(result);
            Assert.Empty(result);

        }

        [Fact]
        public async Task GetBaseInvoiceByBranchProductReturnId_ShouldReturnOrderedInvoices()
        {
            // Arrange
            var returnIds = new List<Guid> {
                Guid.Parse("ead0fcb7-bdfc-4106-893a-5c1c7a709aa1"),
                Guid.Parse("fb5cfa5d-71a9-48c7-af2a-d63e4f1d4be3"),
            };

            var invoices = new List<WmsBaseInvoice> {
                new WmsBaseInvoice { BranchProductReturnId = Guid.Parse("ead0fcb7-bdfc-4106-893a-5c1c7a709aa1"), CreateDate = DateTime.UtcNow.AddDays(-2) },
                new WmsBaseInvoice { BranchProductReturnId = Guid.Parse("fb5cfa5d-71a9-48c7-af2a-d63e4f1d4be3"), CreateDate = DateTime.UtcNow.AddDays(-1) },
                new WmsBaseInvoice { BranchProductReturnId = Guid.Parse("6f17759a-8763-4272-9ba8-1d6dba5786e3"), CreateDate = DateTime.UtcNow }
            };

            invoices = invoices.Where(bi => returnIds.Contains(bi.BranchProductReturnId.Value)).ToList();
            _baseInvoiceRepoMock
                .Setup(repo => repo.FindMultiple(It.IsAny<Expression<Func<WmsBaseInvoice, bool>>>()))
                .ReturnsAsync(invoices);

            // Act
            var result = await _invoiceService.GetBaseInvoiceByBranchProductReturnId(returnIds);

            // Assert
            Assert.NotNull(result);
            Assert.NotEmpty(result);
        }

        [Fact]
        public async Task GetBaseInvoiceByBranchProductReturnId_ShouldReturnEmptyList()
        {
            // Arrange
            var returnIds = new List<Guid> {
                Guid.Parse("ead0fcb7-bdfc-4106-893a-5c1c7a709aa1"),
                Guid.Parse("fb5cfa5d-71a9-48c7-af2a-d63e4f1d4be3"),
            };

            _baseInvoiceRepoMock
                .Setup(repo => repo.FindMultiple(MoqHelper.IsExpression<WmsBaseInvoice>(bi => returnIds.Contains(bi.BranchProductReturnId.Value))))
                .ReturnsAsync(new List<WmsBaseInvoice>());

            // Act
            var result = await _invoiceService.GetBaseInvoiceByBranchProductReturnId(returnIds);

            // Assert
            Assert.NotNull(result);
            Assert.Empty(result);

        }

        [Fact]
        public async Task GetBaseInvoiceByBranchProductReturnId_ShouldReturnEmptyList_ReturnIdNull()
        {
            // Arrange
            List<Guid> returnIds = null;

            _baseInvoiceRepoMock
                .Setup(repo => repo.FindMultiple(MoqHelper.IsExpression<WmsBaseInvoice>(bi => returnIds.Contains(bi.BranchProductReturnId.Value))))
                .ReturnsAsync(new List<WmsBaseInvoice>());

            // Act
            var result = await _invoiceService.GetBaseInvoiceByBranchProductReturnId(returnIds);

            // Assert
            Assert.NotNull(result);
            Assert.Empty(result);

        }

        //[Theory]
        //[InlineData("BaseWarehouseManager", 1, 0)]
        //public async Task ViewDashboard_Returns_BaseDashboard_For_BaseWarehouseManager(string currentUser, int fromAdd, int toAdd)
        //{
        //    DateTime from = DateTime.Now.AddYears(fromAdd);
        //    DateTime to = DateTime.Now.AddYears(toAdd);

        //    // Arrange
        //    _userRepoMock.Setup(u => u.GetUserRoleByUsername(currentUser))
        //        .ReturnsAsync(new[] { new WmsRole { RoleId = CommonConstant.BaseWarehouseManager } });

        //    // Act
        //    var result = await _invoiceService.ViewDashboard(null, null, currentUser);

        //    // Assert
        //    Assert.NotNull(result);
        //    //Assert.Equal(result.);
        //}

        //[Fact]
        //public async Task ViewDashboard_Returns_BranchDashboard_For_BranchWarehouseDirector()
        //{
        //    // Arrange
        //    var userRepositoryMock = new Mock<IUserRepository>();
        //    var unitOfWorkMock = new Mock<IUnitOfWork>();
        //    unitOfWorkMock.Setup(u => u.UserRepository).Returns(userRepositoryMock.Object);

        //    var dashboardService = new DashboardService(unitOfWorkMock.Object);

        //    var currentUser = "BranchWarehouseDirector";
        //    var from = DateTime.Now.AddYears(-1);
        //    var to = DateTime.Now;

        //    userRepositoryMock.Setup(u => u.GetUserRoleByUsername(currentUser))
        //        .ReturnsAsync(new[] { new UserRole { RoleId = CommonConstant.BranchWarehouseDirector } });

        //    // Act
        //    var result = await dashboardService.ViewDashboard(null, null, currentUser);

        //    // Assert
        //    Assert.NotNull(result);
        //    // Add more specific assertions based on the expected behavior of ViewBranchDashboard
        //}
    }
}
