﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using MockQueryable.Moq;
using Moq;
using MoqExpression;
using System.Data;
using System.Linq.Expressions;
using WMS.Business.Abstractions.Service;
using WMS.Business.Dtos.CommonDto;
using WMS.Business.Dtos.EmployeeDto;
using WMS.Business.Dtos.Mapper;
using WMS.Business.Dtos.S3Dto;
using WMS.Business.Dtos.UserDto;
using WMS.Business.Services;
using WMS.Domain.Abstractions;
using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Entities;
using WMS.UnitTest.DummiesData;

namespace WMS.UnitTest.Business
{
    public class EmployeeServiceTest
    {
        private readonly EmployeeService _employeeService;
        private readonly Mock<IUnitOfWork> _unitOfWork = new Mock<IUnitOfWork>();
        private readonly Mock<IWmsUserRepository> _userRepoMock = new Mock<IWmsUserRepository>();
        private readonly Mock<IWmsBaseWarehouseRepository> _baseWarehouseRepoMock = new Mock<IWmsBaseWarehouseRepository>();
        private readonly Mock<IWmsBranchWarehouseRepository> _branchWarehouseRepoMock = new Mock<IWmsBranchWarehouseRepository>();
        private readonly IMock<ILogger<EmployeeService>> _logger = new Mock<ILogger<EmployeeService>>();
        private readonly Mock<IUserService> _userServiceMock = new Mock<IUserService>();
        private readonly Mock<IS3StorageService> _storageService = new Mock<IS3StorageService>();
        private readonly IQueryable<WmsUser> wmsUsers = UserData.Data.BuildMock();
        public EmployeeServiceTest()
        {
            var mockTran = new Mock<IDbTransaction>();

            _unitOfWork.Setup(x => x.BeginTransaction()).Returns(mockTran.Object);
            _unitOfWork.Setup(x => x.UserRepository).Returns(_userRepoMock.Object);
            _unitOfWork.Setup(x => x.BaseWarehouseRepository).Returns(_baseWarehouseRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchWarehouseRepository).Returns(_branchWarehouseRepoMock.Object);

            var configuration = new MapperConfiguration(config =>
            {
                config.AddProfile<DomainToDtoProfile>();
                config.AddProfile<DtoToDomainProfile>();
            });
            var mapper = new Mapper(configuration);

            _employeeService = new EmployeeService(_unitOfWork.Object, _logger.Object, mapper, _userServiceMock.Object, _storageService.Object);
        }

        [Theory]
        [MemberData(nameof(ViewEmployeesListData_NormalCase))]
        public async Task ViewEmployeesList_NormalCase(ViewEmployeesListRequestDto entity, string currentUser)
        {
            //Arrange
            var user = wmsUsers.First();
            user.UserRole = new List<WmsUserRole>() { new WmsUserRole() { RoleId = 2 } };
            _userRepoMock.Setup(x => x.GetUserWithRoleByUsername(currentUser, 1))
                .Returns(new List<WmsUser>() { user }.BuildMock());
            _userRepoMock.Setup(x => x.SearchEmployeesList(entity.SearchWord, entity.Status, 2, currentUser)).Returns(wmsUsers);

            //Act
            var actualResult = await _employeeService.ViewEmployeesList(entity, currentUser).ConfigureAwait(false);

            //Assert
            var size = entity.PagingRequest.PageSize < wmsUsers.Count() ? entity.PagingRequest.PageSize : wmsUsers.Count();
            Assert.Equal(actualResult.Items.Count(), size <= 0 ? 10 : size);
            Assert.NotNull(actualResult.Items);
            Assert.True(actualResult.Pagination.StartPage <= actualResult.Pagination.EndPage);
        }

        public static IEnumerable<object[]> ViewEmployeesListData_NormalCase()
        {
            yield return new object[] {new ViewEmployeesListRequestDto()
            {
                SearchWord = "a", Status = 1, PagingRequest = new PagingRequest() {CurrentPage = 1, PageSize = 5, PageRange = 5},
            }, "nprettejohns0"};
            yield return new object[] {new ViewEmployeesListRequestDto()
            {
                SearchWord = null, Status = null, PagingRequest = new PagingRequest() {CurrentPage = 1, PageSize = 5, PageRange = 5},
            }, "nprettejohns0"};
            yield return new object[] {new ViewEmployeesListRequestDto()
            {
                SearchWord = null, Status = 1, PagingRequest = new PagingRequest() {CurrentPage = 1, PageSize = 5, PageRange = 5},
            }, "nprettejohns0"};
            yield return new object[] {new ViewEmployeesListRequestDto()
            {
                SearchWord = "a", Status = null, PagingRequest = new PagingRequest() {CurrentPage = 1, PageSize = 5, PageRange = 5},
            }, "nprettejohns0"};
        }

        [Theory]
        [MemberData(nameof(ViewEmployeesListData_AbnormalCase))]
        public async Task ViewEmployeesList_AbnormalCase(ViewEmployeesListRequestDto entity, string currentUser)
        {
            //Arrange
            var user = wmsUsers.First();
            user.UserRole = new List<WmsUserRole>() { new WmsUserRole() { RoleId = 2 } };
            _userRepoMock.Setup(x => x.GetUserWithRoleByUsername(currentUser, 1))
                .Returns(new List<WmsUser>() { user }.BuildMock());
            _userRepoMock.Setup(x => x.SearchEmployeesList(entity.SearchWord, entity.Status, 2, currentUser)).Returns(wmsUsers);

            //Act
            var actualResult = await _employeeService.ViewEmployeesList(entity, currentUser).ConfigureAwait(false);

            //Assert
            var size = entity.PagingRequest.PageSize < wmsUsers.Count() ? entity.PagingRequest.PageSize : wmsUsers.Count();
            Assert.Equal(actualResult.Items.Count(), size <= 0 ? 10 : size);
            Assert.NotNull(actualResult.Items);
            Assert.True(actualResult.Pagination.StartPage <= actualResult.Pagination.EndPage);
        }

        public static IEnumerable<object[]> ViewEmployeesListData_AbnormalCase()
        {
            yield return new object[] {new ViewEmployeesListRequestDto()
            {
                SearchWord = null, Status = null, PagingRequest = new PagingRequest() {CurrentPage = -1, PageSize = 5, PageRange = 5},
            }, "nprettejohns0"};
            yield return new object[] {new ViewEmployeesListRequestDto()
            {
                SearchWord = null, Status = null, PagingRequest = new PagingRequest() {CurrentPage = 1, PageSize = -1, PageRange = 5},
            }, "nprettejohns0"};
            yield return new object[] {new ViewEmployeesListRequestDto()
            {
                SearchWord = null, Status = null, PagingRequest = new PagingRequest() {CurrentPage = 1, PageSize = 5, PageRange = -1},
            }, "nprettejohns0"};
        }

        [Fact]
        public async Task ViewEmployeesList_UserHasNoRole()
        {
            //Arrange
            var entity = new ViewEmployeesListRequestDto()
            {
                SearchWord = null,
                Status = null,
                PagingRequest = new PagingRequest() { CurrentPage = 1, PageSize = 5, PageRange = 5 },
            };
            var currentUser = "abcxyz";
            var user = new WmsUser()
            {
                Username = currentUser,
                UserRole = null,
            };
            user.UserRole = null;
            _userRepoMock.Setup(x => x.GetUserWithRoleByUsername(currentUser, 1))
                .Returns(new List<WmsUser>() { user }.BuildMock());
            _userRepoMock.Setup(x => x.SearchEmployeesList(entity.SearchWord, entity.Status, 2, currentUser)).Returns(wmsUsers);

            //Act
            var actualResult = await _employeeService.ViewEmployeesList(entity, currentUser).ConfigureAwait(false);

            //Assert
            Assert.Null(actualResult);
        }

        [Fact]
        public async Task ViewEmployeeDetails_NormalCase()
        {
            //Arrange
            var baseWarehouse = new WmsBaseWarehouse()
            {
                WarehouseId = Guid.Parse("aa7778c5-00f0-47fd-b40d-0e4b7fbb2739"),
            };
            var user = UserData.Data.ElementAt(0);
            user.UserRole = new List<WmsUserRole>() { new WmsUserRole() { RoleId = 2 } };
            var employee = new WmsUser()
            {
                UserId = UserData.Data.ElementAt(1).UserId,
                UserRole = new List<WmsUserRole>() { new WmsUserRole() { RoleId = 3 } },
                WmsBaseWarehouseUsers = new List<WmsBaseWarehouseUser>() {
                    new WmsBaseWarehouseUser() { BaseWarehouseId = baseWarehouse.WarehouseId }
                },
            };

            _userRepoMock.Setup(x => x.GetUserWithWarehouseUsersByUserid(employee.UserId, null))
                .Returns(new List<WmsUser>() { employee }.BuildMock());
            _userRepoMock.Setup(x => x.GetUserWithRoleByUserid(employee.UserId, null))
                .Returns(new List<WmsUser>() { employee }.BuildMock());
            _userRepoMock.Setup(x => x.GetUserWithRoleByUsername(user.Username, 1))
                .Returns(new List<WmsUser>() { user }.BuildMock());
            _userRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsUser>(f => f.Username == "nprettejohns0")))
                .ReturnsAsync(user);
            _storageService.Setup(x => x.GetFileUrl(It.IsAny<S3RequestData>())).Returns("avatar");
            _baseWarehouseRepoMock.Setup(x => x
                .FindObject(MoqHelper.IsExpression<WmsBaseWarehouse>(f => f.WarehouseOwner.Equals(user.UserId))))
                .ReturnsAsync(baseWarehouse);
            //Act
            var actualResult = await _employeeService.ViewEmployeeDetails(employee.UserId, user.Username).ConfigureAwait(false);

            //Assert
            Assert.NotNull(actualResult);
            Assert.Equal(employee.UserId, actualResult.UserId);
            Assert.Equal("avatar", actualResult.AvatarImage);
        }

        [Fact]
        public async Task ViewEmployeeDetails_ManagerHasNoRole()
        {
            //Arrange
            var baseWarehouse = new WmsBaseWarehouse()
            {
                WarehouseId = Guid.Parse("aa7778c5-00f0-47fd-b40d-0e4b7fbb2739"),
            };
            var user = UserData.Data.ElementAt(0);
            user.UserRole = null;
            var employee = new WmsUser()
            {
                UserId = UserData.Data.ElementAt(1).UserId,
                UserRole = new List<WmsUserRole>() { new WmsUserRole() { RoleId = 3 } },
                WmsBaseWarehouseUsers = new List<WmsBaseWarehouseUser>() {
                    new WmsBaseWarehouseUser() { BaseWarehouseId = baseWarehouse.WarehouseId }
                },
            };

            _userRepoMock.Setup(x => x.GetUserWithWarehouseUsersByUserid(employee.UserId, null))
                .Returns(new List<WmsUser>() { employee }.BuildMock());
            _userRepoMock.Setup(x => x.GetUserWithRoleByUsername(user.Username, 1))
                .Returns(new List<WmsUser>() { user }.BuildMock());
            _userRepoMock.Setup(x => x.GetUserWithRoleByUserid(employee.UserId, null))
                .Returns(new List<WmsUser>() { employee }.BuildMock());
            _userRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsUser>(f => f.Username == "nprettejohns0")))
                .ReturnsAsync(user);
            _storageService.Setup(x => x.GetFileUrl(It.IsAny<S3RequestData>())).Returns("avatar");
            _baseWarehouseRepoMock.Setup(x => x
                .FindObject(MoqHelper.IsExpression<WmsBaseWarehouse>(f => f.WarehouseOwner.Equals(user.UserId))))
                .ReturnsAsync(baseWarehouse);
            //Act
            var actualResult = await _employeeService.ViewEmployeeDetails(employee.UserId, user.Username).ConfigureAwait(false);

            //Assert
            Assert.Null(actualResult);
        }

        [Fact]
        public async Task ViewEmployeeDetails_EmployeeHasNoRole()
        {
            //Arrange
            var baseWarehouse = new WmsBaseWarehouse()
            {
                WarehouseId = Guid.Parse("aa7778c5-00f0-47fd-b40d-0e4b7fbb2739"),
            };
            var user = UserData.Data.ElementAt(0);
            user.UserRole = new List<WmsUserRole>() { new WmsUserRole() { RoleId = 2 } };
            var employee = new WmsUser()
            {
                UserId = UserData.Data.ElementAt(1).UserId,
                UserRole = null,
                WmsBaseWarehouseUsers = new List<WmsBaseWarehouseUser>() {
                    new WmsBaseWarehouseUser() { BaseWarehouseId = baseWarehouse.WarehouseId }
                },
            };

            _userRepoMock.Setup(x => x.GetUserWithWarehouseUsersByUserid(employee.UserId, null))
                .Returns(new List<WmsUser>() { employee }.BuildMock());
            _userRepoMock.Setup(x => x.GetUserWithRoleByUsername(user.Username, 1))
                .Returns(new List<WmsUser>() { user }.BuildMock());
            _userRepoMock.Setup(x => x.GetUserWithRoleByUserid(employee.UserId, null))
                .Returns(new List<WmsUser>() { employee }.BuildMock());
            _userRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsUser>(f => f.Username == "nprettejohns0")))
                .ReturnsAsync(user);
            _storageService.Setup(x => x.GetFileUrl(It.IsAny<S3RequestData>())).Returns("avatar");
            _baseWarehouseRepoMock.Setup(x => x
                .FindObject(MoqHelper.IsExpression<WmsBaseWarehouse>(f => f.WarehouseOwner.Equals(user.UserId))))
                .ReturnsAsync(baseWarehouse);
            //Act
            var actualResult = await _employeeService.ViewEmployeeDetails(employee.UserId, user.Username).ConfigureAwait(false);

            //Assert
            Assert.Null(actualResult);
        }

        [Fact]
        public async Task ViewEmployeeDetails_EmployeeNotInBranchWarehouse()
        {
            //Arrange
            var warehouse = new WmsBranchWarehouse()
            {
                WarehouseId = Guid.Parse("aa7778c5-00f0-47fd-b40d-0e4b7fbb2739"),
            };
            var user = UserData.Data.ElementAt(0);
            user.UserRole = new List<WmsUserRole>() { new WmsUserRole() { RoleId = 4 } };
            var employee = new WmsUser()
            {
                UserId = UserData.Data.ElementAt(1).UserId,
                UserRole = new List<WmsUserRole>() { new WmsUserRole() { RoleId = 5 } },
                WmsBranchWarehouseUsers = null,
            };

            _userRepoMock.Setup(x => x.GetUserWithWarehouseUsersByUserid(employee.UserId, null))
                .Returns(new List<WmsUser>() { employee }.BuildMock());
            _userRepoMock.Setup(x => x.GetUserWithRoleByUsername(user.Username, 1))
                .Returns(new List<WmsUser>() { user }.BuildMock());
            _userRepoMock.Setup(x => x.GetUserWithRoleByUserid(employee.UserId, null))
                .Returns(new List<WmsUser>() { employee }.BuildMock());
            _userRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsUser>(f => f.Username == "nprettejohns0")))
                .ReturnsAsync(user);
            _branchWarehouseRepoMock.Setup(x => x
                .FindObject(It.IsAny<Expression<Func<WmsBranchWarehouse, bool>>>()))
                .ReturnsAsync(warehouse);
            //Act
            var actualResult = await _employeeService.ViewEmployeeDetails(employee.UserId, user.Username).ConfigureAwait(false);

            //Assert
            Assert.Null(actualResult);
        }

        private CreateUserAccountRequestDto createAccountRequestDto = new CreateUserAccountRequestDto()
        {
            Username = UserData.Data.First().Username,
            Fullname = UserData.Data.First().Fullname,
            Gender = UserData.Data.First().Gender,
            Email = UserData.Data.First().Email,
            Address = UserData.Data.First().Address,
            PhoneNumber = UserData.Data.First().PhoneNumber,
            Birthdate = UserData.Data.First().Birthdate,
            Supervisor = UserData.Data.First().Supervisor,
            JoinDate = UserData.Data.First().JoinDate,
            AvatarImage = null,
            RoleId = 5,
            WarehouseId = Guid.Empty,
        };

        [Fact]
        public async Task CreateEmployee_NormalCase()
        {
            //Arrange
            var entity = new CreateEmployeeRequestDto() { CreateUser = createAccountRequestDto, };
            var warehouse = new WmsBranchWarehouse()
            {
                WarehouseId = Guid.Parse("aa7778c5-00f0-47fd-b40d-0e4b7fbb2739"),
            };
            var user = UserData.Data.ElementAt(0);
            user.UserRole = new List<WmsUserRole>() { new WmsUserRole() { RoleId = 4 } };

            _userRepoMock.Setup(x => x.GetUserWithRoleByUsername(user.Username, 1))
                .Returns(new List<WmsUser>() { user }.BuildMock());
            _userRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsUser>(f => f.Username == "nprettejohns0")))
                .ReturnsAsync(user);
            _branchWarehouseRepoMock.Setup(x => x
                .FindObject(It.IsAny<Expression<Func<WmsBranchWarehouse, bool>>>()))
                .ReturnsAsync(warehouse);
            _userServiceMock.Setup(x => x.CreateAccount(entity.CreateUser))
                .ReturnsAsync(UserData.Data.First().UserId);
            //Act
            var actualResult = await _employeeService.CreateEmployee(entity, user.Username).ConfigureAwait(false);

            //Assert
            Assert.True(actualResult);
        }

        [Fact]
        public async Task CreateEmployee_InvalidEmployeeRole()
        {
            //Arrange
            var entity = new CreateEmployeeRequestDto() { CreateUser = createAccountRequestDto, };
            entity.CreateUser.RoleId = 1;
            var warehouse = new WmsBranchWarehouse()
            {
                WarehouseId = Guid.Parse("aa7778c5-00f0-47fd-b40d-0e4b7fbb2739"),
            };
            var user = UserData.Data.ElementAt(0);
            user.UserRole = new List<WmsUserRole>() { new WmsUserRole() { RoleId = 4 } };

            _userRepoMock.Setup(x => x.GetUserWithRoleByUsername(user.Username, 1))
                .Returns(new List<WmsUser>() { user }.BuildMock());
            _userRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsUser>(f => f.Username == "nprettejohns0")))
                .ReturnsAsync(user);
            _branchWarehouseRepoMock.Setup(x => x
                .FindObject(It.IsAny<Expression<Func<WmsBranchWarehouse, bool>>>()))
                .ReturnsAsync(warehouse);
            _userServiceMock.Setup(x => x.CreateAccount(entity.CreateUser))
                .ReturnsAsync(UserData.Data.First().UserId);
            //Act
            var actualResult = await _employeeService.CreateEmployee(entity, user.Username).ConfigureAwait(false);

            //Assert
            Assert.False(actualResult);
        }

        [Fact]
        public async Task CreateEmployee_InvalidManagerRole()
        {
            //Arrange
            var entity = new CreateEmployeeRequestDto() { CreateUser = createAccountRequestDto, };
            entity.CreateUser.RoleId = 3;
            var warehouse = new WmsBranchWarehouse()
            {
                WarehouseId = Guid.Parse("aa7778c5-00f0-47fd-b40d-0e4b7fbb2739"),
            };
            var user = UserData.Data.ElementAt(0);
            user.UserRole = new List<WmsUserRole>() { new WmsUserRole() { RoleId = 4 } };

            _userRepoMock.Setup(x => x.GetUserWithRoleByUsername(user.Username, 1))
                .Returns(new List<WmsUser>() { user }.BuildMock());
            _userRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsUser>(f => f.Username == "nprettejohns0")))
                .ReturnsAsync(user);
            _branchWarehouseRepoMock.Setup(x => x
                .FindObject(It.IsAny<Expression<Func<WmsBranchWarehouse, bool>>>()))
                .ReturnsAsync(warehouse);
            _userServiceMock.Setup(x => x.CreateAccount(entity.CreateUser))
                .ReturnsAsync(UserData.Data.First().UserId);
            //Act
            var actualResult = await _employeeService.CreateEmployee(entity, user.Username).ConfigureAwait(false);

            //Assert
            Assert.False(actualResult);
        }

        [Fact]
        public async Task UpdateEmployee_NormalCase()
        {
            //Arrange
            var warehouse = new WmsBranchWarehouse()
            {
                WarehouseId = Guid.Parse("aa7778c5-00f0-47fd-b40d-0e4b7fbb2739"),
            };
            var user = UserData.Data.ElementAt(0);
            user.UserRole = new List<WmsUserRole>() { new WmsUserRole() { RoleId = 4 } };

            var employee = new WmsUser()
            {
                UserId = UserData.Data.ElementAt(1).UserId,
                UserRole = new List<WmsUserRole>() { new WmsUserRole() { RoleId = 5 } },
                WmsBranchWarehouseUsers = new List<WmsBranchWarehouseUser>() {
                    new WmsBranchWarehouseUser() { BranchWarehouseId = warehouse.WarehouseId }
                },
            };

            _userRepoMock.Setup(x => x.GetUserWithRoleByUsername(user.Username, 1))
                .Returns(new List<WmsUser>() { user }.BuildMock());
            _userRepoMock.Setup(x => x.GetUserWithWarehouseUsersByUserid(employee.UserId, null))
                .Returns(new List<WmsUser>() { employee }.BuildMock());
            _userRepoMock.Setup(x => x.GetUserWithRoleByUserid(employee.UserId, null))
                .Returns(new List<WmsUser>() { employee }.BuildMock());
            _userRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsUser>(f => f.Username == "nprettejohns0")))
                .ReturnsAsync(user);
            _branchWarehouseRepoMock.Setup(x => x
                .FindObject(It.IsAny<Expression<Func<WmsBranchWarehouse, bool>>>()))
                .ReturnsAsync(warehouse);

            var entity = new UpdateEmployeeStatusRequestDto() { Status = 1, EmployeeId = employee.UserId };

            //Act
            var actualResult = await _employeeService.UpdateEmployeeStatus(entity, user.Username).ConfigureAwait(false);

            //Assert
            Assert.True(actualResult);
            _userRepoMock.Verify(x => x.Update(It.IsAny<WmsUser>()), Times.Once);
        }

        [Fact]
        public async Task UpdateEmployee_InvalidStatus()
        {
            //Arrange
            var user = UserData.Data.ElementAt(0);
            user.UserRole = new List<WmsUserRole>() { new WmsUserRole() { RoleId = 4 } };
            var employee = new WmsUser()
            {
                UserId = UserData.Data.ElementAt(1).UserId,
                UserRole = new List<WmsUserRole>() { new WmsUserRole() { RoleId = 5 } },
            };
            var entity = new UpdateEmployeeStatusRequestDto() { Status = 1, EmployeeId = employee.UserId };

            //Act
            var actualResult = await _employeeService.UpdateEmployeeStatus(entity, user.Username).ConfigureAwait(false);

            //Assert
            Assert.False(actualResult);
        }

        [Fact]
        public async Task UpdateEmployee_InvalidManagerRole()
        {
            //Arrange
            var warehouse = new WmsBranchWarehouse()
            {
                WarehouseId = Guid.Parse("aa7778c5-00f0-47fd-b40d-0e4b7fbb2739"),
            };
            var user = UserData.Data.ElementAt(0);
            user.UserRole = new List<WmsUserRole>() { new WmsUserRole() { RoleId = 2 } };

            var employee = new WmsUser()
            {
                UserId = UserData.Data.ElementAt(1).UserId,
                UserRole = new List<WmsUserRole>() { new WmsUserRole() { RoleId = 5 } },
                WmsBranchWarehouseUsers = new List<WmsBranchWarehouseUser>() {
                    new WmsBranchWarehouseUser() { BranchWarehouseId = warehouse.WarehouseId }
                },
            };

            _userRepoMock.Setup(x => x.GetUserWithRoleByUsername(user.Username, 1))
                .Returns(new List<WmsUser>() { user }.BuildMock());
            _userRepoMock.Setup(x => x.GetUserWithWarehouseUsersByUserid(employee.UserId, null))
                .Returns(new List<WmsUser>() { employee }.BuildMock());
            _userRepoMock.Setup(x => x.GetUserWithRoleByUserid(employee.UserId, null))
                .Returns(new List<WmsUser>() { employee }.BuildMock());
            _userRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsUser>(f => f.Username == "nprettejohns0")))
                .ReturnsAsync(user);
            _branchWarehouseRepoMock.Setup(x => x
                .FindObject(It.IsAny<Expression<Func<WmsBranchWarehouse, bool>>>()))
                .ReturnsAsync(warehouse);

            var entity = new UpdateEmployeeStatusRequestDto() { Status = 1, EmployeeId = employee.UserId };

            //Act
            var actualResult = await _employeeService.UpdateEmployeeStatus(entity, user.Username).ConfigureAwait(false);

            //Assert
            Assert.False(actualResult);
            _userRepoMock.Verify(x => x.Update(It.IsAny<WmsUser>()), Times.Never);
        }

        [Theory]
        [InlineData("nprettejohns0", 2)]
        [InlineData("ggurlingj", 4)]
        [InlineData("", 0)]
        [InlineData(null, 0)]
        public async Task CheckManagerType(string currentUser, int rs)
        {
            //Arrange
            var user1 = wmsUsers.First();
            var user2 = wmsUsers.Last();
            user1.UserRole = new List<WmsUserRole>() { new WmsUserRole() { RoleId = 2 } };
            user2.UserRole = new List<WmsUserRole>() { new WmsUserRole() { RoleId = 4 } };
            _userRepoMock.Setup(x => x.GetUserWithRoleByUsername("nprettejohns0", 1))
                .Returns(new List<WmsUser>() { user1 }.BuildMock());
            _userRepoMock.Setup(x => x.GetUserWithRoleByUsername("ggurlingj", 1))
                .Returns(new List<WmsUser>() { user2 }.BuildMock());

            //Act
            var result = await _employeeService.CheckManagerType(currentUser).ConfigureAwait(false);

            //Assert
            Assert.Equal(rs, result);
        }

        [Theory]
        [MemberData(nameof(CheckEmployeeInManagerWarehouseData))]
        public async Task CheckEmployeeInManagerWarehouse(int managerType, Guid managerId, WmsUser userWithRole, bool result)
        {
            //Arrange
            _userRepoMock.Setup(x => x.GetUserWithWarehouseUsersByUserid(userWithRole.UserId, null))
                .Returns(new List<WmsUser>() { userWithRole }.BuildMock());
            _branchWarehouseRepoMock.Setup(x => x
                .FindObject(It.IsAny<Expression<Func<WmsBranchWarehouse, bool>>>()))
                .ReturnsAsync(new WmsBranchWarehouse() { WarehouseId = BranchWarehouseData.Data.First().WarehouseId });
            _baseWarehouseRepoMock.Setup(x => x
                .FindObject(It.IsAny<Expression<Func<WmsBaseWarehouse, bool>>>()))
                .ReturnsAsync(new WmsBaseWarehouse() { WarehouseId = BranchWarehouseData.Data.First().WarehouseId });

            //Act
            var rs = await _employeeService.CheckEmployeeInManagerWarehouse(managerType, managerId, userWithRole);

            //Assert
            Assert.Equal(result, rs);
        }

        public static IEnumerable<object[]> CheckEmployeeInManagerWarehouseData()
        {
            yield return new object[] { 2, UserData.Data.First().UserId,
                new WmsUser() { UserRole = new List<WmsUserRole>() { new WmsUserRole() { RoleId = 3 } },
                UserId = Guid.Parse("6c9c6250-b514-4c90-b343-55b08054c0f9"),
                WmsBaseWarehouseUsers = new List<WmsBaseWarehouseUser>(){new WmsBaseWarehouseUser(){ BaseWarehouseId = BranchWarehouseData.Data.First().WarehouseId}} }, true };
            yield return new object[] { 4, UserData.Data.Last().UserId,
                new WmsUser() { UserRole = new List<WmsUserRole>() { new WmsUserRole() { RoleId = 5 } },
                UserId = Guid.Parse("457b3d74-6d79-436b-a958-08d7f713bcd0"),
                WmsBranchWarehouseUsers = new List<WmsBranchWarehouseUser>(){new WmsBranchWarehouseUser(){ BranchWarehouseId = BranchWarehouseData.Data.First().WarehouseId}} }, true };
            yield return new object[] { 2, UserData.Data.Last().UserId,
                new WmsUser() { UserRole = new List<WmsUserRole>() { new WmsUserRole() { RoleId = 5 } },
                UserId = Guid.Parse("457b3d74-6d79-436b-a958-08d7f713bcd0"),
                WmsBranchWarehouseUsers = new List<WmsBranchWarehouseUser>(){new WmsBranchWarehouseUser(){ BranchWarehouseId = BranchWarehouseData.Data.First().WarehouseId}} }, false };
            yield return new object[] { 4, UserData.Data.Last().UserId,
                new WmsUser() { UserRole = new List<WmsUserRole>() { new WmsUserRole() { RoleId = 3 } },
                UserId = Guid.Parse("457b3d74-6d79-436b-a958-08d7f713bcd0"),
                WmsBaseWarehouseUsers = new List<WmsBaseWarehouseUser>(){new WmsBaseWarehouseUser(){ BaseWarehouseId = BranchWarehouseData.Data.First().WarehouseId}} }, false };
            yield return new object[] { 4, UserData.Data.Last().UserId,
                new WmsUser() { UserRole = new List<WmsUserRole>() { new WmsUserRole() { RoleId = 5 } },
                UserId = Guid.Parse("457b3d74-6d79-436b-a958-08d7f713bcd0"),
                WmsBranchWarehouseUsers = new List<WmsBranchWarehouseUser>(){new WmsBranchWarehouseUser(){ BranchWarehouseId = BranchWarehouseData.Data.Last().WarehouseId}} }, false };
        }

        [Fact]
        public async Task GetAllBaseEmployee_ReturnsListOfEmployees()
        {
            // Arrange
            var baseEmployeeQueryable = UserData.Data.AsQueryable().BuildMockDbSet();
            _userRepoMock.Setup(repo => repo.GetAllBaseEmployee()).Returns(baseEmployeeQueryable.Object);

            // Act
            var result = await _employeeService.GetAllBaseEmployee();

            // Assert
            Assert.NotNull(result);
            Assert.IsAssignableFrom<IEnumerable<ViewEmployeesListResponseDto>>(result);
            Assert.Equal(UserData.Data.Count(), result.Count());
        }

        [Theory]
        [InlineData("nprettejohns0")]
        public async Task GetAllBranchEmployee_ValidUser_ReturnsEmployeesList(string username)
        {
            // Arrange
            var user = UserData.Data.FirstOrDefault(x => x.Username == username);
            _userServiceMock.Setup(x => x.FindUserByUsername(username)).ReturnsAsync(user);

            // Mocking unit of work to return a list of employees
            var employeeList = UserData.Data.AsQueryable().BuildMockDbSet();
            _unitOfWork.Setup(x => x.UserRepository.GetAllBranchEmployee(It.IsAny<Guid>())).Returns(employeeList.Object);

            // Act
            var result = await _employeeService.GetAllBranchEmployee(username);

            // Assert
            Assert.NotNull(result);
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public async Task GetAllBranchEmployee_ValidUser_ReturnsNull(string username)
        {
            // Arrange
            var user = UserData.Data.FirstOrDefault(x => x.Username == username);
            _userServiceMock.Setup(x => x.FindUserByUsername(username)).ReturnsAsync(user);

            // Mocking unit of work to return a list of employees
            var employeeList = UserData.Data.AsQueryable().BuildMockDbSet();
            _unitOfWork.Setup(x => x.UserRepository.GetAllBranchEmployee(It.IsAny<Guid>())).Returns(employeeList.Object);

            // Act
            var result = await _employeeService.GetAllBranchEmployee(username);

            // Assert
            Assert.Null(result);
        }
    }
}
