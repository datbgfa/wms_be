﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using MockQueryable.Moq;
using Moq;
using System.Data;
using System.Linq.Expressions;
using WMS.Business.Dtos.BranchWarehouseDto;
using WMS.Business.Dtos.CommonDto;
using WMS.Business.Dtos.Mapper;
using WMS.Business.Services;
using WMS.Domain.Abstractions;
using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Entities;
using WMS.UnitTest.DummiesData;
namespace WMS.UnitTest.Business
{
    public class BranchWarehouseServiceTest
    {
        private readonly BranchWarehouseService _branchWarehouseService;
        private readonly Mock<IUnitOfWork> _unitOfWork = new Mock<IUnitOfWork>();
        private readonly Mock<IWmsUserRepository> _userRepoMock = new Mock<IWmsUserRepository>();
        private readonly Mock<IWmsBranchWarehouseRepository> _branchWarehouseRepoMock = new Mock<IWmsBranchWarehouseRepository>();
        private readonly Mock<IWmsBaseWarehouseRepository> _baseWarehouseRepoMock = new Mock<IWmsBaseWarehouseRepository>();
        private readonly Mock<IWmsBranchWarehouseUserRepository> _branchWarehouseUserRepoMock = new Mock<IWmsBranchWarehouseUserRepository>();
        private readonly Mock<IWmsBranchDebtRepository> _branchDebtRepoMock = new Mock<IWmsBranchDebtRepository>();
        private readonly Mock<IWmsBranchInvoiceRepository> _branchInvoiceRepoMock = new Mock<IWmsBranchInvoiceRepository>();
        private readonly Mock<IWmsBaseInvoiceRepository> _baseInvoiceRepoMock = new Mock<IWmsBaseInvoiceRepository>();
        private readonly Mock<IWmsBaseExportRepository> _baseExportRepoMock = new Mock<IWmsBaseExportRepository>();
        private readonly IMock<ILogger<BranchWarehouseService>> _logger = new Mock<ILogger<BranchWarehouseService>>();
        private IQueryable<WmsBranchWarehouse> wmsBranches = BranchWarehouseData.Data.AsAsyncQueryable();

        public BranchWarehouseServiceTest()
        {
            var mockTran = new Mock<IDbTransaction>();

            _unitOfWork.Setup(x => x.BeginTransaction()).Returns(mockTran.Object);
            _unitOfWork.Setup(x => x.UserRepository).Returns(_userRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchWarehouseRepository).Returns(_branchWarehouseRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchWarehouseUserRepository).Returns(_branchWarehouseUserRepoMock.Object);
            _unitOfWork.Setup(x => x.BaseWarehouseRepository).Returns(_baseWarehouseRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchDebtRepository).Returns(_branchDebtRepoMock.Object);
            _unitOfWork.Setup(x => x.BranchInvoiceRepository).Returns(_branchInvoiceRepoMock.Object);
            _unitOfWork.Setup(x => x.BaseInvoiceRepository).Returns(_baseInvoiceRepoMock.Object);
            _unitOfWork.Setup(x => x.BaseExportRepository).Returns(_baseExportRepoMock.Object);

            var configuration = new MapperConfiguration(config =>
            {
                config.AddProfile<DomainToDtoProfile>();
                config.AddProfile<DtoToDomainProfile>();
            });
            var mapper = new Mapper(configuration);

            _branchWarehouseService = new BranchWarehouseService(_unitOfWork.Object, _logger.Object, mapper);
        }

        [Theory]
        [MemberData(nameof(UpdateBranchWarehouseDetailsData))]
        public async Task UpdateBranchWarehouseDetails(UpdateBranchWarehouseDetailsRequestDto entity, bool result)
        {
            //Arrange
            var branch = BranchWarehouseData.Data.FirstOrDefault();
            var oldStatus = branch.Status;
            var oldNote = branch.Note;
            _branchWarehouseRepoMock.Setup(x => x.GetById(branch.WarehouseId)).ReturnsAsync(branch);
            _userRepoMock.Setup(x => x.GetAllBranchUser(It.IsAny<Guid>())).Returns(UserData.Data.BuildMock());

            //Act
            var actualResult = await _branchWarehouseService.UpdateBranchWarehouseDetails(entity).ConfigureAwait(false);

            //Assert
            Assert.Equal(result, actualResult);
            if (result)
            {
                _branchWarehouseRepoMock.Verify(r => r.Update(branch), Times.Once);
                Assert.Equal(entity.Status, branch.Status);
                if (entity.Status != oldStatus) Assert.NotEqual(oldStatus, branch.Status);
                Assert.Equal(entity.Note, branch.Note);
                if (entity.Note != oldNote) Assert.NotEqual(oldNote, branch.Note);
            }
            else
            {
                _branchWarehouseRepoMock.Verify(r => r.Update(branch), Times.Never);
            }
        }

        public static IEnumerable<object[]> UpdateBranchWarehouseDetailsData()
        {
            yield return new object[] {new UpdateBranchWarehouseDetailsRequestDto()
            {
                WarehouseId = BranchWarehouseData.Data.First().WarehouseId, Note = "", Status = 1
            }, true};
            yield return new object[] {new UpdateBranchWarehouseDetailsRequestDto()
            {
                WarehouseId = Guid.Empty, Note = "", Status = 1
            }, false};
            yield return new object[] {new UpdateBranchWarehouseDetailsRequestDto()
            {
                WarehouseId = BranchWarehouseData.Data.First().WarehouseId, Note = "", Status = 3
            }, false};
        }

        [Theory]
        [MemberData(nameof(ViewBranchWarehouseDetailsData))]
        public async Task ViewBranchWarehouseDetails(Guid warehouseId, bool result)
        {
            //Arrange 
            var branch = BranchWarehouseData.Data.FirstOrDefault();
            var user = UserData.Data.FirstOrDefault(x => x.UserId.Equals(branch.WarehouseOwner));
            _userRepoMock.Setup(x => x.FindObject(x => x.UserId.Equals(branch.WarehouseOwner))).ReturnsAsync(user);
            _branchWarehouseRepoMock.Setup(x => x.GetById(branch.WarehouseId)).ReturnsAsync(branch);
            _branchDebtRepoMock.Setup(x => x.FindObject(It.IsAny<Expression<Func<WmsBranchDebt, bool>>>())).ReturnsAsync(new WmsBranchDebt());
            _branchInvoiceRepoMock.Setup(x => x.FindMultiple(It.IsAny<Expression<Func<WmsBranchInvoice, bool>>>())).ReturnsAsync(new List<WmsBranchInvoice>()); 
            _baseInvoiceRepoMock.Setup(x => x.FindMultiple(It.IsAny<Expression<Func<WmsBaseInvoice, bool>>>())).ReturnsAsync(new List<WmsBaseInvoice>()); 
            _baseExportRepoMock.Setup(x => x.FindMultiple(It.IsAny<Expression<Func<WmsBaseExport, bool>>>())).ReturnsAsync(new List<WmsBaseExport>()); ;

            //Act
            var actualResult = await _branchWarehouseService.ViewBranchWarehouseDetails(warehouseId).ConfigureAwait(false);

            //Assert
            if (result) Assert.NotNull(actualResult);
            else Assert.Null(actualResult);

        }

        public static IEnumerable<object[]> ViewBranchWarehouseDetailsData()
        {
            yield return new object[] { BranchWarehouseData.Data.First().WarehouseId, true };
            yield return new object[] { Guid.Empty, false };
        }

        [Theory]
        [MemberData(nameof(CreateBranchWarehouseData))]
        public async Task CreateBranchWarehouse(CreateBranchWarehouseRequestDto entity, bool result)
        {
            //Arrange
            _baseWarehouseRepoMock.Setup(x => x.All()).ReturnsAsync(new List<WmsBaseWarehouse>() { new WmsBaseWarehouse() { WarehouseId = BranchWarehouseData.Data.First().WarehouseId } });

            //Act
            var actualResult = await _branchWarehouseService.CreateBranchWarehouse(entity).ConfigureAwait(false);

            //Assert
            Assert.Equal(result, actualResult);
            if (result)
            {
                _branchWarehouseRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBranchWarehouse>()), Times.Once);
                _branchDebtRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBranchDebt>()), Times.Once);
            }
            else
            {
                _branchWarehouseRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBranchWarehouse>()), Times.Never);
                _branchDebtRepoMock.Verify(x => x.AddAsync(It.IsAny<WmsBranchDebt>()), Times.Never);
            }
        }

        public static IEnumerable<object[]> CreateBranchWarehouseData()
        {
            yield return new object[] { new CreateBranchWarehouseRequestDto
            {
                PhoneNumber = "090000000", Address = "", Email = "abc@gmail.com",
                Note = "", Status = 1, WarehouseName = "Warehouse1"
            }, true };
            yield return new object[] { new CreateBranchWarehouseRequestDto
            {
                PhoneNumber = "090000000", Address = "", Email = "abc@gmail.com",
                Note = "", Status = 3, WarehouseName = "Warehouse1"
            }, false };
        }


        [Theory]
        [MemberData(nameof(ViewBranchWarehousesListData_NormalCase))]
        public async Task ViewBranchWarehousesList_NormalCase(ViewBranchWarehousesListRequestDto entity)
        {
            //Arrange
            if (entity.SearchWord != null && entity.Status != null)
            {
                wmsBranches = wmsBranches.Where(x => x.WarehouseName.ToLower().Contains(entity.SearchWord) && x.Status == entity.Status);
            }
            else if (entity.SearchWord == null && entity.Status != null)
            {
                wmsBranches = wmsBranches.Where(x => x.Status == entity.Status);
            }
            else if (entity.Status == null && entity.SearchWord != null)
            {
                wmsBranches = wmsBranches.Where(x => x.WarehouseName.ToLower().Contains(entity.SearchWord));
            }
            _branchWarehouseRepoMock.Setup(x => x.SearchBranchWarehousesList(entity.SearchWord, entity.Status)).Returns(wmsBranches);

            //Act
            var actualResult = await _branchWarehouseService.ViewBranchWarehouseList(entity).ConfigureAwait(false);

            //Assert
            var size = entity.PagingRequest.PageSize < wmsBranches.Count() ? entity.PagingRequest.PageSize : wmsBranches.Count();
            Assert.Equal(actualResult.Items.Count(), size <= 0 ? 10 : size);
            Assert.NotNull(actualResult.Items);
            if (entity.Status != null) Assert.True(entity.Status == actualResult.Items.First().Status);
            if (entity.SearchWord != null) Assert.Contains(entity.SearchWord.ToLower(), actualResult.Items.First().WarehouseName.ToLower());
            Assert.True(actualResult.Pagination.StartPage <= actualResult.Pagination.EndPage);
        }

        public static IEnumerable<object[]> ViewBranchWarehousesListData_NormalCase()
        {
            yield return new object[] {new ViewBranchWarehousesListRequestDto()
            {
                SearchWord = "kho ha noi", Status = 1, PagingRequest = new PagingRequest() {CurrentPage = 1, PageSize = 5, PageRange = 5},
            }};
            yield return new object[] {new ViewBranchWarehousesListRequestDto()
            {
                SearchWord = null, Status = null, PagingRequest = new PagingRequest() {CurrentPage = 1, PageSize = 5, PageRange = 5},
            }};
            yield return new object[] {new ViewBranchWarehousesListRequestDto()
            {
                SearchWord = null, Status = 1, PagingRequest = new PagingRequest() {CurrentPage = 1, PageSize = 5, PageRange = 5},
            }};
            yield return new object[] {new ViewBranchWarehousesListRequestDto()
            {
                SearchWord = "kho ha noi", Status = null, PagingRequest = new PagingRequest() {CurrentPage = 1, PageSize = 5, PageRange = 5},
            }};
        }

        [Theory]
        [MemberData(nameof(ViewBranchWarehousesListData_AbnormalCase))]
        public async Task ViewBranchWarehousesList_AbnormalCase(ViewBranchWarehousesListRequestDto entity)
        {
            //Arrange
            _branchWarehouseRepoMock.Setup(x => x.SearchBranchWarehousesList(entity.SearchWord, entity.Status)).Returns(wmsBranches);

            //Act
            var actualResult = await _branchWarehouseService.ViewBranchWarehouseList(entity).ConfigureAwait(false);

            //Assert
            var size = entity.PagingRequest.PageSize < wmsBranches.Count() ? entity.PagingRequest.PageSize : wmsBranches.Count();
            Assert.Equal(actualResult.Items.Count(), size <= 0 ? 10 : size);
            Assert.NotNull(actualResult.Items);
            if (entity.Status != null) Assert.True(entity.Status == actualResult.Items.First().Status);
            if (entity.SearchWord != null) Assert.Contains(entity.SearchWord.ToLower(), actualResult.Items.First().WarehouseName.ToLower());
            Assert.True(actualResult.Pagination.StartPage <= actualResult.Pagination.EndPage);
        }

        public static IEnumerable<object[]> ViewBranchWarehousesListData_AbnormalCase()
        {
            yield return new object[] {new ViewBranchWarehousesListRequestDto()
            {
                SearchWord = null, Status = null, PagingRequest = new PagingRequest() {CurrentPage = -1, PageSize = 5, PageRange = 5},
            }};
            yield return new object[] {new ViewBranchWarehousesListRequestDto()
            {
                SearchWord = null, Status = null, PagingRequest = new PagingRequest() {CurrentPage = 1, PageSize = -1, PageRange = 5},
            }};
            yield return new object[] {new ViewBranchWarehousesListRequestDto()
            {
                SearchWord = null, Status = null, PagingRequest = new PagingRequest() {CurrentPage = 1, PageSize = 5, PageRange = -1},
            }};
        }

        [Fact]
        public async Task ViewAllBranchWarehousesList_HasOwner()
        {
            //Arrange
            wmsBranches = wmsBranches.Where(x => x.WarehouseOwner != null);
            foreach (var branch in wmsBranches)
            {
                branch.WmsWarehouseOwner = UserData.Data.First();
            }
            _branchWarehouseRepoMock.Setup(x => x.GetBranchWarehousesIncludingOwner(true)).Returns(wmsBranches);

            //Act
            var rs = await _branchWarehouseService.ViewAllBranchWarehousesList(true).ConfigureAwait(false);

            //Assert
            Assert.NotNull(rs);
            Assert.NotEmpty(rs.First().WarehouseOwnerName);
        }

        [Fact]
        public async Task ViewAllBranchWarehousesList_NoOwner()
        {
            //Arrange
            wmsBranches = wmsBranches.Where(x => x.WarehouseOwner == null);
            _branchWarehouseRepoMock.Setup(x => x.GetBranchWarehousesIncludingOwner(false)).Returns(wmsBranches);

            //Act
            var rs = await _branchWarehouseService.ViewAllBranchWarehousesList(false).ConfigureAwait(false);

            //Assert
            Assert.NotNull(rs);
            Assert.Empty(rs.First().WarehouseOwnerName);
        }

        [Fact]
        public async Task ViewAllBranchWarehousesList_BothHasOwnerAndNoOwner()
        {
            //Arrange
            _branchWarehouseRepoMock.Setup(x => x.GetBranchWarehousesIncludingOwner(null)).Returns(wmsBranches);

            //Act
            var rs = await _branchWarehouseService.ViewAllBranchWarehousesList(null).ConfigureAwait(false);

            //Assert
            Assert.NotNull(rs);
        }

        [Fact]
        public async Task GetAllBranchWarehouse_ReturnResult()
        {
            var actual = await _branchWarehouseService.GetAllBranchWarehouse();

            Assert.NotNull(actual);
            Assert.True(!actual.Any());
        }

        [Fact]
        public async Task GetBranchHaveDebt_ReturnResult()
        {
            var actual = await _branchWarehouseService.GetBranchHaveDebt();

            Assert.NotNull(actual);
            Assert.True(!actual.Any());
        }
    }
}
