﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Moq;
using MoqExpression;
using System.Data;
using System.Linq.Expressions;
using WMS.Business.Abstractions.Service;
using WMS.Business.Dtos.CommonDto;
using WMS.Business.Dtos.Mapper;
using WMS.Business.Dtos.S3Dto;
using WMS.Business.Dtos.UserDto;
using WMS.Business.Services;
using WMS.Domain.Abstractions;
using WMS.Domain.Abstractions.Repository;
using WMS.Domain.Entities;
using WMS.Domain.Enums;
using WMS.UnitTest.DummiesData;
using WMS.UnitTest.ImageTest;

namespace WMS.UnitTest.Business
{

    public class UserServiceTest
    {
        private readonly UserService _userService;
        private readonly Mock<IUnitOfWork> _unitOfWork = new Mock<IUnitOfWork>();
        private readonly Mock<IS3StorageService> _s3StorageServiceMock = new Mock<IS3StorageService>();
        private readonly Mock<IWmsUserRepository> _userRepoMock = new Mock<IWmsUserRepository>();
        private readonly Mock<IEmailService> _emailServiceMock = new Mock<IEmailService>();
        private readonly IMock<ILogger<UserService>> _logger = new Mock<ILogger<UserService>>();
        private readonly Mock<IWmsTokenRepository> _tokenRepoMock = new Mock<IWmsTokenRepository>();
        private readonly Mock<ITokenService> _tokenServiceMock = new Mock<ITokenService>();
        private readonly IMapper _mapper;
        private IQueryable<WmsUser> wmsUsers = UserData.Data.AsAsyncQueryable();

        public UserServiceTest()
        {
            //Setup mock file using a memory stream
            var content = "Hello World from a Fake File";
            var fileName = "test.pdf";
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(content);
            writer.Flush();
            stream.Position = 0;

            //create FormFile with desired data
            IFormFile file = new FormFile(stream, 0, stream.Length, "id_from_form", fileName);



            _emailServiceMock.Setup(x => x.SendEmail(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(true);
            var mockTran = new Mock<IDbTransaction>();

            _unitOfWork.Setup(x => x.BeginTransaction()).Returns(mockTran.Object);
            _unitOfWork.Setup(x => x.UserRepository).Returns(_userRepoMock.Object);
            _unitOfWork.Setup(x => x.TokenRepository).Returns(_tokenRepoMock.Object);

            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(new DomainToDtoProfile()));
            var mapper = _mapper = new Mapper(configuration);

            _userService = new UserService(_unitOfWork.Object, _logger.Object, mapper, _emailServiceMock.Object, _s3StorageServiceMock.Object, _tokenServiceMock.Object);
        }

        [Theory]
        [InlineData("thond")]
        [InlineData("")]
        [InlineData(null)]
        public async Task GetProfileInformationByUsername_ShouldReturnUser_WhenUsernameExist(string username)
        {
            //set up for GetProfileInformationByUsername
            var userDefault = new WmsUser()
            {
                UserId = Guid.Parse("36103f08-72ad-440b-b535-ac8f846c1b0d"),
                Fullname = "Thodz",
                Email = "tkokoi123@gmail.com",
                Gender = true,
                PhoneNumber = "0349019012",
                Birthdate = DateTime.Parse("2023-10-19"),
                Address = "Ha noi",
                Avatar = "Avatar/thond_5a1c8ba4-33e0-4146-9b85-87529c4acf1e.JPG",
                Username = "thond",
            };

            _userRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsUser>(f => f.Username.Equals(username)
               && f.Status == (short)UserStatusEnum.Active))).ReturnsAsync(userDefault);

            string avatarUrl = "https://s3.cloudfly.vn/wms-obj-st/Avatar/thond_5a1c8ba4-33e0-4146-9b85-87529c4acf1e.JPG?AWSAccessKeyId=YS1LQQTRXWZOSGY94EUB&Expires=1698419895&Signature=uZRcIy77ls4hg8Lyo5TluiOAapg%3D";
            _s3StorageServiceMock.Setup(s3 => s3.GetFileUrl(It.IsAny<S3RequestData>())).Returns(avatarUrl);

            //Act
            var user = await _userService.GetProfileInformationByUsername(username);


            var s3Rq = new S3RequestData()
            {
                Name = userDefault.Avatar
            };

            //Assert
            Assert.Equal(user.UserId, Guid.Parse("36103f08-72ad-440b-b535-ac8f846c1b0d"));
            Assert.Equal(user.Fullname, "Thodz");
            Assert.Equal(user.Email, "tkokoi123@gmail.com");
            Assert.Equal(user.Gender, true);
            Assert.Equal(user.PhoneNumber, "0349019012");
            Assert.Equal(user.Birthdate, DateOnly.Parse("2023-10-19"));
            Assert.Equal(user.Address, "Ha noi");
            Assert.Equal(user.Avatar, "https://s3.cloudfly.vn/wms-obj-st/Avatar/thond_5a1c8ba4-33e0-4146-9b85-87529c4acf1e.JPG?AWSAccessKeyId=YS1LQQTRXWZOSGY94EUB&Expires=1698419895&Signature=uZRcIy77ls4hg8Lyo5TluiOAapg%3D");

            _s3StorageServiceMock.Verify(x => x.GetFileUrl(It.IsAny<S3RequestData>()), Times.Once);

        }

        [Theory]
        [InlineData("thond", "/ImageTest/jpgReal.jpg", "Avatar/default_male.jpg", true, true, true)]
        [InlineData("", "/ImageTest/jpgReal.jpg", "Avatar/default_male.jpg", true, true, true)]
        [InlineData(null, "/ImageTest/jpgReal.jpg", "Avatar/default_male.jpg", true, true, true)]
        [InlineData("thond", "/ImageTest/jpgReal.jpg", "Avatar/hehe.jpg", true, true, false)]
        [InlineData("thond", "/ImageTest/jpgFake.jpg", "Avatar/default_male.jpg", true, false, true)]
        public async Task EditProfileInformationByUsername_ShouldReturnTrue_WhenUsernameExist(string username,
                                                                                              string nameImg,
                                                                                              string nameAvatar,
                                                                                              bool resultUpdate,
                                                                                              bool resultValidImage,
                                                                                              bool resultImageDefault)
        {
            // Arrange.
            var currentPath = System.IO.Directory.GetCurrentDirectory().Split("\\bin")[0];
            var sourceImg = $@"{currentPath}{nameImg}";

            var ifromFile = FileHelper.GenFormFile($"{sourceImg}", "image/jpg", "data", "d");
            // Process file
            await using var ms = new MemoryStream();
            await ifromFile.CopyToAsync(ms);
            //set up for GetProfileInformationByUsername
            //Arrange
            var user = new WmsUser();
            if (!String.IsNullOrEmpty(username))
            {
                user = new WmsUser()
                {
                    Username = username,
                    UserId = Guid.Parse("36103f08-72ad-440b-b535-ac8f846c1b0e"),
                    Email = "namtm@gmail.com",
                    Avatar = nameAvatar,

                };
            }
            else
            {
                user = null;
            }

            _userRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsUser>(f => f.Username.Equals(username) && f.Status == (short)UserStatusEnum.Active)))
                .ReturnsAsync(user);
            _s3StorageServiceMock.Setup(x => x.DeleteFileAsync(It.IsAny<S3RequestData>()));

            //Act
            var resultEdit = await _userService.EditProfileInformationByUsername(new EditProfileInformationRequestDto()
            {
                Fullname = "Thọ dz vô địch",
                Email = "tkokoi123@gmail.com",
                Gender = true,
                PhoneNumber = "0349019012",
                Birthdate = "2023-10-19",
                Address = "Khu 2 Hoàng Cương, huyện Thanh Ba, tỉnh Phú Thọ",
            }, ifromFile, username);

            //Assert

            if (String.IsNullOrEmpty(username))
            {
                _s3StorageServiceMock.Verify(x => x.DeleteFileAsync(It.IsAny<S3RequestData>()), Times.Never);
                _s3StorageServiceMock.Verify(x => x.UploadFileAsync(It.IsAny<S3RequestData>()), Times.Never);
            }

            if (resultUpdate && !String.IsNullOrEmpty(username)) _userRepoMock.Verify(r => r.Update(user), Times.Once);
            if (resultValidImage && resultImageDefault && !String.IsNullOrEmpty(username))
            {
                _s3StorageServiceMock.Verify(x => x.DeleteFileAsync(It.IsAny<S3RequestData>()), Times.Never);
                _s3StorageServiceMock.Verify(x => x.UploadFileAsync(It.IsAny<S3RequestData>()), Times.Once);
            }
            if (resultValidImage && !resultImageDefault && !String.IsNullOrEmpty(username))
            {
                _s3StorageServiceMock.Verify(x => x.DeleteFileAsync(It.IsAny<S3RequestData>()), Times.Once);
                _s3StorageServiceMock.Verify(x => x.UploadFileAsync(It.IsAny<S3RequestData>()), Times.Once);
            }
        }

        [Theory]
        [InlineData("namtm", true)]
        [InlineData("", false)]
        [InlineData(null, false)]
        public async Task Logout(string username, bool result)
        {
            //Arrange
            var user = new WmsUser()
            {
                Username = "namtm",
                UserId = Guid.Parse("36103f08-72ad-440b-b535-ac8f846c1b0e"),
            };

            var userToken = new WmsToken()
            {
                UserId = user.UserId,
                Token = "abcxyz"
            };

            _userRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsUser>(x => x.Username.Equals(user.Username))))
                .ReturnsAsync(user);

            _tokenRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsToken>(x => x.UserId.Equals(user.UserId))))
                .ReturnsAsync(userToken);

            _tokenRepoMock.Setup(x => x.Delete(It.IsAny<WmsToken>()));

            var mockTran = new Mock<IDbTransaction>();
            _unitOfWork.Setup(x => x.BeginTransaction()).Returns(mockTran.Object);

            //Act
            var actualResult = await _userService.Logout(username).ConfigureAwait(false);

            //Assert
            Assert.Equal(result, actualResult);
            if (result) _tokenRepoMock.Verify(r => r.Delete(userToken), Times.Once);
            else _tokenRepoMock.Verify(r => r.Delete(userToken), Times.Never);
        }

        [Theory]
        [InlineData("namtm", "namtm@gmail.com", true)]
        [InlineData("", "namtm@gmail.com", false)]
        [InlineData(null, "namtm@gmail.com", false)]
        [InlineData("namtm", "abcxyz@gmail.com", false)]
        [InlineData("namtm", "", false)]
        [InlineData("namtm", null, false)]
        public async Task ResetPassword(string username, string email, bool result)
        {
            //Arrange
            var user = new WmsUser()
            {
                Username = "namtm",
                UserId = Guid.Parse("36103f08-72ad-440b-b535-ac8f846c1b0e"),
                Email = "namtm@gmail.com",
            };

            _userRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsUser>(f => f.Username.Equals("namtm") && f.Status == (short)UserStatusEnum.Active)))
                .ReturnsAsync(user);
            _userRepoMock.Setup(x => x.Update(user));

            _emailServiceMock.Setup(x => x.SendEmail(email, It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(true);

            //Act
            var actualResult = await _userService.ResetPassword(new ResetPasswordRequestDto()
            {
                Username = username,
                Email = email,
            }).ConfigureAwait(false);

            //Assert
            Assert.Equal(result, actualResult);
            if (result)
            {
                _userRepoMock.Verify(r => r.Update(user), Times.Once);
                _emailServiceMock.Verify(x => x.SendEmail(email, It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            }
            else
            {
                _userRepoMock.Verify(r => r.Update(user), Times.Never);
                _emailServiceMock.Verify(x => x.SendEmail(email, It.IsAny<string>(), It.IsAny<string>()), Times.Never);
            }
        }

        [Theory]
        [InlineData("nprettejohns0", "12345", "abcxyz", "abcxyz", true)]
        [InlineData("nprettejohns0", "12345", "12345", "12345", false)] //new password is still the same with old password
        [InlineData("nprettejohns0", "wrongPassword", "abcxyz", "abcxyz", false)]
        [InlineData("", "12345", "abcxyz", "abcxyz", false)]
        public async Task ChangePassword(string username, string oldPass, string newPass, string reNewPass, bool result)
        {
            //Arrange
            var entity = new ChangePasswordRequestDto()
            {
                Username = username,
                OldPassword = oldPass,
                NewPassword = newPass,
                ReEnterNewPassword = reNewPass
            };

            var user = UserData.Data.First();
            var oldPassword = user.HashedPassword;
            _userRepoMock.Setup(x => x.FindObject(MoqHelper.IsExpression<WmsUser>(f => f.Username.Equals("nprettejohns0") && f.Status == (short)UserStatusEnum.Active)))
                .ReturnsAsync(user);
            _userRepoMock.Setup(x => x.Update(user));

            //Act
            var actualResult = await _userService.ChangePassword(entity).ConfigureAwait(false);

            //Assert
            Assert.Equal(result, actualResult);
            if (result)
            {
                _userRepoMock.Verify(r => r.Update(user), Times.Once);
                Assert.NotEqual(oldPassword, user.HashedPassword);
            }
            else
            {
                _userRepoMock.Verify(r => r.Update(user), Times.Never);
                Assert.Equal(oldPassword, user.HashedPassword);
            }
        }

        [Theory]
        [MemberData(nameof(ViewAccountsListData_NormalCase))]
        public async Task ViewAccountsList_NormalCase(ViewAccountsListRequestDto entity)
        {
            //Arrange
            if (entity.SearchWord != null && entity.Status != null)
            {
                wmsUsers = wmsUsers.Where(x => x.Fullname.ToLower().Contains(entity.SearchWord) && x.Status == entity.Status);
            }
            else if (entity.SearchWord == null && entity.Status != null)
            {
                wmsUsers = wmsUsers.Where(x => x.Status == entity.Status);
            }
            else if (entity.Status == null && entity.SearchWord != null)
            {
                wmsUsers = wmsUsers.Where(x => x.Fullname.ToLower().Contains(entity.SearchWord));
            }
            _userRepoMock.Setup(x => x.SearchAccountsList(entity.SearchWord, entity.Status)).Returns(wmsUsers);

            //Act
            var actualResult = await _userService.ViewAccountsList(entity).ConfigureAwait(false);

            //Assert
            var size = entity.PagingRequest.PageSize < wmsUsers.Count() ? entity.PagingRequest.PageSize : wmsUsers.Count();
            Assert.Equal(actualResult.Items.Count(), size <= 0 ? 10 : size);
            Assert.NotNull(actualResult.Items);
            if (entity.Status != null) Assert.True(entity.Status == actualResult.Items.First().Status);
            if (entity.SearchWord != null) Assert.Contains(entity.SearchWord.ToLower(), actualResult.Items.First().Username.ToLower());
            Assert.True(actualResult.Pagination.StartPage <= actualResult.Pagination.EndPage);
        }

        public static IEnumerable<object[]> ViewAccountsListData_NormalCase()
        {
            yield return new object[] {new ViewAccountsListRequestDto()
            {
                SearchWord = "john", Status = 1, PagingRequest = new PagingRequest() {CurrentPage = 1, PageSize = 5, PageRange = 5},
            }};
            yield return new object[] {new ViewAccountsListRequestDto()
            {
                SearchWord = null, Status = null, PagingRequest = new PagingRequest() {CurrentPage = 1, PageSize = 5, PageRange = 5},
            }};
            yield return new object[] {new ViewAccountsListRequestDto()
            {
                SearchWord = null, Status = 1, PagingRequest = new PagingRequest() {CurrentPage = 1, PageSize = 5, PageRange = 5},
            }};
            yield return new object[] {new ViewAccountsListRequestDto()
            {
                SearchWord = "john", Status = null, PagingRequest = new PagingRequest() {CurrentPage = 1, PageSize = 5, PageRange = 5},
            }};
        }

        [Theory]
        [MemberData(nameof(ViewAccountsListData_AbnormalCase))]
        public async Task ViewAccountsList_AbnormalCase(ViewAccountsListRequestDto entity)
        {
            //Arrange
            _userRepoMock.Setup(x => x.SearchAccountsList(entity.SearchWord, entity.Status)).Returns(wmsUsers);

            //Act
            var actualResult = await _userService.ViewAccountsList(entity).ConfigureAwait(false);

            //Assert
            var size = entity.PagingRequest.PageSize < wmsUsers.Count() ? entity.PagingRequest.PageSize : wmsUsers.Count();
            Assert.Equal(actualResult.Items.Count(), size <= 0 ? 10 : size);
            Assert.NotNull(actualResult.Items);
            if (entity.Status != null) Assert.True(entity.Status == actualResult.Items.First().Status);
            if (entity.SearchWord != null) Assert.Contains(entity.SearchWord.ToLower(), actualResult.Items.First().Username.ToLower());
            Assert.True(actualResult.Pagination.StartPage <= actualResult.Pagination.EndPage);
        }

        public static IEnumerable<object[]> ViewAccountsListData_AbnormalCase()
        {
            yield return new object[] {new ViewAccountsListRequestDto()
            {
                SearchWord = null, Status = null, PagingRequest = new PagingRequest() {CurrentPage = -1, PageSize = 5, PageRange = 5},
            }};
            yield return new object[] {new ViewAccountsListRequestDto()
            {
                SearchWord = null, Status = null, PagingRequest = new PagingRequest() {CurrentPage = 1, PageSize = -1, PageRange = 5},
            }};
            yield return new object[] {new ViewAccountsListRequestDto()
            {
                SearchWord = null, Status = null, PagingRequest = new PagingRequest() {CurrentPage = 1, PageSize = 5, PageRange = -1},
            }};
        }

        [Fact]
        public async Task AccountLogin_WithValidEmail_ReturnsUser()
        {
            // Arrange
            var rqDto = new UserLoginRequestDto("validusername@example.com", "12345");
            var userIdMock = Guid.NewGuid();


            var mockUser = new WmsUser
            {
                UserId = userIdMock,
                Email = rqDto.UserName,
                HashedPassword = "f3dce89cf29bbed5055a9d5be8f0cf889f1098556b05740001b85f2fc97c02fe",
                SaltKey = "abcdefghik"
            };

            var userToken = new WmsToken()
            {
                UserId = mockUser.UserId,
                Token = "abcxyz"
            };

            _tokenServiceMock.Setup(x => x.GetTokenByUserId(It.IsAny<Guid>())).ReturnsAsync(userToken);
            _tokenServiceMock.Setup(x => x.RevokeAccessToken(It.IsAny<string>()));

            _userRepoMock.Setup(repo => repo.FindObject(It.IsAny<Expression<Func<WmsUser, bool>>>()))
                .ReturnsAsync((Expression<Func<WmsUser, bool>> predicate) =>
                {
                    var users = new List<WmsUser>
                    {
                        new WmsUser
                        {
                            UserId = userIdMock,
                            Email = rqDto.UserName,
                            HashedPassword = "f3dce89cf29bbed5055a9d5be8f0cf889f1098556b05740001b85f2fc97c02fe",
                            SaltKey = "abcdefghik",
                            Status = (short)UserStatusEnum.Active
                        },
                        new WmsUser
                        {
                            UserId = Guid.NewGuid(),
                            Email = "another@example.com",
                            HashedPassword = "f3dce89cf29bbed5055a9d5be8f0cf889f1098556b05740001b85f2fc97c02fe",
                            SaltKey = "abcdefghik",
                            Status = (short)UserStatusEnum.Active
                        },
                    };

                    var filteredUsers = users.AsQueryable().Where(predicate.Compile()).ToList();
                    return filteredUsers.FirstOrDefault();
                });

            // Act
            var result = await _userService.AccountLogin(rqDto);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(mockUser.UserId, result.UserId);
            Assert.Equal(mockUser.Email, result.Email);
        }

        [Fact]
        public async Task AccountLogin_WithValidUsername_ReturnsUser()
        {
            // Arrange
            var rqDto = new UserLoginRequestDto("root", "12345");
            var userIdMock = Guid.NewGuid();


            var mockUser = new WmsUser
            {
                UserId = userIdMock,
                Username = rqDto.UserName,
                HashedPassword = "f3dce89cf29bbed5055a9d5be8f0cf889f1098556b05740001b85f2fc97c02fe",
                SaltKey = "abcdefghik"
            };

            var userToken = new WmsToken()
            {
                UserId = mockUser.UserId,
                Token = "abcxyz"
            };

            _tokenServiceMock.Setup(x => x.GetTokenByUserId(It.IsAny<Guid>())).ReturnsAsync(userToken);
            _tokenServiceMock.Setup(x => x.RevokeAccessToken(It.IsAny<string>()));

            _userRepoMock.Setup(repo => repo.FindObject(It.IsAny<Expression<Func<WmsUser, bool>>>()))
                .ReturnsAsync((Expression<Func<WmsUser, bool>> predicate) =>
                {
                    var users = new List<WmsUser>
                    {
                        new WmsUser
                        {
                            UserId = userIdMock,
                            Username = rqDto.UserName,
                            HashedPassword = "f3dce89cf29bbed5055a9d5be8f0cf889f1098556b05740001b85f2fc97c02fe",
                            SaltKey = "abcdefghik",
                            Status = (short)UserStatusEnum.Active
                        },
                        new WmsUser
                        {
                            UserId = Guid.NewGuid(),
                            Username = "admin",
                            HashedPassword = "f3dce89cf29bbed5055a9d5be8f0cf889f1098556b05740001b85f2fc97c02fe",
                            SaltKey = "abcdefghik",
                            Status = (short)UserStatusEnum.Active
                        },
                    };

                    var filteredUsers = users.AsQueryable().Where(predicate.Compile()).ToList();
                    return filteredUsers.FirstOrDefault();
                });

            // Act
            var result = await _userService.AccountLogin(rqDto);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(mockUser.UserId, result.UserId);
            Assert.Equal(mockUser.Username, result.Username);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("paul.example.com")]
        [InlineData("@example.com")]
        public async Task AccountLogin_WithInValidEmail_ReturnsUser(string email)
        {
            // Arrange
            var rqDto = new UserLoginRequestDto(email, "12345");
            var userIdMock = Guid.NewGuid();


            var mockUser = new WmsUser
            {
                UserId = userIdMock,
                Email = rqDto.UserName,
                HashedPassword = "f3dce89cf29bbed5055a9d5be8f0cf889f1098556b05740001b85f2fc97c02fe",
                SaltKey = "abcdefghik"
            };

            var userToken = new WmsToken()
            {
                UserId = mockUser.UserId,
                Token = "abcxyz"
            };

            _tokenServiceMock.Setup(x => x.GetTokenByUserId(It.IsAny<Guid>())).ReturnsAsync(userToken);
            _tokenServiceMock.Setup(x => x.RevokeAccessToken(It.IsAny<string>()));

            _userRepoMock.Setup(repo => repo.FindObject(It.IsAny<Expression<Func<WmsUser, bool>>>()))
                .ReturnsAsync((Expression<Func<WmsUser, bool>> predicate) =>
                {
                    var users = new List<WmsUser>
                    {
                        new WmsUser
                        {
                            UserId = userIdMock,
                            Email = rqDto.UserName,
                            HashedPassword = "f3dce89cf29bbed5055a9d5be8f0cf889f1098556b05740001b85f2fc97c02fe",
                            SaltKey = "abcdefghik",
                            Status = (short)UserStatusEnum.Active
                        },
                        new WmsUser
                        {
                            UserId = Guid.NewGuid(),
                            Email = "another@example.com",
                            HashedPassword = "f3dce89cf29bbed5055a9d5be8f0cf889f1098556b05740001b85f2fc97c02fe",
                            SaltKey = "abcdefghik",
                            Status = (short)UserStatusEnum.Active
                        },
                    };

                    var filteredUsers = users.AsQueryable().Where(predicate.Compile()).ToList();
                    return filteredUsers.FirstOrDefault();
                });

            // Act
            var result = await _userService.AccountLogin(rqDto);

            // Assert
            Assert.Null(result);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("paul.example.com")]
        [InlineData("@example.com")]
        public async Task AccountLogin_WithInValidUsername_ReturnsUser(string username)
        {
            // Arrange
            var rqDto = new UserLoginRequestDto(username, "12345");
            var userIdMock = Guid.NewGuid();


            var mockUser = new WmsUser
            {
                UserId = userIdMock,
                Username = rqDto.UserName,
                HashedPassword = "f3dce89cf29bbed5055a9d5be8f0cf889f1098556b05740001b85f2fc97c02fe",
                SaltKey = "abcdefghik"
            };

            var userToken = new WmsToken()
            {
                UserId = mockUser.UserId,
                Token = "abcxyz"
            };

            _tokenServiceMock.Setup(x => x.GetTokenByUserId(It.IsAny<Guid>())).ReturnsAsync(userToken);
            _tokenServiceMock.Setup(x => x.RevokeAccessToken(It.IsAny<string>()));

            _userRepoMock.Setup(repo => repo.FindObject(It.IsAny<Expression<Func<WmsUser, bool>>>()))
                .ReturnsAsync((Expression<Func<WmsUser, bool>> predicate) =>
                {
                    var users = new List<WmsUser>
                    {
                        new WmsUser
                        {
                            UserId = userIdMock,
                            Username = rqDto.UserName,
                            HashedPassword = "f3dce89cf29bbed5055a9d5be8f0cf889f1098556b05740001b85f2fc97c02fe",
                            SaltKey = "abcdefghik",
                            Status = (short)UserStatusEnum.Active
                        },
                        new WmsUser
                        {
                            UserId = Guid.NewGuid(),
                            Username = "admin",
                            HashedPassword = "f3dce89cf29bbed5055a9d5be8f0cf889f1098556b05740001b85f2fc97c02fe",
                            SaltKey = "abcdefghik",
                            Status = (short)UserStatusEnum.Active
                        },
                    };

                    var filteredUsers = users.AsQueryable().Where(predicate.Compile()).ToList();
                    return filteredUsers.FirstOrDefault();
                });

            // Act
            var result = await _userService.AccountLogin(rqDto);

            // Assert
            Assert.Null(result);
        }

        [Fact]
        public async Task UpdateUserStatus_When_UserExists_Should_Return_UserId()
        {
            // Arrange
            var userId = Guid.NewGuid();
            var userRq = new ChangeUserStatusRequestDto { Username = "testUser", UserStatus = (short)UserStatusEnum.Active };

            var existingUser = new WmsUser { UserId = userId, Username = "testUser", Status = (short)UserStatusEnum.Disable };

            _userRepoMock.Setup(repo => repo.FindObject(It.IsAny<Expression<Func<WmsUser, bool>>>()))
                             .ReturnsAsync(existingUser);

            // Act
            var result = await _userService.UpdateUserStatus(userRq);

            // Assert
            Assert.Equal(userId, result);
            _userRepoMock.Verify(repo => repo.Update(existingUser), Times.Once);
            _unitOfWork.Verify(uow => uow.CommitAsync(), Times.Once);
        }

        [Fact]
        public async Task UpdateUserStatus_When_UserDoesNotExist_Should_Return_EmptyGuid()
        {
            // Arrange
            var userRq = new ChangeUserStatusRequestDto { Username = "nonExistingUser", UserStatus = (short)UserStatusEnum.Active };

            _userRepoMock.Setup(repo => repo.FindObject(It.IsAny<Expression<Func<WmsUser, bool>>>()))
                             .ReturnsAsync((WmsUser)null);

            // Act
            var result = await _userService.UpdateUserStatus(userRq);

            // Assert
            Assert.Equal(Guid.Empty, result);
            _userRepoMock.Verify(repo => repo.Update(It.IsAny<WmsUser>()), Times.Never);
            _unitOfWork.Verify(uow => uow.CommitAsync(), Times.Never);
        }
    }
}
